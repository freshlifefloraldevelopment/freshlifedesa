<?php
session_start();

if(!$_SESSION["subclient"]){
    header("Location: login.php");
		exit();
}


$clientSessionID = $_SESSION["subclient"];
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 5 Mayo 2021
Project: Client market place
Add Protection SQL INY, XSS
**/
require_once("../../config/config_gcp.php");
include('GlobalFSyn.php');


$display = 20;
$XX = '<div class="notfound" style="margin-top:30px;">No Item Found !</div>';

if(isset($_POST["newbox"]) && isset($_POST["subclient"]) && $_POST["newbox"]!='')
{

$newOrderFilter = $_POST['newbox'];
$clientID_Prod = $_POST['subclient'];
$_SESSION["SubCliID"] = $clientID_Prod;
$_SESSION["newOrderF"] = $newOrderFilter;

if(!$clientSessionID)
{
	header("Location: login.php");
	exit();
}


}

if (isset($_POST["startrowMB"]) && $_POST["startrowMB"] != "") {
  //    $sr = $_POST["startrowMB"] + 1;
  $clientID_Prod = $_SESSION["SubCliID"];
  $newOrderFilter = $_SESSION["newOrderF"];

    $filtroCheck = $_POST["startrowMB"];

      if($filtroCheck[0]=='C'){
        $_SESSION["FCat"] = substr($filtroCheck,1);
        $_POST["startrowMB"] = 0;
      }else{
        if($filtroCheck[0]=='P'){
          $_POST["startrowMB"] = substr($filtroCheck,1);
        }else
        {
            if($filtroCheck[0]=='Y')
            {
              $_POST["startrowMB"] = substr($filtroCheck,1);
            }
            else
            {
              $porcionesFiltros = explode("**FLF**", $filtroCheck);

              $filtroColors = $porcionesFiltros[0];
              $filtroSizes = $porcionesFiltros[1];
              $filtroFeatures = $porcionesFiltros[2];

                    if($filtroColors[0]==','){
                      $_SESSION["FColor"] = substr($filtroColors,1);
                    }else{ $_SESSION["FColor"] = $filtroColors; }

                    if($filtroSizes[0]==','){
                      $_SESSION["FSize"] = substr($filtroSizes,1);
                    }
                    if($filtroFeatures[0]==','){
                      $_SESSION["FFeat"] = substr($filtroFeatures,1);
                    }
                    $_POST["startrowMB"] = 0;
            }
        }

      }



     $num_recordMB = numberRecord($con,$_SESSION["buyer"],$_SESSION["FCat"],$_SESSION["FColor"],$_SESSION['FSize'],$_SESSION["FFeat"],'');
    $query2 = query_main($_POST["startrowMB"], $display,$_SESSION["buyer"],$_SESSION['FCat'],$_SESSION['FColor'],$_SESSION['FSize'],$_SESSION['FFeat'],'',$con);
     $sql_products = mysqli_query($con, $query2);
  }
  else
  {

    $newOrderFilter = $_SESSION["newOrderF"];
    //if(isset($_GET["sid"]) && $_GET["sid"] != "")
    if(isset($_GET["sid"]) && isset($_GET["pid"]) && $_GET["pid"] != "" && $_GET["sid"] != "")
    {

      $_SESSION['FCat']='';
      $_SESSION['FColor']='';
      $_SESSION['FSize']='';
      $_SESSION['FFeat']='';

      $inicio = 0;

       $num_recordMB = numberRecord($con,$_SESSION["buyer"],$_GET["sid"],$_SESSION["FColor"],$_SESSION['FSize'],$_SESSION["FFeat"],$_GET["pid"]);
       $query2 = query_main($inicio, $display,$_SESSION["buyer"],$_GET["sid"],$_SESSION['FColor'],$_SESSION['FSize'],$_SESSION['FFeat'],$_GET["pid"],$con);
     $sql_products = mysqli_query($con, $query2);


   }else
   {

     if($clientSessionID==''){
       header("Location: login.php");
       exit();
     }
        if (empty($startrow))
        {
            $startrow = 0;
            $sr = 1;
        }
        //Inicialización de filtros globales
        $_SESSION['FCat']='';
        $_SESSION['FColor']='';
        $_SESSION['FSize']='';
        $_SESSION['FFeat']='';
        //
         $num_recordMB = numberRecord($con,$_SESSION["buyer"],$_SESSION['FCat'],$_SESSION['FColor'],$_SESSION['FSize'],$_SESSION["FFeat"],'');
         $query2 = query_main(0, $display,$_SESSION["buyer"],$_SESSION['FCat'],$_SESSION['FColor'],$_SESSION['FSize'],$_SESSION['FFeat'],'',$con);
          $sql_products = mysqli_query($con, $query2);
    }
}


include('inc/header-2.php');
?>
<!-- PAGE TITLE -->
			<section class="bg-light p-0">
				<div class="container py-5">

					<h1 class="h3">
						Shop Category
					</h1>

					<nav aria-label="breadcrumb">
						<ol class="breadcrumb fs--14">
							<li class="breadcrumb-item"><a href="login.php">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Category</li>
						</ol>
					</nav>

				</div>
			</section>
			<!-- /PAGE TITLE -->




<!-- -->
			<section>
				<div class="container">

					<input type="hidden" id="buyerId" value="<?php echo ','.$_SESSION["buyer"]; ?>" />
					<input type="hidden" id="newBoxMixId" value="<?php echo $newOrderFilter; ?>" />
					<input type="hidden" id="CatId" value="<?php echo ','.$_SESSION['FCat']; ?>" />
					<input type="hidden" id="ColorId" value="<?php echo ','.$_SESSION["FColor"]; ?>" />
					<input type="hidden" id="SizeId" value="<?php echo ','.$_SESSION['FSize']; ?>" />
					<input type="hidden" id="FeatureId" value="<?php echo ','.$_SESSION['FFeat']; ?>" />
					<input type="hidden" id="subclient" value="<?php echo $_SESSION["subclient"]; ?>" />


					<div class="row">

						<!-- sidebar -->
						<div class="col-12 col-sm-12 col-md-12 col-lg-3 mb--60">


							<!-- CATEGORIES -->
							<nav class="nav-deep nav-deep-light mb-4 shadow-xs shadow-none-md shadow-none-xs px-4 pb-3 p-0-md p-0-xs rounded">

								<!-- mobile trigger : categories -->
								<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none" data-target="#nav_responsive" data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3">
									<span class="group-icon px-2 py-2 float-start">
										<i class="fi fi-bars-2"></i>
										<i class="fi fi-close"></i>
									</span>

									<span class="h5 py-2 m-0 float-start">
										Categories
									</span>
								</button>

								<!-- desktop only -->
								<h5 class="h6 pt-3 pb-3 m-0 d-none d-lg-block">
									Categories
								</h5>


								<!-- navigation -->
								<ul id="nav_responsive" class="nav flex-column d-none d-lg-block">

									<div id="categoryGrowerId">
									</div>

								</ul>

							</nav>
							<!-- /CATEGORIES -->



							<!-- mobile trigger : filters -->
							<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none" data-target="#sidebar_filters" data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" data-toggle-body-class="overflow-hidden">
								<i class="px-2 py-2 fs--15 float-start fi fi-eq-horizontal"></i>
								<span class="h5 py-2 m-0 float-start">
									Filters
								</span>
							</button>



                	<form class="d-none d-lg-block" id="sidebar_filters" name="sidebar_filters" onsubmit="return false;">

								<!-- MOBILE ONLY -->
								<div class="bg-white pb-3 mb-4 d-block d-lg-none border-bottom">


									<i class="fi fi-eq-horizontal float-start"></i>
									<span class="h5 m-0 d-inline-block">
										Filters
									</span>

									<!-- mobile : exit fullscreen -->
									<a href="#" class="float-end btn-toggle text-dark mx-1" data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" data-toggle-body-class="overflow-hidden" data-target="#sidebar_filters">
										<i class="fi fi-close"></i>
									</a>

								</div>
								<!-- /MOBILE ONLY -->





								<!-- Reset Filters -->
								<div class="card rounded b-0 shadow-xs d-block mb-4 p-3">
									<a href="subclient-market-place.php" class="text-danger float-end w--20 d-inline">
										<i class="fi fi-close"></i>
									</a>
									Reset Filters
								</div>
								<!-- /Reset Filters -->




								<!-- Color -->
								<div class="card rounded b-0 shadow-xs d-block mb-4 p-3">
									<h3 class="fs--15 mb-4">

										<a href="#" data-target-reset="#filter_color_list" class="form-advanced-reset hide-force text-danger float-end w--20 d-inline">
											<i class="fi fi-close"></i>
										</a>

										Color
									</h3>

									<div id="filter_color_list">

									</div>

								</div>


								<!-- Size -->
								<div class="card rounded b-0 shadow-xs d-block mb-4 p-3">
									<h3 class="fs--15 mb-4">

										<a href="#" data-target-reset="#filter_size_list" class="form-advanced-reset hide-force text-danger float-end w--20 d-inline">
											<i class="fi fi-close"></i>
										</a>

										Size
									</h3>

									<div id="filter_size_list">

									</div>

								</div>

								<!-- Features -->
								<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
									<h3 class="fs--15 mb-3">
										<a class="form-advanced-reset hide-force text-danger float-end w--20 d-inline" data-target-reset="#filter_size_list" href="#"><i class="fi fi-close"></i></a> Features
									</h3>
									<div id="filter_features_list">

									</div>
								</div>




<button class="btn btn-primary btn-soft btn-sm btn-block" onclick="funSearchPageFilter()" >Apply Filters</button>
							</form>


						</div>
						<!-- /sidebar -->



						<!-- products -->
						<div class="col-12 col-sm-12 col-md-12 col-lg-9">


							<!-- additional filters -->


							<div class="shadow-xs bg-white mb-5 p-3 rounded clearfix">

								<div class="clearfix border-bottom pb-3 mb-3">

									<div class="float-start fs--14 position-relative mt-1">
										<a href="#!" class="text-primary text-decoration-none btn p-0" data-toggle="dropdown" aria-expanded="false">
                        <div id="numberReg">
                          <i class="fi fi-arrow-down-slim fs--12"></i>
                          12 / page
                        </div>
										</a>

										<ul class="dropdown-menu b-0 mt-3 rounded fs--15">
											<li class="dropdown-item active">
                        <a href="javascript:onclick=funSearchPage('0,12')">12 / page</a>
                      </li>
											<li class="dropdown-item"><a href="javascript:onclick=funSearchPage('0,36')" class="text-muted py-2 d-block">36 / page</a></li>
											<li class="dropdown-item"><a href="javascript:onclick=funSearchPage('0,72')" class="text-muted py-2 d-block">72 / page</a></li>
											<li class="dropdown-item"><a href="javascript:onclick=funSearchPage('0,100')" class="text-muted py-2 d-block">100 / page</a></li>
										</ul>
									</div>

									<div class="float-end fs--14 position-relative mt-1">
										Sort by: &nbsp; <a href="#" class="text-primary text-decoration-none" data-toggle="dropdown" aria-expanded="false">
											Popular First &nbsp;
											<i class="fi fi-arrow-down-slim fs--12"></i>
										</a>

										<ul class="dropdown-menu b-0 mt-3 rounded fs--15">
											<li class="dropdown-item active"><a href="#!" class="text-muted py-2 d-block">Popular First</a></li>
											<li class="dropdown-item"><a href="#!" class="text-muted py-2 d-block">Newest First</a></li>
											<li class="dropdown-item"><a href="#!" class="text-muted py-2 d-block">Avg. Customer Review</a></li>
											<li class="dropdown-item"><a href="#!" class="text-muted py-2 d-block">Price: Low to High</a></li>
											<li class="dropdown-item"><a href="#!" class="text-muted py-2 d-block">Price: High to Low</a></li>
										</ul>
									</div>

								</div>


								<h2 class="h6 mb-0">
									<?php echo $num_recordMB; ?> total items
								</h2>

							</div>
							<!-- /additional filters -->



							<!-- product list -->
							<div class="row gutters-xs--xs">

                <?php
                while($row_products = mysqli_fetch_array($sql_products))
                {
                  $Syni = $Syni+1;
                  $category_id_product = $row_products["categoryid"];
                  $id_product = $row_products["id"];
                  $name_product = $row_products["Produtcname"];
                  $product_size = $row_products["size"];
                  $bunchname  = $row_products["steams"];
                  $img_product = $row_products["img"];
                  $subcategory_product = $row_products["subcatename"];
                  $subcategoryid = $row_products["subcategoryid"];
                  $featureName = $row_products["featurename"];
                  $featureId = $row_products["fid"];
                  $bunchqty  = $row_products["bunchqty"];
                  $idgor     = $row_products['idgor'];

                  if($featureId==''){
                    $featureId= 0;
                  }

                  $getIdSize = mysqli_query($con,"select id from sizes where name ='$product_size'");
                  $getNamesArray = mysqli_fetch_array($getIdSize);
                  $sizeID = $getNamesArray['id'];

                 ?>

								<!-- item -->
								<div class="col-6 col-md-4 mb-4 mb-2-xs">

									<div class="bg-white shadow-md shadow-3d-hover transition-all-ease-250 transition-hover-top rounded show-hover-container p-2 h-100">
<!-- badges, countdown -->
										<div class="position-absolute top-0 start-0 m-3 m-1-xs z-index-1">


											<div class="bg-danger text-white hide animate-fadein fs--13 opacity-9 px-2 py-1 mb-1 rounded timer-countdown timer-countdown-inline"
												data-timer-countdown-from="11/21/2030 16:00:00"
												data-timer-countdown-end-hide-self="true"
												data-timer-countdown-end-hide-target="">

												<b>Bunches available: <?php echo $bunchqty; ?></b>

											</div>

										</div>
										<!-- hover buttons : top -->
										<div class="position-absolute top-0 end-0 text-align-end w--60 z-index-3 m-3 show-hover-item">

											<!-- add to favourite : not logged in -->
											<!--
											<a href="#" class="js-ajax-modal btn bg-white shadow-lg btn-sm rounded-circle mb-2"
											    data-href="_ajax/modal_signin_md.html"
											    data-ajax-modal-size="modal-md"
											    data-ajax-modal-centered="false"
											    data-ajax-modal-backdrop="static">
											    <i class="fi fi-heart-slim"></i>
											</a>
											-->

											<!-- add to favourite : logged in
											<a href="#" class="btn-toggle btn bg-white shadow-lg btn-sm rounded-circle mb-2"
												data-toggle="tooltip"
												data-original-title="add to favourite"
												data-placement="left"

												data-toggle-ajax-url-on="demo.files/php/demo.ajax_request.php?product_id=1&amp;action=add_to_favourite"
												data-toast-success-message="Added to your favourite!"
												data-toast-success-position="bottom-center">
												<i class="fi fi-heart-slim"></i>
											</a>
										-->

											<!-- <a href="#" class="btn bg-white shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="add to compare">
												<i class="fi fi-graph"></i>
											</a> -->

											<a href="#" class="btn btn-danger shadow-lg btn-sm rounded-circle mb-2" title="" data-toggle="tooltip" data-placement="left" data-original-title="Add to Order">
												<i class="fi fi-cart-1"></i>
											</a>
										</div>
										<!-- /hover buttons : top -->
                    <style>
                    .img {
                        float: left;
                        width:  300px;
                        height: 300px;
                        object-fit: cover;
                    }
                    </style>

										<div class="d-block text-decoration-none">

											<figure class="m-0 text-center bg-light-radial rounded-top overflow-hidden">
                        	<img class="img-fluid bg-suprime opacity-9 img" alt="<?php echo $name_product; ?>"  src="https://app.freshlifefloral.com/<?php echo $img_product; ?>" width="300px" height="300px">
											</figure>

											<span class="d-block text-center-xs text-gray-600 py-3">

												<!--
													.max-height-50  = limited to 2 rows of text
													-or-
													.text-truncate
												-->
												<span class="d-block fs--16 max-h-50 overflow-hidden">
												<?php echo $subcategory_product." ".$name_product."<br> Size: ".$product_size.' [cm] '.$bunchname. " st/bu"; ?>
                        <span class="badge badge-warning float-end pl--3 pr--3 pt--2 pb--2 fs--11 mt-1"><?php echo $featureName;?></span>
												</span>

												<!-- price -->
												<span class="d-block text-danger font-weight-medium fs--16 mt-2">
													$<?php echo precioProductSelected($con,$subcategoryid,$sizeID,$id_product,$featureId); ?>
                            <input type="hidden" id="priceSelected<?php echo $Syni; ?>" value="<?php echo precioProductSelected($con,$subcategoryid,$sizeID,$id_product,$featureId); ?>" />
												</span>

<div class="form-label-group mb-3 ">

											<select id="select_options<?php echo $Syni; ?>" class="form-control">
		<?php
		for($i = 1; $i<=$bunchqty;$i++)
		{
		 ?>
		<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
	<?php } ?>
											</select>
											<label for="select_options">Select bunch quantity</label>
										</div>


									<div class="form-label-group mb-3">
	<center>

  	<button onclick="requestProduct('<?php echo $_SESSION["subclient"]; ?>','<?php echo OrderIdBuyer($con,$_SESSION["buyer"]); ?>','<?php echo $_SESSION["buyer"];?>','<?php echo $id_product;?>','<?php echo $Syni;?>','<?php echo $sizeID; ?>','<?php echo $featureId; ?>','<?php echo $idgor; ?>')" class="btn btn-sm rounded-circle-xs btn-danger btn-pill">
	<i class="fi fi-plus"></i>
	<span>Add to Order</span>
</button></center>
</div>

											</span>

										</div>

									</div>

								</div>
								<!-- /item -->
              <?php }
              ?>

							</div>
							<!-- /product list -->


							<!-- pagination -->
								<input type="hidden" id="numRecords" value="<?php echo $num_recordMB ?>" />
							<nav aria-label="pagination" class="mt-5">
								<ul class="pagination pagination-pill justify-content-end justify-content-center justify-content-md-end">

									<?php
									if ($_POST["startrowMB"] != 0) {

											$prevrow = $_POST["startrowMB"] - $display;

											print("<li class='page-item disabled btn-pill'><a aria-disabled='true' class='page-link' tabindex='-1' href=\"javascript:onclick=funMBPage($prevrow)\">Prev </a></li>");
									}
									$pages = intval($num_recordMB / $display);

									if ($num_recordMB % $display) {

											$pages++;
									}
									$numofpages = $pages;
									$cur_page = $_POST["startrowMB"] / $display;
									$range = 5;
									$range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2;
									$range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min;
									$page_min = $cur_page - $range_min;
									$page_max = $cur_page + $range_max;
									$page_min = ($page_min < 1) ? 1 : $page_min;
									$page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max;
									if ($page_max > $numofpages) {
											$page_min = ($page_min > 1) ? $numofpages - $range + 1 : 1;
											$page_max = $numofpages;
									}
									if ($pages > 1) {

											for ($i = $page_min; $i <= $page_max; $i++) {
													if ($cur_page + 1 == $i) {
															$nextrow = $display * ($i - 1);
															print("<li class='page-item active'><a class='page-link' href='javascript:void();'>$i</a></li>");
													} else {

															$nextrow = $display * ($i - 1);
															print("<li class='page-item'><a class='page-link' href=\"javascript:onclick=funMBPage($nextrow)\"> $i </a></li>");
													}
											}

									}
									if ($pages > 1) {

											if (!(($_POST["startrowMB"] / $display) == $pages - 1) && $pages != 1) {

													$nextrow = $_POST["startrowMB"] + $display;

													print("<li class='page-item'><a class='page-link' href=\"javascript:onclick=funMBPage($nextrow)\" class='page-item'> Next</a></li> ");
											}
									}

									if ($num_recordMB < 1) {
											print("<span class='text'>" . $XX . "</span>");
									}
									?>

								</ul>
							</nav>
							<!-- pagination -->



						</div>
						<!-- /products -->

					</div>

				</div>

        <form method="post" name="frmfprdMB" action="">
            <input type="hidden"  name="startrowMB" value="<?php echo $_POST["startrowMB"]; ?>">

        </form>
			</section>



<?php include('inc/footer-2.php'); ?>
<script src="/back-end/assets/js/blockui.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>

<script type="text/javascript">

window.onload=function() {

  //localStorage.clear();


$.blockUI({ baseZ: 20000, message: '<img src="/back-end/images/cargando.gif" width="100px" height="100px" />' });


  var key = localStorage.getItem("idSessionCompras");

  var buyerId = document.getElementById('buyerId').value;
  buyerId = buyerId.substring(1);

  var CateId = document.getElementById('CatId').value;
  CateId = CateId.substring(1);

  var ColorId = document.getElementById('ColorId').value;
  ColorId = ColorId.substring(1);

  var SizeId = document.getElementById('SizeId').value;
  SizeId = SizeId.substring(1);

  var FeatureId = document.getElementById('FeatureId').value;
  FeatureId = FeatureId.substring(1);

  /*var order_id_prev = document.getElementById('ordenPrevId').value;*/
  var clientId = document.getElementById('subclient').value;

  var datos = "idBuyer="+buyerId+"&idCateg="+CateId+"&idColor="+ColorId+"&idSize="+SizeId+"&idFeature="+FeatureId;
  //var datosLoadPrev = "PreviousProductKey="+key+"&idCategory="+CateId+"&ord_id="+order_id_prev+"&gid_id="+growId+"&box_nex="+newBoxMix_Id;

  $.ajax({
       type: "POST",
       url: "selectCategoryGrowers.php",
       data: datos,
       cache: false,
       success: function(r){

       document.getElementById('categoryGrowerId').innerHTML = r;
       }
   });

   $.ajax({
        type: "POST",
        url: "selectColorsGrowers.php",
        data: datos,
        cache: false,
        success: function(r){
        document.getElementById('filter_color_list').innerHTML = r;
        }
    });


    $.ajax({
         type: "POST",
         url: "selectSizesGrowers.php",
         data: datos,
         cache: false,
         success: function(r){
         document.getElementById('filter_size_list').innerHTML = r;
         }
     });

     $.ajax({
          type: "POST",
          url: "selectFeaturesGrowers.php",
          data: datos,
          cache: false,
          success: function(r){
          document.getElementById('filter_features_list').innerHTML = r;
          }
      });


      $.unblockUI();
      //countOrders(clientId);

}

function countProductPrevious(clientId){

var datos = "ord_id="+order_id_prev+"&PreviousProductKey="+PreviousProductKey+"&gid_id="+growId+"&box_nex="+newBoxMix_Id;

  $.ajax({
       type: "POST",
       url: "count_temp_cartBoxMix.php",
       data: datos,
       cache: false,
       success: function(r){
       //  alert(r);
        // return false;
        if(r==0){

           document.getElementById('endMixBox').disabled = true;

        }else{

          document.getElementById('endMixBox').disabled = false;
        }
         document.getElementById('countPreviousBoxMix').innerHTML = r;

       }
   });

}

function endMixBoxB(){

  document.getElementById('newBoxMixId').value = "";
  location.replace("/en/variety-page.php")
}


function dataIIMB(name,img){

var a ='https://app.freshlifefloral.com/';

$('#growers_modal_MP').modal('show');
$('#Modal_MP').html(name);
$('#img-modal').attr('src',a+img);

}

function funMBPage(pageno)
{
$.blockUI({baseZ: 20000, message: '<img src="/back-end/images/cargando.gif" width="100px" height="100px" />' });
  var Pag = 'P'+pageno;
  document.frmfprdMB.startrowMB.value = Pag;
  document.frmfprdMB.submit();
}

function get_color_data(a)
{
  if(document.getElementById('ColorId').value ==''){
    var c = ','+a+',';
  document.getElementById('ColorId').value += c;
}else{

    if(document.getElementById('ColorId').value.includes(','+a+','))
    {
      var z = document.getElementById('ColorId').value.replace(a+',','');
      document.getElementById('ColorId').value = z;
    }else{
      if(document.getElementById('ColorId').value.charAt(document.getElementById('ColorId').value.length-1)==','){
        document.getElementById('ColorId').value += a+',';
      }
   }
}

}

function get_size_data(a)
{
  if(document.getElementById('SizeId').value ==''){
    var c = ','+a+',';
  document.getElementById('SizeId').value += c;
}else{

    if(document.getElementById('SizeId').value.includes(','+a+','))
    {
      var z = document.getElementById('SizeId').value.replace(a+',','');
      document.getElementById('SizeId').value = z;
    }else{
      if(document.getElementById('SizeId').value.charAt(document.getElementById('SizeId').value.length-1)==','){
        document.getElementById('SizeId').value += a+',';
      }
   }
}

}

function get_feature_data(a)
{
  if(document.getElementById('FeatureId').value ==''){
    var c = ','+a+',';
  document.getElementById('FeatureId').value += c;
}else{

    if(document.getElementById('FeatureId').value.includes(','+a+','))
    {
      var z = document.getElementById('FeatureId').value.replace(a+',','');
      document.getElementById('FeatureId').value = z;
    }else{
      if(document.getElementById('FeatureId').value.charAt(document.getElementById('FeatureId').value.length-1)==','){
        document.getElementById('FeatureId').value += a+',';
      }
   }
}

}

function funSearchPage(a){


//document.getElementById('numberReg').innerHTML = 'a';

  $.blockUI({baseZ: 20000, message: '<img src="/back-end/images/cargando.gif" width="100px" height="100px" />' });
  var Perpage = 'Y'+a;
  document.frmfprdMB.startrowMB.value = Perpage;
  document.frmfprdMB.submit();
}

function funSearchPageCat(a){
  $.blockUI({baseZ: 20000, message: '<img src="/back-end/images/cargando.gif" width="100px" height="100px" />' });
  var Cat = 'C'+a;
  document.frmfprdMB.startrowMB.value = Cat;
  document.frmfprdMB.submit();
}

function funSearchPageFilter(){
  $.blockUI({baseZ: 20000, message: '<img src="/back-end/images/cargando.gif" width="100px" height="100px" />' });
  var a = document.getElementById('ColorId').value;
  var b = document.getElementById('SizeId').value;
  var c = document.getElementById('FeatureId').value;

  var Filtros = a+'**FLF**'+b+'**FLF**'+c+'**FLF**';
  document.frmfprdMB.startrowMB.value = Filtros;
  document.frmfprdMB.submit();
}

function newProduct(a,prodId)
{

//  alert(localStorage.getItem("idSessionCompras"));
  if(localStorage.getItem("idSessionCompras") == null)
  {
    var newPreviousProductKey = RandomNum(1000000001, 2147483647);
    localStorage.setItem("idSessionCompras", newPreviousProductKey);
    var idSessionCompras = localStorage.getItem("idSessionCompras");

      var priceSize = document.getElementById('sub_cat_change2'+a).value;
      var priceSizeArray = priceSize.split("/");
      var price = priceSizeArray[0]
      var size  = priceSizeArray[1]+" [cm]";
      var feature_id  = priceSizeArray[2];
      var size_id  = priceSizeArray[3];
      var prodId  = priceSizeArray[4];
      var growerId  = priceSizeArray[5];
      var cartegoriaId  = priceSizeArray[6];
      var bunchNumber  = priceSizeArray[7];

      var steamsT = bunchNumber*(document.getElementById('select_qty'+a).value);
      var boxId = 0;
      var quantity = 1;
      var bestOp = 0;
      var order_id_prev = document.getElementById('ordenPrevId').value;
      var numRecords = document.getElementById('numRecords').value;

      if(numRecords>20){
        numRecords=20;
      }

      var datosCart = "ord_id="+order_id_prev+"&bestOp_id="+bestOp+"&size_id="+size_id+"&feature_id="+feature_id+"&newPreviousProductKey="+newPreviousProductKey+"&product_id="+prodId+"&grower_id="+growerId+"&price_id="+price+"&size="+size+"&box_id="+boxId+"&quantity_id="+quantity+"&steams="+steamsT+"&categoriaId="+cartegoriaId+"&bunchNumber="+bunchNumber;
      //alert(datosCart);
      //return false;

       $.ajax({
            type: "POST",
            url: "save_temp_cartMixBox.php",
            data: datosCart,
            cache: false,
            success: function(r){
            // alert(r);
          //    return false;
              if(r==1){
                document.getElementById('newBoxMixId').value = '';
                var newBoxMix_Id = document.getElementById('newBoxMixId').value;

               countProductPrevious(idSessionCompras,order_id_prev,growerId,newBoxMix_Id);
                loadDataCart(idSessionCompras,growerId,newBoxMix_Id);
                loadBarraP(idSessionCompras,growerId,newBoxMix_Id);
                  for (j = 1; j <= numRecords; j++) {
                    document.getElementById('select_qty'+j).value = "1";
                  }


                var x = document.getElementsByClassName("filter-option-inner");
                for (i = 0; i < x.length; i++) {
                  x[i].innerHTML = '<div class="filter-option-inner-inner">1</div>';
                }


                swal("Good job!", "Product add to cart!", "success");
              }else{
                swal("Sorry!", "Please, try again!", "error");
              }

            }
        });



  }
    else
  		{
  		addNoFirstTime(localStorage.getItem("idSessionCompras"),a,prodId);
  		}

}

function RandomNum(min, max) {
   return Math.round(Math.random() * (max - min) + min);
}

function addNoFirstTime(newPreviousProductKey,a,prodId)
{

  var priceSize = document.getElementById('sub_cat_change2'+a).value;
  var priceSizeArray = priceSize.split("/");
  var price = priceSizeArray[0]
  var size  = priceSizeArray[1]+" [cm]";
  var feature_id  = priceSizeArray[2];
  var size_id  = priceSizeArray[3];
  var prodId  = priceSizeArray[4];
  var growerId  = priceSizeArray[5];
  var cartegoriaId  = priceSizeArray[6];
  var bunchNumber  = priceSizeArray[7];

  var steamsT = bunchNumber*(document.getElementById('select_qty'+a).value);
  var boxId = 0;
  var quantity = 1;
  var bestOp = 0;
  var order_id_prev = document.getElementById('ordenPrevId').value;
  var newBoxMix_Id = document.getElementById('newBoxMixId').value;

  var numRecords = document.getElementById('numRecords').value;

  if(numRecords>20){
    numRecords=20;
  }


  var datosCart = "ord_id="+order_id_prev+"&bestOp_id="+bestOp+"&size_id="+size_id+"&feature_id="+feature_id+"&newPreviousProductKey="+newPreviousProductKey+"&product_id="+prodId+"&grower_id="+growerId+"&price_id="+price+"&size="+size+"&box_id="+boxId+"&quantity_id="+quantity+"&steams="+steamsT+"&NumBoxId="+newBoxMix_Id+"&categoriaId="+cartegoriaId+"&bunchNumber="+bunchNumber;
  //alert(datosCart);
  //return false;



     $.ajax({
          type: "POST",
          url: "save_temp_cart_itemsMixBox.php",
          data: datosCart,
          cache: false,
          success: function(r){

            //  alert(r);
            //  return false;
            if(r==1){
              document.getElementById('newBoxMixId').value =  '';
              var newBoxMix_Id = document.getElementById('newBoxMixId').value;

             countProductPrevious(newPreviousProductKey,order_id_prev,growerId,newBoxMix_Id);
              loadDataCart(newPreviousProductKey,growerId,newBoxMix_Id);
              loadBarraP(newPreviousProductKey,growerId,newBoxMix_Id);
                  for (j = 1; j <= numRecords; j++) {
                    document.getElementById('select_qty'+j).value = "1";
                  }

                var x = document.getElementsByClassName("filter-option-inner");
                for (i = 0; i < x.length; i++) {
                  x[i].innerHTML = '<div class="filter-option-inner-inner">1</div>';
                }

              swal("Good job!", "Product add to cart!", "success");
            }else{
              swal("Sorry!", "Please, try again!", "error");
            }
          }
      });
}


function deleteItemPrevious(itemPrevious,growerId){
  datos = "PreviousProductKey="+itemPrevious;
  var key = localStorage.getItem("idSessionCompras");
  var order_id_prev = document.getElementById('ordenPrevId').value;


    $.ajax({
         type: "POST",
         url: "del_temp_cartMixBox.php",
         data: datos,
         cache: false,
         success: function(r){
           swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will need to select again this product!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    if(r==1){
                      document.getElementById('newBoxMixId').value = "";
                      var newBoxMix_Id = document.getElementById('newBoxMixId').value;
                      countProductPrevious(key,order_id_prev,growerId,newBoxMix_Id);
                      loadDataCart(key,growerId,newBoxMix_Id);
                      loadBarraP(key,growerId,newBoxMix_Id);
                      swal("Ok! Your product has been deleted!", {
                        icon: "success",
                      });
                    }

                  } else {
                    //Nothing to do!
                  }
                });

          // document.getElementById('countProductosPrev').innerHTML = r;
         }
     });

}

function loadBarraP(key,growerId){

  var CateId = document.getElementById('CatId').value;
  CateId = CateId.substring(1);
  var order_id_prev = document.getElementById('ordenPrevId').value;
  var newBoxMix_Id = document.getElementById('newBoxMixId').value;


  var datosLoadPrev = "PreviousProductKey="+key+"&idCategory="+CateId+"&ord_id="+order_id_prev+"&gid_id="+growerId+"&box_nex="+newBoxMix_Id;

  $.ajax({
       type: "POST",
       url: "loadBarraMixBox.php",
       data: datosLoadPrev,
       cache: false,
       success: function(r){
      //  alert(r);
        // return false;
         document.getElementById('barP').innerHTML = r;
       }
   });

}


function loadDataCart(key,growerId,bx){

  var CateId = document.getElementById('CatId').value;
  CateId = CateId.substring(1);
  var order_id_prev = document.getElementById('ordenPrevId').value;
  var newBoxMix_Id = document.getElementById('newBoxMixId').value;


  var datosLoadPrev = "PreviousProductKey="+key+"&idCategory="+CateId+"&ord_id="+order_id_prev+"&gid_id="+growerId+"&box_nex="+newBoxMix_Id;

  $.ajax({
       type: "POST",
       url: "loadDataItemsPreviuosMixBox.php",
       data: datosLoadPrev,
       cache: false,
       success: function(r){
        //alert(r);
        // return false;
         document.getElementById('loadDataItemsPrevious').innerHTML = r;
       }
   });

   $.ajax({
        type: "POST",
        url: "loadDataPricesSummaryMixBox.php",
        data: datosLoadPrev,
        cache: false,
        success: function(r){
         // alert(r);
         // return false;
          document.getElementById('loadDataPricesSummary').innerHTML = r;
        }
    });
}


function requestProduct(cod_cli , orderid, buyerId,id_product,idSyni,idSize,idFeature,idGor) {
    var flag_s = true;

    var box_quantity = document.getElementById('select_options'+idSyni).value;
    var priceProdId = document.getElementById('priceSelected'+idSyni).value;


    var order_val    = orderid;
    var buyer        = buyerId; // codigo  del  buyer
    var subCli       = cod_cli;
    var productId    = id_product;
    var idSize    = idSize;
    var idFeature = idFeature;
    var idGor     = idGor;

if (flag_s == true) {

    $.ajax({
        type: 'post',
        url: '../../buyer/request_product_ajax_subcli_market.php',
        data:   'order_val=' + order_val +
                   '&buyer=' + buyer     +
        '&subCli=' + subCli    +
               '&productId=' + productId +
            '&box_quantity=' + box_quantity +
               '&idFeature=' + idFeature +
                   '&idGor=' + idGor +
                   '&PriceVal=' + priceProdId +
          '&idSize=' + idSize  ,
        success: function (data_s) {

        //  alert(data_s);
        //  return false;
            if (data_s == 'true') {

              swal({
                  title: "Process complete",
                  text: "Your selected product has been added to order!",
                  icon: "success",
                  dangerMode: false,
                  })
                  .then((willDelete) => {
                  if (willDelete) {
                    location.reload();
                  } else {
                    location.reload();
                  }
                  });

            } else {
              swal({
                  title: "ERROR",
                  text: "There is some error. Please try again!",
                  icon: "warning",
                  dangerMode: true,
                  })
                  .then((willDelete) => {
                  if (willDelete) {
                      location.reload();
                  } else {
                    location.reload();
                  }
                  });
            }


        }
    });
}

}



</script>

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_MP" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Header -->
      <div class="modal-header">
        <h5 class="modal-title" id="Modal_MP">

        </h5>
        <button aria-label="Close" class="close" data-dismiss="modal" type="button">
          <span aria-hidden="true" class="fi fi-close fs--18"></span></button>
      </div><!-- Content -->
      <div class="modal-body">
        <img id="img-modal"  width="100%">
      </div>
    </div>
  </div>
</div><!-- /Modal -->
