<?php

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	require '../PHPMailer-master/src/Exception.php';
	require '../PHPMailer-master/src/PHPMailer.php';
	require '../PHPMailer-master/src/SMTP.php';

	if ((isset($_POST["contact_name"]) && $_POST["contact_name"] != "")
	 		&& (isset($_POST["contact_email"]) && $_POST["contact_email"] != "")
			&& (isset($_POST["contact_phone"]) && $_POST["contact_phone"] != "")
			&& (isset($_POST["contact_message"]) && $_POST["contact_message"] != "")      ) {


 $name = $_POST['contact_name'];
 $email = $_POST['contact_email'];
 $phone = $_POST['contact_phone'];
 $message = $_POST['contact_message'];
 $buyerName = "victoria's Blossom";

	//Instantiation and passing `true` enables exceptions
	$mail = new PHPMailer(true);

	try {
	    //Server settings
	    $mail->SMTPDebug = 0;                      //Enable verbose debug output
	    $mail->isSMTP();                                            //Send using SMTP
	    $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
	    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
	    $mail->Username   = 'annie@victoriasblossom.ca';                     //SMTP username
	    $mail->Password   = 'oshiminio$';                               //SMTP password
	    $mail->SMTPSecure = 'tls';         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
	    $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

	    //Recipients
	    $mail->setFrom('annie@victoriasblossom.ca', "Victoria's Blossom");
	    $mail->addAddress($email, $name);     //Add a recipient
	    $mail->addReplyTo('annie@victoriasblossom.ca', 'MarketPlace Victoria Blossom');
	    $mail->addCC('portega@freshlifefloral.com');
	    $mail->addCC('evalencia@freshlifefloral.com');
	    //$mail->addBCC('annie@victoriasblossom.ca');

	    //Attachments
	    //$mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
	    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name



	    //Content
	    $mail->isHTML(true);
			$mail->AddEmbeddedImage('../images/logo.jpeg', 'Freshlifefloral', '../images/logo.jpeg');            //Set email format to HTML
	    $mail->Subject = 'MarketPlace Victorio Blossom';
	    $mail->Body    = buildEmailBody($name,$email,$phone,$buyerName,$message);


			$mail->AddEmbeddedImage('../images/logo.jpeg', 'logoimg', '../images/logo.jpeg'); // attach file logo.jpg, and later link to it using identfier logoimg

	    $mail->send();
	    	die('{:success:}');
	} catch (Exception $e) {
	echo "<div class='alert alert-danger mb-3'>Message could not be sent. {$mail->ErrorInfo}</div>";
	}
}
	/**
	 *
	 *	Creating a template
	 *	Looks ugly in here, but the final email
	 *	is better than plain text!
	 *
	 *	:: More email template layouts in a future update!
	 */
	function buildEmailBody($name,$email,$phone,$buyerName,$message) {


		// HEADER
		$tpl = '
		<html>
			<head>
				<title>Contact Form</title>
			</head>

			<body style="color:#000000;background-color:#e9ecf3;font-family: Helvetica, Arial, sans-serif;line-height:20px;font-size: 16px;font-weight:normal;">
				<div style="background-color:#ffffff;max-width:600px; margin-left:auto; margin-right:auto; margin-top:60px; margin-bottom:60px; box-shadow: 0 1px 15px 1px rgba(113, 106, 202, 0.08); border-radius:4px; -webkit-border-radius:4px; padding:30px;">


					<h3 style="font-size: 23px; margin-bottom:0px;padding-bottom:30px; border-bottom: #cccccc 1px solid;">
						' . $buyerName . '													<br>
						<span style="color:#999999;font-weight:300; font-size:15px;">
							' . date('d F, Y / H:i') . '
						</span>
					</h3> 																					<br>';

		// BODY
		$tpl .= '
					<div style="font-size:18px;font-weight:normal;">
						' . $message . '
					</div>
																											<br>
					<hr style="border:0; border-top:#cccccc 1px solid;">
																											<br>
					<div style="font-size:14px">
						<strong>Name:</strong> 					' . $name . ' 				<br>
						<strong>Email:</strong> 				' . $email . ' 			<br>
						<strong>Phone:</strong> 				' . $phone . ' 			<br>
					</div> <br>

					<img src="cid:logoimg" width="30%">';



		// FOOTER
		$tpl .= '
				</div>
			</body>
		</html>';


		return $tpl;

	}
