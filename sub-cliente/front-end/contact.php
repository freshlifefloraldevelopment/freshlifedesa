<?php

/*

*/
?>
<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>victoria's Blossom</title>
		<meta name="description" content="...">

        <meta name="viewport" content="width=device-width, maximum-scale=5, initial-scale=1, user-scalable=0">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

        <!-- up to 10% speed up for external res -->
        <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
        <link rel="dns-prefetch" href="https://fonts.gstatic.com/">
        <link rel="preconnect" href="https://fonts.googleapis.com/">
        <link rel="preconnect" href="https://fonts.gstatic.com/">
        <!-- preloading icon font is helping to speed up a little bit -->
        <link rel="preload" href="assets/fonts/flaticon/Flaticon.woff2" as="font" type="font/woff2" crossorigin>

        <link rel="stylesheet" href="assets/css/core.min.css">
        <link rel="stylesheet" href="assets/css/vendor_bundle.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap">

		<!-- favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="apple-touch-icon" href="demo.files/logo/icon_512x512.png">

		<link rel="manifest" href="assets/images/manifest/manifest.json">
		<meta name="theme-color" content="#377dff">

	</head>

	<!--
		 +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++
			USED PLUGINS:

				SOW : Countdown timer
				/documentation/plugins-sow-timer-countdown.html

				Vendor : Swiper Slider
				/documentation/plugins-vendor-swiper.html

				Vendor : Typed
				/documentation/plugins-vendor-typed.html

		 +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++
	-->
	<body>

		<div id="wrapper">

<!-- HEADER -->
			<header id="header" class="shadow-xs">






				<!-- NAVBAR -->
				<div class="container position-relative">


					<nav class="navbar navbar-expand-lg navbar-light justify-content-lg-between justify-content-md-inherit">

						<div class="align-items-start">

							<!-- mobile menu button : show -->
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMainNav" aria-controls="navbarMainNav" aria-expanded="false" aria-label="Toggle navigation">
								<svg width="25" viewBox="0 0 20 20">
									<path d="M 19.9876 1.998 L -0.0108 1.998 L -0.0108 -0.0019 L 19.9876 -0.0019 L 19.9876 1.998 Z"></path>
									<path d="M 19.9876 7.9979 L -0.0108 7.9979 L -0.0108 5.9979 L 19.9876 5.9979 L 19.9876 7.9979 Z"></path>
									<path d="M 19.9876 13.9977 L -0.0108 13.9977 L -0.0108 11.9978 L 19.9876 11.9978 L 19.9876 13.9977 Z"></path>
									<path d="M 19.9876 19.9976 L -0.0108 19.9976 L -0.0108 17.9976 L 19.9876 17.9976 L 19.9876 19.9976 Z"></path>
								</svg>
							</button>

							<!--
								Logo : height: 70px max
							-->
							<a class="navbar-brand m-0" href="">
								<img src="https://cdn.shopify.com/s/files/1/0074/3001/2998/files/Victoria_s_Blossom_Imports_HALF_360x.png?v=1545286605" width="" height="" alt="...">
							</a>

						</div>


						<!--

							[SOW] SEARCH SUGGEST PLUGIN
							++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++
							PLEASE READ DOCUMENTATION
							documentation/plugins-sow-search-suggest.html
							++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++ ++

						-->



					</nav>

				</div>
				<!-- /NAVBAR -->




				<div class="clearfix">

					<!-- line -->
					<hr class="m-0 opacity-5">

					<div class="container">

						<nav class="navbar h-auto navbar-expand-lg navbar-light justify-content-lg-between justify-content-md-inherit">


							<!-- Menu -->
							<!--

								Dropdown Classes (should be added to primary .dropdown-menu only, dropdown childs are also affected)
									.dropdown-menu-dark 		- dark dropdown (desktop only, will be white on mobile)
									.dropdown-menu-hover 		- open on hover
									.dropdown-menu-clean 		- no background color on hover
									.dropdown-menu-invert 		- open dropdown in oposite direction (left|right, according to RTL|LTR)
									.dropdown-menu-uppercase 	- uppercase text (font-size is scalled down to 13px)
									.dropdown-click-ignore 		- keep dropdown open on inside click (useful on forms inside dropdown)

									Repositioning long dropdown childs (Example: Pages->Account)
										.dropdown-menu-up-n100 		- open up with top: -100px
										.dropdown-menu-up-n100 		- open up with top: -150px
										.dropdown-menu-up-n180 		- open up with top: -180px
										.dropdown-menu-up-n220 		- open up with top: -220px
										.dropdown-menu-up-n250 		- open up with top: -250px
										.dropdown-menu-up 			- open up without negative class


									Dropdown prefix icon (optional, if enabled in variables.scss)
										.prefix-link-icon .prefix-icon-dot 		- link prefix
										.prefix-link-icon .prefix-icon-line 	- link prefix
										.prefix-link-icon .prefix-icon-ico 		- link prefix
										.prefix-link-icon .prefix-icon-arrow 	- link prefix

									.nav-link.nav-link-caret-hide 	- no dropdown icon indicator on main link
									.nav-item.dropdown-mega 		- required ONLY on fullwidth mega menu

									Mobile animation - add to .navbar-collapse:
									.navbar-animate-fadein
								.navbar-animate-fadeinup
									.navbar-animate-bouncein

							-->
							<div class="collapse navbar-collapse navbar-animate-fadein" id="navbarMainNav">


								<!-- MOBILE MENU NAVBAR -->
								<div class="navbar-xs d-none"><!-- .sticky-top -->

									<!-- mobile menu button : close -->
									<button class="navbar-toggler pt-0" type="button" data-toggle="collapse" data-target="#navbarMainNav" aria-controls="navbarMainNav" aria-expanded="false" aria-label="Toggle navigation">
										<svg width="20" viewBox="0 0 20 20">
											<path d="M 20.7895 0.977 L 19.3752 -0.4364 L 10.081 8.8522 L 0.7869 -0.4364 L -0.6274 0.977 L 8.6668 10.2656 L -0.6274 19.5542 L 0.7869 20.9676 L 10.081 11.679 L 19.3752 20.9676 L 20.7895 19.5542 L 11.4953 10.2656 L 20.7895 0.977 Z"></path>
										</svg>
									</button>

									<!--
										Mobile Menu Logo
										Logo : height: 70px max
									-->
									<a class="navbar-brand" href="login.php">
										<img src="assets/images/logo/logo_dark.svg" width="110" height="70" alt="...">
									</a>

								</div>
								<!-- /MOBILE MENU NAVBAR -->



								<!-- NAVIGATION -->
								<ul class="navbar-nav navbar-sm">
																<!-- Menu -->
								<!--

									Dropdown Classes (should be added to primary .dropdown-menu only, dropdown childs are also affected)
										.dropdown-menu-dark 		- dark dropdown (desktop only, will be white on mobile)
										.dropdown-menu-hover 		- open on hover
										.dropdown-menu-clean 		- no background color on hover
										.dropdown-menu-invert 		- open dropdown in oposite direction (left|right, according to RTL|LTR)
										.dropdown-menu-uppercase 	- uppercase text (font-size is scalled down to 13px)
										.dropdown-click-ignore 		- keep dropdown open on inside click (useful on forms inside dropdown)

										Repositioning long dropdown childs (Example: Pages->Account)
											.dropdown-menu-up-n100 		- open up with top: -100px
											.dropdown-menu-up-n100 		- open up with top: -150px
											.dropdown-menu-up-n180 		- open up with top: -180px
											.dropdown-menu-up-n220 		- open up with top: -220px
											.dropdown-menu-up-n250 		- open up with top: -250px
											.dropdown-menu-up 			- open up without negative class


										Dropdown prefix icon (optional, if enabled in variables.scss)
											.prefix-link-icon .prefix-icon-dot 		- link prefix
											.prefix-link-icon .prefix-icon-line 	- link prefix
											.prefix-link-icon .prefix-icon-ico 		- link prefix
											.prefix-link-icon .prefix-icon-arrow 	- link prefix

										.nav-link.nav-link-caret-hide 	- no dropdown icon indicator on main link
										.nav-item.dropdown-mega 		- required ONLY on fullwidth mega menu

								-->
								<!-- home -->
								<li class="nav-item dropdown active">

									<a href="login.php" id="mainNavHome" class="nav-link"
										>
										Home
									</a>

								</li>


								<!-- pages -->
								<li class="nav-item dropdown">

									<a href="subclient-market-place.php" id="mainNavPages" class="nav-link">
										Market Place
									</a>

								</li>


								<!-- features -->
								<li class="nav-item dropdown">

									<a href="contact.php" id="mainNavFeatures" class="nav-link">
										Contact Us
									</a>

								</li>




								<!-- social : mobile only (d-block d-sm-none)-->
								<li class="nav-item d-block d-sm-none text-center mb-4">

									<h3 class="h6 text-muted">Follow Us</h3>

									<a href="#!" class="btn btn-sm btn-facebook transition-hover-top mb-2 rounded-circle text-white" rel="noopener">
										<i class="fi fi-social-facebook"></i>
									</a>

									<a href="#!" class="btn btn-sm btn-twitter transition-hover-top mb-2 rounded-circle text-white" rel="noopener">
										<i class="fi fi-social-twitter"></i>
									</a>

									<a href="#!" class="btn btn-sm btn-linkedin transition-hover-top mb-2 rounded-circle text-white" rel="noopener">
										<i class="fi fi-social-linkedin"></i>
									</a>

									<a href="#!" class="btn btn-sm btn-youtube transition-hover-top mb-2 rounded-circle text-white" rel="noopener">
										<i class="fi fi-social-youtube"></i>
									</a>

								</li>



								<!-- Get Smarty : mobile only (d-block d-sm-none)-->
								<li class="nav-item d-block d-sm-none">
									<a target="_blank" href="#buy_now" class="btn btn-block btn-primary shadow-none text-white m-0">
										Get Smarty
									</a>
								</li>

								</ul>
								<!-- /NAVIGATION -->


							</div>

						</nav>

					</div>
				</div>

			</header>
			<!-- /HEADER -->

<style type="text/css">
	.custom_select button.btn.dropdown-toggle.select-form-control.border {
    padding-top: 0px;
}
</style>

<!-- PAGE TITLE -->
			<section class="bg-light p-0">
				<div class="container py-5">

					<h1 class="h3">
						Contact Us
					</h1>

					<nav aria-label="breadcrumb">
						<ol class="breadcrumb fs--14">
							<li class="breadcrumb-item"><a href="login.php">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">Contact</li>
						</ol>
					</nav>

				</div>
			</section>
			<!-- /PAGE TITLE -->




			<!-- CONTACT FORM -->
			<section>
				<div class="container">

					<h2 class="font-weight-light mb-5">
						Contact us
					</h2>


					<div class="row">

						<div class="col-12 col-lg-8 mb-4">


							<!--
								CONTACT FORM : AJAX [TESTED|WORKING AS IT IS]

									Plugin required: SOW Ajax Forms

									In order to work as ajax form, SOW Ajax Forms should be available/enabled
									Else, SOW Form Validation plugin is used.
									If none of them are available, normal submit is used and you can remove:
										.js-ajax
										.bs-validate
										novalidate
										any data-ajax-*
										any data-error-*

									** Remove data-error-toast-* for no error toast notifications




								Ajax will control success/fail alerts according to server response:

									1. unexpected error: 		if server response is this string: {:err:unexpected:}
									2. mising mandatory:		if server response is this string: {:err:required:}
									3. success:					if server response is this string: {:success:}

									data-ajax-control-alerts="true"
									data-ajax-control-alert-succes="#contactSuccess"
									data-ajax-control-alert-unexpected="#contactErrorUnexpected"
									data-ajax-control-alert-mandaroty="#contactErrorMandatory"

								+++++++++++++++++++++++++++++++++++++++++++++++++++++++
									WORKING CONTACT! Edit your php/config.inc.php
								+++++++++++++++++++++++++++++++++++++++++++++++++++++++
							-->
							<form 	novalidate
									action="php/contact_form.php"
									method="POST"

									data-ajax-container="#ajax_dd_contact_response_container"
									data-ajax-update-url="false"
									data-ajax-show-loading-icon="true"
									data-ajax-callback-function=""
									data-error-scroll-up="false"

									data-ajax-control-alerts="true"
									data-ajax-control-alert-succes="#contactSuccess"
									data-ajax-control-alert-unexpected="#contactErrorUnexpected"
									data-ajax-control-alert-mandaroty="#contactErrorMandatory"

									data-error-toast-text="<i class='fi fi-circle-spin fi-spin float-start'></i> Please, complete all required fields!"
									data-error-toast-delay="2000"
									data-error-toast-position="bottom-center"

									class="bs-validate js-ajax">


								<!-- 1.
									optional, hidden action for your backend

									PHP Basic Example
									if($_POST['action'] == 'contact_form_submit') {
										... send message
									}
								-->
								<input type="hidden" name="action" value="contact_form_submit" tabindex="-1">
								<!-- -->


								<!-- 2.
									A very small optional trick (using .hide class instead of type="hidden") for some low spam robots.
									If this is not empty, the process should stop. A normal user/visitor should not be able to see this field.

									PHP Basic Example
									if($_POST['norobot'] != '') {
										exit;
									}
								-->
								<input type="text" name="norobot" value="" class="hide" tabindex="-1">
								<!-- -->

								<div class="form-label-group mb-3">
									<input required placeholder="Name" id="contact_name" name="contact_name" type="text" class="form-control">
									<label for="contact_name">Name</label>
								</div>

								<div class="form-label-group mb-3">
									<input required placeholder="Email" id="contact_email" name="contact_email" type="email" class="form-control">
									<label for="contact_email">Email</label>
								</div>

								<div class="form-label-group mb-3">
									<input required placeholder="Phone" id="contact_phone" name="contact_phone" type="text" class="form-control">
									<label for="contact_phone">Phone</label>
								</div>

								<div class="form-label-group mb-4">
									<textarea required placeholder="Message" id="contact_message" name="contact_message" class="form-control" rows="3"></textarea>
									<label for="contact_message">Message</label>
								</div>





								<!--
									Server detailed error
									!ONLY! If debug is enabled!
									Else, shown ony "Server Error!"
								-->
								<div id="ajax_dd_contact_response_container"></div>

								<!-- {:err:unexpected:} internal server error -->
								<div id="contactErrorUnexpected" class="hide alert alert-danger p-3">
									Unexpected Error!
								</div>

								<!-- {:err:required:} mandatory fields -->
								<div id="contactErrorMandatory" class="hide alert alert-danger p-3">
									Please, review your data and try again!
								</div>

								<!-- {:success:} message sent -->
								<div id="contactSuccess" class="hide alert alert-success p-3">
									Thank you for your message!
								</div>




								<button type="submit" class="btn btn-primary btn-block">
									Send Message
								</button>

							</form>
							<!-- /CONTACT FORM : AJAX -->


						</div>


						<div class="col-12 col-lg-4 mb-4">

							<div class="d-flex">

								<div class="w--40">
									<i class="fi fi-shape-abstract-dots text-gray-500 float-start fs--20"></i>
								</div>

								<div>
									<h2 class="fs--25 font-weight-light">
										Victorias Blossom Imports Inc.
									</h2>
									<ul class="list-unstyled m-0 fs--15">
										<li class="list-item text-muted">1420 172 St</li>
										<li class="list-item text-muted">Surrey, BC V3Z 9M6</li>

								</div>

							</div>

					

							<div class="d-flex mt-4">

								<div class="w--40">
									<i class="fi fi-time text-gray-500 float-start fs--20"></i>
								</div>

								<div>
									<h3 class="h4 font-weight-normal">
										Working Hours
									</h3>
									<ul class="list-unstyled m-0 fs--15">
										<li class="list-item text-muted">Monday - Friday: 09:00 to 18:00</li>
										<li class="list-item text-muted">Saturday: 09:00 to 12:00</li>
									</ul>
								</div>

							</div>


							<div class="d-flex mt-4">

								<div class="w--40">
									<i class="fi fi-phone text-gray-500 float-start fs--20"></i>
								</div>

								<div>
									<h3 class="h4 font-weight-normal">
										Phone Number
									</h3>
									<ul class="list-unstyled m-0">
										<li class="list-item mb-2 text-gray-500">
											<a class="link-muted" href="tel:7853889450">778-922-0136</a>
										</li>
										<li class="list-item mb-2 text-gray-500">
											<a class="link-muted" href="tel:3162881850">778-922-0136</a>
										</li>
									</ul>
								</div>

							</div>

						</div>

					</div>

				</div>
			</section>
			<!-- /CONTACT FORM -->







<?php include('inc/footer-2.php'); ?>
