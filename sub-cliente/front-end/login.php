<?php
session_start();


/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 5 Mayo 2021
Project: Client market place
Add Protection SQL INY, XSS
**/
require_once("../../config/config_gcp.php");


if($_SESSION["login-type"]){
    header("Location: subclient-market-place.php");
		exit();
}

include('inc/header-2.php');
?>
<style type="text/css">
	.custom_select button.btn.dropdown-toggle.select-form-control.border {
    padding-top: 0px;
}
</style>
<!-- PAGE TITLE -->
			<section>
				<div class="px-2 max-w-800 mx-auto text-center">

					<h1 class="display-4 h2-xs font-weight-bold">
						Members Login
					</h1>

					<p class="lead m-0">
						Manage your account and data
					</p>

				</div>
			</section>
			<!-- /PAGE TITLE -->




			<!-- FORM -->
			<section class="pt-0">
				<div class="container">


					<div class="row">


						<!-- ALERT -->


						<div class="col-12 col-sm-8 col-md-8 col-lg-6 offset-sm-2 offset-md-2 offset-lg-3">

							<div class="alert alert-mini margin-bottom-30" id="checkLoginErr">
											<!--<strong>Oh snap!</strong> Login Incorrect!-->
							</div><!-- /ALERT -->

							<!-- optional class: .form-control-pill -->
							<form method="post" autocomplete="off" action="#" id="loginFormID" class="bs-validate p-5 p-4-xs rounded shadow-xs">


								<!--
								<p class="text-danger">
									Ups! Please check again
								</p>
								-->


								<div class="form-label-group mb-3">
									<input required placeholder="Email" name="email"  id="email" type="email" class="form-control">
									<label for="account_email">Email</label>
								</div>

								<div class="input-group-over">
									<div class="form-label-group mb-3">
										<input required placeholder="Password" type="password" name="password" type="password" class="form-control">
										<label for="account_password">Password</label>
									</div>

									<a href="account-simple-signin-password.html" class="btn fs--12">
										FORGOT?
									</a>

								</div>



								<div class="row">

									<div class="col-12 col-md-6 mt-4">
										<button type="submit" class="btn btn-primary btn-block font-weight-medium">
											Sign In
										</button>
									</div>

									<input type="hidden" name="usrtype" id="usrtypeID" value="subclient">
									<input type="hidden" name="submit" value="_login">

									<div class="col-12 col-md-6 mt-4 text-align-end text-center-xs">
										<a href="contact.php" class="btn px-0">
											Don't have an account yet?
										</a>
									</div>

								</div>

							</form>

						</div>

					</div>

				</div>
			</section>
			<!-- /FORM -->

<?php include('inc/footer-2.php'); ?>
<script type="text/javascript">


    $(function () {
        $("#loginFormID").submit(function (event) {
            $('#checkLoginErr').removeClass("alert-danger");
            $('#checkLoginErr').removeClass("alert-success");
            $('#checkLoginErr').html('');
            event.preventDefault();
            $.ajax({
                url: '../../../savelogin.php',
                //url: 'savelogin.php',               
                type: 'POST',
                data: $(this).serialize(),
                success: function (result) {
                     if (result == 1) {
                        $('#checkLoginErr').html('Login success!');
                        $('#checkLoginErr').addClass("alert-success");
                        //window.location.href = window.location.href;
                        window.location.href = "subclient-market-place.php";
                    } else {

                        $('#checkLoginErr').addClass("alert-danger");
                        $('#checkLoginErr').html(result);

                    }
                }
            });
        });
    });
</script>
