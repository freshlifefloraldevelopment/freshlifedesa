<?php
session_start();

if(!$_SESSION["subclient"]){
    header("Location: login.php");
		exit();
}

$totalOrder =0;

$clientSessionID = $_SESSION["subclient"];
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 5 Mayo 2021
Project: Client market place
Add Protection SQL INY, XSS
**/
require_once("../../config/config_gcp.php");
include('GlobalFSyn.php');


$codOrder = $_GET['codOrderDetail'];
$query = query_ordersDetail($con,$codOrder);
$sql_orders = mysqli_query($con, $query);
$row_orders = mysqli_fetch_array($sql_orders);

	$Syni = $Syni+1;
	$id = $row_orders["id"];
	$id_order   = $row_orders["order_cli"];
	$id_cliente = $row_orders["client_id"];
	$order_comment = $row_orders["comment1"];
	$order_date = $row_orders["date_add"];

$query1 = query_clientDetail($con,$_SESSION["subclient"]);
$sql_orders1 = mysqli_query($con, $query1);
$row_orders1 = mysqli_fetch_array($sql_orders1);

  $email = $row_orders1["email"];
  $phone1 = $row_orders1["phone1"];
  $BillingAddress = $row_orders1["BillingAddress"];
  $ShippingAddress = $row_orders1["ShippingAddress"];
  $FullName = $row_orders1["FullName"];


include('inc/header-2.php'); ?>
<style type="text/css">
	.custom_select button.btn.dropdown-toggle.select-form-control.border {
    padding-top: 0px;
}
</style>

	<section class="bg-light p-0">
				<div class="container py-5">

					<h1 class="h3">
						My Orders
					</h1>

					<nav aria-label="breadcrumb">
						<ol class="breadcrumb fs--14">
							<li class="breadcrumb-item"><a href="login.php">Home</a></li>
							<li class="breadcrumb-item"><a href="#!">Account</a></li>
							<li class="breadcrumb-item active" aria-current="page">Orders</li>
						</ol>
					</nav>

				</div>
			</section>

<!-- -->
			<section>
				<div class="container">

					<div class="row">

						<div class="col-12 col-sm-12 col-md-12 col-lg-3 mb--60">

							<nav class="sticky-kit nav-deep nav-deep-light js-ajaxified js-stickified" style="">

								<!-- mobile only -->
								<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none js-togglified" data-target="#nav_responsive" data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3">
									<span class="group-icon px-2 py-2 float-start">
										<i class="fi fi-bars-2"></i>
										<i class="fi fi-close"></i>
									</span>

									<span class="h5 py-2 m-0 float-start">
										Account Menu
									</span>
								</button>

								<!-- desktop only -->
								<h5 class="pt-3 pb-3 m-0 d-none d-lg-block">
									Account Menu
								</h5>


								<!-- navigation -->
								<ul id="nav_responsive" class="nav flex-column d-none d-lg-block">

									<li class="nav-item active">
										<a class="nav-link px-0" href="my-orders.php">
											<i class="fi fi-arrow-end m-0 fs--12"></i>
											<span class="px-2 d-inline-block">
												My Orders
											</span>
										</a>
									</li>

								</ul>

							</nav>

						</div>



						<div class="col-12 col-sm-12 col-md-12 col-lg-9">

							<!-- ORDER INFO -->
							<div class="p-4 shadow-xs border rounded mb-4">

								<div class="row">

									<div class="col-12 col-sm-6 col-md-6 col-lg-6">

										<h2 class="fs--18 mb-0">
											Order #<?php echo $id_order;?>
										</h2>

										<p>
											<?php echo $order_date; ?>
										</p>

										<p class="mb-0">
											Status:

											<span class="text-warning">
												Pending / New
											</span>
										</p>

									</div>


									<div class="col-12 col-sm-6 col-md-6 col-lg-6">

										<a href="#" data-href="#?action=cancel&amp;order_id=1009" data-ajax-confirm-mode="regular" data-ajax-confirm-size="modal-md" data-ajax-confirm-centered="false"
										data-ajax-confirm-title="Please Confirm" data-ajax-confirm-body="Are you sure you want to cancel this order?" data-ajax-confirm-btn-yes-class="btn-sm btn-danger" data-ajax-confirm-btn-yes-text="Yes, Cancel"
										data-ajax-confirm-btn-yes-icon="fi fi-check" data-ajax-confirm-btn-no-class="btn-sm btn-light" data-ajax-confirm-btn-no-text="No" data-ajax-confirm-btn-no-icon="fi fi-close" class="js-ajax-confirm float-end float-none-xs m-0 btn btn-sm btn-light fs--14 mb-0 mt-2 js-ajaxconfirmified">

											 ORDER <?php echo $id_order;?>
											<span class="text-muted fs--12 d-block">
												<?php echo $order_date; ?>
											</span>

										</a>

									</div>

								</div>

							</div>
							<!-- /ORDER INFO -->



							<!-- ORDER PERSONAL DETAIL -->
							<div class="p-4 shadow-xs rounded mb-4">

								<div class="row row-grid b-0">

									<div class="col-12 col-sm-12 col-md-4 col-lg-4">

										<div class="px-3 pb-3">

											<h6 class="mt-2 text-primary">
												ORDER CONTACT
											</h6>

											<!-- not needed/required -->
											<!--
											<div class="mt-3">
												<span class="d-block text-muted">Name:</span>
												John Doe
											</div>
											-->

											<div class="mt-3">
												<label class="text-dark d-block m-0">Email:</label>
												<a class="text-muted" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
											</div>

											<div class="mt-3">
												<label class="text-dark d-block m-0">Phone:</label>
												<span class="text-muted"><?php echo $phone1; ?></span>
											</div>

											<div class="mt-3">
												<span class="d-block text-dark">Payment Method:</span>

												<a class="text-decoration-none" href="#order_payment_info" data-toggle="collapse">
													<span class="group-icon fs--14">
														<i class="fi fi-arrow-down"></i>
														<i class="fi fi-close"></i>
													</span>

													<span class="d-inline-block pl-2 pr-2">
														Bank Deposit
													</span>
												</a>

												<!-- data from checkout -->
												<p class="bg-light p-2 rounded mt-2 collapse" id="order_payment_info">
													<span class="d-block text-muted">Payment Info:</span>
													<b>Bank Name</b>: ACME Bank<br>
													Bank Branch: New York<br>
													Account Name: John Smith<br>
													Account Number: XXXXXXXXXXXX
												</p>
											</div>

										</div>

									</div>

									<div class="col-12 col-sm-6 col-md-4 col-lg-4">

										<div class="px-3 pb-3">

											<h6 class="mb-4 mt-2 text-primary">
												SHIPPING <span class="font-weight-normal">ADDRESS</span>
											</h6>

											<?php echo $BillingAddress; ?>

										</div>

									</div>


									<div class="col-12 col-sm-6 col-md-4 col-lg-4 br-0">

										<div class="px-3 pb-3">

											<h6 class="mb-4 mt-2 text-primary">
												BILLING <span class="font-weight-normal">ADDRESS</span>
											</h6>

											<div class="bg-light p-2 rounded">

												<i class="fi mdi-check text-success"></i>
												<span class="d-inline-block pl-2 pr-2">
													Same as shipping
												</span>

											</div>

										</div>

									</div>


								</div>

								<div class="text-muted mt-4">
									<span class="font-weight-medium">Customer Note:</span>

									Processing order

								</div>
								<!-- customer detail -->

							</div>
							<!-- /ORDER PERSONAL DETAIL -->


							<?php
              //echo getOrdersTotal($id_order,$_SESSION["subclient"],$_SESSION["buyer"]);
							$queryOrders = @mysqli_query($con,getOrdersTotal($id_order,$_SESSION["subclient"],$_SESSION["buyer"]));
							while($row_products = mysqli_fetch_array($queryOrders))
							{
								$Syni = $Syni+1;

								$id_product = $row_products["product"];
								$price = $row_products["price"];
								$stems = $row_products["noofstems"];
                $sizeid = $row_products["sizeid"];
                $feature = $row_products["feature"];
                $qty = $row_products["qty"];

								$getURLProduct = mysqli_query($con,"select image_path, name, subcategoryid from product where id ='$id_product'");
								$getURLArray = mysqli_fetch_array($getURLProduct);
								$imageURL = $getURLArray['image_path'];
								$imageNAME = $getURLArray['name'];
								$imageSUBCat = $getURLArray['subcategoryid'];


								$getNAMESubcategory = mysqli_query($con,"select id, name, cat_id from subcategory where id ='$imageSUBCat'");
								$getNAMESubCatArray = mysqli_fetch_array($getNAMESubcategory);
								$SubCatId = $getNAMESubCatArray['id'];
								$SubCatName = $getNAMESubCatArray['name'];
								$CatId = $getNAMESubCatArray['cat_id'];

								$getNAMECategory = mysqli_query($con,"select id, name from category where id ='$CatId'");
								$getNAMECatArray = mysqli_fetch_array($getNAMECategory);
								$CatId = $getNAMECatArray['id'];
								$CatName = $getNAMECatArray['name'];

                $getNAMESize = mysqli_query($con,"select name from sizes where id='$sizeid'");
                $getNAMESizeArray = mysqli_fetch_array($getNAMESize);
                $SizeName = $getNAMESizeArray['name'];

							 ?>

							<!-- ITEMS -->
							<div class="p-4 shadow-xs rounded mb-4">


								<!-- item -->
								<div class="row">

									<div class="col-3 col-sm-2 col-md-2 col-lg-2 text-center">

										<a class="d-block clearfix" href="#!">
											<img class="img-fluid" src="https://app.freshlifefloral.com/<?php echo $imageURL; ?>" alt="<?php echo $imageNAME;?>">
										</a>

									</div>

									<div class="col">

										<div class="row">
											<div class="col">

												<a class="d-block clearfix" href="#!">
													<?php echo $SubCatName." ". $imageNAME." ".$SizeName." [cm]"; ?> 10 st/bu
												</a>

												<span class="d-block text-muted fs--13"><?php echo $CatName; ?></span>

												<span class="font-weight-medium d-block mt--15">

													Price per bunch $<?php echo $price; ?> <br>
                          Qty bunch <?php echo $qty; ?>
												</span>

											</div>

											<div class="col-12 col-md-4 text-align-end text-start-xs">
												<span class="text-success fs--12 mt--3 d-block text-uppercase">
													<?php echo $stems;?> STEMS
												</span>

												$<?php echo $stems*$price;
												 $totalOrder = $totalOrder + $stems*$price; ?>

											<!--	<del class="text-muted d-block fs--14">
													$<?php //echo ($stems*$price)+(($stems*$price)*60/100); ?>
												</del>-->
											</div>

										</div>

									</div>
								</div>
								<!-- /item -->
								<hr>
							</div>
								<?php
							}
								?>


								<div class="row">
									<div class="offset-sm-6 offset-md-6 offset-lg-7 col-12 col-sm-6 col-md-6 col-lg-5">

										<div class="clearfix mb--15">
											Subtotal:
											<span class="float-end text-align-end">
												$<?php echo $totalOrder; ?>
												<span class="d-block text-muted fs--12">
													IMP 5%
												</span>
											</span>
										</div>

										<hr>

										<div class="clearfix">
											<h5 class="float-start">
												Total:
											</h5>
											<h5 class="float-end">
												$<?php echo round($totalOrder+(($totalOrder*5)/100),2)?>
											</h5>
										</div>

									</div>
								</div>


								<hr>

<!--
								<div class="text-success text-align-end text-center-xs px-3">

									Congratulations John, you saved: <b>$697.00</b>

									show percent on saved more than 10%
									<span class="font-light">(20%)</span>

								</div>

-->

							<!-- /ITEMS -->



							<!-- ORDER OPTIONS -->
							<div class="my-5 d-none d-sm-block"><!-- desktop only -->

								<div class="clearfix text-align-center-xs">

									<h6>
										Order Options
									</h6>

									<a class="btn btn-sm btn-light" href="javascript:window.print()">
										<i class="fi fi-print m-0"></i>
									</a>

									<a class="btn btn-sm btn-light" href="javascript:window.print()">
										<span class="fs--13 m-0">PDF : INVOICE</span>
									</a>

								</div>

							</div>
							<!-- /ORDER OPTIONS -->


						</div>

					</div>

				</div>
			</section>
			<!-- / -->

<?php include('inc/footer-2.php'); ?>
