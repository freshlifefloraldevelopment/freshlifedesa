<?php
/**
Developer educristo@gmail.com
Start 15 Sep 2020
Analyze Frond-end, Bac-End
Add Protection SQL INY, XSS
**/
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
		<!-- up to 10% speed up for external res -->
		<link href="https://fonts.googleapis.com/" rel="dns-prefetch">
		<link href="https://fonts.gstatic.com/" rel="dns-prefetch">
		<link href="https://fonts.googleapis.com/" rel="preconnect">
		<link href="https://fonts.gstatic.com/" rel="preconnect"><!-- preloading icon font is helping to speed up a little bit -->
		<link href="/new.assets/fonts/flaticon/Flaticon.woff2" rel="preload" type="font/woff2"><!-- non block rendering : page speed : js = polyfill for old browsers missing `preload` -->
		<link href="/new.assets/css/core.min.css" rel="stylesheet">
		<link href="/new.assets/css/vendor_bundle.min.css" rel="stylesheet">
		<link href="/new.assets/css/style.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap" rel="stylesheet"><!-- favicon -->
		<link href="favicon.ico" rel="shortcut icon">
		<link href="/demo.files/logo/icon_512x512.png" rel="apple-touch-icon">
		<link href="/new.assets/images/manifest/manifest.json" rel="manifest">



    <!-- THEME CSS -->

    <script type="text/javascript" src="<?php echo SITE_URL; ?>includes/assets/plugins/jquery/jquery-2.1.4.min.js"></script>
<title>Fresh Life Floral</title>
</head>
<body>


<!-- MIDDLE -->
<div id="wrapper_content" class="d-flex flex-fill">
				<div id="middle" class="flex-fill">




	<!--
						PAGE TITLE
					-->
					<div class="page-title bg-transparent b-0">




	<div class="d-none d-sm-block">
						<div class="clearfix d-flex justify-content-between">
							<!-- Logo : height: 60px max -->
							<a class="align-self-center navbar-brand p-3" href="index.html"><img alt="Fresh Life Floral" src="https://app.freshlifefloral.com/images/logo.jpeg" width="10%" style="margin-bottom: 30px;"></a>
						</div>
					</div><!-- /LOGO -->

					</div>






					<!-- WIDGETS -->
					<div>
						<!-- WIDGET : TASKS -->
						<div class="col-12">
							<div class="portlet">

								<div class="portlet-header">

										We ship daily a wide range of fresh flowers to wholesalers and importers across the USA, Australia, South Africa, Russia and many other parts of the world. <br>We give our customers service and tailor made platforms, in order to boost mutual sales. <br>Contact us today and find out what we can do for you.

								</div>

								<div  class="portlet-body max-h-500">
<div class="border-bottom border-light">
										<div class="clearfix mb-2 py-1">
											<span class="float-start mt-1">
												<a href="#!" class="text-dark">Account Manager</a>
											</span>
										</div>
										<address>
<ul>
<li>Av. Interoceanica OE6-73 y Gonzalez Suarez<br> Quito, Ecuador</li>
<li>Phone: +593 602 2630</li>
<li><a href="mailto:info@freshlifefloral.com">info@freshlifefloral.com</a></li>
</ul>
</address>

									</div>


								</div>

							</div>

						</div>
						<!-- /WIDGET : TASKS -->

					</div></div>
</body>
</html>
