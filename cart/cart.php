<?php
require_once("../config/config_new.php");
$page_request = "dashboard";
$buyer_id = $_SESSION['buyer'];
$is_login = $_SESSION['login'];
$user_type = $_SESSION['login-type'];
/*if ($is_login != 1 || $user_type != 'buyer') {
    header("Location:http://localhost/freshlifefloral-staging/");
    //header("Location:http://development.freshlifefloral.com"); // Uncomment this when completed
}
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,company_role,country,
                            state_text,city,zip,shipping_country,shipping_state_text,shipping_city,shipping_zip,coordination,address,
                            address2,shipping_address,shipping_address2,name_on_card,credit_card_type,credit_card,cvv,expiration_month,expiration_year,
                            is_public,biographical_info,profile_image,display_shipping,display_handling,display_tax
                            FROM buyers WHERE id =$buyer_id")
) {
    //$userSessionID
    $stmt->bind_param('i', $_SESSION['buyer']);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $company_role, $country,
        $state_text, $city, $zip, $shipping_country, $shipping_state_text, $shipping_city, $shipping_zip, $coordination, $address,
        $address2, $shipping_address, $shipping_address2, $name_on_card, $credit_card_type, $credit_card, $cvv, $expiration_month, $expiration_year,
        $is_public, $biographical_info, $profile_image, $display_shipping, $display_handling, $display_tax);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {

        header("location:" . SITE_URL);
        die;
    }
}
$img_url = SITE_URL . '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = SITE_URL . '/images/profile_images/' . $profile_image;
}

?>
<?php include("../includes/profile-header.php"); ?>
<?php include("../includes/left_sidebar_buyer.php"); ?>
<!--<link href="../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css">
<!--<link href="https://select2.github.io/dist/css/select2.min.css" type="text/css" rel="stylesheet"/>-->


<!--ahurita-->
<!--<link href="<?php echo SITE_URL; ?>../includes/assets/css/layout-shop.css" type="text/css" rel="stylesheet"/>
-->


<!--<link href="<?php //echo SITE_URL; ?>../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>-->
<!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Raleway:300,400,500,600,700|Lato:300,400,400italic,600,700|
Raleway:300,400,500,600,700" rel="stylesheet" type="text/css"/>-->
<!-- WRAPPER -->
<div id="wrapper">
    <!--MIDDLE-->
    <section id="middle">
        <!-- page title -->
        <header id="page-header">
            <h1>Bootstrap Tables</h1>
            <ol class="breadcrumb">
                <li><a href="#">Tables</a></li>
                <li class="active">Bootstrap Tables</li>
            </ol>
        </header>
        <!-- /page title -->
        <div id="content" class="padding-20">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <!-- LEFT -->
                        <div class="col-md-12">
                            <div id="content" class="padding-20">
                                <div id="panel-2" class="panel panel-default">
                                    <div class="panel-heading">
                                        <span class="title elipsis"><strong>Shopping Cart</strong> <!-- panel title --></span>
                                    </div>
                                </div>
                                <!--My table  Jose Portilla-->
                                <!--/My Table-->
                                <div class="container">
                                    <div class="row">
                                        <!--<div class="col-md-12 for_1280_left">-->
                                        <!-- EMPTY CART -->
                                        <?php
                                        date_default_timezone_set("America/Guayaquil"); // Ecuador Time Zone
                                        //date_default_timezone_set("Asia/Karachi"); // Pakistan Time Zone
                                        //echo date("F j, Y, H:i:s"); echo '<br>';
                                        echo date('Y-m-d 13:00:00');
                                        if (date('H:i:s') > '13:00:00') {
                                            $remTime = strtotime(date('Y-m-d 13:00:00')) - strtotime(date('Y-m-d H:i:s'));
                                            // echo  $zx;
                                            //$remTime = strtotime ( '+1 day' , strtotime ( $zx ) ) ;
                                        } else {
                                            $remTime = strtotime(date('Y-m-d 13:00:00')) - strtotime(date('Y-m-d H:i:s'));
                                        }
                                        echo date('Y-m-d H:i:s');
                                        ?>
                                        <link href="css/jquery.classycountdown.css" rel="stylesheet" type="text/css">
                                        <div class="col-sm-12 col-xs-12">
                                            <strong>Check out Soon!</strong>Before you drop your orders<br>
                                        </div>
                                        <!--<div class="panel panel-default w80 custom_styl">
                                            <div class="panel-body">
                                                <strong>Check out Soon!</strong><br>
                                                You have <span class="countdown countdown-inline" data-from="November 7, 2016, 13:00:00"></span>  minutes to check out your order<br>
                                                Click <a href="index.html">here</a> to continue shopping. <br>
                                                <span class="label label-warning">checkout before you drop your orders</span>
                                            </div>
                                        </div>
                                        <!-- /EMPTY CART -->
                                        <!--</div>-->
                                        <!--<div class="col-md-12 for_1280 padding-10">-->
                                        <!--<div class="countdown bordered" data-from="<?php //echo date("F j, Y, 13:00:00"); ?>"></div>-->

                                        <!--</div>-->
                                        <!-- LEFT -->
                                        <div class="col-md-9 col-sm-8">

                                            <div id="countdown17" class="ClassyCountdownDemo"></div>
                                            <!--Jose  Portilla-->
                                            <div class="modal-body">
                                                <div class="table-responsive">
                                                    <?php
                                                    //print_r($_SESSION['cart_products']);
                                                    ?>
                                                    <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th align="center">
                                                                <small>GROWER</small>
                                                            </th>
                                                            <th>
                                                                <small># OFFER</small>
                                                            </th>
                                                            <th>
                                                                <small>BOXES</small>
                                                            </th>
                                                            <th>
                                                                <small>PRODUCT-SIZE</small>
                                                            </th>
                                                            <!--<th>Size</th>-->
                                                            <th>
                                                                <small>BUNCHES</small>
                                                            </th>

                                                            <th>
                                                                <small>TOTAL ST/BU</small>
                                                            </th>
                                                            <th>
                                                                <small>PRICE UNIT</small>
                                                            </th>
                                                            <?php
                                                            if ($display_shipping == 0) { ?>
                                                                <th>Shipping Unit</th>
                                                            <? } ?>
                                                            <?php
                                                            if ($display_handling == 0) { ?>
                                                                <th>Handling Unit</th>
                                                            <? } ?>
                                                            <?php
                                                            if ($display_tax == 0) { ?>
                                                                <th>Tax Unit</th>
                                                            <? } ?>
                                                            <th>Final Price</th>
                                                            <th>
                                                                <small>TOTAL</small>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                        ?>
                                                        <form action="" method="post" id="add_to_cart_form"
                                                              class="add_to_cart_form">
                                                            <?php

                                                            //left join country cr on g.country_id=cr.id
                                                            //cr.name as countryname, cr.flag
                                                            $al_ready_gro_in = array();
                                                            $sel_check = "select gr.*,gr.id as grid,g.growers_name,g.file_path5 from grower_offer_reply gr 
                                                                              left join growers g on gr.grower_id=g.id 
                                                                              where gr.status='1'  and  gr.buyer_id='" . $userID . "'
                                                                              order  by  gr.grower_id,gr.offer_id_index";
                                                            $rs_growers = mysqli_query($con, $sel_check);
                                                            $sub_total = 0;

                                                            while ($growers = mysqli_fetch_assoc($rs_growers)) {
                                                                ?>
                                                                <?php if ($growers["shipping_method"] > 0) {
                                                                    $total_shipping = 0;
                                                                    $sel_connections = "select type,shipping_rate,addtional_rate,box_price,box_weight_arranged,
                                                                                        price_per_box
                                                                                      from connections where shipping_id='" . $growers["shipping_method"] . "'";
                                                                    $rs_connections = mysqli_query($con, $sel_connections);

                                                                }
                                                                $k = explode("/", $growers["file_path5"]);
                                                                if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                    echo "<tr class=\"set_border\">";
                                                                } else {
                                                                    echo "<tr class=\"not_set_border\">";
                                                                }
                                                                ?>
                                                                <!--IMG-->
                                                                <td>
                                                                    <?php
                                                                    if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                        ?>
                                                                        <img
                                                                        src="<?php echo SITE_URL; ?>/user/logo/<?php echo $k[1]; ?>"
                                                                        width="60"><?php //echo $growers["growers_name"]; ?>
                                                                    <?php }
                                                                    ?>
                                                                </td>
                                                                <!--OFFER ID-->
                                                                <td> <?php
                                                                    if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                        echo "Offert " . $growers['offer_id_index'];
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <!--Boxes offered-->
                                                                <td><?php
                                                                    if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                        $getGrowerReply = "SELECT offer_id,bunchqty,boxqty FROM grower_offer_reply where offer_id='" . $growers['offer_id'] . "'and 
                                                                                grower_id='" . $growers['grower_id'] . "' and  offer_id_index='" . $growers["offer_id_index"] . "' limit 1";
                                                                        $rs_getGrowerReply = mysqli_query($con, $getGrowerReply);
                                                                        $tot_bunch_qty = 0;
                                                                        while ($row_rs_getGrowerReply = mysqli_fetch_array($rs_getGrowerReply)) {
                                                                            $tot_bunch_qty = $tot_bunch_qty + $row_rs_getGrowerReply['boxqty'];
                                                                        }
                                                                        echo $tot_bunch_qty;
                                                                        if ($producs["boxtype"] != "") {
                                                                            $temp = explode("-", $producs["boxtype"]);
                                                                            $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                                                                            $rs_box_type = mysqli_query($con, $sel_box_type);
                                                                            $box_type = mysqli_fetch_array($rs_box_type);
                                                                            $type_box = box_name($box_type["name"]);
                                                                            echo $type_box;
                                                                        }
                                                                        //echo $getGrowerReply;
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <!--Product-->
                                                                <td>
                                                                    <small><?= $growers["product"]; ?><?= $growers["size"] . "cm"; ?></small>
                                                                </td>
                                                                <!--Bunches-->
                                                                <td>
                                                                    <small><?= $growers["bunchqty"] . " Bu * " ?><?= $growers["steams"] . "st/bu"; ?> </small>
                                                                </td>
                                                                <!--$bunch_size_s-->
                                                                <td>
                                                                    <small><?= $growers["total_stems"]; ?></small>
                                                                </td>
                                                                <!--Price-->
                                                                <td>$<?= $growers['price']; ?></td>
                                                                <!--Confirm/Status-->
                                                                <?php
                                                                $shipp = $shipp + $growers["total_stems"] * $growers['shipping'];
                                                                ?>
                                                                <?php
                                                                if ($display_shipping == 0) {
                                                                    ?>
                                                                    <td>
                                                                        <?php $ship_cost = $growers['shipping'];
                                                                        $ship_aux = 0;
                                                                        echo $ship_cost;
                                                                        $total_kilo_charges = $invoice['boxqty'] * $invoice['box_weight'] * $charges_per_kilo;
                                                                        ?>
                                                                    </td>
                                                                    <?php
                                                                } else {
                                                                    //$total_kilo_charges = 0;
                                                                    $ship_cost = 0;
                                                                    $ship_aux = $growers['shipping'];

                                                                }
                                                                ?>
                                                                <!------->
                                                                <?php
                                                                $handling_fee = 0;
                                                                if ($display_handling == 0) {
                                                                    $handling_fee = $growers['handling_fees'];
                                                                    ?>
                                                                    <td>
                                                                        <?php
                                                                        $han_aux = 0;
                                                                        $han = $growers['handling'];
                                                                        echo $han;
                                                                        ?>
                                                                    </td>


                                                                    <?php
                                                                } else {

                                                                    $han_aux = $growers['handling'];
                                                                    $han = 0;

                                                                }
                                                                ?>

                                                                <?php
                                                                $tax = 0;
                                                                if ($display_tax == 0) {
                                                                    $tax = $growers['tax'];
                                                                    ?>
                                                                    <td>
                                                                        <?php
                                                                        $tax_aux = 0;
                                                                        $tax = $growers['tax'];
                                                                        echo $tax;
                                                                        ?>
                                                                    </td>
                                                                    <?php
                                                                } else {

                                                                    $tax_aux = $growers['tax'];
                                                                    $tax = 0;
                                                                }
                                                                ?>


                                                                <!--final  price-->
                                                                <td>
                                                                    $<?php $final_price = $growers['price'] + $ship_cost + $tax + $han;
                                                                    echo number_format($final_price, 2); ?>
                                                                </td>
                                                                <td>$<?php


                                                                    $sub_total += $final_price * $growers["total_stems"];

                                                                    echo $final_price * $growers["total_stems"];

                                                                    //echo $growers["total_price_st_bu"] +($ship_cost+$tax+$han*$growers["total_stems"])  ;
                                                                    //$sub_total += $growers["total_price_st_bu"];

                                                                    ?>
                                                                </td>
                                                                <?php
                                                                $total_ship = ($growers["total_stems"] * $ship_aux);
                                                                $total_han = ($growers["total_stems"] * $han_aux);
                                                                $total_tax = ($growers["total_stems"] * $tax_aux);
                                                                $grand_total_ship = $grand_total_ship + $total_ship;
                                                                $grand_total_han += $total_han;
                                                                $grand_total_tax += +$total_tax;
                                                                ?>

                                                                <?php array_push($al_ready_gro_in, $growers['grower_id'] . "-" . $growers["offer_id_index"]);
                                                                $cn++;
                                                            }
                                                            ?>

                                                        </form>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!--/Jose Portilla-->
                                            <!-- CART -->
                                            <!-- /CART -->
                                        </div>
                                        <!-- RIGHT -->
                                        <div class="col-md-3 col-sm-4">
                                            <!-- TOGGLE -->
                                            <div class="toggle-transparent toggle-bordered-full clearfix">
                                                <div class="toggle nomargin-top">
                                                    <label style="background-color:  #212f3c;color: white     ">Voucher</label>
                                                    <div class="toggle-content">
                                                        <p>Enter your discount coupon code.</p>
                                                        <form action="" method="post" class="nomargin"
                                                              id="voucher-form">
                                                            <input type="text" id="cart-code" name="voucher-code"
                                                                   class="form-control text-center margin-bottom-10"
                                                                   placeholder="Voucher Code" required="required">
                                                            <button class="btn btn-primary btn-block" type="submit"
                                                                    name="submit-voucher">SUBMIT
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /TOGGLE -->
                                            <div class="toggle-transparent toggle-bordered-full clearfix">
                                                <div class="toggle active">
                                                    <div class="toggle-content" style="display: block;">
                                                        <span class="clearfix">
											                <span class="pull-right">$<span
                                                                        id="sub-total"><?php echo number_format($sub_total, 2); ?></span></span>
											                <strong class="pull-left">Subtotal:</strong>
										                </span>

                                                        <?php if ($display_shipping == 1) { ?>
                                                            <span class="clearfix">
                                                            <span class="pull-right">$<span
                                                                        id="shipping-amount"> <?php echo number_format($grand_total_ship, 2) ?></span></span>
											                <span class="pull-left">Shipping:</span>
                                                        </span>
                                                        <? } ?>
                                                        <?php if ($display_handling == 1) { ?>
                                                            <span class="clearfix">
                                                            <span class="pull-right">$<span
                                                                        id="shipping-amount"> <?php echo number_format($grand_total_han, 2) ?></span></span>
											                <span class="pull-left">Handling:</span>
                                                        </span>
                                                        <? } ?>
                                                        <?php if ($display_tax == 1) { ?>
                                                            <span class="clearfix">
                                                            <span class="pull-right">$<span
                                                                        id="shipping-amount"> <?php echo number_format($grand_total_tax, 2) ?></span></span>
											                <span class="pull-left">Tax:</span>
                                                        </span>
                                                        <? } ?>
                                                        <span class="clearfix">
                                                            <span class="pull-right">$<span
                                                                        id="voucher-discount">0.00</span></span>
											                <span class="pull-left">Discount:</span>
										                </span>
                                                        <hr>
                                                        <span class="clearfix" style="background-color: bisque">
											                    <span class="pull-right size-20">$<span
                                                                            id="grand-total-amount"><?php
                                                                        $grand_total = $sub_total + $grand_total_ship + $grand_total_han + $grand_total_tax;
                                                                        echo number_format($grand_total, 2); ?></span></span>
                                                                <strong class="pull-left">TOTAL:</strong>
										                </span>
                                                        <hr>
                                                        <input type="text" id="cart-code" name="checkout-code"
                                                               class="form-control text-center margin-bottom-10"
                                                               placeholder="Checkout Code">
                                                        <input type="hidden" name="offers"
                                                               value="<?php echo $offers_id; ?>"/>
                                                        <input type="hidden" name="grids"
                                                               value="<?php echo $grids_id; ?>"/>
                                                        <input type="hidden" name="sub_totals"
                                                               value="<?php echo $sub_total_amount; ?>"/>
                                                        <input type="hidden" name="is_bunch"
                                                               value="<?php echo $is_bunch_id; ?>"/>
                                                        <input type="hidden" name="bunch_stem_qty_value"
                                                               value="<?php echo $bunch_stem_qty_value_id; ?>"/>
                                                        <button class="btn btn-primary btn-block"
                                                                onclick="add_to_cart(<?= $userID ?>);"><i
                                                                    class="fa fa-mail-forward"></i> Proceed to Checkout
                                                        </button>
                                                        <form action="" method="post" class="nomargin"
                                                              id="checkout-forma">

                                                            <!--required="required"-->
                                                            <!--<button class="btn btn-primary btn-block" type="submit" name="submit-checkout"><i class="fa fa-mail-forward"></i> Proceed to Checkout</button>-->
                                                            <!--type="submit" name="submit-checkout"-->
                                                            <!--<input type="submit" name="btn_add_to_cart" class="btn btn-success   btn-block"   value="Proceed to Checkout"
                                                                   onclick="add_to_cart(<?= $producs['cartid'] ?>,<?= $userSessionID ?>,<?= $growers['grower_id']; ?>,<?= $growers["offer_id_index"]; ?>);"/>-->


                                                        </form>
                                                        <!--<a href="" class="btn btn-primary btn-lg btn-block size-15"><i class="fa fa-mail-forward"></i> Proceed to Checkout</a>-->
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /CART -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Modal Table-->
                </div>
            </div>
        </div>
    </section>
</div>
<!--</section>-->
<!-- /MIDDLE -->
<!--</div>-->
<!-- PRELOADER -->
<div id="preloader">
    <div class="inner">
        <span class="loader"></span>
    </div>
</div><!-- /PRELOADER -->
<!-- JAVASCRIPT FILES -->
<script src="/cart/js/jquery.knob.js"></script>
<script src="/cart/js/jquery.throttle.js"></script>
<script src="/cart/js/jquery.classycountdown.js"></script>
<script type="text/javascript"> var plugin_path = '/includes/assets/plugins/';</script>
<!-- PAGE LEVEL SCRIPTS -->
<script type="text/javascript">
    jQuery(window).ready(function () {

        loadScript(plugin_path + 'jquery/jquery-ui.min.js', function () {
            /** jQuery UI **/
            loadScript(plugin_path + 'jquery/jquery.ui.touch-punch.min.js', function () {
                /** Mobile Touch Slider **/
                loadScript(plugin_path + 'form.slidebar/jquery-ui-slider-pips.min.js', function () { /** Slider Script **/

                    /** Slider 5
                     ******************** **/
                    jQuery("#slider5").slider({
                        value: 0.01,
                        animate: true,
                        min: 0,
                        max: 2,
                        step: 0.01,
                        range: "min",
                        slide: function (event, ui) {
                            jQuery("#donation").val(ui.value);
                        }
                    });

                    jQuery("#donation").val(jQuery("#slider5").slider("value"));
                    jQuery("#donation").blur(function () {
                        jQuery("#slider5").slider("value", jQuery(this).val());
                    });


                    $sliderv1.slider("pips", {
                        first: "pip",
                        last: "pip"
                    });
                });
            });
        });
    });


    // process the checkout form
    $('#checkout-form').submit(function (event) {
        var remTime = "<?= $remTime; ?>";
        if (remTime <= 0) {
            alert('Your order has been expired. You can not checkout now.');
            return false;
        }
        else {

            // get the form data
            // there are many ways to get this data using jQuery (you can use the class or id also)
            var formData = {
                'c-code': $('input[name=checkout-code]').val(),
                'submit-checkout': 'Submit'
            };

            // process the checkout form
            $.ajax({
                type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url: 'process_cart.php', // the url where we want to POST
                data: formData, // our data object
                dataType: 'json', // what type of data do we expect back from the server
                encode: true
            })
            // using the done promise callback
                .done(function (data) {
                    if (data['res'] == 1) {
                        var v_code = $('input[name=voucher-code]').val();
                        var c_code = $('input[name=checkout-code]').val();
                        var offers = $('input[name=offers]').val();
                        var grids = $('input[name=grids]').val();
                        var sub_totals = $('input[name=sub_totals]').val();
                        var is_bunch = $('input[name=is_bunch]').val();
                        var bunch_stem_qty_value = $('input[name=bunch_stem_qty_value]').val();
                        var checkoutData = {
                            'v-code': v_code,
                            'c-code': c_code,
                            'offers': offers,
                            'grids': grids,
                            'sub_totals': sub_totals,
                            'is_bunch': is_bunch,
                            'bunch_stem_qty_value': bunch_stem_qty_value,
                            'checkout-form-submit': 'Submit'
                        };
                        $.ajax({
                            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                            url: '/cart/process_cart.php', // the url where we want to POST
                            data: checkoutData, // our data object
                            dataType: 'json', // what type of data do we expect back from the server
                            encode: true
                        })
                        // using the done promise callback
                            .done(function (data) {

                            });
                    }
                    else {
                        alert('You have entered an invalid checkout code.');
                    }
                    // log data to the console so we can see
                    console.log(data);

                    // here we will handle errors and validation messages
                });

            // stop the form from submitting the normal way and refreshing the page
            event.preventDefault();
        }
    });


    /*******    process the voucher form ***********************/

    $('#voucher-form').submit(function (event) {

        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var formData = {
            'v-code': $('input[name=voucher-code]').val(),
            'submit-voucher': 'Submit'
        };

        // process the form
        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: 'process_cart.php', // the url where we want to POST
            data: formData, // our data object
            dataType: 'json', // what type of data do we expect back from the server
            encode: true
        })
        // using the done promise callback
            .done(function (data) {
                if (data['res'] == 1) {
                    var discount = data['v_amount'];
                    var sub_total = parseFloat($('#sub-total').html().replace(',', ''));
                    var shipping = parseFloat($('#shipping-amount').html());
                    var grand_total = parseFloat($('#grand-total-amount').html());
                    var new_grand_total = sub_total - discount + shipping;
                    if (sub_total > 0) {
                        $('#voucher-discount').html(discount);
                        $('#grand-total-amount').html(addCommas(new_grand_total));
                        $('#voucher-key').val(data['key']);
                    }
                    else {
                        alert('Please add some items to your cart.');
                    }

                }
                else {
                    alert('You have entered an invalid voucher code.');
                }
                // log data to the console so we can see
                console.log(data);

                // here we will handle errors and validation messages
            });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    function add_to_cart(buyer) {
        alert(buyer);

        //url: '<?php echo SITE_URL; ?>cart/travel.php',

        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>/cart/buy.php',
            data: 'buyer=' + buyer,
            success: function (data) {
                alert(data);
                alert('Offer has been sent.');
                location.reload();
            },
            error: function () {
                alert("Ha ocurrido un error");
            }
        });

    }


</script>
<script>
    var remTime = "<?= $remTime; ?>";
    //alert(remTime);
    //flat-colors-very-wide
    $('#countdown17').ClassyCountdown({
        theme: "flat-colors-very-wide",
        end: parseInt($.now()) + parseInt(remTime)
    });
</script>
<!--<<script type="text/javascript" src="assets/js/custom.js"></script>-->
<script type="text/javascript" src="<?php echo SITE_URL; ?>/includes/assets/js/time_counter.js"></script>
<?php require_once("../includes/profile-footer.php"); ?>
