<?php include('inc/header_ini.php'); ?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('inc/sidebar-menu.php'); ?>
				<!-- MIDDLE -->
				<div id="middle" class="flex-fill">

					<!--

						PAGE TITLE
					-->
					<div class="page-title bg-transparent b-0">

						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							Customers
						</h1>
						
					</div>




					<!-- INVITATION -->
					<div class="row gutters-sm">
						
						<div class="col-12 mb-3">


							<!-- portlet -->
							<div class="portlet">
								
								<!-- portlet : header -->
								<div class="portlet-header border-bottom mb-3">
									<span class="d-block text-dark text-truncate font-weight-medium">
										Add Customer
									</span>
								</div>
								<!-- /portlet : header -->


								<!-- portlet : body -->
								<div class="portlet-body">

									<form novalidate method="post" action="/admin_user/admin_staff/" class="bs-validate row"
										data-error-scroll-up="false">

										<!-- CSRF -->
										<input type="text" name="csrf_token" value="{: $csrf_token :}" tabindex="-1" class="form-control hide-force">
										<input type="hidden" name="action" value="process:invitation:add" tabindex="-1">

										<div class="col-12 col-md-6 mb-3">
<div class="p-4 shadow-xs border rounded">

									<div class="row">

<h2 class="h5 mb-4 pl-4">
									Customer Details
								</h2>

										
										<div class="col-12 col-sm-12 col-lg-12">

											<div class="form-label-group mb-3">
												<input required="" placeholder="First Name" id="shipping_first_name" name="shipping_first_name" type="text" class="form-control">
												<label for="shipping_first_name">First Name</label>
											</div>

											<div class="form-label-group mb-3">
												<input required="" placeholder="Last Name" id="shipping_first_name" name="shipping_first_name" type="text" class="form-control">
												<label for="shipping_first_name">Last Name</label>
											</div>

										</div>
										<div class="col-12 col-sm-12 col-lg-12 mb--20">

											<div class="form-label-group mb-3">
												<input required="" placeholder="Business Name" id="shipping_first_name" name="shipping_first_name" type="text" class="form-control">
												<label for="shipping_first_name">Business Name</label>
											</div>

											<div class="form-label-group mb-3">
												<input required="" placeholder="Email Address" id="shipping_first_name" name="shipping_first_name" type="email" class="form-control">
												<label for="shipping_first_name">Email Address</label>
											</div>

										</div>


										


										

									</div>


								</div>
											<!-- /email address -->
											<div class="p-4 shadow-xs border rounded mb-5">

									<div class="row">

<h2 class="h5 mb-4 pl-4">
									Shipping Address
								</h2>

										<div class="col-12 col-sm-12 col-lg-12 mb--20">

											<div class="form-label-group mb-3">
												<input required="" placeholder="Street and Number, P.O. Box, c/o." id="shipping_address_1" name="shipping_address_1" type="text" class="form-control">
												<label for="shipping_address_1">Street and Number, P.O. Box, c/o.</label>
											</div>

											<div class="form-label-group mb-3">
												<input placeholder="Apt, Suite, Unit, Building, Floor, etc" id="shipping_address_2" name="shipping_address_2" type="text" class="form-control">
												<label for="shipping_address_2">Apt, Suite, Unit, Building, Floor, etc <small class="text-info">(optional)</small></label>
											</div>

										</div>







										<div class="col-12 col-sm-6 col-lg-6">

											<div class="form-label-group mb-3">
												<input placeholder="City/Town" id="shipping_city" name="shipping_city" type="text" class="form-control">
												<label for="shipping_city">City/Town</label>
											</div>

										</div>


										<div class="col-12 col-sm-6 col-lg-6">

											<div class="form-label-group mb-3">
												<input placeholder="Zip / Postal Code" id="shipping_zipcode" name="shipping_zipcode" type="text" class="form-control">
												<label for="shipping_zipcode">Zip / Postal Code</label>
											</div>

										</div>


										<div class="col-12 col-sm-6 col-lg-6">

											<!-- 
												based on `SOW : Ajax Select` plugin 
												documentation/plugins-sow-ajax-select.html
											-->
											<div class="dropdown bootstrap-select form-control js-ajax bs-select js-ajaxified"><select name="shipping_country" class="form-control js-ajax bs-select js-ajaxified js-bselectified" data-ajax-target="#shipping_state" data-live-search="true" data-style="select-form-control border">
												<option value="0">Select Contry...</option>
												<option value="1">United States</option>
												<option value="2">Romania</option>
											</select><button type="button" tabindex="-1" class="btn dropdown-toggle select-form-control border" data-toggle="dropdown" role="combobox" aria-owns="bs-select-1" aria-haspopup="listbox" aria-expanded="false" title="Select Contry..."><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner">Select Contry...</div></div> </div></button><div class="dropdown-menu "><div class="bs-searchbox"><input type="search" class="form-control" autocomplete="off" role="combobox" aria-label="Search" aria-controls="bs-select-1" aria-autocomplete="list"></div><div class="inner show" role="listbox" id="bs-select-1" tabindex="-1"><ul class="dropdown-menu inner show" role="presentation"></ul></div></div></div>

										</div>


										<div class="col-12 col-sm-6 col-lg-6">

											<!-- 
												based on `SOW : Ajax Select` plugin 
												documentation/plugins-sow-ajax-select.html
											-->
											<div class="dropdown bootstrap-select disabled form-control bs-select"><select name="shipping_state" id="shipping_state" class="form-control bs-select js-bselectified" disabled="" data-ajax-url="_ajax/select_ajax_state_list.php" data-ajax-method="GET" data-ajax-params="['action','get_state_list']['param2','value2']" data-live-search="true" data-style="select-form-control border">
												<option value="">Select Country First</option>
											</select><button type="button" tabindex="-1" class="btn dropdown-toggle disabled select-form-control border bs-placeholder" data-toggle="dropdown" role="combobox" aria-owns="bs-select-2" aria-haspopup="listbox" aria-expanded="false" data-id="shipping_state" aria-disabled="true" title="Select Country First"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner">Select Country First</div></div> </div></button><div class="dropdown-menu "><div class="bs-searchbox"><input type="search" class="form-control" autocomplete="off" role="combobox" aria-label="Search" aria-controls="bs-select-2" aria-autocomplete="list"></div><div class="inner show" role="listbox" id="bs-select-2" tabindex="-1"><ul class="dropdown-menu inner show" role="presentation"></ul></div></div></div>

										</div>

									</div>


								</div>


											

											<button type="submit" class="btn btn-warning mb-3 mt-3 d-block-xs w-100-xs">
												<i class="fi fi-plus"></i>
												Add Customer
											</button>

										</div>

										

									</form>

								</div>
								<!-- /portlet : body -->

							</div>
							<!-- /portlet -->


						</div>

					</div>
					<!-- /INVITATION -->



					<!-- ADMIN LIST -->
					<div class="row gutters-sm">
						<div class="col-12">

							<!-- portlet -->
							<div class="portlet">
								
								<!-- portlet : header -->
								<div class="portlet-header border-bottom mb-3">
									<span class="d-block text-dark text-truncate font-weight-medium">
										Admin List <span class="font-weight-light">(3)</span>
									</span>
								</div>
								<!-- /portlet : header -->


								<!-- portlet : body -->
								<div class="portlet-body">



									<!-- 
										LIST START 
									-->
									<div class="table-responsive">
									
										<table class="table">

											<thead>
												<tr class="text-muted fs--13">
													<th class="w--100">&nbsp;</th>
													<th>ADMIN</th>
													<th class="w--250">LAST LOGIN</th>
													<th class="w--150">&nbsp;</th>
												</tr>
											</thead>

											<tbody id="group_list">

												<!-- ITEM -->
												<tr id="admin_user_1">
													<td class="text-center">

														<!-- no avatar image -->
														<span data-initials="Happy Flowers" data-assign-color="true" class="sow-util-initials h6 m-0 w--50 h--50 rounded-circle d-inline-flex justify-content-center align-items-center"></span>

														<!-- avatar image -->
														<!--
														<span class="w--50 h--50 rounded-circle d-inline-block bg-light bg-cover lazy" data-background-image="avatar.jpg"></span>
														-->

													</td>

													<td>

														Happy Flowers
														
														<span class="d-block text-muted fs--12">
															<a href="mailto:john.doe@gmail.com" class="text-muted">john.doe@gmail.com</a>
														
															<span class="d-block text-muted">
																Candy Wu
															</span>

														</span>
													</td>


													<td class="text-muted">
														Nov 09, 2019 / 09:12
													</td>

												</tr>
												<!-- /ITEM -->



												<!-- ITEM -->
												<tr id="admin_user_2">
													<td class="text-center">

														<!-- no avatar image -->
														<span data-initials="Melissa Doe" data-assign-color="true" class="sow-util-initials h6 m-0 w--50 h--50 rounded-circle d-inline-flex justify-content-center align-items-center"></span>

														<!-- avatar image -->
														<!--
														<span class="w--50 h--50 rounded-circle d-inline-block bg-light bg-cover lazy" data-background-image="avatar.jpg"></span>
														-->

													</td>

													<td>

														Melissa Doe
														
														<span class="d-block text-muted fs--12">
															<a href="mailto:melissa.doe@gmail.com" class="text-muted">melissa.doe@gmail.com</a>
														
															<span class="d-block text-muted">
																Joined: Sep 01, 2019 / 03:14
															</span>

														</span>
													</td>




													<td class="text-muted">
														Nov 09, 2019 / 09:12
													</td>
<td></td>
												
												</tr>
												<!-- /ITEM -->


											</tbody>

										</table>

									</div>


								</div>
								<!-- /portlet : body -->

							</div>
							<!-- /portlet -->

						</div>
					</div>
					<!-- /ADMIN LIST -->

				</div>
				<!-- /MIDDLE -->
			</div><!-- FOOTER -->
<?php include('inc/footer.php'); ?>