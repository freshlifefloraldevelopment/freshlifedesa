<?php include('inc/header_ini.php'); ?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('inc/sidebar-menu.php'); ?>
<!-- MIDDLE -->

				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							Orders
						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">
						<!-- graph header -->
						<div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">
							Customer Orders Request: <a href="#" class="datepicker link-muted"
														data-layout-rounded="false" 
														data-title="Smarty Datepicker" 
														data-show-weeks="true" 
														data-today-highlight="true" 
														data-today-btn="true" 
														data-autoclose="true" 
														data-date-start="today" 
														data-format="MM/DD/YYYY"
														data-quick-locale='{
															"days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
															"daysShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
															"daysMin": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
															"months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
															"monthsShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
															"today": "Today",
															"clear": "Clear",
															"titleFormat": "MM yyyy"
														}'
														data-id="1"
														data-ajax-url="php/demo.ajax_request.php" 
														data-ajax-params="['action','date_change']['section','customer_invoice']"
														data-ajax-method="POST" 

														data-toast-success="Sucessfully Updated!"
														data-toast-position="top-center">
													08/08/2020
												</a>
							<!-- fullscreen -->
										<a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
											<span class="group-icon">
												<i class="fi fi-expand"></i>
												<i class="fi fi-shrink"></i>
											</span>
										</a>
						</div>


						<div class="row gutters-sm">
							<div class="col-12 col-lg-3 col-xl-3 mb-5">
								<!-- CATEGORIES -->
								
								
								<form class="d-none d-lg-block" id="sidebar_filters" method="get" name="sidebar_filters">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->
									
									
									<!-- Brands -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="Delivery Date" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 12 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 13 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 14 July 2020
												</label>
											</div>
										</div>
									</div><!-- /Brands -->

									<!-- customers -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="Customers" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="customers[]" type="checkbox" value="1"> <i></i> 12 Happy Flowers
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="customers[]" type="checkbox" value="1"> <i></i> 13 Angela & Gabriel
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="customers[]" type="checkbox" value="1"> <i></i> 14 Flowers & Company
												</label>
											</div>
										</div>
									</div><!-- /customer -->
									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" type="submit">Apply Filters</button>
								</form>
							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-9 col-xl-9 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container clearfix pl-0">
										
								<!-- Order Period -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2" 
										data-style="bg-light select-form-control" 
										title="Order Period" 
										data-header="Order Period"
										onchange="window.location=this.value">
									<option value="#">All (12)</option>
									<option value="?filter_order_period=1">Last 3 months</option>
									<option value="?filter_order_period=2">Last 6 months</option>
									<option value="?filter_order_period=2019">Year 2019</option>
								</select>

								<!-- Order Status -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2" 
										data-style="bg-light select-form-control" 
										title="Order Status" 
										data-header="Order Status"
										onchange="window.location=this.value">
									<option value="#">Any</option>
									<option value="?filter_order_status=1">Completed</option>
									<option value="?filter_order_status=2">Canceled</option>
									<option value="?filter_order_status=3">Refunded</option>
								</select>

							</div>

							<!-- order -->
							<div class="clearfix p-3 shadow-xs shadow-md-hover mb-3 rounded bg-white">

								<h2 class="fs--18">
									<a href="account-order-detail.html" class="float-end fs--12">
										ORDER DETAIL
									</a>
									<a	 href="#" data-toggle="modal" data-target="#customer_orders"
	class="text-dark">
	Order #1487
</a>
								</h2>

								<p class="mb-0 fs--14">
									Date: November 23, 2019, 11:38 | Total: $2796.45
								</p>

								<p class="mb-0 fs--14">
									Status:&nbsp;
									<span class="text-warning font-weight-normal">Pending / New</span>
								</p>

							</div>
							<!-- /order -->



							<!-- order -->
							<div class="clearfix p-3 shadow-xs shadow-md-hover mb-3 rounded bg-white">

								<h2 class="fs--18">
									<a href="account-order-detail.html" class="float-end fs--12">
										ORDER DETAIL
									</a>
									<a	 href="#" data-toggle="modal" data-target="#customer_orders"
	class="text-dark">
	Order #1487
</a>
								</h2>

								<p class="mb-0 fs--14">
									Date: November 23, 2019, 11:38 | Total: $2796.45
								</p>

								<p class="mb-0 fs--14">
									Status:&nbsp;
									<span class="text-info font-weight-normal">Refunded</span>
								</p>

							</div>
							<!-- /order -->



							<!-- order -->
							<div class="clearfix p-3 shadow-xs shadow-md-hover mb-3 rounded bg-white">

								<h2 class="fs--18">
									<a href="account-order-detail.html" class="float-end fs--12">
										ORDER DETAIL
									</a>
									<a	 href="#" data-toggle="modal" data-target="#customer_orders"
	class="text-dark">
	Order #1487
</a>
								</h2>

								<p class="mb-0 fs--14">
									Date: November 23, 2019, 11:38 | Total: $2796.45
								</p>

								<p class="mb-0 fs--14">
									Status:&nbsp;
									<span class="text-danger font-weight-normal">Canceled</span>
								</p>

							</div>
							<!-- /order -->




							<!-- order -->
							<div class="clearfix p-3 shadow-xs shadow-md-hover mb-3 rounded bg-white">

								<h2 class="fs--18">
									<a href="account-order-detail.html" class="float-end fs--12">
										ORDER DETAIL
									</a>
									<a	 href="#" data-toggle="modal" data-target="#customer_orders"
	class="text-dark">
	Order #1487
</a>
								</h2>

								<p class="mb-0 fs--14">
									Date: November 23, 2019, 11:38 | Total: $2796.45
								</p>

								<p class="mb-0 fs--14">
									Status:&nbsp;
									<span class="text-success font-weight-normal">Completed</span>
								</p>

							</div>
							<!-- /order -->



							<!-- pagination -->
							<nav aria-label="pagination" class="mt-5">
								<ul class="pagination pagination-pill justify-content-end justify-content-center justify-content-md-end">

									<li class="page-item disabled">
										<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
									</li>
									
									<li class="page-item active">
										<a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
									</li>
									
									<li class="page-item" aria-current="page">
										<a class="page-link" href="#">2</a>
									</li>
									
									<li class="page-item">
										<a class="page-link" href="#">3</a>
									</li>
									
									<li class="page-item">
										<a class="page-link" href="#">Next</a>
									</li>

								</ul>
							</nav>
							<!-- pagination -->
									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->


			</div><!-- FOOTER -->
<!-- /Order Details Modal -->
<div class="modal fade" id="customer_orders" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
		<div class="modal-dialog modal-full" role="document">
				<div class="modal-content">
						
						<!-- Header -->
						<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabelMd">Order #1487</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span class="fi fi-close fs--18" aria-hidden="true"></span>
								</button>
						</div>

						<!-- Content -->
						<div class="modal-body">
											

							<!-- ORDER INFO -->
							<div class="p-4 shadow-xs border rounded mb-4">
								
								<div class="row">

									<div class="col-12 col-sm-6 col-md-6 col-lg-6">

										<h2 class="fs--18 mb-0">
											Order #1009
										</h2>

										<p>
											Delivery date: November 23, 2020
										</p>

										<p class="mb-0">
											Status:

											<span class="text-warning">
												Pending / New
											</span>
										</p>

									</div>


									<div class="col-12 col-sm-6 col-md-6 col-lg-6">

										
										<a href="#"
											data-toggle="modal" data-target="#add_from_inventory"
											class="float-end float-none-xs m-0 btn btn-sm btn-success fs--14 mb-0 mt-2" style="margin-left: 10px!important;">

											ADD FROM INVENTORY
											<span class="fs--12 d-block">
												29 min. remaining
											</span>

										</a>
										<a href="#"
											data-toggle="modal" data-target="#add_from_inventory"
											class="float-end float-none-xs m-0 btn btn-sm btn-warning fs--14 mb-0 mt-2">

											ORDER REQUEST
											<span class="fs--12 d-block">
												10 min. remaining
											</span>

										</a>

									</div>

								</div>

							</div>
							<!-- /ORDER INFO -->



							<!-- ORDER PERSONAL DETAIL -->
							<div class="p-4 shadow-xs rounded mb-4">

								<div class="row row-grid b-0">

									<div class="col-12 col-sm-12 col-md-4 col-lg-4">

										<div class="px-3 pb-3">

											<h6 class="mt-2 text-primary">
												ORDER CONTACT
											</h6>

											<!-- not needed/required -->
											<!--
											<div class="mt-3">
												<span class="d-block text-muted">Name:</span>
												John Doe
											</div>
											-->

											<div class="mt-3">
												<label class="text-dark d-block m-0">Email:</label>
												<a class="text-muted" href="mailto:john.doe@gmail.com">john.doe@gmail.com</a>
											</div>

											<div class="mt-3">
												<label class="text-dark d-block m-0">Phone:</label>
												<span class="text-muted">(+01) 555-5555</span>
											</div>

											<div class="mt-3">
												<span class="d-block text-dark">Payment Method:</span> 

												<a class="text-decoration-none" href="#order_payment_info" data-toggle="collapse">
													<span class="group-icon fs--14">
														<i class="fi fi-arrow-down"></i> 
														<i class="fi fi-close"></i> 
													</span>
												
													<span class="d-inline-block pl-2 pr-2">
														Bank Deposit
													</span>
												</a>
												
												<!-- data from checkout -->
												<p class="bg-light p-2 rounded mt-2 collapse" id="order_payment_info">
													<span class="d-block text-muted">Payment Info:</span> 
													<b>Bank Name</b>: ACME Bank<br>
													Bank Branch: New York<br>
													Account Name: John Smith<br>
													Account Number: XXXXXXXXXXXX
												</p>
											</div>

										</div>

									</div>

									<div class="col-12 col-sm-6 col-md-4 col-lg-4">

										<div class="px-3 pb-3">

											<h6 class="mb-4 mt-2 text-primary">
												SHIPPING <span class="font-weight-normal">ADDRESS</span>
											</h6>

											<label class="text-dark d-block m-0">Name</label>
											<p>
												John Doe
												<span class="d-block fs--13 text-muted">(+01) 555-5555</span>
											</p>

											<label class="text-dark d-block m-0">Company</label>
											<p>
												Smarty Inc.
											</p>
											
											<label class="text-dark d-block m-0">Address</label>
											<p>
												Road 741, No.44 <br>
												United States <br>
												New York <br>
												500096<br>
											</p>

										</div>

									</div>


									<div class="col-12 col-sm-6 col-md-4 col-lg-4 br-0">

										<div class="px-3 pb-3">

											<h6 class="mb-4 mt-2 text-primary">
												BILLING <span class="font-weight-normal">ADDRESS</span>
											</h6>
											
											<div class="bg-light p-2 rounded">

												<i class="fi mdi-check text-success"></i>
												<span class="d-inline-block pl-2 pr-2">
													Same as shipping
												</span>

											</div>
											
										</div>

									</div>


								</div>

								<div class="text-muted mt-4">
									<span class="font-weight-medium">Customer Note:</span>

									â€“ â€“

								</div>
								<!-- customer detail -->

							</div>
							<!-- /ORDER PERSONAL DETAIL -->



							<!-- ITEMS -->
							<div class="p-4 shadow-xs rounded mb-4">


								


								

								<!-- 
	default 
		data-table-clone-method="append|prepend"
-->
<div class="js-form-advanced-table mb-6" 
	data-table-column-insert-before=".js-clone-before" 
	data-table-column-insert-element='<input type="text" class="form-control form-control-sm" value="">' 
	data-table-column-delete-button='<span class="btn-table-column-delete fi fi-close fs--15 cursor-pointer px-1 d-inline-block"></span>' 
	data-table-column-limit="3" 
	data-table-row-limit="5" 
	data-table-row-method="append">


	<!-- 
		
		ADD COLUMN

			The name of this input (name="option" in this demo) 
			is used to generate input fields with names like this:
				name="option[color]"
			
			if no input name used, field name is generated like this:
				name="color"

	-->
	<div class="js-form-advanced-table-column-add input-group mb-3 max-w-300">

		<!-- 
			Optional Plugin used:
			SOW : Search Inline
		-->
		<input type="text" name="variant" class="form-control form-control-sm input-suggest" 
			placeholder="Ex: color, size" 
			aria-label="Table Cloner" 
			aria-describedby="button-table-cloner"
			data-input-suggest-type="text" 
			data-input-suggest-name="variant" 
			data-input-suggest-ajax-url="_ajax/input_suggest_variants.json" 
			data-input-suggest-ajax-method="GET" 
			data-input-suggest-ajax-limit="100">

		<div class="input-group-append">
			<button class="btn btn-sm btn-outline-secondary" type="button" id="button-table-cloner">Add</button>
		</div>
	</div>


	<table class="table table-bordered table-align-middle table-sm">
		<thead>
			<tr>
				<th class="w--90">Image</th>
				<th>Flower Name</th>
				<th>Barcode</th>
				<th class="js-clone-before">Price</th>
				<th>BS Select</th>
				<th>Date</th>
				<th class="w--80"> </th>
			</tr>
		</thead>

		<tbody>

			<tr>
				<td>
					<img src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="100%">
				</td>
				<!-- SKU -->
				<td>
					<select class="form-control form-control-sm bs-select" name="variant[select]" title="Please Select...">
						<option value="0">Standard Rose Freedom 60cm 25st/bu Red - Florana Farms S.A (916)</option>
						<option value="1">Standard Rose Freedom 60cm 25st/bu Red - Florana Farms S.A (916)</option>
						<option value="2">Standard Rose Freedom 60cm 25st/bu Red - Florana Farms S.A (916)</option>
						<option value="3">Standard Rose Freedom 60cm 25st/bu Red - Florana Farms S.A (916)</option>
					</select>
				</td>

				<!-- Barcode -->
				<td>
					<input type="text" class="form-control form-control-sm" name="variant[barcode]" value="">
				</td>

				<!-- Price -->
				<td class="js-clone-before">
					<input type="number" class="form-control form-control-sm" name="variant[price]" value="">
				</td>

				<!-- Select -->
				<td>
					<select class="form-control form-control-sm bs-select" name="variant[select]" title="Please Select...">
						<option value="0">Basic</option>
						<option value="1">Mustard</option>
						<option value="2">Ketchup</option>
						<option value="3">Barbecue</option>
					</select>
				</td>

				<!-- Date -->
				<td>
					<input autocomplete="off" type="text" name="variant[date]" class="form-control form-control-sm rangepicker" 
						data-layout-rounded="false" 
						data-single-datepicker="true" 
						data-timepicker="false" 
						data-timepicker-24h="true" 
						data-timepicker-show-seconds="false" 
						data-disable-past-dates="true" 
						data-date-format="MM/DD/YYYY" 
						data-disable-auto-update-input="true" 
						data-quick-locale='{
							"lang_apply"	: "Apply",
							"lang_cancel"	: "Cancel",
							"lang_crange"	: "Custom Range",
							"lang_months"	 : ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
							"lang_weekdays" : ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"]
						}'>
				</td>

				

				<!-- Option -->
				<td class="text-center">

					<a href="#" class="btn btn-sm btn-primary btn-table-clone rounded-circle">
						<span class="group-icon">
							<i class="fi fi-plus"></i>
							<i class="fi fi-thrash"></i>
						</span>
					</a>

				</td>

			</tr>

		</tbody>
	</table>


</div>


								<div class="row">
									<div class="offset-sm-6 offset-md-6 offset-lg-7 col-12 col-sm-6 col-md-6 col-lg-5">

										<div class="clearfix mb--15">
											Subtotal:
											<span class="float-end text-align-end">
												$2796.45
												<span class="d-block text-muted fs--12">
													VAT: 19% (included)
												</span>
											</span>
										</div>


										<div class="clearfix">
											Shipping:
											<span class="float-end text-align-end">
												$120.00
											</span>
										</div>

										<hr>

										<div class="clearfix">
											<h5 class="float-start">
												Total:
											</h5>
											<h5 class="float-end">
												$2916.45 
											</h5>
										</div>

									</div>
								</div>


								<hr>


								<div class="text-success text-align-end text-center-xs px-3">

									Congratulations John, you saved: <b>$697.00</b> 

									<!-- show percent on saved more than 10% -->
									<span class="font-light">(20%)</span>

								</div>


							</div>
							<!-- /ITEMS -->



							<!-- ORDER OPTIONS -->
							<div class="my-5 d-none d-sm-block"><!-- desktop only -->

								<div class="clearfix text-align-center-xs">

									<h6>
										Order Options
									</h6>

									<a class="btn btn-sm btn-light" href="#!">
										<i class="fi fi-print m-0"></i> 
									</a>

									<a class="btn btn-sm btn-light" href="#!">
										<span class="fs--13 m-0">PDF : INVOICE</span> 
									</a>

								</div>

							</div>
							<!-- /ORDER OPTIONS -->


						</div>

						<!-- Footer -->
						<div class="modal-footer">
								<button type="button" class="btn btn-primary">
										<i class="fi fi-check"></i> 
										Save
								</button>
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
										<i class="fi fi-close"></i> 
										Close
								</button>
						</div>

				</div>
		</div>
</div>
<!-- /Order Details Modal -->
<!-- add from inventory Modal -->
										<div class="modal fade" id="add_from_inventory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelXl" aria-hidden="true">
											<div class="modal-dialog modal-xl" role="document" style="-webkit-box-shadow: -1px 0px 25px 2px rgba(0,0,0,0.75);-moz-box-shadow: -1px 0px 25px 2px rgba(0,0,0,0.75);
box-shadow: -1px 0px 25px 2px rgba(0,0,0,0.75);">
												<div class="modal-content">
													
													<div class="modal-header">
														<h5 class="modal-title" id="exampleModalLabelXl">Modal title</h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span class="fi fi-close fs--18" aria-hidden="true"></span>
														</button>
													</div>

													<div class="modal-body">
														<table class="table table-framed">
													<thead>
														<tr>
															<th></th>
															<th class="text-gray-500 font-weight-normal fs--14 w--300">
																VARIETY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--100">
																PRICE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--100 text-left">
																SELECT QUANTITY
															</th>
														</tr>
													</thead><!-- #item_list used by checkall: data-checkall-container="#item_list" -->
													<tbody id="item_list">
														<!-- product -->
														<tr>
															<td>
																<img alt="..." class="img-fluid" src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="65">
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Growers: 14, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span> 
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..." class="img-fluid" src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Average Market Price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html" 
											data-ajax-modal-size="modal-md" 
											data-ajax-modal-centered="true" 
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Stems/Bunch: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0"> 

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>
																
									</div>

									<div class="pl-2 pr-2 w--120"> 
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;"> 
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>		
															</td><!-- options -->
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<img alt="..." class="img-fluid" src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="65">
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Growers: 14, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span> 
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..." class="img-fluid" src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Average Market Price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html" 
											data-ajax-modal-size="modal-md" 
											data-ajax-modal-centered="true" 
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Stems/Bunch: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0"> 

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>
																
									</div>

									<div class="pl-2 pr-2 w--120"> 
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;"> 
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>		
															</td><!-- options -->
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<img alt="..." class="img-fluid" src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="65">
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Growers: 14, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span> 
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..." class="img-fluid" src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Average Market Price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html" 
											data-ajax-modal-size="modal-md" 
											data-ajax-modal-centered="true" 
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Stems/Bunch: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0"> 

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>
																
									</div>

									<div class="pl-2 pr-2 w--120"> 
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;"> 
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>		
															</td><!-- options -->
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<img alt="..." class="img-fluid" src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="65">
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Growers: 14, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span> 
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..." class="img-fluid" src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Average Market Price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html" 
											data-ajax-modal-size="modal-md" 
											data-ajax-modal-centered="true" 
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Stems/Bunch: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0"> 

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>
																
									</div>

									<div class="pl-2 pr-2 w--120"> 
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;"> 
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>		
															</td><!-- options -->
														</tr>
														<!-- product -->
														<!-- product -->
														<tr>
															<td>
																<img alt="..." class="img-fluid" src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg" width="65">
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_1" data-toggle="modal" href="#">Standard Rose Freedom 50 25st/bu</a> <span class="d-block text-muted fs--13">ECUADOR</span> <span class=
																"d-block text-muted fs--13 mt--10">Growers: 14, Quality Records: 5, Last price: $0,18</span> <span class="d-block text-gray-500 fs--13">Arrival Date : Sep 26 2019 / 11:51am</span> 
																<!-- Modal -->
																<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_1" role="dialog" tabindex="-1">
																	<div class="modal-dialog" role="document">
																		<div class="modal-content">
																			<!-- Header -->
																			<div class="modal-header">
																				<h5 class="modal-title" id="exampleModalLabelMd">
																					Standard Rose Freedom 50 25st/bu
																				</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																			</div><!-- Content -->
																			<div class="modal-body">
																				<img alt="..." class="img-fluid" src="https://app.freshlifefloral.com/images/product-image/big/062013073839_crop.jpg">
																			</div>
																		</div>
																	</div>
																</div><!-- /Modal -->

															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Average Market Price 0.18 <sup class="text-muted fs--10">USD</sup></span> <a href="#"
											data-href="_ajax/price_modal.html" 
											data-ajax-modal-size="modal-md" 
											data-ajax-modal-centered="true" 
											data-ajax-modal-callback-function="" class="d-block text-success fs--15 js-ajax-modal">&nbsp; Your price 0.75 <sup class=
																"text-muted fs--10">CAD</sup></a> <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Stems/Bunch: Stems</span> <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box Pack: 150</span>
															</td><!-- brand -->
															<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0"> 

									<!-- QUANTITY INPUT -->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_options" class="form-control-sm bs-select">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="3">4</option>
												<option value="3">5</option>
												<option value="3">6</option>
												<option value="3">7</option>
												<option value="3">8</option>
												<option value="3">9</option>
												<option value="3">10</option>
											</select>
																
									</div>

									<div class="pl-2 pr-2 w--120"> 
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" data-toggle="modal" data-target="#buy_modal" onclick="return false;"> 
											<span class="p-0-xs">
												<span class="fs--18">Buy Now</span>
											</span>
										</button>
									</div>

								</div>		
															</td><!-- options -->
														</tr>
														<!-- product -->
														
														
													</tbody>
													<tfoot>
														<tr>
															<th class="text-gray-500 font-weight-normal fs--14 w--120">
																&nbsp;
															</th>
															<th class="text-gray-500 font-weight-normal fs--14">
																VARIETY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--200">
																PRICE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--100 text-left">
																SELECT QUANTITY
															</th>
														</tr>
													</tfoot>
												</table>
													</div>

													<div class="modal-footer">
														<button type="button" class="btn btn-primary">
															<i class="fi fi-check"></i> 
															Save changes
														</button>
														<button type="button" class="btn btn-secondary" data-dismiss="modal">
															<i class="fi fi-close"></i> 
															Close
														</button>
													</div>

												</div>
											</div>
										</div>

<?php include('inc/footer.php'); ?>