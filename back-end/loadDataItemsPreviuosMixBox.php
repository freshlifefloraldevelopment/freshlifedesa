<?php
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 15 Abril 2021
Structure MarketPlace previous to buy

**/
include('config/config_gcp.php');

$htmlLoadData="";
if(isset($_POST["PreviousProductKey"]) && $_POST['PreviousProductKey']!=''){
$PreviousProductKey = $_POST['PreviousProductKey'];
$order_id_prev = $_POST['ord_id'];
$grower_id = $_POST['gid_id'];
$NewB    = $_POST['box_nex'];

if($NewB!=1){
 $countMixBox = "select max(box_mix_id) as max
from buyer_requests_shoping_cart_temp_items
where id_buyer_temp = '$PreviousProductKey'
and  id_order_prev='$order_id_prev'
and id_grower = '$grower_id'
and state_item='0'";

$rs_countMixBox    = mysqli_query($con, $countMixBox);
$totalCountMixBox  = mysqli_fetch_array($rs_countMixBox);
$box_mix_id = $totalCountMixBox['max'];

  $loadDataItemsPrevious= "select sct.id_buyer_temp_item as keyItem, sct.id_product , p.name as prodname,
  p.image_path as prodimage, sct.price as prodprice, sct.quantity as prodquantity, sct.size_steam as sizesteam,
  sct.steams as steams, sct.feature_id as features, sct.size_id as size_id, sct.id_category as categoryid
from buyer_requests_shoping_cart_temp_items sct
inner join product  p   ON sct.id_product=p.id
where sct.id_buyer_temp = '$PreviousProductKey'
and sct.id_grower = '$grower_id'
and sct.id_order_prev='$order_id_prev'
and sct.box_mix_id ='$box_mix_id'
and sct.state_item='0'
order by sct.id_buyer_temp_item";

  $result_loadDataItemsPrevious = mysqli_query($con,$loadDataItemsPrevious);
  while ($row_result = mysqli_fetch_array($result_loadDataItemsPrevious))
  {
    $id_feature = $row_result['features'];
    $id_product = $row_result['id_product'];
    $id_size = $row_result['size_id'];
    $idCategory = $row_result['categoryid'];

    $selectFeatures= mysqli_query($con,"select name from features where id='$id_feature'");
     $fila = mysqli_fetch_array($selectFeatures);
      $name_features = "<br>".$fila['name'];

  $htmlLoadData .= "<div class='clearfix d-block px-3 py-3 border-top'><div class='h--50 overflow-hidden float-start mt-1'><img width='40' src='https://app.freshlifefloral.com/".$row_result['prodimage']."' alt=".$row_result['prodname']."></div><a href='#!' class='fs--15 d-block position-relative'><span class='d-block text-truncate'>1 × ".$row_result['steams']." <br> ".$row_result['prodname'].' '.$row_result['sizesteam']."<font color='#800080'>".$name_features."</font></span></a><span style='font-size: 1em; color: grey;'><div class='float-end' onclick='deleteItemPrevious(".$row_result['keyItem'].",".$grower_id.")'><i class='fa fa-trash fa-1x' aria-hidden='true' title='Delete product'></i></div></span><span  class='d-block fs--12 mt-1'>".number_format($row_result['prodprice']*$row_result['steams'],2)."</span></div>";
  }




}
 echo $htmlLoadData;
}
?>
