<?php
if(!$growerID_Prod){
	header("Location: ../en/variety-page.php");
	exit();
}
include('inc/header.php'); ?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('inc/sidebar-menu.php'); ?>
				<!-- MIDDLE -->
				<div id="middle" class="flex-fill">

					<!--

						PAGE TITLE
					-->
					<div class="page-title bg-transparent b-0">

						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							Reports
						</h1>

					</div>


					<div class="row gutters-sm">

						<div class="col-12 mb-3">

							<!-- start:portlet -->
							<div class="portlet">

								<div class="portlet-header border-bottom">
									<span>Reports</span>
								</div>

								<div class="portlet-body">
									<div class="container py-6">

									<div class="row">

										<div class="col-lg-6 mb-4">

											<div id="clipboard_1" class="mb-4">
												<!-- Aero List -->
												<ul class="list-group list-group-flush rounded overflow-hidden">

													<li class="list-group-item pt-4 pb-4">
														<div class="d-flex">


															<div class="pl--12 pr--12">
																<p class="text-dark font-weight-medium m-0">
																	Category box packing
																</p>

																<p class="m-0">
																	Using the same core as frontend!
																</p>
															</div>

														</div>
													</li>

													<li class="list-group-item pt-4 pb-4">
														<div class="d-flex">



															<div class="pl--12 pr--12">
																<p class="text-dark font-weight-medium m-0">
																	Category box packing
																</p>

																<p class="m-0">
																	Using helpers to build all blocks
																</p>
															</div>

														</div>
													</li>

													<li class="list-group-item pt-4 pb-4">
														<div class="d-flex">


															<div class="pl--12 pr--12">
																<p class="text-dark font-weight-medium m-0">
																	Category box packing
																</p>

																<p class="m-0">
																	All updates are free, no charge!
																</p>
															</div>

														</div>
													</li>

													<li class="list-group-item pt-4 pb-4">
														<div class="d-flex">


															<div class="pl--12 pr--12">
																<p class="text-dark font-weight-medium m-0">
																	Category box packing
																</p>

																<p class="m-0">
																	Well, we tried but without much luck!
																</p>
															</div>

														</div>
													</li>

												</ul>
												<!-- /Aero List -->
											</div>

										</div>

										<div class="col-lg-6 mb-4">

											<div id="clipboard_2" class="mb-4">
												<!-- Aero List -->
												<ul class="list-group list-group-flush rounded overflow-hidden">

													<li class="list-group-item d-flex justify-content-between align-items-center pt-4 pb-4">

														<div>
															<p class="text-dark font-weight-medium m-0">
																Admin Template Included
															</p>

															<p class="m-0">
																Using the same core as frontend!
															</p>
														</div>


													</li>

													<li class="list-group-item d-flex justify-content-between align-items-center pt-4 pb-4">

														<div>
															<p class="text-dark font-weight-medium m-0">
																Modular &amp; Lightweight
															</p>

															<p class="m-0">
																Using helpers to build all blocks
															</p>
														</div>



													</li>

													<li class="list-group-item d-flex justify-content-between align-items-center pt-4 pb-4">

														<div>
															<p class="text-dark font-weight-medium m-0">
																Free lifetime update
															</p>

															<p class="m-0">
																All updates are free, no charge!
															</p>
														</div>


													</li>

													<li class="list-group-item d-flex justify-content-between align-items-center pt-4 pb-4">

														<div>
															<p class="text-dark font-weight-medium m-0">
																Free Ferrari included
															</p>

															<p class="m-0">
																Well, we tried but without much luck!
															</p>
														</div>


													</li>

												</ul>
												<!-- /Aero List -->
											</div>

										</div>


									</div>

									</div>
								</div>

							</div>
							<!-- end:portlet -->

						</div>

					</div>



				</div>
				<!-- /MIDDLE -->
			</div><!-- FOOTER -->
<?php include('inc/footer.php'); ?>
