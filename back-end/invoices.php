<?php
if(!$growerID_Prod){
	header("Location: ../en/variety-page.php");
	exit();
}
include('inc/header.php'); ?>
			<div class="d-flex flex-fill" id="wrapper_content">
<?php include('inc/sidebar-menu.php'); ?>
<!-- MIDDLE -->

				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							Invoices
						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">
						<!-- graph header -->
						<div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">
							Customer Orders Request: <a href="#" class="datepicker link-muted"
														data-layout-rounded="false"
														data-title="Smarty Datepicker"
														data-show-weeks="true"
														data-today-highlight="true"
														data-today-btn="true"
														data-autoclose="true"
														data-date-start="today"
														data-format="MM/DD/YYYY"
														data-quick-locale='{
															"days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
															"daysShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
															"daysMin": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
															"months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
															"monthsShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
															"today": "Today",
															"clear": "Clear",
															"titleFormat": "MM yyyy"
														}'
														data-id="1"
														data-ajax-url="php/demo.ajax_request.php"
														data-ajax-params="['action','date_change']['section','customer_invoice']"
														data-ajax-method="POST"

														data-toast-success="Sucessfully Updated!"
														data-toast-position="top-center">
													08/08/2020
												</a>
							<!-- fullscreen -->
										<a href="#" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
											<span class="group-icon">
												<i class="fi fi-expand"></i>
												<i class="fi fi-shrink"></i>
											</span>
										</a>
						</div>


						<div class="row gutters-sm">
							<div class="col-12 col-lg-3 col-xl-3 mb-5">
								<!-- CATEGORIES -->


								<form class="d-none d-lg-block" id="sidebar_filters" method="get" name="sidebar_filters">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->


									<!-- Brands -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<div class="input-group-over">
											<input class="form-control form-control-sm iqs-input" data-container=".iqs-container" placeholder="quick filter" type="text" value=""><span class=
											"fi fi-search btn btn-sm pl--12 pr--12 text-gray-500"></span>
										</div>
										<div class="iqs-container mt-3 scrollable-horizontal scrollable-styled-light max-h-250">
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 12 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 13 July 2020
												</label>
											</div>
											<div class="iqs-item">
												<label class="form-checkbox form-checkbox-primary">
													<input name="brand[]" type="checkbox" value="1"> <i></i> 14 July 2020
												</label>
											</div>
										</div>
									</div><!-- /Brands -->
									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" type="submit">Apply Filters</button>
								</form>
							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-9 col-xl-9 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container clearfix pl-0">

								<!-- Order Period -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Period"
										data-header="Order Period"
										onchange="window.location=this.value">
									<option value="#">All (12)</option>
									<option value="?filter_order_period=1">Last 3 months</option>
									<option value="?filter_order_period=2">Last 6 months</option>
									<option value="?filter_order_period=2019">Year 2019</option>
								</select>

								<!-- Order Status -->
								<select class="form-control b-0 bg-light bs-select w--250 w-100-xs float-start float-none-xs mb-2"
										data-style="bg-light select-form-control"
										title="Order Status"
										data-header="Order Status"
										onchange="window.location=this.value">
									<option value="#">Any</option>
									<option value="?filter_order_status=1">Completed</option>
									<option value="?filter_order_status=2">Canceled</option>
									<option value="?filter_order_status=3">Refunded</option>
								</select>

							</div>

							<!-- order -->
							<div class="clearfix p-3 shadow-xs shadow-md-hover mb-3 rounded bg-white">

								<h2 class="fs--18">
									<a href="account-order-detail.html" class="float-end fs--12">
										ORDER DETAIL
									</a>
									<a href="account-order-detail.html" class="text-dark">
										Order #1487
									</a>
								</h2>

								<p class="mb-0 fs--14">
									Date: November 23, 2019, 11:38 | Total: $2796.45
								</p>

								<p class="mb-0 fs--14">
									Status:&nbsp;
									<span class="text-warning font-weight-normal">Pending / New</span>
								</p>

							</div>
							<!-- /order -->



							<!-- order -->
							<div class="clearfix p-3 shadow-xs shadow-md-hover mb-3 rounded bg-white">

								<h2 class="fs--18">
									<a href="account-order-detail.html" class="float-end fs--12">
										ORDER DETAIL
									</a>
									<a href="account-order-detail.html" class="text-dark">
										Order #1123
									</a>
								</h2>

								<p class="mb-0 fs--14">
									Date: November 23, 2019, 11:38 | Total: $2796.45
								</p>

								<p class="mb-0 fs--14">
									Status:&nbsp;
									<span class="text-info font-weight-normal">Refunded</span>
								</p>

							</div>
							<!-- /order -->



							<!-- order -->
							<div class="clearfix p-3 shadow-xs shadow-md-hover mb-3 rounded bg-white">

								<h2 class="fs--18">
									<a href="account-order-detail.html" class="float-end fs--12">
										ORDER DETAIL
									</a>
									<a href="account-order-detail.html" class="text-dark">
										Order #1009
									</a>
								</h2>

								<p class="mb-0 fs--14">
									Date: November 23, 2019, 11:38 | Total: $2796.45
								</p>

								<p class="mb-0 fs--14">
									Status:&nbsp;
									<span class="text-danger font-weight-normal">Canceled</span>
								</p>

							</div>
							<!-- /order -->




							<!-- order -->
							<div class="clearfix p-3 shadow-xs shadow-md-hover mb-3 rounded bg-white">

								<h2 class="fs--18">
									<a href="account-order-detail.html" class="float-end fs--12">
										ORDER DETAIL
									</a>
									<a href="account-order-detail.html" class="text-dark">
										Order #987
									</a>
								</h2>

								<p class="mb-0 fs--14">
									Date: November 23, 2019, 11:38 | Total: $2796.45
								</p>

								<p class="mb-0 fs--14">
									Status:&nbsp;
									<span class="text-success font-weight-normal">Completed</span>
								</p>

							</div>
							<!-- /order -->



							<!-- pagination -->
							<nav aria-label="pagination" class="mt-5">
								<ul class="pagination pagination-pill justify-content-end justify-content-center justify-content-md-end">

									<li class="page-item disabled">
										<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
									</li>

									<li class="page-item active">
										<a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
									</li>

									<li class="page-item" aria-current="page">
										<a class="page-link" href="#">2</a>
									</li>

									<li class="page-item">
										<a class="page-link" href="#">3</a>
									</li>

									<li class="page-item">
										<a class="page-link" href="#">Next</a>
									</li>

								</ul>
							</nav>
							<!-- pagination -->
									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->


			</div><!-- FOOTER -->
<?php include('inc/footer.php'); ?>
