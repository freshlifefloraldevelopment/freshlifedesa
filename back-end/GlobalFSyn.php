<?php
/**
#Market place cart
Developer educristo@gmail.com
Start 12 Abril 2021
Structure MarketPlace previous to buy
**/

function getDataBuyer($con,$userSessionID){

	$sql_name = "SELECT first_name as fname, last_name as lname FROM buyers
									where id = '$userSessionID' ";

										$rs_nameB = mysqli_query($con,$sql_name);
										$row_nameB = mysqli_fetch_array($rs_nameB);
										$fnameB = $row_nameB['fname'];
										$lnameB = $row_nameB['lname'];

										return $fname." ".$lnameB;
}

function getemailBuyer($con,$userSessionID){

	$sql_name = "SELECT email FROM buyers
									where id = '$userSessionID' ";

										$rs_emailB = mysqli_query($con,$sql_name);
										$row_emailB = mysqli_fetch_array($rs_emailB);
										$emailB = $row_emailB['email'];

										return $emailB;
}

function getNameGrower($con, $growerID_Prod){

	 $sql_name = "SELECT growers_name FROM growers
									where id = '$growerID_Prod' ";

										$rs_name = mysqli_query($con,$sql_name);
										$row_nameB = mysqli_fetch_array($rs_name);
										$nameG = $row_nameB['growers_name'];

										return $nameG;
}

function calculateKilo($userSessionID,$con){
   $getBuyerShippingMethod = "select shipping_method_id
                              from buyer_shipping_methods
                             where buyer_id ='" . $userSessionID . "'";

  $buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
  $buyerShippingMethod = mysqli_fetch_assoc($buyerShippingMethodRes);
   $shipping_method_id = $buyerShippingMethod['shipping_method_id'];

     $getShippingMethod = "select connect_group
                             from shipping_method
                            where id='" . $shipping_method_id . "'";

      $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
      $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);

       $temp_conn = explode(',', $shippingMethodDetail['connect_group']);

      $id_conn = $temp_conn[1];  // Default

      $getConnect = "select charges_per_kilo
                       from connections
                      where id='" . $id_conn . "'";

      $rs_connect = mysqli_query($con, $getConnect);

      $charges = mysqli_fetch_assoc($rs_connect);

       /////////////////////////////////////////////////////////////

      $cost = $charges['charges_per_kilo'];
      $cost_un = unserialize($cost);

      $cost_sum = 0;

      foreach ($cost_un as $key => $value) {
          $cost_sum = $cost_sum + $value;
      }

      return $charges_per_kilo_trans =  $cost_sum;
}

function calculatePriceGrower($growerid,$productid,$sizeid,$idsc,$con,$userSessionID){

	//$tasaKilo = calculateKilo($userSessionID,$con);

    $getGrowerPriceMeth1 = "select gp.price as price
															from growcard_prod_price gp
															INNER JOIN growers g ON gp.growerid = g.id
															INNER JOIN product p ON gp.productid = p.id
															INNER JOIN subcategory s ON p.subcategoryid = s.id
															INNER JOIN colors c on p.color_id = c.id
															where g.active = 'active'
															and gp.growerid = '$growerid'
															and gp.productid = '$productid'
															and gp.sizeid = '$sizeid'";
  $getGrowerPriceMeth1_array = mysqli_query($con, $getGrowerPriceMeth1);
  $GrowerPriceMeth1 = mysqli_fetch_assoc($getGrowerPriceMeth1_array);
  $priceGrower = $GrowerPriceMeth1['price'];
															if($priceGrower<=0){
																$getGrowerPriceMeth2 = "select gp.id,gp.price_adm as price,
																s.name as sizename , gp.feature as feature, gp.factor,
																gp.stem_bunch , b.name as stems
																from grower_parameter gp
																inner JOIN sizes s ON gp.size = s.id
																inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
																left JOIN features f ON gp.feature = f.id
																where gp.idsc = '$idsc'
																and gp.size = '$sizeid'";
																$getGrowerPriceMeth2_array = mysqli_query($con, $getGrowerPriceMeth2);
																$GrowerPriceMeth2 = mysqli_fetch_assoc($getGrowerPriceMeth2_array);
																//$a = $GrowerPriceMeth2['price'];
																$priceGrower = sprintf('%.2f',round($GrowerPriceMeth2['price'],2));
															}

  	return $priceGrower;
}


function calculateCost_Transp($growerid,$productid,$sizeid,$idsc,$con,$userSessionID){

	$tasaKilo = calculateKilo($userSessionID,$con);

   $getGrowerPriceMeth1 = "select gp.price as price
															from growcard_prod_price gp
															INNER JOIN growers g ON gp.growerid = g.id
															INNER JOIN product p ON gp.productid = p.id
															INNER JOIN subcategory s ON p.subcategoryid = s.id
															INNER JOIN colors c on p.color_id = c.id
															where g.active = 'active'
															and gp.growerid = '$growerid'
															and gp.productid = '$productid'
															and gp.sizeid = '$sizeid'";
  $getGrowerPriceMeth1_array = mysqli_query($con, $getGrowerPriceMeth1);
  $GrowerPriceMeth1 = mysqli_fetch_assoc($getGrowerPriceMeth1_array);
  $priceGrower = $GrowerPriceMeth1['price'];
		if($priceGrower<=0){
			$getGrowerPriceMeth2 = "select gp.id,gp.price_adm as price,
			s.name as sizename , gp.feature as feature, gp.factor,
			gp.stem_bunch , b.name as stems
			from grower_parameter gp
			inner JOIN sizes s ON gp.size = s.id
			inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
			left JOIN features f ON gp.feature = f.id
			where gp.idsc = '$idsc'
			and gp.size = '$sizeid'";
			$getGrowerPriceMeth2_array = mysqli_query($con, $getGrowerPriceMeth2);
			$GrowerPriceMeth2 = mysqli_fetch_assoc($getGrowerPriceMeth2_array);

			$priceBunch = $tasaKilo * $GrowerPriceMeth2['factor'];
		 	$priceSteam = $priceBunch / $GrowerPriceMeth2['stems'];
			//$a = $GrowerPriceMeth2['price'];
			$priceGrower = sprintf('%.2f',round($priceSteam,2));
		}

  	return $priceGrower;
}



function calculatePriceCliente($growerid,$productid,$sizeid,$idsc,$userSessionID,$con){
	$tasaKilo = calculateKilo($userSessionID,$con);

							 $sql_price = "select gp.id,gp.price_adm as price,
							s.name as sizename , gp.feature as feature, gp.factor,
							gp.stem_bunch , b.name as stems
							from grower_parameter gp
							inner JOIN sizes s ON gp.size = s.id
							inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
							left JOIN features f ON gp.feature = f.id
							where gp.idsc = '$idsc'
							and gp.size = '$sizeid'";

										$rs_price = mysqli_query($con,$sql_price);
										$row_price = mysqli_fetch_array($rs_price);
										$priceBunch = $tasaKilo * $row_price['factor'];
										$priceSteam = $priceBunch / $row_price['stems'];
										$priceCalculado = sprintf('%.2f',round($priceSteam + $row_price['price'],2));
										return $priceCalculado;
}

function detailsOrder($orderId,$con){
$selecDetailsOrder = "select order_number, del_date from buyer_orders where id = '$orderId'";
$rs_DetailOrder = mysqli_query($con,$selecDetailsOrder);
$row_DetailOrder = mysqli_fetch_array($rs_DetailOrder);

return 	" Order: <strong>".$row_DetailOrder['order_number']."</strong> 	Arrival date: <strong>".$row_DetailOrder['del_date']."</strong>";
}

function query_main($init,$display,$growerID,$fcat,$fcolor,$fsize,$ffeat,$fproductID){
$posicion_coincidencia = strpos($init, ',');
//se puede hacer la comparacion con 'false' o 'true' y los comparadores '===' o '!=='
if ($posicion_coincidencia === false)
{
      if($fcat!=''){
        $filterCat = "and p.subcategoryid = '$fcat'";
      }else{ $filterCat ='';}

      if($fproductID!=''){
				$filterProd2 = ", sz.id as siId, sz.name as NSsize";
				$filterProd1 = " LEFT JOIN growcard_prod_bunch_sizes  gr ON(gr.grower_id=gp.grower_id AND gr.product_id=gp.product_id) LEFT JOIN sizes sz ON gr.sizes = sz.id";
        $filterProd = "and p.id = '$fproductID'";
				$filterProd1_1 = ",sz.name ";
      }else{ $filterProd ='';}

      if($fcolor!=''){
        $fcolor = substr($fcolor, 0, -1);
        $filterColor = "and p.color_id in ($fcolor)";
        $filterColor1 = " INNER JOIN colors c on p.color_id = c.id ";
      }else{ $filterColor =''; $filterColor1 ='';}

      if($fsize!=''){
        $fsize = substr($fsize, 0, -1);
        $filterSizes = "and ps.sizes in ($fsize)";
        $filterSizes1 = " INNER JOIN growcard_prod_sizes ps ON (gp.grower_id = ps.grower_id and gp.product_id = ps.product_id) INNER join sizes sz on ps.sizes = sz.id ";
        $filterSizes2 = ", sz.id as siId";
      }else{ $filterSizes =''; $filterSizes1 =''; $filterSizes2 =''; }

      if($ffeat!=''){
        $ffeat = substr($ffeat, 0, -1);
        $filterFeatures = "and f.id IN ($ffeat)";
        $filterFeatures1 = " LEFT JOIN growcard_prod_features fe ON (gp.grower_id = fe.grower_id and gp.product_id = fe.product_id) LEFT JOIN features f on fe.features = f.id";
        $filterFeatures2 = ", f.name as fname, f.id as fid";
      }else{ $filterFeatures =''; $filterFeatures1 =''; $filterFeatures2 ='';}

    $query = "select  p.id as id, p.name as name, p.categoryid as categoryid, p.image_path as img,
    p.subcate_name as subcatename, p.subcategoryid as subcategoryid $filterProd2 $filterSizes2 $filterFeatures2
    from grower_product gp
    INNER JOIN growers g ON gp.grower_id = g.id
    INNER JOIN product p ON gp.product_id = p.id
    INNER JOIN subcategory s ON p.subcategoryid = s.id
    $filterColor1
    $filterSizes1
    $filterFeatures1
		$filterProd1
    where g.active = 'active'
    and p.status = 0
      and gp.grower_id = '$growerID'";

    $query = $query.' '.$filterCat.' '.$filterColor.' '.$filterSizes.' '.$filterFeatures.' '.$filterProd.   "  order by p.name $filterProd1_1 LIMIT $init,$display";

} else{

  if($fcat!=''){
    $filterCat = "and p.subcategoryid = '$fcat'";
  }else{ $filterCat ='';}

	if($fproductID!=''){
		$filterProd2 = ", sz.id, sz.name as namesize";
		$filterProd1 = " LEFT JOIN growcard_prod_bunch_sizes  gr ON(gr.grower_id=gp.grower_id AND gr.product_id=gp.product_id) LEFT JOIN sizes sz ON gr.sizes = sz.id";
		$filterProd = "and p.id = '$fproductID'";
		$filterProd1_1 = ",sz.name ";
	}else{ $filterProd ='';}

  if($fcolor!=''){
    $fcolor = substr($fcolor, 0, -1);
    $filterColor = "and p.color_id in ($fcolor)";
    $filterColor1 = " INNER JOIN colors c on p.color_id = c.id ";
  }else{ $filterColor =''; $filterColor1 ='';}

  if($fsize!=''){
    $fsize = substr($fsize, 0, -1);
    $filterSizes = "and ps.sizes in ($fsize)";
    $filterSizes1 = " INNER JOIN growcard_prod_sizes ps ON (gp.grower_id = ps.grower_id and gp.product_id = ps.product_id) INNER join sizes sz on ps.sizes = sz.id ";
    $filterSizes2 = ", sz.id as siId";
  }else{ $filterSizes =''; $filterSizes1 =''; $filterSizes2 =''; }

  if($ffeat!=''){
    $ffeat = substr($ffeat, 0, -1);
    $filterFeatures = "and f.id IN ($ffeat)";
    $filterFeatures1 = " LEFT JOIN growcard_prod_features fe ON (gp.grower_id = fe.grower_id and gp.product_id = fe.product_id) LEFT JOIN features f on fe.features = f.id";
    $filterFeatures2 = ", f.name as fname, f.id as fid";
  }else{ $filterFeatures =''; $filterFeatures1 =''; $filterFeatures2 ='';}

  $limites = explode(",",$init);
  $Linicio = $limites[0];
  $Lfin = $limites[1];

  if($Lfin==0){
    $Lfin=100;
  }else{
      $Lfin = $limites[1]- $limites[0];
  }

	$query = "select  p.id as id, p.name as name, p.categoryid as categoryid, p.image_path as img,
	p.subcate_name as subcatename, p.subcategoryid as subcategoryid $filterProd2 $filterSizes2 $filterFeatures2
	from grower_product gp
	INNER JOIN growers g ON gp.grower_id = g.id
	INNER JOIN product p ON gp.product_id = p.id
	INNER JOIN subcategory s ON p.subcategoryid = s.id
	$filterColor1
	$filterSizes1
	$filterFeatures1
	$filterProd1
	where g.active = 'active'
	and p.status = 0
		and gp.grower_id = '$growerID'";

  $query = $query.' '.$filterCat.' '.$filterColor.' '.$filterSizes.' '.$filterFeatures.' '.$filterProd.   "  order by p.name $filterProd1_1 LIMIT $Linicio,$Lfin";
}
    return $query;
}

function numberRecord($con,$growerID,$fcat,$fcolor,$fsize,$ffeat,$fproductID){

  if($fcat!=''){
    $filterCat = "and p.subcategoryid = '$fcat'";
  }else{ $filterCat ='';}

	if($fproductID!=''){
		$filterProd2 = ", sz.id, sz.name as namesize";
		$filterProd1 = " LEFT JOIN growcard_prod_bunch_sizes  gr ON(gr.grower_id=gp.grower_id AND gr.product_id=gp.product_id) LEFT JOIN sizes sz ON gr.sizes = sz.id";
		$filterProd = "and p.id = '$fproductID'";
		$filterProd1_1 = ",sz.name ";
	}else{ $filterProd ='';}

  if($fcolor!=''){
    $fcolor = substr($fcolor, 0, -1);
    $filterColor = "and p.color_id in ($fcolor)";
    $filterColor1 = " INNER JOIN colors c on p.color_id = c.id ";
  }else{ $filterColor =''; $filterColor1 ='';}

  if($fsize!=''){
    $fsize = substr($fsize, 0, -1);
    $filterSizes = "and ps.sizes in ($fsize)";
    $filterSizes1 = " INNER JOIN growcard_prod_sizes ps ON (gp.grower_id = ps.grower_id and gp.product_id = ps.product_id) INNER join sizes sz on ps.sizes = sz.id ";
    $filterSizes2 = ", sz.id as siId";
  }else{ $filterSizes =''; $filterSizes1 =''; $filterSizes2 =''; }

  if($ffeat!=''){
    $ffeat = substr($ffeat, 0, -1);
    $filterFeatures = "and f.id IN ($ffeat)";
    $filterFeatures1 = " LEFT JOIN growcard_prod_features fe ON (gp.grower_id = fe.grower_id and gp.product_id = fe.product_id) LEFT JOIN features f on fe.features = f.id";
    $filterFeatures2 = ", f.name as fname, f.id as fid";
  }else{ $filterFeatures =''; $filterFeatures1 =''; $filterFeatures2 ='';}

        $sel_pagina = "select  p.id as id, p.name as name, p.categoryid as categoryid, p.image_path as img,
				p.subcate_name as subcatename, p.subcategoryid as subcategoryid $filterProd2 $filterSizes2 $filterFeatures2
				from grower_product gp
				INNER JOIN growers g ON gp.grower_id = g.id
				INNER JOIN product p ON gp.product_id = p.id
				INNER JOIN subcategory s ON p.subcategoryid = s.id
				$filterColor1
				$filterSizes1
				$filterFeatures1
				$filterProd1
				where g.active = 'active'
				and p.status = 0
					and gp.grower_id = '$growerID'";

      $sel_pagina1 = $sel_pagina.' '.$filterCat.' '.$filterColor.' '.$filterSizes.' '.$filterFeatures.' '.$filterProd;

    $rs_pagina    = mysqli_query($con, $sel_pagina1);
    $total_pagina = mysqli_num_rows($rs_pagina);

   return $num_recordMB   = $total_pagina;
}

?>
