<?php
if(!$growerID_Prod){
	header("Location: ../en/variety-page.php");
	exit();
}
include('inc/header.php'); ?>
<style type="text/css">
	.portlet-body.max-h-500 {
    margin-left: 18px;
}
</style>
			<div id="wrapper_content" class="d-flex flex-fill">

				<?php include('inc/sidebar-menu.php'); ?>


				<!-- MIDDLE -->
				<div id="middle" class="flex-fill">

					<!--
						PAGE TITLE
					-->
					<div class="page-title bg-transparent b-0">

						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							Dashboard
						</h1>

					</div>






					<!-- WIDGETS -->
					<div class="row gutters-sm">
						<!-- WIDGET : TASKS -->
						<div class="col-12 col-xl-4 mb-3">
							<div class="portlet">

								<div class="portlet-header">
								<span class="d-block text-muted text-truncate font-weight-medium">
										Victoria's Blossom Imports
									</span>
								</div>

								<div  class="portlet-body max-h-500">
<div class="border-light">
										<div class="clearfix mb-2 py-1">
											<span class="float-start mt-1">
												<a href="#!" class="text-dark">Account Manager</a>
											</span>
										</div>
										<p class="fs--14 mb-2 pt-1">
Annie Martinez<br>
annie@freshlifefloral.com<br>
1-877-625-3243
										</p>

									</div>


								</div>

							</div>

						</div>
						<!-- /WIDGET : TASKS -->




						<!-- WIDGET : TASKS -->
						<div class="col-12 col-xl-4 mb-3">

							<div class="portlet">

								<div class="portlet-header">
								<span class="d-block text-muted text-truncate font-weight-medium">
										Shipping Information
									</span>
								</div>

								<div  class="portlet-body max-h-500">

<div class="border-bottom border-light">
										<div class="clearfix mb-2 py-1">
											<span class="float-start mt-1">
												<a href="#!" class="text-dark">Address</a>
											</span>
										</div>
										<p class="fs--14 mb-2 pt-1">
West Coast Floral Growers & Distr<br>
1420 172nd St., <br>
Surrey, BC V3S 9M6<br>
Canada
										</p>

									</div>
<div class="border-light">
										<div class="clearfix mb-2 py-1">
											<span class="float-start mt-1">
												<a href="#!" class="text-dark">Shipping Instructions</a>
											</span>
										</div>
										<p class="fs--14 mb-2 pt-1">
Victoria's Blossom<br>
Door-to-Door Service<br>
Fees: N/A
										</p>

									</div>


								</div>

							</div>

						</div>
						<!-- /WIDGET : TASKS -->
						<div class="col-12 col-xl-4 mb-3">

							<div class="portlet">

<div class="portlet-header">

									<div class="float-end">

<button style="margin-right: 10px;" class="dropdown-toggle btn btn-sm btn-soft btn-primary px-2 py-1 fs--15 mt--n3" data-toggle="modal" data-target="#billing_modal" onclick="return false;">Billing History</button>
<button class="dropdown-toggle btn btn-sm btn-soft btn-primary px-2 py-1 fs--15 mt--n3" data-toggle="modal" data-target="#card_modal" onclick="return false;">Buy credits</button>



									</div>


									<span class="d-block text-muted text-truncate font-weight-medium">
										Account Information
									</span>

								</div>

								<div  class="portlet-body max-h-500">


<div class="border-bottom border-light">
										<p class="fs--14 mb-2 pt-1">
Purchase Limit: $500<br>
Available: $500
										</p>

									</div>
<div class="border-light">
										<div class="clearfix mb-2 py-1">
											<span class="float-start mt-1">
												<a href="#!" class="text-dark">Billing Information</a>
											</span>
										</div>
										<p class="fs--14 mb-2 pt-1">
Victoria's Blossom Imports<br>
820 Ashbury Ave<br>
Victoria BC V9B 0A1 Canada<br>
sales@victoriasblossom.ca
										</p>

									</div>


								</div>

							</div>

						</div>






					</div>
					<!-- /WIDGETS -->


				</div>
				<!-- /MIDDLE -->

			</div><!-- FOOTER -->
<!-- Card Modal -->
<div class="modal fade" id="card_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
		<div class="modal-dialog modal-md modal-lg" role="document">
				<div class="modal-content">

						<!-- Header -->
						<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabelMd">Payment</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span class="fi fi-close fs--18" aria-hidden="true"></span>
								</button>
						</div>

						<!-- Content -->
						<div class="modal-body">
								<div class="form-advanced-list p-4 shadow-xs border rounded mb-5">










									<label class="form-radio form-radio-primary d-block py-3 border-bottom">Credit Card

										<span class="float-end mt--n3 ml--n10 mr--n10">
											<img src="assets/images/credit_card/visa.svg" width="38" height="24" alt="cc">
											<img src="assets/images/credit_card/mastercard.svg" width="38" height="24" alt="cc">
											<span class="fs--11 d-block text-align-end">and more...</span>
										</span>

									</label>

									<!-- CREDIT CARD FORM -->
									<div id="payment_card_form" class="form-advanced-list-reveal-item bg-gradient-light px-4 pt-4 rounded mt--n10 border bt-0">

										<div class="row">

											<div class="col-12 pl--5 pr--5 mb-3">

												<div class="input-group-over">
													<input type="hidden" name="cc_type" id="cc_type" value="">

													<div class="form-label-group">
														<input placeholder="Card Number" id="cc_number" type="text" data-card-type="#cc_type" class="form-control cc-format cc-number">
														<label for="cc_number">Card Number</label>
													</div>


													<span class="px-3 text-muted">
														<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="22px" height="22px" viewBox="0 0 47 47" xml:space="preserve">
															<path fill="#bbbebf" d="M23.498,10.141c-2.094,0-3.796,1.709-3.796,3.807v3.706h7.596v-3.706C27.298,11.851,25.593,10.141,23.498,10.141z"></path>
															<path fill="#bbbebf" d="M3.431,0v23.928C3.431,36.67,23.5,47,23.5,47s20.069-10.33,20.069-23.072V0H3.431z M34.351,30.148c0,1.571-1.286,2.856-2.855,2.856h-15.99c-1.57,0-2.855-1.285-2.855-2.856v-9.64c0-1.569,1.286-2.855,2.855-2.855h0.387v-3.706c0-4.198,3.413-7.613,7.606-7.613c4.195,0,7.609,3.415,7.609,7.613v3.706h0.388c1.57,0,2.855,1.286,2.855,2.855V30.148z"></path>
														</svg>
													</span>
												</div>

											</div>

											<div class="col-12 col-md-5 pl--5 pr--5 mb-2">

												<div class="form-label-group">
													<input placeholder="Cardholder Name" id="cc_name" type="text" value="" class="form-control">
													<label for="cc_name">Cardholder Name</label>
												</div>

											</div>

											<div class="col-6 col-md-3 pl--5 pr--5 mb-4">

												<div class="form-label-group">
													<input placeholder="MM / YY" id="cc_date" type="text" value="" class="form-control cc-format cc-expire" maxlength="5">
													<label for="cc_date">MM / YY</label>
												</div>

											</div>

											<div class="col-6 col-md-4 pl--5 pr--5 mb-4">

												<div class="form-fancy form-fancy-input mb--6">

													<div class="input-group-over">

														<div class="form-label-group">
															<input placeholder="CVV" id="cc_cvv" type="text" value="" class="form-control cc-format cc-cvc">
															<label for="cc_cvv">CVV</label>
														</div>

														<span class="px-3 text-muted" title="" data-html="true" data-toggle="tooltip" data-original-title="<span class='fs--12'>3-digit security code on the back of your card.<br><span class='fs--11'>(amex = 4-digit code on the front)</</span>">
															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.999 511.999" xml:space="preserve" width="22px" height="22px">
																<path fill="#bbbebf" d="M256,0C134.684,0,36,96.243,36,215.851c0,68.63,33.021,132.639,88.879,173.334V492c0,7.938,4.694,15.124,11.962,18.313c7.225,3.173,15.704,1.809,21.577-3.593l82.144-75.555c5.17,0.355,10.335,0.535,15.438,0.535c121.316,0,220-96.243,220-215.851C476,96.13,377.21,0,256,0z M256,391.701c-6.687,0-13.516-0.376-20.298-1.118c-5.737-0.634-11.466,1.253-15.715,5.161    l-55.108,50.687v-67.726c0-6.741-3.396-13.029-9.034-16.726C105.848,329.2,76,274.573,76,215.851C76,118.886,156.747,40,256,40    s180,78.886,180,175.851C436,312.814,355.252,391.701,256,391.701z"></path>
																<path fill="#bbbebf" d="M266,98.877h-70c-11.046,0-20,8.954-20,20c0,11.046,8.954,20,20,20h70c16.542,0,30,13.458,30,30c0,16.542-13.458,30-30,30h-20c-11.046,0-20,8.954-20,20v28.793c0,11.046,8.954,20,20,20s20-8.954,20-20v-8.793c38.598,0,70-31.401,70-70C336,130.279,304.598,98.877,266,98.877z"></path>
																<circle fill="#bbbebf" cx="246" cy="319.16" r="27"></circle>
															</svg>
														</span>

													</div>

												</div>

											</div>
											<div class="col-12 col-md-5 pl--5 pr--5 mb-2">

												<div class="form-label-group">
													<input placeholder="Dollar Amount ($)" id="cc_name" type="text" value="" class="form-control">
													<label for="cc_name">Dollar Amount</label>
												</div>

											</div>
											<div class="col-12 col-md-5 pl--5 pr--5 mb-2">

												<div class="form-label-group">
													<button type="button" class="btn btn-success">
										Place Your Order
								</button>
												</div>

											</div>

										</div>
									</div>
									<!-- /CREDIT CARD FORM -->


								</div>
						</div>

						<!-- Footer -->
						<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
										<i class="fi fi-close"></i>
										Close
								</button>
						</div>

				</div>
		</div>
</div>
<!-- /card Modal -->

<!-- Billing Modal -->
<div class="modal fade" id="billing_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelMd" aria-hidden="true">
		<div class="modal-dialog modal-md modal-xl" role="document">
				<div class="modal-content">

						<!-- Header -->
						<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabelMd">History</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span class="fi fi-close fs--18" aria-hidden="true"></span>
								</button>
						</div>

						<!-- Content -->
						<div class="modal-body">
							<table class="table-datatable table table-bordered table-hover table-striped"
											data-lng-empty="No data available in table"
											data-lng-page-info="Showing _START_ to _END_ of _TOTAL_ entries"
											data-lng-filtered="(filtered from _MAX_ total entries)"
											data-lng-loading="Loading..."
											data-lng-processing="Processing..."
											data-lng-search="Search..."
											data-lng-norecords="No matching records found"
											data-lng-sort-ascending=": activate to sort column ascending"
											data-lng-sort-descending=": activate to sort column descending"

											data-lng-column-visibility="Column Visibility"
											data-lng-csv="CSV"
											data-lng-pdf="PDF"
											data-lng-xls="XLS"
											data-lng-copy="Copy"
											data-lng-print="Print"
											data-lng-all="All"

											data-main-search="true"
											data-column-search="false"
											data-row-reorder="false"
											data-col-reorder="true"
											data-responsive="true"
											data-header-fixed="true"
											data-select-onclick="true"
											data-enable-paging="true"
											data-enable-col-sorting="true"
											data-autofill="false"
											data-group="false"
											data-items-per-page="10"

											data-lng-export="<i class='fi fi-squared-dots fs--18 line-height-1'></i>"
											dara-export-pdf-disable-mobile="true"
											data-export='["csv", "pdf", "xls"]'
											data-options='["copy", "print"]'
										>
											<thead>
												<tr>
													<th>Date</th>
													<th>Description</th>
													<th>Type</th>
													<th>Status</th>
													<th>Amount</th>
													<th>Available Balance</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>
												<tr>
													<td>09/16/2020</td>
													<td>Standard Rose Freedom 50cm 25st/bu 150 stems</td>
													<td>Payment</td>
													<td>In Process</td>
													<td>$234.56</td>
													<td>$1322.44</td>
												</tr>

											</tbody>
											<tfoot>
												<tr>
													<th>Date</th>
													<th>Description</th>
													<th>Type</th>
													<th>Status</th>
													<th>Amount</th>
													<th>Available Balance</th>
												</tr>
											</tfoot>
										</table>
						</div>

						<!-- Footer -->
						<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">
										<i class="fi fi-close"></i>
										Close
								</button>
						</div>

				</div>
		</div>
</div>
<!-- Billing Modal -->
<?php include('inc/footer.php'); ?>
