<aside class="aside-start aside-primary aside-hide-xs d-flex flex-column h-auto" id="aside-main">
					<!--
                        LOGO
                        visibility : desktop only
                    -->
					<div class="d-none d-sm-block">
						<div class="clearfix d-flex justify-content-between">
							<!-- Logo : height: 60px max -->
							<a class="align-self-center navbar-brand p-3" href="index.html"><img alt="..." src="images/logo.jpeg" width="100%" style="margin-bottom: 30px;"></a>
						</div>
					</div><!-- /LOGO -->
					<div class="aside-wrapper scrollable-vertical scrollable-styled-light align-self-baseline h-100 w-100">
						<!--

                            All parent open navs are closed on click!
                            To ignore this feature, add .js-ignore to .nav-deep

                            Links height (paddings):
                                .nav-deep-xs
                                .nav-deep-sm
                                .nav-deep-md    (default, ununsed class)

                            .nav-deep-hover     hover background slightly different
                            .nav-deep-bordered  bordered links


                            ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            IMPORTANT NOTE:
                                Curently using ajax navigation!
                                remove .js-ajax class to have regular links!
                            ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        -->
						<nav class="nav-deep nav-deep-dark nav-deep-hover pb-5">
							<ul class="nav flex-column">
								<li class="nav-item active">
									<a class="nav-link" href="#"><i class="fi fi-menu-dots"></i> <b>Dashboard</b></a>
									<!-- <a class="nav-link" href="dashboard.php"><i class="fi fi-menu-dots"></i> <b>Dashboard</b></a> -->
								</li>
								<!--
								<li class="nav-item">
									<a class="nav-link" href="#"><span class="group-icon float-end"><i class="fi fi-arrow-end-slim"></i> <i class="fi fi-arrow-down-slim"></i></span> <i class="fi fi-box"></i> Buy Flowers</a>
									<ul class="nav flex-column">
										<li class="nav-item">
											<a class="nav-link" href="marketplace.php">Marketplace</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="open-request.php">Open Request</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="index.php">Grower Request</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="mix-box.php">Mix Box</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="flower-market.php">Flower Market</a>
										</li>
									</ul>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="orders.php"><i class="fi fi-squared-graph"></i> <b>Orders</b></a>
								</li>
							-->
							</ul>
						</nav>
					</div>
				</aside><!-- /SIDEBAR -->
