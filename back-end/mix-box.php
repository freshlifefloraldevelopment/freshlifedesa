<?php
session_start();
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 06 Abril 2021
Structure MarketPlace
**/
include('../config/config_gcp.php');
include('GlobalFSyn.php');
//445 341
$_SESSION["buyer"];
$_SESSION["order_prev"] = $_SESSION['orderSelected'];
$userSessionID = $_SESSION["buyer"];
$order_prevID = $_SESSION["order_prev"];

$display = 20;
$XX = '<div class="notfound" style="margin-top:30px;">No Item Found !</div>';

if(isset($_POST["newbox"]) && isset($_POST["grower_id"]) && $_POST["grower_id"]!='' && $_POST["newbox"]!='')
{
//if($_POST["newbox"] == ""){
//$newOrderFilter = $_POST["newbox"];

$newOrderFilter = $_POST['newbox'];
$growerID_Prod = $_POST['grower_id'];
$_SESSION["GrwID"] = $growerID_Prod;
$_SESSION["newOrderF"] = $newOrderFilter;

if($userSessionID)
{
  if($order_prevID)
  {
    if(!$growerID_Prod){
      header("Location: ../en/variety-page.php");
      exit();
    }
  }
  else
  {
    header("Location: ../buyer/buyers-account.php?menu=1");
    exit();
  }
}
else
{
    header("Location: ../buyer/buyers-account.php?menu=1");
    exit();
  }

}








if (isset($_POST["startrowMB"]) && $_POST["startrowMB"] != "") {
  //    $sr = $_POST["startrowMB"] + 1;
  $growerID_Prod = $_SESSION["GrwID"];
  $newOrderFilter = $_SESSION["newOrderF"];

    $filtroCheck = $_POST["startrowMB"];

      if($filtroCheck[0]=='C'){
        $_SESSION["FCat"] = substr($filtroCheck,1);
        $_POST["startrowMB"] = 0;
      }else{
        if($filtroCheck[0]=='P'){
          $_POST["startrowMB"] = substr($filtroCheck,1);
        }else
        {
            if($filtroCheck[0]=='Y')
            {
              $_POST["startrowMB"] = substr($filtroCheck,1);
            }
            else
            {
              $porcionesFiltros = explode("**FLF**", $filtroCheck);

              $filtroColors = $porcionesFiltros[0];
              $filtroSizes = $porcionesFiltros[1];
              $filtroFeatures = $porcionesFiltros[2];

                    if($filtroColors[0]==','){
                      $_SESSION["FColor"] = substr($filtroColors,1);
                    }else{ $_SESSION["FColor"] = $filtroColors; }

                    if($filtroSizes[0]==','){
                      $_SESSION["FSize"] = substr($filtroSizes,1);
                    }
                    if($filtroFeatures[0]==','){
                      $_SESSION["FFeat"] = substr($filtroFeatures,1);
                    }
                    $_POST["startrowMB"] = 0;
            }
        }

      }



     $num_recordMB = numberRecord($con,$growerID_Prod,$_SESSION["FCat"],$_SESSION["FColor"],$_SESSION['FSize'],$_SESSION["FFeat"],'');
     $query2 = query_main($_POST["startrowMB"], $display,$growerID_Prod,$_SESSION['FCat'],$_SESSION['FColor'],$_SESSION['FSize'],$_SESSION['FFeat'],'');
     $sql_products = mysqli_query($con, $query2);
  }
  else
  {
    $growerID_Prod = $_SESSION["GrwID"];
    $newOrderFilter = $_SESSION["newOrderF"];
    //if(isset($_GET["sid"]) && $_GET["sid"] != "")
    if(isset($_GET["sid"]) && isset($_GET["pid"]) && $_GET["pid"] != "" && $_GET["sid"] != "")
    {

      $_SESSION['FCat']='';
      $_SESSION['FColor']='';
      $_SESSION['FSize']='';
      $_SESSION['FFeat']='';

      $inicio = 0;

       $num_recordMB = numberRecord($con,$growerID_Prod,$_GET["sid"],$_SESSION["FColor"],$_SESSION['FSize'],$_SESSION["FFeat"],$_GET["pid"]);
      $query2 = query_main($inicio, $display,$growerID_Prod,$_GET["sid"],$_SESSION['FColor'],$_SESSION['FSize'],$_SESSION['FFeat'],$_GET["pid"]);
     $sql_products = mysqli_query($con, $query2);


   }else
   {

     if($_SESSION["GrwID"]==''){
       header("Location: ../en/variety-page.php");
       exit();
     }
        if (empty($startrow))
        {
            $startrow = 0;
            $sr = 1;
        }
        //Inicialización de filtros globales
        $_SESSION['FCat']='';
        $_SESSION['FColor']='';
        $_SESSION['FSize']='';
        $_SESSION['FFeat']='';
        //
         $num_recordMB = numberRecord($con,$growerID_Prod,$_SESSION['FCat'],$_SESSION['FColor'],$_SESSION['FSize'],$_SESSION["FFeat"],'');
         $query2 = query_main(0, $display,$growerID_Prod,$_SESSION['FCat'],$_SESSION['FColor'],$_SESSION['FSize'],$_SESSION['FFeat'],'');
         $sql_products = mysqli_query($con, $query2);
    }
}

include('inc/header.php');

?>

<style type="text/css">
.select_options .form-control-sm {
    padding: 0px!important;
}
.w--140{
	width: 140px!important;
}
li.list-inline-item.mx-1.dropdown {
    width: 55px;
    position: absolute;
    right: 30px;
    top: 27px;
}
.wp-95{
	width: 93%;
}
@media(max-width: 800px)
{
	.wp-95{
	width: 83%;
}
li.list-inline-item.mx-1.dropdown {
    width: 50px;
    position: absolute;
    right: 4px;
    top: 15px;
}
}
</style>

			<div class="d-flex flex-fill" id="wrapper_content">



<?php include('inc/sidebar-menu.php'); ?>
				<!-- MIDDLE -->

				<div class="flex-fill" id="middle">
					<div class="page-title bg-transparent b-0">
						<h1 class="h4 mt-4 mb-0 px-3 font-weight-normal">
							Mix Box

						</h1>
					</div><!-- Primary-->
					<section class="rounded mb-3 bg-white" id="section_1">



            <input type="hidden" id="growerId" value="<?php echo ','.$growerID_Prod; ?>" />
            <input type="hidden" id="newBoxMixId" value="<?php echo $newOrderFilter; ?>" />
            <input type="hidden" id="CatId" value="<?php echo ','.$_SESSION['FCat']; ?>" />
            <input type="hidden" id="ColorId" value="<?php echo ','.$_SESSION["FColor"]; ?>" />
            <input type="hidden" id="SizeId" value="<?php echo ','.$_SESSION['FSize']; ?>" />
            <input type="hidden" id="FeatureId" value="<?php echo ','.$_SESSION['FFeat']; ?>" />
            <input type="hidden" id="ordenPrevId" value="<?php echo $order_prevID; ?>" />


						<!-- graph header -->
						<div class="clearfix fs--18 pt-2 pb-3 mb-3 border-bottom">

              <button class="btn btn-sm btn-block btn-danger bg-gradient-success text-white b-0" id="bback" onclick="Javascript:Goback();">
                <span class="p-0-xs">
                  <span class="fs--14"><i class="fa fa-arrow-left" aria-hidden="true"></i> Go Back</span>
                </span>
              </button>
            <div class="page-title bg-transparent b-0">
              <h1 class="h1 mt-4 mb-0 font-weight-normal"><font color="#990099"><?php echo getNameGrower($con, $growerID_Prod); ?></font></h1>
              <?php echo detailsOrder($order_prevID,$con); ?>
              <a href="#" id="expandedSection" class="btn-toggle" data-toggle-container-class="fullscreen" data-toggle-body-class="overflow-hidden" data-target="#section_1" style="position: absolute;right: 22px;">
                <span class="group-icon">
                  <i class="fi fi-expand"></i>
                  <i class="fi fi-shrink"></i>
                </span>
              </a>
            </div>

							<!-- fullscreen -->

						</div>
						<div class="card fs--18 pt-2 pb-3 bg-white shadow-md rounded mb-3 d-flex b-0 p--20">
	             <form 	action="#"
							method="GET"
							data-autosuggest="on"
							data-mode="json"
							data-json-max-results='10'
							data-json-related-title='Search Varieties'
							data-json-related-item-icon='fi fi-star-empty'
							data-json-suggest-title='Suggestions for you'
							data-json-suggest-noresult='No results for'
							data-json-suggest-item-icon='fi fi-search'
							data-json-suggest-min-score='5'
							data-json-highlight-term='true'
							data-contentType='application/json; charset=utf-8'
							data-dataType='json'
							data-container="#sow-search-container"
							data-input-min-length="2"
							data-input-delay="100"
							data-related-keywords=""
							data-related-url="demo.ajax_request.php"
							data-suggest-url="demo.ajax_request.php"
							data-related-action="related_get"
							data-suggest-action="suggest_get"

							class="js-ajax-search sow-search sow-search-mobile-float d-flex-1-1-auto mx-4">
						<div class="sow-search-input w-100 d-flex align-items-center">

							<div class="input-group-over d-flex align-items-center w-100 h-100 rounded">

								<input placeholder="Search Varieties..." name="s" id="sr" type="text" class="form-control-sow-search form-control form-control-pill b-0 bg-gray-100" value="" autocomplete="off">

								<span class="sow-search-buttons">

									<!-- search button -->
									<button type="submit" class="btn btn-primary btn-noshadow m-0 px-2 py-1 b-0 bg-transparent text-muted">
										<i class="fi fi-search fs--20"></i>
									</button>

									<!-- close : mobile only (d-inline-block d-lg-none) -->
									<a href="javascript:;" class="btn-sow-search-toggler btn btn-light btn-noshadow m-0 px-2 py-1 d-inline-block d-lg-none">
										<i class="fi fi-close fs--20"></i>
									</a>

								</span>

							</div>

						</div>

						<!-- search suggestion container -->
						<div class="sow-search-container rounded-xl w-100 p-0 hide shadow-md" id="sow-search-container">
							<div class="sow-search-container-wrapper rounded-xl">

								<!-- main search container -->
								<div class="sow-search-loader p--15 text-center hide">
									<i class="fi fi-circle-spin fi-spin text-muted fs--30"></i>
								</div>

								<!--
									AJAX CONTENT CONTAINER
									SHOULD ALWAYS BE AS IT IS : NO COMMENTS OR EVEN SPACES!
								--><div class="sow-search-content rounded w-100 scrollable-vertical"></div>

							</div>
						</div>
						<!-- /search suggestion container -->

						<!--

							overlay combinations:
								overlay-dark opacity-* [1-9]
								overlay-light opacity-* [1-9]

						-->
						<div class="sow-search-backdrop overlay-dark opacity-3 hide"></div>

					</form>
					<!-- /SEARCH -->
						</div>


<div class="">


					<nav class="navbar navbar-expand-lg navbar-light justify-content-lg-between justify-content-md-inherit">




						<!--Progress Bar-->
<div class="card fs--18 pt-2 pb-3 bg-white shadow-md rounded mb-3 d-flex b-0 p--40 w-100">
										<div class="progress ph-35 wp-95" id='barP'>

										</div>
<li class="list-inline-item mx-1 dropdown">

								<a href="#" aria-label="My Cart" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true" class="d-inline-block text-center text-dark js-stoppropag">
									<span id="countPreviousBoxMix" class="badge badge-danger position-absolute end-0 mt--n5">0</span>
									<i class="fas fa-box fs--20"></i>
									<span class="d-block font-weight-light fs--14">my box</span>
								</a>

								<!-- dropdown -->
								<div aria-labelledby="dropdownAccount" id="dropdownAccount" class="dropdown-menu dropdown-menu-clean dropdown-menu-invert dropdown-click-ignore mt--18 w--300 p-0">
									<div class="p-3">Cart Products</div>

									<!--
									<div class="pt-5 pb-5 text-center bg-light">
										Your cart is empty!
									</div>
									-->


									<!-- item list -->
									<div class="max-h-50vh scrollable-vertical" id="loadDataItemsPrevious">


									</div>
									<!-- /item list -->

									<!-- subtotal -->
									<div class="fs--14 text-align-start border-top px-3 py-2" id="loadDataPricesSummary">

									</div>

									<!-- go to cart button -->
									<div class="clearfix border-top p-3">


										<a onclick="Javascript:endMixBoxB()" id="endMixBox" class="btn btn-sm btn-block btn-success bg-gradient-success text-white b-0">
											<span>End MixBox</span>
											<i class="fi fi-arrow-end fs--12"></i>
										</a>

									</div>


								</div>


							</li>

</div>
<!--Progress Bar-->






					</nav>

				</div>






						<div class="row gutters-sm">
							<div class="col-12 col-lg-3 col-xl-3 mb-5">
								<!-- CATEGORIES -->
								<nav class="card nav-deep nav-deep-light mb-3 b-0 px-4 pb-3 p-0-md p-0-xs shadow-xs rounded">
									<!-- mobile trigger : categories -->
									<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none" data-target="#nav_responsive" data-toggle-container-class=
									"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3"><span class="group-icon px-2 py-2 float-start"><i class="fi fi-bars-2"></i> <i class="fi fi-close"></i></span>
									<span class="h5 py-2 m-0 float-start">Categories</span></button> <!-- desktop only -->
									<h5 class="h6 pt-3 pb-3 m-0 d-none d-lg-block">
										Categories
									</h5><!-- navigation -->
									<ul class="nav flex-column d-none d-lg-block" id="nav_responsive">
                    <div id="categoryGrowerId">
                    </div>
					             </ul>
								</nav><!-- /CATEGORIES-->
								<!-- mobile trigger : filters -->
								<button class="clearfix btn btn-toggle btn-sm btn-block text-align-left shadow-md border rounded mb-1 d-block d-lg-none" data-target="#sidebar_filters" data-toggle-body-class=
								"overflow-hidden" data-toggle-container-class="d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen"><i class=
								"px-2 py-2 fs--15 float-start fi fi-eq-horizontal"></i> <span class="h5 py-2 m-0 float-start">Filters</span></button>
								<form class="d-none d-lg-block" id="sidebar_filters" name="sidebar_filters" onsubmit="return false;">
									<!-- MOBILE ONLY -->
									<div class="bg-white pb-3 mb-3 d-block d-lg-none border-bottom">
										<i class="fi fi-eq-horizontal float-start"></i> <span class="h5 m-0 d-inline-block">Filters</span> <!-- mobile : exit fullscreen -->
										 <a class="float-end btn-toggle text-dark mx-1" data-target="#sidebar_filters" data-toggle-body-class="overflow-hidden" data-toggle-container-class=
										"d-none d-sm-block bg-white shadow-md border animate-fadein rounded p-3 fullscreen" href="#"><i class="fi fi-close"></i></a>
									</div><!-- /MOBILE ONLY -->
									<!-- Reset Filters -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<a class="text-danger float-end w--20 d-inline" href="mix-box.php"><i class="fi fi-close"></i></a> Reset Filters
									</div><!-- /Reset Filters -->
									<!-- Price -->
									<!-- <div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<h3 class="fs--15 mb-3">
											<a class="form-advanced-reset hide-force text-danger float-end w--20 d-inline" data-target-reset="#filter_price_list" href="#"><i class="fi fi-close"></i></a> Price
										</h3>
										<div id="filter_price_list">
											<label class="form-radio form-radio-primary clearfix d-block"><input name="price" type="radio" value="10:100"> <i></i> $10 &ndash; $100</label> <label class=
											"form-radio form-radio-primary clearfix d-block"><input name="price" type="radio" value="100:300"> <i></i> $100 &ndash; $300</label> <label class=
											"form-radio form-radio-primary clearfix d-block"><input name="price" type="radio" value="300:500"> <i></i> $300 &ndash; $500</label> <label class=
											"form-radio form-radio-primary clearfix d-block"><input name="price" type="radio" value="500:1000"> <i></i> $500 &ndash; $1000</label> <label class=
											"form-radio form-radio-primary clearfix d-block"><input name="price" type="radio" value="1000:3000"> <i></i> $1000 &ndash; $3000</label> <small class=
											"text-muted d-block border-top mt-3 pt-3 mb-3">Custom price</small>
											<div class="row gutters-xs">
												<div class="col-5">
													<label class="d-block fs--13 mb-1">From</label> <input class="form-control form-control-sm" type="number" value="">
												</div>
												<div class="col-5">
													<label class="d-block fs--13 mb-1">To</label> <input class="form-control form-control-sm" type="number" value="">
												</div>
												<div class="col-2">
													<button class="btn btn-sm btn-block btn-light mt-4 px-2" type="submit"><i class="fi fi-arrow-end m-0"></i></button>
												</div>
											</div>
										</div>
									</div> --><!-- Color -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<h3 class="fs--15 mb-3">
											<a class="form-advanced-reset hide-force text-danger float-end w--20 d-inline" data-target-reset="#filter_color_list" href="#">
                        <i class="fi fi-close"></i></a>
                        Color
										</h3>
										<div id="filter_color_list">

										</div>
									</div><!-- Size -->
									<div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
										<h3 class="fs--15 mb-3">
											<a class="form-advanced-reset hide-force text-danger float-end w--20 d-inline" data-target-reset="#filter_size_list" href="#"><i class="fi fi-close"></i></a> Sizes
										</h3>
										<div id="filter_size_list">

										</div>
									</div>


                  <!-- Size -->
                  <div class="card rounded b-0 shadow-xs d-block mb-3 p-3">
                    <h3 class="fs--15 mb-3">
                      <a class="form-advanced-reset hide-force text-danger float-end w--20 d-inline" data-target-reset="#filter_size_list" href="#"><i class="fi fi-close"></i></a> Features
                    </h3>
                    <div id="filter_features_list">

                    </div>
                  </div>

									<!-- optional button -->
									<button class="btn btn-primary btn-soft btn-sm btn-block" onclick="funSearchPageFilter()" >Apply Filters</button>
								</form>
							</div><!-- MAIN GRAPH -->
							<div class="col-12 col-lg-9 col-xl-9 mb-5">
								<div class="shadow-xs bg-white mb-5 p-3 clearfix">
									<div class="container">



<?php //echo $num_recordMB." / ".$query2;?>
<input type="hidden" id="numRecords" value="<?php echo $num_recordMB ?>" />
										<form action="#" onsubmit="return false;" class="bs-validate" id="form_id" method="post" name="form_id" novalidate="">
											<!--

                                                IMPORTANT
                                                The "action" hidden input is updated by javascript according to button params/action:
                                                    data-js-form-advanced-hidden-action-id="#action"
                                                    data-js-form-advanced-hidden-action-value="delete"

                                                In your backend, should process data like this (PHP example):

                                                    if($_POST['action'] === 'delete') {

                                                        foreach($_POST['item_id'] as $item_id) {
                                                            // ... delete $item_id from database
                                                        }

                                                    }

                                            -->
											<input id="action" name="action" type="hidden" value=""><!-- value populated by js -->
											<div class="mt-4 text-center-xs">
												<div class="row">
													<div class="col-12 col-md-6 mt-4">
														<!-- SELECTED ITEMS -->
														<div class="clearfix">
															<!-- using .dropdown, autowidth not working -->
															<a aria-expanded="false" aria-haspopup="true" class="btn btn-sm btn-pill btn-light js-stoppropag" data-toggle="dropdown" href="#"><span class="group-icon"><i class=
															"fi fi-dots-vertical-full"></i> <i class="fi fi-close"></i></span> <span>Products per page</span></a>
															<div class="dropdown-menu dropdown-menu-clean dropdown-click-ignore max-w-250">
																<div class="scrollable-vertical max-h-50vh">
																	<a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="javascript:onclick=funSearchPage('1,20')"> 1 - 20</a>
                                  <a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="javascript:onclick=funSearchPage('20,30')">20 - 30</a>
                                  <a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="javascript:onclick=funSearchPage('30,40')">30- 40</a>
																	<a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="javascript:onclick=funSearchPage('40,50')">40 - 50</a>
																	<div class="dropdown-divider"></div>
                                  <a class="dropdown-item text-truncate js-form-advanced-bulk js-form-advancified" href="javascript:onclick=funSearchPage('50,0')">50 and up</a>
																</div>
															</div>
														</div><!-- /SELECTED ITEMS -->


													</div>
													<div class="col-12 col-md-6 mt-4">
                            <nav aria-label="pagination">
                              <ul class="pagination pagination-pill justify-content-end justify-content-center justify-content-md-end">
                                <?php
                                if ($_POST["startrowMB"] != 0) {

                                    $prevrow = $_POST["startrowMB"] - $display;

                                    print("<li class='page-item disabled btn-pill'><a aria-disabled='true' class='page-link' tabindex='-1' href=\"javascript:onclick=funMBPage($prevrow)\">Prev </a></li>");
                                }
                                $pages = intval($num_recordMB / $display);

                                if ($num_recordMB % $display) {

                                    $pages++;
                                }
                                $numofpages = $pages;
                                $cur_page = $_POST["startrowMB"] / $display;
                                $range = 5;
                                $range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2;
                                $range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min;
                                $page_min = $cur_page - $range_min;
                                $page_max = $cur_page + $range_max;
                                $page_min = ($page_min < 1) ? 1 : $page_min;
                                $page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max;
                                if ($page_max > $numofpages) {
                                    $page_min = ($page_min > 1) ? $numofpages - $range + 1 : 1;
                                    $page_max = $numofpages;
                                }
                                if ($pages > 1) {

                                    for ($i = $page_min; $i <= $page_max; $i++) {
                                        if ($cur_page + 1 == $i) {
                                            $nextrow = $display * ($i - 1);
                                            print("<li class='page-item active'><a class='page-link' href='javascript:void();'>$i</a></li>");
                                        } else {

                                            $nextrow = $display * ($i - 1);
                                            print("<li class='page-item'><a class='page-link' href=\"javascript:onclick=funMBPage($nextrow)\"> $i </a></li>");
                                        }
                                    }

                                }
                                if ($pages > 1) {

                                    if (!(($_POST["startrowMB"] / $display) == $pages - 1) && $pages != 1) {

                                        $nextrow = $_POST["startrowMB"] + $display;

                                        print("<li class='page-item'><a class='page-link' href=\"javascript:onclick=funMBPage($nextrow)\" class='page-item'> Next</a></li> ");
                                    }
                                }

                                if ($num_recordMB < 1) {
                                    print("<span class='text'>" . $XX . "</span>");
                                }
                                ?>


                              </ul>
                            </nav><!-- pagination -->
													</div>
												</div>
											</div><!-- /options and pagination -->






											<div class="table-responsive pd-15">



												<table class="table table-framed">
													<thead>
														<tr>
															<th></th>
															<th class="text-gray-500 font-weight-normal fs--14 w--300">
																VARIETY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--300">
																PRICE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--60 text-align-left">
																SELECT QTY/BUNCH
															</th>
														</tr>
													</thead><!-- #item_list used by checkall: data-checkall-container="#item_list" -->
													<tbody id="item_list">
														<!-- product -->
                            <?php
                            $idG = $growerID_Prod;

                            $sql_country_grower = "select g.growers_name, co.name as countryname
                            from growers g
                            left join country co on g.country_id = co.id
                            where g.active='active'
                            and g.id = '$idG'";
                            $rs_country_array = mysqli_query($con,$sql_country_grower);
                            $rs_country = mysqli_fetch_array($rs_country_array);
                            $country = $rs_country['countryname'];


                                  while($row_products = mysqli_fetch_array($sql_products))
                                  {
                                    $Syni = $Syni+1;
                                    $category_id_product = $row_products["categoryid"];
                                    $id_product = $row_products["id"];
                                    $name_product = $row_products["name"];
                                    $img_product = $row_products["img"];
                                    $subcategory_product = $row_products["subcatename"];
                                    $subcategoryid = $row_products["subcategoryid"];
                                    $sizeid = $row_products["siId"];
                                    $featureName = $row_products["fname"];
                                    $featureId = $row_products["fid"];

                                    if($featureId==''){
                                      $featureId= 0;
                                    }

                                    $sql_description_product = "select s.* , c.name as namecategory from grower_product gp
                                    inner join product p on gp.product_id = p.id
                                    left join subcategory s on p.subcategoryid = s.id left join category c on p.categoryid = c.id
                                    left join growers g on gp.grower_id = g.id
                                    where g.active='active' and p.status = 0
                                    and p.categoryid ='$category_id_product'
                                    and p.status = 0
                                    group by s.id order by s.name";

                                    $rs_descrip_product_array = mysqli_query($con,$sql_description_product);
                                    $rs_descrip_product = mysqli_fetch_array($rs_descrip_product_array);
                                    $description = $rs_descrip_product['namecategory'];

                                    // sql date arrive product //

                                    $sql_date_arrive_product = "select max(bo.del_date) as date
                                    from buyer_requests br
                                    INNER join grower_offer_reply gor on gor.offer_id = br.id
                                    INNER join product p on br.product = p.id
                                    INNER join buyer_orders bo ON br.id_order= bo.id
                                    where br.buyer = '$userSessionID'
                                    and br.product = '$id_product'";

                                    $rs_date_arrive_array = mysqli_query($con,$sql_date_arrive_product);
                                    $rs_date_arrive = mysqli_fetch_array($rs_date_arrive_array);
                                    $date_arrive = $rs_date_arrive['date'];

                                    // sql date arrive product //
                                    if($date_arrive==""){
                                      $date_arrive = "2021-04-12";
                                    }

                                    if($sizeid!=''){
                                    $sql_product_size = "select name as sizen
                                    from sizes
                                    WHERE id = '$sizeid'";
                                    $rs_product_size_array = mysqli_query($con,$sql_product_size);
                                    $rs_produc_size = mysqli_fetch_array($rs_product_size_array);
                                    $product_size = $rs_produc_size['sizen'];
                                  }
                                    else{

                                    $sql_product_size = "select gps.sizes as sizeid  , sz.name as sizen
                                    from growcard_prod_sizes gps
                                    INNER JOIN growers g ON gps.grower_id = g.id inner join sizes sz on gps.sizes = sz.id
                                    where g.active = 'active'
                                    and gps.grower_id ='$idG'
                                    and product_id = '$id_product'
                                    group by gps.sizes
                                    order by sz.name";

                                    $rs_product_size_array = mysqli_query($con,$sql_product_size);
                                    $rs_produc_size = mysqli_fetch_array($rs_product_size_array);
                                    $product_size = $rs_produc_size['sizen'];
                                    $sizeid  = $rs_produc_size['sizeid'];
                                    }




                                    $sql_product_box_bunch = "select gs.id, b.name as boxname , (bs.name *pac.qty) stems, bs.name as bunchna,
                                    b.width , b.length , b.height , ff.name as featurename,
                                    ((b.width*b.length*b.height)/6000) AS peso
                                    from growcard_prod_bunch_sizes gs
                                    left join growcard_prod_features gpf on gs.product_id=gpf.product_id and gs.grower_id=gpf.grower_id
                                    left join features ff on gpf.features=ff.id
                                    right join grower_product_box gpb on gs.product_id=gpb.product_id and gs.grower_id=gpb.grower_id
                                    left join product p on gs.product_id=p.id
                                    left join category pc on p.categoryid=pc.id
                                    left join subcategory s on p.subcategoryid=s.id
                                    left join colors c on p.color_id=c.id
                                    left join boxes b on gpb.boxes=b.id
                                    left join bunch_sizes bs on gs.bunch_sizes=bs.id
                                    left join sizes sh on gs.sizes=sh.id
                                    left join growcard_prod_box_packing pac on (gs.product_id = pac.prodcutid and gs.grower_id = pac.growerid and gs.sizes = pac.sizeid and b.id = pac.box_id and bs.id = pac.bunch_size_id)
                                    where gs.grower_id = '$idG'
                                    and gs.product_id = '$id_product'";

                                    $rs_product_box_bunch_array = mysqli_query($con,$sql_product_box_bunch);
                                    $rs_produc_box_bunch = mysqli_fetch_array($rs_product_box_bunch_array);
                                    $bunchname = $rs_produc_box_bunch['bunchna'];
                                    $peso = round($rs_produc_box_bunch['peso'],2);

                                    $growerPrice = calculatePriceGrower($idG,$id_product,$sizeid,$subcategoryid,$con,$userSessionID);
                                    $clientePrice = calculatePriceCliente($idG,$id_product,$sizeid,$subcategoryid,$userSessionID,$con);




                                    $selectRelativeboxweight = "select p.name as prod_name ,sh.name as CM,bs.name as st_bu,pac.qty as bunch,
                                    prt.factor/((b.width*b.length*b.height)/6000)*100 AS factor_peso
                                    from growcard_prod_bunch_sizes gs
                                    right join grower_product_box gpb on gs.product_id=gpb.product_id and gs.grower_id=gpb.grower_id
                                    left join product p on gs.product_id=p.id
                                    left join category pc on p.categoryid=pc.id
                                    left join subcategory s on p.subcategoryid=s.id
                                    left join colors c on p.color_id=c.id
                                    left join boxes b on gpb.boxes=b.id
                                    left join bunch_sizes bs on gs.bunch_sizes=bs.id
                                    left join sizes sh on gs.sizes=sh.id
                                    left join growcard_prod_box_packing pac on (gs.product_id = pac.prodcutid and gs.grower_id = pac.growerid
                                    and gs.sizes = pac.sizeid and b.id = pac.box_id and bs.id = pac.bunch_size_id)
                                    left join grower_parameter prt on (p.subcategoryid = prt.idsc and gs.sizes = prt.size)
                                    where gs.grower_id = '$idG'
                                    and gs.product_id = '$id_product'
                                    and gs.sizes = '$sizeid'
                                    and p.subcategoryid = $subcategoryid limit 0,1";

                                    $rs_relative_box_weight = mysqli_query($con,$selectRelativeboxweight);
                                    $rw_relative_box_weight = mysqli_fetch_array($rs_relative_box_weight);
                                    $Relative_box_weight = round($rw_relative_box_weight['factor_peso'],2)."%";



                            ?>
														<tr>
															<td>
																<img alt="<?php echo $name_product; ?>"  src="https://app.freshlifefloral.com/<?php echo $img_product; ?>" width="65">
															</td><!-- product name -->
															<td>
																<a data-target="#growers_modal_MP" data-toggle="#growers_modal_MP" href="javascript:dataIIMB('<?php echo $subcategory_product." ".$name." ".$product_size.' [cm] '.$bunchname. " st/bu"; ?>','<?php echo $img_product; ?>')">
                                  <?php echo $subcategory_product." ".$name_product." ".$product_size.' [cm] '.$bunchname. " st/bu"; ?>
                                  <span class="badge badge-warning pl--3 pr--3 pt--2 pb--2 fs--11 mt-1"><?php echo $featureName; ?></span>

                                </a>

                                <span class="d-block text-muted fs--13"><?php echo $country; ?></span> <span class=
																"d-block text-muted fs--13 mt--10"><?php echo $description; ?></span> <span class="d-block text-gray-500 fs--13">Arrival Date : <?php echo $date_arrive; ?></span>


															</td><!-- price -->
															<td>
																<span class="d-block text-danger fs--15"><sup>*</sup>Grower price <?php echo $growerPrice; ?> <sup class="text-muted fs--10">USD</sup></span>
                                <span class="d-block text-success fs--15">&nbsp; Your price <?php echo $clientePrice; ?> <sup class="text-muted fs--10">USD</sup></span>
                                <span class="d-block fs--13 mt--10 text-muted"><sup>**</sup> Relative box weight: <?php echo $Relative_box_weight;?></span>
                                <span class="d-block text-muted fs--13"><sup>&nbsp;&nbsp;&nbsp;</sup> Box weight: <?php echo $peso;?> kg</span>
															</td><!-- brand -->

															<td class="text-left custom_td">
															<div class="d-flex flex-fill ml-0 mr-0">

									<!-- QUANTITY INPUT class="form-control show-menu-arrow"-->
									<div data-toggle="tooltip" data-original-title="Quantity" class="select_options">
										<select id="select_qty<?php echo $Syni; ?>" class="form-control show-menu-arrow">
                        <?php
                        for($i=1;$i<=100;$i++){
                        ?>
                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
											  <?php
                        }
                        ?>
											</select>

									</div>

<input type="hidden" id="sub_cat_change2<?php echo $Syni; ?>" value="<?php echo $clientePrice.'/'.$product_size.'/'.$featureId.'/'.$sizeid.'/'.$id_product.'/'.$growerID_Prod.'/'.$subcategoryid.'/'.$bunchname; ?>" />

									<div class="pl-2 pr-2 w--140">
										<button class="btn btn-sm btn-block btn-danger bg-gradient-danger text-white b-0" id="addCart<?php echo $Syni; ?>" onclick="newProduct(<?php echo $Syni?>,<?php echo $id_product; ?>)">
											<span class="p-0-xs">
												<span class="fs--18">Add to box</span>
											</span>
										</button>
									</div>

								</div>
															</td><!-- options -->
														</tr>
														<!-- product -->

<?php } ?>
													</tbody>
													<tfoot>
														<tr>
															<th class="text-gray-500 font-weight-normal fs--14 w--140">
																&nbsp;
															</th>
															<th class="text-gray-500 font-weight-normal fs--14">
																VARIETY
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--300">
																PRICE
															</th>
															<th class="text-gray-500 font-weight-normal fs--14 w--60 text-align-left">
																SELECT QTY/BUNCH
															</th>
														</tr>
													</tfoot>
												</table>
											</div>
                      <!-- options and pagination -->
											<div class="mt-4 text-center-xs">
												<div class="row">
													<div class="col-12 col-md-6 mt-4">

														<!-- Inline custom modal (should stay inside <form> to be able to post data) -->
														<div aria-hidden="true" aria-labelledby="modal-title-confirm" class="modal fade show" id="my_custom_modal" role="dialog" tabindex="-1">
															<div class="modal-dialog" role="document">
																<div class="modal-content">
																	<!--

                                                                        Header color - optional
                                                                            .bg-[primary|danger|warning|success|info|pink|indigo]-soft
                                                                    -->
																	<div class="modal-header b-0 bg-primary-soft">
																		<h5 class="modal-title font-weight-light fs--18" id="modal-title-confirm">
																			Inline custom modal
																		</h5><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true" class="fi fi-close fs--18"></span></button>
																	</div><!-- /header -->
																	<!-- body -->
																	<div class="modal-body pt--30 pb--30">
																		Selected items: <span class="js-form-advanced-selected-items">0</span>
																		<div class="fs--18">
																			Customize as you like!<br>
																			<br>
																			<!-- FILE UPLOADER -->
																			<div class="clearfix">
																				<!--

                                                                                    2. AJAX UPLOAD : DYNAMIC PROGRESS UNDER BUTTON
                                                                                    No any extra html code needed for the progress bar.

                                                                                -->
																				<label class="btn btn-warning btn-sm cursor-pointer position-relative"><!--
                                                                                        We use .absolute-full class instead of .viewport-out
                                                                                        Just to make sure the element is working crossbrowser!

                                                                                        .show-hover-container   = show delete button only on hover (always visible on mobile)

                                                                                     -->
																				 <input class="custom-file-input absolute-full js-advancified" data-file-ajax-callback-function="" data-file-ajax-delete-enable="true" data-file-ajax-delete-params=
																				"['action','delete_file']" data-file-ajax-delete-url="../../html_frontend/demo.files/php/demo.ajax_file_upload.php" data-file-ajax-progressbar-custom=""
																				data-file-ajax-progressbar-disable="false" data-file-ajax-reorder-enable="true" data-file-ajax-reorder-params="['action','reorder']" data-file-ajax-reorder-toast-position=
																				"bottom-center" data-file-ajax-reorder-toast-success="Order Saved!" data-file-ajax-reorder-url="../../html_frontend/demo.files/php/demo.ajax_file_upload.php"
																				data-file-ajax-toast-error-txt="One or more files not uploaded!" data-file-ajax-toast-success-txt="Successfully Uploaded!" data-file-ajax-upload-enable="true"
																				data-file-ajax-upload-params="['action','upload']['param2','value2']" data-file-ajax-upload-url="../../html_frontend/demo.files/php/demo.ajax_file_upload.php" data-file-btn-clear=
																				"a.js-form-advanced-btn-multiple-ajax-remove" data-file-exist-err-msg="File already exists:" data-file-ext="jpg,png,gif" data-file-ext-err-msg="Allowed:"
																				data-file-max-size-kb-per-file="3000" data-file-max-size-kb-total="5000" data-file-max-total-files="3" data-file-preview-class=
																				"show-hover-container shadow-md m-2 rounded float-start" data-file-preview-container=".js-form-advanced-container-table-form-test" data-file-preview-img-cover="false"
																				data-file-preview-img-height="120" data-file-preview-show-info="true" data-file-size-err-item-msg="File too large!" data-file-size-err-max-msg="Maximum allowed files:"
																				data-file-size-err-total-msg="Total allowed size exceeded!" data-file-toast-position="bottom-center" data-js-advanced-identifier="3557" multiple name=
																				"ajax_files_progress_dynamic[]" type="file"> <span class="group-icon"><i class="fi fi-arrow-upload"></i> <i class="fi fi-circle-spin fi-spin"></i></span> <span>Ajax
																				Uploader</span></label>
																				<div class="js-form-advanced-container-table-form-test position-relative mt-3 clearfix hide-empty js-sortablified" data-ajax-update-identifier="ajax_files_progress_dynamic"
																				data-ajax-update-params="['action','reorder']" data-ajax-update-url="../../html_frontend/demo.files/php/demo.ajax_file_upload.php" data-update-toast-position="bottom-center"
																				data-update-toast-success="Order Saved!" id="strand_e4w"></div><small class="d-block text-gray-400">Upload few images and then... reorder them :)</small>
																			</div><!-- /FILE UPLOADER -->
																			<br>
																			<small>Yes, ajax content for modals also supported!</small><br>
																			<small>Check <a class="js-ajax link-muted" href="plugins-sow-form-advanced.html">SOW : Form Advanced</a> for more &amp; documentation!</small>
																		</div>
																	</div><!-- /body -->
																	<!-- footer ; buttons -->
																	<div class="modal-footer">
																		<!-- submit button - actually submitting the form -->
																		<button class="btn pt--10 pb--10 fs--16 btn-primary" type="submit"><i class="fi fi-check"></i> Oh, Great!</button> <!-- cancel|close button -->
																		 <a class="btn pt--10 pb--10 fs--16 btn-light" data-dismiss="modal" href="#"><i class="fi fi-close"></i> Close</a>
																	</div><!-- /footer ; buttons -->
																</div>
															</div>
														</div><!-- /Inline custom modal -->
													</div>
													<div class="col-12 col-md-6 mt-4">
                            <nav aria-label="pagination">
                              <ul class="pagination pagination-pill justify-content-end justify-content-center justify-content-md-end">
                                <?php
                                if ($_POST["startrowMB"] != 0) {

                                    $prevrow = $_POST["startrowMB"] - $display;

                                    print("<li class='page-item disabled btn-pill'><a aria-disabled='true' class='page-link' tabindex='-1' href=\"javascript:onclick=funMBPage($prevrow)\">Prev </a></li>");
                                }
                                $pages = intval($num_recordMB / $display);

                                if ($num_recordMB % $display) {

                                    $pages++;
                                }
                                $numofpages = $pages;
                                $cur_page = $_POST["startrowMB"] / $display;
                                $range = 5;
                                $range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2;
                                $range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min;
                                $page_min = $cur_page - $range_min;
                                $page_max = $cur_page + $range_max;
                                $page_min = ($page_min < 1) ? 1 : $page_min;
                                $page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max;
                                if ($page_max > $numofpages) {
                                    $page_min = ($page_min > 1) ? $numofpages - $range + 1 : 1;
                                    $page_max = $numofpages;
                                }
                                if ($pages > 1) {

                                    for ($i = $page_min; $i <= $page_max; $i++) {
                                        if ($cur_page + 1 == $i) {
                                            $nextrow = $display * ($i - 1);
                                            print("<li class='page-item active'><a class='page-link' href='javascript:void();'>$i</a></li>");
                                        } else {

                                            $nextrow = $display * ($i - 1);
                                            print("<li class='page-item'><a class='page-link' href=\"javascript:onclick=funMBPage($nextrow)\"> $i </a></li>");
                                        }
                                    }

                                }
                                if ($pages > 1) {

                                    if (!(($_POST["startrowMB"] / $display) == $pages - 1) && $pages != 1) {

                                        $nextrow = $_POST["startrowMB"] + $display;

                                        print("<li class='page-item'><a class='page-link' href=\"javascript:onclick=funMBPage($nextrow)\" class='page-item'> Next</a></li> ");
                                    }
                                }

                                if ($num_recordMB < 1) {
                                    print("<span class='text'>" . $XX . "</span>");
                                }
                                ?>


                              </ul>
                            </nav><!-- pagination -->
													</div>
												</div>
											</div><!-- /options and pagination -->
										</form>
									</div>
								</div>
							</div><!-- /MAIN GRAPH -->
						</div>
            <form method="post" name="frmfprdMB" action="">
                <input type="hidden"  name="startrowMB" value="<?php echo $_POST["startrowMB"]; ?>">

            </form>
					</section><!-- /Primary -->
				</div><!-- /MIDDLE -->
			</div><!-- FOOTER -->


<?php include('inc/footer.php'); ?>
<script src="assets/js/blockui.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">
<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>

<script type="text/javascript">

window.onload=function() {

  //localStorage.clear();


$.blockUI({ baseZ: 20000, message: '<img src="images/cargando.gif" width="100px" height="100px" />' });
  document.getElementById('expandedSection').click();

  var key = localStorage.getItem("idSessionCompras");

  var growId = document.getElementById('growerId').value;
  growId = growId.substring(1);
  var CateId = document.getElementById('CatId').value;
  CateId = CateId.substring(1);
  var ColorId = document.getElementById('ColorId').value;
  ColorId = ColorId.substring(1);
  var SizeId = document.getElementById('SizeId').value;
  SizeId = SizeId.substring(1);
  var FeatureId = document.getElementById('FeatureId').value;
  FeatureId = FeatureId.substring(1);

  var order_id_prev = document.getElementById('ordenPrevId').value;
  var newBoxMix_Id = document.getElementById('newBoxMixId').value;

  var datos = "idGrower="+growId+"&idCateg="+CateId+"&idColor="+ColorId+"&idSize="+SizeId+"&idFeature="+FeatureId;
  var datosLoadPrev = "PreviousProductKey="+key+"&idCategory="+CateId+"&ord_id="+order_id_prev+"&gid_id="+growId+"&box_nex="+newBoxMix_Id;


  $.ajax({
       type: "POST",
       url: "selectCategoryGrowers.php",
       data: datos,
       cache: false,
       success: function(r){
       document.getElementById('categoryGrowerId').innerHTML = r;
       }
   });

   $.ajax({
        type: "POST",
        url: "selectColorsGrowers.php",
        data: datos,
        cache: false,
        success: function(r){
        document.getElementById('filter_color_list').innerHTML = r;
        }
    });

    $.ajax({
         type: "POST",
         url: "selectSizesGrowers.php",
         data: datos,
         cache: false,
         success: function(r){
         document.getElementById('filter_size_list').innerHTML = r;
         }
     });

     $.ajax({
          type: "POST",
          url: "selectFeaturesGrowers.php",
          data: datos,
          cache: false,
          success: function(r){
          document.getElementById('filter_features_list').innerHTML = r;
          }
      });

      $.ajax({
           type: "POST",
           url: "loadDataItemsPreviuosMixBox.php",
           data: datosLoadPrev,
           cache: false,
           success: function(r){
          //  alert(r);
            // return false;
             document.getElementById('loadDataItemsPrevious').innerHTML = r;
           }
       });

       $.ajax({
            type: "POST",
            url: "loadBarraMixBox.php",
            data: datosLoadPrev,
            cache: false,
            success: function(r){
           //  alert(r);
             // return false;
              document.getElementById('barP').innerHTML = r;
            }
        });

       $.ajax({
            type: "POST",
            url: "loadDataPricesSummaryMixBox.php",
            data: datosLoadPrev,
            cache: false,
            success: function(r){
             // alert(r);
             // return false;
              document.getElementById('loadDataPricesSummary').innerHTML = r;
              $.unblockUI();
            }
        });


        countProductPrevious(key,order_id_prev,growId,newBoxMix_Id);

}

function countProductPrevious(PreviousProductKey,order_id_prev,growId,newBoxMix_Id){

var datos = "ord_id="+order_id_prev+"&PreviousProductKey="+PreviousProductKey+"&gid_id="+growId+"&box_nex="+newBoxMix_Id;

  $.ajax({
       type: "POST",
       url: "count_temp_cartBoxMix.php",
       data: datos,
       cache: false,
       success: function(r){
       //  alert(r);
        // return false;
        if(r==0){

           document.getElementById('endMixBox').disabled = true;

        }else{

          document.getElementById('endMixBox').disabled = false;
        }
         document.getElementById('countPreviousBoxMix').innerHTML = r;

       }
   });

}

function endMixBoxB(){

  document.getElementById('newBoxMixId').value = "";
  location.replace("/en/variety-page.php")
}


function dataIIMB(name,img){

var a ='https://app.freshlifefloral.com/';

$('#growers_modal_MP').modal('show');
$('#Modal_MP').html(name);
$('#img-modal').attr('src',a+img);

}

function funMBPage(pageno)
{
$.blockUI({baseZ: 20000, message: '<img src="images/cargando.gif" width="100px" height="100px" />' });
  var Pag = 'P'+pageno;
  document.frmfprdMB.startrowMB.value = Pag;
  document.frmfprdMB.submit();
}

function get_color_data(a)
{
  if(document.getElementById('ColorId').value ==''){
    var c = ','+a+',';
  document.getElementById('ColorId').value += c;
}else{

    if(document.getElementById('ColorId').value.includes(','+a+','))
    {
      var z = document.getElementById('ColorId').value.replace(a+',','');
      document.getElementById('ColorId').value = z;
    }else{
      if(document.getElementById('ColorId').value.charAt(document.getElementById('ColorId').value.length-1)==','){
        document.getElementById('ColorId').value += a+',';
      }
   }
}

}

function get_size_data(a)
{
  if(document.getElementById('SizeId').value ==''){
    var c = ','+a+',';
  document.getElementById('SizeId').value += c;
}else{

    if(document.getElementById('SizeId').value.includes(','+a+','))
    {
      var z = document.getElementById('SizeId').value.replace(a+',','');
      document.getElementById('SizeId').value = z;
    }else{
      if(document.getElementById('SizeId').value.charAt(document.getElementById('SizeId').value.length-1)==','){
        document.getElementById('SizeId').value += a+',';
      }
   }
}

}

function get_feature_data(a)
{
  if(document.getElementById('FeatureId').value ==''){
    var c = ','+a+',';
  document.getElementById('FeatureId').value += c;
}else{

    if(document.getElementById('FeatureId').value.includes(','+a+','))
    {
      var z = document.getElementById('FeatureId').value.replace(a+',','');
      document.getElementById('FeatureId').value = z;
    }else{
      if(document.getElementById('FeatureId').value.charAt(document.getElementById('FeatureId').value.length-1)==','){
        document.getElementById('FeatureId').value += a+',';
      }
   }
}

}

function funSearchPage(a){


  $.blockUI({baseZ: 20000, message: '<img src="images/cargando.gif" width="100px" height="100px" />' });
  var Perpage = 'Y'+a;
  document.frmfprdMB.startrowMB.value = Perpage;
  document.frmfprdMB.submit();
}

function funSearchPageCat(a){
  $.blockUI({baseZ: 20000, message: '<img src="images/cargando.gif" width="100px" height="100px" />' });
  var Cat = 'C'+a;
  document.frmfprdMB.startrowMB.value = Cat;
  document.frmfprdMB.submit();
}

function funSearchPageFilter(){
  $.blockUI({baseZ: 20000, message: '<img src="images/cargando.gif" width="100px" height="100px" />' });
  var a = document.getElementById('ColorId').value;
  var b = document.getElementById('SizeId').value;
  var c = document.getElementById('FeatureId').value;

  var Filtros = a+'**FLF**'+b+'**FLF**'+c+'**FLF**';
  document.frmfprdMB.startrowMB.value = Filtros;
  document.frmfprdMB.submit();
}

function newProduct(a,prodId)
{

//  alert(localStorage.getItem("idSessionCompras"));
  if(localStorage.getItem("idSessionCompras") == null)
  {
    var newPreviousProductKey = RandomNum(1000000001, 2147483647);
    localStorage.setItem("idSessionCompras", newPreviousProductKey);
    var idSessionCompras = localStorage.getItem("idSessionCompras");

      var priceSize = document.getElementById('sub_cat_change2'+a).value;
      var priceSizeArray = priceSize.split("/");
      var price = priceSizeArray[0]
      var size  = priceSizeArray[1]+" [cm]";
      var feature_id  = priceSizeArray[2];
      var size_id  = priceSizeArray[3];
      var prodId  = priceSizeArray[4];
      var growerId  = priceSizeArray[5];
      var cartegoriaId  = priceSizeArray[6];
      var bunchNumber  = priceSizeArray[7];

      var steamsT = bunchNumber*(document.getElementById('select_qty'+a).value);
      var boxId = 0;
      var quantity = 1;
      var bestOp = 0;
      var order_id_prev = document.getElementById('ordenPrevId').value;
      var numRecords = document.getElementById('numRecords').value;

      if(numRecords>20){
        numRecords=20;
      }

      var datosCart = "ord_id="+order_id_prev+"&bestOp_id="+bestOp+"&size_id="+size_id+"&feature_id="+feature_id+"&newPreviousProductKey="+newPreviousProductKey+"&product_id="+prodId+"&grower_id="+growerId+"&price_id="+price+"&size="+size+"&box_id="+boxId+"&quantity_id="+quantity+"&steams="+steamsT+"&categoriaId="+cartegoriaId+"&bunchNumber="+bunchNumber;
      //alert(datosCart);
      //return false;

       $.ajax({
            type: "POST",
            url: "save_temp_cartMixBox.php",
            data: datosCart,
            cache: false,
            success: function(r){
            // alert(r);
          //    return false;
              if(r==1){
                document.getElementById('newBoxMixId').value = '';
                var newBoxMix_Id = document.getElementById('newBoxMixId').value;

               countProductPrevious(idSessionCompras,order_id_prev,growerId,newBoxMix_Id);
                loadDataCart(idSessionCompras,growerId,newBoxMix_Id);
                loadBarraP(idSessionCompras,growerId,newBoxMix_Id);
                  for (j = 1; j <= numRecords; j++) {
                    document.getElementById('select_qty'+j).value = "1";
                  }


                var x = document.getElementsByClassName("filter-option-inner");
                for (i = 0; i < x.length; i++) {
                  x[i].innerHTML = '<div class="filter-option-inner-inner">1</div>';
                }


                swal("Good job!", "Product add to cart!", "success");
              }else{
                swal("Sorry!", "Please, try again!", "error");
              }

            }
        });



  }
    else
  		{
  		addNoFirstTime(localStorage.getItem("idSessionCompras"),a,prodId);
  		}

}

function RandomNum(min, max) {
   return Math.round(Math.random() * (max - min) + min);
}

function addNoFirstTime(newPreviousProductKey,a,prodId)
{

  var priceSize = document.getElementById('sub_cat_change2'+a).value;
  var priceSizeArray = priceSize.split("/");
  var price = priceSizeArray[0]
  var size  = priceSizeArray[1]+" [cm]";
  var feature_id  = priceSizeArray[2];
  var size_id  = priceSizeArray[3];
  var prodId  = priceSizeArray[4];
  var growerId  = priceSizeArray[5];
  var cartegoriaId  = priceSizeArray[6];
  var bunchNumber  = priceSizeArray[7];

  var steamsT = bunchNumber*(document.getElementById('select_qty'+a).value);
  var boxId = 0;
  var quantity = 1;
  var bestOp = 0;
  var order_id_prev = document.getElementById('ordenPrevId').value;
  var newBoxMix_Id = document.getElementById('newBoxMixId').value;

  var numRecords = document.getElementById('numRecords').value;

  if(numRecords>20){
    numRecords=20;
  }


  var datosCart = "ord_id="+order_id_prev+"&bestOp_id="+bestOp+"&size_id="+size_id+"&feature_id="+feature_id+"&newPreviousProductKey="+newPreviousProductKey+"&product_id="+prodId+"&grower_id="+growerId+"&price_id="+price+"&size="+size+"&box_id="+boxId+"&quantity_id="+quantity+"&steams="+steamsT+"&NumBoxId="+newBoxMix_Id+"&categoriaId="+cartegoriaId+"&bunchNumber="+bunchNumber;
  //alert(datosCart);
  //return false;



     $.ajax({
          type: "POST",
          url: "save_temp_cart_itemsMixBox.php",
          data: datosCart,
          cache: false,
          success: function(r){

            //  alert(r);
            //  return false;
            if(r==1){
              document.getElementById('newBoxMixId').value =  '';
              var newBoxMix_Id = document.getElementById('newBoxMixId').value;

             countProductPrevious(newPreviousProductKey,order_id_prev,growerId,newBoxMix_Id);
              loadDataCart(newPreviousProductKey,growerId,newBoxMix_Id);
              loadBarraP(newPreviousProductKey,growerId,newBoxMix_Id);
                  for (j = 1; j <= numRecords; j++) {
                    document.getElementById('select_qty'+j).value = "1";
                  }

                var x = document.getElementsByClassName("filter-option-inner");
                for (i = 0; i < x.length; i++) {
                  x[i].innerHTML = '<div class="filter-option-inner-inner">1</div>';
                }

              swal("Good job!", "Product add to cart!", "success");
            }else{
              swal("Sorry!", "Please, try again!", "error");
            }
          }
      });
}


function deleteItemPrevious(itemPrevious,growerId){
  datos = "PreviousProductKey="+itemPrevious;
  var key = localStorage.getItem("idSessionCompras");
  var order_id_prev = document.getElementById('ordenPrevId').value;


    $.ajax({
         type: "POST",
         url: "del_temp_cartMixBox.php",
         data: datos,
         cache: false,
         success: function(r){
           swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will need to select again this product!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    if(r==1){
                      document.getElementById('newBoxMixId').value = "";
                      var newBoxMix_Id = document.getElementById('newBoxMixId').value;
                      countProductPrevious(key,order_id_prev,growerId,newBoxMix_Id);
                      loadDataCart(key,growerId,newBoxMix_Id);
                      loadBarraP(key,growerId,newBoxMix_Id);
                      swal("Ok! Your product has been deleted!", {
                        icon: "success",
                      });
                    }

                  } else {
                    //Nothing to do!
                  }
                });

          // document.getElementById('countProductosPrev').innerHTML = r;
         }
     });

}

function loadBarraP(key,growerId){

  var CateId = document.getElementById('CatId').value;
  CateId = CateId.substring(1);
  var order_id_prev = document.getElementById('ordenPrevId').value;
  var newBoxMix_Id = document.getElementById('newBoxMixId').value;


  var datosLoadPrev = "PreviousProductKey="+key+"&idCategory="+CateId+"&ord_id="+order_id_prev+"&gid_id="+growerId+"&box_nex="+newBoxMix_Id;

  $.ajax({
       type: "POST",
       url: "loadBarraMixBox.php",
       data: datosLoadPrev,
       cache: false,
       success: function(r){
      //  alert(r);
        // return false;
         document.getElementById('barP').innerHTML = r;
       }
   });

}


function loadDataCart(key,growerId,bx){

  var CateId = document.getElementById('CatId').value;
  CateId = CateId.substring(1);
  var order_id_prev = document.getElementById('ordenPrevId').value;
  var newBoxMix_Id = document.getElementById('newBoxMixId').value;


  var datosLoadPrev = "PreviousProductKey="+key+"&idCategory="+CateId+"&ord_id="+order_id_prev+"&gid_id="+growerId+"&box_nex="+newBoxMix_Id;

  $.ajax({
       type: "POST",
       url: "loadDataItemsPreviuosMixBox.php",
       data: datosLoadPrev,
       cache: false,
       success: function(r){
        //alert(r);
        // return false;
         document.getElementById('loadDataItemsPrevious').innerHTML = r;
       }
   });

   $.ajax({
        type: "POST",
        url: "loadDataPricesSummaryMixBox.php",
        data: datosLoadPrev,
        cache: false,
        success: function(r){
         // alert(r);
         // return false;
          document.getElementById('loadDataPricesSummary').innerHTML = r;
        }
    });
}



</script>

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="exampleModalLabelMd" class="modal fade" id="growers_modal_MP" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <!-- Header -->
      <div class="modal-header">
        <h5 class="modal-title" id="Modal_MP">

        </h5>
        <button aria-label="Close" class="close" data-dismiss="modal" type="button">
          <span aria-hidden="true" class="fi fi-close fs--18"></span></button>
      </div><!-- Content -->
      <div class="modal-body">
        <img id="img-modal"  width="100%">
      </div>
    </div>
  </div>
</div><!-- /Modal -->
