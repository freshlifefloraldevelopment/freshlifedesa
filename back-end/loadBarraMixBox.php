<?php
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 20 Abril 2021
Structure MarketPlace previous to buy

**/
include('config/config_gcp.php');

$htmlLoadData="";
if(isset($_POST["PreviousProductKey"]) && $_POST['PreviousProductKey']!=''){

$PreviousProductKey = $_POST['PreviousProductKey'];
$order_id_prev = $_POST['ord_id'];
$grower_id = $_POST['gid_id'];
$NewB    = $_POST['box_nex'];

if($NewB!=1){
 $countMixBox = "select max(box_mix_id) as max
from buyer_requests_shoping_cart_temp_items
where id_buyer_temp = '$PreviousProductKey'
and  id_order_prev='$order_id_prev'
and id_grower = '$grower_id'
and state_item='0'";

$rs_countMixBox    = mysqli_query($con, $countMixBox);
$totalCountMixBox  = mysqli_fetch_array($rs_countMixBox);
$box_mix_id = $totalCountMixBox['max'];

  $loadDataItemsPrevious= "select sct.id_buyer_temp_item as keyItem, sct.id_product , p.name as prodname,
  p.image_path as prodimage, sct.price as prodprice, sct.quantity as prodquantity, sct.size_steam as sizesteam,
  sct.steams as steams, sct.feature_id as features, sct.size_id as size_id, sct.id_category as categoryid, sct.bunchs
from buyer_requests_shoping_cart_temp_items sct
inner join product  p   ON sct.id_product=p.id
where sct.id_buyer_temp = '$PreviousProductKey'
and sct.id_grower = '$grower_id'
and sct.id_order_prev='$order_id_prev'
and sct.box_mix_id ='$box_mix_id'
and sct.state_item='0'
order by sct.id_buyer_temp_item";

  $result_loadDataItemsPrevious = mysqli_query($con,$loadDataItemsPrevious);
  while ($row_result = mysqli_fetch_array($result_loadDataItemsPrevious))
  {
    $id_feature = $row_result['features'];
    $id_product = $row_result['id_product'];
    $id_size = $row_result['size_id'];
    $idCategory = $row_result['categoryid'];
    $bunchNum   = $row_result['bunchs'];

      //Percent BAR

      if($id_feature!=0)
      {
        $filterFeature = ", ff.id, ff.name as featureName";
        $filterFeature1 = "left join features ff on gs.feature=ff.id left join growcard_prod_features gpf on gs.product_id=gpf.product_id and gs.grower_id=gpf.grower_id and ff.id=gpf.features";
        $filterFeature2 = "and ff.id = pac.feature";
        $filterFeature3 = "and ff.id = prt.feature";
        $filterFeature4 = "and ff.id = '$id_feature'";
      }else{  $filterFeature = '';
        $filterFeature1 = '';
        $filterFeature2 = '';
        $filterFeature3 = '';
        $filterFeature4 = '';}

        $selectPercentbar = "select p.name as prod_name, sh.name as CM, bs.name as st_bu, pac.qty as
      bunch, prt.factor/((b.width*b.length*b.height)/6000)*100 AS factor_peso $filterFeature
      from growcard_prod_bunch_sizes gs
      $filterFeature1
      right join grower_product_box gpb on gs.product_id=gpb.product_id and gs.grower_id=gpb.grower_id
      left join product p on gs.product_id=p.id
      left join category pc on p.categoryid=pc.id
      left join subcategory s on p.subcategoryid=s.id
      left join colors c on p.color_id=c.id
      left join boxes b on gpb.boxes=b.id
      left join bunch_sizes bs on gs.bunch_sizes=bs.id
      left join sizes sh on gs.sizes=sh.id
      left join growcard_prod_box_packing pac on (gs.product_id = pac.prodcutid and gs.grower_id = pac.growerid
      and gs.sizes = pac.sizeid and b.id = pac.box_id and bs.id = pac.bunch_size_id $filterFeature2)
      left join grower_parameter prt on (p.subcategoryid = prt.idsc and gs.sizes = prt.size $filterFeature3)
      where gs.grower_id = '$grower_id'
      and gs.product_id = '$id_product'
      and gs.sizes = '$id_size'
      and p.subcategoryid = '$idCategory'
      $filterFeature4
      limit 0,1";
      $selectPercentBarArray= mysqli_query($con,$selectPercentbar);
      $percentBar = mysqli_fetch_array($selectPercentBarArray);
       $porcentajeTallo = $percentBar['factor_peso'];

       $factorProcentaje = $row_result['steams'] / $bunchNum;

    //   $valorP += number_format($porcentajeTallo*$row_result['steams'],0);

    $valorP += number_format($porcentajeTallo*$factorProcentaje,0);

    if($valorP>100){
      $color="danger";
    }else{
      $color="primary";
    }


  }
  $htmlLoadData .= "<div class='progress-bar bg-".$color."' role='progressbar' style='width: ".$valorP."%' aria-valuenow='50' aria-valuemin='0' aria-valuemax='100'>".$valorP." %</div>";

}
 echo $htmlLoadData;
}
?>
