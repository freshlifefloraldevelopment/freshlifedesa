<?php
/**
#Market place cart
Developer educristo@gmail.com
Start 06 Abril 2021
Structure MarketPlace previous to buy
**/

require_once("../config/config_gcp.php");

$htmlLoadData="";
if(isset($_POST["idGrower"]) && $_POST['idGrower']!=''){
$idGrower = $_POST['idGrower'];
$idFeatures = str_replace(',',' ',$_POST['idFeature']);

		$sql_features = "select f.id as id, fe.features , f.name as featurename
		 from grower_product gp
		INNER JOIN growers g ON gp.grower_id = g.id
		INNER JOIN product p ON gp.product_id = p.id
		INNER JOIN subcategory s ON p.subcategoryid = s.id
		INNER JOIN colors c on p.color_id = c.id
		INNER JOIN growcard_prod_features fe ON (gp.grower_id = fe.grower_id and gp.product_id = fe.product_id)
		INNER JOIN features f on fe.features = f.id
		where g.active = 'active'
		and gp.grower_id = '$idGrower'
		group by f.id, fe.features , f.name
		order by f.name";


       $rs_features = mysqli_query($con,$sql_features);
			 if(mysqli_num_rows($rs_features)>0)
			 {
           while ($row_features = mysqli_fetch_array($rs_features))
           {
						 $SizeFeature = '';
						 if (strpos($idFeatures, $row_features['id']) !== false)
						 {
						 	$SizeFeature = 'checked';
						 }

					 $htmlLoadData .='<label class="form-selector"><input onclick="get_feature_data('.$row_features['id'].')" name="size['.$row_features['featurename'].']" type="checkbox" '.$SizeFeature.'><span>'.$row_features['featurename'].'</span></label>';

           }
		   }
 echo $htmlLoadData;
}
?>
