<?php
include("config/config_new.php");
$country = $_REQUEST['cname'];
$userSessionID = $_REQUEST['userSessionID'];

//Get Buyer Country ( from )
$get_buyer_info = "select country from buyers where id='" . $userSessionID . "'";
$buyer_info = mysqli_query($con, $get_buyer_info);
$buyer = mysqli_fetch_assoc($buyer_info);
$from = $buyer['country'];

//Get input country ( to )
$getCountryId = "select id from country where name ='$country'";
$getRes = mysqli_query($con, $getCountryId);
$countryIdArray = mysqli_fetch_assoc($getRes);
$to = $countryIdArray['id'];
$getShippingMethods = mysqli_query($con, "select * from shipping_method where source_country='" . $from . "' and destination_country='" . $to . "'");
if (mysqli_num_rows($getShippingMethods) > 0) {
    while ($shippingMethods = mysqli_fetch_assoc($getShippingMethods)) {
        //print_r($shippingMethods);
        $shippingMethodsId = $shippingMethods['id'];
        $connections = unserialize($shippingMethods['connections']);
        $firstConnection = $connections['connection_1'];
        $getConnection = "select * from connections where id='" . $firstConnection . "'";
        $conData = mysqli_query($con, $getConnection);
        $connectionData = mysqli_fetch_assoc($conData);
        //print_r($connectionData);

        $getCargo = "select * from cargo_agency where id ='" . $connectionData['cargo_agency'] . "'";
        $cargoData = mysqli_query($con, $getCargo);
        $cargoAgencyData = mysqli_fetch_assoc($cargoData);
        //print_r($cargoAgencyData);
        $airLine = $connectionData['shipping_company'];
        $getAirLine = "select * from shipping_company where id ='" . $airLine . "'";
        $airLData = mysqli_query($con, $getAirLine);
        $airLineData = mysqli_fetch_assoc($airLData);
        $chargesPerKilo = 0;
        $chargesPerShip = 0;
        foreach ($connections as $connection) {
            $getConnections = "select * from connections where id='" . $connection . "'";
            $conDatas = mysqli_query($con, $getConnections);
            $connectionDatas = mysqli_fetch_assoc($conDatas);
            $cpk = unserialize($connectionDatas['charges_per_kilo']);
            foreach ($cpk as $perkilo) {
                $chargesPerKilo = $chargesPerKilo + $perkilo;
            }

            $cps = unserialize($connectionDatas['charges_per_shipment']);
            foreach ($cps as $pership) {
                $chargesPerShip = $chargesPerShip + $pership;
            }
        }
        ?>
        <tr class="to_select" id="to_select<?php echo $shippingMethodsId; ?>">
            <td><?php if (!empty($cargoAgencyData['logo'])) { ?>
                    <img alt="<?php echo $cargoAgencyData['name'] ?>" width="60" src="<?php echo SITE_URL . $cargoAgencyData['logo']; ?>">
                    <?php
                } else {
                    echo $cargoAgencyData['name'];
                }
                ?>
            </td>
            <td><?php if (!empty($airLineData['logo'])) { ?>
                    <img alt="<?php echo $airLineData['name'] ?>" width="60" src="<?php echo SITE_URL . $airLineData['logo']; ?>">
                    <?php
                } else {
                    echo $airLineData['name'];
                }
                ?>
            </td>
            <td>$<?php echo $chargesPerKilo; ?></td>
            <td>Wed Mar 9 – Wed Mar 16</td>
            <td><?php echo (!empty($connectionData['minimum_weight']))? $connectionData['minimum_weight'].' Kg' : '--'; ?></td>
            <td><a href="javascript:void(0);" onclick="shippingInfo('<?php echo $shippingMethodsId; ?>')" class="btn btn-purple btn-xs select_btn"><i class="fa fa-chevron-right white"></i> Select </a></td>
        </tr> 
        
        <?php
    }
} else {
    ?>
    <tr>
        <td colspan="6">Thank you for working to update your shipping alternatives Our database doesn't have a shipping option for your city and/or country, a shipping request has been sent our team we will get back to you shortly</td>
    </tr>
    <?php
}
?>
