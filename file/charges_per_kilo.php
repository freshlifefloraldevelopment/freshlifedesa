<?php
require_once("../config/config_gcp.php");

$userSessionID = $_SESSION["buyer"];
$date = date('Y-m-d');

$getBuyerShippingMethod = "select shipping_method_id
                            from buyer_shipping_methods 
                           where buyer_id ='" . $userSessionID . "'";  

$buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
$buyerShippingMethod = mysqli_fetch_assoc($buyerShippingMethodRes);    
$shipping_method_id = $buyerShippingMethod['shipping_method_id'];
    
   $getShippingMethod = "select connect_group
                           from shipping_method 
                          where id='" . $shipping_method_id . "'";
    
    $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);
    
    $temp_conn = explode(',', $shippingMethodDetail['connect_group']);
    
    $id_conn = $temp_conn[1];  // Default
          
    $getConnect = "select charges_per_kilo 
                     from connections
                    where id='" . $id_conn . "'";
    
    $rs_connect = mysqli_query($con, $getConnect);
    
    $charges = mysqli_fetch_assoc($rs_connect);     
               
     /////////////////////////////////////////////////////////////             
                
    $cost = $charges['charges_per_kilo'];
    $cost_un = unserialize($cost);
    
    $cost_sum = 0;
    
    foreach ($cost_un as $key => $value) {
        $cost_sum = $cost_sum + $value;
    }
    $charges_per_kilo_trans =  $cost_sum;   
    
    $sql_price = "select gp.id,gp.price_adm as price,   s.name as sizename   , 
                         f.name as featurename, gp.feature as feature, s.id size_id,
                         gp.factor , gp.stem_bunch , b.name as stems
                    from grower_parameter gp
                   inner JOIN sizes s ON gp.size = s.id
                   inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
                    left JOIN features f  ON gp.feature = f.id
                   where gp.idsc = '2'" ;

                  $rs_price = mysqli_query($con,$sql_price);
                  
                while ($cal_price = mysqli_fetch_array($rs_price)) {
                    
                     $priceBunch = $charges_per_kilo_trans * $cal_price['factor'];
                     $priceSteam = $priceBunch / $cal_price['stems'];                     
                     $priceCalculado = $priceSteam + $cal_price['price'];
                }                        
?>