<?php
$menuoff = 1;
$page_id = 421;
$message = 0;
include("config/config_new.php");
if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = 'profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = 'profile_images/' . $profile_image;
}
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);
$page_request = "summary";


require_once 'includes/profile-header.php';

?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>

<?php
require_once "includes/left_sidebar_buyer.php";


$sel_check = "select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,cr.flag,br.lfd,br.type as brtype,br.id as cartid,
            br.product as pid,br.isy,
            bo.order_number
            from grower_offer_reply gr 
            left join buyer_requests br on gr.offer_id=br.id
            left join (select  id,order_number from  buyer_orders) bo  on  br.id_order= bo.id
            left join growers g on gr.grower_id=g.id 
            left join country cr on g.country_id=cr.id
            where gr.buyer_id='".$userSessionID."' and gr.status=2 and gr.reject=0 group by br.product order by gr.id desc";

$rs_growers = mysqli_query($con, $sel_check);
$totalio = mysqli_num_rows($rs_growers);
?>

<section id="middle">


    <!-- page title -->
    <header id="page-header">
        <h1>Bootstrap Tables</h1>
        <ol class="breadcrumb">
            <li><a href="#">Tables</a></li>
            <li class="active">Bootstrap Tables</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>Purchase Summary   --------</strong> <!-- panel title -->
                </span>
                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse"
                           data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen"
                           data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-Confirmed-title="Confirmed"
                           data-Confirmed-message="Are you sure you want to remove this panel?" data-toggle="tooltip"
                           title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul>
                <!-- /right options -->

            </div>

            <!-- panel content -->
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                        <tr>
                            <th class="width-30">Img</th>
                            <th >Product</th>
                            <th>Boxes Requested</th>
                            <!--<th>Price</th>-->
                            <th>Date</th>
                            <th>P.O. Number</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tr>
                        <?php
                        $i = 1;
                        while ($growers = mysqli_fetch_array($rs_growers)) { //echo '<pre>';print_r($growers);//die;
                            $sel_check1 = "select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,cr.flag,br.lfd,br.type as brtype,br.id as
                                              cartid,br.shpping_method ,br.product as pid,br.isy from grower_offer_reply gr 
                                              left join buyer_requests br on gr.offer_id=br.id	
                                              left join growers g on gr.grower_id=g.id 
                                              left join country cr on g.country_id=cr.id
                                              where gr.buyer_id='" . $userSessionID . "' and gr.status=2 and gr.reject=0 and 
                                              br.product = '" . $growers['pid'] . "' order by gr.id desc";
                            $getimage = "select image_path from product where id=" . $growers['pid'];
                            $imageres = mysqli_query($con, $getimage);
                            $resimg = mysqli_fetch_array($imageres);
                            ?>
                            <tr>

                                <td class="text-center"><a data-toggle="modal"
                                                           data-target=".flower_smal<?php echo $i; ?>"><img
                                                src="<?php echo SITE_URL . $resimg['image_path']; ?>" alt="" width="60"></a>
                                    <!--Pop Up-->
                                    <div class="modal fade flower_smal<?php echo $i; ?>" tabindex="-1" role="dialog"
                                         aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <!-- header modal -->
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close"><span aria-hidden="true">X</span>
                                                    </button>
                                                    <h4 class="modal-title"
                                                        id="mySmallModalLabel"><?= $growers["product_subcategory"] ?> <?= $growers["product"] ?> <?= $growers["size"] ?></h4>
                                                </div>

                                                <!-- body modal -->
                                                <div class="modal-body">
                                                    <img src="<?php echo SITE_URL . $resimg['image_path']; ?>" alt=""
                                                         width="100%">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!--Pop Up-->
                                </td>
                                <td><?= $growers["product_subcategory"] ." ". $growers["product"] ?> <?= $growers["product"] ?> <?= $growers["size"] . "cm" ?></td>
                                <td><?= $growers["boxqty"] ?> <?= $growers["boxtype"] ?> Box(es)</td>

                                <td><?php
                                    if ($growers["lfd"] != '0000-00-00') {
                                        echo date("F j, Y", strtotime($growers["lfd"]));
                                    } else {
                                        echo 'No Date';
                                    }
                                    ?></td>
                                <td><?= $growers['order_number'] ?></td>
                                <td><a data-toggle="modal" data-target=".open_offer_modal_<?php echo $i; ?>" class="btn btn-success btn-xs relative">Confirmed</a>
                                    <!--Offer Modal Start-->
                                    <div class="modal fade open_offer_modal_<?php echo $i; ?>" tabindex="-1"
                                         role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <!-- header modal -->
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                                    <h4 class="modal-title" id="myLargeModalLabel"> <?= $growers["boxqty"] ?> <?= $growers["boxtype"]. " Box(es) " ?><?= $growers["product"] ?> <?= $growers["size"]." cm" ?></h4>
                                                </div>
                                                <!-- body modal -->
                                                <div class="modal-body">
                                                    <h5>Information  Order</h5>
                                                    P.O. Number<?= $growers['order_number'] ?>
                                                    <br>
                                                    Date : <?php
                                                    if ($growers["lfd"] != '0000-00-00') {
                                                        echo date("F j, Y", strtotime($growers["lfd"]));
                                                    } else {
                                                        echo 'No Date';
                                                    }
                                                    ?>
                                                    <br>
                                                    <h6></h6>
                                                    <div class="table-responsive">
                                                        <table class="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th  style="width:30%">Grower</th>
                                                                <th>Boxes Requested</th>
                                                                <th>Order</th>
                                                                <th>Price</th>
                                                                <!--<th>Date</th>-->
                                                                <!--<th>Delivery Option</th>-->
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            $cont_box=0;
                                                            $rs_growers1 = mysqli_query($con, $sel_check1);
                                                            while ($growers1 = mysqli_fetch_array($rs_growers1)) { //echo '<pre>';print_r($growers1);
                                                                ?>
                                                                <tr>
                                                                    <!--<td><?=$sel_check1?></td>-->
                                                                    <td><?= $growers1["growers_name"] ?></td>
                                                                    <td><?php

                                                                        $cont_box+=$growers1["boxqty"];
                                                                        echo  $growers1["boxqty"] ?> <?= $growers1["boxtype"]." Box(es)" ?></td>

                                                                    <td style="width: 30%"><small><?= $growers1["product_subcategory"]. " ".$growers1["product"]. " ".$growers1["size"]. "cm"?> <?= $growers1['bunchqty'] ." Bunch x"." ".$growers1['steams']." steams" ?></small></td>

                                                                    <td><?= $growers1['price']?></td>
                                                                   <!-- <td><?php
                                                                        if ($growers1["lfd"] != '0000-00-00') {
                                                                            echo date("F j, Y", strtotime($growers1["lfd"]));
                                                                        } else {
                                                                            echo 'No Date';
                                                                        }
                                                                        ?></td>-->
                                                                    <!--<td><?php
                                                                        $select_shipping_info = "select name,description from shipping_method where id='" . $growers1["shipping_method"] . "'";
                                                                        $rs_shipping_info = mysqli_query($con, $select_shipping_info);
                                                                        $shipping_info = mysqli_fetch_array($rs_shipping_info);
                                                                        echo $shipping_info["name"];
                                                                        ?></td>-->
                                                                </tr>
                                                            <?php } ?>
                                                            <tr>

                                                                <td>Total :</td>
                                                                <td><?=$cont_box ." Box(es)"?></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--Offer Modal End-->
                                </td>
                        </tr>

                            <?php
                            $i++;
                        }
                        ?>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>
<?php require_once 'includes/footer_new.php'; ?>
