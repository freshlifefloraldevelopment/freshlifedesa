<?php
require_once("config/config_new.php");

#############QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################
$pageId = 6; //VARIETY PAGE ID 
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL . 'includes/assets/css/essentials-flfv5.css', SITE_URL . 'includes/assets/css/layout-flfv5.css',
    SITE_URL . 'includes/assets/css/header-1-flfv5.css', SITE_URL . 'includes/assets/css/color_scheme/blue.css');
$jsHeadArray = array();
$pageData['meta_title'] = $pageData['page_title'];
$pageData['meta_keyword'] = $pageData['page_title'];
$pageData['meta_desc'] = $pageData['page_title'];
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################
require_once 'includes/header.php';
?>
<section class="page-header page-header-xs">
    <div class="container">

        <!-- breadcrumbs -->
        <ol class="breadcrumb breadcrumb-inverse">
            <li><a href="<?php echo SITE_URL; ?>">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Privacy</li>
        </ol><!-- /breadcrumbs -->

    </div>
</section>
<!-- /PAGE HEADER -->

<!-- -->
<section>
    <div class="container">

        <div class="row">

            <div class="col-lg-12">

                <p><?php echo $pageData['page_desc'] ?></p>

            </div>
        </div>

    </div>
</section>
<!-- / -->
<?php
require_once 'includes/footer.php';
?>
