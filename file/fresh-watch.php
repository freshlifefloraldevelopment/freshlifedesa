<?php
require_once("config/config_new.php");
if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}
$img_url = 'profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = 'profile_images/' . $profile_image;
}
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);
$shipping_method = trim($info["shipping"], ",");
$page_request = "fresh_watch";

require_once 'includes/profile-header.php';
require_once "includes/left_sidebar_buyer.php";
?>

<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Bootstrap Tables</h1>
        <ol class="breadcrumb">
            <li><a href="#">Tables</a></li>
            <li class="active">Bootstrap Tables</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <!-- LEFT -->
                    <div class="col-md-12">
                        <div id="content" class="padding-20">
                            <div id="panel-2" class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="title elipsis">
                                        <strong>Request for Product</strong> <!-- panel title -->
                                    </span>
                                    <!-- right options -->
                                    <ul class="options pull-right list-inline">
                                        <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                                        <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                                        <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Close"><i class="fa fa-times"></i></a></li>
                                    </ul>
                                    <!-- /right options -->
                                </div>
                                <!-- panel content -->
                                <div class="panel-body">
                                    <div class="">
                                        <div id="showMsg" style="display: none;margin:30px; color:#030; font-size:22px; text-align:center; font-weight:bold;"> </div>
                                        <table class="table table-hover">
                                            <button type="button" class="btn btn-primary btn-lg btn-block" style="background:purple!important;" data-toggle="modal" data-target=".list_modal">Add a new product to your FRESH WATCH list</button>
                                            <div class="modal fade list_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <!-- header modal -->
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myLargeModalLabel">Fullwidth modal</h4>
                                                        </div>
                                                        <!-- body modal -->
                                                        <div class="modal-body">						
                                                         <div id="showMsgAdd" style="display: none;font-size:22px; text-align:center; font-weight:bold;"> </div>
                                                            <form class="searchProduct" action="" method="post" id="show_hide1" style="display:none!important;">
                                                                <div class="panel-heading panel-heading-transparent">
                                                                    <strong>REFINE YOUR SEARCH</strong><hr>
                                                                </div>
                                                                <fieldset>
                                                                    <!-- required [php action request] -->
                                                                    <div class="row">
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>Product</label>
                                                                                <select name="subcategorycombo" id="subcategorycombo" onchange="filterByProductWatch(this.value)" class="form-control pointer required select2" style="width:100%;">
                                                                                    <option value="">Select Product</option>
                                                                                    <?php
                                                                                    $sel_testi = "select gpb.id,s.name as sname,s.id as sid from grower_product_box_packing gpb
                                                                                                    left join product p on gpb.prodcutid = p.id
                                                                                                    left join subcategory s on p.subcategoryid=s.id
                                                                                                    left join growers g on gpb.growerid=g.id
                                                                                                    where g.active='active' and s.id > 0 ";
                                                                                    $sel_testi.=" group by p.subcategoryid order by p.categoryid,s.name ";

                                                                                    $rs_testi = mysqli_query($con, $sel_testi);
                                                                                    while ($testi = mysqli_fetch_array($rs_testi)) {
                                                                                        ?>	
                                                                                        <option value="<?= $testi["sid"] ?>"  ><?= $testi["sname"] ?></option>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>Variety</label>
                                                                                <select name="productcombo" id="productcombo" onchange="filterByVarietyWatch(this.value)" class="form-control pointer required select2" style="width:100%;">
                                                                                    <option value="">Select Variety</option>
                                                                                    <?php
                                                                                    $sel_testi = "select gpb.id,p.name as pname,p.id as pid from grower_product_box_packing gpb
                                                                                    left join product p on gpb.prodcutid = p.id
                                                                                    left join growers g on gpb.growerid=g.id
                                                                                    where g.active='active' and p.name is not NULL ";
                                                                                    $sel_testi.=" group by gpb.prodcutid order by p.name";

                                                                                    $rs_testi = mysqli_query($con, $sel_testi);
                                                                                    while ($testi = mysqli_fetch_array($rs_testi)) {
                                                                                        ?>	
                                                                                        <option value="<?= $testi["pid"] ?>"  ><?= $testi["pname"] ?></option>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>Size</label>
                                                                                <select id="sizecombo" name="sizecombo" onchange="filterBySizeWatch(this.value)" class="form-control pointer required select2" style="width:100%;">
                                                                                    <option value="">Select Size</option>
                                                                                    <?php
                                                                                    $sel_testi = "select gpb.id,s.name as sname,s.id as sid from grower_product_box_packing gpb
                                                                                        left join product p on gpb.prodcutid = p.id
                                                                                        left join sizes s on gpb.sizeid=s.id
                                                                                        left join growers g on gpb.growerid=g.id
                                                                                        where g.active='active' and s.name is not NULL ";

                                                                                    $sel_testi.=" group by gpb.sizeid order by CONVERT(SUBSTRING(s.name,1), SIGNED INTEGER)";
                                                                                    $rs_testi = mysqli_query($con, $sel_testi);
                                                                                    while ($testi = mysqli_fetch_array($rs_testi)) {
                                                                                        ?>		  
                                                                                        <option value="<?= $testi["sid"] ?>" ><?= $testi["sname"] ?>cm</option>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>Special Feature</label>
                                                                                <select name="specialcombo" id="specialcombo" class="form-control pointer required select2" style="width:100%;">
                                                                                    <?php
                                                                                    $sel_testi = "select gpb.id,s.name as sname,s.id as sid from grower_product_box_packing gpb
                                                                                    left join product p on gpb.prodcutid = p.id
                                                                                    left join features s on gpb.feature=s.id
                                                                                    left join growers g on gpb.growerid=g.id
                                                                                    where g.active='active' and s.name is not NULL ";

                                                                                    $sel_testi.=" group by gpb.feature order by s.name";
                                                                                    $rs_testi = mysqli_query($con, $sel_testi);
                                                                                    ?>
                                                                                    <option value="">Select Special Feature</option>
                                                                                    <?php
                                                                                    while ($testi = mysqli_fetch_array($rs_testi)) {
                                                                                        ?>
                                                                                        <option value="<?= $testi["sid"] ?>"  ><?= $testi["sname"] ?></option>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="form-group">

                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>Growers</label>
                                                                                <select name="growercombo" id="growercombo" class="form-control pointer required select2" style="width:100%;">
                                                                                    <option value="">Select Growers</option>
                                                                                    <?php
                                                                                    $sel_testi = "select gpb.id,g.growers_name as gname,g.id as gid,g.blockstate as gbs from grower_product_box_packing gpb
                                                                                    left join product p on gpb.prodcutid = p.id
                                                                                    left join growers g on gpb.growerid=g.id		  
                                                                                    where g.active='active' and g.growers_name is not NULL ";

                                                                                    $sel_testi.=" group by g.id order by g.growers_name";
                                                                                    $rs_testi = mysqli_query($con, $sel_testi);
                                                                                    $total_testi = mysqli_num_rows($rs_testi);
                                                                                    ?><?php
                                                                                    while ($testi = mysqli_fetch_array($rs_testi)) {
                                                                                        if ($info["state"] > 0 && $testi["gbs"] != "") {
                                                                                            $temp47 = explode(",", $testi["gbs"]);
                                                                                            if (in_array($info["state"], $temp47)) {
                                                                                                
                                                                                            } else {
                                                                                                ?>
                                                                                                <option value="<?= $testi["gid"] ?>"  ><?= $testi["gname"] ?></option>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            ?>
                                                                                            <option value="<?= $testi["gid"] ?>"  ><?= $testi["gname"] ?></option>
                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>Price</label>
                                                                                <input type="text" name="wprice" id="wprice" class="form-control" >   
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </fieldset>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <button type="submit" class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30">
                                                                            <i class="fa fa-search"></i> Add Product 
                                                                            <span class="block font-lato">Search over more than 4000 varieties using your criteria</span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Grower</th>
                                                    <th>Price</th>
                                                    <th>Update</th>
                                                    <th>Remove</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sel_watch = "select * from fwatch where buyer='" . $userSessionID . "'";
                                                $rs_watch = mysqli_query($con, $sel_watch);
                                                $u = 1;
                                                while ($products = mysqli_fetch_array($rs_watch)) {
                                                    $getImage = "select image_path from product where id= " . $products['product'];
                                                    $imgRes = mysqli_query($con, $getImage);
                                                    $image = mysqli_fetch_assoc($imgRes);
                                                    ?>
                                                    <tr>
                                                        <td><?= $products["subcatname"] ?> <?= $products["productname"] ?> <?= $products["featurename"] ?> <?= $products["sizename"] ?>cm <a href="" data-toggle="modal" data-target="#single_product_modal<?= $products["id"] ?>"><i class="fa fa-camera"></i></a>
                                                            <!--Modal image for single product-->
                                                            <div class="modal fade bs-example-modal-sm" id="single_product_modal<?= $products["id"] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">Ãƒâ€”</button>
                                                                            <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;"><?= $products["subcatname"] ?> <?= $products["productname"] ?> <?= $products["featurename"] ?> <?= $products["sizename"] ?>cm</h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <img src="<?php echo SITE_URL . $image['image_path']; ?>" width="100%">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!---->
                                                        </td>
                                                        <td><?php
                                                            if ($products["growername"] != "") {
                                                                echo $products["growername"];
                                                            } else {
                                                                echo "All";
                                                            }
                                                            ?></td>
                                                        <td>$<?= $products["wprice"] ?></td>
                                                        <td><button class="btn btn-purple btn-xs" style="background-color:#8a2b83;color:#fff;" data-toggle="modal" data-target=".update_modal_<?= $products["id"] ?>"><i class="fa fa-gear" style="color:#fff;"></i> Update</button>
                                                            <!--Update pop from Start-->
                                                            <div class="modal fade update_modal_<?= $products["id"] ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg">
                                                                    <div class="modal-content">
                                                                        <!-- header modal -->
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            <h4 class="modal-title" id="myLargeModalLabel">Update Product</h4>
                                                                        </div>
                                                                        <!-- body modal -->
                                                                        <div class="modal-body">
                                                                            <div id="showMsg<?= $products["id"] ?>" style="display: none;color:#030; font-size:22px; text-align:center; font-weight:bold;"> </div>
                                                                            <table class="table table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>Product</th>
                                                                                        <th>Grower</th>
                                                                                        <th>Price</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <!--Update box section-->	 	
                                                                                    <tr>
                                                                                        <td><?= $products["subcatname"] ?> <?= $products["productname"] ?> <?= $products["featurename"] ?> <?= $products["sizename"] ?>cm </td>
                                                                                        <td><?php
                                                                                            if ($products["growername"] != "") {
                                                                                                echo $products["growername"];
                                                                                            } else {
                                                                                                echo "All";
                                                                                            }
                                                                                            ?></td>
                                                                                        <td><input type="text" name="wprice<?= $products["id"] ?>" id="wPrice<?= $products["id"] ?>" value="<?php echo $products["wprice"]; ?>" /></td>
                                                                                    </tr>
                                                                                    <!--Update box section-->
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <!-- Modal Footer -->
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <a href="javascript:void(0)" onclick="updateRequest('<?= $products["id"] ?>', 'update')" class="btn btn-primary">Save changes</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--Update pop from End-->
                                                        </td>  
                                                        <td><a href="javascript:void(0)" onclick="updateRequest('<?= $products["id"] ?>', 'delete')" class="btn btn-purple btn-xs" style="background-color:#8a2b83;color:#fff;"><i class="fa fa-trash" style="color:#fff;"></i> Delete</a></td>  
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>		
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Modal Table-->
                    </td>
                    </tr>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /MIDDLE -->
<?php include("includes/footer_new.php"); ?>
<script>
    function updateRequest(id, type) {
        var price = $('#wPrice' + id).val();
        if (type == 'delete') {
            k = confirm('Are you sure you want to remove this watch ?')
            if (k == true)
            {
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>update_freshwatch_ajax.php',
                    data: 'id=' + id + '&type=' + type,
                    success: function (data) {
                        if (data == 'delete true') {
                            $('#showMsg').html('Your fresh watch  has been deleted successfully. ');
                            $('#showMsg').show();
                        } else {
                            $('#showMsg').html('There is some error. Please try again');
                            $('#showMsg').show();
                        }
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                });
            }
        } else {
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>update_freshwatch_ajax.php',
                data: 'id=' + id + '&type=' + type + '&price=' + price,
                success: function (data) {
                    if (data == 'update true') {
                        $('#showMsg' + id).html('Your fresh watch  has been updated successfully. ');
                        $('#showMsg' + id).show();
                    } else {
                        $('#showMsg' + id).html('There is some error. Please try again');
                        $('#showMsg' + id).show();
                    }
                    setTimeout(function () {
                        location.reload();
                    }, 2000);

                }
            });
        }
    }

    function filterByProductWatch(pid) {
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>filter_freshwatch_ajax.php',
            data: 'pid=' + pid,
            success: function (data) {
                dt = jQuery.parseJSON(data);
                if (!jQuery.isEmptyObject(dt.specialcombo)) {
                    $('#specialcombo').html(dt.specialcombo);
                } else {
                    $('#specialcombo').html('');
                }
                if (!jQuery.isEmptyObject(dt.productcombo)) {
                    $('#productcombo').html(dt.productcombo);
                } else {
                    $('#productcombo').html('');
                }
                if (!jQuery.isEmptyObject(dt.sizecombo)) {
                    $('#sizecombo').html(dt.sizecombo);
                } else {
                    $('#sizecombo').html('');
                }
                if (!jQuery.isEmptyObject(dt.growercombo)) {
                    $('#growercombo').html(dt.growercombo);
                } else {
                    $('#growercombo').html('');
                }
            }
        });
    }

    function filterByVarietyWatch(vid) {
        var pid = $('#subcategorycombo').val();
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>filter_freshwatch_variety_ajax.php',
            data: 'vid=' + vid + '&pid=' + pid,
            success: function (data) {
                var dt = jQuery.parseJSON(data);
                if (!jQuery.isEmptyObject(dt.sizecombo)) {
                    $('#sizecombo').html(dt.sizecombo);
                } else {
                    $('#sizecombo').html('');
                }
                if (!jQuery.isEmptyObject(dt.specialcombo)) {
                    $('#specialcombo').html(dt.specialcombo);
                } else {
                    $('#specialcombo').html('');
                }
                if (!jQuery.isEmptyObject(dt.growercombo)) {
                    $('#growercombo').html(dt.growercombo);
                } else {
                    $('#growercombo').html('');
                }
            }
        });
    }

    function filterBySizeWatch(sid) {
        var pid = $('#subcategorycombo').val();
        var vid = $('#productcombo').val();
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>filter_freshwatch_size_ajax.php',
            data: 'vid=' + vid + '&pid=' + pid + '&sid=' + sid,
            success: function (data) {
                var dt = jQuery.parseJSON(data);
                if (!jQuery.isEmptyObject(dt.specialcombo)) {
                    $('#specialcombo').html(dt.specialcombo);
                } else {
                    $('#specialcombo').html('');
                }
                if (!jQuery.isEmptyObject(dt.growercombo)) {
                    $('#growercombo').html(dt.growercombo);
                } else {
                    $('#growercombo').html('');
                }
            }
        });
    }
    $(function () {
        $('form.searchProduct').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>add_freshwatch_ajax.php',
                data: $('form.searchProduct').serialize(),
                success: function (data) {
                    if (data == 'true') {
                        $('#showMsgAdd').html('Product added successfully.');
                        $('#showMsgAdd').css('color','#030').show();
                        setTimeout(function () {
                        location.reload();
                    }, 2000);
                    } else if (data == 'pricenull') {
                        $('#showMsgAdd').html('Please enter price');
                        $('#showMsgAdd').css('color','#f00').show();
                    } else {
                        $('#showMsgAdd').html('There is some error. Please try again.');
                        $('#showMsgAdd').css('color','#f00').show();
                        setTimeout(function () {
                        location.reload();
                    }, 2000);
                    }
                }
            });
        });
    });
</script>
