<?php
include("config/config_new.php");
$pid = $_POST['pid'];
$vid = $_POST['vid'];
$sid = $_POST['sid'];

// specialcombo select box refill
$specialcombo = '<option value="">Select Special Feature</option>';

$sel_specialcombo = "select gpb.id,s.name as sname,s.id as sid from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join features s on gpb.feature=s.id
			  left join growers g on gpb.growerid=g.id
			  where g.active='active' and s.name is not NULL ";
if ($pid != "") {
    $sel_specialcombo.=" and p.subcategoryid IN (" . $pid . ")";
}
if ($vid != "") {
    $sel_specialcombo.=" and  gpb.prodcutid IN (" . $vid . ")";
}
if ($sid != "") {
    $sel_testi.=" and  gpb.sizeid IN (" . $sid . ")";
}
$sel_specialcombo .= " group by gpb.feature order by s.name";
$rs_specialcombo = mysqli_query($con, $sel_specialcombo);
while ($specialcomboi = mysqli_fetch_array($rs_specialcombo)) {
    $specialcombo .='<option value="' . $specialcomboi["sid"] . '">' . trim($specialcomboi["sname"]) . '</option>';
}

$growercombo ='<option value="">Select Growers</option>';

$sel_growercombo = "select gpb.id,g.growers_name as gname,g.id as gid,g.blockstate as gbs from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join growers g on gpb.growerid=g.id		  
			  where g.active='active' and g.growers_name is not NULL ";
if ($pid != "") {
    $sel_growercombo.=" and p.subcategoryid IN (" . $pid . ")";
}
if ($vid != "") {
    $sel_growercombo.=" and  gpb.prodcutid IN (" . $vid . ")";
}
if ($sid != "") {
    $sel_testi.=" and  gpb.sizeid IN (" . $sid . ")";
}
$sel_growercombo .= " group by g.id order by g.growers_name";
$rs_growercombo = mysqli_query($con, $sel_growercombo);
while ($growercomboi = mysqli_fetch_array($rs_growercombo)) {
    $growercombo .='<option value="' . $growercomboi["gid"] . '">' . trim($growercomboi["gname"]) . '</option>';
}

$output = array();
if (!empty($specialcombo)) {
    $output['specialcombo'] = utf8_encode($specialcombo);
} else {
    $output['specialcombo'] = '';
}
if (!empty($growercombo)) {
    $output['growercombo'] = utf8_encode($growercombo);
} else {
    $output['growercombo'] = '';
}
echo json_encode($output);
die;
?>
