<?php
require_once("config/config_new.php");
if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}
if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor-account.php");
    die;
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = 'profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = 'profile_images/' . $profile_image;
}

if (isset($_GET["delete"])) {
    $insert = "delete from offer_cart where id='" . $_GET["delete"] . "' and buyer='" . $_SESSION["buyer"] . "'";
    mysqli_query($con, $insert);
    $k = mysqli_affected_rows();
    if ($k == 1) {
        $message = 1;

        $name = $info["first_name"] . " " . $info["last_name"];
        $activity = "Deleted offer from Name your pirce at " . $hm;
        $insert_login = "insert into activity set buyer='" . $info["id"] . "',ldate='" . date("Y-m-d") . "',type='buyer',name='" . $name . "',activity='" . $activity . "',ltime='" . $hm . "',atype='np'";
        mysqli_query($con, $insert_login);
    }
}


$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);


$shipping_method = trim($info["shipping"], ",");
if ($shipping_method > 0) {
    $select_shipping_info = "select name,description from shipping_method where id='" . $shipping_method . "'";
    $rs_shipping_info = mysqli_query($con, $select_shipping_info);
    $shipping_info = mysqli_fetch_array($rs_shipping_info);
}


$sel_testi = "select gpb.id,g.growers_name as gname,g.id as gid,g.blockstate as gbs from grower_product_box_packing gpb
			   left join product p on gpb.prodcutid = p.id
			   left join growers g on gpb.growerid=g.id		  
			   where g.active='active' and g.growers_name is not NULL ";
$sel_testi.=" group by g.id order by g.growers_name";
$rs_testi = mysqli_query($con, $sel_testi);
$total_testi = mysqli_num_rows($rs_testi);

if ($total_testi >= 1) {
    $check_grower = "";
    while ($testi = mysqli_fetch_array($rs_testi)) {
        if ($info["state"] > 0 && $testi["gbs"] != "") {
            $temp47 = explode(",", $testi["gbs"]);
            if (in_array($info["state"], $temp47)) {
                
            } else {
                $check_grower.=$testi["gid"] . ",";
            }
        } else {
            $check_grower.=$testi["gid"] . ",";
        }
    }
}

$check_grower = rtrim($check_grower, ",");


$sel_connections = "select days from connections where shipping_id='" . $shipping_method . "' order by id desc limit 0,1";
$rs_connections = mysqli_query($con, $sel_connections);
$connections = mysqli_fetch_array($rs_connections);

$days = explode(",", $connections["days"]);
$count1 = sizeof($days);

$res = "";
for ($io = 0; $io <= $count1 - 1; $io++) {
    if ($days[$io] == 0 || $days[$io] == 1) {

        $res.="6";
        $res.=",";
    } else {
        $res.=$days[$io] - 1;
        $res.=",";
    }
}

$avdays = explode(",", $res);
$result = array_unique($avdays);
$cunav = sizeof($result);

$sel_products = "select gpb.id from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join growers g on gpb.growerid=g.id
			  where g.active='active' and p.name is not null ";

$sel_products.=" group by gpb.prodcutid,gpb.sizeid,gpb.feature";
$rs_prodcuts = mysqli_query($con, $sel_products);
$total = mysqli_num_rows($rs_prodcuts);

$num_record = $total;
$display = 50;

$query2 = "select gpb.prodcutid,gpb.comment,gpb.id as gid,gpb.stock as stock,gpb.qty,gpb.sizeid,gpb.feature,gpb.type as bv,gpb.boxname as bvname,gpb.box_type as bvtype,gpb.growerid,gpb.price,p.id,p.name,p.color_id,p.image_path,p.box_type as p_box_type,s.name as subs,g.file_path5,g.growers_name,sh.name as sizename,ff.name as featurename,b.name as boxname,b.width,b.length,b.height,bs.name as bname,bt.name as boxtype,bt.id as boxtypeid,b.type,c.name as colorname,gpb.box_id from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join subcategory s on p.subcategoryid=s.id  
	          left join colors c on p.color_id=c.id 
			  left join features ff on gpb.feature=ff.id
			  left join sizes sh on gpb.sizeid=sh.id 
			  left join boxes b on gpb.box_id=b.id
			  left join boxtype bt on b.type=bt.id
			  left join growers g on gpb.growerid=g.id
			  left join bunch_sizes bs on gpb.bunch_size_id=bs.id
			  where g.active='active' and p.name  is not null  and sh.name is not null and  bt.name is not null";

$query2.=" ";
$query2.= "order by p.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER) LIMIT 0,$display";
$result2 = mysqli_query($con, $query2);
$page_request = "counter_bid";
?>
<?php require_once 'includes/profile-header.php'; ?>
<?php require_once "includes/left_sidebar_buyer.php"; ?>


<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Bootstrap Tables</h1>
        <ol class="breadcrumb">
            <li><a href="#">Tables</a></li>
            <li class="active">Bootstrap Tables</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">

        <div class="panel panel-default">
            <div class="panel-body">

                <div class="row">

                    <!-- LEFT -->
                    <div class="col-md-12">
                        <div id="content" class="padding-20">


                            <div id="panel-2" class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="title elipsis">
                                        <strong>Request for Product</strong> <!-- panel title -->
                                    </span>

                                    <!-- right options -->
                                    <ul class="options pull-right list-inline">
                                        <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Colapse"></a></li>
                                        <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand"></i></a></li>
                                        <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Close"><i class="fa fa-times"></i></a></li>
                                    </ul>
                                    <!-- /right options -->

                                </div>

                                <!-- panel content -->

                                <div class="panel-body">.
                                    <div class="autosuggest fancy-form" data-minLength="1" data-queryURL="php/view/demo.autosuggest.php?limit=10&search=">
                                        <input type="text" name="src" placeholder="Search From Here..." class="form-control typeahead" />
                                        <i class="fa fa-arrow-down to_right" id="on_click"></i>
                                    </div>
                                    <form class="searchProduct"   method="post" enctype="multipart/form-data" data-success="Sent! Thank you!" data-toastr-position="top-right" id="show_hide">
                                        <div class="panel-heading panel-heading-transparent">
                                            <strong>REFINE YOUR SEARCH</strong><hr>
                                        </div>
                                        <fieldset>
                                            <!-- required [php action request] -->
                                            <input type="hidden" name="action" value="search_inventory_send" />

                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6">
                                                        <label>Variety</label>
                                                        <select name="productcombo" id="productcombo" class="form-control pointer required select2" style="width:100%;">
                                                            <option value="">Select Variety</option>
                                                            <?php
                                                            $sel_testi = "select gpb.id,p.name as pname,p.id as pid from grower_product_box_packing gpb
                                                                left join product p on gpb.prodcutid = p.id
                                                                left join growers g on gpb.growerid=g.id
                                                                where g.active='active' and p.name is not NULL ";
                                                            $sel_testi.=" group by gpb.prodcutid order by p.name";
                                                            $rs_testi = mysqli_query($con, $sel_testi);
                                                            $m = 1;
                                                            while ($testi = mysqli_fetch_array($rs_testi)) {
                                                                ?>	
                                                                <option value="<?= $testi["pid"] ?>"><?= $testi["pname"] ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <label>Size</label>
                                                        <select name="sizecombo" id="sizecombo" class="form-control pointer required select2" style="width:100%;">
                                                            <option value="">Select Size</option>
                                                            <?php
                                                            $sel_testi = "select gpb.id,s.name as sname,s.id as sid from grower_product_box_packing gpb
                                                            left join product p on gpb.prodcutid = p.id
                                                            left join sizes s on gpb.sizeid=s.id
                                                            left join growers g on gpb.growerid=g.id
                                                            where g.active='active' and s.name is not NULL ";
                                                            $sel_testi.=" group by gpb.sizeid order by CONVERT(SUBSTRING(s.name,1), SIGNED INTEGER)";
                                                            $rs_testi = mysqli_query($con, $sel_testi);
                                                            $k = 1;
                                                            while ($testi = mysqli_fetch_array($rs_testi)) {
                                                                ?>		  
                                                                <option value="<?= $testi["sid"] ?>" ><?= $testi["sname"] ?>cm</option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-6">
                                                        <label>Special Feature</label>
                                                        <select name="specialcombo" id="specialcombo" class="form-control pointer required select2" style="width:100%;">
                                                            <option value="">Select Special Feature</option>
                                                            <?php
                                                            $sel_testi = "select gpb.id,s.name as sname,s.id as sid from grower_product_box_packing gpb
                                                            left join product p on gpb.prodcutid = p.id
                                                            left join features s on gpb.feature=s.id
                                                            left join growers g on gpb.growerid=g.id
                                                            where g.active='active' and s.name is not NULL ";

                                                            $sel_testi.=" group by gpb.feature order by s.name";
                                                            $rs_testi = mysqli_query($con, $sel_testi);
                                                            $total_testi = mysqli_num_rows($rs_testi);
                                                            while ($testi = mysqli_fetch_array($rs_testi)) {
                                                                ?>	
                                                                <option value="<?= $testi["sid"] ?>"  ><?= $testi["sname"] ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <label>Growers</label>
                                                        <select name="growercombo" id="growercombo" class="form-control pointer required select2" style="width:100%;">
                                                            <option value="">Select Growers</option>
                                                            <?php
                                                            $sel_testi = "select gpb.id,g.growers_name as gname,g.id as gid,g.blockstate as gbs from grower_product_box_packing gpb
                                                                                    left join product p on gpb.prodcutid = p.id
                                                                                    left join growers g on gpb.growerid=g.id		  
                                                                                    where g.active='active' and g.growers_name is not NULL ";
                                                            $sel_testi.=" group by g.id order by g.growers_name";
                                                            $rs_testi = mysqli_query($con, $sel_testi);
                                                            $total_testi = mysqli_num_rows($rs_testi);
                                                            ?><?php
                                                            $check_grower = "";
                                                            while ($testi = mysqli_fetch_array($rs_testi)) {
                                                                ?>                     
                                                                <?php
                                                                if ($info["state"] > 0 && $testi["gbs"] != "") {
                                                                    $temp47 = explode(",", $testi["gbs"]);
                                                                    if (in_array($info["state"], $temp47)) {
                                                                        
                                                                    } else {
                                                                        // $check_grower.=$testi["gid"].",";
                                                                        ?>


                                                                        <option value="<?= $testi["gid"] ?>" ><?= $testi["gname"] ?></option>
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    //$check_grower.=$testi["gid"].",";
                                                                    ?>
                                                                    <option value="<?= $testi["gid"] ?>" ><?= $testi["gname"] ?></option>
                                                                    <?php
                                                                }
                                                                ?>	

                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class="imgDiv" style="width:100%; text-align: center; display: none;"><img src="<?php echo SITE_URL; ?>/includes/assets/images/loaders/5.gif" id="loaderImg" /></div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-3d btn-teal btn-xlg btn-block margin-top-30">
                                                    <i class="fa fa-search"></i> REFINE SELECTION 
                                                    <span class="block font-lato">Search over more than 4000 varieties using your criteria</span>
                                                </button>
                                            </div>
                                        </div>

                                    </form>
                                    <div class="dataRequest">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Grower</th>
                                                    <th>Product</th>
                                                    <th>Bunch/Stem</th>
                                                    <th>Price</th>
                                                    <!-- <th>Box QTY.</th> -->
                                                    <th>Counter Bid</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $u = 1;
                                                $i=0;
                                                while ($products = mysqli_fetch_array($result2)) {
                                                    $total = 0;
                                                    $sel_price = "select price from grower_product_price where growerid='" . $products["growerid"] . "' and prodcutid='" . $products["prodcutid"] . "' and sizeid='" . $products["sizeid"] . "' and feature='" . $products["feature"] . "' order by id desc limit 0,1";
                                                    $rs_price = mysqli_query($con, $sel_price);
                                                    $price = mysqli_fetch_array($rs_price);
                                                    // $price["price"]=$products["price"];
                                                    if ($products["growerid"] != "407") {

                                                        $sel_weight = "select weight from grower_product_box_weight where growerid='" . $products["growerid"] . "' and prodcutid='" . $products["prodcutid"] . "' and sizeid='" . $products["sizeid"] . "' and feature='" . $products["feature"] . "' and box_id='" . $products["box_id"] . "' order by id desc limit 0,1";
                                                        $rs_weight = mysqli_query($con, $sel_weight);
                                                        $weight = mysqli_fetch_array($rs_weight);
                                                    } else {
                                                        $sel_weight = "select weight from grower_product_box_weight where growerid='" . $products["growerid"] . "' and prodcutid='" . $products["prodcutid"] . "' and sizeid='" . $products["sizeid"] . "' and feature='" . $products["feature"] . "' order by id desc limit 0,1";
                                                        $rs_weight = mysqli_query($con, $sel_weight);
                                                        $weight = mysqli_fetch_array($rs_weight);
                                                    }
                                                    $growers["box_weight"] = $weight["weight"];
                                                    $growers["box_volumn"] = round(($products["width"] * $products["length"] * $products["height"]) / 1728, 2);
                                                    $growers["shipping_method"] = $shipping_method;
                                                    if ($growers["shipping_method"] > 0) {
                                                        $total_shipping = 0;
                                                        $sel_connections = "select type,shipping_rate,addtional_rate,box_price,box_weight_arranged,price_per_box from connections where shipping_id='" . $growers["shipping_method"] . "'";
                                                        $rs_connections = mysqli_query($con, $sel_connections);
                                                        while ($connections = mysqli_fetch_array($rs_connections)) {
                                                            if ($connections["type"] == 2) {
                                                                $total_shipping = $total_shipping + round(($growers["box_weight"] * $connections["shipping_rate"]), 2);
                                                            } else if ($connections["type"] == 4) {
                                                                $total_shipping = $total_shipping + round(($growers["box_volumn"] * $connections["shipping_rate"]), 2);
                                                            } else if ($connections["type"] == 1) {
                                                                $box_type_calc = $products["boxtype"];

                                                                if ($box_type_calc == 'HB') {
                                                                    $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2;
                                                                    $connections["price_per_box"] = $connections["price_per_box"] / 2;
                                                                }
                                                                if ($box_type_calc == 'JUMBO') {
                                                                    $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2;
                                                                    $connections["price_per_box"] = $connections["price_per_box"] / 2;
                                                                } else if ($box_type_calc = 'QB') {
                                                                    $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 4;
                                                                    $connections["price_per_box"] = $connections["price_per_box"] / 4;
                                                                } else if ($box_type_calc = 'EB') {
                                                                    $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 8;
                                                                    $connections["price_per_box"] = $connections["price_per_box"] / 8;
                                                                }
                                                                $connections["shipping_rate"] = $connections["price_per_box"] / $connections["box_weight_arranged"];
                                                                if ($growers["box_weight"] <= $connections["box_weight_arranged"]) {
                                                                    $total_shipping = $total_shipping + round(( $growers["box_weight"] * $connections["shipping_rate"]), 2);
                                                                } else {
                                                                    $total_shipping = $total_shipping + round(($connections["box_weight_arranged"] * $connections["shipping_rate"]), 2);
                                                                    $remaining = round($growers["box_weight"] - $connections["box_weight_arranged"], 2);
                                                                    $total_shipping = $total_shipping + round(($remaining * $connections["addtional_rate"]), 2);
                                                                }
                                                            } else if ($connections["type"] == 3) {
                                                                $total_shipping = $total_shipping + $connections["box_price"];
                                                            }
                                                        }
                                                        // echo $total_shipping;
                                                    }

                                                    $k = explode("/", $products["file_path5"]);
                                                    $data = getimagesize(SITE_URL . "user/logo2/" . $k[1]);
                                                    $logourl = SITE_URL . "user/logo/" . $k[1];
                                                    ?>
                                                    <tr>
                                                        <td>
                                                        <a href="" data-toggle="modal" data-target="#single_product_<?= $products["gid"] ?>" style="color:#8A2B83;">
                                                        <?php 
                                                            $getprofile = "SELECT profile_image,file_path5 FROM growers WHERE id='".$products["growerid"]."'";
                                                            $row_getprofile = mysqli_query($con, $getprofile);
                                                            $row_Profile=mysqli_fetch_assoc($row_getprofile);
                                                            //echo "<pre>";print_r($row_Profile);echo "</pre>";
                                                            if($row_Profile['profile_image'] != "")
                                                            { ?>
                                                                <img width="50" src="<?php echo SITE_URL."user/".$row_Profile['profile_image'];?>">
                                                            <?php }
                                                            else if($row_Profile['file_path5'] != "")
                                                            { ?>
                                                                <img width="50" src="<?php echo SITE_URL."user/".$row_Profile['file_path5'];?>">
                                                            <?php } ?>
                                                        </a>
                                                            <!--Modal image for 3452-->
                                                            <div class="modal fade bs-example-modal-sm" id="single_product_<?= $products["gid"] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                                            <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;"><img width="100%" src="<?php echo $logourl ?>" /></h4>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!---->
                                                        </td>
                                                        <td><a href="" data-toggle="modal" data-target="#single_product_modal_<?= $products["gid"] ?>"><?= $products["subs"] ?> <?= $products["name"] ?> <?= $products["featurename"] ?> <?= $products["colorname"] ?> <?= $products["sizename"] ?>cm</a>
                                                            <!--Modal image for single product-->
                                                            <div class="modal fade bs-example-modal-sm" id="single_product_modal_<?= $products["gid"] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                                            <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;"><?= $products["subs"] ?> <?= $products["name"] ?> <?= $products["featurename"] ?> <?= $products["colorname"] ?> <?= $products["sizename"] ?>cm  </h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <img src="<?php echo SITE_URL . $products["image_path"]; ?>" width="100%">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!---->
                                                        </td>
                                                        
                                                        <td><?php //$products["bname"] * $products["qty"]
                                                            $box_type_s = "";
                                                            if($products['p_box_type'] == "0")
                                                            {
                                                                $box_type_s = "Stem";
                                                            }   
                                                            else if($products['p_box_type'] == "1")
                                                            {
                                                                $box_type_s = "Bunch";
                                                            } 
                                                            echo $box_type_s;
                                                         ?>
                                                        </td>
                                                        <?php
                                                        if ($total_shipping > 0) {
                                                            $product_shipping = round($total_shipping / ($products["bname"] * $products["qty"]), 2);
                                                        }
                                                        $total_price = $price["price"];
                                                        $total_taxs = 0;
                                                        $total_handling_feess = 0;
                                                        if ($info["tax"] != 0) {
                                                            $total_price = $total_price + (($price["price"] * $info["tax"]) / 100);
                                                            $total_taxs = ($price["price"] * $info["tax"]) / 100;
                                                        }
                                                        if ($info["handling_fees"] != 0) {
                                                            $total_price = $total_price + (($price["price"] * $info["handling_fees"] ) / 100);
                                                            $total_handling_feess = ($price["price"] * $info["handling_fees"]) / 100;
                                                        }
                                                        if ($product_shipping > 0) {
                                                            $total_price = $total_price + $product_shipping;
                                                            $product_shippings = $product_shipping;
                                                        }

                                                        $total_price = round(($total_price), 2);
                                                        $final_price2 = $total_price;
                                                        ?>	
                                                        
                                                        <td>
                                                            <?php if ($price["price"] > 0) { ?>
                                                                <a href="" data-toggle="modal" data-target="#single_product_price_<?= $products["gid"] ?>" style="color:#8A2B83;">$<?= sprintf("%.2f", $total_price) ?></a>
                                                            <?php } else { ?>
                                                                <a href="javascript:void(0);" style="color:#8A2B83;">N/A</a>
                                                            <?php } ?>
                                                            <!--Modal image for 3452-->
                                                            <div class="modal fade bs-example-modal-sm" id="single_product_price_<?= $products["gid"] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                                            <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;">ALL IN PRICE</h4>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <p>Grower Price.............$<?= sprintf("%.2f", $price["price"]) ?></p>
                                                                                    <p>Shipping.....................$<?= sprintf("%.2f", $product_shippings) ?></p>
                                                                                    <p>Tax..............................$<?= sprintf("%.2f", $total_taxs) ?></p>
                                                                                    <p>Handling (8,5%).........$<?= sprintf("%.2f", $total_handling_feess) ?></p>
                                                                                    <hr>
                                                                                    <p>Final Price..................$<?= sprintf("%.2f", $total_price) ?></p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>

                                                       <!--  <td><select class="form-control pointer" id="change-page-size" style="width:93px;height:25px;padding: 0px 12px!important;">
                                                                <option value="2">1</option>
                                                                <option value="3">2</option>
                                                                <option value="5">3</option>
                                                                <option value="10" selected="selected">4</option>
                                                                <option value="15">5</option>
                                                                <option value="20">6</option>
                                                            </select></td>         -->
                                                        <td><a href="javascript:void(0);" data-toggle="modal" data-target=".price_modal_<?php echo $i;?>" class="btn btn-purple btn-xs" style="background-color: #ffffff;border: 1px solid #ccc;color: #000 !important;"><i class="fa fa-gavel white" style="color:#000;"></i> 100</a>
                                                            <div aria-hidden="true" aria-labelledby="mySmallModalLabel" role="dialog" tabindex="-1" class="modal fade price_modal_<?php echo $i;?>">
                                                                <div class="modal-dialog modal-lg">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                                                                        <h4 class="modal-title" id="myModalLabel">Order Details</h4>
                                                                    </div>
                                                                    <div class="modal-body">

                                                                    <h4>Please Select Quantity</h4>
                                <div class="stepper-wrap" style="margin: 0px 49px 0px 0px;"><input type="text" value="2" min="0" max="1000" class="form-control stepper" id="qty_amount" style="margin: 0px;"><div class="stepper-btn-wrap"><a class="stepper-btn-up">▴</a><a class="stepper-btn-dwn">▾</a></div></div>
                                    <hr>
                                                                    <h4>Price</h4>
                                                                        <div class="row">
                                                                        <div class="col-md-12">
                                                                        
                                <div class="margin-bottom-20">
                                    <label for="donation">Select your price</label>
                                    <label class="field">
                                        <input type="text" id="select_price_cls" class="form-control select_price_cls">
                                    </label> 
                                </div>                
                                    <div class="slider-wrapper black-slider">
                                       <div id="slider5"></div>
                                    </div>
                                </div>
                                                                            <div class="col-md-12">
                                                                               <div class="table-responsive" style="margin-top:30px;">
                                                            <table class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                       
                                                                        <th>Bunch/Stem</th>
                                                                        <th>Grower Price</th>
                                                                        <th>Shipping Cost</th>
                                                                        <th>Tax</th>
                                                                        <th>Handling</th>
                                                                        <th>Final Price</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                            
                                                            <td>Stem</td>
                                                            
                                                            <td>$0.10</td>
                                                            
                                                            <td>$0.10</td>
                                                            <td>$0.10</td>            
                                                            <td>$0.10</td>
                                                        <td>$0.10</td>                      
                                                        </tr>
                                                        
                                                          
                                                         
                                                          
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                           <hr>
                                 <form class="validate" action="" method="post" enctype="multipart/form-data" novalidate="novalidate">
                                                                            <fieldset>
                                                                                <!-- required [php action request] -->
                                                                                

                                                                                

                                                                                

                                                                                <div class="row">
                                                                                    <div class="form-group">
                                                                                        <div class="col-md-12 col-sm-12">
                                                                                        <h4>Let your growers know if you have any special requirements</h4>
                                                                                            <label>Comment</label>
                                                                                            <textarea name="contact[experience]" rows="4" class="form-control required"></textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>



                                                                            </fieldset>


                                                                                               
                                                                                <input type="hidden" name="is_ajax" value="true"></form></div>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="button" class="btn btn-primary save_d" style="background:#8a2b83!important;" data-dismiss="modal">Save Details</button>
                                                                </div>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <input type="hidden" name="product-<?= $products["gid"] ?>" id="product-<?= $products["gid"] ?>"  value="<?= $products["prodcutid"] ?>" >
                                                <input type="hidden" name="subs-<?= $products["gid"] ?>" id="subs-<?= $products["gid"] ?>" value="<?= $products["subs"] ?>" >
                                                <input type="hidden" name="sizeid-<?= $products["gid"] ?>" id="sizeid-<?= $products["gid"] ?>" value="<?= $products["sizeid"] ?>" >
                                                <input type="hidden" name="feature-<?= $products["gid"] ?>" id="feature-<?= $products["gid"] ?>" value="<?= $products["feature"] ?>" >
                                                <input type="hidden" name="boxtype-<?= $products["gid"] ?>" id="boxtype-<?= $products["gid"] ?>" value="<?= $products["boxtypeid"] ?>" >
                                                <input type="hidden" name="growers-<?= $products["gid"] ?>" id="growers-<?= $products["gid"] ?>" value="<?= $products["growerid"] ?>" />
                                                <input type="hidden" name="boxtypename-<?= $products["gid"] ?>" id="boxtypename-<?= $products["gid"] ?>" value="<?= $products["boxtype"] ?>" >
                                                <input type="hidden" name="bunchsize-<?= $products["gid"] ?>" id="bunchsize-<?= $products["gid"] ?>" value="<?= $products["bname"] ?> Stems" >
                                                <input type="hidden" name="bunchqty-<?= $products["gid"] ?>" id="bunchqty-<?= $products["gid"] ?>" value="<?= $products["qty"] ?>" >
                                                <input type="hidden" name="shipping-<?= $products["gid"] ?>" id="shipping-<?= $products["gid"] ?>" value="<?= $product_shippings ?>" >
                                                <input type="hidden" name="handling-<?= $products["gid"] ?>" id="handling-<?= $products["gid"] ?>" value="<?= $total_handling_feess ?>" >
                                                <input type="hidden" name="tax-<?= $products["gid"] ?>" id="tax-<?= $products["gid"] ?>" value="<?= $total_taxs ?>" >
                                                <input type="hidden" name="totalprice-<?= $products["gid"] ?>" id="totalprice-<?= $products["gid"] ?>" value="<?= $final_price2 ?>" >
                                            <?php
                                                $i++; 
                                                } 
                                            ?>
                                            </tbody>
                                        </table>		

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--Modal Table-->
                    </td>
                    </tr>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /MIDDLE -->

</div>
<?php include("includes/footer_new.php"); ?>
<script type="text/javascript">
    jQuery(window).ready(function() {

        loadScript(plugin_path + 'jquery/jquery-ui.min.js', function() { /** jQuery UI **/
            loadScript(plugin_path + 'jquery/jquery.ui.touch-punch.min.js', function() { /** Mobile Touch Slider **/
                loadScript(plugin_path + 'form.slidebar/jquery-ui-slider-pips.min.js', function() { /** Slider Script **/

                    


                    /** Slider 5
                    ******************** **/
                    jQuery("#slider5").slider({
                        value:0.01,
                        animate: true,
                        min: 0,
                        max: 2,
                        step: 0.01,
                        range: "min",
                        slide: function(event, ui) {
                            jQuery(".select_price_cls").val(ui.value);
                        }
                    });
                    
                    jQuery(".select_price_cls").val(jQuery("#slider5").slider("value"));
                    jQuery(".select_price_cls").blur(function() {
                            jQuery(".select_price_cls").slider("value", jQuery(this).val());
                    });


                    
                    /*$sliderv1.slider("pips", { 
                        first: "pip", 
                        last: "pip"
                    });*/


                });
            });
        });

    });
</script>
<script>
    $(document).ready(function () {

        var productsearch = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '<?php echo SITE_URL; ?>includes/autosuggest.php?page_name=counterbid&limit=10&search=%QUERY',
                wildcard: '%QUERY'
            }
        });

        $('.autosuggest #typeSearch').typeahead(null, {
            name: 'productsearch',
            source: productsearch
        });
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            //console.log('Selection: ' + suggestion + ev);
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>counter_bid_search.php',
                data: 'name=' + suggestion,
                success: function (data) {
                    $('.imgDiv').hide();
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });
    });
    $(function () {
        $('form.searchProduct').on('submit', function (e) {
            e.preventDefault();
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>search_counter_bid_page.php',
                data: $('form.searchProduct').serialize(),
                success: function (data) {
                    $('.imgDiv').hide();
                    $("#show_hide").toggle("slow");
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });
    });
    function buynow(gid) {
        var bv = $('#bv-' + gid).val();
       // console.log(bv);
        var fields_name = {};
        fields_name['gid6'] = gid;
        var lfd = $('#lfd').val();
        var shipping = $('#shipping-' + gid).val();
        var handling = $('#handling-' + gid).val();
        var tax = $('#tax-' + gid).val();
        var totalprice = $('#totalprice-' + gid).val();
        var bunchsize = $('#bunchsize-' + gid).val();
        var bunchqty = $('#bunchqty-' + gid).val();
        var boxtypename = $('#boxtypename-' + gid).val();
        var oprice = $('#oprice-' + gid).val();
        var qty = $('#qty-' + gid).val();
        var product = $('#product-' + gid).val();
        var subs = $('#subs-' + gid).val();
        var sizeid = $('#sizeid-' + gid).val();
        var feature = $('#feature-' + gid).val();
        var boxtype = $('#boxtype-' + gid).val();
        var growers = $('#growers-' + gid).val();
            
            fields_name['lfd'] = lfd;
            fields_name['qty'] = qty;
            fields_name['product'] = product;
            fields_name['subs'] = subs;
            fields_name['growers'] = growers;
            fields_name['bunchsize'] = bunchsize;
            fields_name['bunchqty'] = bunchqty;
            fields_name['shipping'] = shipping;
            fields_name['handling'] = handling;
            fields_name['tax'] = tax;
            fields_name['totalprice'] = totalprice;
            fields_name['oprice'] = oprice;
            fields_name['boxtypename'] = boxtypename;
            fields_name['boxtype'] = boxtype;
            fields_name['sizeid'] = sizeid;
            fields_name['feature'] = feature;
        //console.log(fields_name);
        $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>counter_bid_ajax.php',
                data: fields_name,
                success: function (data) {
                    if(data == 'true'){
                    alert('Your reuqest has been sent...');
                    }else{
                        alert('There is some error. Please try again');
                    }
                }
            });
    }
</script>
