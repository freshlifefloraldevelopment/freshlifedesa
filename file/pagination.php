<?php
function pagination($num_record, $per_page, $page)
{//$query = "SELECT COUNT(*) as `num` FROM {$query}";
    //$query = $query_product;
    //$row_cnt=mysql_num_rows(mysql_query($query_product));
    $total = $num_record;
    $adjacents = "2";
    $page = ($page == 0 ? 1 : $page);
    $start = ($page - 1) * $per_page;
    $counter = 1;
    $prev = $page - 1;
    $next = $page + 1;
    $lastpage = ceil($total / $per_page);
    $lpm1 = $lastpage - 1;
    $pagination = "";
    $id = "employee";
    //echo $page."<".$counter." - 1";
    if ($lastpage > 1) {
        $pagination .= "<ul class='pagination' id='pagination_main'>";
        if ($page > $counter) {
            $pagination .= "<li><a href='?page=1' value='$id' >First</a></li>";
            $pagination .= "<li><a href='?page=$prev' value='$id'>Pervious</a></li>";
        } else {
            $pagination .= "<li class=''><a href='javascript:void(0);' value='$id'>First</a></li>";
            $pagination .= "<li class=''><a href='javascript:void(0);' value='$id'>Pervious</a></li>";
        }
        if ($lastpage < 7 + ($adjacents * 2)) {
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page)
                    $pagination .= "<li class='active'><a href='javascript:void(0);' value='$id'>$counter</a></li>";
                else
                    $pagination .= "<li><a href='?page=$counter' value='$id'>$counter</a></li>";
            }
        } elseif ($lastpage > 5 + ($adjacents * 2)) {
            if ($page < 1 + ($adjacents * 2)) {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class='active'><a href='javascript:void(0);' value='$id'>$counter</a></li>";
                    else
                        $pagination .= "<li><a href='?page=$counter' value='$id'>$counter</a></li>";
                }

                $pagination .= "<li class='dot'>...</li>";
                $pagination .= "<li><a href='?page=$lpm1' value='$id'>$lpm1</a></li>";
                $pagination .= "<li><a href='?page=$lastpage' value='$id'>$lastpage</a></li>";
            } elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                $pagination .= "<li><a href='?page=1' value='$id'>1</a></li>";
                $pagination .= "<li><a href='?page=2' value='$id'>2</a></li>";
                $pagination .= "<li class='dot' value='$id'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
                    else
                        $pagination .= "<li><a href='?page=$counter' value='$id'>$counter</a></li>";
                }
                $pagination .= "<li class='dot'>..</li>";
                $pagination .= "<li><a href='?page=$lpm1' value='$id'>$lpm1</a></li>";
                $pagination .= "<li><a href='?page=$lastpage' value='$id'>$lastpage</a></li>";

            } else {
                $pagination .= "<li><a href='?page=1' value='$id'>1</a></li>";
                $pagination .= "<li><a href='?page=2' value='$id'>2</a></li>";
                $pagination .= "<li class='dot' value='$id'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class='active'><a href='javascript:void(0);' value='$id' >$counter</a></li>";
                    else
                        $pagination .= "<li><a href='?page=$counter' value='$id'>$counter</a></li>";
                }
            }
        }
        if ($page < $counter - 1) {
            $pagination .= "<li><a href='?page=$next' value='$id'>Next</a></li>";
            $pagination .= "<li><a href='?page=$lastpage' value='$id'>Last</a></li>";

        } else {
            $pagination .= "<li class=''><a href='javascript:void(0);' value='$id'>Next</a></li>";
            $pagination .= "<li class=''><a href='javascript:void(0);' value='$id'>Last</a></li>";
        }
        $pagination .= "</ul>\n";
    }
    return $pagination;
}

?>
