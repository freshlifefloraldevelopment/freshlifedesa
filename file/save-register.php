<?php   
require_once("config/config_new.php");
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
function generateRandomString($length,$f) {
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
    //return strtoupper($f).$randomString;
}

if (isset($_POST["submit"]) && $_POST["submit"] == '_register') {
    if ($_POST['frm_mode'] == 'buyer') {
        $message = "";
        // set parameters and execute
        $first_name = $_POST["first_name"];
        $last_name = $_POST["last_name"];
        $password = $_POST["password"];
        $phone = $_POST["phone"];
        $email = $_POST["email"];
        $activation=md5($email.time());
        $active = 'active';

        $stmt = $con->prepare("SELECT id FROM buyers WHERE email ='".$email."'");
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $stmt->bind_result($userID);
        $stmt->fetch();
        if(!empty($userID)){
            $message = "Email address is already exists";
        }
        if (strlen($password) < 6) {
            $message = "Password length should be at least 6 characters";
        }
        if ($password != $_POST["cpassword"]) {
            $message = "Password and confirm password should be same";
        }
        if ($message == "") {
            // prepare and bind
            $customer_code=generateRandomString(3,0);
            if ($stmt = $con->prepare("INSERT INTO buyers (first_name, last_name, password, email, phone,customer_code, active,activation) VALUES ('".$first_name."', '".$last_name."', '".$password."', '".$email."','".$phone."','".$customer_code."','".$active."','".$activation."')")) {
                    $stmt->bind_param("ssssss", $first_name, $last_name, $password, $email, $phone,$customer_code,$active,$activation);
                    $stmt->execute();
                    $stmt->close();
                    $lifecycle = 'subscriber';

                    //profile_completion table
                    $last_id=mysqli_insert_id($con);
                    //echo $last_id;
                    $pro_com_array=array("confirm-account","update-profile","contribution","shipping-information","payment-information");
                    for($i=0;$i<count($pro_com_array);$i++)
                    {
                        $pro_com_que="INSERT INTO profile_completion (buyer_id,profile,status,entry_date) VALUES('".$last_id."','".$pro_com_array[$i]."',0,'".date("Y-m-d H:i:s")."')";    
                        mysqli_query($con,$pro_com_que);
                    }
                    //exit();
                    
                    //end of code
                    //Process a new form submission in HubSpot in order to create a new Contact.
                    $hubspotutk = $_COOKIE['hubspotutk']; //grab the cookie from the visitors browser.
                    $ip_addr = $_SERVER['REMOTE_ADDR']; //IP address too.
                    $hs_context = array(
                        'hutk' => $hubspotutk,
                        'ipAddress' => $ip_addr,
                        'pageUrl' => SITE_URL .'sign-up.php',
                        'pageName' => 'Sign Up'
                    );
                    $hs_context_json = json_encode($hs_context);
                    //Need to populate these varilables with values from the form.
                    $str_post = "firstname=" . urlencode($first_name)
                            . "&lastname=" . urlencode($last_name)
                            . "&email=" . urlencode($email)
                            . "&phone_number=" . urlencode($phone)
                            . "&lifecyclestage=" . urlencode($lifecycle)
                            . "&hs_context=" . urlencode($hs_context_json); //Leave this one be :)
                    //replace the values in this URL with your portal ID and your form GUID
                    $endpoint = 'https://forms.hubspot.com/uploads/form/v2/295496/cc41d7bb-8fe6-4012-a29d-18e77c070505';
                    $ch = @curl_init();
                    @curl_setopt($ch, CURLOPT_POST, true);
                    @curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
                    @curl_setopt($ch, CURLOPT_URL, $endpoint);
                    @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                    @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = @curl_exec($ch); //Log the response from HubSpot as needed.
                    @curl_close($ch);

                    //$success = "Your registration has been done successfully. Please sign-in to continue";

                    //$to = "info@freshlifefloral.com";
                    //$to = "bharat.pal04@gmail.com";
                    $to = $email;
                    $subject = "New Buyer has register at freshlifefloral.com";
                    $body .='Hi, <br/>';
                    $body .= "First Name: " . $first_name . "<br />";
                    $body .= "Email: " . $email . "<br />";
                    $body .= "Phone Number: " . $phone . "<br />";
                    $body .= "We need to make sure you are human. Please verify your email and get started using your Website account. <br/> <br/>"; 
                    $body .= "<a href=".SITE_URL."activation.php?code=".$activation.">".SITE_URL.'activation.php?code='.$activation.'</a>';
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: FreshliFefloral Registration<cheta1163@gmail.com>' . "\r\n";
                    mail($to, $subject, $body, $headers);
                    echo 1;
            }
            else{
                echo "Please try again !";
            }
        } 
        else {
            echo $message;
        }
        exit();
    }
    if ($_POST['frm_mode'] == 'grower') {
        $message = "";
        // set parameters and execute
        $first_name = $_POST["first_name"];
        $last_name = $_POST["last_name"];
        $password = $_POST["password"];
        $phone = $_POST["phone"];
        $email = $_POST["uname"];
        $active = 'active';
        $type = 'Grower';
        $stmt = $con->prepare("SELECT id FROM admin WHERE uname =? AND type=?");
        $stmt->bind_param('ss', $email, $type);
        $stmt->execute();
        $stmt->bind_result($userID);
        $stmt->fetch();
        if(!empty($userID)){
            $message = "Email address is already exists";
        }
        if (strlen($password) < 6) {
            $message = "Password length should be at least 6 characters";
        }
        if ($password != $_POST["cpassword"]) {
            $message = "Password and confirm password should be same";
        }
        if ($message == "") {

            $name = $first_name . " " . $last_name;
            
            $stmtGrower = $con->prepare("INSERT INTO growers (growers_name, contact1email, phone, active) VALUES (?, ?, ?, ?)");
            $stmtGrower->bind_param("ssss", $name, $email, $phone, $active);
            $stmtGrower->execute();
            $stmtGrower->close();
            $last_id = mysqli_insert_id($con);
          
            if (!empty($last_id)) {

                $stmt = $con->prepare("INSERT INTO admin (uname, pass, type, grower_name, grower_id) VALUES (?, ?, ?, ?, ?)");
                $stmt->bind_param("ssssi", $email, $password, $type, $name, $last_id);
                $stmt->execute();
                $stmt->close();
                //Process a new form submission in HubSpot in order to create a new Contact.
                $hubspotutk = $_COOKIE['hubspotutk']; //grab the cookie from the visitors browser.
                $ip_addr = $_SERVER['REMOTE_ADDR']; //IP address too.
                $hs_context = array(
                    'hutk' => $hubspotutk,
                    'ipAddress' => $ip_addr,
                    'pageUrl' => SITE_URL .'sign-up.php',
                    'pageName' => 'Grower Sign Up'
                );
                $hs_context_json = json_encode($hs_context);
                //Need to populate these varilables with values from the form.
                $str_post = "firstname=" . urlencode($first_name)
                        . "&lastname=" . urlencode($last_name)
                        . "&email=" . urlencode($email)
                        . "&phone_number=" . urlencode($phone)
                        . "&hs_context=" . urlencode($hs_context_json); //Leave this one be :)
                //replace the values in this URL with your portal ID and your form GUID
                $endpoint = 'https://forms.hubspot.com/uploads/form/v2/295496/36a249ec-ce63-467b-bd60-2e4fa670d4ee';
                $ch = @curl_init();
                @curl_setopt($ch, CURLOPT_POST, true);
                @curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
                @curl_setopt($ch, CURLOPT_URL, $endpoint);
                @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = @curl_exec($ch); //Log the response from HubSpot as needed.
                @curl_close($ch);

                $to = "info@freshlifefloral.com";
                $subject = "New Grower has register at freshlifefloral.com";
                $body .= "First Name: " . $_POST["first_name"] . "<br />";
                $body .= "Email: " . $_POST["uname"] . "<br />";
                $body .= "Phone Number: " . $_POST["phone"] . "<br />";
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                $headers .= "From:" . $_POST["email"] . "\r\n";
                mail($to, $subject, $body, $headers);
                echo 1;
            } 
            else {
                $message = "Please try again!";
            }
        } 
        else {
            echo $message;
        }
        exit();
    }
} 
else {
    echo "Invalid Information to Register";
    exit();
}
?>
