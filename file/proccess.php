<?php
// print_r($_REQUEST);
// print_r($_POST);
if (isset($_REQUEST['submit'])) {
	
		//Process a new form submission in HubSpot in order to create a new Contact.
		$hubspotutk = $_COOKIE['hubspotutk']; //grab the cookie from the visitors browser.
		$ip_addr = $_SERVER['REMOTE_ADDR']; //IP address too.
		$hs_context = array(
		'hutk' => $hubspotutk,
		'ipAddress' => $ip_addr,
		'pageUrl' => 'http://staging.freshlifefloral.com/cms/contactus',
		'pageName' => 'Contact Us'
		);
		$hs_context_json = json_encode($hs_context);
		//Need to populate these varilables with values from the form.
		$str_post = "firstname=" . urlencode($_POST['first_name'])
		. "&email=" . urlencode($_POST['emailid'])
		. "&country=" . urlencode($_POST['country'])
		. "&markets=" . urlencode($_POST['markets'])
		. "&live_id=" . urlencode($_POST['liveid'])
		. "&phone_number=" . urlencode($_POST['phone'])
		. "&information=" . urlencode($_POST['description'])
		. "&company_name=" . urlencode($_POST['company'])
		. "&hs_context=" . urlencode($hs_context_json); //Leave this one be :)
		//replace the values in this URL with your portal ID and your form GUID
		$endpoint = 'https://forms.hubspot.com/uploads/form/v2/295496/69df5aed-3e03-40bb-b85e-429a6784fd97';
		$ch = @curl_init();
		@curl_setopt($ch, CURLOPT_POST, true);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
		@curl_setopt($ch, CURLOPT_URL, $endpoint);
		@curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = @curl_exec($ch); //Log the response from HubSpot as needed.
		@curl_close($ch);
// echo $response;
    
	 $first_name = $_POST['first_name']; // required
    $company = $_POST['company']; // required
	$from = $_POST['emailid']; // required
    $country = $_POST['country']; // required
    $markets = $_POST['markets']; // required
	$telephone = $_POST['phone']; // required
    $skypeid = $_POST['skypeid']; // required
	$liveid = $_POST['liveid']; // required
    $comments = $_POST['description']; // required
     
    // EDIT THE 2 LINES BELOW AS REQUIRED
	$to = "info@freshlifefloral.com" ;
    $subject = "Contact us Form";  
  //  $body = "Contact us Form.<br /><br />";
    $body .= "First Name: ".$first_name."<br />";
    $body .= "Company: ".$company."<br />";
    $body .= "Email: ".$from."<br />";
    $body .= "Country: ".$country."<br />";
    $body .= "Markets: ".$markets."<br />";
    $body .= "Phone Number: ".$telephone."<br />";
	$body .= "Skype Id: ".$skypeid."<br />";
    $body .= "Live id: ".$liveid."<br /><br />";
	$body .= "Description: ".$comments."<br />";
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	$headers .= "From: $from" . "\r\n";
	mail($to,$subject,$body,$headers);
	header("Location: contact-us.php?success=true");
	
	

}
else {
echo "mail send fail";
}


?>
