<?php
$menuoff = 1;
$page_id = 5;
$message = 0;
include("../config/config_new.php");
include "Encryption.class.php";
$key = "23c34eWrg56fSdrt"; // Encryption Key
$crypt = new Encryption($key);
if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

if ($info["active"] != "demo" && $info["active"] != "suspended") {
    if (isset($_GET["offerid"]) && isset($_GET["replyid"])) {
        //print 'dfhdfh';
        $update = "update grower_offer_reply set status=1,accept_date='" . date("Y-m-d") . "' where id='" . $_GET["replyid"] . "' and offer_id='" . $_GET["offerid"] . "'";
        mysqli_query($con, $update);

        $sel_grower = "select grower_id from grower_offer_reply where id='" . $_GET["replyid"] . "' and offer_id='" . $_GET["offerid"] . "'";
        $rs_grower = mysqli_query($con, $sel_grower);
        $grower = mysqli_fetch_array($rs_grower);

        $sel_mail_email = "select contact1email,growers_name from growers where id='" . $grower["grower_id"] . "'";
        $rs_mail_email = mysqli_query($con, $sel_mail_email);
        $mail_email = mysqli_fetch_array($rs_mail_email);
        $email_address = $mail_email["contact1email"];

        $er = "select gpb.id as cartid,gpb.type,gpb.lfd,gpb.noofstems,gpb.bunches,gpb.qty,gpb.boxtype,p.id,p.name,s.name as subs,sh.name as sizename,ff.name as featurename from buyer_requests gpb
						 left join product p on gpb.product = p.id
						 left join subcategory s on p.subcategoryid=s.id  
						 left join features ff on gpb.feature=ff.id
						 left join sizes sh on gpb.sizeid=sh.id where gpb.id='" . $_GET["offerid"] . "' ";
        $ers = mysqli_query($con, $er);

        while ($er5 = mysqli_fetch_array($ers)) {

            $text .= "You proposal for following offer has been accepted. <br/> <br/>";
            if ($er5["type"] != "3") {

                if ($er5["type"] == 2) {
                    $text .= "Name Your Price - &nbsp;&nbsp;";
                }

                if ($er5["type"] == 0) {
                    $text .= "Request for Product Quote - &nbsp;&nbsp;";
                }


                $text .= "For ";
                $temp_lfd = explode("-", $er5["lfd"]);
                $text .= $temp_lfd[1] . "-" . $temp_lfd[2] . "-" . $temp_lfd[0];
                $text .= " - " . $er5["qty"] . " ";
                $temp = explode("-", $er5["boxtype"]);
                $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                $rs_box_type = mysqli_query($con, $sel_box_type);
                $box_type = mysqli_fetch_array($rs_box_type);
                $text .= $box_type["name"] . " Box(es)";
                $text .= "<br/>";

                $text .= $er5["subs"] . " - " . $er5["name"] . " - ";
                if ($er5["featurename"] != "") {
                    $text .= $er5["featurename"] . " - ";
                }

                $text .= $er5["sizename"] . " cm ";

                $text .= "For ";
                $temp_lfd = explode("-", $er5["lfd"]);
                $text .= $temp_lfd[1] . "-" . $temp_lfd[2] . "-" . $temp_lfd[0];

                if ($er5["noofstems"] > 0) {
                    $text .= " - " . $er5["noofstems"] . " Stems";
                } else {
                    $text .= " - " . $er5["qty"] . " ";

                    $temp = explode("-", $er5["boxtype"]);
                    $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                    $rs_box_type = mysqli_query($con, $sel_box_type);
                    $box_type = mysqli_fetch_array($rs_box_type);
                    $text .= $box_type["name"] . " Box(es)";
                }
            } else {
                $text .= "By the Bunch - &nbsp;&nbsp;";

                $text .= "For ";
                $temp_lfd = explode("-", $er5["lfd"]);
                $text .= $temp_lfd[1] . "-" . $temp_lfd[2] . "-" . $temp_lfd[0];

                $text .= " - " . $er5["qty"] . " ";

                $temp = explode("-", $er5["boxtype"]);
                $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                $rs_box_type = mysqli_query($con, $sel_box_type);
                $box_type = mysqli_fetch_array($rs_box_type);
                $text .= $box_type["name"] . " Box(es)";

                $text .= "<br/>";
                $text .= "Selected Bunch(es)<br/><br/>";


                $templ = explode(",", $er5["bunches"]);
                $count = sizeof($templ);
                for ($io = 0; $io <= $count - 2; $io++) {
                    $r = explode(":", $templ[$io]);
                    $query2 = "select gpb.growerid,gpb.prodcutid,gpb.sizeid,gpb.feature ,p.name as productname,s.name as subs,sh.name as sizename,ff.name as featurename,bs.name as bname from grower_product_box_packing gpb
				  left join product p on gpb.prodcutid = p.id
				   left join subcategory s on p.subcategoryid=s.id  
				  left join features ff on gpb.feature=ff.id
				  left join sizes sh on gpb.sizeid=sh.id 
				  left join bunch_sizes bs on gpb.bunch_size_id=bs.id
				  where gpb.id='" . $r[0] . "'";
                    $rs2 = mysqli_query($con, $query2);
                    while ($product_box = mysqli_fetch_array($rs2)) {

                        $text .= '&nbsp;' . $product_box["subs"] . '&nbsp;&nbsp;  
				  ' . $product_box["productname"] . ' ' . $product_box["featurename"] . ' &nbsp;&nbsp; - ' . $product_box["sizename"] . 'cm &nbsp;&nbsp; - ' . $product_box["bname"] . ' Stems &nbsp;&nbsp;-' . $r[1] . ' Bunch(es) &nbsp;
						  <br/>';
                    }
                }

                $text .= '<br/>';

                $text .= "<br/><br/>";
            }

            $text .= "<br/>";
        }

        $to = $email_address;
        $subject = "Your proposal has been accepted at freshlifefloral.com ";

        $body .= "Hello <b>" . $mail_email["growers_name"] . "</b><br/><br/>";

        $body .= $text;

        $body .= "<br/><br/>Please click on following link to login to view proposal.<br/>";

        $body .= "<a href='http://staging.freshlifefloral.com/'>http://staging.freshlifefloral.com/</a><br/><br/>";

        $body .= "Thanks!";

        $body .= "<br/><b>Freshlife Floral Team<b>";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= "From:info@freshlifefloral.com" . "\r\n";
        //mail($to, $subject, $body, $headers);
        header("location:" . SITE_URL . "my-offers.php");
    }
}

$today = date("m-d-Y H:i:s", strtotime("+2 hours"));
$today1 = explode(" ", $today);
$today_date = $today1["0"];
$time = $today1["1"];
$hours_array = explode(":", $time);

if ($hours_array[0] <= 12) {
    if ($info["live_days"] == 0) {
        $shpping_on = date("Y-m-d");
        $shpping_onr = $shpping_on;
        $tempk = explode("-", $shpping_on);
        $shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
    } else {
        $shpping_on = date('Y-m-j', strtotime($info["live_days"] . ' weekdays'));
        $shpping_onr = $shpping_on;
        $tempk = explode("-", $shpping_on);
        $shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
    }
} else {
    if ($info["live_days"] == 0) {
        $shpping_on = date('Y-m-j', strtotime('1 weekdays'));
        $shpping_onr = $shpping_on;
        $tempk = explode("-", $shpping_on);
        $shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
    } else {
        $addone = $info["live_days"] + 1;
        $shpping_on = date('Y-m-j', strtotime($addon . ' weekdays'));
        $shpping_onr = $shpping_on;
        $tempk = explode("-", $shpping_on);
        $shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
    }
}
$page_request = "growers_offers";
function box_type($box)
{
    $box_type_s = "";
    if ($box == "0") {
        $box_type_s = "Stems";
    } else if ($box == "1") {
        $box_type_s = "Bunch";
    }
    return $box_type_s;

}

function box_name($box_type)
{
    //$box_type["name"]
    $res = "";
    if ($box_type == "HB") {
        $res = "half boxes";
    } else if ($box_type == "EB") {
        $res = "eight boxes";
    } else if ($box_type == "QB") {
        $res = "quarter boxes";
    } else if ($box_type == "JB") {
        $res = "jumbo boxes";
    }
    return $res;

}

?>
<?php include("../includes/profile-header.php"); ?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>
<?php include("../includes/left_sidebar_buyer.php"); ?>
<style type="text/css">
    .not_set_border td {
        border: none !important;
    }

</style>
<!--style  btn  buy-->
<style>
    .buybtn {
        font-family: Arial, sans-serif;
        /*background:#ff8400;*/
        background: #b300b3;
        background: -webkit-gradient(linear, left top, left bottom, color-stop(#b300b3, 0), color-stop(#b300b3, 1));
        background: -webkit-linear-gradient(top, #b300b3 0%, #b300b3 100%);
        background: -moz-linear-gradient(top, #b300b3 0%, #b300b3 100%);
        background: -o-linear-gradient(top, #b300b3 0%, #b300b3 100%);
        background: linear-gradient(top, #b300b3 0%, #b300b3 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#b300b3', endColorstr='#ff6600', GradientType=0);
        padding-left: 2px;
        padding-right: 65px;
        height: 30px;
        width: 70px;
        display: inline-block;
        position: relative;
        border: 1px solid #b300b3;
        -webkit-box-shadow: 0px 1px 1px rgba(255, 255, 255, 0.8) inset, 1px 1px 3px rgba(0, 0, 0, 0.2), 0px 0px 0px 4px rgba(188, 188, 188, 0.5);
        -moz-box-shadow: 0px 1px 1px rgba(255, 255, 255, 0.8) inset, 1px 1px 3px rgba(0, 0, 0, 0.2), 0px 0px 0px 4px rgba(188, 188, 188, 0.5);
        box-shadow: 0px 1px 1px rgba(255, 255, 255, 0.8) inset, 1px 1px 3px rgba(0, 0, 0, 0.2), 0px 0px 0px 4px rgba(188, 188, 188, 0.5);
        -webkit-box-shadow: 0px 1px 1px rgba(255, 255, 255, 0.8) inset, 1px 1px 3px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0px 1px 1px rgba(255, 255, 255, 0.8) inset, 1px 1px 3px rgba(0, 0, 0, 0.2);
        box-shadow: 0px 1px 1px rgba(255, 255, 255, 0.8) inset, 1px 1px 3px rgba(0, 0, 0, 0.2);
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        float: left;
        clear: both;
        margin: 10px 0px;
        overflow: hidden;
        -webkit-transition: all 0.3s linear;
        -moz-transition: all 0.3s linear;
        -o-transition: all 0.3s linear;
        transition: all 0.3s linear;
    }

    .buybtn-text {
        padding-top: 10px;
        display: block;
        font-size: 12px;
        white-space: nowrap;
        text-shadow: 0px 1px 1px rgba(255, 255, 255, 0.3);
        color: #660066;
        -webkit-transition: all 0.2s linear;
        -moz-transition: all 0.2s linear;
        -o-transition: all 0.2s linear;
        transition: all 0.2s linear;
    }

    .buybtn-hidden-text {
        position: absolute;
        height: 100%;
        top: 0px;
        right: 52px;
        width: 0px;
        background: #660066;
        text-shadow: 0px -1px 1px #363f49;
        color: #fff;
        font-size: 18px;
        white-space: nowrap;
        text-transform: uppercase;
        text-align: left;
        text-indent: 17px;
        overflow: hidden;
        line-height: 42px;
        -webkit-box-shadow: -1px 0px 1px rgba(255, 255, 255, 0.4), 1px 1px 2px rgba(0, 0, 0, 0.2) inset;
        -moz-box-shadow: -1px 0px 1px rgba(255, 255, 255, 0.4), 1px 1px 2px rgba(0, 0, 0, 0.2) inset;
        box-shadow: -1px 0px 1px rgba(255, 255, 255, 0.4), 1px 1px 2px rgba(0, 0, 0, 0.2) inset;
        -webkit-transition: width 0.3s linear;
        -moz-transition: width 0.3s linear;
        -o-transition: width 0.3s linear;
        transition: width 0.3s linear;
    }

    .buybtn-image {
        position: absolute;
        right: 0px;
        top: 0px;
        height: 100%;
        width: 52px;
        border-left: 1px solid #660066;
        -webkit-box-shadow: 1px 0px 1px rgba(255, 255, 255, 0.4) inset;
        -moz-box-shadow: 1px 0px 1px rgba(255, 255, 255, 0.4) inset;
        box-shadow: 1px 0px 1px rgba(255, 255, 255, 0.4) inset;
    }

    .buybtn-image span {
        width: 38px;
        height: 38px;
        opacity: 0.7;
        position: absolute;
        left: 50%;
        top: 50%;
        margin: -20px 0px 0px -20px;
        background: transparent url(cart.png) no-repeat 75% 55%;
        -webkit-transition: all 0.3s linear;
        -moz-transition: all 0.3s linear;
        -o-transition: all 0.3s linear;
        transition: all 0.3s linear;
    }

    /* Hover Style*/

    .buybtn:hover .buybtn-text {
        text-shadow: 0px 1px 1px #5d81ab;
        color: #fff;
        /*background:#76b900;*/
    }

    .buybtn:hover .buybtn-hidden-text {
        width: 100px;
        /*background:#76b900;*/

    }

    .buybtn:hover .buybtn-image span {
        opacity: 1;
        /*background:#76b900;*/
    }

    /* Active Style */

    .buybtn:active {
        /*background:#76b900;*/
    }


</style>
<!--style btn buy-->

<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Bootstrap Tables</h1>
        <ol class="breadcrumb">
            <li><a href="#">Tables</a></li>
            <li class="active">Bootstrap Tables</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>Offers</strong> <!-- panel title -->
                </span>

                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a>
                    </li>
                </ul>
                <!-- /right options -->
            </div>
            <!-- panel content -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                        <tr>
                            <th class="width-30">PRODUCT</th>
                            <th>ORDER</th>
                            <th>BOXES REQUESTED</th>
                            <th>BOXES STILL NEEDED</th>
                            <th>DATE</th>
                            <th>P.O. NUMBER</th>
                            <th>OFFERS</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 1;
                        $query2 = "select gpb.*, gpb.id as cartid,gpb.date_added ,p.id,p.name,p.color_id,p.image_path,p.box_type,s.name as subs,sh.name as sizename,
                                    ff.name as featurename from buyer_requests gpb
			                        left join product p on gpb.product = p.id
			                        left join subcategory s on p.subcategoryid=s.id  
			                        left join features ff on gpb.feature=ff.id
			                        left join sizes sh on gpb.sizeid=sh.id 
			                        where gpb.buyer='" . $userSessionID . "' and 
			                        gpb.lfd>='" . date("Y-m-d") . "' order by gpb.id desc";

                        //and( gpb.type!=1 and gpb.type!=7 and gpb.type!=5 )
                        //echo $query2;
                        $result2 = mysqli_query($con, $query2);
                        $tp = mysqli_num_rows($result2);
                        if ($tp >= 1) {
                            while ($producs = mysqli_fetch_array($result2)) {
                                $sel_check = "select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,
                                                                    cr.flag from grower_offer_reply gr 
                                                                        left join growers g on gr.grower_id=g.id 
                                                                        left join country cr on g.country_id=cr.id
                                                                        where gr.offer_id='" . $producs["cartid"] . "' and 
                                                                        gr.buyer_id='" . $userSessionID . "' and 
                                                                         g.id is not NULL order  by  gr.grower_id,gr.offer_id_index";

                                $rs_growers = mysqli_query($con, $sel_check);
                                $rs_growers1 = mysqli_query($con, $sel_check);
                                $totalio = mysqli_num_rows($rs_growers);
                                $sel_id = "select count(distinct offer_id_index ) as ct from  grower_offer_reply where  offer_id ='" . $producs["cartid"] . "'";


                                $sel_id=" select  count(*)  as ct from (  select  distinct offer_id_index,grower_id  as ct from grower_offer_reply where offer_id ='" . $producs["cartid"] . "'
                                  group  by  offer_id_index,grower_id) as conteo";

                                $rs_sel_id = mysqli_query($con, $sel_id);
                                $fila = mysqli_fetch_assoc($rs_sel_id)



                                ?>
                                <tr>
                                    <!--Img-->
                                    <td class="text-center"><a data-toggle="modal" data-target=".flower_smal_<?php echo $i; ?>"><img src="<?php echo SITE_URL . $producs['image_path']; ?>" alt="" width="60"></a>
                                        <!--Pop Up-->
                                        <div class="modal fade flower_smal_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <!-- header modal -->
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="mySmallModalLabel"><?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"]."cm" ?></h4>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body">
                                                        <img src="<?php echo SITE_URL . $producs['image_path']; ?>" alt="" width="100%">
                                                    </div>
                                                    <h5>Fresh Life Floral</h5>

                                                </div>
                                            </div>
                                        </div>
                                        <!--Pop Up-->
                                    </td>
                                    <!--Order-->
                                    <td><?php //echo  $sel_check;?><?= $producs["subs"] ?> <?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"]."cm" ?></td>
                                    <!--Boxes Requested-->
                                    <td><?= $producs["qty"] ?><?php
                                        if ($producs["boxtype"] != "") {
                                            $temp = explode("-", $producs["boxtype"]);
                                            $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                                            $rs_box_type = mysqli_query($con, $sel_box_type);
                                            $box_type = mysqli_fetch_array($rs_box_type);
                                            echo $box_type["name"];
                                        }
                                        ?>
                                    </td>
                                    <!--Boxes Still Needed-->
                                    <td>
                                        <?php //echo  $sel_check. "<br>" ;?>
                                        <?php
                                       $bq = 0;
                                        while ($growers_1 = mysqli_fetch_assoc($rs_growers1)) {
                                            if  ($growers_1['status']==1){
                                                $bq+=$growers_1['boxqty'];
                                            }
                                        }
                                        $cant =$producs["qty"] - $bq;
                                        if  ($cant<0){
                                            echo  0;
                                        } else{
                                            echo  $cant;
                                        }
                                        ?>
                                    </td>
                                    <!--Date-->
                                    <td><?php
                                        if ($producs["lfd2"] == '0000-00-00') {
                                            echo date("F j, Y", strtotime($producs["lfd"]));
                                        } else {
                                            echo date("F j, Y", strtotime($producs["lfd"])) . " ";
                                        }
                                        ?></td>
                                    <td><?= $producs["cod_order"];?></td>
                                    <!--<td><?=$sel_id?></td>-->
                                    <!--Offers-->
                                    <td><a data-toggle="modal" data-target=".open_offer_modal<?php echo $i; ?>" class="btn btn-success btn-xs relative">Offer<span
                                                    class="badge badge-dark badge-corner radius-0"><?php echo   $totalio_gro . $fila['ct'] ; ?></span></a>
                                        <!--Offer Modal Start-->
                                        <div class="modal fade open_offer_modal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <!-- header modal -->
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                                        <h4 class="modal-title" id="myLargeModalLabel"><?php echo $producs['qty'] . " " . $box_type["name"]; ?>
                                                            Boxes <?= $producs["subs"] ?> <?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] ?>cm</h4>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Grower</th>
                                                                    <th># Grower offer</th>
                                                                    <th>Boxes offered</th>
                                                                    <th align="center"> Product Size</th>
                                                                    <th>Price</th>
                                                                    <th>Confirm/Status</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <form action="" method="post" id="add_to_cart_form" class="add_to_cart_form">
                                                                    <?php
                                                                    $al_ready_gro_in = array();
                                                                    $cn = 1;
                                                                    while ($growers = mysqli_fetch_assoc($rs_growers)) {
                                                                        $k = explode("/", $growers["file_path5"]);
                                                                        if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                            echo "<tr class=\"set_border\">";
                                                                        } else {
                                                                            echo "<tr class=\"not_set_border\">";
                                                                        }
                                                                        ?>
                                                                        <!--IMG-->
                                                                        <td>
                                                                            <?php
                                                                            if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                                ?>

                                                                                <img src="<?php echo SITE_URL; ?>user/logo2/<?php echo $k[1]; ?>" width="65"><?php //echo $growers["growers_name"]; ?>
                                                                            <?php }
                                                                            ?>
                                                                        </td>
                                                                        <!--OFFER ID-->
                                                                        <td> <?php
                                                                            if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                                echo "Offert " . $growers['offer_id_index'];
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                        <!--Boxes offered-->
                                                                        <td>
                                                                            <?php
                                                                            //before if (!in_array($growers['grower_id'], $al_ready_offer)) {
                                                                            if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                                $getGrowerReply = "SELECT offer_id,bunchqty,boxqty FROM grower_offer_reply where offer_id='" . $producs['cartid'] . "'and 
                                                                                grower_id='" . $growers['grower_id'] . "' and  offer_id_index='" . $growers["offer_id_index"] . "' limit 1";
                                                                                $rs_getGrowerReply = mysqli_query($con, $getGrowerReply);
                                                                                $tot_bunch_qty = 0;
                                                                                while ($row_rs_getGrowerReply = mysqli_fetch_array($rs_getGrowerReply)) {
                                                                                    $tot_bunch_qty = $tot_bunch_qty + $row_rs_getGrowerReply['boxqty'];
                                                                                    /*before  sum  row_rs_getGrowerReply['bunchqty']*/
                                                                                }
                                                                                echo $tot_bunch_qty;
                                                                                if ($producs["boxtype"] != "") {
                                                                                    $temp = explode("-", $producs["boxtype"]);
                                                                                    $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                                                                                    $rs_box_type = mysqli_query($con, $sel_box_type);
                                                                                    $box_type = mysqli_fetch_array($rs_box_type);
                                                                                    $type_box = box_name($box_type["name"]);
                                                                                    echo $type_box;
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                        <!--Size-->
                                                                        <td>
                                                                            <?php
                                                                            $getbunchSize = "SELECT *  FROM grower_product_bunch_sizes WHERE grower_id = '" . $growers['grower_id'] . "' AND product_id = '" . $producs['product'] . "' 
                                                                            AND sizes = '" . $producs['sizeid'] . "'";
                                                                            $rs_bunchSize = mysqli_query($con, $getbunchSize);
                                                                            $row_bunchSize = mysqli_fetch_array($rs_bunchSize);
                                                                            //echo  $getbunchSize."<br>";
                                                                            $bunch_size_s = "";
                                                                            if ($row_bunchSize['bunch_value'] == 0) {
                                                                                $bunch_size_s = $row_bunchSize['is_bunch_value'];
                                                                            } else {
                                                                                $bunch_size_s = $row_bunchSize['bunch_value'];
                                                                            }
                                                                            $box_type_s = box_type($producs['box_type']);/*Steam or Bunch*/ ?>
                                                                            <!--$bunch_size_s-->
                                                                            <!--Before   <?= $growers["product"] ?><?= $growers["bunchqty"] . "  Bunches " ?><?= $growers["size"] . "cm" ?> <?php echo $growers["steams"] . $box_type_s; ?> -->
                                                                            <?= $growers["product"] ?><?= $growers["bunchqty"] . " Bunches " ?><?= $growers["size"] . "cm" ?> <?= $growers["steams"] . "st/bu"; ?>
                                                                        </td>
                                                                        <!--Price-->
                                                                        <td>$<?= $growers['price']; ?></td>
                                                                        <!--Confirm/Status-->
                                                                        <td>
                                                                            <input type="hidden" name="off-id-<?= $cn ?>" value="<?php echo $producs['cartid']; ?>"/>
                                                                            <input type="hidden" name="offer_id[]" value="<?php echo $producs['cartid']; ?>"/>
                                                                            <input type="hidden" name="grid_id[]" value="<?php echo $growers['grid']; ?>"/>
                                                                            <input type="hidden" name="grower_id[]" value="<?php echo $growers['grower_id']; ?>"/>
                                                                            <input type="hidden" name="grower_pic[]" value="<?php echo $growers['file_path5']; ?>"/>
                                                                            <input type="hidden" name="product_name[]" value="<?php echo $producs['name']; ?>"/>
                                                                            <input type="hidden" name="product_image[]" value="<?php echo $producs['image_path']; ?>"/>
                                                                            <input type="hidden" name="product_size[]" value="<?php echo $producs["sizename"]; ?>"/>
                                                                            <input type="hidden" name="bunch_size[]" value="<?php echo $bunch_size_s; ?>"/>
                                                                            <input type="hidden" name="box_qty[]" value="<?php echo $growers['boxqty']; ?>"/>
                                                                            <input type="hidden" name="box_type[]" value="<?php echo $growers['boxtype']; ?>"/>
                                                                            <input type="hidden" name="is_bunch[]" value="<?php echo $row_bunchSize['is_bunch']; ?>"/><br>
                                                                            <input type="hidden" name="product_price[]" value="<?php echo $growers['price']; ?>"/>
                                                                            <input type="hidden" name="lfd[]" value="<?php echo $producs["lfd"]; ?>"/>
                                                                            <input type="hidden" name="btn_add_to_cart" value="Submit"/>
                                                                            <?php
                                                                            //before if (!in_array($growers['grower_id'], $al_ready_offer)) {
                                                                            if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                                if ($growers["reject"] != 1) {
                                                                                    $tdate2 = date("Y-m-d");
                                                                                    $date12 = date_create($grower_offer["date_added"]);
                                                                                    $date22 = date_create($tdate2);
                                                                                    $interval = $date22->diff($date12);
                                                                                    $checka1 == $interval->format('%R%a');;
                                                                                    $expired = 0;
                                                                                    if ($checka1 <= 2) {
                                                                                        $sel_buyer_info = "select live_days from buyers where id='" . $grower_offer["buyer_id"] . "'";
                                                                                        $rs_buyer_info = mysqli_query($con, $sel_buyer_info);
                                                                                        $buyer_info = mysqli_fetch_array($rs_buyer_info);
                                                                                        if ($buyer_info["live_days"] > 0) {
                                                                                            $tdate = date("Y-m-d");
                                                                                            $tdate;
                                                                                            $date1 = date_create($producs['lfd']);
                                                                                            $date2 = date_create($tdate);
                                                                                            $interval = $date2->diff($date1);
                                                                                            $checka2 = $interval->format('%R%a');
                                                                                            if ($checka2 == 0) {
                                                                                                $expired = 1;
                                                                                            }
                                                                                            if ($checka2 >= $buyer_info["live_days"]) {
                                                                                                $expired = 0;
                                                                                            } else {
                                                                                                $expired = 1;
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        $expired = 1;
                                                                                    }
                                                                                    if ($growers["status"] > 0) {
                                                                                        ?>
                                                                                        <!--Primer  estado-->
                                                                                        <a href="javascript:void(0);" class="btn btn-success btn-xs relative" style="border:0px solid #fff; background-color: #d68910">Confirmed </a>
                                                                                        <?php

                                                                                        //$b++;
                                                                                        //$cn++;
                                                                                    } else if ($growers["status"] == 0) {
                                                                                        if ($info["active"] == 'suspended') {
                                                                                            if ($expired == 0) {
                                                                                                ?>
                                                                                                <!--Agregar  al  carrito style="border:0px solid #fff;"-->
                                                                                                <!--class="btn btn-success btn-xs relative"     -->
                                                                                                <a href="<?php echo SITE_URL; ?>suspend.php?id=<?= $info["id"] ?>" class="btn btn-success btn-xs relative"
                                                                                                   style="border:0px solid #fff; background-color: #f4d03f ">Add to Cart</a>

                                                                                            <?php } else { ?>
                                                                                                <span style=" font-size:18px|bold;  display:block; color: #FF0000;">Bid closed</span>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            if ($expired == 0) {
                                                                                                ?>
                                                                                                <!--<a href="<?php //echo SITE_URL; ?>my-offers.php?offerid=<?= $producs["cartid"] ?>&replyid=<?= $growers["grid"] ?>" class="btn btn-success btn-xs relative" style="border:0px solid #fff;" onClick="return confirm('Are you sure you want to accept this offer ?')" >Atif? Add to Cart</a>-->
                                                                                                <!--<input type="submit" name="btn_add_to_cart" class="btn btn-success btn-xs relative" style="border:0px solid #fff;" value="Add to Cart" onclick="add_to_cart();"/>-->

                                                                                                <input type="submit" name="btn_add_to_cart" class="btn btn-success btn-xs relative" style="border:0px solid #fff; " value="Add to Cart"
                                                                                                       onclick="add_to_cart(<?= $producs['cartid'] ?>,<?= $userSessionID ?>,<?= $growers['grower_id']; ?>,<?= $growers["offer_id_index"]; ?>);"/>
                                                                                                <!--add to  cart  Jose Portilla-->

                                                                                            <?php } else { ?>
                                                                                                <span style="  font-size:18px|bold; display:block; color: #FF0000;">Bid closed</span>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        ?>
                                                                                        <!--Estado  Confirmado-->
                                                                                        <span style=" font-size:10px|normal;  display:block; color:#333">Accept Date--------------------------</span>
                                                                                        <?php
                                                                                        $temp_offer_date = explode("-", $growers["accept_date"]);
                                                                                        $temp_offer = $temp_offer_date[1] . "-" . $temp_offer_date[2] . "-" . $temp_offer_date[0];
                                                                                        echo $temp_offer;
                                                                                    }
                                                                                } else {
                                                                                    ?>
                                                                                    <a class="btn btn-danger btn-xs relative" style="border:0px solid #fff;">Rejected : <?= $growers["reason"] ?></a>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                        <?php array_push($al_ready_gro_in, $growers['grower_id'] . "-" . $growers["offer_id_index"]);

                                                                        $cn++;
                                                                        ?>

                                                                        </tr>

                                                                        <?php
                                                                        //array_push($al_ready_offer, $growers['grower_id']);
                                                                    }
                                                                    ?>
                                                                </form>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!--Offer Modal End-->
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>

<?php require_once 'includes/footer_new.php'; ?>
<script>


    function send_offer(cartid, buyer, product, product_subcategory, sizename, price, qty) {
        var boxtype = $("#boxtype-" + cartid).val();
        var k = confirm('Are you sure you want send offer');
        if (k == true) {
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>growers-send-offers-ajax-direct.php',
                data: 'offer=' + cartid + '&buyer=' + buyer + '&product=' + product + '&product_subcategory=' + product_subcategory + '&boxtype=' + boxtype + '&sizename' + sizename + '&price=' + price + '&qty=' + qty,
                success: function (data) {
                    alert('Offer has been sent.');
                    location.reload();
                }
            });
        }
    }


    function add_to_cart(offer, buyer, grower, index) {
        //alert(offer);


        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>cart/viaje.php',
            data: 'offer=' + offer + '&buyer=' + buyer + '&grower=' + grower + '&index=' + index,
            success: function (data) {
                //alert(data);
                alert('Offer has been sent.');
                location.reload();
            },
            error: function () {
                alert("Ha ocurrido un error");
            }
        });

    }


    /*jQuery(window).ready(function () {
     $('.add_to_cart_form').submit(function (event) {
     var r = confirm("Are you sure you want to accept this offer?");
     if (r == true) {
     // get the form data
     // there are many ways to get this data using jQuery (you can use the class or id also)
     var formData = $(this).serialize();
     //alert(formData);
     // process the form

$.ajax({
type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
url: 'cart/process_cart.php', // the url where we want to POST
data: formData, // our data object
dataType: 'json', // what type of data do we expect back from the server
encode: true
})
// using the done promise callback
.done(function (data) {
//console.log( JSON.stringify(data));
//console.log(data['126']['0']));
if (data['res'] == 1) {
alert()
alert('Items has been added to your cart.');
$('.modal').modal('hide');
location.reload();
}
//console.log(data[])
// log data to the console so we can see
// here we will handle errors and validation messages
});
//data: $('form#sendoffer' + id).serialize(),
}

// stop the form from submitting the normal way and refreshing the page
event.preventDefault();
});

});*/
</script>
