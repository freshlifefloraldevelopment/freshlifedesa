<?php
require_once("../config/config_gcp.php");


if ($_SESSION["login"] != 1 && $_SESSION["grower"] == "") {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["grower"];
/* * *******get the data of session user*************** */
$sel_info = "select * from growers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

$img_url = 'imagenes/profile_images/noavatar.jpg';
if ($info["file_path5"] != '') {
    $k = explode("/", $info["file_path5"]);
    $data = getimagesize(SITE_URL . "user/logo2/" . $k[1]);
    $img_url = SITE_URL . "user/logo/" . $k[1];
}

if (isset($_REQUEST["total"])) {
    $temp = "";
    for ($i = 1; $i <= $_POST["totalrow"] - 1; $i++) {
        
        $sqlDelivery = "select DATE_SUB(TIME(NOW()), INTERVAL 5 HOUR) hora";
        
                        $row_sqldel = mysqli_query($con, $sqlDelivery);
                                                            
                while ($calDel =mysqli_fetch_assoc($row_sqldel)) {
                        $startHour= $calDel["hora"];           //---------------- SEMANA
                }
        
        
                          $startDate = date('Y-m-d');
                         // $startHour = date("H:i:s");
                          
        $update = "update grower_product_box_packing 
                      set date_update = now()            ,
                          hora        = '".$startHour."' , 
                          stock       = '" . $_POST["qty-"   . $_POST["pro-" . $i]] . "',
                          price       = '" . $_POST["price-" . $_POST["pro-" . $i]] . "' 
                    where id='" . $_POST["pro-" . $i] . "'";               
        
        
        mysqli_query($con, $update);
        $temp2 = explode(",", $temp);
    }
}
$page_request = "update_stock";
require_once '../includes/profile-header.php';
require_once "../includes/left_sidebar_growers.php";
?>
<?php

$sel_products = "select gpb.id 
                   from grower_product_box_packing gpb
                   left join product p on gpb.prodcutid = p.id
                   left join growers g on gpb.growerid  = g.id
                  where g.active!='deactive'  
                    and gpb.growerid='" . $userSessionID . "'  
                    and p.name is not null";    


$rs_prodcuts = mysqli_query($con, $sel_products);
$total = mysqli_num_rows($rs_prodcuts);
$num_record = $total;
$display = 50;
$XX = '<div class="notfound">No Item Found !</div>';


function initial($user, $init, $display){
    $query = "select gpb.prodcutid        ,
                     gpb.id as gid        ,
                     gpb.price as gprice  ,
                     gpb.qty              ,
                     gpb.sizeid           ,
                     gpb.feature          ,
                     gpb.type as bv       ,
                     gpb.boxname as bvname,
                     gpb.growerid         ,
                     p.id                 , p.name , p.color_id , p.image_path,
                     s.name as subs,
                     g.file_path5 , g.growers_name , g.inventory ,
                     sh.name as sizename,
                     ff.name as featurename,
                     b.name as boxname,
                     bs.name as bname,
                     bt.name as boxtype,
                     c.name as colorname, 
                     b.width , b.length , b.height
                from grower_product_box_packing gpb
                left join product p      on gpb.prodcutid    = p.id
                left join subcategory s  on p.subcategoryid  = s.id  
                left join colors c       on p.color_id       = c.id 
                left join features ff    on gpb.feature      = ff.id
                left join sizes sh       on gpb.sizeid       = sh.id 
                left join boxes b        on gpb.box_id       = b.id
                left join boxtype bt     on b.type           = bt.id
                left join growers g      on gpb.growerid     = g.id
                left join bunch_sizes bs on gpb.bunch_size_id=bs.id
               where g.active    != 'deactive' 
                 and gpb.growerid = '" . $user . "' 
                 and gpb.type    !=2  
                 and p.name is not null 
                 and bt.name     !=''     
               order by p.name , CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER) , g.growers_name LIMIT " . $init . ",$display";
    
    return $query;
}

function price($grower_id, $product_id, $size_id, $feature)
{
    $query_price = "select id , price 
                      from grower_product_price 
                     where growerid  = '" . $grower_id . "' 
                       and prodcutid = '" . $product_id . "'                               
                       and sizeid    = '" . $size_id . "'
                       and feature   = '" . $feature . "' 
                     order by id desc limit 0,1";               
    
    return $query_price;
}

function stock($produ_box_id)
{
    global $con;
    $query = "select stock 
                from grower_product_box_packing 
               where id='" . $produ_box_id . "'";  
    
    $rs_stock = mysqli_query($con, $query);
    $stock = mysqli_fetch_array($rs_stock);
    return $stock;

}


if (isset($_POST["startrow"]) && $_POST["startrow"] != "") {
    $sr = $_POST["startrow"] + 1;
    $query2 = initial($userSessionID, $_POST["startrow"], $display);
    $result2 = mysqli_query($con, $query2);
    //echo $query2;
} else {
    if (empty($startrow)) {
        $startrow = 0;
        $sr = 1;
    }
    $query2 = initial($userSessionID, 0, $display);
    $result2 = mysqli_query($con, $query2);
    //echo $query2;
}


echo $display;
?>

    <section id="middle">
        <!-- page title -->
        <header id="page-header">
            <h1>Update Stock</h1>
            <ol class="breadcrumb">
                <li><a href="#">Stock</a></li>
                <li class="active"> Update Stock</li>
            </ol>
        </header>
        <!-- /page title -->
        <div id="content" class="padding-20">
            <div id="panel-2" class="panel panel-default">
                <div class="panel-heading">
                <span class="title elipsis">
                 <!--   <strong>Update Stock</strong>  panel title -->
                </span>
                    <!-- right options -->
                    <ul class="options pull-right list-inline">
                        <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                        <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                        <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                    </ul>
                    <!-- /right options -->
                        <!--Select  Box Type (2) -->
                        <div class="col-md-2 margin-top-4">
                        <!--<label>Select Inventory Grower </label>-->
                        <div class="fancy-form fancy-form-select">
                        <select style="width: 100%; diplay: none;" name="filter_category" id="box_type_<?php echo $products["prodcutid"] . '_' . $i; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                        <option value="">Select Inventory</option>                                                                                            
                        <?php
                        $sql_unit = "select id,name from inventary_growers";
                        $result_units = mysqli_query($con, $sql_unit);
                        while ($row_category = mysqli_fetch_assoc($result_units)) { ?>
                        <option value="<?php echo $row_category['id']; ?>"><?= $row_category['name']; ?></option>
                        <?php }
                        ?>
                        </select>
                        <i class="fancy-arrow"></i>
                        </div>                    
                        </div>                    
                </div>
                <!-- panel content -->
                <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">
                    <div class="panel-body">
                        <input type="submit" id="submitu" class="btn btn-success btn-sm" name="submitu" value="Update All">
                        <div class="table-responsive">
                            <table id="sample_1" class="table table-hover table-vertical-middle nomargin dataTable" role="grid" aria-describedby="sample_1_info">
                                <thead>
                                   <tr role="row">
                                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" style="width: 190px;">Product</th>
                                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 284px;">Stems</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column descending" style="width: 257px;" aria-sort="ascending">Box Type</th>
                                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 159px;">Price</th>
                                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 112px;">CSS Stock</th>
                                   </tr>
                                </thead>
                                
                                <tbody>
                                <?php
                                $i = 1;

                                while ($producs = mysqli_fetch_array($result2)) {
                                    $sel_price = price($producs["growerid"], $producs["prodcutid"], $producs["sizeid"], $producs["feature"]);
                                    $rs_price = mysqli_query($con, $sel_price);
                                    $price = mysqli_fetch_array($rs_price);

                                    /*$sel_stock = stock($producs["gid"]);
                                    $rs_stock = mysqli_query($con, $sel_stock);
                                    $stock = mysqli_fetch_array($rs_stock);*/

                                    $stock=stock($producs["gid"]);


                                    if ($producs["gprice"] > 0) {
                                        $kp = $producs["gprice"];
                                    } else {
                                        $kp = $price["price"];
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $producs["subs"] ?> <?php echo $producs["name"] ?><?php echo $producs["featurename"] ?> <?php echo $producs["sizename"] ?>cm <?php echo $producs["colorname"] ?><input type="hidden" name="subcategoryname-<?php echo $producs["gid"] ?>" value="<?php echo $producs["subs"] ?>"></td>
                                        <td><?php echo $producs["bname"] * $producs["qty"] ?></td>
                                        <td><?php echo $producs["boxtype"] ?>
                                            <small>  <?php echo $producs["width"] ?>x<?php echo $producs["length"] ?>x<?php echo $producs["height"] ?></small>
                                        </td>
                                        <td><input type="text" class="form-control" name="price-<?php echo $producs["gid"] ?>" id="price-<?php echo $producs["gid"] ?>" value="<?php echo $kp ?>"></td>
                                        <td>
                                            <input type="text" class="form-control" name="qty-<?php echo $producs["gid"] ?>" id="qty-<?php echo $producs["gid"] ?>" value="<?php echo $stock["stock"] ?>">
                                            <input type="hidden" class="form-control" name="pro-<?php echo $i ?>" value="<?php echo $producs["gid"] ?>"/>
                                            <input type="hidden" class="form-control" name="grower-<?php echo $producs["gid"] ?>" value="<?php echo $producs["growerid"] ?>">
                                        </td>
                                    </tr>
                                    <?php $i++;
                                }
                                ?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                    <input type="hidden" name="totalrow" value="<?php echo $i ?>"/>
                </form>
                <!-- /panel content -->
            </div>
            <!-- /PANEL -->
            <nav>
                <ul class="pagination">
                    <?php
                    if ($_POST["startrow"] != 0) {

                        $prevrow = $_POST["startrow"] - $display;

                        print("<li><a href=\"javascript:onclick=funPage($prevrow)\" class='link-sample'>Previous </a></li>");
                    }
                    $pages = intval($num_record / $display);

                    if ($num_record % $display) {

                        $pages++;
                    }
                    $numofpages = $pages;
                    $cur_page = $_POST["startrow"] / $display;
                    $range = 5;
                    $range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2;
                    $range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min;
                    $page_min = $cur_page - $range_min;
                    $page_max = $cur_page + $range_max;
                    $page_min = ($page_min < 1) ? 1 : $page_min;
                    $page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max;
                    if ($page_max > $numofpages) {
                        $page_min = ($page_min > 1) ? $numofpages - $range + 1 : 1;
                        $page_max = $numofpages;
                    }
                    if ($pages > 1) {
                        print("&nbsp;");
                        for ($i = $page_min; $i <= $page_max; $i++) {
                            if ($cur_page + 1 == $i) {
                                $nextrow = $display * ($i - 1);
                                print("<li class='active'><a href='javascript:void();'>$i</a></li>");
                            } else {

                                $nextrow = $display * ($i - 1);
                                print("<li><a href=\"javascript:onclick=funPage($nextrow)\"  class='link-sample'> $i </a></li>");
                            }
                        }
                        print("&nbsp;");
                    }
                    if ($pages > 1) {

                        if (!(($_POST["startrow"] / $display) == $pages - 1) && $pages != 1) {

                            $nextrow = $_POST["startrow"] + $display;

                            print("<li><a href=\"javascript:onclick=funPage($nextrow)\" class='link-sample'> Next</a></li> ");
                        }
                    }

                    if ($num_record < 1) {
                        print("<span class='text'>" . $XX . "</span>");
                    }
                    ?></ul>
            </nav>
        </div>
        <form method="post" name="frmfprd" action="">
            <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
        </form>
    </section>
    <!-- /MIDDLE -->
    <script tpe="text/javascript">
        function funPage(pageno) {
            document.frmfprd.startrow.value = pageno;
            document.frmfprd.submit();
        }
        function docolorchange() {
            window.location.href = '<?php echo SITE_URL; ?>buy.php?id=<?php echo $_GET["id"] ?>&categories=<?php echo $strdelete ?>&l=<?php echo $_GET["l"] ?>&c=' + $('#color').val();
        }
        function frmsubmite() {
            document.frmfilter.submit();
        }
        function doadd(id) {
            var check = 0;
            if ($('#qty-' + id).val() == "") {
                $('#ermsg-' + id).html("please enter box qty.")
                check = 1;
            }

            if (check == 0) {
                $('#ermsg-' + id).html("");
                $('#gid6').val(id);
                $('#frmrequest').submit();
            }
        }
    </script>
    <link href="../assets/css/layout-datatables.css" rel="stylesheet" type="text/css"/>
<?php include("../includes/footer_new.php"); ?>
