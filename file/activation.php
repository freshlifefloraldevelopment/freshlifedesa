<?php 
require_once("config/config_new.php");
if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}

#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL.'includes/assets/css/essentials-flfv3.css', SITE_URL.'includes/assets/css/layout-flfv3.css', 
                            SITE_URL.'includes/assets/css/header-1.css', SITE_URL.'includes/assets/css/layout-shop.css', SITE_URL.'includes/assets/css/color_scheme/blue.css' );
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################
require_once 'includes/header.php';
?>
<section class="page-header page-header-xs">
  <div class="container">
    <h1>Activation</h1>
    <!-- breadcrumbs -->
    <ol class="breadcrumb">
      <li><a href="#">Home</a></li>
      <li><a href="#">buyer</a></li>
      <li class="active">Activation</li>
    </ol>
    <!-- /breadcrumbs -->
    <!-- page tabs -->
   
    <!-- /page tabs -->
  </div>
</section>
<section>
  <div class="container">
        <?php 
            $msg='';
            if(!empty($_GET['code']) && isset($_GET['code']))
            {
                $code=mysqli_real_escape_string($con,$_GET['code']);

                $c=mysqli_query($con,"SELECT id FROM buyers WHERE activation='$code'");
                $row_buyer=mysqli_fetch_array($c);
                if(mysqli_num_rows($c) > 0)
                {
                    $count=mysqli_query($con,"SELECT id FROM buyers WHERE activation='$code' and status='0'");

                    if(mysqli_num_rows($count) == 1)
                    {
                        mysqli_query($con,"UPDATE buyers SET status='1' WHERE activation='$code'");
                        mysqli_query($con,"UPDATE profile_completion SET status='1' WHERE buyer_id='".$row_buyer['id']."' AND profile='confirm-account'");
                        $msg="Your account is activated, Please click here to <a href='http://staging.freshlifefloral.com/login.php'>login</a>"; 
                    }
                    else
                    {
                        mysqli_query($con,"UPDATE profile_completion SET status='1' WHERE buyer_id='".$row_buyer['id']."' AND profile='confirm-account'");
                        $msg ="Your account is already active, no need to activate again, Please click here to <a href='http://staging.freshlifefloral.com/login.php'>login</a>";
                    }

                }
                else
                {
                $msg ="Wrong activation code.";
                }

            }
            echo $msg;
        ?>
  </div>
</section>
<?php require_once("includes/footer.php"); ?>
