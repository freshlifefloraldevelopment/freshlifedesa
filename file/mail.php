<?php 
session_start();
if($_POST['name'] != '' && $_POST['email'] != ''){
		
	     if ($_REQUEST['captcha']!=4) {
		  header("Location:http://staging.freshlifefloral.com/error.php");
	     }
	 
	     else
	     {
		    //Process a new form submission in HubSpot in order to create a new Contact.
		$hubspotutk = $_COOKIE['hubspotutk']; //grab the cookie from the visitors browser.
		$ip_addr = $_SERVER['REMOTE_ADDR']; //IP address too.
		$hs_context = array(
		'hutk' => $hubspotutk,
		'ipAddress' => $ip_addr,
		'pageUrl' => 'http://staging.freshlifefloral.com/cms/contactus',
		'pageName' => 'Quick Contact'
		);
		$hs_context_json = json_encode($hs_context);
		//Need to populate these varilables with values from the form.
		$str_post = "firstname=" . urlencode($_POST['name'])
		. "&email=" . urlencode($_POST['email'])
		. "&phone_number=" . urlencode($_POST['phone'])
		. "&information=" . urlencode($_POST['comment'])
		. "&hs_context=" . urlencode($hs_context_json); //Leave this one be :)
		//replace the values in this URL with your portal ID and your form GUID
		$endpoint = 'https://forms.hubspot.com/uploads/form/v2/295496/41fac4ff-b696-4227-9297-d97233ed8334';
		$ch = @curl_init();
		@curl_setopt($ch, CURLOPT_POST, true);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
		@curl_setopt($ch, CURLOPT_URL, $endpoint);
		@curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = @curl_exec($ch); //Log the response from HubSpot as needed.
		@curl_close($ch);
		
			$to  = 'info@freshlifefloral.com'; // note the comma
			// subject
			$subject = 'Quick Contact';
			
			// message
			$message='
			<html>
			<head>
			  <title>Quick Contact</title>
			</head>
			<body>
			<table>
				<tr>
				  <td width="100">Name</td><td>'.$_POST['name'].'</td>
				</tr>
				<tr>
				  <td>Email</td><td>'.$_POST['email'].'</td>
				</tr>
				<tr>
				  <td>Phone</td><td>'.$_POST['phone'].'</td>
				</tr>
				<tr>
				  <td>Comment</td><td>'.nl2br($_POST['comment']).'</td>
				</tr>
			  </table>
			</body>
			</html>
			';
			
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: '.$_POST['name'].' <'.$_POST['email'].'>' . "\r\n";

            if(mail($to, $subject, $message, $headers))
	        header('location: http://staging.freshlifefloral.com/thankyou.php');
		 }
	
}
?>
