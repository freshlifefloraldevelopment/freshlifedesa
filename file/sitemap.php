<?
    $page_id=7;  
    include("config/config.php");
	$sel_page="select * from page_mgmt where page_id=5";
	$rs_page=mysql_query($sel_page);
	$page=mysql_fetch_array($rs_page);
	$pageid=5;
?>
<!DOCTYPE html>
<html lang="en">
<?php include("include/head.php"); ?>
<body>
<!-- main wrapper starts -->
<div class="wrapper">
  <!-- header starts -->
 <?php include("include/header.php"); ?>
  <!-- header ends -->
  <div class="cl"></div>
  <!-- About us banner starts -->
  <div class="aboutus-banner">
    <div class="banner-about">
      <div class="ban-image"><img src="<?=$page["image_path"]?>" width="485" height="267" alt=""> </div>
      <div class="ban-content">
        <h3><?=$page["banner_title"]?></h3>
        <p><?=$page["banner_description"]?></p>
      </div>
    </div>
  </div>
  <!-- body starts -->
  <div class="cl"></div>
  <!-- inner-content starts -->
  <div class="content-container inner-content">
    <div class="brad"><a href="index.php" class="no-active">Home</a> / <a href="sitemap.php">Site Map</a></div>
    <div class="cl"></div>
    <!-- Left Starts -->
    <div class="left-container">
      <div class="top"></div>
      <div class="left-mid">
        <div class="content_area">
          <h2><?=$page["page_title"]?></h2>
          <?=$page["page_desc"]?>
        </div>
        <div class="cl"></div>
      </div>
      <div class="bot"></div>
      <div class="cl"></div>
    </div>
    <!-- Left ends -->
    <!-- Right Starts -->
    <?php include("include/right.php"); ?>
    <!-- Right Ends -->
    <div class="cl"></div>
  </div>
  <!-- inner-content ends -->
  <!-- footer starts -->
  <div class="footerwraper">
  <?php include("include/footer.php"); ?>
  </div>
  <!-- footer ends -->
   <?php include("include/copyright.php"); ?>
  <div class="cl"></div>
  <!-- body ends -->
</div>
<!-- main wrapper -->
<!-- Fixed-div starts -->
<?php include("include/fixeddiv.php"); ?>
<!-- Fixed-div ends -->
</body>
</html>
