<?php	
	include "../config/config_gcp.php";
        session_start();

    if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)  {
	    header("location: index.php");
    }	
    	
	
	$sel_info="select * from growers where id='".$_GET["id"]."'";
	$rs_info=mysqli_query($con,$sel_info);
	$info=mysqli_fetch_array($rs_info);
	
	$select_product_info="select name from product where id='".$_GET["pid"]."'";
	$rs_product_info=mysqli_query($con,$select_product_info);
	$product_info=mysqli_fetch_array($rs_product_info);
	
	$str = trim($info['products'],",");  
	
	if($str!="")	{
	
		$qsel="select gs.product_id,
                              gs.sizes as sizeid,                             
                              gs.grower_id ,
                              gpf.features ,                    
                              p.id as pid  ,
                              p.box_type   ,
                              ff.name as featurename,
                              p.name as productname ,
                              p.color_id as colorid ,
                              p.categoryid as productcategory        ,
                              p.subcategoryid as productsubcategoryid,
                              pc.name productcateogryname            ,
                              sh.name as sizename,
                              c.name as colorname,
                              s.name as productsubcategoryname
                         from growcard_prod_sizes gs 
                         inner join product p     on gs.product_id=p.id 
                         inner join category pc   on p.categoryid=pc.id 
                         inner join subcategory s on p.subcategoryid=s.id  
                         inner join colors c      on p.color_id=c.id 
                         inner join sizes sh      on gs.sizes=sh.id 
                         left join growcard_prod_features gpf on gs.product_id=gpf.product_id and gs.grower_id = gpf.grower_id
                         left join features ff on gpf.features=ff.id
                        where gs.grower_id  ='".$_GET["id"]."' 
                        group by gs.product_id   ,
                                 gs.sizes        ,                             
                                 gs.grower_id    ,
                                 gpf.features    ,                    
                                 p.id            ,
                                 p.box_type      ,
                                 ff.name         ,
                                 p.name          ,
                                 p.color_id      ,
                                 p.categoryid    ,
                                 p.subcategoryid ,
                                 pc.name         ,
                                 sh.name         ,
                                 c.name          ,
                                 s.name
                        order by CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER) ";

		$rs=mysqli_query($con,$qsel);

	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">



$.fn.dataTableExt.oApi.fnFilterClear  = function ( oSettings )

{

    /* Remove global filter */

    oSettings.oPreviousSearch.sSearch = "";

      

    /* Remove the text of the global filter in the input boxes */

    if ( typeof oSettings.aanFeatures.f != 'undefined' )

    {

        var n = oSettings.aanFeatures.f;

        for ( var i=0, iLen=n.length ; i<iLen ; i++ )

        {

            $('input', n[i]).val( '' );

        }

    }

      

    /* Remove the search text for the column filters - NOTE - if you have input boxes for these

     * filters, these will need to be reset

     */

    for ( var i=0, iLen=oSettings.aoPreSearchCols.length ; i<iLen ; i++ )

    {

        oSettings.aoPreSearchCols[i].sSearch = "";

    }

      

    /* Redraw */

    oSettings.oApi._fnReDraw( oSettings );

};



var oTable;

$(document).ready(function() {
    oTable = $('#example').dataTable({
                    "bJQueryUI": true, 
                    "sPaginationType": "full_numbers"
});
   
    $('#form1').submit( function () {

            var oSettings = oTable.fnSettings();
		oTable.fnFilterClear();
                oSettings._iDisplayLength = -1;
		oSettings._bFilter = false;
		oTable.fnDraw();
    });
         
});

</script>


</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

  <?php include("includes/header_inner.php");?>

  <tr>
    <td height="5"></td>
  </tr>

  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <?php include("includes/grower-left-param.php");?>

          <td width="5">&nbsp;</td>

          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>
                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr>

                      <td width="10">&nbsp;</td>

                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                      <tr>
                            <td height="5"></td>
                      </tr>
                 
                         <tr>
                            <td class="pagetitle"><font color="green">Manage Bunch Packing  (<?php echo $info["growers_name"]?> )</font></td>
                         </tr>
                          
                          <tr><td style="height:10px;"></td></tr>        

                         
            <tr>

                    <td>

		    <table width="100%">                                            
                        <!--tr>
                            <td>
                                <a class="pagetitle1" href="bunch_packing_mgmt.php?id=<?php echo $_GET["id"]?>" onclick="this.blur();"><span> Manage Bunch Packing </span></a>
                            </td>
                        </tr-->
                                            
                    </table>
                    </td>
                          </tr>
                              <tr><td style="height:10px;"></td></tr>   
                  
                          <tr>

                            <td><form action="" id="form1" method="post">

                                <div id="box">

                                  <div id="container">

                                    <div class="demo_jui">

                                      <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

                                        <thead>

                                          <tr>
                                                <th width="13%" align="left">Product</th>
                                                <th width="10%" align="left">Category</th>
                                                <th width="13%" align="left">Sub Category</th>
                                                <th width="6%" align="left">Size</th>
                                                <th width="10%" align="left">Bunch</th>
                                          </tr>

                                        </thead>

                                        <tbody>

                        <?php
					$sr=1;
						  
			  if($str!="")  {
                              while($product=mysqli_fetch_array($rs))  {

			?>

                            <tr class="gradeU">

                                        <td class="text" align="left"><?php echo $product["productname"];?> <?php if($product["featurename"]!=""){ ?> <br/> (<?php echo $product["featurename"]?>) <?php } ?>	  </td>
					<td class="text" align="left"><?php echo $product["productcateogryname"]?> </td>
					<td class="text" align="left"><?php echo $product["productsubcategoryname"]?> </td>
					<td class="text" align="left"><?php echo $product["sizename"]?>CM</td>                                           
                                            
                                        <td>                                    		

                                        	<?php

                                    		$bunch_chk="select * 
                                                              from growcard_prod_bunch_sizes 
                                                             where grower_id  ='".$_GET["id"]."' 
                                                               and product_id ='".$product["pid"]."' 
                                                               and sizes      ='".$product["sizeid"]."'";

                                    		$rs_bunch=mysqli_query($con,$bunch_chk);
						$b_row=mysqli_fetch_array($rs_bunch);
                                                
                                                $control = $product["box_type"];

                                        	if($product["box_type"] == "1"){ 
                                                    ?>
                                    			<p id="bunch_box_<?php echo $product["pid"]."_".$product["sizename"];?>" style="display:block;">

	                                        		<input type="text" id="is_bunch_value_<?php echo $product["pid"]."_".$product["sizename"];?>" value="<?php echo $b_row["is_bunch_value"]?>" name="is_bunch_value<?php echo $sr; ?>" />	

	                                        	</p>

                                                <?php 
                                                
                                                    }else{
                                                 ?>
                                            
                                            
                                                <p id="stemp_box_<?php echo $product["pid"]."_".$product["sizename"];?>" >
                                                        <?php 
                                                        
                                                        $sel_sizes="select b.id , b.name 
                                                                      from growcard_prod_bunch_sizes cpb
                                                                     inner join bunch_sizes b on cpb.bunch_sizes = b.id
                                                                     where cpb.product_id ='".$product["pid"]."' 
                                                                       and cpb.grower_id  ='".$_GET["id"]."' 
                                                                       and cpb.sizes      ='".$product["sizeid"]."'
                                                                     group by b.id
                                                                     order by CONVERT(SUBSTRING(b.name,1), SIGNED INTEGER) ";
                                                        
                                                        $rs_sizes=mysqli_query($con,$sel_sizes);
							while($bsizes=mysqli_fetch_array($rs_sizes))  {
														  
								$sel_chk="select grower_id 
                                                                            from growcard_prod_bunch_sizes 
                                                                           where chk       ='".$product["pid"]."-".$product["sizeid"]."-".$bsizes["id"]."' 
                                                                             and grower_id ='".$_GET["id"]."' 
                                                                             and sizes     ='".$product["sizeid"]."' 
                                                                             group by grower_id";    
															   															  
								$rs_chk=mysqli_query($con,$sel_chk);
								$chk=mysqli_num_rows($rs_chk);

							?>

                                                               <input class="boxcheckbox_<?php  echo $product["pid"]?>-<?php echo $product["sizeid"]?>-<?php echo $bsizes["id"]?>" type="checkbox" name="<?php echo $product["pid"]?>-<?php echo $product["sizeid"]?>-<?php echo $bsizes["id"]?>" value="<?php echo $bsizes["id"]?>" <?php if($chk==1){echo "checked";} ?>  />
                                                                                                                                                            
				                                <?php echo $bsizes["name"]; ?> Stems <br/>

				                        <?php

							}
							?>     
						</p>

                                        	<?php } ?> 
											                                            
                                        </td>

                                     </tr>

                        <?php					 
						      $sr++;
							  
                              }
					
			}	

			?>

                                        </tbody>
                                      </table>
                                    </div>
                                  </div>

                                  <div style="float:right; margin-top:10px;">

                                    <input type="hidden" name="total" value="<?php echo $sr?>"  />
                                    <input type="hidden" name="valida" value="<?php echo $control?>"  />

                                    <!--input name="Submit" type="Submit" class="buttongrey" value="Update Packing" /-->

                                  </div>
                                </div>

                              </form></td>

                          </tr>

                        </table></td>

                      <td width="10">&nbsp;</td>

                    </tr>

                  </table></td>

                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

              </tr>

              <tr>

                <td background="images/middle-leftline.gif"></td>
                <td>&nbsp;</td>
                <td background="images/middle-rightline.gif"></td>

              </tr>

              <tr>
                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                <td background="images/middle-bottomline.gif"></td>
                <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
              </tr>

            </table></td>
        </tr>
      </table></td>

  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>

</table>
</body>
</html>