<?php

  // Cambio Verificar   incluir fecha

include "../config/config_gcp.php";

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}

if (isset($_POST["Submit"]) && $_POST["Submit"] == "Save") {
    $ins = "update destiny set 
                   country    ='" . $_POST["country"] . "'    ,
                   city       ='" . $_POST["cities"] . "'     ,
                   descripcion='" . $_POST["descripcion"] . "',
                   region_id  ='" . $_POST["region"] . "'    
             where id='" . $_GET["sid"] . "'";                                              
    
    if(mysqli_query($con, $ins))
    header('location:destiny.php');
}

$sel_des = "select d.id, d.country, d.city, d.descripcion, d.region_id
              from destiny d
             where d.id='" . $_GET["sid"] . "'";

$rs_des = mysqli_query($con, $sel_des);
$dest = mysqli_fetch_array($rs_des);


?>
<?php include("header.php"); ?>
<script type="text/javascript">
            $(document).ready(function () {
                var maxField = 10; //Input fields increment limitation
                var addButton = $('.add_button'); //Add button selector
                var wrapper = $('.field_wrapper'); //Input field wrapper
                var fieldHTML = '<tr><td width="34%" align="left" valign="middle" class="text"><input type="text" name="charges_per_kilo_key[]" value=""/></td><td width="66%" bgcolor="#f2f2f2"><input type="text" name="charges_per_kilo_value[]" value=""/><input type="button" value="-"  class="remove_button" /><input type="button" class="add_button" value="+" onclick="add();" /><br><span class="error" id="lblkilograms"></span></td></tr>'
                wrapper.on('click', '.remove_button', function (e) { //Once remove button is clicked
                    e.preventDefault();
                    $(this).parent('td').parent().remove(); //Remove field html
                });
            });
            function add() {
                var wrapper1 = $('.field_wrapper'); //Input field wrapper
                //var fieldHTML = '<div><input type="text" name="charges_per_kilo[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="remove-icon.png"/></a></div>'; //New input field html 
                var fieldHTML1 = '<tr><td width="34%" align="left" valign="middle" class="text"><input type="text" name="charges_per_kilo_key[]" value=""/></td><td width="66%" bgcolor="#f2f2f2"><input type="text" name="charges_per_kilo_value[]" value=""/><input type="button" value="-"  class="remove_button" /><input type="button" class="add_button" value="+" onclick="add();" /><br><span class="error" id="lblkilograms"></span></td></tr>'
                wrapper1.append(fieldHTML1);
            }
            
            
            $(document).ready(function () {
                var maxField = 10; //Input fields increment limitation
                var addButton = $('.add_button2'); //Add button selector
                var wrapper = $('.field_wrapper2'); //Input field wrapper
                var fieldHTML = '<tr><td width="34%" align="left" valign="middle" class="text"><input type="text" name="charges_per_shipment_key[]" value=""/></td><td width="66%" bgcolor="#f2f2f2"><input type="text" name="charges_per_shipment_value[]" value=""/><input type="button" value="-"  class="remove_button2" /><input type="button" class="add_button2" value="+" onclick="add_shipment();" /><br><span class="error" id="lblkilograms"></span></td></tr>'
                wrapper.on('click', '.remove_button2', function (e) { //Once remove button is clicked
                    e.preventDefault();
                    $(this).parent('td').parent().remove(); //Remove field html
                });
            });
            function add_shipment() {
                var wrapper1 = $('.field_wrapper2'); //Input field wrapper
                //var fieldHTML = '<div><input type="text" name="charges_per_kilo[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="remove-icon.png"/></a></div>'; //New input field html 
                var fieldHTML1 = '<tr><td width="34%" align="left" valign="middle" class="text"><input type="text" name="charges_per_shipment_key[]" value=""/></td><td width="66%" bgcolor="#f2f2f2"><input type="text" name="charges_per_shipment_value[]" value=""/><input type="button" value="-"  class="remove_button2" /><input type="button" class="add_button2" value="+" onclick="add_shipment();" /><br><span class="error" id="lblkilograms"></span></td></tr>'
                wrapper1.append(fieldHTML1);
            }

            $(document).ready(function () {
                $('select').select2();
                              
                $('#cities').select2({
                    ajax: {
                        url: "search_cities.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });
                
            });
            
            
            function verify() {
                var arrTmp = new Array();
                arrTmp[0] = checkfrom();
                arrTmp[1] = checkto();
                
                var i;

                _blk = true;

                for (i = 0; i < arrTmp.length; i++)   {

                    if (arrTmp[i] == false)  {
                        _blk = false;
                    }
                }

                if (_blk == true) {
                    return true;
                }else{
                    return false;
                }
            }

            function trim(str)  {
                if (str != null)  {
                    var i;
                    for (i = 0; i < str.length; i++)  {
                        if (str.charAt(i) != " ") {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }

                    for (i = str.length - 1; i >= 0; i--)   {
                        if (str.charAt(i) != " ")  {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }

                    if (str.charAt(0) == " ") {
                        return "";
                    }else{
                        return str;
                    }
                }
            }

            function checkfrom()  {
                if (trim(document.frmcat.from.value) == "")  {
                    document.getElementById("lblfrom").innerHTML = "Please enter Source City ";
                    return false;
                }else {
                    document.getElementById("lblfrom").innerHTML = "";
                    return true;
                }
            }

            function checkto()  {
                var type = $('#type1div input[type="radio"]:checked:first').val();
                
                if (trim(document.frmcat.to.value) == "" && type != 4)    {
                    document.getElementById("lblto").innerHTML = "Please enter Destination ";
                    return false;
                } else {
                    document.getElementById("lblto").innerHTML = "";
                    return true;
                }
            }

            function doshowtype()  {
                $('#spdiv').hide();
                $('#airdiv2').hide();

                if (1)  {
                    $('#typediv').show();
                    $('#cargodiv').show();
                    $('#type1div').show();
                    $('#type2div').hide();

                }else {
                    $('#typediv').show();
                    $('#cargodiv').hide();
                    $('#type2div').show();
                    $('#type1div').hide()
                }
            }

            function perbox() {
                $('#type1').hide();
                $('#type2').show();
            }

            function percubicfeet() {
                $('#type2').hide();
                $('#type1').show();
            }
</script>

<script>           
function selectsregion(){

   removeAllOptions(document.frmcat.region);

	addOption(document.frmcat.region,"","-- Select Regions --");

	  <?php

		$sel_reg1="select id , name , code , country_id from regions ";

		$res_reg1 = mysqli_query($con,$sel_reg1);

		while($rw_state=mysqli_fetch_array($res_reg1)){

	    ?>

		if(document.frmcat.country.value=="<?php echo $rw_state["country_id"]?>"){

			addOption(document.frmcat.region,"<?php echo $rw_state["id"];?>","<?php echo $rw_state["name"];?>");

			 $('#region').val('<?php echo $_POST["region"]?>');
		}

	  <?php
		} 
	  ?>	
	}	

	function removeAllOptions(selectbox){

		var i;

		for(i=selectbox.options.length-1;i>=0;i--)	{
			selectbox.remove(i);
		}
	}

	function addOption(selectbox,value,text){

		var optn=document.createElement("OPTION");

		optn.text=text;
		optn.value=value;

		selectbox.options.add(optn);
	}            
 </script>        
<?php include("includes/shipping-left.php"); ?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pagetitle">Edit Destiny </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td><table width="100%">
                                                                        <tr>
                                                                            <td><a class="pagetitle1" href="destiny.php" onclick="this.blur();"><span> Manage Destiny </span></a></td>
                                                                        </tr>
                                                                    </table></td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>

                                                            <tr>
                                                                <td><div id="box">
                                                                        
                                                                        <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data" action="destiny_edit.php?sid=<?php echo $_GET["sid"] ?>">                                                                        
                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td> 
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Destiny Description </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="descripcion" id="descripcion" value="<?php echo $dest["descripcion"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lbldescripcion"></span></td>
                                                                                </tr>                                                                                                                                                                
                                                                                

                                                                                <tr id="cargodiv">
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Country </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                        <select class="listmenu" name="country" id="country" onchange="selectsregion();" style="width:230px;">
                                                                                            <option value="">--Select--</option>
                                                                                            <?php
                                                                                            $sel_country = "select * from countries where id='" . $dest["country"] . "'";
                                                                                            $rs_country = mysqli_query($con, $sel_country);
                                                                                            while ($pais = mysqli_fetch_array($rs_country)) {
                                                                                                ?>
                                                                                                        <option value="<?php echo $pais["id"] ?>" <?php  if($dest["country"]==$pais["id"]) { echo "selected" ; } ?> >
                                                                                                            <?php echo $pais["name"] ?></option>
                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                        </select>
                                                                                        <br>
                                                                                            <span class="error" id="lblcountry"></span></td>
                                                                                </tr>

                                                                                
                                                                                
                                                                                
                                                                                <tr id="destinydiv">
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Regions </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                        <select class="listmenu" name="region" id="region" style="width:230px;">
                                                                                            <option value="">--Select--</option>
                                                                                        </select>
                                                                                        <br>
                                                                                            <span class="error" id="lblregion"></span></td>
                                                                                </tr>                                                                                
                                                                                
                                                                                
                                                                                <tr id="to_local">
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Cities </td>
                                                                                    <td style="float: left;height: 30px; padding: 8px 0 8px 8px; border:0px; border-bottom: 1px solid #e0e0e0;width: 98.5%;" width="66%" bgcolor="#f2f2f2">
                                                                                        <select class="listmenu" name="cities" id="cities" style="width:230px;">
                                                                                            <option value="">--Select--</option>
                                                                                        </select>
                                                                                        <br>
                                                                                            <span class="error" id="lblcities"></span></td>
                                                                                </tr>
                                                                                
                                                                               
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                                                                                        
                                                                                            
                                                                                            
                                                                                            
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                

                                                                                <tr>
                                                                                    <td colspan="2"><input name="Submit" type="Submit" class="buttongrey" value="Save" /></td>
                                                                                </tr>
                                                                            </table>
                                                                        </form>    
                                                                    </div></td>
                                                            </tr>

                                                        </table></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
<?php include("footer.php"); ?>
