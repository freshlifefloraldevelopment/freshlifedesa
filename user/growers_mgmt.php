<?php 	

include "../config/config_gcp.php";

//@session_start();
        
if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
        header("location: index.php");
}

if(isset($_GET['delete'])) {
    $query = 'DELETE FROM growers WHERE  id= '.(int)$_GET['delete'];
    mysqli_query($query);
}

$qsel="select g.*,a.uname as person 
         from growers g 
         left join admin a on a.id=g.person_of_charge   ";
        
//if($_SESSION['uid']!=1){
//    $qsel.=" WHERE ( g.person_of_charge='".$_SESSION['uid']."')";
//}

$qsel.=" order by g.growers_name";
$rs=mysqli_query($con, $qsel);
?>

<?php 
   // $sql = "SELECT growers.id, growers.growers_name FROM growers";
   // $res = mysqli_query($con,$sql);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">

            $(document).ready(function () {

                oTable = $('#example').dataTable({
                    //"sScrollXInner": "130%",

                    "bJQueryUI": true,
                    //"sScrollY": "536",

                    "sPaginationType": "full_numbers"

                });

            });

        </script>
    </head>

    <body>
        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
            <?php  include("includes/header_inner.php");?>
            <tr>
                <td height="5"></td>
            </tr>
            <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <?php  include("includes/left.php");?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pagetitle">Manage Growers</td>
                                                            </tr>

                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>


                                                            <?php 
                                                            if ($_SESSION['utype'] != "Grower") {
                                                                ?>			

                                                                <tr>
                                                                    <td>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td>
                                                                                    <a href="growers_add.php" class="pagetitle" onclick="this.blur();"><span> + Add New Grower  </span></a>

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <?php 
                                                            }
                                                            ?>

                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td><div id="box">
                                                                        <div id="container">			
                                                                            <div class="demo_jui">
                                                                                <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th width="22%" align="left">Grower</th> 
                                                                                            <th width="22%" align="left">Farm</th> 
                                                                                            <th width="22%" align="left">Parameter</th> 
                                                                                            <th align="center" width="9%">Status</th>
                                                                                            <?php 
                                                                                           // if ($_SESSION['uid'] == 1) {
                                                                                                ?>	
                                                                                                <!--th align="center" width="10%">Report</th-->
                                                                                                <th align="center" width="10%">User Rating</th>
                                                                                                <th align="center" width="10%">Buyer Rating</th>
                                                                                                <th align="center" width="10%">Admin Rating</th>
                                                                                                <?php 
                                                                                           // }
                                                                                            ?>
                                                                                            <th align="center" width="8%">Manage</th>
                                                                                            <th align="center" width="9%">Delete</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    
                                                                                    <tbody>

                                                                                        <?php 

                                                                                        $sr=1;

                                                                   while($product=mysqli_fetch_array($rs))      {
                                                                       
                                                                       
                                                                    $sel_control="select id , descripcion, time  
                                                                                    from general_parameters
                                                                                   where id = 4  ";
                                       
                                                                                $rs_control =mysqli_query($con,$sel_control);
                                                                                $control=mysqli_fetch_array($rs_control);
                                       
                                                                                $swc = $control["time"];                                                                       
                                                                       
                                                                       
                                                                       
                                                                                                $temp=explode(",",$product["products"]);  
                                                                                                $no=count($temp);							 

                                                                                        if($no>=3)   {
                                                                                                $no = $no-2; 
                                                                                        }else{
                                                                                                $no = 0;
                                                                                        }


                                                                                        $temp2=explode(",",$product["farms"]);  
                                                                                        $no2=count($temp2);							 

                                                                                        if($no2<=1)  {
                                                                                            if($no2>=3)     {
                                                                                                $no2 = $no2-2; 
                                                                                            }else {
                                                                                                $no2 = 0 ;
                                                                                            }
                                                                                        }

                                                                                        ?>

                                                                                        <tr class="gradeU">
                                                                                            <td class="text" align="left"><?php  echo $product["growers_name"] ?></td>
                                                                                            <td class="text" align="left"><?php 
                                                                                                if($no2>=1)  { 
                                                                                                    $o=1;
                                                                                                    $tempdss="";
                                                                                                    for($k=1;$k<=$no2;$k++) { 
                                                                                                        if(isset($temp2[$k])){
                                                                                                            $select_farm_names="select name from farms where id='".$temp2[$k]."'";
                                                                                                            $rs_farm_names=mysqli_query($con,$select_farm_names);

                                                                                                            while($farm_names=mysqli_fetch_array($rs_farm_names))      {
                                                                                                                $tempdss.= " " .$farm_names['name'] ." ,";
                                                                                                            }
                                                                                                        }
                                                                                                    }

                                                                                                    $str = trim($tempdss,","); 
                                                                                                    echo $str ;
                                                                                                }else{
                                                                                                    $select_farm_names="select name from farms where id='".$product["farms"]."'";                                                                                                
                                                                                                    $rs_farm_names=mysqli_query($con,$select_farm_names);
                                                                                                    while($farm_names=mysqli_fetch_array($rs_farm_names))  {
                                                                                                        echo $farm_names['name'];
                                                                                                    }
                                                                                                }
                                                                                                ?></td>
                                                                                            
                                                                                            <td align="center" ><a href="growers_parameter.php?id=<?php  echo $product["id"] ?>" style="color:#000; font-weight:bold;" >Parameter</a></td>                                                                                                                                                                                        

                                                                                            <td class="text" align="left"><?php if($product["active"]=='deactive') { echo "Deactive"; } else if($product["active"]=='active') { echo "Active"; } else { echo "Advertising"; } ?> </td>

                                                                                            <?php 
                                                                                           // if ($_SESSION['uid'] == 1) {
                                                                                                ?>	

                                                                                                <!--td class="text" align="center"><a href="print_boxgrow.php?idgw=<?php echo $product["id"]?> "  >Box</a> </td-->   

                                                                                                <?php 
                                                                                            //}
                                                                                            ?>

                                                                                            <?php  
                                                                                                $user_rating="select AVG(ur_final_rating) as user_rating from user_ratings where ur_user_type = 'user' AND ur_grower_idFk=".$product["id"];
                                                                                                $rs_user_rating=mysqli_query($user_rating);
                                                                                                $rating = mysqli_fetch_assoc($rs_user_rating);
                                                                                                $user_final_rating = round($rating['user_rating']);
                                                                                                if($user_final_rating == ''){
                                                                                                    $user_final_rating = 0;
                                                                                                }
                                                                                                
                                                                                            ?>
                                                                                            <td align="center" ><?php  echo $user_final_rating ?></td>
                                                                                            <?php  
                                                                                                $user_rating="select AVG(ur_final_rating) as buyer_rating from user_ratings where ur_user_type = 'buyer' AND ur_grower_idFk=".$product["id"];
                                                                                                $rs_user_rating=mysqli_query($user_rating);
                                                                                                $rating = mysqli_fetch_assoc($rs_user_rating);
                                                                                                $buyer_rating = round($rating['buyer_rating']);
                                                                                                if($buyer_rating == ''){
                                                                                                    $buyer_rating = 0;
                                                                                                }
                                                                                                
                                                                                            ?>
                                                                                            <td align="center" ><?php  echo $buyer_rating ?></td>
                                                                                            
                                                                                            <?php  
                                                                                                $user_rating="select AVG(ur_final_rating) as admin_rating from user_ratings where ur_user_type = 'admin' AND ur_grower_idFk=".$product["id"];
                                                                                                $rs_user_rating=mysqli_query($user_rating);
                                                                                                $rating = mysqli_fetch_assoc($rs_user_rating);
                                                                                                $admin_final_rating = round($rating['admin_rating']);
                                                                                                if($admin_final_rating == ''){
                                                                                                    $admin_final_rating = 0;
                                                                                                }
                                                                                                
                                                                                            ?>
                                                                                            <td align="center" ><?php  echo $admin_final_rating ?></td>  
                                                                                            
                                                                                            <?php  if($swc==1)      {?>
                                                                                                    <td align="center" ><a href="growers.php?id=<?php  echo $product["id"] ?>" style="color:#000; font-weight:bold;" >Manage</a></td>
                                                                                            <?php  }else{ ?>
                                                                                                    <td class="text" align="center">Manage </td>
                                                                                            <?php   }        ?>                                                                                            
                                                                                            
                                                                                            
                                                                                            <td align="center" ><a href="?delete=<?php  echo $product["id"] ?>"  onclick="return confirm('Are you sure, you want to delete this grower ?');"><img src="images/delete.gif" border="0" alt="Delete" /></a></td>
                                                                                        </tr>

                                                                                        <?php 

                                                                                        $sr++;
                                                                                        }
                                                                                        ?> 

                                                                                    </tbody>

                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>

                                                            </tr>

                                                        </table></td>

                                                    <td width="10">&nbsp;</td>

                                                </tr>

                                            </table></td>

                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>

                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>

                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <?php  include("includes/footer-inner.php"); ?>

            <tr>
                <td>&nbsp;</td>
            </tr>
            
        </table>
    </body>
</html>