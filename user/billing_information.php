<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";
        
   $id_shipping  = $_GET['id_bill'];
   $buyer = $_GET['buy'];
   
	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}

	  $qsel="select bsm.id              , bsm.buyer_id        , bsm.shipping_method_id , bsm.country      , bsm.own_shipping     , 
                        bsm.cargo_agency_id , bsm.grower_box_name , bsm.choose_shipping    , bsm.first_name   , bsm.last_name        , 
                        bsm.address         , bsm.state           , bsm.zipcode            , bsm.phone        , bsm.company          , 
                        bsm.airport_id      , bsm.days            , bsm.drop_off_option    , bsm.address_type , bsm.default_shipping , 
                        bsm.shipping_code   , c.name as country   , bsm.city               , bsm.tax
                  from buyer_shipping_methods bsm
                  left JOIN country c ON bsm.country = c.id
                 where bsm.shipping_method_id = '" . $id_shipping . "' ";      
        
	$rs=mysqli_query($con,$qsel);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					"bJQueryUI": true,

					"sPaginationType": "full_numbers"
				});

			} );

</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
  <?php include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <?php include("includes/shipping-left.php");?>
          <td width="5">&nbsp;</td>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="5"></td>
                          </tr>
                          <tr>
                            <td class="pagetitle">Billing Information</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>

                          <tr>
                            <td><table width="100%">
                                <tr>
                                 <a class="pagetitle" href="shipping.php?id=<?php echo $buyer?>" onclick="this.blur();"><span><u>Manage Shipping Method</u></span></a>                                               
                                </tr>
                              </table></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><div id="box">
                                <div id="container">
                                  <div class="demo_jui">
                                    <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                      <thead>
                                        <tr>
                                          <th width="6%" align="center">Sr.</th>
                                          <th width="30%" align="left">Company</th>
                                          <th width="30%" align="left">Name</th>
                                          <th width="35%" align="left">Address</th>                                          
                                          <th width="25%" align="left">Country</th>                                          
                                          <th width="35%" align="left">City</th>                                           
                                          <th width="25%" align="left">Tax</th>                                                                                     
                                          <th width="35%" align="left">Phone</th>                                          
                                          <th align="center" width="15%">Edit</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php

						  	$sr=1;

						  while($billing=mysqli_fetch_array($rs))

						  {

						  ?>
                                        <tr class="gradeU">
                                          <td align="center" class="text"><?php echo $sr;?></td>
                                          <td class="text" align="left"><?php echo $billing["company"]?></td>                                                                                    
                                          <td class="text" align="left"><?php echo $billing["first_name"]." ".$billing["last_name"]?></td>
                                          <td class="text" align="left"><?php echo $billing["address"]?></td>                                          
                                          <td class="text" align="left"><?php echo $billing["country"]?></td>                                          
                                          <td class="text" align="left"><?php echo $billing["city"]?></td>
                                          <td class="text" align="left"><?php echo $billing["tax"]?></td>
                                          <td class="text" align="left"><?php echo $billing["phone"]?></td>                                          
                                          <td align="center" ><a href="billing_infor_edit.php?id=<?php echo $billing["id"]."&id_ship=".$billing["shipping_method_id"]?>"><img src="images/edit.gif" border="0" alt="Edit" /></a></td>
                                        </tr>
                                        <?php

						 		$sr++;

						 	}

						 ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div></td>
                          </tr>
                        </table></td>
                      <td width="10">&nbsp;</td>
                    </tr>
                  </table></td>
                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
              </tr>
              <tr>
                <td background="images/middle-leftline.gif"></td>
                <td>&nbsp;</td>
                <td background="images/middle-rightline.gif"></td>
              </tr>
              <tr>
                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                <td background="images/middle-bottomline.gif"></td>
                <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <?php include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
