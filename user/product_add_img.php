<?php	

	include "../config/config_gcp.php";

        $grow_num = $_GET["id_grow"];
        $prod_num = $_GET["pdid"];

	@session_start();


	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}

	
	if(isset($_POST["Submit"]) && $_POST["Submit"]=="Add")	{	   
		
                                    $today = date('mdyHis');
                                    $tmp1  = $_FILES['image']['name'];
                                    $ext1  = explode('.',$tmp1);
                                    $image = 0;       
                                    
                                    $uploaddir = '/var/www/html/product-image/big/';                                                
                                                                        
                                    $sub       = $_POST["description"];
                                    $desc      = str_replace("../gallery/", "gallery/",$sub);										

					if($ext1[1]=="JPG"){
                                                $extention="jpg";
					}else if($ext1[1]=="GIF"){
                                                $extention="gif";
					}else if($ext1[1]=="PNG"){
                                                $extention="png";
					}else{
                                                $extention=$ext1[1];
					}
										
                                                                                $uploadfile1 = $uploaddir.$today."_crop.".$extention;                                                
                                                                                
										$filepath1 = 'product-image/'.$today."big.".$extention;
										$filepath3 = 'product-image/big/'.$today."_crop.".$extention;	
                                                                                
										move_uploaded_file($_FILES['image']['tmp_name'],$uploadfile1);				

										list($width, $height) = getimagesize("../$filepath1");										
                                                                                
										$image->new_width  = 493;
										$image->new_height = 456;										
										$image->image_to_resize = "../$filepath1"; // Full Path to the file                                                                                								
										$image->ratio = false; // Keep Aspect Ratio?												
                                                                                
                                                                                // Name of the new image (optional) - If it's not set a new will be added automatically								
                                                                                $image->new_image_name =$today.'_crop';								
                                                                                        /* Path where the new image should be saved. If it's not set the script will output the image without saving it */								
										$image->save_folder = 'product-image/big/';	
                                                                                
										// po $process = $image->resize();	
                                                                                
										unlink("/$filepath1");
											
					$ins="insert into grower_product_img set 
                                                                        product_id     = '".$prod_num   ."',
                                                                        grower_id      = '".$grow_num   ."',
                                                                        image_path     = '".$filepath3  ."'   ";
                                                                                              
					mysqli_query($con,$ins); 

					header("location:grower_product_img.php?id=$grow_num");
												
												
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script type="text/javascript">

   

	function verify(){ 

		var arrTmp=new Array();

		arrTmp[0]=checkname();
		arrTmp[1]=checkcategory();                
		arrTmp[2]=checksubcategory();                                

		arrTmp[4]=checkcolor();                                
		arrTmp[5]=checkimage();

		var i;

		_blk=true;

		for(i=0;i<arrTmp.length;i++)	{

			if(arrTmp[i]==false)	{
			   _blk=false;
			}
		}

		if(_blk==true)	{
			return true;
		}else{
			return false;
		}	
 	}	

	function trim(str) {    

		if (str != null) {        

			var i;        

			for (i=0; i<str.length; i++) {           
				if (str.charAt(i)!=" ") {               
					str=str.substring(i,str.length);                 
					break;            
				}        
			}            

			for (i=str.length-1; i>=0; i--)	{            
				if (str.charAt(i)!=" ") {                
					str=str.substring(0,i+1);                
					break;            
				}         
			}                 

			if (str.charAt(0)==" ") {            
				return "";         
			} else 	{            
				return str;         
			}    
		}
	}

	

	function checkname(){    // 0

		if(trim(document.frmproduct.pname.value) == "")		{	 

			document.getElementById("lblpname").innerHTML="Please enter product name";

			return false;

		}else {

			document.getElementById("lblpname").innerHTML="";

			return true;
		}
	}

	function checkcategory(){   // 1

		if(trim(document.frmproduct.pcategory.value) == ""){	 

			document.getElementById("lblpcategory").innerHTML="Please select product category";

			return false;

		}else {

			document.getElementById("lblpcategory").innerHTML="";

			return true;
		}
	}

	function checksubcategory(){   // 2

		if(trim(document.frmproduct.subcategory.value) == ""){	 

			document.getElementById("lblsubcategory").innerHTML="Please select product subcategory";

			return false;

		}else {

			document.getElementById("lblsubcategory").innerHTML="";

			return true;
		}
	}

	function checkcolor(){   // 4

		if(trim(document.frmproduct.color.value) == ""){	 

			document.getElementById("lblcolors").innerHTML="Please select product color";

			return false;

		}else {

			document.getElementById("lblcolors").innerHTML="";

			return true;
		}
	}
	function checkcode(){    //  

		if(trim(document.frmproduct.pcode.value) == "")	{	 

			document.getElementById("lblpcode").innerHTML="Please enter product code";

			return false;

		}else {

			document.getElementById("lblpcode").innerHTML="";

			return true;
		}
	}

	function checkprice(){      // 

		if(trim(document.frmproduct.price.value) == "")	{	 

			document.getElementById("lblprice").innerHTML="Please enter product price";

			return false;

		}else {

			document.getElementById("lblprice").innerHTML="";
			return true;
		}
	}

	function checkdisplayorder(){     // 

		if(trim(document.frmproduct.display_order.value) == "")	{	 

			document.getElementById("lbldisplay_order").innerHTML="Please enter display order";

			return false;

		}else {

			document.getElementById("lbldisplay_order").innerHTML="";

			return true;
		}
	}

	function checkshort_desc(){    // 

		if(trim(document.frmproduct.short_desc.value) == "")	{	 

			document.getElementById("lblshort_desc").innerHTML="Please enter short description";

			return false;

		}else {

			document.getElementById("lblshort_desc").innerHTML="";

			return true;
		}
	}

	function checkimage(){    // 5

		if(trim(document.frmproduct.image.value) == "")	{	 

			document.getElementById("lblimage").innerHTML="Please upload image";

			return false;

		}else {

			if(!validImageFile(document.frmproduct.image.value))	{

				document.getElementById("lblimage").innerHTML="Please select valid image file";

				return false;

			}else{

				document.getElementById("lblimage").innerHTML="";

				return true;
			}
		}
	}
</script>

<script type="text/javascript">

	function textareaCounter(field,cntfield,maxlimit) 

	{

		if (field.value.length > maxlimit) 

		{

			field.value = field.value.substring(0, maxlimit);

		}

		else

		{

			cntfield.value = maxlimit - field.value.length;

		}



	}

	function funLoad()

	{

		var xmaxlimit1=220;

		var xmaxlimit2=220;

		document.frmproduct.txtLen1.value=xmaxlimit1-document.frmproduct.short_desc.value.length;

		

	}

	function validImageFile(strfile)

	{

		var str = strfile;

		var pathLenth = strfile.length;

		var start = (str.lastIndexOf("."));

		var fileType = str.slice(start,pathLenth);

		fileType = fileType.toLowerCase();

		if (strfile.length > 0)

		{

		   if((fileType == ".gif") || (fileType == ".jpg") || (fileType == ".jpeg") || (fileType == ".png") || (fileType == ".bmp") || (fileType == ".GIF") || (fileType == ".JPG") || (fileType == ".JPEG") || (fileType == ".PNG") || (fileType == ".BMP")) 

		   {

				return true;

		   }

		   else 

		   {

				return false;

		   } 

		}

	}

</script>

<script>
function selectcolor()

	{
                var selected_text = $("#subcategory option:selected").text();
                
                $('input[name=sname]').val(selected_text);            
        }
function selectscategory()

	{

   removeAllOptions(document.frmproduct.subcategory);

	addOption(document.frmproduct.subcategory,"","-- Select Subcategory --");

	  <?php

		$sel_subcategory="select * from subcategory order by name";

		$res_subcategory=mysqli_query($con,$sel_subcategory);

		while($rw_subcategory=mysqli_fetch_array($res_subcategory))

		{

	  ?>

		if(document.frmproduct.pcategory.value=="<?php echo $rw_subcategory["cat_id"]?>")

		{

			addOption(document.frmproduct.subcategory,"<?php echo $rw_subcategory["id"];?>","<?php echo $rw_subcategory["name"];?>");

			 $('#subcategory').val('<?php echo $_POST["subcategory"]?>');                                                  

		}
                

                
                

	  <?php

		} 

	  ?>	

	}	

	function removeAllOptions(selectbox)

	{

		var i;

		for(i=selectbox.options.length-1;i>=0;i--)

		{

			selectbox.remove(i);

		}

	}



	function addOption(selectbox,value,text)

	{

		var optn=document.createElement("OPTION");

		optn.text=text;

		optn.value=value;

		selectbox.options.add(optn);

	}

</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

  <?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <?php include("includes/product-left.php");?>

          <td width="5">&nbsp;</td>

          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr>

                      <td width="10">&nbsp;</td>

                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                          <tr>

                            <td height="5"></td>

                          </tr>

                          <tr>

                            <td class="pagetitle">Add New IMG</td>

                          </tr>

                          <tr>

                            <td>&nbsp;</td>

                          </tr>

                          <tr>

                            <td><table width="100%">

                                <tr>

                                  <!--td><a class="pagetitle1" href="product_mgmt.php" onclick="this.blur();"><span> Manage Products</span></a> </td-->

                                </tr>

                              </table></td>

                          </tr>

                          <tr>

                            <td>&nbsp;</td>

                          </tr>

                          <form name="frmproduct" method="post" onsubmit="return verify();" enctype="multipart/form-data">

                            <tr>

                              <td><div id="box">

                                  <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">

                                    <tr>

                                      <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>

                                    </tr>
                                     
                                      
                                                                            
                                      


                                                                         
                                                                            


                                    

                                    <tr>

                                      <td align="left" valign="middle" class="text">&nbsp;<span class="error">*</span>&nbsp; Image : </td>

                                      <td bgcolor="#f2f2f2"><input type="file" name="image" id="image" /> <b> Image width should greater than height </b>

                                        <br>

                                        <span class="error" id="lblimage"></span> </td>

                                    </tr>
                                                                                                

                          


                          

                          

                                    <tr>

                                      <td>&nbsp;</td>

                                      <td><input name="Submit" type="Submit" class="buttongrey" value="Add" />

                                      </td>

                                    </tr>

                                  </table>

                                </div></td>

                            </tr>

                          </form>

                        </table></td>

                      <td width="10">&nbsp;</td>

                    </tr>

                  </table></td>

                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

              </tr>

              <tr>

                <td background="images/middle-leftline.gif"></td>

                <td>&nbsp;</td>

                <td background="images/middle-rightline.gif"></td>

              </tr>

              <tr>

                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

                <td background="images/middle-bottomline.gif"></td>

                <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

              </tr>

            </table></td>

        </tr>

      </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>

</body>

</html>
