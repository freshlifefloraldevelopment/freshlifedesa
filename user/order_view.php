<?	
	include "../config/config.php";
	session_start();

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
	{
		header("location: index.php");
	}

	if(isset($_POST['Submit']) && $_POST['Submit'] == "Save")
	{
		if(($_POST['orderstatus'] == "Payment Received") || ($_POST['orderstatus'] == "Order In Process") || ($_POST['orderstatus'] == "Order Shipped") || ($_POST['orderstatus'] == "Partially Shipped") || ($_POST['orderstatus'] == "Order Delivered"))
		{
			$sel_ord1="select coupon_code,iscoupon_applied,status from `order` where order_id=".$_GET['id']."";
			$rs_ord1=mysql_query($sel_ord1);
			$ord1=mysql_fetch_array($rs_ord1);
			
			if($ord1["iscoupon_applied"] == "Yes" && $ord1["status"] == "Unpaid Order")
			{
					$couponcode=$ord1["coupon_code"];
					$upd_cp="update coupon_master set tot_used=tot_used + 1 where coupon_code='".$couponcode."'";
					mysql_query($upd_cp);
			}
		}
		
		$upd_order = "UPDATE `order` SET status='".$_POST['orderstatus']."', admin_note='".$_POST['admin_note']."' WHERE order_id=".$_GET['id']."";
		
		$upd_exe = mysql_query($upd_order);
		if($upd_exe)
		{
			header("location: order_view.php?id=".$_GET['id']."");
		}
	}
?>
<?
	$sel_ord="select * from `order` where order_id=".$_GET["id"];
	$rs_ord=mysql_query($sel_ord);
	$ord=mysql_fetch_array($rs_ord);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
 <? include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
       <? include("includes/left.php");?>
        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10">&nbsp;</td>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="5"></td>
                  </tr>
                  <tr>
                    <td class="pagetitle">Details of Order </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
				  
                  <tr>
                    <td><a class="pagetitle1" href="order_mgmt.php" onclick="this.blur();"><span> Manage Orders</span></a> </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
				   <?
				  	if($_GET["invsend"] == "1")
					{
				  ?>
				   <tr>
                    <td class="error" align="center"><b>Invoice Send Successfully.</b></td>
                  </tr>
				  <tr>
                    <td>&nbsp;</td>
                  </tr>
				  <?
				  	}
				  ?>
                  <tr>
                    <td><div id="box">
                      <table style="border-collapse: collapse;" width="100%" border="1" cellpadding="2"
                                            cellspacing="0" bordercolor="#e4e4e4">
                        <tr>
                          <td width="17%" align="left" class="text11"><b>Order ID : </b> </td>
                          <td colspan="2" bgcolor="#f2f2f2" class="text11"><b><?=$ord["order_id"]?></b></td>
                        </tr>
					    <tr>
                          <td width="17%" align="left" class="text11"><b>Customer Name</b> </td>
                          <td colspan="2" bgcolor="#f2f2f2" class="text11"><b><?=$ord["cust_name"]?></b></td>
                        </tr>
						<?
							$odt=explode("-",$ord["order_date"]);
							$orderdate=$odt["2"]."-".$odt["1"]."-".$odt["0"];
						?>
                        <tr>
                          <td align="left" valign="top" class="text11"><b>Order Date : </b></td>
							  <td colspan="2" bgcolor="#f2f2f2" class="text11"><b><?=$orderdate;?></b></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top" class="text11"><b>Order Status : </b></td>
							  <td colspan="2" bgcolor="#f2f2f2" class="text11blue"><b><?=$ord["status"]?></b></td>
                        </tr>
						<tr>
                          <td align="left" valign="top" class="text11"><b>Transaction Id : </b></td>
							  <td colspan="2" bgcolor="#f2f2f2" class="text11"><b><?=$ord["txn_id"]?></b></td>
                        </tr>
						<tr>
                          <td align="left" valign="top" class="text11"><b>Auth Code : </b></td>
							  <td colspan="2" bgcolor="#f2f2f2" class="text11"><b><?=$ord["authcode"]?></b></td>
                        </tr>
                        <tr>
                          <td colspan="3" align="left" class="text">
						  		<table border="0" width="100%">
									<tr>
          		  <td class="text11" valign="top">
				  		<table border="0" width="100%"  style="border-collapse:collapse; border-color:#e4e4e4;">
							<tr height="22">
								<td colspan="2" class="text11" align="center"><b>Billing Details</b></td>
							</tr>
							<tr>
							  <td colspan="2"  height="10"></td>
						    </tr>
							<tr height="22">
								<td width="42%" class="text11"><b>First name :</b></td>
								<td width="58%" class="text11"><b><?=$ord["bfname"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>Last name :</b></td>
								<td class="text11"><b><?=$ord["blname"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>Email address :</b></td>
								<td class="text11"><b><?=$ord["bemail"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>Address :</b></td>
								<td class="text11"><b><?=$ord["baddress"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>City :</b></td>
								<td class="text11"><b><?=$ord["bcity"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>State :</b></td>
								<td class="text11"><b><?=$ord["bstate"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>Country :</b></td>
								<td class="text11"><b><?=$ord["bcountry"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>Telephone :</b></td>
								<td class="text11"><b><?=$ord["btelephone"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>Mobile :</b></td>
								<td class="text11"><b><?=$ord["bmobile"]?></b></td>
							</tr>
						</table>				  </td>
         		  <td class="text11">
				  		<table border="0" width="100%"  style="border-collapse:collapse; border-color:#e4e4e4;">
							<tr height="22">
								<td colspan="2" class="text11" align="center"><b>Dispatch Details</b></td>
							</tr>
							<tr>
							  <td colspan="2"  height="10"></td>
						    </tr>
							<tr height="22">
								<td width="40%" class="text11"><b>First name :</b></td>
								<td width="60%" class="text11"><b><?=$ord["dfname"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>Last name :</b></td>
								<td class="text11"><b><?=$ord["dlname"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>Email address :</b></td>
								<td class="text11"><b><?=$ord["demail"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>Address :</b></td>
								<td class="text11"><b><?=$ord["daddress"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>City :</b></td>
								<td class="text11"><b><?=$ord["dcity"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>State :</b></td>
								<td class="text11"><b><?=$ord["dstate"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>Country :</b></td>
								<td class="text11"><b><?=$ord["dcountry"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>Telephone :</b></td>
								<td class="text11"><b><?=$ord["dtelephone"]?></b></td>
							</tr>
							<tr height="22">
								<td class="text11"><b>Mobile :</b></td>
								<td class="text11"><b><?=$ord["dmobile"]?></b></td>
							</tr>
						</table>				  </td>
         		  </tr>
								</table>						  </td>
                          </tr>
                        <tr>
                          <td colspan="3" align="left" class="text">&nbsp;</td>
                          </tr>
                        <tr>
                          <td colspan="3" align="center" class="text11blue"><b>Product Information</b></td>
                        </tr>
                        <tr>
                          <td colspan="3" align="left" class="text">
						  		<table style="border-collapse: collapse;" width="100%" border="1" cellpadding="0"
                                            cellspacing="0" bordercolor="#e4e4e4">
                        <tr>
                          <td width="23%" height="30" align="center" bgcolor="#f2f2f2" class="text"><b>Product Name </b></td>
                          <td width="19%" align="center" bgcolor="#f2f2f2" class="text"><b>Product Code</b> </td>
                          <td width="13%" align="center" bgcolor="#f2f2f2" class="text"><b>Qty</b></td>
                          <td width="23%" align="center" bgcolor="#f2f2f2" class="text"><b>Price [ <?=$currency ?> ] </b></td>
                          <td width="22%" align="center" bgcolor="#f2f2f2" class="text"> <b>Total [<?=$currency ?> ] </b> </td>
                          </tr>
						  <?
						    $totalweight=0;
							$subweight=0;
							
						  	$sel_item="select * from order_item where order_id=".$ord["order_id"];
							$rs_item=mysql_query($sel_item);
							while($item=mysql_fetch_array($rs_item))
							{
						  ?>
                        <tr height="25">
                          <td bgcolor="#F4F4F4" align="center"><?=$item["prod_name"]?></td>
                          <td bgcolor="#F4F4F4" align="center"><?=$item["prod_code"]?></td>
                          <td bgcolor="#F4F4F4" align="center"><?=$item["qty"]?></td>
                          <td bgcolor="#F4F4F4" align="center"><?=$item["price"]?> </td>
                          <td bgcolor="#F4F4F4" align="center"><?=$item["subtotal"]?></td>
                        </tr>
							
						  <?
						  		$subweight=( $item["qty"] * $item["prod_weight"] );
								$totalweight=($totalweight + $subweight);
						  	}
						  ?>
						  <tr>
						  	<td colspan="5" height="1" bgcolor="#9A4214"></td>
						  </tr>						
						
						<tr height="25">
                          <td bgcolor="#F4F4F4"></td>
                          <td bgcolor="#F4F4F4">&nbsp;</td>
                          <td bgcolor="#F4F4F4">&nbsp;</td>
                          <td bgcolor="#F4F4F4" align="center"><b>Total Price ( In <?=$currency ?> ) :</b></td>
						  <td bgcolor="#F4F4F4" align="center"><b><?=$ord["subtotal"]?>&nbsp;</b></td>
                        </tr>
                        <tr height="25">
                          <td bgcolor="#F4F4F4"></td>
                          <td bgcolor="#F4F4F4">&nbsp;</td>
                          <td bgcolor="#F4F4F4">&nbsp;</td>
                          <td bgcolor="#F4F4F4" align="center"><b>Shipping Charge ( In <?=$currency ?> ) :</b></td>
						  <td bgcolor="#F4F4F4" align="center"><b><?=$ord["shipping_charge"]?>&nbsp;</b></td>
                        </tr>
                         <tr height="25">
                          <td bgcolor="#F4F4F4"></td>
                          <td bgcolor="#F4F4F4">&nbsp;</td>
                          <td bgcolor="#F4F4F4">&nbsp;</td>
                          <td bgcolor="#F4F4F4" align="center"><b>Grand Total ( In <?=$currency ?> ) :</b></td>
						  <td bgcolor="#F4F4F4" align="center"><b><?=$ord["grandtotal"]?>&nbsp;</b></td>
                        </tr>
                         <tr height="25">
                          <td bgcolor="#F4F4F4"></td>
                          <td bgcolor="#F4F4F4">&nbsp;</td>
                          <td bgcolor="#F4F4F4">&nbsp;</td>
                          <td bgcolor="#F4F4F4" align="center"><b>Grand Total ( In USD ) :</b></td>
                          <? $usdtotal=((int)($ord["grandtotal"]*0.14527)); ?>
						  <td bgcolor="#F4F4F4" align="center"><b><?=$usdtotal?>&nbsp;</b></td>
                        </tr>
                    </table>						  </td>
                          </tr>
                        <tr>
                          <td align="left" class="text">&nbsp;</td>
                          <td colspan="2" bgcolor="#f2f2f2" class="text">&nbsp;</td>
                        </tr>
				<form action="order_view.php?id=<?=$_GET["id"]?>" method="post" >		
                        <tr>
                          <td align="left" class="text"><b>Order Status :</b> </td>
                          <td colspan="2" bgcolor="#f2f2f2" class="text">
						  	<select name="orderstatus">
									
									<option value="Unpaid Order" <?php if($ord["status"] == "Unpaid Order") echo "selected";?>>Unpaid Order</option>
									<option value="Payment Received" <?php if($ord["status"] == "Payment Received") echo "selected";?>>Payment Received</option>
									<option value="Order In Process" <?php if($ord["status"] == "Order In Process") echo "selected";?>>Order In Process</option>
									<option value="Order Shipped" <?php if($ord["status"] == "Order Shipped") echo "selected";?>>Order Shipped</option>
									<option value="Partially Shipped" <?php if($ord["status"] == "Partially Shipped") echo "selected";?>>Partially Shipped</option>
									<option value="Order Delivered" <?php if($ord["status"] == "Order Delivered") echo "selected";?>>Order Delivered</option>
									<option value="Order Cancelled" <?php if($ord["status"] == "Order Cancelled") echo "selected";?>>Order Cancelled</option>
							</select>						  </td>
                        </tr>
                        <tr>
                          <td align="left" class="text"><b>Admin Note :</b></td>
                          <td colspan="2" bgcolor="#f2f2f2" class="text"><textarea name="admin_note" class="textarea" id="admin_note"><?=$ord["admin_note"]?></textarea></td>
                        </tr>
						 <tr>
                          <td align="left" class="text">&nbsp;</td>
                          <td colspan="2" bgcolor="#f2f2f2" class="text"><input type="Submit" name="Submit" value="Save" class="buttongrey"></td>
                        </tr>
				</form>		
						 <tr>
                          <td align="left" class="text">&nbsp;</td>
                          <td width="64%" bgcolor="#f2f2f2" class="text">&nbsp;</td>
                          <td width="19%" bgcolor="#f2f2f2" class="text"><a href="send-invoice.php?id=<?=$_GET["id"]?>" onclick="return confirm('Are you sure you want to send invoice ?');"><img src="images/send-invoice.gif" border="0"></a></td>
						 </tr>
                      </table>
                    </div></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                </table></td>
                <td width="10">&nbsp;</td>
              </tr>
            </table></td>
            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>
          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>
          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <? include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
