<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";
  $fact_number = $_GET['id_fact'];
  $offer_id    = $_GET['id_ord'];
  
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {

    header("location: index.php");
}

if (isset($_POST["Submit"]) && $_POST["Submit"] == "Save") {
    
    $ins = "update invoice_requests 
               set salesPriceCli ='" . $_POST["salesPriceCli"] . "' , 
                   price_Flf     ='" . $_POST["price_Flf"] . "' , 
                   bunchqty      ='" . $_POST["bunchqty"] . "' ,
                   size          ='" . $_POST["size"]     . "' ,
                   prod_name     ='" . $_POST["prod_name"] . "' ,                       
                   steams        ='" . $_POST["steams"]   . "'                                              
             where id_fact  = '" . $fact_number . "'
               and offer_id = '" . $offer_id . "' ";
    
    // If stem_bunch product
    //$ins_tot = "update invoice_orders io
    //           join (select id_fact,sum(steams*bunchqty*salesPriceCli) as subt from invoice_requests where id_fact = '" . $fact_number . "') ir on io.id_fact = ir.id_fact
    //           set sub_total_amount = subt
    //           where io.id_fact = '" . $fact_number . "' ";
   
    
    if(mysqli_query($con, $ins))
   //  mysqli_query($con, $ins_tot);
    header("location:manage_update_invoice.php?id_fact=$fact_number");
    
}


$sel_invoice = "select id_fact            , id_order       , order_serial  , 
                            cod_order          , product        , sizeid        , 
                            qty                , buyer          , date_added    , 
                            bunches            , box_name       , lfd           , 
                            comment            , box_id         , shpping_method, 
                            mreject            , bunch_size     , unseen        , 
                            inventary          , offer_id       , prod_name     , 
                            product_subcategory, size           , boxtype       , 
                            bunchsize          , boxqty         , bunchqty      , 
                            steams             , gorPrice       , box_weight    , 
                            box_volumn         , grower_box_name, reject        , 
                            reason             , coordination   , cargo         , 
                            color_id           , gprice         , tax           , 
                            cost_ship          , round(handling,0) as handling  , 
                            grower_id          , offer_id_index,
                            substr(rg.growers_name,1,19) as name_grower ,salesPriceCli , price_Flf      
                       from invoice_requests ir
                      INNER JOIN growers rg ON ir.grower_id = rg.id                  
                      where id_fact  = '" . $fact_number . "'
                        and offer_id = '" . $offer_id . "' ";

$rs_invoice = mysqli_query($con, $sel_invoice);
$row = mysqli_fetch_array($rs_invoice);
   

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>Admin Area</title>

        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>

        <script type="text/javascript">
            function verify()  {
                var arrTmp = new Array();
                arrTmp[0] = checkfrom();
                var i;

                _blk = true;

                for (i = 0; i < arrTmp.length; i++)  {
                    if (arrTmp[i] == false)                    {
                        _blk = false;
                    }
                }

                if (_blk == true)  {
                    return true;
                } else {
                    return false;
                }
            }

            function trim(str) {
                if (str != null) {
                    var i;
                    for (i = 0; i < str.length; i++)  {
                        if (str.charAt(i) != " ")  {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }

                    for (i = str.length - 1; i >= 0; i--) {
                        if (str.charAt(i) != " ") {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }

                    if (str.charAt(0) == " ")  {
                        return "";
                    }else {
                        return str;
                    }
                }
            }

            function checkfrom()       {
                if (trim(document.frmcat.salesPriceCli.value) == "")  {
                    document.getElementById("lblfrom").innerHTML = "Please enter price ";
                    return false;
                }else {
                    document.getElementById("lblfrom").innerHTML = "";
                    return true;
                }
                
                if (trim(document.frmcat.price_Flf.value) == "")  {
                    document.getElementById("lblflf").innerHTML = "Please enter price Fresh Life ";
                    return false;
                }else {
                    document.getElementById("lblflf").innerHTML = "";
                    return true;
                }                
                
                
                if (trim(document.frmcat.prod_name.value) == "")  {
                    document.getElementById("lblprod_name").innerHTML = "Please enter product name ";
                    return false;
                }else {
                    document.getElementById("lblprod_name").innerHTML = "";
                    return true;
                }                
                
                
                
                
                if (trim(document.frmcat.bunchqty.value) == "")  {
                    document.getElementById("lblbunch").innerHTML = "Please enter bunches ";
                    return false;
                }else {
                    document.getElementById("lblbunch").innerHTML = "";
                    return true;
                }                
                if (trim(document.frmcat.size.value) == "")  {
                    document.getElementById("lblsize").innerHTML = "Please enter size ";
                    return false;
                }else {
                    document.getElementById("lblsize").innerHTML = "";
                    return true;
                }                                
                if (trim(document.frmcat.steams.value) == "")  {
                    document.getElementById("lblsteams").innerHTML = "Please enter stems ";
                    return false;
                }else {
                    document.getElementById("lblsteams").innerHTML = "";
                    return true;
                }                                                
            }

        </script>
    </head>
    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php include("includes/header_inner.php"); ?>

            <tr>

                <td height="5"></td>

            </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php include("includes/agent-left.php");?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>

                                                                <td height="5"></td>

                                                            </tr>

                                                            <tr>

                                                                <td class="pagetitle">Edit Invoice</td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <tr>

                                                                <td>

                                                                    <table width="100%">

                                                                        <tr>

                                                                            <td>

                                                                                <a class="pagetitle1" href="manage_update_invoice.php" onclick="this.blur();"><span> Invoice</span></a>

                                                                            </td>

                                                                        </tr>

                                                                    </table>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data" action="update_invoice_edit.php?id_fact=<?php echo $row["id_fact"]."&id_ord=".$row["offer_id"] ?>">

                                                                <tr>

                                                                    <td><div id="box">

                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">

                                                                                <tr>



                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>

                                                                                </tr>

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Price </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="salesPriceCli" id="salesPriceCli" value="<?php echo $row["salesPriceCli"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblfrom"></span></td>
                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Price Freslifefloral</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="price_Flf" id="price_Flf" value="<?php echo $row["price_Flf"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblflf"></span></td>
                                                                                </tr> 
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Product Name</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="prod_name" id="prod_name" value="<?php echo $row["prod_name"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblprod_name"></span></td>
                                                                                </tr>                                                                                



                                                                                
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Bunches </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="bunchqty" id="bunchqty" value="<?php echo $row["bunchqty"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblbunch"></span></td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Size </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="size" id="size" value="<?php echo $row["size"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblsize"></span></td>
                                                                                </tr> 
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Stems </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="steams" id="steams" value="<?php echo $row["steams"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblsteams"></span></td>
                                                                                </tr>                                                                                                                                                                
                                                                                
                                                                                <tr>

                                                                                    <td>&nbsp;</td>

                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Save" /> </td>

                                                                                </tr>

                                                                            </table>

                                                                        </div></td>

                                                                </tr>

                                                            </form>

                                                        </table></td>

                                                    <td width="10">&nbsp;</td>

                                                </tr>

                                            </table></td>

                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

                                    </tr>

                                    <tr>

                                        <td background="images/middle-leftline.gif"></td>

                                        <td>&nbsp;</td>

                                        <td background="images/middle-rightline.gif"></td>

                                    </tr>

                                    <tr>

                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

                                        <td background="images/middle-bottomline.gif"></td>

                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

                                    </tr>

                                </table></td>

                        </tr>

                    </table></td>

            </tr>

            <tr>

                <td height="10"></td>

            </tr>

            <?php include("includes/footer-inner.php"); ?>

            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>