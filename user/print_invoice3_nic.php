<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_GET['id_buy'];
    $idfac = $_GET['b'];

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name 
                      from buyers  b , country c 
                     where b.id = '" . $userSessionID . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id_fact          , buyer_id         , order_number, 
                         order_date       , shipping_method  , del_date    , 
                         date_range       , is_pending       , order_serial, 
                         seen             , delivery_dates   , lfd_grower  , 
                         quick_desc       , bill_number      , gross_weight, 
                         volume_weight    , freight_value    , guide_number, 
                         total_boxes      , sub_total_amount , tax_rate    , 
                         shipping_charge  , handling         , grand_total , 
                         bill_state       , date_added       , user_added  ,
                         air_waybill      , charges_due_agent,
                         credit_card_fees , per_kg                         
                         from invoice_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id_fact = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Requests

   $sqlDetalis="select c.name as cate , sc.name as subcate,
                       sum(ir.steams * ir.bunchqty ) as steams,
                       sum(ir.steams * ir.bunchqty * 0.12) as total,
                       count(*) reg
                  from invoice_requests ir
                 INNER JOIN growers rg     ON ir.grower_id = rg.id                  
                 INNER JOIN product p      ON ir.product = p.id                  
                 INNER JOIN category c     ON c.id = p.categoryid                  
                 INNER JOIN subcategory sc ON sc.id = p.subcategoryid                  
                 where ir.buyer    = '" . $buyer_cab . "'
                   and ir.id_fact  = '" . $id_fact_cab . "'
                 group by c.name , sc.name ";   
        $result   = mysqli_query($con, $sqlDetalis);    

    $pdf = new PDF();
    
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'COMMERCIAL INVOICE',0,0,'L'); 
    $pdf->Image('logo.png',148,5); 
    
    $pdf->Ln(10);
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Cell(0,10,'Shipping Details ',0,0,'R');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,0,'L');
    $pdf->Cell(0,6,'INVOICE: '.$buyerOrderCab['order_number'],0,1,'R');  
    
    $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,0,'L');    
    $pdf->Cell(0,6,'Total Boxes: '.$buyerOrderCab['total_boxes'],0,1,'R');    
    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,0,'L');        
    $pdf->Cell(0,6,'Gross Weight: '.$buyerOrderCab['gross_weight'],0,1,'R');        
    
    $pdf->Cell(70,6,'Ferrari Bridge Co. WLL: ');  
    $pdf->Cell(0,6,'Volume Weight: '.$buyerOrderCab['volume_weight'],0,1,'R');  
    
    $pdf->Ln(10);

    $pdf->Cell(70,6,'Category',0,0,'L');
    $pdf->Cell(25,6,'Subcategory',0,0,'C');
    $pdf->Cell(25,6,'Steams',0,0,'C');
    $pdf->Cell(25,6,'Price',0,0,'C');
    $pdf->Cell(30,6,'Total',0,1,'R'); 
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
/*
    while($row = mysqli_fetch_assoc($result))  {
         
        //  *$row['steams']   campo con algunos registros en null   *****   ARREGLAR  *******
         
         $pdf->Cell(70,4,$row['cate'],0,0,'L');            
         $pdf->Cell(25,6,$row['subcate'],0,0,'L');                              
         $pdf->Cell(25,6,$row['steams'],0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.12',0,0,'C');                                               
         $pdf->Cell(30,6,'$'.number_format($row['total'], 2, '.', ','),0,1,'R');           
                 
         $totalCal = $totalCal + $row['total'];
         $totalStem = $totalStem + $row['steams'];
    }

*/
    ////////////   Inicio
         // 1
         $pdf->Cell(70,4,'Flowers and Fillers',0,0,'L');            
         $pdf->Cell(25,6,'Gypsophila',0,0,'L');                              
         $pdf->Cell(25,6,'2445',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.06',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 146.70',0,1,'R');    
         // 2
         $pdf->Cell(70,4,'Limonium',0,0,'L');            
         $pdf->Cell(25,6,'Hybrid Limonium',0,0,'L');                              
         $pdf->Cell(25,6,'300',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.05',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 15.00',0,1,'R');               
         // 3
         $pdf->Cell(70,4,'Lilies',0,0,'L');            
         $pdf->Cell(25,6,'Lilies',0,0,'L');                              
         $pdf->Cell(25,6,'790',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.09',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 71.10',0,1,'R');                                      
         // 4
         $pdf->Cell(70,4,'Flowers and Fillers',0,0,'L');            
         $pdf->Cell(25,6,'Hydrageas',0,0,'L');                              
         $pdf->Cell(25,6,'20',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.05',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 1.00',0,1,'R');    
         // 5
         $pdf->Cell(70,4,'Flowers and Fillers',0,0,'L');            
         $pdf->Cell(25,6,'Sunflowers',0,0,'L');                              
         $pdf->Cell(25,6,'170',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.10',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 17.00',0,1,'R');               
         // 6
         $pdf->Cell(70,4,'Flowers and Fillers',0,0,'L');            
         $pdf->Cell(25,6,'Stock',0,0,'L');                              
         $pdf->Cell(25,6,'100',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.06',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 6.00',0,1,'R');   
         
         // 7
         $pdf->Cell(70,4,'Asters',0,0,'L');            
         $pdf->Cell(25,6,'Asters',0,0,'L');                              
         $pdf->Cell(25,6,'200',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.06',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 12.00',0,1,'R');    
         // 8
         $pdf->Cell(70,4,'Snapdragon',0,0,'L');            
         $pdf->Cell(25,6,'Snapdragon',0,0,'L');                              
         $pdf->Cell(25,6,'120',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.05',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 6.00',0,1,'R');               
         // 9
         $pdf->Cell(70,4,'Flowers and Fillers',0,0,'L');            
         $pdf->Cell(25,6,'Hypericum',0,0,'L');                              
         $pdf->Cell(25,6,'160',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.06',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 9.60',0,1,'R');                                      
         // 10
         $pdf->Cell(70,4,'Flowers and Fillers',0,0,'L');            
         $pdf->Cell(25,6,'Delphinium',0,0,'L');                              
         $pdf->Cell(25,6,'50',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.07',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 3.50',0,1,'R');    
         // 11
         $pdf->Cell(70,4,'Agapanthus',0,0,'L');            
         $pdf->Cell(25,6,'Agapanthus',0,0,'L');                              
         $pdf->Cell(25,6,'25',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.07',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 1.75',0,1,'R');               
         // 12
         $pdf->Cell(70,4,'Solidago',0,0,'L');            
         $pdf->Cell(25,6,'Solidago',0,0,'L');                              
         $pdf->Cell(25,6,'500',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.05',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 25.00',0,1,'R');    
         
         // 13
         $pdf->Cell(70,4,'Roses',0,0,'L');            
         $pdf->Cell(25,6,'Standard Rose',0,0,'L');                              
         $pdf->Cell(25,6,'11300',0,0,'C');                              
         $pdf->Cell(25,6,'$ 0.16',0,0,'C');                                               
         $pdf->Cell(30,6,'$ 1808.00',0,1,'R');                                                        
    
    ////////////  Fin
         
         
    $pdf->Ln(2);
    $pdf->SetFont('Arial','B',15);
    $pdf->Cell(70,10,'Contact Details ',0,1,'L');
    $pdf->SetFont('Arial','B',10);  
    
    $pdf->Cell(70,6,'Total Stems: 16180'.$totalStem,0,0,'L');
    $pdf->Cell(0,6,'Sub - Total Amount: 2122.65'.$totalCal,0,1,'R');
    
    $pdf->Cell(70,6,'Av. Interoceanica OE6-73 y Gonzalez Suarez',0,1,'L'); 
    $pdf->Cell(70,6,'Quito, Ecuador',0,1,'L');   
    $pdf->Cell(70,6,'Phone: +593 602 2630',0,1,'L');    
    $pdf->Cell(70,6,'Email:info@freshlifefloral.com',0,1,'L'); 
  

    
  $pdf->Output();
  ?>