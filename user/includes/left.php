<script type="text/javascript" src="../assets/js/sliding_effect.js"></script><?php
// PO # 1

if ($_SESSION['uid'] != 1) {
    $sel_rights = "SELECT rights from admin where id='" . $_SESSION['uid'] . "'";
    $rs_rights = mysqli_query($con, $sel_rights);
    $rights = mysqli_fetch_array($rs_rights);
    $p = $rights["rights"];
} else {
    $p = "All";
}
?>
<td width="210" valign="top">
    <table width="" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><img src="images/images_front/admin-menu-top3.png" border="0"/></td>
        </tr>
        <tr>
            <td background="images/images_front/admin-menu-middle.gif" style="background-repeat:repeat-y;" valign="top">
                <table id="sliding-navigation" align="left" class="menutable">
                    <tr>
                        <td height="10" class="sliding-element"></td>
                    </tr><?php
                    $pos1 = strchr($p, "Manage CMS Pages");
                    $pos2 = strchr($p, "Manage Slideshow");
                    $pos3 = strchr($p, "Manage Home Boxes");
                    $pos4 = strchr($p, "Manage Testimonials");

                    $pos1 = strchr($p, "Manage Product Categories");
                    $pos2 = strchr($p, "Manage Sub Categories");
                    $pos3 = strchr($p, "Manage Colors");
                    $pos4 = strchr($p, "Manage Sizes");
                    $pos5 = strchr($p, "Manage Quality Grade");
                    $pos6 = strchr($p, "Manage Products");
                    $pos7 = strchr($p, "Manage Boxes");
                    $pos8 = strchr($p, "Manage Bunch Sizes");
                    $pos16 = strchr($p, "Manage Box Types");
                    $pos9 = strchr($p, "Manage Grower Boxes");
                    $pos10 = strchr($p, "Manage Coutries");
                    $pos11 = strchr($p, "Manage Farms List");
                    $pos12 = strchr($p, "Manage Special Features");
                    $pos13 = strchr($p, "Product Special Features");
                    $pos14 = strchr($p, "Manage Certificates");
                    $pos15 = strchr($p, "Grower Certificates");

                    if ($p == 'All' || $pos1 != false || $pos2 != false || $pos3 != false || $pos4 != false || $pos5 != false || $pos6 != false || $pos7 != false || $pos8 != false || $pos9 != false || $pos10 != false || $pos11 != false || $pos12 != false || $pos13 != false || $pos14 != false || $pos15 != false || $pos16 != false) {
                        ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="product_home.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Product Info. </a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }

                    $pos = strchr($p, "Manage Growers");
                    if ($p == 'All' || $pos != false) {
                        ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="growers_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Growers</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr><?php
                    }

                    $pos = strchr($p, "Manage Users");
                    if ($p == 'All' || $pos != false) {
                        ?>
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="users_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Users</a></li>
                            </td>
                        </tr>
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr>                        
                        <tr>
                            <td>
                                <li class="sliding-element" id="menulink"><a href="assign_posts.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Assign Posts</a></li>
                            </td>
                        </tr>
                        
                        <tr>
                            <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                        </tr>
                            <?php
                    }

                    $pos = strchr($p, "Agents");
                    if ($p == 'All' || $pos != false) {
                        ?>
                        <tr>
                        <?php
                        $pos = strchr($p, "Manage Agents");
                        if ($p == 'All' || $pos != false) {
                            ?>
                            <tr>
                                <td>
                                    <li class="sliding-element" id="menulink"><a href="agent_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Agents</a></li>
                                </td>

                            </tr>
                            <tr>
                                <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                            </tr><?php
                        }

                        $pos = strchr($p, "Manage Buyers");
                        if ($p == 'All' || $pos != false) {
                            ?>
                            <tr>
                                <td>
                                    <li class="sliding-element" id="menulink"><a href="buyer_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Buyers</a></li>
                                </td>
                            </tr>
                            <tr>
                                <td height="1" class="sliding-element"><img
                                            src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                            </tr><?php
                        }


                        $pos = strchr($p, "Manage Shipping Info.");
                        if ($p == 'All' || $pos != false) {
                            ?>
                            <tr>
                                <td>
                                    <li class="sliding-element" id="menulink"><a href="shipping_info_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Shipping Info.</a></li>
                                </td>
                            </tr>
                            <tr>
                                <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                            </tr><?php
                        }

                        $pos = strchr($p, "Analytics Feature.");
                        if ($p == 'All' || $pos != false) {
                            ?>
                            <tr>
                                <td>
                                    <li class="sliding-element" id="menulink"><a href="farms_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Manage Farms </a></li>
                                </td>
                            </tr>
                            <tr>
                                <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                            </tr><?php
                        }


                        $pos = strchr($p, "Tax Tables");
                        if ($p == 'All' || $pos != false) {
                            ?>
                            <tr>
                                <td>
                                    <li class="sliding-element" id="menulink"><a href="tax_tables.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Tax Tables</a></li>
                                </td>
                            </tr>
                            <tr>
                                <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                            </tr><?php
                        }
                        
                        
                        $pos = strchr($p, "Session");
                        if ($p == 'All' || $pos != false) {
                            ?>
                            <tr>
                                <td>
                                    <li class="sliding-element" id="menulink"><a href="session_gen.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; Change Session</a></li>
                                </td>
                            </tr>
                            <tr>
                                <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                            </tr><?php
                        }                        

                        
                        $pos = strchr($p, "Reports");
                        if ($p == 'All' || $pos != false) {
                            ?>
                            <tr>
                                <td>
                                    <li class="sliding-element" id="menulink"><a href="reports_gen.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; General Reports</a></li>
                                </td>
                            </tr>
                            <tr>
                                <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                            </tr><?php
                        }                        
                        
                        $pos = strchr($p, "Parameters");
                        if ($p == 'All' || $pos != false) {
                            ?>
                            <tr>
                                <td>
                                    <li class="sliding-element" id="menulink"><a href="parameters_mgmt.php" class="greylink11"><img src="images/images_front/admin-menu-arrow.gif" border="0" align="absmiddle"/>&nbsp;&nbsp; General Parameters</a></li>
                                </td>
                            </tr>
                            <tr>
                                <td height="1" class="sliding-element"><img src="images/images_front/admin-menu-underline.gif" border="0"/></td>
                            </tr><?php
                        }                                                


                        if ($p == 'All') {
                            ?><?php
                        }
                    }
                    ?></table>
            </td>
        </tr>
        <tr>
            <td height="10">
                <img src="images/images_front/admin-menu-bottom.gif" border="0"/>
            </td>
        </tr>
    </table>
</td>
