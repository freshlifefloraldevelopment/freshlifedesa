<?php

include "../config/config_new.php";
error_reporting(1);
session_start();
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}
$sel_info = "select * from growers where id='" . $_GET["id"] . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

$str = trim($info['products'], ",");

$select_product_info = "select name from product where id='" . $_GET["pid"] . "'";
$rs_product_info = mysqli_query($con, $select_product_info);
$product_info = mysqli_fetch_array($rs_product_info);

if (isset($_POST["Submit"])) {

    $delete = "delete from grower_product_box_packing where growerid='" . $info["id"] . "' and prodcutid='" . $_POST["pro_id"] . "'";
    mysqli_query($con,$delete);

    $prodcutid   = $_POST["prodcutid"];
    $feature     = $_POST["feature"];
    $sizeid      = $_POST["sizeid"];
    $boxid       = $_POST["boxid"];
    $bunchsizeid = $_POST["bunchsizeid"];

    for ($i = 0; $i <= $_POST["total"] ; $i++) {

        if ($_POST[$i . "-qty"] != "") {
                $insert = "insert into grower_product_box_packing set 
						qty           ='" . $_POST[$i . "-qty"] . "',
						growerid      ='" . $info["id"] . "',
						prodcutid     ='" . $prodcutid. "',
						box_id        ='" . $boxid[$i] . "',
						bunch_size_id ='" . $bunchsizeid[$i] . "',
						feature       ='" . $feature[$i]. "',
						sizeid        ='" . $sizeid[$i] . "'";
            mysqli_query($con, $insert);
            echo  $insert+"<br>";
        }
    }


    $delete = "delete from grower_product_box_weight where growerid='" . $info["id"] . "' and prodcutid='" . $_POST["pro_id"] . "'";
    mysqli_query($con, $delete);

    for ($i = 0; $i <= $_POST["total"] ; $i++) {
        if ($_POST[$i . "-weight"] != "") {
            $insert = "insert into grower_product_box_weight set 
						weight='" . $_POST[$i . "-weight"] . "',
						growerid='" . $info["id"] . "',
						prodcutid='" . $prodcutid . "',
						box_id='" . $boxid[$i] . "',
						bunch_size_id='" . $bunchsizeid[$i] . "',
						feature='" .  $feature[$i] . "',
						bunch_qty='" . $_POST[$i . "-qty"] . "',
						sizeid='" . $sizeid[$i] . "'";
            mysqli_query($con, $insert);
        }

    }
}
if ($str != "") {

    $qsel = "select b.name as boxname,gpf.features as fid,ff.name as featurename,b.id as boxid,bs.id as bunchsizeid,b.width,b.length,
    b.height,bs.name as bunchname, p.id as pid,p.name as productname,p.color_id as colorid,p.categoryid as productcategory,
    p.subcategoryid as productsubcategoryid,gs.sizes as sizeid,pc.name productcateogryname,sh.name as sizename,c.name as colorname,gq.quality as qualitygradeid,s.name as productsubcategoryname,q.name as qualitygradename,gs.* from  grower_product_bunch_sizes gs 
	left join grower_product_features gpf on gs.product_id=gpf.product_id
	left join features ff on gpf.features=ff.id
	right join grower_product_box gpb on gs.product_id=gpb.product_id and gs.grower_id=gpb.grower_id
	left join product p on gs.product_id=p.id 
	left join category pc on p.categoryid=pc.id 
	left join subcategory s on p.subcategoryid=s.id  
	left join colors c on p.color_id=c.id 
	left join boxes b on gpb.boxes=b.id
	left join bunch_sizes bs on gs.bunch_sizes=bs.id
	left join sizes sh on gs.sizes=sh.id 
	left join grower_product_quality gq on p.id=gq.product_id 
	left join quality q on gq.quality=q.id 
	where gs.grower_id='" . $_GET["id"] . "' and gs.product_id='" . $_GET["pid"] . "' group by gs.product_id,gs.sizes,gs.grower_id,
	gs.bunch_sizes,gpb.boxes,gpf.features order by p.name,gs.sizes,b.id,gpf.features,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER)  ";
    $rs = mysqli_query($con, $qsel);


}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Admin Area</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link href="css/demo_page.css" rel="stylesheet" type="text/css"/>
    <link href="css/demo_table_jui.css" rel="stylesheet" type="text/css"/>
    <link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf-8">
        function verify() {
            k = 0;


            for (i = 0; i <= $('#total').val(); i++) {

                if ($('#' + i + '-qty').val() == "" && $('#' + i + '-weight').val() == "") {
                }
                else {
                    if ($('#' + i + '-qty').val() > 0 && $('#' + i + '-weight').val() == "") {
                        $('#' + i + '-weightr').show();
                        k = 1;
                    }
                    else if ($('#' + i + '-weight').val() > 0 && $('#' + i + '-qty').val() == "") {
                        $('#' + i + '-qtyr').show();
                        k = 1;
                    }
                    else {
                        $('#' + i + '-qtyr').hide();
                        $('#' + i + '-weightr').hide();
                    }
                }
            }

            if (k == 0) {
                return true;
            }
            else {
                return false;
            }

        }
        $.fn.dataTableExt.oApi.fnFilterClear = function (oSettings) {
            /* Remove global filter */
            oSettings.oPreviousSearch.sSearch = "";

            /* Remove the text of the global filter in the input boxes */
            if (typeof oSettings.aanFeatures.f != 'undefined') {
                var n = oSettings.aanFeatures.f;
                for (var i = 0, iLen = n.length; i < iLen; i++) {
                    $('input', n[i]).val('');
                }
            }

            /* Remove the search text for the column filters - NOTE - if you have input boxes for these
             * filters, these will need to be reset
             */
            for (var i = 0, iLen = oSettings.aoPreSearchCols.length; i < iLen; i++) {
                oSettings.aoPreSearchCols[i].sSearch = "";
            }
            /* Redraw */
            oSettings.oApi._fnReDraw(oSettings);
        };
        var oTable;
        $(document).ready(function () {
            oTable = $('#example').dataTable({
                //"sScrollXInner": "130%",
                "bJQueryUI": true,
                //"sScrollY": "536",
                "sPaginationType": "full_numbers"

            });

            $('#form1').submit(function () {
                var oSettings = oTable.fnSettings();
                oTable.fnFilterClear();
                oSettings._iDisplayLength = -1;
                oSettings._bFilter = false;
                oTable.fnDraw();
            });


        });
    </script>
</head>
<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
    <?php include("includes/header_inner.php"); ?>
    <tr>
        <td height="5"></td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <?php include("includes/grower-left.php"); ?>
                    <td width="5">&nbsp;</td>
                    <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80"/></td>
                                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="10">&nbsp;</td>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td height="5"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="pagetitle">Manage Box Packing for <?php echo $product_info["name"] ?> (<?php echo $info["growers_name"] ?>)</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:10px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td>
                                                                        <a class="pagetitle1" href="box_packing_mgmt.php?id=<?php echo $_GET["id"] ?>" onclick="this.blur();"><span> Manage Box Packing </span></a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:10px;"></td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <form action="" id="form1" method="post" >
                                                                <div id="box">
                                                                    <div id="container">
                                                                        <div class="demo_jui">
                                                                            <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                                                                <thead>
                                                                                <tr>
                                                                                    <th width="20%" align="left">Box</th>
                                                                                    <th width="20%" align="left">Product</th>
                                                                                    <th width="10%" align="left"> Sub Category</th>
                                                                                    <th width="8%" align="left">Size</th>
                                                                                    <th width="10%" align="left"> Bunch Sizes</th>
                                                                                    <th width="10%" align="left"> Bunches</th>
                                                                                    <th width="10%" align="left"> Weight(Kg.)</th>
                                                                                </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <?php
                                                                                $sr = 0;

                                                                                if ($str != "") {
                                                                                    while ($product = mysqli_fetch_array($rs)) {
                                                                                        ?>
                                                                                        <tr class="gradeU">
                                                                                            <input type="hidden" name="prodcutid" value="<?php echo $product["pid"] ?>"/>
                                                                                            <td class="text" align="left">
                                                                                                <?php echo $product["boxname"] ?> (<?php echo $product["width"] ?>*<?php echo $product["length"] ?>*<?php echo $product["height"] ?>)
                                                                                            </td>
                                                                                            <td class="text" align="left">
                                                                                                <?php echo $product["productname"] ?><?php if ($product["featurename"] != "") { ?> <br/> (<?php echo $product["featurename"] ?>) <?php } ?>
                                                                                            </td>

                                                                                            <td class="text" align="left"><?php echo $product["productsubcategoryname"] ?></td>
                                                                                            <td class="text" align="left"><?php echo $product["sizename"] . "cm" ?></td>


                                                                                            <td class="text" align="center">
                                                                                                <?php
                                                                                                $bunch_val = "";
                                                                                                if ($product["is_bunch"] == "1") {
                                                                                                    $bunch_val = $product["is_bunch_value"];
                                                                                                    echo $bunch_val . " Bunch";
                                                                                                } else {
                                                                                                    $bunch_val = $product["bunchname"];
                                                                                                    echo $bunch_val . " Stems";
                                                                                                }

                                                                                                ?>

                                                                                                <!-- name="prodcutid<?php echo $sr ?>" -->
                                                                                                <!--<input type="hidden" name="prodcutid[]" value="<?php echo $product["pid"] ?>"/>-->
                                                                                                <!--<input type="hidden" name="prodcutname[]" value="<?php echo $product["productname"] ?>"/>-->
                                                                                                <input type="hidden" name="sizeid[]" value="<?php echo $product["sizeid"] ?>"/>
                                                                                                <!--<input type="hidden" name="sizename<?php echo $sr ?>" value="<?php echo $product["sizename"] ?>"/>-->
                                                                                                <!--<input type="hidden" name="categoryid<?php echo $sr ?>" value="<?php echo $product["productcategory"] ?>"/>-->
                                                                                                <!--<input type="hidden" name="subcategoryid<?php echo $sr ?>" value="<?php echo $product["sizename"] ?>"/>-->
                                                                                                <input type="hidden" name="boxid[]" value="<?php echo $product["boxid"] ?>"/>
                                                                                                <input type="hidden" name="bunchsizeid[]" value="<?php echo $product["bunchsizeid"] ?>"/>
                                                                                                <input type="hidden" name="feature[]" value="<?php echo $product["fid"] ?>"/>
                                                                                                <!--<input type="hidden" name="qualityid<?php echo $sr ?>" value="<?php echo $product["qualitygradeid"] ?>"/>-->
                                                                                                <!--<input type="hidden" name="qualityname<?php echo $sr ?>" value="<?php echo $product["qualitygradename"] ?>"/>-->
                                                                                                <!--<input type="hidden" name="colorid<?php echo $sr ?>" value="<?php echo $product["colorid"] ?>"/>-->
                                                                                                <!--<input type="hidden" name="colorname<?php echo $sr ?>" value="<?php echo $product["colorname"] ?>"/>-->
                                                                                            </td>
                                                                                            <?php

                                                                                            $sel_last_price = "select qty from grower_product_box_packing where growerid='" . $_GET["id"] . "' and prodcutid='" . $product["pid"] . "' and 
								                                                            sizeid='" . $product["sizeid"] . "' and box_id='" . $product["boxid"] . "' and feature='" . $product["fid"] . "' and bunch_size_id='" . $product["bunchsizeid"] . "' order by id desc limit 0,1";
                                                                                            $rs_last_price = mysqli_query($con, $sel_last_price);
                                                                                            $last_price = mysqli_fetch_array($rs_last_price);

                                                                                            $sel_last_price2 = "select weight from grower_product_box_weight where growerid='" . $_GET["id"] . "' and prodcutid='" . $product["pid"] . "' and 
								                                                            sizeid='" . $product["sizeid"] . "' and box_id='" . $product["boxid"] . "' and feature='" . $product["fid"] . "' and bunch_size_id='" . $product["bunchsizeid"] . "' order by id desc limit 0,1";
                                                                                            $rs_last_price2 = mysqli_query($con, $sel_last_price2);
                                                                                            $last_price2 = mysqli_fetch_array($rs_last_price2);

                                                                                            ?>

                                                                                            <td><input type="text" name="<?php echo $sr ?>-qty" id="<?php echo $sr ?>-qty" value="<?php echo $last_price["qty"] ?>" size="10" style="width:70px;"/>
                                                                                                <br/> <span style="color:#F00; font-size:12px; display:none;" id="<?php echo $sr ?>-qtyr"> Required</span></td>
                                                                                            <td><input type="text" name="<?php echo $sr ?>-weight" id="<?php echo $sr ?>-weight" value="<?php echo $last_price2["weight"] ?>" size="10" style="width:70px;"/>
                                                                                                <br/> <span style="color:#F00; font-size:12px; display:none;" id="<?php echo $sr ?>-weightr"> Required</span></td>
                                                                                        </tr>
                                                                                        <?php

                                                                                        $sr++;

                                                                                    }

                                                                                }

                                                                                ?>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div style="float:right; margin-top:10px;">
                                                                        <input type="hidden" name="total" id="total" value="<?php echo $sr ?>"/>
                                                                        <input type="hidden" name="pro_id" value="<?php echo $_GET["pid"] ?>"/>
                                                                        <input name="Submit" type="submit" class="buttongrey" value="Update Packing"/>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="10">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80"/></td>
                            </tr>
                            <tr>
                                <td background="images/middle-leftline.gif"></td>
                                <td>&nbsp;</td>
                                <td background="images/middle-rightline.gif"></td>
                            </tr>
                            <tr>
                                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10"/></td>
                                <td background="images/middle-bottomline.gif"></td>
                                <td><img src="images/middle-bottomright.gif" width="10" height="10"/></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="10"></td>
    </tr>
    <?php include("includes/footer-inner.php"); ?>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>
</body>
</html>
