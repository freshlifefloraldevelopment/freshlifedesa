<?php

// PO 2018-09-21 

require_once("../config/config_gcp.php");
session_start();

$fact_number  = $_GET['id_fact'];

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
	header("location: index.php");
}

if($_SESSION['grower_id']!=0){
	header("location: growers.php?id=".$_SESSION['grower_id']);
}  

	if(isset($_GET['delete'])) {
	  $querydel = 'DELETE FROM buyer_requests WHERE  id= '.(int)$_GET['delete'];
	  mysqli_query($con,$querydel);
	}
			

	$qsel="select p.name as prod_name, unseen ,br.id,bo.qucik_desc,b.first_name , b.last_name,  
                      CAST(br.id_order AS UNSIGNED) idorder,
                      br.cod_order , br.qty ,  br.lfd , br.type,
                      p.categoryid, p.subcategoryid,sc.name as subcatego,
                      br.feature , f.name as name_feature,bo.id as idbo
                 from buyer_requests br
                INNER JOIN product p    ON br.product = p.id
                INNER JOIN buyers b     ON br.buyer = b.id
                INNER JOIN buyer_orders bo ON br.id_order = bo.id
                INNER JOIN subcategory sc ON p.subcategoryid = sc.id and p.categoryid = sc.cat_id               
                 left JOIN features f     ON br.feature = f.id                
                 where id_order > 770
               order by br.id_order desc ";	

	 $rs=mysqli_query($con,$qsel);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
			} );
</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
 <?php include("includes/header_inner.php");?>

  <tr>
    <td height="5"></td>
  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/agent-left.php");?>

        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td height="5"></td>
                   </tr>

                  <tr>
                    <td class="pagetitle">Request Global</td>
                  </tr>
                 
                  <tr>
                    <td>&nbsp;</td>
                  </tr>

                  <tr>

                <td><div id="box">

		<div id="container">			
                    <div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>       
                    <th align="left" width="5%" >#</th>                                          
                    <th align="left" width="8%" >Id</th>                      
                    <th align="left" width="20%" >Buyer</th>                      
                    <th align="left" width="22%" >Variety</th>  
                    <th align="left" width="18%">Cod order </th>                    
                    <th align="left" width="8%">Qty</th>                    
                    <th align="left" width="14%">Lfd</th>                                        
                    <th align="left" width="16%">Market</th>                                                            
		</tr>

</thead>

	<tbody>
		<?php

		  	$sr=1;
        		  while($product=mysqli_fetch_array($rs))  {
                              if ($product["type"] == "0") {
                                    $market = "Standing Order";
                              }else{
                                    $market = "Open Market";
                              }
		?>
                          <tr class="gradeU"> 
                                <td class="text" align="left"><?php echo $sr ?>  </td>                                                                                                                                                                                                            
                                <td class="text" align="left"><?php echo $product["idbo"]?>  </td>                                                                                                                                                                              
                                <td class="text" align="left"><?php echo $product["first_name"]." ".$product["last_name"]?>  </td>                                                                                                                                                
                                <td class="text" align="left"><?php echo $product["prod_name"]." ".$product["subcatego"]." ".$product["name_feature"]?></td>                                                                                  
                                <td class="text" align="left"><?php echo $product["cod_order"]?>  </td>                                                                                                                  
                                <td class="text" align="left"><?php echo $product["qty"]?> </td>                                                           
                                <td class="text" align="left"><?php echo $product["lfd"]?> </td>  
                                <td class="text" align="left"><?php echo $market?> </td> 
                                
                                
                                <td class="text" align="center"><a href="grower_offer_all.php?id_offer=<?php echo $product["id"]?>" >Offers</a> </td>	                                
                                <!--td align="center" ><a href="buyer_requests_edit.php?idor=<?php echo $product["id"]."&id_fact=".$fact_number ?> "><img src="images/edit.gif" border="0" alt="Edit" /></a></td>                                
                                <td align="center" ><a href="?delete=<?php  echo $product["id"]?>"  onclick="return confirm('You want to delete this REQUEST? CHECK if you have offers.');"><img src="images/delete.gif" border="0" alt="Delete" /></a></td-->                                

                          </tr>
				 <?php 
			     $sr++;
			   } ?> 	
	</tbody>
</table>
                                </div>
                            </div>
			</div>
		</td>
                </tr>
                </table>
                </td>

                <td width="10">&nbsp;</td>

                    </tr>
                </table>                    
            </td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>

          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>

          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table>
        </td>
      </tr>

    </table>
    </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>