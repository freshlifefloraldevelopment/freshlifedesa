<?php

// PO 2018-09-21 Cambio Eduardo

require_once("../config/config_gcp.php");
session_start();

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
            header("location: index.php");
}

if($_SESSION['grower_id']!=0){
            header("location: growers.php?id=".$_SESSION['grower_id']);
}  
	
if(isset($_GET['delete'])) {
	  $query = 'DELETE FROM grower_offer_reply  WHERE  id= '.(int)$_GET['delete'];
	  mysqli_query($con,$query);
}
	 $qsel="select b.first_name,b.last_name, gr.*,g.growers_name,br.lfd,br.type,br.id as buyer_request_id 
                  from grower_offer_reply gr 
                  LEFT JOIN buyer_requests br ON gr.offer_id = br.id
	          LEFT JOIN buyers b ON gr.buyer_id = b.id
	          LEFT JOIN growers g ON gr.grower_id = g.id
	         where gr.buyer_id ='".$_GET["b"]."'
	           and br.lfd      ='".$_GET["lfd"]."' 
                   and gr.status   = 1 
                   AND gr.invoice  = 0 
                   and gr.shipping_method='".$_GET["s"]."'";
	 $rs=mysqli_query($con,$qsel);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<link href="css/demo_page.css" rel="stylesheet" type="text/css" />

<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />

<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					//"sScrollXInner": "130%",

					"bJQueryUI": true,

					//"sScrollY": "536",

					"sPaginationType": "full_numbers"

				});

			} );

</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

 <?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>

                    <td height="5"></td>

                  </tr>

                  <tr>

                    <td class="pagetitle">Create Invoice</td>

                  </tr>

                 
                  <tr>

                    <td>&nbsp;</td>

                  </tr>

                  <tr>

                    <td><div id="box">

		<div id="container">			

			<div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>
        
			<th width="10%" align="left">Customer</th> 
            
            <th width="22%" align="left">Offer</th> 
            
            <th align="center" width="9%">Grower</th>

			<th align="center" width="9%">Delete</th>

		</tr>

	</thead>

	<tbody>

		<?php

						  	$sr=1;
							$ftotal=0;
							$ids="";
							$buyer_request_id="";
						  while($product=mysqli_fetch_array($rs))
						  {
						  		$buyer_request_id=$product["buyer_request_id"];
                                  $ids.=$product["offer_id"]."-".$product["id"].",";
						     
                                 if($product["product"]=="")
								 {
								    $product["product"]="Assorted";
								 }
						  ?>

                        <tr class="gradeU">

                          

                          <td class="text" align="left"><?php echo $product["first_name"]?> <?php echo $product["last_name"]?> </td>
                          
                        
                           <td class="text" align="left"><?php echo $product["product"]?> <?php echo $product["size"]?> @  <?php echo $product["atprice_box"]?> , <?php echo $product["atqty_box"]?> <?php echo $product["boxtype"]?>
                           
                        <?php  
						
					   $sel_boxes="select * from grower_reply_box where offer_id='".$product["offer_id"]."' and reply_id=".$product["id"];
					   $rs_boxes=mysqli_query($con,$sel_boxes);
					   $total_boxes=mysqli_num_rows($rs_boxes);
					   if($total_boxes>=1)
					   {
					      echo'<br/><br/>Bunches : <br/><br/><span style="color:#004000;font-weight:bold;">';
					   }
					   while($boxes=mysqli_fetch_array($rs_boxes))
					   {
					   
					      echo $boxes["product"]." ".$boxes["bunchsize"]." * ".$boxes["bunchqty"]." @ ".$boxes["finalprice"]." <br/>";
					   
                       }
					   
					   if($total_boxes>=1)
					   {
					      echo'</span>';
					   }
					   
					   ?>
                           
                            <br/> <br/> <b> LFD :- 
						   
                           <?php
						    $temp=explode("-",$product["lfd"]); 
							?>
						   <?php echo $temp[1]."-".$temp[2]."-".$temp[0]?> 
                           
                           <br/> Buying Method :- 
                           
                            <?php if($product["type"]=="0") { echo "RQ";  } ?>
                            <?php if($product["type"]=="1") { echo "LI";  } ?>
                            <?php if($product["type"]=="2") { echo "NP";  } ?>
                            <?php if($product["type"]=="3") { echo "BB";  } ?>
                           <br /> Total Stems :-  <?php echo $product["totalstems_box"]?> <br /><br />
                           Total :- $<?php echo round($product["totalstems_box"]*$product["atprice_box"],2);?>
                            </b>
                           </td>
                           
                            <td class="text" align="left"><?php echo $product["growers_name"]?></td>
                          
						
                          <td align="center" ><a href="?delete=<?php echo $product["id"]?>&b=<?php echo $_GET["b"]?>&lfd=<?php echo $_GET["lfd"]?>&s=<?php echo $_GET["s"]?>"  onclick="return confirm('Are you sure, you want to delete this offer ?');"><img src="images/delete.gif" border="0" alt="Delete" /></a></td>
                          

                          </tr>

						 <?php

						 		$sr++;

						 	}

						 ?> 

	

	</tbody>

</table>



			</div>

			</div>



			</div>
            
            <div style="margin-top:20px;">
                <form method="GET" action="invoice_make.php">             	
                    <input type="hidden" name="buyer_request_id" id="buyer_request_id" value="<?php echo $buyer_request_id?>"  />	
                    <input type="hidden" name="ids" id="ids" value="<?php echo $ids?>"  />
                    <input type="hidden" name="b" id="b" value="<?php echo $_GET["b"]?>"  />
                    <input type="hidden" name="lfd" id="lfd" value="<?php echo $_GET["lfd"]?>"  />
                    <input type="submit" value="Generate Invoice"  /> 
                </form>
            </div>

		</td>

                  </tr>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>

</body>

</html>
