<?php
    // PO #1  2-jul-2018
include "../config/config_gcp.php";


if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}

if (isset($_POST["Submit"]) && $_POST["Submit"] == "Add") {
    $name = $_REQUEST['name'];
    $tax = "INSERT INTO tax_table(name) value ('$name')";
    mysqli_query($con, $tax);
    $last_id = mysqli_insert_id($con);

    //$ins = "insert into tax_table set name='" . trim($_POST["name"]) . "',cat_id='" . trim($_POST["cat_id"]) . "',tax_rate_percent='" . trim($_POST["tax_rate_percent"]) . "'";
    $qry = "select * from category order by id";
    $qryRes = mysqli_query($con, $qry);
    $count = mysqli_num_rows($qryRes);
    for ($i = 1; $i <= $count; $i++) {
        $id = $_REQUEST['cat_id' . $i];
        $rate = $_REQUEST['tax_rate_percent' . $i];
        $ins = "INSERT INTO tax_cat_det(tax_table_id, cat_id, tax_rate_percent)
		VALUES('$last_id','$id','$rate')";
        mysqli_query($con, $ins);
    }

    header('location:tax_tables.php');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="js/select2/select2.min.js"></script>
        <link href="js/select2/select2.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            $(document).ready(function () {
                $('select').select2();
            });


            function verify()
            {
                var arrTmp = new Array();
                //arrTmp[0] = checkcname();
                arrTmp[0] = checkcat_desc();
                arrTmp[1] = checkcat_con();
                var i;
                _blk = true;
                for (i = 0; i < arrTmp.length; i++)
                {
                    if (arrTmp[i] == false)
                    {
                        _blk = false;
                    }
                }
                if (_blk == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            function trim(str)
            {
                if (str != null)
                {
                    var i;
                    for (i = 0; i < str.length; i++)
                    {
                        if (str.charAt(i) != " ")
                        {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }
                    for (i = str.length - 1; i >= 0; i--)
                    {
                        if (str.charAt(i) != " ")
                        {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }
                    if (str.charAt(0) == " ")
                    {
                        return "";
                    }
                    else
                    {
                        return str;
                    }
                }
            }

            function checkcname()
            {
                if (trim(document.frmcat.cname.value) == "")
                {
                    document.getElementById("lblcname").innerHTML = "Please enter shipping method name";
                    return false;
                }
                else
                {
                    document.getElementById("lblcname").innerHTML = "";
                    return true;
                }
            }

            function checkcat_desc()
            {
                if (trim(document.frmcat.cat_desc.value) == "")
                {
                    document.getElementById("lblcat_desc").innerHTML = "Please enter shipping description";
                    return false;
                }
                else
                {
                    document.getElementById("lblcat_desc").innerHTML = "";
                    return true;
                }
            }

            function checkcat_con()
            {
                if (trim(document.frmcat.Connection_1.value) == "")
                {
                    document.getElementById("lblcat_connections_1").innerHTML = "Please select atleast one connection";
                    return false;
                }
                else
                {
                    document.getElementById("lblcat_connections_1").innerHTML = "";
                    return true;
                }
            }

            function checkimage()
            {
                if (trim(document.frmcat.image.value) == "")
                {
                    document.getElementById("lblimage").innerHTML = "Please upload image";
                    return false;

                }
                else
                {
                    if (!validImageFile(document.frmcat.image.value))
                    {
                        document.getElementById("lblimage").innerHTML = "Please select valid image file";
                        return false;
                    }
                    else
                    {
                        document.getElementById("lblimage").innerHTML = "";
                        return true;
                    }
                }

            }

        </script>
    </head>
    <body>
        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
            <?php include("includes/header_inner.php"); ?>
            <tr>
                <td height="5"></td>
            </tr>
            <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <?php include("includes/left.php"); ?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td height="5"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pagetitle">Add New Tax Table</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><table width="100%">
                                                                            <tr>
                                                                                <td><a class="pagetitle1" href="tax_tables.php" onclick="this.blur();"><span> Manage Tax tables</span></a></td>
                                                                            </tr>
                                                                        </table></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                </tr>

                                                                <tr>
                                                                    <td><div id="box">
                                                                            <input type="hidden" id="count" name="count" value="1" />
                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                                                                <tr>
                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Name </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="name" id="cname" value="" />
                                                                                        <br>
                                                                                            <span class="error" id="lblcname"></span></td>
                                                                                </tr>
                                                                                <tr>

                                                                                    <td width="66%" bgcolor="#f2f2f2" colspan="2">
                                                                                        <table name="cat_id1" style="width: 659px;">
                                                                                            <tr>
                                                                                                <td>Category</td>
                                                                                                <td>Tax rate</td>
                                                                                            </tr>
                                                                                            <?php
                                                                                            $qry = "select * from category order by id";
                                                                                            $qryRes = mysqli_query($con, $qry);
                                                                                            $nme = 1;
                                                                                            while ($data = mysqli_fetch_assoc($qryRes)) {
                                                                                                echo "<tr>
                                                                                                        <td>" . $data['name'] . "<input type='hidden' value='" . $data['id'] . "' name='cat_id" . $nme . "'>
                                                                                                        </td>
                                                                                                        <td><input type='text' class='textfieldbig' name='tax_rate_percent" . $nme . "' id='tax_rate_percent' value='' /></td>
                                                                                                  </tr>";
                                                                                                $nme++;
                                                                                            }
                                                                                            ?>
                                                                                        </table>
                                                                                        <br>
                                                                                            <span class="error" id="lblcname"></span></td>
                                                                                </tr>
                                                                                <!--<tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Tax rate </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="tax_rate_percent" id="tax_rate_percent" value="" />
                                                                                        <br>
                                                                                            <span class="error" id="lblcname"></span></td>
                                                                                </tr>-->

                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Add" /></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div></td>
                                                                </tr>

                                                            </table>
                                                        </form></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
<?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
