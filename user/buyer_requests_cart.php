<?php

// PO 2018-09-21 

require_once("../config/config_gcp.php");
session_start();

$fact_number  = $_GET['id_fact'];

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
	header("location: index.php");
}

if($_SESSION['grower_id']!=0){
	header("location: growers.php?id=".$_SESSION['grower_id']);
}  

        
     $qsel_bo="select bo.buyer_id
                 from buyer_orders bo
                inner join buyers b on bo.buyer_id = b.id
                where bo.id = '" . $fact_number . "' ";	

	 $rsbo = mysqli_query($con,$qsel_bo);   
         
         while($border=mysqli_fetch_array($rsbo))  {
             $id_buyer = $border["buyer_id"];
         }
			
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         
	$qsel="select bi.id_product,
                    p.name as prod_name,
                    p.subcate_name , 
                    bi.price,
                    bi.size_steam,
                    bi.quantity, 
                    bi.id_boxes,
                    bi.steams
              from buyer_requests_shoping_cart_temp_items bi
             inner JOIN product p ON bi.id_product = p.id 
             where bi.id_order_prev = '" . $fact_number . "' ";	

	 $rs=mysqli_query($con,$qsel);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
			} );
</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
 <?php include("includes/header_inner.php");?>

  <tr>
    <td height="5"></td>
  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/agent-left.php");?>

        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                
                            <tr>
                                <td>&nbsp;</td>
                            </tr>                        

                   <tr>
                        <td height="5"></td>
                   </tr>

                  <tr>
                      <td><h3>Shopping Cart</h3></td>
                           <!--td><a href="requests_adm_add.php?id=<?php echo $id_buyer."&id_fact=".$fact_number ?>" class="pagetitle" onclick="this.blur();"><span> + Add New Special Requests </span></a></td-->
                  </tr>
                 
                  <tr>
                    <td>&nbsp;</td>
                  </tr>

                  <tr>

                <td><div id="box">

		<div id="container">			
                    <div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>       
                    <th align="left" width="10%" >Num</th>                      
                    <th align="left" width="30%" >Variety</th>  
                    <th align="left" width="18%">Size </th>                    
                    <th align="left" width="8%">Price</th>                    
                    <th align="left" width="8%">Qty</th>                                        
                    <th align="left" width="10%">Stems</th>                                                            
                    <th align="left" width="10%">Type</th>                                                            

		</tr>

</thead>

	<tbody>
		<?php

		  	$sr=1;
        		  while($product=mysqli_fetch_array($rs))  {
                              
                              if ($product["id_boxes"] == "0") {
                                    $typeBox = "Stems";
                              }else{
                                    $typeBox = "Boxes";
                              }
                              
                                // Verificacion Stems/Bunch
                                $sel_bu_st = "select box_type,subcategoryid from product where id = '" . $product["product"] . "' ";
                                $rs_bu_st = mysqli_query($con,$sel_bu_st);       
                                $bunch_stem = mysqli_fetch_array($rs_bu_st); 
                                
                              

                              
		?>
                          <tr class="gradeU">       
                                <td class="text" align="left"><?php echo $sr ?>  </td>                              
                                <td class="text" align="left"><?php echo $product["prod_name"]." ".$product["subcate_name"]?></td>                                                                                  
                                <td class="text" align="left"><?php echo $product["size_steam"]?>  </td>                                                                                                                  
                                <td class="text" align="left"><?php echo $product["price"]."".$unit?> </td>                                                           
                                <td class="text" align="left"><?php echo $product["quantity"]?> </td> 
                                <td class="text" align="left"><?php echo $product["steams"]?> </td>                                                                  
                                <td class="text" align="left"><?php echo $typeBox?> </td>                                                                  

                                
                          </tr>
				 <?php 
			     $sr++;
			   } ?> 	
	</tbody>
</table>
                                </div>
                            </div>
			</div>
		</td>
                </tr>
                </table>
                </td>

                <td width="10">&nbsp;</td>

                    </tr>
                </table>                    
            </td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>

          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>

          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table>
        </td>
      </tr>

    </table>
    </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>