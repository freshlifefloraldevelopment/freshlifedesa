<?php
	include "../config/config_gcp.php";

	session_start();

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}
        
        
        $sel_info = "select * from subcategory where id='" . $_GET["ids"] . "'";
        $rs_info = mysqli_query($con,$sel_info);
        $info = mysqli_fetch_array($rs_info);                

        $sel_card = "select * from packing_card where id_subcate='" . $_GET["ids"] . "' and id_ficha ='" . $_GET["id_card"] . "'   ";
        $rs_card = mysqli_query($con,$sel_card);
        $card = mysqli_fetch_array($rs_card);                        
        
        
	if(isset($_POST["Submit"]) && $_POST["Submit"]=="Add")	{
            
            if ($_POST["feature"] != "") {
                    $tmp_feature = $_POST["feature"];
            }else{
                    $tmp_feature = 0;
            }      
		
                $relacion = $_GET["id_card"].$_GET["ids"];

		$ins="insert into grower_parameter set idsc         ='".$_GET["ids"]."'     ,
                                                       subcategory  ='".$info["name"]."'    ,
                                                       size         ='".$_POST["size"]."'   ,
                                                       stem_bunch   ='".$card["stems"]."'   ,
                                                       feature      ='".$tmp_feature."'     ,
                                                       factor       ='".$_POST["factor"]."' ,                                                           
                                                       id_ficha     ='".$_GET["id_card"]."' ,    
                                                       type         = 0                     ,
                                                       price_adm    ='".$_GET["price_adm"]."' ,                                                           
                                                       relacion     = '".$relacion."'     ";        

		mysqli_query($con,$ins);

		 header("location:subcat_param.php?ids=".$_GET["ids"]."&id_card=".$_GET["id_card"]);

	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript">

	function verify() { 

		var arrTmp=new Array();

		arrTmp[0]=checkcname();  
                
		arrTmp[1]=checkcprice();                                

		var i;

		_blk=true;

		for(i=0;i<arrTmp.length;i++)	{

			if(arrTmp[i]==false)			{
			   _blk=false;
			}
		}

		if(_blk==true)		{
			return true;
		}else	{
			return false;
		}
	
 	}	
		

	function trim(str) {    

		if (str != null) 	{        

			var i;        

			for (i=0; i<str.length; i++) 	{           

				if (str.charAt(i)!=" ") {               

					str=str.substring(i,str.length);                 

					break;            
				}        

			}            

			for (i=str.length-1; i>=0; i--)		{            

				if (str.charAt(i)!=" ") {                
					str=str.substring(0,i+1);                
					break;            
				}         
			}                 

			if (str.charAt(0)==" ") 	{            
				return "";         
			} else 	{            
				return str;         

			}    

		}
	}

	
	function checkcname()	{

		if(trim(document.frmcat.factor.value) == "")		{	 
			document.getElementById("lblcname").innerHTML="Please enter Factor";
			return false;
		}else{
			document.getElementById("lblcname").innerHTML="";
			return true;
		}
	}
        
	function checkcprice()	{

		if(trim(document.frmcat.price_adm.value) == "")		{	 
			document.getElementById("lblcprice").innerHTML="Please enter Price";
			return false;
		}else{
			document.getElementById("lblcprice").innerHTML="";
			return true;
		}
	}        

	
</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

<?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/product-left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                  <tr>
                    <td height="5"></td>
                  </tr>

                  <tr>
                    <td class="pagetitle">Add New Sub Category Parameter</td>
                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

		<tr>

                    <td>

			<table width="100%">
				<tr>
				    <td>
					<a class="pagetitle1" href="subcat_param.php" onclick="this.blur();"><span> Manage Sub Categories Parameter</span></a>
				    </td>
				</tr>
			</table>
		    </td>

                  </tr>

                  <tr>
                    <td>&nbsp;</td>
                  </tr>

    <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data">

          <tr>

            <td><div id="box">

              <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">

		    <tr>                        
                          <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory                </td>
                    </tr>

                 
                    <tr>
                          <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Sizes</td>
				<td width="66%" bgcolor="#f2f2f2">
                                <select name="size" class="listmenu">
                                        <option value="">--Select--</option>

                                <?php 	    $sel_size="select * from sizes 
                                                        order by (-1*name) desc";

					$rs_size=mysqli_query($con,$sel_size);

				while($sizecat=mysqli_fetch_array($rs_size)){
				?>
                                        <option value="<?php echo $sizecat["id"]?>"><?php echo $sizecat["name"]." cm"?></option>
                                <?php
				 }
				?> 
                                </select>

                                </td>
                    </tr>

			<tr>
                          <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Factor</td>
					<td width="66%" bgcolor="#f2f2f2">
				  	<input type="text" class="textfieldbig" name="factor" id="factor" value="<?php echo $_POST["factor"]?>" />
                                        </td>
                                                   <br>
                                    <span class="error" id="lblcname"></span>	
                        </tr>                                                                                                                
                  
			<tr>
                          <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Price</td>
					<td width="66%" bgcolor="#f2f2f2">
				  	<input type="text" class="textfieldbig" name="price_adm" id="price_adm" value="<?php echo $_POST["price_adm"]?>" />
                                        </td>
                                                   <br>
                                    <span class="error" id="lblcprice"></span>	                          
                        </tr>                                                                                                                                  
                                                      
                  
                    <tr>
                          <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Feature</td>
				<td width="66%" bgcolor="#f2f2f2">
                                <select name="feature" class="listmenu">
                                        <option value="">--Select--</option>

                                <?php 	    $sel_feat="select * from features 
                                                        order by name";

					$rs_feat=mysqli_query($con,$sel_feat);

				while($featurecat=mysqli_fetch_array($rs_feat)){
				?>
                                        <option value="<?php echo $featurecat["id"]?>"><?php echo $featurecat["name"]?></option>
                                <?php
				 }
				?> 
                                </select>
                                            <br>
                                    <span class="error" id="lblfeat"></span>	
                                </td>
                    </tr>               
                  
                    <!--tr>
                          <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Stems/Bunch</td>
				<td width="66%" bgcolor="#f2f2f2">
                                <select name="stem_bunch" class="listmenu">
                                        <option value="">--Select--</option>

                                <?php 	    $sel_stbu="select * from bunch_sizes 
                                                        order by (-1*name) desc";

					$rs_stbu=mysqli_query($con,$sel_stbu);

				while($stbucat=mysqli_fetch_array($rs_stbu)){
				?>
                                        <option value="<?php echo $stbucat["id"]?>"><?php echo $stbucat["name"]?></option>
                                <?php
				 }
				?> 
                                </select>
                                            <br>
                                    <span class="error" id="lblstbu"></span>	
                                </td>
                    </tr-->                                    
                        
                        

                                                                             
                       

                <tr>

                  <td>&nbsp;</td>

                  <td><input name="Submit" type="Submit" class="buttongrey" value="Add" />                  </td>

                </tr>

              </table>

            </div></td>

          </tr>

</form>

</table></td>

        <td width="10">&nbsp;</td>

        </tr>

        </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>

</table>
</body>
</html>
