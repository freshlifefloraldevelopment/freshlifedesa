<?	
    include "../config/config.php";
    session_start();
    if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
    {
        header("location: index.php");
    }
    
    if(!isset($_REQUEST['id'])){
        header("location: create_proposal.php");        
    }

    if(isset($_GET['delete'])) 
    {
      $query = 'DELETE FROM buyers WHERE  id= '.(int)$_GET['delete'];
      mysql_query($query);
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?=$siteurl?>css/stickytooltip.css" />
        <link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                oTable = $('#example').dataTable({
                    //"sScrollXInner": "130%",
                    "bJQueryUI": true,
                    //"sScrollY": "536",
                    "sPaginationType": "full_numbers"
                });
            } );
        </script>
        <style>
            ul{
                list-style-type: none;                
            }
            
        </style>
    </head>
    <body>
        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
            <? include("includes/header_inner.php");?>
            <tr>
                <td height="5"></td>
            </tr>
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <? include("includes/left.php");?>
                            <td width="5">&nbsp;</td>
                            <td valign="top">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pagetitle">Manage Orders</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td style="text-align: center;">
                                                                                <a href="new-purchase-order.php?id=<?=$_REQUEST['id']?>" class="pagetitle" style="text-decoration: underline;"><span> Create new purchase order</span></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div id="gallery">
                                                                        <ul style="border:1px solid #f0f0f0;" ><?php 

                                                                                $i=1;
                                                                                $query2 = "select gpb.*, gpb.id as cartid,gpb.date_added ,p.id,p.name,p.color_id,p.image_path,s.name as subs,sh.name as sizename,ff.name as featurename from buyer_requests gpb
                                                                                left join product p on gpb.product = p.id
                                                                                left join subcategory s on p.subcategoryid=s.id  
                                                                                left join features ff on gpb.feature=ff.id
                                                                                left join sizes sh on gpb.sizeid=sh.id 
                                                                                where gpb.id='".$_REQUEST["id"]."' and ( gpb.type!=1 and gpb.type!=7 and gpb.type!=5 )  and gpb.lfd>='".date("Y-m-d")."' order by gpb.id desc";

                                                                                $result2=mysql_query($query2);
                                                                                $tp=mysql_num_rows($result2);
                                                                                if($tp>=1)
                                                                                {
                                                                                        while($producs=mysql_fetch_array($result2))
                                                                                        {	
                                                                                                ?><li style="float:none; height:auto; <? if($i%2!="0") { ?> background:#f3f3f3; <? } ?> margin:0px; padding:0px;  overflow:hidden; padding-bottom:5px;"><? 
                                                                                                        if($producs["type"]!=3) 
                                                                                                        { 
                                                                                                                ?><div style="float:left; width:360px; margin-top:0px; overflow:hidden;">
                                                                                                                        <div style=" padding-left:10px;width:360px;text-align:left; float:left; ">
                                                                                                                                <div style="float:left; width:120px;">
                                                                                                                                        <?=$producs["subs"]?> 
                                                                                                                                </div>  
                                                                                                                                <div style="float:left; width:170px;">  
                                                                                                                                        <a  href="javascript:void(0);"  style="font-family:verdana;font-weight:normal;color:#000;font-size:12px; font-weight:normal; text-decoration:none;"> 
                                                                                                                                                <div style="float:left; width:15px; margin-right:7px; margin-top:5px;"></div>
                                                                                                                                                <?=$producs["name"]?> <?=$producs["featurename"]?>  
                                                                                                                                        </a>
                                                                                                                                </div>
                                                                                                                                <?=$producs["sizename"]?>cm 
                                                                                                                        </div>
                                                                                                                </div>
                                                                                                                <div style="float:left; width:175px;">
                                                                                                                        <div style=" padding-left:10px;padding-top:0px;width:175px;text-align:left; float:left; "><? 
                                                                                                                                if($producs["comment"]!="") 
                                                                                                                                { 
                                                                                                                                        echo $producs["comment"]; 
                                                                                                                                } 
                                                                                                                                else 
                                                                                                                                { 
                                                                                                                                        echo "&nbsp;"; 
                                                                                                                                } 
                                                                                                                        ?></div>
                                                                                                                </div>
                                                                                                                <div style="float:left;width:100px;font-family:arial;font-weight:normal;color:#000;font-size:13px; font-size:normal;"><? 
                                                                                                                        if($producs["lfd2"]=='0000-00-00')
                                                                                                                        {
                                                                                                                                $lfd_temp=explode("-",$producs["lfd"]); 
                                                                                                                                echo $lfd_temp[1]."-".$lfd_temp[2]."-".$lfd_temp[0]; 
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                                $lfd_temp=explode("-",$producs["lfd2"]); 
                                                                                                                                echo $lfd_temp[1]."-".$lfd_temp[2]."-".$lfd_temp[0]." to <br/>"; 
                                                                                                                                $lfd_temp=explode("-",$producs["lfd"]); 
                                                                                                                                echo $lfd_temp[1]."-".$lfd_temp[2]."-".$lfd_temp[0]." "; 
                                                                                                                        }
                                                                                                                ?></div>
                                                                                                                <div style="float:left; width:80px;font-family:arial;font-weight:normal;color:#000;font-size:13px;"><?
                                                                                                                        if($producs["price"]!="0.00")
                                                                                                                        {
                                                                                                                                echo "$".sprintf("%.2f",$producs["price"]);
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                                echo "-";
                                                                                                                        }
                                                                                                                ?></div><? 
                                                                                                        } 
                                                                                                        else 
                                                                                                        { 
                                                                                                                ?><div style="float:left; width:360px;">
                                                                                                                        <div style=" padding-left:10px;width:588px;text-align:left; float:left; ">
                                                                                                                                Assorted &nbsp;&nbsp;
                                                                                                                                <a  href="javascript:void(0);" onClick="boxshow(<?=$producs["cartid"]?>)" style="font-family:Arial;font-weight:normal;color:#666;text-decoration:underline;">View Products + </a>
                                                                                                                                <br/>
                                                                                                                                <div id="<?=$producs["cartid"]?>" style=" padding-left:10px;width:950px;text-align:left; float:left; display:none;  margin-bottom:20px; margin-top:10px; "><div style="margin-bottom:8px; padding:2px; border:1px solid #e1e1e1; width:950px;">
                                                                                                                                        <div style="width:170px; margin-left:13px; overflow:hidden; float:left; font-size:12px;color:#666; margin-top:0px; font-size:normal;"> Product </div>
                                                                                                                                                <div style="width:270px;overflow:hidden; float:left; font-size:12px;color:#666; margin-top:0px; font-size:normal;"> 
                                                                                                                                                        Variety 
                                                                                                                                                </div>
                                                                                                                                                <div style="width:106px;overflow:hidden; float:left; font-size:12px;color:#666; margin-top:0px; font-size:normal;"> 
                                                                                                                                                        Size 
                                                                                                                                                </div>
                                                                                                                                                <div style="width:120px; overflow:hidden; float:left; font-size:12px;color:#666; margin-top:0px; font-size:normal;"> Bunch Size</div>
                                                                                                                                                <div style="width:140px; overflow:hidden; float:left; font-size:12px;color:#666; margin-top:0px; font-size:normal;"> No. of Bunch</div>
                                                                                                                                                <div style="clear:both;"></div><?
                                                                                                                                                $templ=explode(",",$producs["bunches"]);
                                                                                                                                                $count=sizeof($templ);
                                                                                                                                                for($io=0;$io<=$count-2;$io++)
                                                                                                                                                {
                                                                                                                                                        $r=explode(":",$templ[$io]);
                                                                                                                                                        //echo $r[0]."<br/>";

                                                                                                                                                        $query2 = "select gpb.growerid,gpb.prodcutid,gpb.sizeid,gpb.feature ,p.name as productname,s.name as subs,sh.name as sizename,ff.name as featurename,bs.name as bname from grower_product_box_packing gpb
                                                                                                                                                        left join product p on gpb.prodcutid = p.id
                                                                                                                                                        left join subcategory s on p.subcategoryid=s.id  
                                                                                                                                                        left join features ff on gpb.feature=ff.id
                                                                                                                                                        left join sizes sh on gpb.sizeid=sh.id 
                                                                                                                                                        left join bunch_sizes bs on gpb.bunch_size_id=bs.id
                                                                                                                                                        where gpb.id='".$r[0]."'";	

                                                                                                                                                        $rs2=mysql_query($query2);
                                                                                                                                                        while($product_box=mysql_fetch_array($rs2))
                                                                                                                                                        {
                                                                                                                                                                ?><div style="float:left; width:560px; margin-top:0px; overflow:hidden; font-size:16px;">
                                                                                                                                                                        <div style=" padding-left:10px;width:560px;text-align:left; float:left; font-size:16px; ">
                                                                                                                                                                                <div style="float:left; width:170px;">
                                                                                                                                                                                        <?=$product_box["subs"]?> 
                                                                                                                                                                                </div>  
                                                                                                                                                                                <div style="float:left; width:270px;">  
                                                                                                                                                                                        <a  href="javascript:void(0);"  style="font-family:verdana;font-weight:normal;color:#000;font-size:16px; font-weight:normal; text-decoration:none;"> 
                                                                                                                                                                                                <?=$product_box["productname"]?> <?=$product_box["featurename"]?>  
                                                                                                                                                                                        </a>
                                                                                                                                                                                </div> <?=$product_box["sizename"]?>cm 
                                                                                                                                                                        </div>
                                                                                                                                                                </div>
                                                                                                                                                                <div style="width:120px; overflow:hidden; float:left; font-size:16px;color:#000; margin-top:0px; font-size:normal;"> <?=$product_box["bname"]?> Stems</div>
                                                                                                                                                                <div style="width:140px; overflow:hidden; float:left; font-size:16px;color:#000; margin-top:0px; font-size:normal;"> <?=$r[1]?> </div>
                                                                                                                                                                <div style="clear:both; height:5px;"></div><? 
                                                                                                                                                        } 
                                                                                                                                                } 
                                                                                                                                        ?></div> 
                                                                                                                                </div>
                                                                                                                        </div>
                                                                                                                </div> 
                                                                                                                <div style="float:left; width:173px; ">
                                                                                                                        <div style=" padding-left:10px;padding-top:0px;width:173px;text-align:left; float:left; ">
                                                                                                                                <? if($producs["comment"]!="") { echo $producs["comment"]; } else { echo "&nbsp;"; } ?>
                                                                                                                        </div>
                                                                                                                </div>
                                                                                                                <div style="float:left; width:102px;  font-family:arial;font-weight:normal;color:#000;font-size:14px; font-size:normal;"><? 
                                                                                                                        if($producs["lfd2"]=='0000-00-00')
                                                                                                                        {
                                                                                                                                $lfd_temp=explode("-",$producs["lfd"]); 
                                                                                                                                echo $lfd_temp[1]."-".$lfd_temp[2]."-".$lfd_temp[0]; 
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                                $lfd_temp=explode("-",$producs["lfd2"]); 
                                                                                                                                echo $lfd_temp[1]."-".$lfd_temp[2]."-".$lfd_temp[0]." to <br/>"; 
                                                                                                                                $lfd_temp=explode("-",$producs["lfd"]); 
                                                                                                                                echo $lfd_temp[1]."-".$lfd_temp[2]."-".$lfd_temp[0]." "; 
                                                                                                                        }
                                                                                                                ?></div>
                                                                                                                <div style="float:left; width:80px;  font-family:arial;font-weight:normal;color:#000;font-size:13px; font-size:normal;">-</div><? 
                                                                                                        } 
                                                                                                ?><div style="float:left; width:190px;"><? 
                                                                                                        if($producs["boxtype"]!="") 
                                                                                                        { 
                                                                                                                $temp=explode("-",$producs["boxtype"]);
                                                                                                                $sel_box_type="select * from boxtype where id='".$temp[0]."'";
                                                                                                                $rs_box_type=mysql_query($sel_box_type);
                                                                                                                $box_type=mysql_fetch_array($rs_box_type);
                                                                                                                ?><div style="float:left; width:89px;  font-family:arial;font-weight:normal;color:#000;font-size:14px; font-size:normal; ">
                                                                                                                        <?=$box_type["name"]?> &nbsp;
                                                                                                                </div><? 
                                                                                                        } 
                                                                                                        else 
                                                                                                        { 
                                                                                                                ?><div style="float:left; width:89px;  font-family:arial;font-weight:normal;color:#000;font-size:14px; font-size:normal; ">
                                                                                                                        &nbsp;
                                                                                                                </div><? 
                                                                                                        } 

                                                                                                        if($producs["qty"]>=1) 
                                                                                                        { 
                                                                                                                ?><div style="float:left; width:90px;font-family:arial;font-weight:normal;color:#000;font-size:14px; font-size:normal;"> 
                                                                                                                        <?=$producs["qty"]?> Box(es)
                                                                                                                </div><? 
                                                                                                        } 

                                                                                                        if($producs["noofstems"]>=1) 
                                                                                                        { 
                                                                                                                ?><div style="float:left; width:90px;  font-family:arial;font-weight:normal;color:#000;font-size:14px; font-size:normal;"> 
                                                                                                                        <?=$producs["noofstems"]?> Stems
                                                                                                                </div><? 
                                                                                                        } 
                                                                                                ?></div><?php

                                                                                                $sel_check="select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,cr.flag from grower_offer_reply gr 
                                                                                                left join growers g on gr.grower_id=g.id 
                                                                                                left join country cr on g.country_id=cr.id
                                                                                                where gr.offer_id='".$producs["cartid"]."' and gr.buyer_id='".$_REQUEST["id"]."' and g.id is not NULL order by gr.date_added desc";
                                                                                                $rs_growers=mysql_query($sel_check);
                                                                                                $totalio=mysql_num_rows($rs_growers);
                                                                                                ?><div style="float:left; width:72px; margin-left:10px; font-family:arial;font-weight:normal;color:#333;font-size:14px; font-size:normal;"> 
                                                                                                        <a href="javascript:void(0);" onClick="dotoggle(<?=$i?>)" style="color:#8B2B85; font-size:normal; text-decoration:underline;" > Replies (<?=$totalio?>) </a>
                                                                                                </div><br/><?
                                                                                                if($producs["price"]!="0.00")
                                                                                                {
                                                                                                        ?><span style="color:#FF0000; font-size:10px; font-weight:bold; float:right; margin-right:138px;">( Excluding Shipping , Handling fee and tax )</span><?php 
                                                                                                } 
                                                                                                ?><div style="clear:both;"></div>
                                                                                                <div style="margin-bottom:10px; display:none;" id="divshow-<?=$i?>"><?php
                                                                                                        $l=1;		
                                                                                                        while($growers=mysql_fetch_array($rs_growers))
                                                                                                        {
                                                                                                                if($growers["shipping_method"] > 0  )
                                                                                                                {
                                                                                                                        $total_shipping=0; 
                                                                                                                        $sel_connections="select type,shipping_rate,addtional_rate,box_price,box_weight_arranged,price_per_box from connections where shipping_id='".$growers["shipping_method"]."'";
                                                                                                                        $rs_connections=mysql_query($sel_connections);
                                                                                                                        while($connections=mysql_fetch_array($rs_connections))
                                                                                                                        {
                                                                                                                                if($connections["type"]==2 )
                                                                                                                                {
                                                                                                                                        $total_shipping = $total_shipping + round(($growers["box_weight"]*$connections["shipping_rate"]),2);
                                                                                                                                }
                                                                                                                                else if($connections["type"]==4)
                                                                                                                                {
                                                                                                                                        $total_shipping = $total_shipping + round(($growers["box_volumn"]*$connections["shipping_rate"]),2);
                                                                                                                                }
                                                                                                                                else if($connections["type"]==1)
                                                                                                                                {
                                                                                                                                        $box_type_calc=$producs["boxtype"];
                                                                                                                                        if($box_type_calc=='HB')
                                                                                                                                        {
                                                                                                                                                $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2 ; 
                                                                                                                                                $connections["price_per_box"] = $connections["price_per_box"] / 2 ;
                                                                                                                                        }
                                                                                                                                        if($box_type_calc=='JUMBO')
                                                                                                                                        {
                                                                                                                                                $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2 ; 
                                                                                                                                                $connections["price_per_box"] = $connections["price_per_box"] / 2 ;
                                                                                                                                        }
                                                                                                                                        else if($box_type_calc='QB')
                                                                                                                                        {
                                                                                                                                                $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 4 ;
                                                                                                                                                $connections["price_per_box"] =  $connections["price_per_box"] / 4 ;
                                                                                                                                        }
                                                                                                                                        else if($box_type_calc='EB')
                                                                                                                                        {
                                                                                                                                                $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 8 ;
                                                                                                                                                $connections["price_per_box"] =  $connections["price_per_box"] / 8 ;
                                                                                                                                        }

                                                                                                                                        $connections["shipping_rate"] = $connections["price_per_box"] / $connections["box_weight_arranged"];
                                                                                                                                        if($growers["box_weight"] <= $connections["box_weight_arranged"])
                                                                                                                                        {
                                                                                                                                                $total_shipping = $total_shipping + round(( $growers["box_weight"] * $connections["shipping_rate"] ),2);
                                                                                                                                        }
                                                                                                                                        else
                                                                                                                                        {
                                                                                                                                                $total_shipping = $total_shipping + round(($connections["box_weight_arranged"]*$connections["shipping_rate"]),2);  
                                                                                                                                                $remaining = round($growers["box_weight"] - $connections["box_weight_arranged"],2);
                                                                                                                                                $total_shipping = $total_shipping + round(($remaining*$connections["addtional_rate"]),2);
                                                                                                                                        }
                                                                                                                                }
                                                                                                                                else if( $connections["type"]==3 )
                                                                                                                                {
                                                                                                                                        $total_shipping = $total_shipping + $connections["box_price"] ;
                                                                                                                                }
                                                                                                                        }
                                                                                                                }
                                                                                                                $k=explode("/",$growers["file_path5"]);
                                                                                                        ?><div style="clear:both; height:20px;"> &nbsp;</div>
                                                                                                        <div style="border-bottom:1px solid #e0e0e0; height:1px; width:100%; margin-top:10px;"> </div>
                                                                                                        <div style=" <? if($growers["reject"]==0) { ?> height:auto; <? } else { ?> height:60px; <? } ?> width:990px; margin-bottom:10px; overflow:hidden; padding-top:10px;">
                                                                                                                <div style="float:left; width:100%; margin-left:20px; font-family:Arial, Helvetica, sans-serif; font-size:17px; font-size:normal;  color:#8B2B85;">
                                                                                                                        <div style="float:left; width:110px; margin-left:10px;"> <img src="<?=$siteurl?>user/logo2/<?=$k[1]?>" width="100" height="48" /> </div>
                                                                                                                                <div style="float:left; width:150px; margin-left:10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-size:normal;color:#8B2B85; margin-top:10px;">
                                                                                                                                        <?=$growers["growers_name"]?><?	
                                                                                                                                        if($growers["countryname"]!="")
                                                                                                                                        {
                                                                                                                                                if($growers["flag"]!="") 
                                                                                                                                                { 
                                                                                                                                                        ?><br/>
                                                                                                                                                        <div style="float:left; width:20px;"><img src="<?php echo $growers["flag"] ?>" title="" width="16" height="11" style="border:none; padding:5px; margin:0px;" /></div><?
                                                                                                                                                }
                                                                                                                                                ?><div style="color:rgb(102, 102, 102); float:left; height:50px; font-size:normal; font-size:11px; padding-top:3px; margin-left:10px; ">
                                                                                                                                                        <?=$growers["countryname"]?>
                                                                                                                                                </div><?
                                                                                                                                                $l++;
                                                                                                                                        }
                                                                                                                                        ?></div><?	 
                                                                                                                                        if($growers["reject"]!=1) 
                                                                                                                                        { 
                                                                                                                                                ?><div style="float:left; width:100px;font-family:arial;font-weight:normal;color:#333;font-size:14px; font-size:normal; "> 
                                                                                                                                                        <span style=" font-size:10px; margin-top:7px; font-size:normal; display:block; color:#333">Box Type</span>
                                                                                                                                                        <?=$growers["boxtype"]?>
                                                                                                                                                </div>
                                                                                                                                                <div style="float:left; width:70px;font-family:arial;font-weight:normal;color:#333;font-size:14px; font-size:normal; "> <span style="font-size:10px; margin-top:7px; font-size:normal; display:block; color:#333">Box Qty</span>
                                                                                                                                                        <?=$growers["boxqty"]?>
                                                                                                                                                </div>
                                                                                                                                        <div style="float:left; width:415px; margin-top:7px; font-family:arial;font-weight:normal;color:#666;font-size:15px; font-size:normal;">
                                                                                                                                                <span style=" font-size:10px; font-size:normal; display:block; color:#333">Offer Date</span><?
                                                                                                                                                $temp_offer_date=explode("-",$growers["date_added"]);
                                                                                                                                                $temp_offer=$temp_offer_date[1]."-".$temp_offer_date[2]."-".$temp_offer_date[0];
                                                                                                                                                echo $temp_offer ;
                                                                                                                                        ?></div>
                                                                                                                                        <div style="float:left; width:100px;  margin-top:4px; font-family:arial;font-weight:normal;color:#666;font-size:15px; font-size:normal;"><?
                                                                                                                                                $tdate2=date("Y-m-d");
                                                                                                                                                $date12=date_create($grower_offer["date_added"]);
                                                                                                                                                $date22=date_create($tdate2);
                                                                                                                                                $interval = $date22->diff($date12);
                                                                                                                                                $checka1==$interval->format('%R%a');; 

                                                                                                                                                $expired=0;

                                                                                                                                                if($checka1<=2)
                                                                                                                                                {

                                                                                                                                                        $sel_buyer_info="select live_days from buyers where id='".$grower_offer["buyer_id"]."'";
                                                                                                                                                        $rs_buyer_info=mysql_query($sel_buyer_info);
                                                                                                                                                        $buyer_info=mysql_fetch_array($rs_buyer_info);

                                                                                                                                                        if($buyer_info["live_days"]>0)
                                                                                                                                                        {
                                                                                                                                                                $tdate=date("Y-m-d");
                                                                                                                                                                $tdate;
                                                                                                                                                                $date1=date_create($producs['lfd']);
                                                                                                                                                                $date2=date_create($tdate);
                                                                                                                                                                $interval = $date2->diff($date1);
                                                                                                                                                                $checka2=$interval->format('%R%a');

                                                                                                                                                                if($checka2==0)
                                                                                                                                                                {
                                                                                                                                                                        $expired=1;
                                                                                                                                                                }
                                                                                                                                                                if($checka2>=$buyer_info["live_days"])
                                                                                                                                                                {
                                                                                                                                                                        $expired=0;
                                                                                                                                                                }
                                                                                                                                                                else
                                                                                                                                                                {
                                                                                                                                                                        $expired=1;
                                                                                                                                                                }
                                                                                                                                                        }	 
                                                                                                                                                }
                                                                                                                                                else
                                                                                                                                                {
                                                                                                                                                        $expired=1;
                                                                                                                                                }
                                                                                                                                                if($growers["status"]==0) 
                                                                                                                                                { 
                                                                                                                                                        if($info["active"]=='suspended') 
                                                                                                                                                        { 
                                                                                                                                                                if($expired==0) 
                                                                                                                                                                { 
                                                                                                                                                                        ?><a href="http://staging.freshlifefloral.com/suspend.php?id=<?=$info["id"]?>" class="example358 signin cboxElement" ><img src="<?=$siteurl?>/images/accept.png" border="0" style="background:none; border:0px" /></a><? 
                                                                                                                                                                } 
                                                                                                                                                                else 
                                                                                                                                                                { 
                                                                                                                                                                        ?><span style=" font-size:18px; font-size:bold; display:block; color: #FF0000;">Expired</span><? 
                                                                                                                                                                } 
                                                                                                                                                        } 
                                                                                                                                                        else 
                                                                                                                                                        { 
                                                                                                                                                                if($expired==0) 
                                                                                                                                                                { 
                                                                                                                                                                        ?><a href="?offerid=<?=$producs["cartid"]?>&replyid=<?=$growers["grid"]?>" onClick="return confirm('Are you sure you want to accept this offer ?')" ><img src="<?=$siteurl?>/images/accept.png" border="0" style="background:none; border:0px" /></a><? 
                                                                                                                                                                } 
                                                                                                                                                                else 
                                                                                                                                                                { 
                                                                                                                                                                        ?><span style=" font-size:18px; font-size:bold; display:block; color: #FF0000;">Expired</span><? 
                                                                                                                                                                } 
                                                                                                                                                        } 
                                                                                                                                                } 
                                                                                                                                                else 
                                                                                                                                                { 
                                                                                                                                                        ?><span style=" font-size:10px; font-size:normal; display:block; color:#333">Accept Date</span><?
                                                                                                                                                        $temp_offer_date=explode("-",$growers["accept_date"]);
                                                                                                                                                        $temp_offer=$temp_offer_date[1]."-".$temp_offer_date[2]."-".$temp_offer_date[0];
                                                                                                                                                        echo $temp_offer ;
                                                                                                                                                }
                                                                                                                                        ?></div>
                                                                                                                                </div><? 
                                                                                                                        } 
                                                                                                                        else 
                                                                                                                        { 
                                                                                                                                ?><div style="float:left; width:630px;font-family:arial;font-weight:bold;color:#F00;font-size:14px; margin-top:15px; text-align:left; font-size:normal;"> Rejected : <?=$growers["reason"]?> </div><? 
                                                                                                                        } 
                                                                                                                        ?><div style="clear:both;"></div><? 
                                                                                                                        if($growers["type"]!=2)
                                                                                                                        {
                                                                                                                                if($growers["reject"]==0) 
                                                                                                                                {
                                                                                                                                        if($total_shipping>0)
                                                                                                                                        {
                                                                                                                                                $product_shipping = round($total_shipping / ($growers["bunchsize"]*$growers["bunchqty"]),2);
                                                                                                                                        }
                                                                                                                                        ?><div style="float:left; width:350px; margin-top:0px; margin-left:18px;">
                                                                                                                                                <div style=" padding-left:10px;width:380px;text-align:left; float:left; ">
                                                                                                                                                        <a  href="javascript:void(0);" style="font-family:Verdana;font-weight:normal;color:#333;font-size:14px; font-size:normal;">
                                                                                                                                                                <span style=" font-size:10px; font-size:normal; display:block; color:#333">Variety</span><?=$growers["product"]?> <?=$growers["size"]?>
                                                                                                                                                        </a> 
                                                                                                                                                </div>
                                                                                                                                        </div>
                                                                                                                                        <div style="float:left; width:85px;font-family:arial;font-weight:normal;color:#333;font-size:14px; font-size:normal; "> 
                                                                                                                                                <span style=" font-size:10px; font-size:normal; display:block; color:#333">Offer Price</span><?
                                                                                                                                                $total_price=$growers["price"]; 

                                                                                                                                                $total_taxs4[$i]=0;
                                                                                                                                                $total_handling_feess[$i]=0;
                                                                                                                                                $total_product_price4[$i]=$growers["price"];

                                                                                                                                                if($info["tax"]!=0)
                                                                                                                                                {
                                                                                                                                                        // $total_price = $total_price + (($growers["price"]*$info["tax"]) / 100);
                                                                                                                                                        $total_taxs4[$i] = round(($growers["price"]*$info["tax"]) / 100,2);
                                                                                                                                                }
                                                                                                                                                if($info["handling_fees"]!=0)
                                                                                                                                                {
                                                                                                                                                        // $total_price = $total_price + (($growers["price"]*$info["handling_fees"] ) / 100);
                                                                                                                                                        $total_handling_feess4[$i]= round(($growers["price"]*$info["handling_fees"]) / 100,2);
                                                                                                                                                }
                                                                                                                                                if($product_shipping > 0)
                                                                                                                                                {
                                                                                                                                                        // $total_price = $total_price +  $product_shipping ;
                                                                                                                                                        $product_shippings4[$i] = $product_shipping ; 
                                                                                                                                                }

                                                                                                                                                $total_price = round(($total_price),2) ;
                                                                                                                                                $final_price24[$i] = $total_price ;

                                                                                                                                                $final_price24[$i] = round(($total_product_price4[$i] + $total_taxs4[$i] + $total_handling_feess4[$i] + $product_shippings4[$i]),2);

                                                                                                                                                ?><a  href="javascript:void(0);" style="font-family:verdana;font-weight:normal;color:#000;font-size:12px; font-weight:normal;" 
                                                                                                                                                        <?php if( $total_taxs4[$i] > 0 || $total_handling_feess4[$i] > 0 || $product_shippings4[$i] > 0   ) { ?> data-tooltip="shipping<?=$i?>" <? } ?>  > $ <?=sprintf ("%.2f", $final_price24[$i])?> 
                                                                                                                                                </a>
                                                                                                                                        </div>
                                                                                                                                        <div style="width:402px; overflow:hidden; float:left;">
                                                                                                                                                <div style="float:left; width:120px;font-family:arial;font-weight:normal;color:#333;font-size:14px; font-size:normal; "> <span style=" font-size:10px; font-size:normal; display:block; color:#333">Bunch Size</span>
                                                                                                                                                        <?=$growers["bunchsize"]?>
                                                                                                                                                </div>
                                                                                                                                                <div style="float:left; width:105px;font-family:arial;font-weight:normal;color:#333;font-size:14px; font-size:normal; "> <span style="font-size:10px; font-size:normal; display:block; color:#333">Bunch Qty / box </span>
                                                                                                                                                        <?=$growers["bunchqty"]?>
                                                                                                                                                </div>
                                                                                                                                        </div><? 
                                                                                                                                } 
                                                                                                                        } 
                                                                                                                        else 
                                                                                                                        { 
                                                                                                                                $sel_boxes="select boxname,boxqty,boxtype from grower_reply_box where offer_id='".$producs["cartid"]."' and reply_id!=0 group by boxname";
                                                                                                                                $rs_boxes=mysql_query($sel_boxes);
                                                                                                                                $total_boxes=mysql_num_rows($rs_boxes);
                                                                                                                                while($boxes=mysql_fetch_array($rs_boxes))
                                                                                                                                {
                                                                                                                                        $name=explode("-",$boxes["boxname"])
                                                                                                                                        ?><div style="overflow:hidden; height:auto; margin-bottom:10px; border:1px solid #e0e0e0; padding-bottom:20px; width:955px; margin-left:15px; clear:both;"  >
                                                                                                                                                <div style="margin-left:40px; margin-top:20px;">
                                                                                                                                                        <div style="clear:both;"></div><?
                                                                                                                                                        $sel_box_products="select id,product,price,bunchsize,bunchqty from grower_reply_box where offer_id='".$producs["cartid"]."' and reply_id!=0 and boxname='".$boxes["boxname"]."'";
                                                                                                                                                        $rs_box_products=mysql_query($sel_box_products);
                                                                                                                                                        $kh=1;
                                                                                                                                                        while($box_products=mysql_fetch_array($rs_box_products))
                                                                                                                                                        {
                                                                                                                                                                if($growers["total_stems"]>0)
                                                                                                                                                                {
                                                                                                                                                                        $product_shipping_2 = ($box_products["bunchsize"]*$box_products["bunchqty"]);
                                                                                                                                                                        $product_shipping_temp = round(($product_shipping_2*$total_shipping) / $growers["total_stems"],2);
                                                                                                                                                                        $shipping_per_stems =  round( $product_shipping_temp / $product_shipping_2 ,2);
                                                                                                                                                                }
                                                                                                                                                                ?><div style="clear:both; width:903px; margin-top:10px;">
                                                                                                                                                                        <div style="float:left; width:380px;  font-family:arial;font-weight:normal;color:#333;font-size:14px; font-size:normal; "><? 
                                                                                                                                                                                if($kh==1) 
                                                                                                                                                                                { 
                                                                                                                                                                                        ?><span style=" font-size:10px; font-size:normal; display:block; color:#333"> Variety</span><? 
                                                                                                                                                                                } 
                                                                                                                                                                                ?>
                                                                                                                                                                                <?=$box_products["product"]?>
                                                                                                                                                                        </div><?php
                                                                                                                                                                        $total_price=$box_products["price"]; 
                                                                                                                                                                        $total_taxs[$i]=0;
                                                                                                                                                                        $total_handling_feess[$i]=0;
                                                                                                                                                                        $total_product_price[$kh]=$box_products["price"];

                                                                                                                                                                        if($info["tax"]!=0)
                                                                                                                                                                        {
                                                                                                                                                                                // $total_price = $total_price + (($box_products["price"]*$info["tax"]) / 100);
                                                                                                                                                                                $total_taxs[$kh]=round(($box_products["price"]*$info["tax"]) / 100,2);
                                                                                                                                                                        }
                                                                                                                                                                        if($info["handling_fees"]!=0)
                                                                                                                                                                        {
                                                                                                                                                                                // $total_price = $total_price + (($box_products["price"]*$info["handling_fees"] ) / 100);
                                                                                                                                                                                $total_handling_feess[$kh]=round(($box_products["price"]*$info["handling_fees"]) / 100,2);
                                                                                                                                                                        }
                                                                                                                                                                        if($shipping_per_stems > 0)
                                                                                                                                                                        {
                                                                                                                                                                                // $total_price = $total_price + $shipping_per_stems ;
                                                                                                                                                                                $product_shippings[$kh] = $shipping_per_stems ; 
                                                                                                                                                                        }

                                                                                                                                                                        // $total_price = round(($total_price),2) ;
                                                                                                                                                                        $final_price2[$kh] = round(($total_product_price[$kh] + $total_taxs[$kh] + $total_handling_feess[$kh] + $product_shippings[$kh]),2);

                                                                                                                                                                        ?><div style="float:left; width:95px;  font-family:arial;color:#333;font-size:14px; font-size:normal; "><? 
                                                                                                                                                                                if($kh==1) 
                                                                                                                                                                                { 
                                                                                                                                                                                        ?><span style="font-size:10px; font-size:normal; display:block; color:#333">Offer Price</span><? 
                                                                                                                                                                                } 
                                                                                                                                                                                ?><a  href="javascript:void(0);" style="font-family:verdana;font-weight:normal;color:#000;font-size:12px; font-weight:normal;" <?php if( $total_taxs[$kh] > 0 || $total_handling_feess[$kh] > 0 || $product_shippings[$kh] > 0   ) { ?> data-tooltip="shipping2<?=$kh?>" <? } ?>  > $ <?=sprintf ("%.2f",$final_price2[$kh])?> </a>
                                                                                                                                                                        </div>

                                                                                                                                                                        <div style="float:left; width:120px;  font-family:arial;font-weight:normal;color:#333;font-size:14px; font-size:normal; "><? 
                                                                                                                                                                                if($kh==1) 
                                                                                                                                                                                { 
                                                                                                                                                                                        ?><span style=" font-size:10px; font-size:normal; display:block; color:#333">Bunch Size</span><? 
                                                                                                                                                                                } 
                                                                                                                                                                                ?><?=$box_products["bunchsize"]?>
                                                                                                                                                                        </div>
                                                                                                                                                                        <div style="float:left; width:80px;  font-family:arial;font-weight:normal;color:#333;font-size:14px; font-size:normal; "><? 
                                                                                                                                                                                if($kh==1) 
                                                                                                                                                                                { 
                                                                                                                                                                                        ?><span style="font-size:10px; font-size:normal; display:block; color:#333">Bunch Qty </span><? 
                                                                                                                                                                                } 
                                                                                                                                                                                ?><?=$box_products["bunchqty"]?>
                                                                                                                                                                        </div>
                                                                                                                                                                        <div style="clear:both"></div>
                                                                                                                                                                </div><?   
                                                                                                                                                                $kh++; 
                                                                                                                                                        } 
                                                                                                                                                ?></div>
                                                                                                                                        </div>
                                                                                                                                </div><?  
                                                                                                                        } 
                                                                                                                } 
                                                                                                                ?><div style="clear:both; height:20px;"> &nbsp;</div><?   
                                                                                                        }
                                                                                                ?></div>
                                                                                        </li><?
                                                                                        $i++;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    ?><li style="float:none; height:80px; margin:0px; padding:0px;">
                                                                                            <div style="margin-left:10px; margin-top:35px; text-align:center;  font-size:30px; color:#333;" > No offer(s) found ! </div>
                                                                                    </li><?
                                                                                }
                                                                            ?></ul>
                                                                            <div id="mystickytooltip" class="stickytooltip"><?
                                                                                for ($j=1;$j<=$i;$j++) 
                                                                                {
                                                                                        $total_price12[$j] = $total_product_price4[$j] ;
                                                                                        if($info["display_shipping"]==1) 
                                                                                        {
                                                                                                $total_price12[$j] = ( $total_price12[$j] + $product_shippings4[$j] ); 
                                                                                        }
                                                                                        if($info["display_handling"]==1)
                                                                                        {
                                                                                                $total_price12[$j] = ($total_price12[$j] +  $total_handling_feess4[$j]); 
                                                                                        }
                                                                                        if($info["display_tax"]==1)
                                                                                        {
                                                                                                $total_price12[$j] = ( $total_price12[$j] +  $total_taxs4[$j]); 
                                                                                        }
                                                                                        ?><div id="shipping<?=$j?>" class="atip" style="width:180px; height:90px; padding:20px;" > 
                                                                                                <div style="font-family:Verdana, Geneva, sans-serif; font-size:14px;">  
                                                                                                        <div style="width:75px; float: left;"> Price  </div> : $ 
                                                                                                        <?=sprintf("%.2f",$total_price12[$j] )?>  
                                                                                                </div><?php 
                                                                                                if($info["display_handling"]==0) 
                                                                                                {
                                                                                                        ?><div style="font-family:Verdana, Geneva, sans-serif; font-size:14px;"> <div style="width:75px; float: left;"> Handling </div> : $ 
                                                                                                                <?=sprintf("%.2f",$total_handling_feess4[$j])?>
                                                                                                        </div><?php 
                                                                                                } 

                                                                                                if($info["display_shipping"]==0) 
                                                                                                {
                                                                                                        ?><div style="font-family:Verdana, Geneva, sans-serif; font-size:14px;"> 
                                                                                                                <div style="width:75px; float: left;"> Shipping </div> : $ 
                                                                                                                <?=sprintf("%.2f",$product_shippings4[$j])?>
                                                                                                        </div><?php 
                                                                                                } 
                                                                                                if($info["display_tax"]==0)
                                                                                                {
                                                                                                        ?><div style="font-family:Verdana, Geneva, sans-serif; font-size:14px;"> 
                                                                                                                <div style="width:75px; float: left;"> Tax </div> : $ 
                                                                                                                <?=sprintf("%.2f",$total_taxs4[$j])?>
                                                                                                        </div><?php 
                                                                                                } 
                                                                                                if( $info["display_tax"]==0 || $info["display_shipping"]==0 ||  $info["display_handling"]==0  ) 
                                                                                                { 
                                                                                                        ?><div style="font-family:Verdana, Geneva, sans-serif; font-size:14px;"> <div style="width:70px; float: left; text-align:right; padding-right:5px;"> <b>Total </b>  </div> :  <b> $ 
                                                                                                                <?=sprintf("%.2f",$final_price24[$j])?> </b> 
                                                                                                        </div><? 
                                                                                                } 
                                                                                        ?></div><? 
                                                                                } 
                                                                                for ($j=1;$j<=$kh;$j++) 
                                                                                {
                                                                                        $total_price12[$j] = $total_product_price[$j] ;
                                                                                        if($info["display_shipping"]==1) 
                                                                                        {
                                                                                                $total_price12[$j] = ( $total_price12[$j] + $product_shippings[$j] ); 
                                                                                        }
                                                                                        if($info["display_handling"]==1)
                                                                                        {
                                                                                                $total_price12[$j] = ($total_price12[$j] +  $total_handling_feess[$j]); 
                                                                                        }
                                                                                        if($info["display_tax"]==1)
                                                                                        {
                                                                                                $total_price12[$j] = ( $total_price12[$j] +  $total_taxs[$j]); 
                                                                                        }
                                                                                        ?><div id="shipping2<?=$j?>" class="atip" style="width:180px; height:90px; padding:20px;" > 
                                                                                            <div style="font-family:Verdana, Geneva, sans-serif; font-size:14px;">  
                                                                                                    <div style="width:75px; float: left;"> Price  </div> : $ 
                                                                                                    <?=sprintf("%.2f", $total_price12[$j])?>  
                                                                                            </div><?php 
                                                                                            if($info["display_handling"]==0) 
                                                                                            {
                                                                                                    ?><div style="font-family:Verdana, Geneva, sans-serif; font-size:14px;"> <div style="width:75px; float: left;"> Handling </div> : $ 
                                                                                                            <?=sprintf("%.2f",$total_handling_feess[$j])?>
                                                                                                    </div><?php 
                                                                                            } 

                                                                                            if($info["display_shipping"]==0) 
                                                                                            {
                                                                                                    ?><div style="font-family:Verdana, Geneva, sans-serif; font-size:14px;"> <div style="width:75px; float: left;"> Shipping </div> : $ 
                                                                                                            <?=sprintf("%.2f",$product_shippings[$j])?>
                                                                                                    </div><?php 
                                                                                            } 
                                                                                            if($info["display_tax"]==0)
                                                                                            {
                                                                                                    ?><div style="font-family:Verdana, Geneva, sans-serif; font-size:14px;"> <div style="width:75px; float: left;"> Tax </div> : $ 
                                                                                                            <?=sprintf("%.2f",$total_taxs[$j])?>
                                                                                                    </div><?php 
                                                                                            } 

                                                                                            if( $info["display_tax"]==0 || $info["display_shipping"]==0 ||  $info["display_handling"]==0  ) 
                                                                                            { 
                                                                                                    ?><div style="font-family:Verdana, Geneva, sans-serif; font-size:14px;"> 
                                                                                                            <div style="width:70px; float: left; text-align:right; padding-right:5px;"> <b>Total </b>  </div> :  <b> $ 
                                                                                                            <?=sprintf("%.2f",$final_price2[$j])?> </b> 
                                                                                                    </div><? 
                                                                                            } 
                                                                                     ?></div><? 
                                                                                } 
                                                                            ?></div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                      <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                      <td background="images/middle-bottomline.gif"></td>
                                      <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
              <td height="10"></td>
            </tr>
            <? include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
