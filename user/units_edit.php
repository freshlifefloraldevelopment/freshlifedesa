<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";

	session_start();

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}

	if(isset($_POST["Submit"]) && $_POST["Submit"]=="Save")	{			

	    $ins="update units set code ='".$_POST["code"]."', descrip ='".$_POST["descrip"]."' , qty1 ='".$_POST["qty"]."' , type ='".$_POST["type"]."' where id='".$_GET["id"]."' ";

		mysqli_query($con,$ins);
		header('location:units_mgmt.php');
	}
	

	$sel_colors="select * from units where id='".$_GET["id"]."' ";

	$rs_colos=mysqli_query($con,$sel_colors);

	$colors=mysqli_fetch_array($rs_colos);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript">

	function verify(){ 

	    var arrTmp=new Array();
		arrTmp[0]=checkcode();
                arrTmp[1]=checkdesc();
		arrTmp[2]=checkqty();
		
		var i;

		_blk=true;

		for(i=0;i<arrTmp.length;i++){
			if(arrTmp[i]==false){
			   _blk=false;
			}
		}

		if(_blk==true)	{
			return true;
		}else{
			return false;
		}	
 	}	
			
	function trim(str) {    

		if (str != null) {        

			var i;        

			for (i=0; i<str.length; i++) {           

				if (str.charAt(i)!=" ") {               
					str=str.substring(i,str.length);                 
					break;            
				}        
			}            

			for (i=str.length-1; i>=0; i--)	{            

				if (str.charAt(i)!=" ") {                
					str=str.substring(0,i+1);                
					break;            
				}         
			}                 

			if (str.charAt(0)==" ") {            
				return "";         
			}else {            
				return str;         
			}    
		}
	}
	

	function checkcode(){

		if(trim(document.frmcat.code.value) == ""){	 
			document.getElementById("lblcode").innerHTML="Please enter code name";
			return false;
		}else {
			document.getElementById("lblcode").innerHTML="";
			return true;
		}
	}

	function checkdesc()	{

		if(trim(document.frmcat.width.value) == ""){	 

			document.getElementById("lbldesc").innerHTML="Please enter description";
			return false;
		}else {

			document.getElementById("lbldesc").innerHTML="";
			return true;
		}
	}
		
	

	function checkqty()	{

		if(trim(document.frmcat.length.value) == "")	{	 

			document.getElementById("lblqty").innerHTML="Please enter qty";
			return false;
		}else {
			document.getElementById("lblqty").innerHTML="";
			return true;
		}
	}
	
</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

<?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/product-left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                  <tr>

                    <td height="5"></td>

                  </tr>

                  <tr>

                    <td class="pagetitle">Edit Unit</td>

                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

				  <tr>

                            <td>
				<table width="100%">
				<tr>
				<td>

					<a class="pagetitle1" href="units_mgmt.php" onclick="this.blur();"><span> Manage Units</span></a>

				</td>
				</tr>
				</table>
			    </td>

                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

               <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data" action="units_edit.php?id=<?php echo $_GET["id"]?>">

          <tr>

            <td><div id="unit">

              <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">

			<tr>                        
                          <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory                </td>
                        </tr>

		        <tr>
                          <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Code :</td>
			     <td width="66%" bgcolor="#f2f2f2">
				    <input type="text" class="textfieldbig" name="code" id="code" value="<?php echo $colors["code"]?>" />
				    <br><span class="error" id="lblcode"></span>	
                        </td>

                        </tr>     
                        
                        
                         <tr>
                          <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Description :</td>
			    <td width="66%" bgcolor="#f2f2f2">
				<input type="text" class="textfieldbig" name="descrip" id="descrip" value="<?php echo $colors["descrip"]?>" />
				<br><span class="error" id="lbldesc"></span>	
	                    </td>
                        </tr>                            
                        
                         <tr>
                              <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Qty :</td>
			      <td width="66%" bgcolor="#f2f2f2">
			  	<input type="text" class="textfieldbig" name="qty" id="qty" value="<?php echo $colors["qty1"]?>" />
				<br><span class="error" id="lblqty"></span>	
	                    </td>
                        </tr>  
                                                                        
                        <tr>
                            
                                      <td width="20%" align="left" valign="middle" class="text">&nbsp;<span class="error">&nbsp;&nbsp;</span>Type :</td>
                                      <td width="80%" bgcolor="#f2f2f2"><select name="type" id="type">
                                          <option value="0">--Select--</option>
                                          <?php    
						$sel_user="select * from units_type";
						$rs_user=mysqli_query($con,$sel_user);
						while($user=mysqli_fetch_array($rs_user))   {
									  ?>
                                          <option value="<?php echo $user["id"]?>" <?php if($colors["type"]==$user["id"]) { echo "selected"; } ?> >
                                          <?php echo $user["name"]?>
                                          </option>
                                          <?php
									      }
									  ?>
                                        </select>
                                        <br>
                                        <span class="error" id="lbltype"></span> </td>
                        </tr>                                      

                <tr>

                  <td>&nbsp;</td>

                  <td><input name="Submit" type="Submit" class="buttongrey" value="Save" /></td>

                </tr>

              </table>

            </div></td>

          </tr>

		  </form>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>