<?php
    // PO #1  2-jul-2018
include "../config/config_gcp.php";


if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}

if (isset($_POST["Submit"]) && $_POST["Submit"] == "Save") {
    
    //echo '<pre>';print_r($_REQUEST);die;
    $name = $_REQUEST['name'];
    $tax = "UPDATE `tax_table` SET `name` = '".$name."' WHERE `tax_table_id` = '" . $_GET["id"] . "'";
    mysqli_query($con, $tax);
    $last_id = mysqli_insert_id($con);
    //$ins = "insert into tax_table set name='" . trim($_POST["name"]) . "',cat_id='" . trim($_POST["cat_id"]) . "',tax_rate_percent='" . trim($_POST["tax_rate_percent"]) . "'";
    $qry = "select * from category order by id";
    $qryRes = mysqli_query($con, $qry);
    $count = mysqli_num_rows($qryRes);
    for ($i = 1; $i <= $count; $i++) {
        $id = $_REQUEST['cat_id' . $i];
        $rate = $_REQUEST['tax_rate_percent' . $i];
        $taxId = $_REQUEST['tax_rt_id' . $i];
        
        $ins = "UPDATE `tax_cat_det` SET `cat_id` = '".$id."',  `tax_rate_percent` = '".$rate."' WHERE `id` = '".$taxId."'";
        mysqli_query($con, $ins);
    }

    header('location:tax_tables.php');
}

$sel_info = "select * from tax_table where tax_table_id='" . $_GET["id"] . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="js/select2/select2.min.js"></script>
        <link href="js/select2/select2.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            $(document).ready(function () {
                $('select').select2();
            });

            function verify()
            {
                var arrTmp = new Array();
                //arrTmp[0] = checkcname();
                arrTmp[0] = checkcat_desc();

                var i;

                _blk = true;

                for (i = 0; i < arrTmp.length; i++)

                {

                    if (arrTmp[i] == false)

                    {

                        _blk = false;

                    }

                }

                if (_blk == true)

                {

                    return true;

                }

                else

                {

                    return false;

                }

            }

            function trim(str)
            {
                if (str != null)
                {
                    var i;
                    for (i = 0; i < str.length; i++)
                    {
                        if (str.charAt(i) != " ")
                        {
                            str = str.substring(i, str.length);
                            break;

                        }
                    }

                    for (i = str.length - 1; i >= 0; i--)

                    {

                        if (str.charAt(i) != " ")

                        {

                            str = str.substring(0, i + 1);

                            break;

                        }

                    }

                    if (str.charAt(0) == " ")

                    {

                        return "";

                    }

                    else

                    {

                        return str;

                    }

                }

            }

            function checkcname()
            {
                if (trim(document.frmcat.cname.value) == "")
                {
                    document.getElementById("lblcname").innerHTML = "Please enter shipping method name";
                    return false;
                }
                else
                {
                    document.getElementById("lblcname").innerHTML = "";
                    return true;
                }

            }

            function checkcat_desc()
            {
                if (trim(document.frmcat.cat_desc.value) == "")
                {
                    document.getElementById("lblcat_desc").innerHTML = "Please enter shipping description";
                    return false;

                }

                else
                {
                    document.getElementById("lblcat_desc").innerHTML = "";
                    return true;

                }

            }

        </script>
    </head>
    <body>
        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
            <?php include("includes/header_inner.php"); ?>
            <tr>
                <td height="5"></td>
            </tr>
            <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <?php include("includes/left.php"); ?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><form name="frmcat" action="tax_edit.php?id=<?php echo $_GET["id"] ?>" method="post" onsubmit="return verify();" enctype="multipart/form-data">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td height="5"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pagetitle">Edit Tax table</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><table width="100%">
                                                                            <tr>
                                                                                <td><a class="pagetitle1" href="tax_tables.php" onclick="this.blur();"><span> Manage Tax tables</span></a></td>
                                                                            </tr>
                                                                        </table></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                </tr>

                                                                <tr>
                                                                    <td><div id="box">
                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                                                                <tr>
                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Name </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="name" id="cname" value="<?php echo $info["name"] ?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblcname"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="66%" bgcolor="#f2f2f2" colspan="2">
                                                                                        <table name="cat_id1" style="width: 659px;">
                                                                                            <tr>
                                                                                                <td>Category</td>
                                                                                                <td>Tax rate</td>
                                                                                            </tr>
                                                                                            <?php
                                                                                            $getTaxRate = "select * from tax_cat_det where tax_table_id='" . $_GET["id"] . "'";
                                                                                            $taxRes = mysqli_query($con, $getTaxRate);
                                                                                            $nme = 1;
                                                                                            while ($data = mysqli_fetch_assoc($taxRes)) {
                                                                                                $qry = "select * from category where id ='" . $data['cat_id'] . "'";
                                                                                                $qryRes = mysqli_query($con, $qry);
                                                                                                $catData = mysqli_fetch_assoc($qryRes);
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td><?php echo $catData['name']; ?><input type='hidden' value='<?php echo $catData['id']; ?>' name='cat_id<?php echo $nme; ?>' />
                                                                                                        <input type="hidden" name="tax_rt_id<?php echo $nme ?>" value="<?php echo $data['id']; ?>" />
                                                                                                    </td>
                                                                                                    <td><input type='text' class='textfieldbig' name='tax_rate_percent<?php echo $nme; ?>' id='tax_rate_percent<?php echo $nme; ?>' value='<?php echo $data['tax_rate_percent']; ?>' /></td>
                                                                                                </tr>

                                                                                                <?php
                                                                                                $nme++;
                                                                                            }
                                                                                            ?>
                                                                                        </table>
                                                                                        <br>
                                                                                            <span class="error" id="lblcname"></span></td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Save" /></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div></td>
                                                                </tr>

                                                            </table> </form></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
