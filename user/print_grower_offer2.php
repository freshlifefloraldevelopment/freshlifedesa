<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_GET['id_buy'];
    $idfac = $_GET['b'];
    $cajastot = 0;

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $userSessionID . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id , buyer_id , order_number , ADDDATE(del_date, INTERVAL -4 DAY) as lfd
                         from buyer_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Requests
   
   $sqlDetalis="select gor.grower_id , 
                       gor.offer_id  , 
                       gor.offer_id_index,                        
                       gor.product as prod_name,
                       gor.size   ,                        
                       gor.steams , 
                       (gor.price) as salesPriceCli ,
                       gor.status,
                       sum(gor.bunchqty) as bunchqty   , 
                       substr(rg.growers_name,1,19) as name_grower,
                       gor.product_subcategory  , 
                       c.name as name_color ,
                       p.id as idprod       ,
                       ff.name as featurename
                  from grower_offer_reply gor
                 inner join buyer_requests g on gor.offer_id=g.id 
                 inner join buyer_orders bo on bo.id = g.id_order                  
                 inner join growers rg ON gor.grower_id = rg.id                  
                 left  join product p ON gor.product = p.name  and gor.product_subcategory = p.subcate_name
                 left  join colors c ON p.color_id = c.id 
                 left join features ff on g.feature=ff.id                 
                 where g.id_order   = '" . $id_fact_cab . "'
                   and gor.buyer_id = '" . $buyer_cab . "'
                  group by gor.grower_id , 
                           offer_id      ,
                           offer_id_index, 
                           gor.product , 
                           gor.size    ,
                           gor.steams  ,
                           gor.price   ,
                           gor.status  , 
                           gor.product_subcategory  , 
                           c.name ,
                           ff.name
                 order by gor.grower_id,gor.id  ";

        $result   = mysqli_query($con, $sqlDetalis);    
        
        
   // Resumen Request por finca
   
    $sqlResumen="select gor.grower_id ,         gor.offer_id  , 
                        gor.offer_id_index,    gor.product as prod_name,
                        gor.size   ,           gor.steams , 
                        gor.status,        sum(gor.bunchqty) as bunchqty   , 
                        substr(rg.growers_name,1,19) as name_grower,
                        gor.product_subcategory  ,         c.name as name_color ,
                        p.id as idprod       ,        ff.name as featurename    ,
                        gor.price
                   from grower_offer_reply gor
                  inner join buyer_requests g on gor.offer_id=g.id 
                  inner join buyer_orders bo on bo.id = g.id_order                  
                  inner join growers rg ON gor.grower_id = rg.id     
                  left  join product p ON gor.product = p.name  and gor.product_subcategory = p.subcate_name
                  left  join colors c ON p.color_id = c.id 
                  left join features ff on g.feature=ff.id                 
                  where g.id_order    = '" . $id_fact_cab . "'
                    and gor.buyer_id  = '" . $buyer_cab . "'
                   group by gor.grower_id ,
                            gor.product   , 
                            gor.size      ,gor.steams  ,
                            gor.product_subcategory  , 
                            ff.name
                  order by gor.grower_id , gor.product_subcategory , gor.product , gor.size";

        $resumen   = mysqli_query($con, $sqlResumen);            

    $pdf = new PDF();
    
    /*
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'GROWERS OFFERS',0,0,'L'); 
    $pdf->Ln(10);        
    $pdf->SetFont('Arial','B',15);
    $pdf->Cell(70,10,'Client Details ',0,0,'L');
    $pdf->Cell(0,10,'Shipping Details ',0,0,'R');
    $pdf->Ln(10);            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,0,'L');
    $pdf->Cell(0,6,'Invoice #: '.$buyerOrderCab['order_number'],0,1,'R');         
    
    if ($userSessionID != 318) {
       $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');
    }else{
       $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');  
    }
          
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');        
    $pdf->Cell(70,6,'-'.$buy['company'],0,1,'L');      
    $pdf->Ln(10);
    */
    
    ///////////////////////////////////////////////////////////////////////////
/*
    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Stem/Bunch',0,0,'C');
    $pdf->Cell(25,6,'Sale Price',0,0,'C');    
    $pdf->Cell(25,6,'Qty',0,0,'C');
    $pdf->Cell(25,6,'Subtotal',0,0,'C'); 
    $pdf->Cell(25,6,'Status',0,1,'C'); 
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    while($row = mysqli_fetch_assoc($result))  {
        $sel_boxg = "select id_order,offer_id_index,
                            sum(boxqty) as totbox,
                            count(*) as reg
                       from grower_offer_reply gor
                       inner join buyer_requests g on gor.offer_id=g.id 
                       inner join growers rg     ON gor.grower_id = rg.id                  
                       where g.id_order = '" . $id_fact_cab . "'
                         and gor.buyer_id = '" . $buyer_cab . "'
                         and grower_id = '" . $row['grower_id'] . "'
                       group by offer_id , offer_id_index
                       order by gor.grower_id  ";
        $rs_boxg = mysqli_query($con,$sel_boxg);       
          
        $totalr = mysqli_num_rows($rs_boxg);
        
        $cajas = 0;
        while($tot_boxg = mysqli_fetch_array($rs_boxg))  {
                $cajas = $cajas + 1;
        }
        
        
         // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type,subcategoryid from product where id = '" . $row['idprod'] . "' ";
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
        
        $sel_subcatego = "select name from subcategory where id = '" . $bunch_stem['subcategoryid'] . "' ";
        $rs_subcatego = mysqli_query($con,$sel_subcatego);       
        $subc = mysqli_fetch_array($rs_subcatego);        
        
              if ($row['status'] == 0) {
                    $statusOffer = "---------";                     
              }else{
                    $statusOffer = "Confirmed";                   
              }        
                
            
              if ($bunch_stem['box_type'] == 0) {
                    $Subtotal= $row['steams'] * $row['bunchqty'] * $row['salesPriceCli'];
                    $qtyFac = $row['bunchqty']*$row['steams'];
                    $unitFac = "STEMS";                     
              }else{
                    $Subtotal=  $row['bunchqty'] * $row['salesPriceCli'];
                    $qtyFac = $row['bunchqty']; 
                    $unitFac = "BUNCHES";                   
              }        
                
               $subtotalStems= $row['steams'] *$row['bunchqty'] ;      
         
         $subtotalStems= $row['steams'] *$row['bunchqty'] ;         
         if ($row['grower_id'] != $tmp_idorder) {
               $pdf->SetFont('Arial','B',8);                   
               if ($sw == 1) { 
                      $pdf->Cell(70,6,"  ",0,1,'L');                       
                      $pdf->Cell(70,6,"Total Stems Grower : ".$subStemsGrower,0,1,'L');                                 
                      $pdf->Cell(0,6,"Total Grower : $".$subTotalGrower,0,1,'L');                                                       
               }
                      $sw = 1;             
                      $pdf->Cell(70,6,"  ",0,1,'L'); 
                   //   $pdf->Cell(70,6,$row['name_grower']." (".$cajas." "."Boxes".")",0,1,'L');   
                      $pdf->Cell(70,6,$row['name_grower']." (".$cajas." "."Boxes".")",0,1,'L');   
                              $cajastot =  $cajastot + $cajas;
                      $pdf->Cell(70,6,"  ",0,1,'L'); 
                      $subStemsGrower = 0;                      
                      $subTotalGrower = 0;                       
         }

  $pdf->SetFont('Arial','',8);
  
         $pdf->Cell(70,4,$row['prod_name']." ".$row['product_subcategory']." ".$row['size']." cm ".$row['steams']." st/bu ".$row['name_color']." ".$row['featurename'],0,0,'L');            
         $pdf->Cell(25,6,$unitFac,0,0,'C');                              
         $pdf->Cell(25,6,number_format($row['salesPriceCli'], 2, '.', ','),0,0,'C'); 

if ($bunch_stem['box_type'] == 0) {         
         $pdf->Cell(25,6,$row['bunchqty']*$row['steams'],0,0,'C');                                       
}else{
         $pdf->Cell(25,6,$row['bunchqty'],0,0,'C');                                           
}         

         $pdf->Cell(25,6,number_format($Subtotal, 2, '.', ','),0,0,'C'); 
         $pdf->Cell(25,6,$statusOffer,0,1,'C');     
         
            $totalCal = $totalCal + $Subtotal;
            $totalStems = $totalStems + $subtotalStems;            
            $tmp_idorder = $row['grower_id'];
            $subStemsGrower= $subStemsGrower + ($row['steams'] *$row['bunchqty']) ; 
            $subTotalGrower= $subTotalGrower + ($row['steams'] *$row['bunchqty']*$row['salesPriceCli']) ;             
    }

    
                      $pdf->SetFont('Arial','B',8);                   
                      $pdf->Cell(70,6,"  ",0,1,'L');                       
                      $pdf->Cell(70,6,"Total Stems Grower : ".$subStemsGrower,0,1,'L'); 
                      $pdf->Cell(0,6,"Total Grower : $".$subTotalGrower,0,1,'L');                                                                             
    
    $pdf->Ln(2);
    $pdf->SetFont('Arial','B',15);
    $pdf->Cell(70,10,'Contact Details ',0,1,'L');
    
    $pdf->SetFont('Arial','B',10);             
    $pdf->Cell(35,6,'Total Stems:'.number_format($totalStems, 0, '.', ','),0,1,'R');              
    $pdf->Cell(30,6,'Total Boxes:'.$cajastot,0,1,'R');  
    
        $pdf->Ln(20);        

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

*/
        
    
    $tmp_idorder_res = 0;   
    
    $tmp_grow = 0;
    
    while($rowres = mysqli_fetch_assoc($resumen))  {
        
              
         // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type,subcategoryid from product where id = '" . $rowres['idprod'] . "' ";
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
        
        $sel_subcatego = "select name from subcategory where id = '" . $bunch_stem['subcategoryid'] . "' ";
        $rs_subcatego = mysqli_query($con,$sel_subcatego);       
        $subc = mysqli_fetch_array($rs_subcatego);        
                                   
              if ($bunch_stem['box_type'] == 0) {
                    $Subtotal1= $rowres['steams'] * $rowres['bunchqty'] * $rowres['salesPriceCli'];
                    $unitFac = "STEMS";                     
              }else{
                    $Subtotal1=  $rowres['bunchqty'] * $rowres['salesPriceCli'];
                    $unitFac = "BUNCHES";                   
              }        
                
         
         $subtotalStems1= $rowres['steams'] *$rowres['bunchqty'] ;         
         
         if ($rowres['grower_id'] != $tmp_idorder_res) {
             $pdf->AddPage();  
             
    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,$rowres['name_grower'],0,1,'L');     
        $pdf->SetFont('Arial','B',10);    
    
    $pdf->Cell(70,6,$buy['company'],0,1,'L');            
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');        
    $pdf->Cell(70,6,'LFD : '.$buyerOrderCab['lfd'],0,1,'L');        


    $pdf->Ln(10);   
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(90,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Stem/Bunch',0,0,'C');
    $pdf->Cell(25,6,'Qty',0,0,'C');
    $pdf->Cell(25,6,'Price',0,1,'C');
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
             
             
               $pdf->SetFont('Arial','B',10);                   
               if ($sw == 1) { 
                     // $pdf->Cell(70,6,"  ",0,1,'L');                       
                     // $pdf->Cell(70,6,"Total Stems Grower : ".$subStemsGrower1,0,1,'L');                                 
               }
                      $sw = 1;             
                      $pdf->Cell(70,6,"  ",0,1,'L'); 
                      //$pdf->Cell(70,6,$rowres['name_grower'],0,1,'L');   

                      $pdf->Cell(70,6,"  ",0,1,'L'); 
                      $subStemsGrower1 = 0;                      

         }

  $pdf->SetFont('Arial','',8);
  
         $pdf->Cell(90,4,$rowres['product_subcategory']." ".$rowres['prod_name']." ".$rowres['size']." cm ".$rowres['steams']." st/bu ".$rowres['name_color']." ".$rowres['featurename'],0,0,'L');            
         $pdf->Cell(25,6,$unitFac,0,0,'C');                              

            if ($bunch_stem['box_type'] == 0) {         
                     $pdf->Cell(25,6,$rowres['bunchqty']*$rowres['steams'],0,0,'C');                                       
            }else{
                     $pdf->Cell(25,6,$rowres['bunchqty'],0,0,'C');                                           
            }         
                     $pdf->Cell(25,6,number_format($rowres['price'], 2, '.', ','),0,1,'R'); 
            $totalCal1 = $totalCal1 + $Subtotal1;
            $totalStems1 = $totalStems1 + $subtotalStems; 
            
            $tmp_idorder_res = $rowres['grower_id'];
            
            $subStemsGrower1= $subStemsGrower1 + ($rowres['steams'] *$rowres['bunchqty']) ; 
            

    }    
                            
  $pdf->Output();
  ?>