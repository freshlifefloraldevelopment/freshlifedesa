<?php
include "../config/config_new.php";
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}
if (isset($_POST["Submit"]) && $_POST["Submit"] == "Add") {
    $ccode = trim($_POST["c_code"]);
    $cbuyer = trim($_POST["c_buyer"]);
    $ccreatedat = date("Y-m-d H:i:s");
    
	$cc_sql = "SELECT * FROM `checkout_codes` WHERE `cc_code` = '".$ccode."'";
	$cc_res = mysqli_query($con, $cc_sql);
	if(mysqli_num_rows($cc_res) == 0){
		$ins = "INSERT INTO `checkout_codes` (`cc_code`,`cc_buyer_id`, `cc_created_at`) VALUES ('".$ccode."','" . $cbuyer . "', '".$ccreatedat."')";
		mysqli_query($con, $ins);
		header('location:manage_checkout_codes.php');
		
	}
	else{
		$_SESSION['error'] = 'This checkout code already exists, Please try to add some unique code.';
	}
}

$b_sql = "SELECT * FROM buyers ORDER BY first_name";
$b_res=mysqli_query($con,$b_sql);
//var_dump($b_res);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>	
		
		<style>
			button.datepick-trigger{
				width: 100px;
				height: 24px;
			}
		</style>

        <script type="text/javascript">
			function verify()
            {
                var isValid = true;
				
                if (trim(document.form_checkout_codes.c_code.value) == "")
                {
                    document.getElementById("lblcode").innerHTML = "Please enter unique checkout code.";
                    isValid = false;
					
                }
                else
                {
                    document.getElementById("lblcode").innerHTML = "";
                } 
				
				if (trim(document.form_checkout_codes.c_buyer.value) == "")
                {
                    document.getElementById("lblbuyer").innerHTML = "Please select a buyer to assign voucher.";
                    isValid = false;
					
                }
                else
                {
                    document.getElementById("lblbuyer").innerHTML = "";
                }
                
                return isValid;
				
            }

            function trim(str)
            {
                if (str != null)
                {

                    var i;
                    for (i = 0; i < str.length; i++)
                    {

                        if (str.charAt(i) != " ")
                        {

                            str = str.substring(i, str.length);
                            break;

                        }

                    }

                    for (i = str.length - 1; i >= 0; i--)
                    {
                        if (str.charAt(i) != " ")
                        {
                            str = str.substring(0, i + 1);
                            break;
                        }

                    }

                    if (str.charAt(0) == " ")
                    {
                        return "";
                    }

                    else
                    {
                        return str;
                    }

                }

            }

            
        </script>

    </head>

    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php include("includes/header_inner.php"); ?>

            <tr>

                <td height="5"></td>

            </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php include("includes/left.php"); ?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>

                                                                <td height="5"></td>

                                                            </tr>

                                                            <tr>

                                                                <td class="pagetitle">Add Checkout Code</td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <tr>

                                                                <td>

                                                                    <table width="100%">

                                                                        <tr>

                                                                            <td>

                                                                                <a class="pagetitle1" href="manage_checkout_codes.php" onclick="this.blur();"><span> Manage Checkout Codes</span></a>

                                                                            </td>

                                                                        </tr>

                                                                    </table>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>
															
															<tr><td colspan="2">
																
																<?php
																	if($_SESSION['error'] != ''){
																		echo '<p style="color: #f00; padding-bottom:10px;">'.$_SESSION['error'].'</p>';
																		unset($_SESSION['error']);
																	}
																?>
															</td></tr>

                                                            <form name="form_checkout_codes" method="post" onsubmit="return verify();">

                                                                <tr>
                                                                    <td><div id="box">
                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                                                                <tr>
                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fields Marked with (<span class="error">*</span>) are Mandatory</td>
                                                                                </tr>
                                                                                <tr>

                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Checkout Code</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                        <input type="text" class="textfieldbig" name="c_code" id="code" value="" />
                                                                                        <br><span class="error" id="lblcode"></span>	

                                                                                    </td>

                                                                                </tr>    
																				
																				<tr>

                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Select Buyer</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                        <!--<input type="text" class="textfieldbig" name="buyer" id="buyer" value="" />-->
																						<select class="listmenu" name="c_buyer" id="buyer" style="width:230px; height: 22px;">
																							<option value="">-- Select Buyer --</option>
																							<?php 
																								if($b_res){
																								while ($buyer = mysqli_fetch_array($b_res)) {
																							  ?>
																							  <option value="<?= $buyer["id"] ?>"><?= $buyer["first_name"].' '.$buyer["last_name"] ?></option>
																							  <?php
																								}
																							   }
																							?>
																						</select>
                                                                                        <br><span class="error" id="lblbuyer"></span>	

                                                                                    </td>

                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Add" /></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div></td>
                                                                </tr>
                                                            </form>
                                                        </table></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
