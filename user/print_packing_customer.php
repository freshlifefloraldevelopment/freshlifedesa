<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_SESSION["buyer"];
    $idbuy = $_GET['id_buy'];    
    $idfac = $_GET['b'];
    $cajastot = 0;
    
    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $idbuy . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
   
    // Datos de la Orden
   $buyerOrder = "select id_fact          , buyer_id         , order_number, 
                         order_date       , shipping_method  , del_date    , 
                         date_range       , is_pending       , order_serial, 
                         seen             , delivery_dates   , lfd_grower  , 
                         quick_desc       , bill_number      , gross_weight, 
                         volume_weight    , freight_value    , guide_number, 
                         total_boxes      , sub_total_amount , tax_rate    , 
                         shipping_charge  , handling         , grand_total , 
                         bill_state       , date_added       , user_added  ,
                         air_waybill      , charges_due_agent,
                         credit_card_fees , per_kg                         
                         from invoice_orders
                   where buyer_id = '" . $idbuy . "'
                     and id_fact = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];  
   
   ///////////////////////////////////////////////////////////////////////////////////////////////////////
   
   $buyerJoin = "select  id              , buyer_id        , order_number    , order_date      , shipping_method ,
                        del_date        , date_range      , is_pending      , order_serial    , seen            ,
                        delivery_dates  , lfd_grower      , qucik_desc      , type_market     , delivery_day    ,
                        availability    , assigned        , check_ord       , join_order    
                   from buyer_orders
                  where buyer_id = '" . $idbuy . "'
                    and id       = '" . $idfac . "' " ;

   $joinCab = mysqli_query($con, $buyerJoin);
   
   $joinOrder = mysqli_fetch_array($joinCab); 
   
   $idOrder = $joinOrder['join_order'];

   
   // Datos del Packing
   
     //  $con->set_charset("utf8");
      $sqlDetalis="select br.id_order as id_fact     ,
                          br.product                 ,                            
                          gor. product as  prod_name , 
                          gor.product_subcategory as subcate_real   , 
                          gor.size        , 
                          gor.steams                  ,
                          sum(gor.bunchqty-gor.reserve) as  bunchqty,                          
                          gor.reserve         ,   
                          cl.name,                   
                          IFNULL(cl.name, 'color') as colorname,
                          br.id as idbr       , 
                          br.lfd              ,
                          br.feature, 
                          p.id as codvar      ,
                          gor.id as idgor     ,  
                          gor.offer_id        , 
                          g.growers_name , 
                          f.name as featurename   , 
                          res.id_client as control , 
                          res.qty as qty_control  ,
                          IFNULL(res.qty, 0) as qtyres
                     from buyer_requests br
                    inner join grower_offer_reply gor on gor.offer_id = br.id
                     left join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name 
                     left join colors cl ON p.color_id = cl.id                          
                     left join growers g on gor.grower_id = g.id
                     left join features f on br.feature = f.id 
                     left JOIN buyer_requests res ON gor.request_id = res.id and res.comment = 'SubClient-Reques'
                          where br.buyer   = '" . $idbuy . "'
                          and br.id_order  in ('" . $idfac . "' ,'" . $idOrder . "')
                          and g.active     = 'active' 
                          and (gor.bunchqty-gor.reserve) > 0 
                     group by gor.product_subcategory , gor.product , gor.size ,f.name     
                     order by gor.product_subcategory , gor.product";

                              //  and (gor.bunchqty-gor.reserve-IFNULL(res.qty, 0)) > 0 
      
        $result   = mysqli_query($con, $sqlDetalis);    

    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'  AVAILABILITY. ',0,0,'L'); 
    $pdf->Image('vic44.png',170,5); 
    
    $pdf->Ln(10);    
    
    $pdf->SetFont('Arial','B',15);
    $pdf->Ln(5);
            
    $pdf->SetFont('Arial','B',10);
    //$pdf->Cell(70,6,' '.$buy['first_name'],0,1,'L');
    //$pdf->Cell(0,6,'Invoice #: '.$buyerOrderCab['order_number'],0,1,'R');  
       
    
    if ($userSessionID != 318) {
     //  $pdf->Cell(70,6,' '.$buy['last_name'],0,1,'L');
       //$pdf->Cell(0,6,'Total Boxes: '.$buyerOrderCab['total_boxes'],0,1,'R');     
    }else{
      // $pdf->Cell(70,6,' '.$buy['last_name'],0,1,'L');  
    }
      
   
    
    $pdf->Ln(8);
    $pdf->Cell(120,6,'Variety',0,0,'L');
    $pdf->Cell(20,6,'Bunch',0,1,'C');

    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',10);
    
    $tmp_idorder = 0;
    
    $tmp_subca = "";
    
    while($row = mysqli_fetch_assoc($result))  {
        
             if ($tmp_subca != $row['subcate_real']) {

                    $pdf->Cell(20,6,"------------",0,1,'C');    
              }
                                       
         // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type , subcategoryid 
                       from product 
                      where name         = '" . $row['prod_name'] . "' 
                        and subcate_name = '" . $row['subcate_real'] . "' ";        
        
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
            
              if ($bunch_stem['box_type'] == 0) {
                    $qtyFac = $row['bunchqty'];                   
                    $unitFac = "STEMS";                     
              }else{
                    $qtyFac = $row['bunchqty']; 
                    $unitFac = "BUNCHES";                   
              }                        

        $sel_subcatego = "select name from subcategory where id = '" . $bunch_stem['subcategoryid'] . "' ";
        $rs_subcatego = mysqli_query($con,$sel_subcatego);       
        $subc = mysqli_fetch_array($rs_subcatego);              
                         
        $subtotalStems= $row['bunchqty'] ;      

        $variety= $row['subcate_real']." ".$row['prod_name']." ".$row['size']." cm ".$row['steams']." st/bu  ".$row['featurename']." ".$row['colorname'];
        
       // $varietyUpper = strtoupper($variety);        
          $varietyUpper = ($variety);        

  //$pdf->SetFont('Arial','',8);

         if ($subc['name'] == 'Snapdragon'||$subc['name'] == 'Alstroemeria'||$subc['name'] == 'Carnations'||$subc['name'] == 'Mini Carnations'||$subc['name'] == 'Limonium') { 
               
                    if ($subc['name'] == 'Limonium') { 
                        $pdf->Cell(120,6,$row['subcate_real']." ".$row['prod_name']." ".$row['size']." cm ".$row['featurename']." ".$row['colorname'],0,0,'L');            
                    }else{
                        $pdf->Cell(120,6,$row['subcate_real']." ".$row['prod_name']." ".$row['steams']." st/bu  ".$row['featurename']." ".$row['colorname'],0,0,'L');            
                    }
                                

         }else{
             if ($row['prod_name'] == "Pink Mondial" ||$row['prod_name'] == "Red Spray") {
                   $pdf->Cell(120,6,$varietyUpper,0,0,'L');                             
             }else{
                   $pdf->Cell(120,6,$variety,0,0,'L');            
             }

         }         
         
                           
         $pdf->Cell(20,6,$row['bunchqty'],0,1,'C');    
         
         
            $tmp_subca = $row['subcate_real'] ;
            
            $totalStems = $totalStems + $subtotalStems;            
            $tmp_idorder = $row['idcli'];
            $subStemsGrower= $subStemsGrower + ($row['bunchqty']) ;             
    }
            
            $pdf->SetFont('Arial','B',8);                   
            $pdf->Cell(70,6,"  ",0,1,'L');                       
            $pdf->Cell(70,6,"Total Bunches: ".$subStemsGrower,0,1,'L');  
    
            $pdf->Ln(2);
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Contact ',0,1,'L');   
            $pdf->Cell(70,6,'Email:annie@victoriasblossom.ca',0,1,'L');   
    
  $pdf->Output();
  ?>