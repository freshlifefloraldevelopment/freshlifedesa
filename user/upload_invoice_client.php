<?php

include("../config/config_gcp.php");

$fact_num = $_GET['id_fact'];
$buyerid  = $_GET['id'];
$date     = date('Y-m-d H:i:s');

$date1    = date('d-m-Y');
$contador = 0;

$IdMaximo = 1;

// Cabecera de Facturas

             $qryMax="select (max(id_fact)+1) as id from invoice_orders_subcli";
	      $dataMaximo = mysqli_query($con, $qryMax);
			while ($dt = mysqli_fetch_assoc($dataMaximo)) {
                                $IdMaximo= $dt['id'];
			}
                        
$sel_info = "select * from buyers where id='" . $buyerid . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info); 



$order_number = $info['customer_code'].$date1;

$bill = $info['customer_code'].$IdMaximo;

$query_customer = "select ipb.cliente_id ,
                          cl.name as name_client ,
                          count(*) lineas, 
                          sum(ipb.qty_pack) bunches,
                          sum(ipb.price_cad*ipb.qty_pack) total
                     from invoice_packing_box ipb
                    INNER JOIN sub_client cl ON cl.id = ipb.cliente_id
                    where id_fact in (select id from buyer_orders where availability = 1)
                      and cliente_id not in ( 0,47)
                      and processed = 'N'
                    group by ipb.cliente_id,cl.name
                    order by cl.name  ";

    $fact_customer    = mysqli_query($con, $query_customer);
    

        while($inv_customer = mysqli_fetch_array($fact_customer))  {            
                
            //$sel_order = "select order_number  from buyer_orders  where id = '" . $fact_num . "' ";
            //$rs_order = mysqli_query($con,$sel_order);       
            //$border = mysqli_fetch_array($rs_order); 
            
            $contador = $contador + 1;
            
            
        
            $set_tts = "insert into invoice_orders_subcli set                 
                                    id_fact          = '".$IdMaximo."',  
                                    buyer_id         = '".$buyerid."',  
                                    id_client        = '".$inv_customer['cliente_id']."',  
                                    order_number     = '".$order_number."',  
                                    order_date       = '".$date."',  
                                    shipping_method  = '131'  ,
                                    date_order       = '".$date1."',   
                                    date_range       = '".$date1."',                                               
                                    is_pending       = '0',   
                                    order_serial     = '1',   
                                    description      = 'SUBCLIENT-PREINVOICE',   
                                    bill_number      = '".$bill."',  
                                    gross_weight     = '0',    
                                    volume_weight    = '0',    
                                    freight_value    = '0',                                        
                                    sub_total_amount = '".$inv_customer['total']."',    
                                    tax_rate         = '0',    
                                    shipping_charge  = '0',    
                                    handling         = '0',                                        
                                    grand_total      = '".$inv_customer['total']."',    
                                    bill_state       = 'P',                                            
                                    date_added       = '".$date."',    
                                    user_added       = 'PREFACT' ,                                    
                                    tot_reg          = '".$inv_customer['lineas']."',    
                                    tot_bunches      = '".$inv_customer['bunches']."' ";

                    mysqli_query($con,$set_tts);
                   
          
    // Detalle de  Facturas
  
$query_cli_det = "select cl.name as name_client , cl.id , cl.buyer as idbuyer,
                         det.id_fact         , det.id_order  , det.order_number , det.order_serial , det.offer_id     , 
                         det.product         , det.prod_name , det.buyer        , det.grower_id    , det.box_qty_pack , det.qty_pack  , 
                         det.qty_box_packing , det.box_type  , det.comment      , det.date_added   , det.box_name     , det.size      , 
                         det.steams          , det.price     , det.cliente_id   , det.ship_cost    , det.cost_cad     , det.price_cad , 
                         det.gorPrice        , det.duties    , det.handling_pro , det.total_duties , det.branch       ,
                         det.product_subcategory
                    from invoice_packing_box det
                   INNER JOIN sub_client cl ON cl.id = det.cliente_id
                   where id_fact in (select id from buyer_orders where availability = 1)
                     and cliente_id = '".$inv_customer['cliente_id']."'
                     and processed = 'N' ";

$fact_cli_det = mysqli_query($con, $query_cli_det);    
    
  
      
      while($invoice_det = mysqli_fetch_array($fact_cli_det))  {
                    
            $query_pack = "select offer_id from invoice_packing where id = '".$invoice_det['id_order']."'  ";
            $rs_pack = mysqli_query($con, $query_pack);   
            $offer_pack = mysqli_fetch_array($rs_pack);
         
            $query_price = "select br.product,p.id,p.name,p.categoryid, p.subcategoryid, sc.name,
                                   sc.price_client
                              from buyer_requests br
                              INNER JOIN product AS p ON br.product = p.id
                              INNER JOIN subcategory AS sc ON (p.subcategoryid = sc.id and p.categoryid = sc.cat_id)
                              where br.id = '".$offer_pack['offer_id']."'  ";
         
            $rs_price = mysqli_query($con, $query_price);   
            $price_quick = mysqli_fetch_array($rs_price);         
          
                             
            $set_det = "insert into invoice_requests_subcli set                 
                                id_fact         = '".$IdMaximo."',  
                                id_order        = '".$invoice_det['id_order']."',
                                order_number    = '".$order_number."',   
                                order_serial    = '1',   
                                offer_id        = '".$invoice_det['offer_id']."',
                                product         = '".$invoice_det['product']."',
                                prod_name       = '".$invoice_det['prod_name']."',
                                buyer           = '".$invoice_det['buyer']."',
                                grower_id       = '".$invoice_det['grower_id']."',
                                box_qty_pack    = '".$invoice_det['box_qty_pack']."',
                                qty_pack        = '".$invoice_det['qty_pack']."',
                                qty_box_packing = '".$invoice_det['qty_box_packing']."',
                                box_type        = '".$invoice_det['box_type']."',
                                comment         = '".$offer_pack['offer_id']."',   
                                date_added      = '".$date."',  
                                box_name        = '".$invoice_det['box_name']."',
                                size            = '".$invoice_det['size']."',
                                steams          = '".$invoice_det['steams']."',
                                price           = '".$invoice_det['price']."',
                                cliente_id      = '".$invoice_det['cliente_id']."',
                                ship_cost       = '".$invoice_det['ship_cost']."',
                                cost_cad        = '".$invoice_det['cost_cad']."',
                                price_cad       = '".$invoice_det['price_cad']."',
                                gorPrice        = '".$invoice_det['gorPrice']."',
                                duties          = '".$invoice_det['duties']."',
                                handling_pro    = '".$invoice_det['handling_pro']."',
                                total_duties    = '".$invoice_det['total_duties']."' ,
                                product_subcategory    = '".$invoice_det['product_subcategory']."' ,    
                                price_quick     = '".$price_quick['price_client']."',    
                                fact_quick      = '".$contador."',    
                                branch          = '".$invoice_det['branch']."'   " ;

                    mysqli_query($con,$set_det);                      
          
        }  
        
        
      }
      
      
   /*    $update_tts = "update invoice_requests 
                         set inventary ='1' 
                       where id_fact  = '" . $fact_num .     "' 
                         and inventary = '0'  ";

                mysqli_query($con,$update_tts); 
     */           
header('location:buyer_mgmt.php')
?> 