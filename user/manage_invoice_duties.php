<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";
        
    $fact_number  = $_GET['id_fact'];
    
	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}

        /*
	  $sel_info="select ir.id_fact            , ir.id_order       , ir.order_serial  , 
                            ir.cod_order          , ir.product        , ir.sizeid        , 
                            ir.qty                , ir.buyer          , ir.date_added    , 
                            ir.bunches            , ir.box_name       , ir.lfd           , 
                            ir.comment            , ir.box_id         , ir.shpping_method, 
                            ir.mreject            , ir.bunch_size     , ir.unseen        , 
                            ir.inventary          , ir.offer_id       , ir.prod_name     , 
                            ir.product_subcategory, ir.size           , ir.boxtype       , 
                            ir.bunchsize          , ir.boxqty         , ir.bunchqty      , 
                            ir.steams             , ir.gorPrice       , ir.box_weight    , 
                            ir.box_volumn         , ir.grower_box_name, ir.reject        , 
                            ir.reason             , ir.coordination   , ir.cargo         , 
                            ir.color_id           , ir.gprice         , ir.tax           , 
                            ir.cost_ship          , round(ir.handling,0) as handling     , 
                            ir.grower_id          , ir.offer_id_index,
                            substr(rg.growers_name,1,19) as name_grower ,ir.salesPriceCli, 
                            ir.price_Flf,
                            ir.ship_cost , ir.cost_cad , ir.price_cad,
                            duties       , handling_pro , total_duties , price_cad_duties
                       from invoice_requests ir
                      INNER JOIN growers rg ON ir.grower_id = rg.id                  
                      where id_fact  = '" . $fact_number . "'
                      order by ir.grower_id ";          
          */
          
        
	  $sel_info="select g.growers_name as name_grower, 
                            gr.product_subcategory ,			 
                            gr.product as prod_name , 					 
                            ipx.size ,					 
                            ipx.steams ,					 
                            f.name as features , 
                            ipx.qty_pack as bunchqty,
                            gr.price as gorPrice , 
                            ipx.duties , 					 
                            ipx.ship_cost,					 
                            ipx.handling_pro,  
                            ipx.price_cad   ,
                            ipx.box_packing,  
                            ipx.truck   ,                            
                            p.box_type  ,
                            ipx.total_duties
                from grower_offer_reply gr 
               inner join buyer_requests br  on gr.offer_id     = br.id 
               inner join product p          on gr.product      = p.name and gr.product_subcategory = p.subcate_name
                left join subcategory s      on p.subcategoryid = s.id
               inner join growers g          on gr.grower_id    = g.id 
               inner join invoice_packing ip on (gr.grower_id = ip.grower_id and gr.id = ip.id_grower_offer )
               inner join invoice_packing_box ipx  on ip.id = ipx.id_order               
                left join sub_client scl           on ipx.cliente_id = scl.id
                left join sub_client_branch bra    on ipx.branch = bra.id
                left join features f               on br.feature = f.id                
               where gr.buyer_id='318'
                 and gr.offer_id >= '6731'
                 and ip.id_fact = '" . $fact_number . "'
                 and reject in (0) 
               group by ipx.id ";        
        
	$rs_info=mysqli_query($con,$sel_info);
        
	if(isset($_GET['delete'])) 
	{
	  $query = 'DELETE FROM invoice_requests WHERE offer_id = '.(int)$_GET['delete'];
	  mysqli_query($con,$query);
          //header('location:list_mgmt.php?id_fact='.$fact_number.'&id_grow='.$growerid);              
          //     inner join buyer_orders bo    on br.id_order     = bo.id 
          //     inner join invoice_packing ip on (br.id_order = ip.id_fact and gr.grower_id = ip.grower_id and gr.id = ip.id_grower_offer )
	}        

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
			} );
</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
  <?php include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <?php include("includes/agent-left.php");?>
          <td width="5">&nbsp;</td>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="5"></td>
                          </tr>
                          <tr>
                            <td class="pagetitle">Shipping Cost and Duties </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><table width="100%">
                                <tr>
                                  <td>Shipping</td>
                                </tr>
                              </table></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><div id="box">
                                <div id="container">
                                  <div class="demo_jui">
                                    <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                      <thead>
                                        <tr>
                                          <th>Variety</th>
                                          <th>Stems</th>                                          
                                          <th>Farm Price</th>
                                          <th>Duties</th>
                                          <th>Ship.Cost</th>                                          
                                          <th>Hand.</th> 
                                          <th>Box Packing</th>                                          
                                          <th>Freight Truck</th>                                           
                                          <th>Price </th>                                                                                       
                                          <th>Price CAD</th>                                             
                                          
                                          <!--th>CAD Old</th-->                                                                                                                              
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php	  $sr=1;
                                        
                                                  $stemsGen = 0;
                                                  $gorPriceGen = 0;
                                                  
                                                  $dutiesGen       = 0;
                                                  $ship_costGen    = 0;
                                                  $handling_proGen = 0;
                                                  $box_packingGen  = 0;
                                                  $truckGen        = 0;
                                                  $price_Gen       = 0;                                                  
                                                  $price_cadGen    = 0;                                                  
                                                  
						  while($row=mysqli_fetch_array($rs_info)) {
					?>
                                          
                                        <tr class="gradeU">
						<td>
                                                        <?php

                                                                   
                            if ($row['box_type'] == 0) {
                                    $totalStems = $row['steams'] * $row['bunchqty'] ;                                       
                            }else{
                                    $totalStems = $row['steams']  ;
                            }    
                            
                                                                                                                                                                                                                                                                                                 
                                                           
                                                        ?>
                                                            <strong><?php echo $row['name_grower'] ?></strong>                                                                                                     
						  	    <div><?php echo $row['prod_name']." ".$row['size']." cm"." ".$row['steams']." st/bu"; ?></div>
							    <small><?php echo $row['product_subcategory'] ?></small>
						 </td> 


										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo $row['steams'] * $row['bunchqty'] ?>
                                                                                    </td>
                                            
										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($row['gorPrice'], 4, '.', ',') ; ?>
                                                                                    </td>
                                            
										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($row['duties'], 4, '.', ',') ; ?>
                                                                                    </td>                                                                                              
                                            
                                            
										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($row['ship_cost'], 4, '.', ',') ; ?>
                                                                                    </td>                                            

										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($row['handling_pro'], 4, '.', ',') ; ?>
                                                                                    </td> 

										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($row['box_packing'], 4, '.', ',') ; ?>
                                                                                    </td> 

										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($row['truck'], 4, '.', ',') ; ?>
                                                                                    </td>                                             
                                            
										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($row['total_duties'], 4, '.', ',') ; ?>
                                                                                    </td>                                                                                         

										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($row['price_cad'], 4, '.', ',') ; ?>
                                                                                    </td>                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                                                                                                
                                        </tr>
                                          
                                        <?php
                                                $stemsGen        = $stemsGen    + ($row['steams'] * $row['bunchqty']);
                                                
                                                $gorPriceGen     = $gorPriceGen     + ($row['gorPrice']*$totalStems);                                                
                                                $dutiesGen       = $dutiesGen       + ($row['duties']*$totalStems);
                                                $ship_costGen    = $ship_costGen    + ($row['ship_cost']*$totalStems);
                                                $handling_proGen = $handling_proGen + ($row['handling_pro']*$totalStems);
                                                
                                                $box_packingGen  = $box_packingGen + ($row['box_packing']*($row['steams'] * $row['bunchqty']));
                                                $truckGen        = $truckGen + ($row['truck']*($row['steams'] * $row['bunchqty']));
                                                
                                                $price_Gen       = $price_Gen     + ($row['total_duties']*$totalStems);                                                
                                                $price_cadGen    = $price_cadGen  + ($row['price_cad']*$totalStems);
                                                
                                                
                                        
						$sr++;
						}
					 ?>
										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo "TOTALES :";?>
                                                                                    </td>
                                          
										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo $stemsGen;?>
                                                                                    </td>
                                            
										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($gorPriceGen, 2, '.', ',') ; ?>
                                                                                    </td>
                                            
										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($dutiesGen, 2, '.', ',') ; ?>
                                                                                    </td>                                                                                              
                                            
                                            
										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($ship_costGen, 2, '.', ',') ; ?>
                                                                                    </td>                                            

										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($handling_proGen, 2, '.', ',') ; ?>
                                                                                    </td> 

										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($box_packingGen, 0, '.', ',') ; ?>
                                                                                    </td> 

										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($truckGen, 0, '.', ',') ; ?>
                                                                                    </td>                                             

										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($price_Gen, 2, '.', ',') ; ?>
                                                                                    </td>                                          
                                          
                                          
										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($price_cadGen, 2, '.', ',') ; ?>
                                                                                    </td>
                                                                                                                                          
                                      </tbody>
                                    </table>
                                      
                                                                                                                                      
                                  </div>
                                </div>
                              </div></td>
                          </tr>
                        </table>
                          
                      </td>
                      <td width="10">&nbsp;</td>
                    </tr>
                    </table>



                    
                </td>
                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
              </tr>
                  
                  
              <tr>
                <td background="images/middle-leftline.gif"></td>
                <td>&nbsp;</td>
                <td background="images/middle-rightline.gif"></td>
              </tr>
              <tr>
                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                <td background="images/middle-bottomline.gif"></td>
                <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
              </tr>

                  
                  
            </table>
              

        </tr>
            
            
      </table>
        

          </td>        

  </tr>
    

    
  <tr>
    <td height="10"></td>
  </tr>
    
  <?php include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
    

</table>

    
</body>
</html>
