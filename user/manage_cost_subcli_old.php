<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";

	session_start();

        $fact_number = $_GET['id_fact'];
        $offer_id    = $_GET['id_ord'];
  
	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
	{
		header("location: index.php");
	}

	if(isset($_GET['delete'])) 
	{
	  $query = 'DELETE FROM invoice_packing_cab WHERE  id= '.(int)$_GET['delete'];
	  mysqli_query($con,$query);
	}
	$qsel="select * from invoice_packing_cab where id_fact = '".$fact_number."' order by comment ";
	$rs=mysqli_query($con,$qsel);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					//"sScrollXInner": "130%",

					"bJQueryUI": true,

					//"sScrollY": "536",

					"sPaginationType": "full_numbers"

				});

			} );

</script>
</head>
<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
  <?php include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <?php include("includes/agent-left.php");?>
          <td width="5">&nbsp;</td>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="5"></td>
                          </tr>
                          <tr>
                            <td class="pagetitle">Cost Analisis</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><div id="box">
                                <div id="container">
                                  <div class="demo_jui">
                                    <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                      <thead>
                                        <tr>
                                          <th width="4%" align="center">Sr.</th>
                                          <th width="25%" align="left">Grower</th>                                          
                                          <th width="8%" align="left">Piece</th>
                                          <th width="10%" align="left">Weight</th>
                                          <th width="10%" align="left">Box Weight</th>
                                          <th width="10%" align="left">% Dist</th>
                                          <th width="10%" align="left">% Cost</th>
                                          <th width="10%" align="left">Price Box</th>
                                          <th width="10%" align="left">Stems</th>
                                          <th width="10%" align="left">Gyp</th>
                                          <th width="10%" align="left">Price st/bu</th>                                                                                         
                                          <th width="5%" align="left">Edit</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php
						  	$sr=1;
						  while($pack=mysqli_fetch_array($rs))

						  {
						     $sel_category="select * from country where id='".$pack["cat_id"]."'";
							 $rs_category=mysqli_query($con,$sel_category);
							 $category=mysqli_fetch_array($rs_category);

						  ?>
                                        <tr class="gradeU">
                                          <td align="center" class="text"><?php echo $sr;?></td>
                                          <td class="text" align="left"><?php echo $pack["comment"]?></td>                                          
                                          <td class="text" align="left"><?php echo $pack["Piezas"]?></td> 
                                          <td class="text" align="left"><?php echo $pack["Peso"]?></td> 
                                          <td class="text" align="left"><?php echo $pack["box_weight"]?></td> 
                                          <td class="text" align="left"><?php echo $pack["Dist"]?></td> 
                                          <td class="text" align="left"><?php echo $pack["Cost"]?></td> 
                                          <td class="text" align="left"><?php echo $pack["price_per_box"]?></td> 
                                          <td class="text" align="left"><?php echo $pack["Stems"]?></td> 
                                          <td class="text" align="left"><?php echo $pack["gyp"]?></td> 
                                          <td class="text" align="left"><?php echo $pack["price_st_bun"]?></td>                                          
                                          
                                          <td align="center" ><a href="update_costclie_edit.php?id_cab=<?php echo $pack["id"]."&id_fact=".$pack["id_fact"] ?> "><img src="images/edit.gif" border="0" alt="Edit" /></a></td>
                                        </tr>
                                        <?php

						 		$sr++;

						 	}

						 ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div></td>
                          </tr>
                        </table></td>
                      <td width="10">&nbsp;</td>
                    </tr>
                  </table></td>
                
                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
              </tr>
              <tr>
                <td background="images/middle-leftline.gif"></td>
                <td>&nbsp;</td>
                <td background="images/middle-rightline.gif"></td>
              </tr>
              <tr>
                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                <td background="images/middle-bottomline.gif"></td>
                <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <?php include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
