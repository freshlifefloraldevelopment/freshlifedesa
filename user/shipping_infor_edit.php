<?php

	include "../config/config_gcp.php";

	session_start();

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
		header("location: index.php");
	}
        
      $bb=$_GET["id"];
      
	if(isset($_POST["Submit"]) && $_POST["Submit"]=="Save")	{			

	    $ins="update cargo_agency set 
                         name         = '".$_POST["name"]."',
                         contact_name = '".$_POST["contact_name"]."' ,  
                         phone        = '".$_POST["phone"]."'   ,  
                         coordination = '".$_POST["coordination"]."'         
                   where id='".$_GET["id"]."'";         

		mysqli_query($con,$ins);
                
		//header('location:shipping_information.php?id_ca=8');
		header('location:shipping_information.php?id_ca='.$bb);                
	}


	  $sel_ship="select id, name, logo, contact_name, phone, coordination
                       from cargo_agency 
                      where id='".$_GET["id"]."'";                       

	$rs_ship=mysqli_query($con, $sel_ship);

	$shipping=mysqli_fetch_array($rs_ship);               
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript">

	function verify(){ 

		var arrTmp=new Array();

		arrTmp[0]=checkname();
		arrTmp[1]=checkcontact_name();                
		arrTmp[2]=checkphone();                                
		arrTmp[3]=checkcoordination();                                                

		var i;
		_blk=true;

		for(i=0;i<arrTmp.length;i++){

			if(arrTmp[i]==false){
			   _blk=false;
			}
		}

		if(_blk==true)	{
			return true;
		}else{
			return false;
		}
 	}	
	
	function trim(str) {    

		if (str != null) {        
			var i;        

			for (i=0; i<str.length; i++) {           

				if (str.charAt(i)!=" ") {               
					str=str.substring(i,str.length);                 
					break;            
				}        
			}            

			for (i=str.length-1; i>=0; i--)	{            

				if (str.charAt(i)!=" ") {                
					str=str.substring(0,i+1);                
					break;            
				}         
			}                 

			if (str.charAt(0)==" ") {            
				return "";         
			} else {            
				return str;         
			}    
		}
	}

	function checkname(){

		if(trim(document.frmcat.name.value) == "")	{	 

			document.getElementById("lblname").innerHTML="Please enter Cargo Agency";
			return false;
		}else 	{

			document.getElementById("lblname").innerHTML="";
			return true;
		}
	}
        
	function checkcontact_name(){

		if(trim(document.frmcat.contact_name.value) == "")	{	 

			document.getElementById("lblcontact_name").innerHTML="Please enter Contact Person";
			return false;
		}else 	{

			document.getElementById("lblcontact_name").innerHTML="";
			return true;
		}
	}        
              
	function checkphone(){

		if(trim(document.frmcat.phone.value) == "")	{	 

			document.getElementById("lblphone").innerHTML="Please enter Phone";
			return false;
		}else 	{

			document.getElementById("lblphone").innerHTML="";
			return true;
		}
	}                        
        
	function checkcoordination(){

		if(trim(document.frmcat.coordination.value) == "")	{	 

			document.getElementById("lblcoordination").innerHTML="Please enter Coordination";
			return false;
		}else 	{

			document.getElementById("lblcoordination").innerHTML="";
			return true;
		}
	}                        
	
</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
<?php include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>

  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
       <?php include("includes/shipping-left.php");?>
        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10" valign="top" background="images/images_front/middle-leftline.gif"><img src="images/images_front/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/images_front/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10">&nbsp;</td>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="5"></td>
                  </tr>

                  <tr>
                    <td class="pagetitle">Edit Shipping Information</td>
                  </tr>

                  <tr>
                    <td>&nbsp;</td>
                  </tr>

		  <tr>
                                <td>
					<table width="100%">
					<tr>
					<td>
					<a class="pagetitle1" href="shipping_information.php" onclick="this.blur();"><span>Shipping</span></a>
					</td>
					</tr>
					</table>
				</td>
                  </tr>
                        
                  <tr>
                    <td>&nbsp;</td>
                  </tr>

        <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data" action="shipping_infor_edit.php?id=<?php echo $_GET["id"]?>">

          <tr>
            <td><div id="box">
              <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
		  <tr>                        
                          <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory                </td>
                 </tr>

                                                <!-- Name -->
		    <tr>
                          <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Cargo Agency :</td>
				<td width="66%" bgcolor="#f2f2f2">
				<input type="text" class="textfieldbig" name="name" id="name" value="<?php echo $shipping["name"]?>" />
				<br><span class="error" id="lblname"></span>	
                        </td>
                    </tr> 
                                                
                                                <!-- contact_name -->
		    <tr>
                          <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Contact :</td>
				<td width="66%" bgcolor="#f2f2f2">
				<input type="text" class="textfieldbig" name="contact_name" id="contact_name" value="<?php echo $shipping["contact_name"]?>" />
				<br><span class="error" id="lblcontact_name"></span>	
                        </td>
                    </tr>                                                 

                                                <!-- Phone -->
		    <tr>
                          <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Phone :</td>
				<td width="66%" bgcolor="#f2f2f2">
				<input type="text" class="textfieldbig" name="phone" id="phone" value="<?php echo $shipping["phone"]?>" />
				<br><span class="error" id="lblphone"></span>	
                        </td>
                    </tr>                                                                                                                                                                                                 
                                                <!-- Coordination -->
		    <tr>
                          <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Coordination :</td>
				<td width="66%" bgcolor="#f2f2f2">
				<input type="text" class="textfieldbig" name="coordination" id="coordination" value="<?php echo $shipping["coordination"]?>" />
				<br><span class="error" id="lblcoordination"></span>	
                        </td>
                    </tr>                                                                                                 
                                                

                  
                <tr>
                  <td>&nbsp;</td>
                  <td><input name="Submit" type="Submit" class="buttongrey" value="Save" />                  </td>
                </tr>

              </table>

            </div></td>

          </tr>

		  </form>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/images_front/middle-rightline.gif"><img src="images/images_front/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>
            <td background="images/images_front/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/images_front/middle-rightline.gif"></td>
          </tr>

          <tr>
            <td height="10"><img src="images/images_front/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/images_front/middle-bottomline.gif"></td>
            <td><img src="images/images_front/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>

        </table></td>

      </tr>

    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>
    
</table>
</body>
</html>