<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $idbuy = $_GET['id_buy'];    
    
    $id_order = $_GET['b'];    


     // Datos del Buyer orders
    $buyerOrder = "select *
                      from buyer_orders  
                     where id = '" . $id_order . "'  "     ;
     
    $buyerO = mysqli_query($con, $buyerOrder);
    $buyOrder = mysqli_fetch_array($buyerO);   
    
    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $idbuy . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
      
   
   // Datos de la Orden  DOH
   
   $sqlDetalis="select unseen,
                       br.id as idreq,
                       bo.qucik_desc,
                       b.first_name ,   
                       CAST(br.id_order AS UNSIGNED) idorder,
                       br.cod_order , 
                       br.product,
                       p.name , 
                       br.qty ,  
                       br.lfd ,
                       sizeid ,
                       sz.name as size_name,
                       g.growers_name   ,
                       s.name as subcate , s.id as idsub , p.id as idprod
                  from buyer_requests br
                 INNER JOIN product p       ON br.product = p.id
                 INNER JOIN buyers b        ON br.buyer = b.id
                 INNER JOIN buyer_orders bo ON br.id_order = bo.id
                 INNER JOIN sizes sz        ON br.sizeid = sz.id  
                  left join subcategory s   on p.subcategoryid=s.id
                 left JOIN growers g     ON br.id_grower = g.id                                                  
                 where br.buyer = '" . $idbuy . "' 
                   and br.id_order = '" . $id_order . "' 
                   and br.type = 0
                 order by s.name,p.name ";

        $result   = mysqli_query($con, $sqlDetalis);            

    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'STANDING ORDER ',0,0,'L'); 
    $pdf->Image('logo.png',148,5); 
    $pdf->Ln(8);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'Company Name: '.$buy['first_name'],0,1,'L');           
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');           
    $pdf->Cell(70,6,'Delivery Day: '.$buyOrder['delivery_day'],0,1,'L');               
    
    $pdf->Ln(10);    
    
    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Bunch/Stems',0,0,'L');    
    $pdf->Cell(25,6,'Qty',0,1,'R');
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = '0';
        
    
    while($row = mysqli_fetch_assoc($result))  {
        
         if ($row['subcate'] != $tmp_idorder) {
                $pdf->Ln(4); 
               $pdf->SetFont('Arial','B',10);                   
               $pdf->Cell(70,6,$row['subcate'],0,1,'L');
               $pdf->Ln(2);
         }        
                                          
         $pdf->SetFont('Arial','B',8);
         
         // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type,subcategoryid from product where id = '" . $row['idprod'] . "' ";
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
        
              if ($bunch_stem['box_type'] == 0) {
                    $unitFac = "Stems";                     
              }else{
                    $unitFac = "Bunch";                   
              }                
         
  
         $pdf->Cell(70,4,$row['name']." ".$row['size_name']." cm. ",0,0,'L');            
         $pdf->Cell(25,6,$unitFac,0,0,'L');                                                                                   
         $pdf->Cell(25,6,$row['qty'],0,1,'R');                                                                          
          
         
         $tmp_idorder = $row['subcate'];
    }
                   
  $pdf->Output();
  ?>