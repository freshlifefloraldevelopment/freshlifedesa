<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
   
    $idbuy = $_POST['codbuy'];    
      
    /////////////////////////////////////////////////////////////////////////
    
    $userSessionID = $_POST['codgrow'];
        
    for ($i=0;$i<count($userSessionID);$i++)    {     
        
            $pedido .= $userSessionID[$i].",";
                       
    } 
                $pedido.="0";
    ////////////////////////////////////////////////////////////////////////
                   
    $userProductID = $_POST['codprod'];
    
    for ($i=0;$i<count($userProductID);$i++)    {     
        
            $variedad .= $userProductID[$i].",";
                       
    } 
                $variedad.="0";                
                
    ////////////////////////////////////////////////////////////////////////                


    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $idbuy . "'
                       and c.id = b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy   = mysqli_fetch_array($buyer);
    
    
    ////////////////////////////////////////////////////////////////////////                
    
        // Datos del Producto
    $sql_var = "select id , name
                      from product 
                     where id in (" . $variedad . ")  
                       and id != 0   " ;
     
    $rs_var = mysqli_query($con, $sql_var);    

    ////////////////////////////////////////////////////////////////////////                    
      
   
   // Datos de la Orden  DOH
   
   $sqlDetalis="select unseen,
                       br.id as idreq,
                       b.first_name ,   
                       CAST(br.id_order AS UNSIGNED) idorder,
                       br.cod_order , 
                       br.product,
                       p.name , 
                       p.box_type ,                        
                       br.qty ,  
                       br.lfd ,
                       sizeid ,
                       sz.name as size_name,
                       g.growers_name      ,
                       c.name as colorname ,
                       sc.name as subcate  ,
                       br.type ,
                       br.id_order
                  from buyer_requests br
                 INNER JOIN product p     ON br.product = p.id
                 INNER JOIN subcategory sc ON p.subcategoryid = sc.id
                 INNER JOIN buyers b     ON br.buyer = b.id
                 INNER JOIN sizes sz ON br.sizeid = sz.id  
                  LEFT JOIN colors c ON p.color_id = c.id
                  left JOIN growers g     ON br.id_grower = g.id                                                  
                 where br.buyer    = '" . $idbuy . "'
                   and br.id_order in (" . $pedido . ")
                   and br.product in (" . $variedad . ")
                 order by p.name ";

        $result   = mysqli_query($con, $sqlDetalis);    
        
    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'  REQUEST VS PRICE (Details)',0,0,'L'); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,1,'L');           
    $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');           
    $pdf->Cell(70,6,'-'.$buy['company'],0,1,'L');  
    $pdf->Cell(40,6,'Request..:  '.$pedido,0,1,'L');  
        
        $pdf->Ln(4);
        
     while($varId = mysqli_fetch_assoc($rs_var))  {
             $pdf->Cell(40,6,$varId['name'],0,1,'L');  
     }
    
    $pdf->Ln(10);
    $cabCount = 1;  
    
// Cabecera
    
    $pdf->Cell(15,6,'',0,0,'L');
    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Order',0,0,'L');
    $pdf->Cell(25,6,'Qty',0,0,'R');    
    $pdf->Cell(8,6,' ',0,0,'L');            
    $pdf->Cell(25,6,'Lfd',0,1,'L');        
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    $totrequest = 0;
    $totoffer   = 0;
    $poceValgen = 0;
    
    while($row = mysqli_fetch_assoc($result))  {
        
                              if ($row["type"] == "0") {
                                    $market = "S.Ord";
                              }else{
                                    $market = "O.Mar";
                              }
 
       
           // Datos de la Oferta
   
   $sqlOffer="select gor.marks          ,
                     gor.id             ,  
                     gor.offer_id       , 
                     gor.offer_id_index , 
                     gor.grower_id      , 
                     gor.buyer_id       , 
                     gor.status         , 
                     gor.product        , 
                     gor.price          , 
                     gor.size           , 
                     gor.boxtype        , 
                     gor.bunchsize      , 
                     gor.boxqty         , 
                     gor.bunchqty       , 
                     gor.steams         ,
                     gor.req_group      ,
                     gor.request_id     ,
                     g.growers_name     ,
                     gor.product_subcategory,
                     gor.type_market    ,
                     c.name as colorname                     
                from grower_offer_reply gor
                left JOIN product p ON  gor.product = p.name and gor.product_subcategory = p.subcate_name 
                left JOIN colors c ON p.color_id = c.id
                left JOIN growers g ON gor.grower_id = g.id                                                                  
               where request_id = '" . $row['idreq'] . "'
                 and buyer_id   = '" . $idbuy . "'      " ;         
                  

        $result_off   = mysqli_query($con, $sqlOffer);            
              
         $pdf->SetFont('Arial','B',8);
  
         $pdf->Cell(15,6,$row['id_order']." ".$market,0,0,'L');                                                                     
         $pdf->Cell(70,6,$row['name']." ".$row['subcate']." ".$row['size_name']."cm ".$row['colorname'],0,0,'L');            
         $pdf->Cell(25,6,$row['cod_order'],0,0,'L');          
         $pdf->Cell(25,6,$row['qty'],0,0,'R');                              
         $pdf->Cell(8,6,' ',0,0,'L');                 
         $pdf->Cell(25,6,$row['lfd'],0,1,'L');                                                                     

         
         $totrequest = $totrequest + $row['qty'];
         
         
     //    $pdf->Cell(25,6,$row['growers_name'],0,1,'L'); 
         
                  $pdf->SetFont('Arial','',8);
                                     $partialStem = 0;  
                                     $totalPrice  = 0;
                                     $totalPriceFlf  = 0;
                                     $utilTotal1   = 0;
                                                  
                        while($rowOff = mysqli_fetch_assoc($result_off))  {
                            
                            
                                $invoiceEntity = "select price_Flf 
                                                    from invoice_requests
                                                   where id_order  = '" . $rowOff['request_id'] . "'
                                                     and id_fact   = '" . $row['id_order'] . "'
                                                     and grower_id = '" . $rowOff['grower_id'] . "'
                                                     and prod_name = '" . $rowOff['product'] . "' ";
     
                                $invoiceID = mysqli_query($con, $invoiceEntity);
                                $flfprice  = mysqli_fetch_array($invoiceID);
                                
                                
                            
                                if ($row['box_type'] == 0) {
                                    $stems = $rowOff['bunchqty']*$rowOff['steams'];
                                } else {
                                    $stems = $rowOff['bunchqty'];
                                }
                            
                                if ($rowOff["type_market"] == "0") {
                                    $market = "S.Order";
                                }else{
                                    $market = "O.Market";
                                }
                                
                                $util = (($flfprice['price_Flf']-$rowOff['price'])/$flfprice['price_Flf'])*100;
                                 
                                $pdf->Cell(15,6,$market,0,0,'L');                                
                                $pdf->Cell(60,6,$rowOff['product']." ".$rowOff['product_subcategory']." ".$rowOff['steams']."st/bu ".$rowOff['size']."cm ".$rowOff['colorname'],0,0,'L');    
                                
                                $pdf->Cell(20,6,number_format($rowOff['price'], 2, '.', ','),0,0,'R');    
                                $pdf->Cell(20,6,number_format($util, 2, '.', ',')."% ",0,0,'R');    
                                
                                $pdf->Cell(20,6,$stems,0,0,'R');    
                                $pdf->Cell(8,6,'',0,0,'L');    
                                $pdf->Cell(25,6,substr($rowOff['growers_name'],0,15),0,1,'L'); 
                                
                                $totoffer = $totoffer + $stems;
                                
                                $cabCount = 0;  
                                $partialStem  = $partialStem + $stems;  
                                $totalPrice   = $totalPrice + ($rowOff['price']) ;  
                                $totalPriceFlf= $totalPriceFlf + ($flfprice['price_Flf']) ;  
                                
                        }  
                        
                          $differ = $row['qty'] - $partialStem;
                          
                          $porceStems = ($partialStem/$row['qty'])*100;
                          
                          $utilTotal1 = ( ($totalPriceFlf-$totalPrice) / $totalPriceFlf)*100;
                          $porceVal   = $totalPriceFlf-$totalPrice;
                          
                          $poceValtot = $porceVal * $partialStem;
                          
                          if(is_nan($utilTotal1)){  
                              $utilTotal1 = 0;
                          }
                          
                $pdf->Ln(2);
                $pdf->SetFont('Arial','B',10);
                $pdf->Cell(30,10,' ',0,0,'L');     
                $pdf->Cell(30,10,'%Fullfill   :      ',0,0,'R');     
                $pdf->Cell(25,10,number_format($partialStem, 0, '.', ','),0,0,'L');    
                $pdf->Cell(25,10,number_format($porceStems, 2, '.', ',')." %",0,0,'L');    
                $pdf->Cell(30,10,'To complete :  '.$differ,0,0,'R');   
                $pdf->Cell(25,10,'Profit : '.number_format($utilTotal1, 2, '.', ',')."% ($ ".number_format($poceValtot, 2, '.', ',').")",0,1,'L');                  
                $pdf->Cell(70,6,'            _____________________________________________________________________________________',0,1,'L');  

                
                
                                $pdf->Ln(8);
                
                                  if ($cabCount == 0) {
                                        $pdf->Ln(4);    
                                      $cabCount = 1;  
                                  }

         
         $totalStems = $totalStems + $subtotalStems;                     
         $subStemsGrower= $subStemsGrower + ($row['bunchqty']) ;  
         $poceValgen = $poceValgen + $poceValtot;
         

    }
               
    $pdf->Ln(2);
    $pdf->Cell(70,6,'_________________________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','B',10);

    $pdf->Cell(30,10,'TOTALS :     ',0,0,'L');    
    $pdf->Cell(25,10,'Req.  : '.number_format($totrequest, 0, '.', ','),0,0,'L');    
    $pdf->Cell(25,10,'Offer : '.number_format($totoffer, 0, '.', ','),0,0,'L');    
    $pdf->Cell(60,10,'To complete : '.number_format($totrequest-$totoffer, 0, '.', ','),0,0,'L');      
    
    $pdf->Cell(25,10,'Profit : '.number_format($poceValgen, 2, '.', ','),0,1,'L');      
       
    
  $pdf->Output();
  ?>