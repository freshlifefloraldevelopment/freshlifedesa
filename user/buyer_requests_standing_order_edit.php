<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";

  $off_id = $_GET['idor'];  
  $fact_num = $_GET['id_fact'];  
  
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {

    header("location: index.php");
}

      
if (isset($_POST["Submit"]) && $_POST["Submit"] == "Save") {
    
     
    $ins = "update buyer_requests_standing_order 
               set qty      = '" . $_POST["qty"]   . "' ,
                   id_state = '" . $_POST["id_state"] . "'
             where id = '" . $off_id . "' ";    
       
   if(mysqli_query($con, $ins))
    
    header("location:buyer_requests_52.php?id_fact=$fact_num");   
}


  $sel_offers = "select id      , id_order  , order_serial  , cod_order , product       , 
                        sizeid  , feature   , noofstems     , qty       , buyer         , 
                        boxtype , date_added, type          , bunches   , box_name      , 
                        lfd     , lfd2      , comment       , box_id    , shpping_method, 
                        isy     , mreject   , bunch_size    , unseen    , req_qty       , 
                        bunches2, discount  , inventary     , type_price, id_client     , 
                        id_grower , id_state
                   from buyer_requests_standing_order 
                  where id = '" . $off_id . "' ";

$rs_offers = mysqli_query($con, $sel_offers);
$row = mysqli_fetch_array($rs_offers);   


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>Admin Area</title>

        <link href="css/style.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>

        <script type="text/javascript">
            function verify()  {
                var arrTmp = new Array();
                
                arrTmp[0] = checkfrom();
                arrTmp[1] = checkunseen();
                arrTmp[2] = checkqty();
                
                var i;

                _blk = true;

                for (i = 0; i < arrTmp.length; i++)  {
                    if (arrTmp[i] == false)                    {
                        _blk = false;
                    }
                }

                if (_blk == true)  {
                    return true;
                } else {
                    return false;
                }
            }

            function trim(str) {
                if (str != null) {
                    var i;
                    for (i = 0; i < str.length; i++)  {
                        if (str.charAt(i) != " ")  {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }

                    for (i = str.length - 1; i >= 0; i--) {
                        if (str.charAt(i) != " ") {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }

                    if (str.charAt(0) == " ")  {
                        return "";
                    }else {
                        return str;
                    }
                }
            }

            function checkfrom()       {
                
                if (trim(document.frmcat.type.value) == "")  {
                    document.getElementById("lblfrom").innerHTML = "Please enter type Market ";
                    return false;
                }else {
                    document.getElementById("lblfrom").innerHTML = "";
                    return true;
                }                
                                                
            }

            function checkqty()       {
                
                if (trim(document.frmcat.qty.value) == "")  {
                    document.getElementById("lblqty").innerHTML = "Please enter Quantity ";
                    return false;
                }else {
                    document.getElementById("lblqty").innerHTML = "";
                    return true;
                }                
                                                
            }


            function checkunseen()       {
                
                if (trim(document.frmcat.unseen.value) == "")  {
                    document.getElementById("lblunseen").innerHTML = "Please enter state ";
                    return false;
                }else {
                    document.getElementById("lblunseen").innerHTML = "";
                    return true;
                }                
                                                
            }            

        </script>
    </head>
    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php include("includes/header_inner.php"); ?>

            <tr>

                <td height="5"></td>

            </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php include("includes/agent-left.php");?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>

                                                                <td height="5"></td>

                                                            </tr>

                                                            <tr>

                                                                <td class="pagetitle">Edit Request </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <tr>

                                                                <td>

                                                                    <table width="100%">

                                                                        <tr>

                                                                            <td>

                                                                                <a class="pagetitle1" href="buyer_requests_52.php" onclick="this.blur();"><span> Request (Future Packing)</span></a>

                                                                            </td>

                                                                        </tr>

                                                                    </table>

                                                                </td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data" action="buyer_requests_standing_order_edit.php?idor=<?php echo $row["id"]."&id_fact=".$row["id_order"] ?>">

                                                                <tr>

                                                                    <td><div id="box">

                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">

                                                                                <tr>



                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>

                                                                                </tr>
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Cancel </td>
                                                                                    
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                    <select name="id_state" id="id_state" class="listmenu">
                                                                                        <option value="0" <?php  if($row["id_state"] == 0) echo "selected";?>>NO</option>
                                                                                        <option value="1" <?php  if($row["id_state"] == 1) echo "selected";?>>SI</option>
                                                                                    </select>                                                                                        
                                                                                        
                                                                                        <br>
                                                                                            <span class="error" id="lblfrom"></span>
                                                                                    </td>
                                                                                </tr>    
                                                                                
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Quantity: </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="qty" id="qty" value="<?php echo $row["qty"]?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblqty"></span></td>
                                                                                </tr>
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                
                                                                                <tr>
                                                                                    <td>&nbsp;</td>

                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Save" /> </td>
                                                                                </tr>

                                                                            </table>

                                                                        </div></td>

                                                                </tr>

                                                            </form>

                                                        </table></td>

                                                    <td width="10">&nbsp;</td>

                                                </tr>

                                            </table></td>

                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

                                    </tr>

                                    <tr>

                                        <td background="images/middle-leftline.gif"></td>

                                        <td>&nbsp;</td>

                                        <td background="images/middle-rightline.gif"></td>

                                    </tr>

                                    <tr>

                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

                                        <td background="images/middle-bottomline.gif"></td>

                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

                                    </tr>

                                </table></td>

                        </tr>

                    </table></td>

            </tr>

            <tr>

                <td height="10"></td>

            </tr>

            <?php include("includes/footer-inner.php"); ?>

            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>