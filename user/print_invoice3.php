<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_GET['id_buy'];
    $idfac = $_GET['b'];

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $userSessionID . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id_fact          , buyer_id         , order_number, 
                         order_date       , shipping_method  , del_date    , 
                         date_range       , is_pending       , order_serial, 
                         seen             , delivery_dates   , lfd_grower  , 
                         quick_desc       , bill_number      , gross_weight, 
                         volume_weight    , freight_value    , guide_number, 
                         total_boxes      , sub_total_amount , tax_rate    , 
                         shipping_charge  , handling         , grand_total , 
                         bill_state       , date_added       , user_added  ,
                         air_waybill      , charges_due_agent,
                         credit_card_fees , per_kg                         
                         from invoice_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id_fact = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Requests
/*
   $sqlDetalis="select c.name as cate , sc.name as subcate,
                       sum(ir.steams * ir.bunchqty ) as steams,
                       sum(ir.steams * ir.bunchqty * 0.12) as total_fijo,
                       sum(ir.steams * ir.bunchqty * sc.price) as total,
                       sum(sc.price)/count(*) as price,
                       count(*) reg
                  from invoice_requests ir
                 INNER JOIN growers rg     ON ir.grower_id = rg.id                  
                 INNER JOIN product p      ON ir.product = p.id                  
                 INNER JOIN category c     ON c.id = p.categoryid                  
                 INNER JOIN subcategory sc ON sc.id = p.subcategoryid                  
                 where ir.buyer    = '" . $buyer_cab . "'
                   and ir.id_fact  = '" . $id_fact_cab . "'
                 group by c.name , sc.name ";   
  */
   
      $sqlDetalis="select c.name as cate , sc.name as subcate,
                       sum(ir.steams * ir.bunchqty ) as steams,
                       sum(ir.steams * ir.bunchqty * 0.12) as total_fijo,
                       sum(ir.steams * ir.bunchqty * sc.price) as total,
                       sum(sc.price)/count(*) as price,
                       count(*) reg
                  from invoice_requests ir
                 INNER JOIN growers rg     ON ir.grower_id = rg.id                  
                 INNER JOIN product p      ON ir.prod_name = p.name and ir.product_subcategory = p.subcate_name
                 INNER JOIN category c     ON c.id = p.categoryid                  
                 INNER JOIN subcategory sc ON sc.id = p.subcategoryid                                   
                 where ir.buyer    = '" . $buyer_cab . "'
                   and ir.id_fact  = '" . $id_fact_cab . "'
                 group by c.name , sc.name ";   
   
        $result   = mysqli_query($con, $sqlDetalis);    

    $pdf = new PDF();
    
    $pdf->AddPage();
    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'COMMERCIAL INVOICE',0,0,'L'); 
    $pdf->Image('logo.png',148,5); 
    
    $pdf->Ln(10);
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Cell(0,10,'Shipping Details ',0,0,'R');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,0,'L');
    $pdf->Cell(0,6,'INVOICE: '.$buyerOrderCab['order_number'],0,1,'R');  
    
    $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,0,'L');    
    $pdf->Cell(0,6,'Total Boxes: '.$buyerOrderCab['total_boxes'],0,1,'R');    
    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');        
   // $pdf->Cell(0,6,'Gross Weight: '.$buyerOrderCab['gross_weight'],0,1,'R');        
    
    $pdf->Cell(70,6,'Company Name: '.$buy['company'],0,1,'L');  
   // $pdf->Cell(0,6,'Volume Weight: '.$buyerOrderCab['volume_weight'],0,1,'R');  
    
    $pdf->Ln(10);

    $pdf->Cell(70,6,'Category',0,0,'L');
    $pdf->Cell(25,6,'Subcategory',0,0,'C');
    $pdf->Cell(25,6,'Steams',0,0,'C');
    $pdf->Cell(25,6,'Price',0,0,'C');
    $pdf->Cell(30,6,'Total',0,1,'R'); 
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);

    while($row = mysqli_fetch_assoc($result))  {
         
        //  *$row['steams']   campo con algunos registros en null   *****   ARREGLAR  *******
         
         $pdf->Cell(70,4,$row['cate'],0,0,'L');            
         $pdf->Cell(25,6,$row['subcate'],0,0,'L');                              
         $pdf->Cell(25,6,$row['steams'],0,0,'C');                              
         $pdf->Cell(25,6,'$'. number_format($row['price'], 2, '.', ','),0,0,'C');                                               
         $pdf->Cell(30,6,'$'.number_format($row['total'], 2, '.', ','),0,1,'R');           
                 
         $totalCal = $totalCal + $row['total'];
         $totalStem = $totalStem + $row['steams'];
    }


         
    $pdf->Ln(2);
    $pdf->SetFont('Arial','B',15);
    $pdf->Cell(70,10,'Contact Details ',0,1,'L');
    $pdf->SetFont('Arial','B',10);            
    $pdf->Cell(70,6,'Total Stems: '.$totalStem,0,0,'L');
    $pdf->Cell(0,6,'Sub - Total Amount: $'.number_format($totalCal, 2, '.', ','),0,1,'R');
    
    $pdf->Cell(70,6,'Av. Interoceanica OE6-73 y Gonzalez Suarez',0,1,'L'); 
    $pdf->Cell(70,6,'Quito, Ecuador',0,1,'L');   
    $pdf->Cell(70,6,'Phone: +593 602 2630',0,1,'L');    
    $pdf->Cell(70,6,'Email:info@freshlifefloral.com',0,1,'L'); 
  

    
  $pdf->Output();
  ?>