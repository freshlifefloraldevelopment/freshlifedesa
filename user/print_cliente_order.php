<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $idbuy = $_GET['id_buy'];    
    
    $id_order = $_GET['b'];    


    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $idbuy . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);

    $OrderCab = "select * from buyer_orders
                  where id = '" . $id_order . "'  " ;
     
    $buyOrder = mysqli_query($con, $OrderCab);
    $cabOrder = mysqli_fetch_array($buyOrder);
    
   
   // Datos de la Orden 
   
   $sqlDetalis="
       
select unseen, br.order_serial , br.id_order,
                       br.id as idreq,
                       bo.qucik_desc,
                       b.first_name ,   
                       CAST(br.id_order AS UNSIGNED) idorder,
                       br.cod_order , 
                       br.product,
                       br.boxtype,
                       br.sizeid ,
                       p.subcategoryid as sid,
                       p.name as nameprod, 
                       br.qty ,  
                       br.lfd ,
                       sz.name as size_name,
                       g.growers_name , br.id_client,  br.id_reser , substr(sc.name,1,25) as namecli , s.name as subcate  ,
                       'Assigned' as tipo,f.name as featurename,c.name as colorname
                  from reser_requests br
                 INNER JOIN product p     ON br.product = p.id
                 INNER JOIN buyers b     ON br.buyer = b.id
                 INNER JOIN buyer_orders bo ON br.id_order = bo.id
                 INNER JOIN sizes sz ON br.sizeid = sz.id  
                 INNER JOIN subcategory s ON s.id = p.subcategoryid 
	          left join colors c on p.color_id=c.id      
                  left join features f on br.feature = f.id                
                  left join sub_client sc ON br.id_client = sc.id   
                  left JOIN growers g     ON br.id_grower = g.id                                                  
                 where br.buyer    = '" . $idbuy . "' 
                   and br.id_order = '" . $id_order . "' 
        union
            select unseen, br.order_serial , br.id_order,
                       br.id as idreq,
                       bo.qucik_desc,
                       b.first_name ,   
                       CAST(br.id_order AS UNSIGNED) idorder,
                       br.cod_order , 
                       br.product,
                       br.boxtype,
                       br.sizeid ,
                       p.subcategoryid as sid,
                       p.name as nameprod, 
                       br.qty ,  
                       br.lfd ,
                       sz.name as size_name,
                       g.growers_name , br.id_client,  br.id_reser , substr(sc.name,1,25) as namecli , s.name as subcate  ,
                       'Requests' as tipo,f.name as featurename,c.name as colorname
                  from buyer_requests br
                 INNER JOIN product p     ON br.product = p.id
                 INNER JOIN buyers b     ON br.buyer = b.id
                 INNER JOIN buyer_orders bo ON br.id_order = bo.id
                 INNER JOIN sizes sz ON br.sizeid = sz.id  
                 INNER JOIN subcategory s ON s.id = p.subcategoryid   
	          left join colors c on p.color_id=c.id 
                  left join features f on br.feature = f.id                                  
                  left join sub_client sc ON br.id_client = sc.id   
                  left JOIN growers g     ON br.id_grower = g.id                                                  
                 where br.buyer    = '" . $idbuy . "' 
                   and br.id_order = '" . $id_order . "' 
                   and br.comment = 'SubClient-Reques'                       
                 order by subcate, nameprod ,size_name ";

    $result   = mysqli_query($con, $sqlDetalis);    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    // Resumen de la Orden
    
     $sqlresumen="select p.name as variname   , s.name as subcate  ,  sz.name as size_name ,qty ,
                         br.sizeid , s.id as sid    , br.id as idreq,    br.id_client,
                         br.feature,c.name as colorname
                    from reser_requests br
                   INNER JOIN product p       ON br.product = p.id
                   INNER JOIN buyers b        ON br.buyer = b.id
                   INNER JOIN buyer_orders bo ON br.id_order = bo.id
                   INNER JOIN sizes sz        ON br.sizeid = sz.id  
                   INNER JOIN subcategory s   ON s.id = p.subcategoryid  
	            left join colors c        on p.color_id=c.id                    
                    left join features f on br.feature = f.id                                    
                    left join sub_client sc   ON br.id_client = sc.id                      
                    left JOIN growers g       ON br.id_grower = g.id                                                                     
                   where br.buyer    = '" . $idbuy . "' 
                     and br.id_order = '" . $id_order . "'          
         union
         select p.name as variname   , s.name as subcate  ,  sz.name as size_name ,qty ,
                         br.sizeid , s.id as sid    , br.id as idreq,    br.id_client ,
                         br.feature,c.name as colorname
                    from buyer_requests br
                   INNER JOIN product p       ON br.product = p.id
                   INNER JOIN buyers b        ON br.buyer = b.id
                   INNER JOIN buyer_orders bo ON br.id_order = bo.id
                   INNER JOIN sizes sz        ON br.sizeid = sz.id  
                   INNER JOIN subcategory s   ON s.id = p.subcategoryid                                   
                    left join sub_client sc   ON br.id_client = sc.id                      
                    left JOIN growers g       ON br.id_grower = g.id   
	            left join colors c        on p.color_id=c.id 
                     left join features f on br.feature = f.id                                    
                   where br.buyer    = '" . $idbuy . "' 
                     and br.id_order = '" . $id_order . "' 
                     and br.comment = 'SubClient-Reques'                         
                   order by   subcate   ,variname ,  size_name  ";

    $resultRes   = mysqli_query($con, $sqlresumen);    



    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'  CUSTOMER ORDERS TOTAL',0,0,'L'); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,1,'L');           
    $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');           
    $pdf->Cell(70,6,'-'.$cabOrder['qucik_desc'],0,1,'L');  
    $pdf->Cell(70,6,'Delivery :'.$cabOrder['del_date'],0,1,'L');  
    $pdf->Cell(70,6,'Order :'.$cabOrder['order_number'],0,1,'L');      
    
    $pdf->Ln(10);
    $cabCount = 1;  
    
// Cabecera
    $pdf->Cell(15,6,'Num.',0,0,'L');    
    $pdf->Cell(55,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Qty',0,0,'R');
    $pdf->Cell(25,6,'Order',0,0,'L');
    $pdf->Cell(40,6,'Client',0,0,'L');     
    $pdf->Cell(25,6,'Weight',0,1,'R');     
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    $totalBunch  = 0;
    $totalOffer  = 0;
    
                        $cantidad1= 0;
                        $weight1 = 0;
    $totweight= 0;
    
    while($row = mysqli_fetch_assoc($result))  {    
        
        //////////////////////////////////////////////////////////////////////////////////////////
                        $sqlbunch = "select product_id,sizes  , size_value , sid , bunch_sizes , bunch_value 
                                        from growcard_prod_bunch_sizes
                                       where product_id = '" . $row['product'] . "'
                                         and sizes      = '" . $row['sizeid'] . "'
                                        group by product_id   " ;
     
                        $rs_bunch = mysqli_query($con, $sqlbunch);
                        $facBunch    = mysqli_fetch_array($rs_bunch);
        /////////////////////////////////////////////////////////////////////////
                        $sqlfactor = "select subcategory , size , factor
                                        from grower_parameter
                                       where idsc = '" . $row['sid'] . "'
                                         and size = '" . $row['sizeid'] . "'  " ;
     
                        $rs_factor = mysqli_query($con, $sqlfactor);
                        $factor    = mysqli_fetch_array($rs_factor);        
        
                         $weight = $factor['factor'] * $row['qty'];
       
           // Datos de la Oferta
   
   $sqlOffer="select gor.marks          ,
                     gor.id             ,  
                     gor.offer_id       , 
                     gor.offer_id_index , 
                     gor.grower_id      , 
                     gor.buyer_id       , 
                     gor.status         , 
                     gor.product        , 
                     gor.price          , 
                     gor.size           , 
                     gor.boxtype        , 
                     gor.bunchsize      , 
                     gor.boxqty         , 
                     gor.bunchqty       , 
                     gor.steams         ,
                     gor.req_group      ,
                     gor.request_id     ,
                     g.growers_name                                            
                from grower_offer_reply gor
                left JOIN growers g ON gor.grower_id = g.id                                                                  
               where request_id     = '" . $row['idreq'] . "'
                 and buyer_id       = '" . $idbuy . "' 
                 and gor.cliente_id = '" . $row['id_client'] . "'     " ;      
                        

        $result_off   = mysqli_query($con, $sqlOffer);            
              
         $pdf->SetFont('Arial','B',8);
  
         $pdf->Cell(15,6,$row['order_serial'],0,0,'L');                     
         $pdf->Cell(55,6,$row['subcate']." ".$row['nameprod']." ".$row['size_name']." cm. ".$row['colorname']." ".$row['featurename'],0,0,'L');            
         $pdf->Cell(25,6,$row['qty'],0,0,'R');                              
         $pdf->Cell(25,6,$row['tipo'],0,0,'L'); 
         $pdf->Cell(40,6,$row['namecli'],0,0,'L'); 
         $pdf->Cell(25,6,$factor['factor']."  ".$weight,0,1,'R'); 
         
                  $pdf->SetFont('Arial','',8);
                        $offerQty = 0;

                        
                        while($rowOff = mysqli_fetch_assoc($result_off))  {
                                                                                                                        
                                 //////////////////////////////////////////// 
                                $pdf->Cell(34,6,'                 ',0,0,'L');                                
                                $pdf->Cell(40,6,$rowOff['product']." ".$rowOff['size']."cm.",0,0,'L');    
                                $pdf->Cell(25,6,$rowOff['bunchqty'],0,0,'L');    
                                $pdf->Cell(25,6,$rowOff['growers_name'],0,1,'L'); 
                                
                                //$cabCount = 0;  
                                $offerQty = $offerQty + $rowOff['bunchqty'] ;  
                            }         

                if ($row['idreq'] == 39098){ 
                    $cantidad1 = 0;
                }  
         
         $totalBunch = $totalBunch + $row['qty'];  
         
         $totalOffer = $totalOffer + $offerQty ;  
           $totweight = $totweight + $weight;
    }
    
    $porcen = ($totalOffer / $totalBunch)*100 ;  
     $pdf->Cell(70,6,'______________________________________________________________________________________________________________',0,1,'L');  
                       $pdf->SetFont('Arial','B',10);
     $pdf->Cell(40,6,"TOTALS..: ",0,0,'R');                              
     $pdf->Cell(45,6,$totalBunch,0,0,'R');                              
     $pdf->Cell(25,6,"        ".$totalOffer,0,0,'L'); 
     
     $pdf->Cell(20,6,'% '.number_format($porcen, 2, '.', ','),0,0,'L');   
     
     $pdf->Cell(25,6,"            ".$totweight." KG",0,1,'R');
 
 
                $pdf->Ln(20);
                
     /////////////////////////////////////////////////////////////////////////////////////////////////////////////           
          
    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'  TOTAL SUMMARY ',0,0,'L'); 
    
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);    
    
    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(20,6,'Request',0,0,'L'); 
    $pdf->Cell(20,6,'Offer',0,0,'L'); 
    $pdf->Cell(20,6,'Missing',0,1,'L'); 

    
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',10);  
    $totweight = 0;
    
    $totqty2 = 0;
    $totbun2 = 0;   
    
     $tmp_idorder = 0;
    
    while($totRes = mysqli_fetch_assoc($resultRes))  {
                   // Datos de la Oferta
   
                $sqlOffer1="select  sum(gor.bunchqty) as  bunchqty 
                            from grower_offer_reply gor
                            left JOIN growers g ON gor.grower_id = g.id                                                                  
                           where request_id     = '" . $totRes['idreq'] . "'
                             and buyer_id       = '" . $idbuy . "' 
                             and gor.cliente_id = '" . $totRes['id_client'] . "'     " ;  

                $result_off1   = mysqli_query($con, $sqlOffer1); 
                
                $offerQty1=0;
                
        
                 if ($totRes['subcate']." ".$totRes['variname']." ".$totRes['size_name']." ".$totRes['colorname'] != $tmp_idorder) {
                     
                     $pdf->Cell(80,6,$tmp_idorder,0,0,'L');    
                     $pdf->Cell(20,6,$sumvarie,0,0,'L');    
                     $pdf->Cell(20,6,$detOffer,0,0,'L');    
                     $pdf->Cell(20,6,($sumvarie-$detOffer),0,1,'L');    
                     
                        $sumvarie = 0;
                        $detOffer = 0;
                 }
                                               
                 
                        $sumvarie = $sumvarie + $totRes['qty'];
                                      
                //$pdf->Cell(40,6,$totRes['subcate'],0,0,'L');    
                //$pdf->Cell(25,6,$totRes['name'],0,0,'L');    
                //$pdf->Cell(25,6,$totRes['size_name'],0,0,'L');                 
                //$pdf->Cell(15,6,$totRes['qty'],0,1,'L');                                                
                        while($rowOff1 = mysqli_fetch_assoc($result_off1))  {
                                                                                                                                                        
                                $offerQty1 = $offerQty1 + $rowOff1['bunchqty'] ;  
                            }  
                            
                            $detOffer = $detOffer + $offerQty1;
                
                $totqty2 = $totqty2 + $totRes['qty'];
                $totbun2 = $totbun2 + $offerQty1;
                

                
                $tmp_idorder = $totRes['subcate']." ".$totRes['variname']." ".$totRes['size_name']." ".$totRes['colorname'];   
    } 
                            $tot2    = $tot2 + ($totqty2-$totbun2);
    
                    $pdf->Ln(5);
        $pdf->Cell(70,6,'_________________________________________________________________________________________________________',0,1,'L');  
        
                               $pdf->SetFont('Arial','B',10);
        $pdf->Cell(50,6,'Totals : ',0,0,'L');
        $pdf->Cell(20,6,$totqty2,0,0,'R');
        $pdf->Cell(20,6,$totbun2,0,0,'R');
        $pdf->Cell(20,6,$tot2,0,1,'R');
                                 
    
  $pdf->Output();
  ?>