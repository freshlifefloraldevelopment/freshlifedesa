<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";

	session_start();

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}

	if(isset($_GET['delete'])) 	{
	  $query = 'DELETE FROM subcategory WHERE  id= '.(int)$_GET['delete'];
	  mysqli_query($con,$query);
	}

	

	$qsel="select * from subcategory where id not in (92) order by name";
        
        /*  $qsel="select sc.id , sc.cat_id , sc.name     ,sc.description , sc.subcat_image, sc.features , sc.price , 
                        sc.price_client   ,
                        ca.id , ca.name as catego  , ca.cat_image, ca.cat_desc , ca.tax_rate , ca.price
                  from subcategory sc
                 inner join category ca on sc.cat_id = ca.id 
                 order by ca.name";
         */
        
	$rs=mysqli_query($con,$qsel);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					"bJQueryUI": true,

					"sPaginationType": "full_numbers"
				});
			} );

</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

 <?php include("includes/header_inner.php");?>

  <tr>
        <td height="5"></td>
  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/product-left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr>
                           <td height="5"></td>
                </tr>

                  <tr>

                    <td class="pagetitle">Manage Sub Categories</td>

                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

				  <tr>

                    <td>

					<table width="100%">

					<tr>

					<td>

					<a href="subcategory_add.php" class="pagetitle" onclick="this.blur();"><span> + Add New Sub Category  </span></a>

					</td>

					</tr>

					</table>

					</td>

                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

                  <tr>

                    <td><div id="box">

		<div id="container">			

			<div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>

			<th width="8%" align="center">Sr.</th>
			<th width="25%" align="left">Sub Category</th>
                        <th width="14%" align="left">Price QB</th>
                        <th width="25%" align="left">Parent Category</th>                        
                        <!--th width="12%" align="left">Price Client</th-->                        
                        <th width="25%" align="left">QuickBooks</th>                            
                        <th width="25%" align="left">Param</th>                                                    
                        <th width="30%" align="left">Browse</th>    
                        <th width="12%" align="left">Status</th>                                                
                        <th align="center" width="8%">Edit</th>
			<th align="center" width="10%">Del</th>

		</tr>

	</thead>

	<tbody>

		<?php

						  	$sr=1;

						  while($subcategory=mysqli_fetch_array($rs))	  {
                                                      
                                                        if ($subcategory["status"] == 0) {
                                                            $status="Active";
                                                        } else {
                                                            $status="Locked";
                                                        }                                                      

						     $sel_category="select * from category where id='".$subcategory["cat_id"]."'";

							 $rs_category=mysqli_query($con,$sel_category);

							 $category=mysqli_fetch_array($rs_category);

						  ?>

                    <tr class="gradeU">

                          <td align="center" class="text"><?php echo $sr;?></td>

                          <td class="text" align="left"><?php echo $subcategory["name"]?></td>
                          
                          <td class="text" align="left"><?php echo $subcategory["price_client"]?></td>
                          
                           <!--td class="text" align="left"><img src="/images/<?php echo $subcategory["subcat_image"]?>" width="40" height="40"  /></td-->

                           <td class="text" align="left"><?php echo $category["name"]?></td>
                           
                           <!--td class="text" align="left"><?php echo $subcategory["price_client"]?></td-->                           
                           
                           <td class="text" align="left"><?php echo $subcategory["code_quick"]?></td>   
                           
                           <td class="text" align="center"><a href="packing_card_mgmt.php?ids=<?php echo $subcategory["id"]?> "  > Card </a> </td>  
                           
                           <td class="text" align="center"><a href="browse_page_mgmt_card.php?ids=<?php echo $subcategory["id"]?> "  > Varieties </a> </td>                             
                           
                           
                           <td class="text" align="left"><?php echo $status?></td>                                                      
                           
                           <td align="center" ><a href="subcategory_edit.php?id=<?php echo $subcategory["id"]?>"><img src="images/edit.gif" border="0" alt="Edit" /></a></td>

			   <td align="center" ><a href="?delete=<?php echo $subcategory["id"]?>"  onclick="return confirm('Are you sure, you want to delete this subcategory ?');"><img src="images/delete.gif" border="0" alt="Delete" /></a></td>

                    </tr>

			 <?php

			 		$sr++;

			}

			?> 

	

	</tbody>

</table>



			</div>

			</div>



			</div>

		</td>

                  </tr>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>

</body>

</html>
