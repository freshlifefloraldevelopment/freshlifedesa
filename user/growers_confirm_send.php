<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_SESSION["buyer"];
    $idbuy = $_GET['id_buy'];    
    $idfac = $_GET['b'];
    $cajastot = 0;
                             
   
   // Datos Growers
   
   $sqlDetalis="select gd.id       , gd.buyer_id   , gd.grower_id , gd.order_number , gd.send_date ,
                       gd.comment1 , gd.is_sending , gd.send_type ,
                       gw.growers_name , bu.first_name, bu.last_name	   
                  from growers_destail_send gd
                 inner JOIN growers gw ON gd.grower_id = gw.id                  
                 inner JOIN buyers bu ON gd.buyer_id = bu.id  
                 order by gd.buyer_id , gd.grower_id , gd.order_number ";

        $result   = mysqli_query($con, $sqlDetalis);    

        
    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',15);    
    $pdf->Cell(70,10,'  Growers Confirmation',0,0,'L'); 
    
    
        $pdf->SetFont('Arial','B',10);

                                   
    $pdf->Ln(15);
    $pdf->Cell(35,6,'Buyer',0,0,'L');    
    $pdf->Cell(70,6,'Grower',0,0,'L');
    $pdf->Cell(15,6,'Order.',0,0,'L');
    $pdf->Cell(30,6,'Confirm',0,0,'L');
    $pdf->Cell(20,6,'Type',0,1,'L');    

    $pdf->Cell(70,6,'________________________________________________________________________________________________',0,1,'L');  
    //$pdf->SetFont('Arial','',10);
    
    $tmp_idorder = 0;
    
    $ii = 0;
    
    while($row = mysqli_fetch_assoc($result))  {
                                            
       $ii++; 
       
                                  if ($row["send_type"] == 0) {
                                        $typeId = "WhatsApp";
                                  } elseif ($row["send_type"] == 1){
                                        $typeId = "Skype";
                                  } elseif ($row["send_type"] == 2){
                                        $typeId = "Mail";
                                  }                                  


     $pdf->SetFont('Arial','',8);

         $pdf->Cell(35,4,$row['first_name']." ",0,0,'L');                     
                           
         $pdf->Cell(70,4,$row['growers_name'],0,0,'L');                                                   
         
         $pdf->Cell(15,4,$row['order_number'],0,0,'L');                                                            
         
         $pdf->Cell(30,4,$row['send_date'],0,0,'L');                                                            
         
         $pdf->Cell(20,4,$typeId,0,1,'L');                                                                     
                           
    }
    
    $pdf->Ln(4);    
        
    $pdf->Cell(70,6,'Freshlifefloral',0,0,'L');   
    
  $pdf->Output();
  ?>