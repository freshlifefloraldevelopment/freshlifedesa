<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
   
    $userSessionID = $_GET['id_buy'];
    $idfac = $_GET['b'];
    $cajastot = 0;

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $userSessionID . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id , buyer_id , order_number
                         from buyer_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Requests
   
   $sqlDetalis="select gor.grower_id           , 
                       gor.offer_id            , 
                       gor.offer_id_index      ,                        
                       gor.product as prod_name,
                       gor.size                ,                        
                       gor.steams              , 
                       (gor.price) as salesPriceCli ,
                       gor.status,
                       sum(gor.bunchqty) as bunchqty   , 
                       substr(rg.growers_name,1,19) as name_grower,
                       gor.product_subcategory  , 
                       c.name as name_color     ,
                       p.id as idprod           ,
                       ff.name as featurename
                  from grower_offer_reply gor
                 inner join buyer_requests g on gor.offer_id=g.id 
                 inner join buyer_orders bo  on bo.id = g.id_order                  
                 inner join growers rg       ON gor.grower_id = rg.id                  
                  left join product p        ON gor.product = p.name  and gor.product_subcategory = p.subcate_name
                  left join colors c         ON p.color_id = c.id 
                  left join features ff      on g.feature=ff.id                 
                 where g.id_order   = '" . $id_fact_cab . "'
                   and gor.buyer_id = '" . $buyer_cab . "'
                  group by gor.grower_id , 
                           offer_id      ,
                           offer_id_index, 
                           gor.product , 
                           gor.size    ,
                           gor.steams  ,
                           gor.price   ,
                           gor.status  , 
                           gor.product_subcategory  , 
                           c.name ,
                           ff.name
                 order by gor.grower_id,gor.id  ";

        $result   = mysqli_query($con, $sqlDetalis);    
        
    $pdf = new PDF();
    
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'COORDINATION',0,0,'L'); 
    
    $pdf->Ln(10);    
    
    $pdf->SetFont('Arial','B',15);
    $pdf->Cell(70,10,'Cargo Agency ',0,0,'L');
    $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(70,6,'Buyer: '.$buy['first_name'],0,0,'L');
    $pdf->Cell(0,6,'Invoice #: '.$buyerOrderCab['order_number'],0,1,'R');            
    $pdf->Ln(5);

    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Box',0,1,'C');
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  

    
    $tmp_idorder = 0;
    $fullestot = 0;
            
    while($row = mysqli_fetch_assoc($result))  {
        
        $sel_boxg = "select gor.box_id , gor.box_qty , gor.factor
                       from box_grower_confirmation gor
                      where gor.order_id  = '" . $id_fact_cab . "'
                        and gor.grower_id = '" . $row['grower_id'] . "'    ";

        $rs_boxg = mysqli_query($con,$sel_boxg);                 
        
        $cajas  = 0;
        $fulles = 0;        

        while($tot_boxg = mysqli_fetch_array($rs_boxg))  {
                $cajas  = $cajas  + $tot_boxg['box_qty'];
                $fulles = $fulles + $tot_boxg['box_qty'] * $tot_boxg['factor'];
        }
        
        
         // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type,subcategoryid from product where id = '" . $row['idprod'] . "' ";
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
        
        $sel_subcatego = "select name from subcategory where id = '" . $bunch_stem['subcategoryid'] . "' ";
        $rs_subcatego = mysqli_query($con,$sel_subcatego);       
        $subc = mysqli_fetch_array($rs_subcatego);        
                        
            
              if ($bunch_stem['box_type'] == 0) {
                    $Subtotal= $row['steams'] * $row['bunchqty'] * $row['salesPriceCli'];
                    $qtyFac = $row['bunchqty']*$row['steams'];
                    $unitFac = "STEMS";                     
              }else{
                    $Subtotal=  $row['bunchqty'] * $row['salesPriceCli'];
                    $qtyFac = $row['bunchqty']; 
                    $unitFac = "BUNCHES";                   
              }        
                
         $subtotalStems= $row['bunchqty'] ;      
         
         $subtotalStems= $row['bunchqty'] ;      
         
         if ($row['grower_id'] != $tmp_idorder) {
               $pdf->SetFont('Arial','',10);    
               
               if ($sw == 1) { 
                    //  $pdf->Cell(70,6,"  ",0,1,'L');                       
                    //  $pdf->Cell(70,6,"Total Stems Grower : ".$subStemsGrower,0,1,'L');                                 
               }
                      $sw = 1;             
                      $pdf->Cell(70,6,$row['name_grower']." (".$cajas." "."Boxes".") ",0,0,'L');   

                      $pdf->Cell(70,6,number_format($fulles, 1, '.', ',')." Full box",0,1,'L'); 
                      
                      $cajastot =  $cajastot + $cajas;                      
                      
                      $fullestot =  $fullestot + $fulles;                      
                      
                      $subStemsGrower = 0;                      

         }

  $pdf->SetFont('Arial','B',15);
  
  //$pdf->Cell(70,4,$row['prod_name']." ".$row['product_subcategory']." ".$row['size']." cm ".$row['steams']." st/bu ".$row['name_color']." ".$row['featurename'],0,0,'L');            
  //$pdf->Cell(25,6,$row['bunchqty'],0,1,'C');                                           
         
            $totalCal = $totalCal + $Subtotal;
            $totalStems = $totalStems + $subtotalStems;            
            $tmp_idorder = $row['grower_id'];
            $subStemsGrower= $subStemsGrower + ($row['bunchqty']) ;             
    }
    
   
    
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');    
    
     $pdf->Cell(70,6,"Total Fulles : ".$fullestot,0,1,'L'); 
    
    $pdf->Ln(2);
                                    
  $pdf->Output();
  ?>