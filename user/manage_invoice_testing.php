<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";
        
    $fact_number  = $_GET['id_fact'];
    
	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}

	  $sel_info="select ir.id_fact            , ir.id_order       , ir.order_serial  , 
                            ir.cod_order          , ir.product        , ir.sizeid        , 
                            ir.qty                , ir.buyer          , ir.date_added    , 
                            ir.bunches            , ir.box_name       , ir.lfd           , 
                            ir.comment            , ir.box_id         , ir.shpping_method, 
                            ir.mreject            , ir.bunch_size     , ir.unseen        , 
                            ir.inventary          , ir.offer_id       , ir.prod_name     , 
                            ir.product_subcategory, ir.size           , ir.boxtype       , 
                            ir.bunchsize          , ir.boxqty         , ir.bunchqty      , 
                            ir.steams             , ir.gorPrice       , ir.box_weight    , 
                            ir.box_volumn         , ir.grower_box_name, ir.reject        , 
                            ir.reason             , ir.coordination   , ir.cargo         , 
                            ir.color_id           , ir.gprice         , ir.tax           , 
                            ir.cost_ship          , round(ir.handling,0) as handling     , 
                            ir.grower_id          , ir.offer_id_index, ir.peso ,
                            substr(rg.growers_name,1,19) as name_grower ,ir.salesPriceCli, 
                            ir.price_Flf,
                            ir.ship_cost , ir.cost_cad , ir.price_cad,
                            ir.duties       , ir.handling_pro , ir.total_duties , ir.price_cad_duties,
                            io.per_kg
                       from invoice_requests ir
                      INNER JOIN growers rg ON ir.grower_id = rg.id     
                      INNER JOIN invoice_orders io ON ir.id_fact = io.id_fact                  
                      where ir.id_fact  = '" . $fact_number . "'
                      order by ir.grower_id ";
        
	$rs_info=mysqli_query($con,$sel_info);
        
	if(isset($_GET['delete'])) 
	{
	  $query = 'DELETE FROM invoice_requests WHERE offer_id = '.(int)$_GET['delete'];
	  mysqli_query($con,$query);
          //header('location:list_mgmt.php?id_fact='.$fact_number.'&id_grow='.$growerid);          
	}        

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
			} );
</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
  <?php include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <?php include("includes/agent-left.php");?>
          <td width="5">&nbsp;</td>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="5"></td>
                          </tr>
                          <tr>
                            <td class="pagetitle">Shipping Cost and Duties </td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><table width="100%">
                                <tr>
                                  <td>Shipping</td>
                                </tr>
                              </table></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><div id="box">
                                <div id="container">
                                  <div class="demo_jui">
                                    <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                      <thead>
                                        <tr>
                                          <th>Variety</th>
                                          <th>Ship.Cost</th>                                                                                    
                                          <th>Stems</th>                                                                                    
                                          <th>Weigth</th>
                                          
                                          <th>Per Kg.</th> 
                                          <th>Cost.Total</th>                                          
                                          <!--th>CAD</th-->                                             
                                          
                                          <!--th>CAD Old</th-->                                                                                                                              
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php	  $sr=1;
						  while($row=mysqli_fetch_array($rs_info))	  {
					?>
                                          
                                        <tr class="gradeU">
						<td>
                                                        <?php
                                                            $res = "";
                                                        if ($row['boxtype'] == "HB") {
                                                                    $res = "Half Boxes";
                                                        } else if ($row['boxtype'] == "EB") {
                                                                    $res = "Eight Boxes";
                                                        } else if ($row['boxtype'] == "QB") {
                                                                    $res = "Quarter Boxes";
                                                        } else if ($row['boxtype'] == "JB") {
                                                                    $res = "Jumbo Boxes";
                                                        }
                                                        

         // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type from product where id = '" . $row['product'] . "' ";
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
            
              if ($bunch_stem['box_type'] == 0) {
                    $Subtotal    = $row['steams'] *$row['bunchqty'] * $row['salesPriceCli'];                   
                    $totalStems = $row['steams'] *$row['bunchqty'] ;                                       
              }else{
                    $Subtotal   = $row['bunchqty'] * $row['salesPriceCli'];
                    $totalStems= $row['bunchqty'] ;
              }             
              
              
$cost_tot   = $row['ship_cost'] * $row['peso'] * $row['per_kg'] * $totalStems;              
                                                        
                                                                                                                                                                              
                                                           
                                                           
                                                        ?>
                                                            <strong><?php echo $row['name_grower']." (". $row['boxqty']." ".$res.")"  ?></strong>                                                                                                     
						  	    <div><?php echo $row['prod_name']." ".$row['size']." cm"." ".$row['steams']." st/bu"; ?></div>
							    <small><?php echo $row['product_subcategory'] ?></small>
						 </td> 

										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo "$".number_format($row['ship_cost'], 2, '.', ',') ; ?>
                                                                                    </td>                                                                                        

										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo $totalStems;?>
                                                                                    </td>
                                            
                                            
										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($row['peso'], 2, '.', ',') ; ?>
                                                                                    </td>      
                                            
                                            
                                            
                                            


										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo number_format($row['per_kg'], 2, '.', ',') ; ?>
                                                                                    </td> 
                                            
										    <td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo "$".number_format($cost_tot, 2, '.', ',') ; ?>
                                                                                    </td> 
                                            
                                            
                                            

										    <!--td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo "$".number_format($row['price_cad_duties'], 2, '.', ',') ; ?>
                                                                                    </td-->                                             
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
										    <!--td>
                                                                                        <?php " " ?>   <br>                                                                                            
                                                                                        <?php echo "$".number_format($row['price_cad'], 2, '.', ',') ; ?>
                                                                                    </td-->                                                                                                                                    
                                            <td align="center" ><a href="update_invoice_test_edit.php?id_fact=<?php echo $row["id_fact"]."&id_ord=".$row["offer_id"] ?> "><img src="images/edit.gif" border="0" alt="Edit" /></a></td>                                                                                                                                    
                                                
                                                
                                        </tr>
                                          
                                        <?php
						$sr++;
						}
					 ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div></td>
                          </tr>
                        </table></td>
                      <td width="10">&nbsp;</td>
                    </tr>
                  </table></td>
                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
              </tr>
              <tr>
                <td background="images/middle-leftline.gif"></td>
                <td>&nbsp;</td>
                <td background="images/middle-rightline.gif"></td>
              </tr>
              <tr>
                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                <td background="images/middle-bottomline.gif"></td>
                <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <?php include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
