<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";
        


if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}
if (isset($_POST["Submit"]) && $_POST["Submit"] == "Add") {
    $cname = trim($_POST["cname"]);
    $cfilename = str_replace(' ', '_', $cname) . basename($_FILES["clogo"]["name"]);
    $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/includes/assets/images/shipping_logos/";
    $target_file = $target_dir . $cfilename;
    $uploadOk = 1;
    $logo = "/includes/assets/images/shipping_logos/" . $cfilename;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
       
//    if (move_uploaded_file($_FILES["clogo"]["tmp_name"], $target_file)) {
        //echo "The file " . basename($_FILES["clogo"]["name"]) . " has been uploaded.";
        //$ins = "insert into cargo_agency set name='" . $cname . "'";
        $ins = "INSERT INTO `shipping_company` (`name`,`logo`) VALUES ('" . $cname . "','" . $logo . "')";
        mysqli_query($con, $ins);
        header('location:shipping_company_mgmt.php');
//    }
    
//    $ins = "insert into shipping_company set name='" . $_POST["cname"] . "'";
//    mysqli_query($con, $ins);
//    header('location:shipping_company_mgmt.php');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript">
            function verify()
            {
                var arrTmp = new Array();
                arrTmp[0] = checkcname();
                arrTmp[1] = validate_fileupload();
                var i;
                _blk = true;
                for (i = 0; i < arrTmp.length; i++)
                {
                    if (arrTmp[i] == false)
                    {
                        _blk = false;

                    }
                }

                if (_blk == true)
                {
                    return true;

                }
                else
                {
                    return false;
                }
            }

            function trim(str)
            {
                if (str != null)
                {

                    var i;
                    for (i = 0; i < str.length; i++)
                    {

                        if (str.charAt(i) != " ")
                        {

                            str = str.substring(i, str.length);
                            break;

                        }

                    }

                    for (i = str.length - 1; i >= 0; i--)
                    {
                        if (str.charAt(i) != " ")
                        {
                            str = str.substring(0, i + 1);
                            break;
                        }

                    }

                    if (str.charAt(0) == " ")
                    {
                        return "";
                    }

                    else
                    {
                        return str;
                    }

                }

            }

            function checkcname()
            {
                if (trim(document.frmcat.cname.value) == "")
                {
                    document.getElementById("lblcname").innerHTML = "Please enter shipping company";
                    return false;

                }
                else
                {
                    document.getElementById("lblcname").innerHTML = "";
                    return true;

                }
            }
            function validate_fileupload()
            {
                var lgt = document.getElementById('clogo').files.length;
                if (lgt > 0) {
                    var fileName = document.getElementById("clogo").value;
                    var allowed_extensions = new Array("jpg", "png", "gif", "jpeg");
                    var file_extension = fileName.split('.').pop(); // split function will split the filename by dot(.), and pop function will pop the last element from the array which will give you the extension as well. If there will be no extension then it will return the filename.
                    var chk = '';
                    for (var i = 0; i <= allowed_extensions.length; i++)
                    {
                        if (allowed_extensions[i] == file_extension)
                        {
                            chk = 'true'; // valid file extension
                        }
                    }
                    if (chk == 'true') {

                        return true;
                    } else {
                        document.getElementById("lblclogo").innerHTML = "Please upload shipping company logo 1";
                        return false;
                    }
                } else {
                    document.getElementById("lblclogo").innerHTML = "Please upload shipping company logo 2";
                    return false;
                }

            }
        </script>
    </head>
    <body>
        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
<?php include("includes/header_inner.php"); ?>
            <tr>
                <td height="5"></td>
            </tr>
            <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
<?php include("includes/shipping-left.php"); ?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pagetitle">Add Shipping Company</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <a class="pagetitle1" href="shipping_company_mgmt.php" onclick="this.blur();"><span> Manage Shipping Company</span></a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data">
                                                                <tr>
                                                                    <td><div id="box">
                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                                                                <tr>
                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Shipping Company</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                        <input type="text" class="textfieldbig" name="cname" id="cname" value="" />
                                                                                        <br><span class="error" id="lblcname"></span>	
                                                                                    </td>
                                                                                </tr>   
                                                                                <tr>

                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Shipping Company Logo</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                        <input type="file" name="clogo" id="clogo" />
                                                                                        <br><span class="error" id="lblclogo"></span>	

                                                                                    </td>

                                                                                </tr> 
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Add" /></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div></td>
                                                                </tr>
                                                            </form>
                                                        </table></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
<?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
