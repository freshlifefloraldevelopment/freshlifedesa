<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    //$userSessionID = $_GET['idgw'];
    $userSessionID = $_POST['codgrow'];
    
    $idfac = $_GET['b'];
    $cajastot = 0;

    // Datos del Buyer
    $buyerEntity = "select g.growers_name,g.boxes
                      from growers  g 
                     where g.id = '" . $userSessionID . "'     " ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    $str = trim($buy['boxes'], ",");
    
    //$boxnum = count($str);
    //$boxnum = array($str);
    
    $connections = explode(',', $str);
    $boxnum = count($connections);
    ////////////////////////////////////////////////////
      $a = substr($buy['boxes'], 0, -1);
      $a = substr($a, 1);
      
      
 $sel_boxes = " SELECT b.id,b.name,b.width,b.length,b.height ,bo.name AS name_box ,b.type,un.code,unt.name as name_unit,numbox
                  FROM boxes b 
                  LEFT JOIN  boxtype bo    ON  b.type=bo.id 
                  LEFT JOIN  units un       ON  b.type=un.id 
                  LEFT JOIN  units_type unt ON  un.type=unt.id 
                 WHERE  b.id IN(" . $a . ") AND bo.name in ('HB','QB','EB')  ";
 
           $rs_boxes = mysqli_query($con, $sel_boxes);
           $total_records = mysqli_num_rows($rs_boxes);  
   
   // Datos del Grower
   
   $sqlDetalis="select s.name as catego , p.id as pidvar ,p.name as variety , sz.name as sizename,'cm ',bs.name as bunname,'st/bu',bx.name,
                       bx.width  , bx.length , bx.height , bx.type   ,gpp.qty ,bt.name , bt.numbox,
                       gpp.id ,  gpp.growerid ,  gpp.prodcutid ,  gpp.sizeid ,  gpp.box_id , gpp.bunch_size_id ,  gpp.feature ,ff.name as featurename ,
                       p.box_type , pr.price
                  from growcard_prod_box_packing gpp
                 inner join growers g   on gpp.growerid = g.id 
                 inner join product p   on gpp.prodcutid = p.id 
                 inner join subcategory s  on p.subcategoryid = s.id
                 inner join sizes sz on gpp.sizeid = sz.id
                 inner join bunch_sizes bs on gpp.bunch_size_id=bs.id
                 inner join boxes bx on gpp.box_id=bx.id
                 inner join boxtype bt on bx.type=bt.id                 
                 inner join grower_product_box box on (gpp.growerid = box.grower_id and gpp.prodcutid = box.product_id and gpp.box_id = box.boxes )                 
                 inner join grower_product_price pr on (gpp.growerid = pr.growerid and gpp.prodcutid = pr.prodcutid and gpp.sizeid = pr.sizeid ) 
                  left join features ff  ON gpp.feature = ff.id
                 where gpp.growerid = '" . $userSessionID . "'
                 order by s.name , p.name , sz.name,(bx.width*bx.length*bx.height)  ";

        $result   = mysqli_query($con, $sqlDetalis);    

    $pdf = new PDF();
    
    //$pdf->AliasPages();   
    
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'GROWERS CARD',0,0,'L'); 
   // $pdf->Image('logo.png',1,5,22); 
    
    $pdf->Ln(15);    
                
    $pdf->SetFont('Arial','B',10);  
    
    $pdf->Cell(70,6,'Name: '.$buy['growers_name'],0,1,'L');
    $pdf->Cell(70,6,'Box Types: '.$total_records,0,1,'L');
    
            while ($box_sel = mysqli_fetch_array($rs_boxes)) {
               $pdf->Cell(70,6,$box_sel['numbox']." ".$box_sel['name'],0,1,'L');
           }
              
    $pdf->Ln(5);

    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Box Num',0,0,'C');
    $pdf->Cell(25,6,'Box Size',0,0,'C');    
    $pdf->Cell(35,6,'Stems/Bu',0,0,'C');
    $pdf->Cell(15,6,'Qty',0,0,'C'); 
    $pdf->Cell(15,6,'Price',0,1,'C');     
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    while($row = mysqli_fetch_assoc($result))  {
        
        
        
        $sel_boxg = "  select gpw.id ,  gpw.growerid ,  gpw.prodcutid ,  gpw.sizeid ,  gpw.box_id ,  gpw.bunch_size_id ,  gpw.bunch_qty ,  gpw.weight ,  
                              gpw.feature 
                         from growcard_prod_box_weight gpw
                        where gpw.growerid  = '" . $userSessionID . "'  
                          and gpw.prodcutid = '" . $row['pidvar'] . "'    
                          and gpw.sizeid    = '" . $row['sizeid'] . "'  
                          and gpw.box_id    = '" . $row['box_id'] . "'    
                          and gpw.bunch_size_id  = '" . $row['bunch_size_id'] . "' 
                          order by gpw.id     "; 
        
        $rs_boxg = mysqli_query($con,$sel_boxg);       
                  
        $cajas = 0;
        while($tot_boxg = mysqli_fetch_array($rs_boxg))  {
                $peso = $tot_boxg['weight'];
        }
        
        

                                if ($row['box_type'] == 0) {
                                    $tipo     = "Stems";
                                    $totalqty = $row['bunname']*$row['qty'];
                                } else {
                                    $tipo     = "Bunches";
                                    $totalqty = $row['qty'];
                                } 
                                
                                $idkg = round( (($row['width']*$row['length']*$row['height'])/6000),2);
            

  $pdf->SetFont('Arial','',8);
  
         $pdf->Cell(70,4,$row['catego']." ".$row['variety']." ".$row['sizename']." cm ".$row['bunname']." st/bu ".$row['featurename'],0,0,'L');            
         $pdf->Cell(25,6,$row['numbox'],0,0,'C');                              
         
         //$pdf->Cell(25,6,$boxnum,0,0,'C');                              
         $pdf->Cell(25,6,$row['width']."*".$row['length']."*".$row['height']." (".$idkg." kg)",0,0,'L'); 
         $pdf->Cell(35,6,$tipo,0,0,'C');                                                
         $pdf->Cell(15,6,$totalqty,0,0,'C');                                                            
         $pdf->Cell(15,6,$row['price'],0,1,'C');                                                            
            
            
    }

    
                            
  $pdf->Output();
  ?>