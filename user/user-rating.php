<?php	
        // po 04-ene-2021
	include "../config/config_gcp.php";
	session_start();

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}
	
	$qsel="select * from user_ratings order by ur_id";

	$rs=mysqli_query($con,$qsel);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<link href="css/demo_page.css" rel="stylesheet" type="text/css" />

<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />

<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					"bJQueryUI": true,
					"sPaginationType": "full_numbers"

				});

			} );

</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

<?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/grower-left-param.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>

                    <td height="5"></td>

                  </tr>

                  <tr>

                    <td class="pagetitle">Manage User & Rating</td>

                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

				  <tr>

                    <td>
			<table width="100%">
				<tr>

				    <td>
					<!--a href="user_rating_add.php" class="pagetitle" onclick="this.blur();"><span> + Add User & Rating</span></a-->
				    </td>

				</tr>
			</table>
		    </td>

                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

                  <tr>

                    <td><div id="box">

		<div id="container">			

			<div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>
			<th width="5%" align="center">Sr.</th>
                        
			<th width="10%" align="left">User Type</th>   
                        
                        <th width="15%" align="left">Grower Name</th>    
                        
                        <th width="25%" align="left">Comment</th>
                        
                        <th width="10%" align="left">Status</th>
                        
                        <th align="center" width="8%">Edit</th>
		</tr>

	</thead>

	<tbody>

		<?php	
				$sr=1;

			while($row_review=mysqli_fetch_array($rs)) {
                            
                            $growers_sql="select growers_name,id from growers where id='".$row_review['ur_grower_idFk']."'";
                            $rs_growers=mysqli_query($con,$growers_sql);
                            $row_growers=mysqli_fetch_array($rs_growers);
                                    
                                    if ($row_review['ur_status'] == 0) {
                                        $estado = 'Pending';
                                    }else{
                                        $estado = 'Approv.';
                                    }

                ?>

                                    <tr class="gradeU">
                                        <td align="center" class="text"><?php	echo $sr;?></td>
                                        
                                        <td class="text" align="left"><?php echo $row_review["ur_user_type"]." ".$row_review["last_name"]?></td>
                                        
                                        <td class="text" align="left"><?php echo $row_growers['growers_name']?></td>
                                        
                                        <td class="text" align="left"><?php echo $row_review['ur_comment']?></td>
                                        
                                        <td class="text" align="left"><?php echo $estado?></td>                                        
                                        
                                        <td align="center" ><a href="user_rating_edit.php?id=<?php echo $row_review["ur_id"]?>"><img src="images/edit.gif" border="0" alt="Edit" /></a></td>
                                    </tr>

			<?php	
                            $sr++;

			}
			 ?> 

	

	</tbody>

</table>



			</div>

			</div>



			</div>

		</td>

                  </tr>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php	 include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>

</body>

</html>
