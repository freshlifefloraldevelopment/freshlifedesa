<?php
include "../config/config_new.php";
if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}
if (isset($_POST["Submit"]) && $_POST["Submit"] == "Update") {
    $iname = trim($_POST["item_name"]);
    $ilink = trim($_POST["item_link"]);
    $iorder = trim($_POST["item_order"]);
    $imainitemid = trim($_POST["main_menu_item_name"]);
    
    $ins = "UPDATE `sub_menu` SET `m_menu_id` = ".$imainitemid." , `s_menu_name` = '" . $iname . "', `s_menu_link` = '" . $ilink . "', `s_menu_order` = '" . $iorder . "' WHERE s_menu_id='" . $_GET["id"] . "'";
    mysqli_query($con, $ins);
    header('location:sub_menu_mgmt.php');
}

$sel_menu_item = "select * from sub_menu where s_menu_id='" . $_GET["id"] . "'";
$rs_menu_item = mysqli_query($con, $sel_menu_item);
$item = mysqli_fetch_array($rs_menu_item);
//var_dump($item);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript">
            function verify()
            {
                var isValid = true;
                
                if (trim(document.form_menu.main_menu_item_name.value) == "")
                {
                    document.getElementById("lbl_main_menu_item_name").innerHTML = "Please select main menu item name";
                    isValid = false;
                }
                else
                {
                    document.getElementById("lbl_main_menu_item_name").innerHTML = "";
                }
                
                if (trim(document.form_menu.item_name.value) == "")
                {
                    document.getElementById("lbl_item_name").innerHTML = "Please enter menu item name";
                    isValid = false;
                }
                else
                {
                    document.getElementById("lbl_item_name").innerHTML = "";
                } 
                
                if (trim(document.form_menu.item_link.value) == "")
                {
                    document.getElementById("lbl_item_link").innerHTML = "Please enter menu item link";
                    isValid = false;
                }
                else
                {
                    document.getElementById("lbl_item_link").innerHTML = "";
                }
                
                if (trim(document.form_menu.item_order.value) == "")
                {
                    document.getElementById("lbl_item_order").innerHTML = "Please enter menu item order number";
                    isValid = false;
                }
                else
                {
                    document.getElementById("lbl_item_order").innerHTML = "";
                }
                
                return isValid;
            }

            function trim(str)
            {
                if (str != null)
                {

                    var i;
                    for (i = 0; i < str.length; i++)
                    {

                        if (str.charAt(i) != " ")
                        {

                            str = str.substring(i, str.length);
                            break;

                        }

                    }

                    for (i = str.length - 1; i >= 0; i--)
                    {
                        if (str.charAt(i) != " ")
                        {
                            str = str.substring(0, i + 1);
                            break;
                        }

                    }

                    if (str.charAt(0) == " ")
                    {
                        return "";
                    }

                    else
                    {
                        return str;
                    }

                }

            }


        </script>

    </head>



<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

 <?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>
    
  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/website-left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                  <tr>

                    <td height="5"></td>

                  </tr>

                  <tr>

                    <td class="pagetitle">Update Sub Menu Item</td>

                  </tr>
                  
                  <tr>

                    <td>&nbsp;</td>

                  </tr>      
                         
                  <tr>

                    <td>&nbsp;</td>

                  </tr>

                  <form name="form_menu" method="post" onsubmit="return verify();" enctype="multipart/form-data">

                    <tr>
                        <td><div id="box">
                                <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                    <tr>
                                        <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory</td>
                                    </tr>
                                    
                                    <tr>
                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Main Menu Item Name</td>
                                        <td width="66%" bgcolor="#f2f2f2">
                                            <select class="listmenu" name="main_menu_item_name" id="main_menu_item_name" style="width:230px; height: 22px;">
                                                <option value="">--Select--</option>
                                                <?php 
                                                    $sel_menu_item = "select * from main_menu";
                                                    $rs_menu_item = mysqli_query($con, $sel_menu_item);
                                                    while ($mitem = mysqli_fetch_array($rs_menu_item)) {
                                                  ?>
                                                  <option value="<?= $mitem["m_menu_id"] ?>" <?php if($item["m_menu_id"] == $mitem["m_menu_id"]) echo 'selected'; ?> ><?= $mitem["m_menu_name"] ?></option>
                                                  <?php
                                                  }
                                                ?>
                                            </select>
                                            <!--<input type="text" class="textfieldbig" name="from" id="from" value="" />-->
                                            <br> <span class="error" id="lbl_main_menu_item_name"></span>
                                        </td>

                                    </tr>
                                    
                                    <tr>
                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Main Menu Item Name</td>
                                        <td width="66%" bgcolor="#f2f2f2">
                                            <input type="text" class="textfieldbig" name="item_name" id="item_name" value="<?php echo ($item) ? $item['s_menu_name'] : ''; ?>" />
                                            <br><span class="error" id="lbl_item_name"></span>	

                                        </td>

                                    </tr>    
                                    <tr>
                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Main Menu Item Link</td>
                                        <td width="66%" bgcolor="#f2f2f2">
                                            <input type="text" class="textfieldbig" name="item_link" id="item_link" value="<?php echo ($item) ? $item['s_menu_link'] : ''; ?>" />
                                            <br><span class="error" id="lbl_item_link"></span>	

                                        </td>

                                    </tr>    
                                    <tr>
                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Main Menu Item Order</td>
                                        <td width="66%" bgcolor="#f2f2f2">
                                            <input type="text" class="textfieldbig" name="item_order" id="item_order" value="<?php echo ($item) ? $item['s_menu_order'] : ''; ?>" />
                                            <br><span class="error" id="lbl_item_order"></span>	

                                        </td>

                                    </tr>    
                                    
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><input name="Submit" type="Submit" class="buttongrey" value="Update" /></td>
                                    </tr>
                                </table>
                            </div></td>
                    </tr>
                </form>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>

</table>

</body>

</html>

