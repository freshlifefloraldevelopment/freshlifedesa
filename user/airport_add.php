<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";


if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}
if (isset($_POST["Submit"]) && $_POST["Submit"] == "Add") {
   // print_r($_POST);
     $airport_name = $_POST['airport_name'];
     $airport_code = $_POST['airport_code'];
     $airport_address = $_POST['airport_addresss'];
     $insert = "insert into airports (airport_name,airport_code,airport_address) values ( '".$airport_name."','".$airport_code."','".$airport_address."' )";
    if(mysqli_query($con, $insert)){
         header("location: manage_airports.php");
    }else{
        
    }
}

$info = '';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript">
            function verify()
            {
                var arrTmp = new Array();
                arrTmp[0] = checkfrom();
                arrTmp[1] = checkto();
                var i;

                _blk = true;

                for (i = 0; i < arrTmp.length; i++)
                {

                    if (arrTmp[i] == false)
                    {
                        _blk = false;

                    }

                }

                if (_blk == true)
                {
                    return true;
                }

                else
                {
                    return false;
                }

            }

            function trim(str)
            {
                if (str != null)
                {
                    var i;
                    for (i = 0; i < str.length; i++)
                    {
                        if (str.charAt(i) != " ")
                        {
                            str = str.substring(i, str.length);
                            break;

                        }
                    }

                    for (i = str.length - 1; i >= 0; i--)
                    {
                        if (str.charAt(i) != " ")
                        {
                            str = str.substring(0, i + 1);
                            break;
                        }

                    }

                    if (str.charAt(0) == " ")
                    {
                        return "";
                    }

                    else
                    {
                        return str;
                    }

                }

            }

            function checkfrom()
            {
                if (trim(document.frmcat.airport_name.value) == "")
                {
                    document.getElementById("lblfrom").innerHTML = "Please enter Airport Name ";
                    return false;
                }
                else
                {
                    document.getElementById("lblfrom").innerHTML = "";
                    return true;
                }

            }

            function checkto()
            {
                if (trim(document.frmcat.airport_code.value) == "")
                {
                    document.getElementById("lblto").innerHTML = "Please enter Airport Code";
                    return false;
                }
                else
                {
                    document.getElementById("lblto").innerHTML = "";
                    return true;
                }

            }
        </script>
    </head>
    <body>
        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
<?php include("includes/header_inner.php"); ?>
            <tr>
                <td height="5"></td>
            </tr>
            <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
<?php include("includes/shipping-left.php"); ?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pagetitle">Add New Connection </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td><table width="100%">
                                                                        <tr>
                                                                            <td><a class="pagetitle1" href="manage_airports.php" onclick="this.blur();"><span> Manage Connections </span></a></td>
                                                                        </tr>
                                                                    </table></td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>

                                                            <tr>
                                                                <td><div id="box">
                                                                        <form name="frmcat" action="airport_add.php" method="post" onsubmit="return verify();" enctype="multipart/form-data">
                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                                                                <tr>
                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>                                    </tr>
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Airport Name </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="airport_name" id="airport_name" value="" />
                                                                                        <br>
                                                                                            <span class="error" id="lblfrom"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Code </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="airport_code" id="airport_code" value="" />
                                                                                        <br>
                                                                                            <span class="error" id="lblto"></span></td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;Airport Address</td>
                                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                                        <input type="text" class="textfieldbig" name="airport_addresss" id="airport_addresss" value="" />

                                                                                        <br>
                                                                                            <span class="error" id="lblunit"></span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Add" /></td>
                                                                                </tr>
                                                                            </table>
                                                                        </form>    
                                                                    </div></td>
                                                            </tr>

                                                        </table></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
<?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
