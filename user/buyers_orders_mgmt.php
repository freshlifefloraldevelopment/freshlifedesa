<?php 	

// PO

include "../config/config.php";

@session_start();

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
{

header("location: index.php");

}
   $qsel = "SELECT  s.name AS subs , p.name , sh.name AS sizename , gpb.comment , gpb.lfd , gpb.qty , gpb.boxtype , rg.gprice AS price,
                      gpb.buyer,gpb.id AS cartid,gpb.id_order,gpb.order_serial,gpb.cod_order,gpb.product,gpb.sizeid,gpb.shpping_method,gpb.unseen,
                      p.color_id,rg.tax AS tax,rg.handling,gor.offer_id 
                FROM buyer_requests gpb
               INNER JOIN request_growers rg     ON gpb.id=rg.rid
                LEFT JOIN product p              ON gpb.product = p.id
                LEFT JOIN subcategory s          ON p.subcategoryid=s.id  
                LEFT JOIN features ff            ON gpb.feature=ff.id
                LEFT JOIN grower_offer_reply gor ON rg.rid=gor.offer_id AND rg.gid=gor.grower_id
                LEFT JOIN sizes sh               ON gpb.sizeid=sh.id                                                                                                          
               WHERE rg.gid='" . $_GET["id"] . "' AND gpb.lfd>='" . date("Y-m-d") . "'  and   IFNULL(gor.reject,'-1')<>'1' "; 
                                                                                                            
$qsel.= " GROUP BY gpb.id ";
$qsel.=" ORDER BY gpb.id DESC";        
$rs=mysql_query($qsel);


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>Admin Area</title>

        <link href="css/style.css" rel="stylesheet" type="text/css" />
        
        <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        
        <script type="text/javascript" charset="utf-8">       
        


            $(document).ready(function () {

                oTable = $('#example').dataTable({
                    //"sScrollXInner": "130%",

                    "bJQueryUI": true,
                    //"sScrollY": "536",

                    "sPaginationType": "full_numbers"

                });

            });

        </script>

    </head>

    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php  include("includes/header_inner.php");?>

            <tr>

                <td height="5"></td>

            </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php  include("includes/left.php");?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>

                                                                <td height="5"></td>

                                                            </tr>

                                                            <tr>

                                                                <td class="pagetitle">Buyer Requests</td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>




                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <tr>

                                                                <td><div id="box">

                                                                        <div id="container">			

                                                                            <div class="demo_jui">

                                                                                <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

                                                                                    <thead>

                                                                                        <tr>

                                                                                            <th width="22%" align="left">Product</th> 

                                                                                            <th align="center" width="8%">Comment</th>
                                                                                            
                                                                                            <th align="center" width="8%">Destiny</th>
                                                                                            
                                                                                            <th align="center" width="8%">LFD</th>

                                                                                            <th align="center" width="8%">Boxes/Stems</th>
                                                                                            
                                                                                            <th align="center" width="8%">Price</th>                                                                                            

                                                                                        </tr>

                                                                                    </thead>

                                                                                    <tbody>

                                                                                        <?php 

                                                                                        $sr=1;

                                                                                        while($product=mysql_fetch_array($rs))  {
                                                                                                                                                                                        
                                                                                        $sel_box_type = "SELECT * FROM boxtype WHERE id='" .$product["boxtype"] . "'";
                                                                                        $rs_box_type = mysql_query($sel_box_type);
                                                                                        $box_type = mysql_fetch_array($rs_box_type);
                                                                                        
                                                                                        $buyre_detail_sql = "SELECT * FROM buyer_shipping_methods WHERE shipping_method_id='" . $product['shpping_method'] . "'";
                                                                                        $rs_buyre_detail = mysql_query($buyre_detail_sql);
                                                                                        $row_buyre = mysql_fetch_array($rs_buyre_detail);

                                                                                        $country_sql = "SELECT * FROM country WHERE id='" . $row_buyre['country'] . "'";
                                                                                        $rs_country_detail = mysql_query($country_sql);
                                                                                        $row_country = mysql_fetch_array($rs_country_detail);                                                                                        

                                                                                        ?>
                                                                                        <tr class="gradeU">

                                                                                            <td class="text" align="left"><?php  echo $product["subs"]."  ".$product["name"]."  ".$product["sizename"]."  cm." ?></td>

                                                                                            <td class="text" align="left"><?php  echo $product["comment"] ?></td>
                                                                                            
                                                                                            <td class="text" align="center">
                                                                                                <img src="<?php echo SITE_URL; ?>includes/assets/images/flags/<?php echo $row_country['flag'] ?>" width="25"/>
                                                                                            </td>                                                                                            

                                                                                            <td class="text" align="left"><?php  echo $product["lfd"] ?></td>
                                                                                                                                                                                       
                                                                                            <td class="text" align="center"><?php  echo $product["qty"]."  ".$box_type["name"] ?></td>
                                                                                            
                                                                                            <td class="text" align="right"><?php  echo $product["price"] ?></td>

                                                                                        </tr>

                                                                                        <?php 

                                                                                        $sr++;

                                                                                        }

                                                                                        ?> 



                                                                                    </tbody>

                                                                                </table>



                                                                            </div>

                                                                        </div>



                                                                    </div>

                                                                </td>

                                                            </tr>

                                                        </table></td>

                                                    <td width="10">&nbsp;</td>

                                                </tr>

                                            </table></td>

                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

                                    </tr>

                                    <tr>

                                        <td background="images/middle-leftline.gif"></td>

                                        <td>&nbsp;</td>

                                        <td background="images/middle-rightline.gif"></td>

                                    </tr>

                                    <tr>

                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

                                        <td background="images/middle-bottomline.gif"></td>

                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

                                    </tr>

                                </table></td>

                        </tr>

                    </table></td>

            </tr>

            <tr>

                <td height="10"></td>

            </tr>

            <?php  include("includes/footer-inner.php"); ?>

            <tr>

                <td>&nbsp;</td>

            </tr>

        </table>

    </body>

</html>
