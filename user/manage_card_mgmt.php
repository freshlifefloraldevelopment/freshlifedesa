<?php
	include "../config/config_gcp.php";
                
	session_start();
	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}
	
	$qsel="select gs.subcaegoryid , gs.categoryid , s.name as scubcategory , 
                      c.name as category 
                 from grower_subcategory gs 
	        inner join subcategory s on gs.subcaegoryid = s.id
	        inner join category    c on gs.categoryid    = c.id
                where grower_id = '".$_GET["id"]."'  
	        group by gs.subcaegoryid , gs.categoryid ";
	
	 $rs=mysqli_query($con,$qsel);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});

			} );

</script>
</head>
<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
  <?php include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <?php include("includes/grower-left-param.php");?>
          <td width="5">&nbsp;</td>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="5"></td>
                          </tr>
                          <tr>
                            <td class="pagetitle">Manage Card</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><div id="box">
                                <div id="container">
                                  <div class="demo_jui">
                                    <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                      <thead>
                                        <tr>
                                          <th width="22%" align="left">Category</th>
                                          <th width="22%" align="left">Subcategory</th>
                                          <th align="center" width="22%">Manage Card</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                <?php
						  	$sr=1;

				while($product=mysqli_fetch_array($rs))	  {
						     
				?>
                                        <tr class="gradeU">
                                            <td class="text" align="left"><?php echo $product["category"]?></td>
                                            <td class="text" align="left"><?php echo $product["scubcategory"]?></td>
                                            <td align="center" ><a href="manage_card_parameters.php?ids=<?php echo $product["subcaegoryid"]."&id_grow=".$_GET["id"]."&id_cate=".$product["categoryid"]?>" style="color:#000; font-weight:bold;" >Manage Card Parameters</a></td>
                                        </tr>
                                <?php
					$sr++;
				}
				?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div></td>
                          </tr>
                        </table></td>
                      <td width="10">&nbsp;</td>
                    </tr>
                  </table></td>
                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
              </tr>
              <tr>
                <td background="images/middle-leftline.gif"></td>
                <td>&nbsp;</td>
                <td background="images/middle-rightline.gif"></td>
              </tr>
              <tr>
                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                <td background="images/middle-bottomline.gif"></td>
                <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <?php include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
