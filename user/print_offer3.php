<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_GET['id_buy'];
    $idfac = $_GET['b'];
    $cajastot = 0;

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $userSessionID . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id , buyer_id , order_number
                         from buyer_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Requests
   
   $sqlDetalis="select gor.id , gor.offer_id , gor.offer_id_index, gor.grower_id , gor.size , 
                       (gor.price) as salesPriceCli , g.product ,
                       gor.steams , gor.product as prod_name,gor.boxqty,
                       (gor.bunchqty) as bunchqty   , gor.boxtype     ,
                       substr(rg.growers_name,1,19) as name_grower
                  from grower_offer_reply gor
                 inner join buyer_requests g on gor.offer_id=g.id 
                 inner join growers rg     ON gor.grower_id = rg.id                  
                 where g.id_order = '" . $id_fact_cab . "'
                   and gor.buyer_id = '" . $buyer_cab . "'
                 order by gor.product , id ";

   //                   group by gor.product , offer_id  , offer_id_index , size , gor.steams , gor.price      
   
        $result   = mysqli_query($con, $sqlDetalis);    

    $pdf = new PDF();
    //$pdf->AliasPages();   
    
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'OFFERS-VARIETY',0,0,'L'); 
   // $pdf->Image('logo.png',1,5,22); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Cell(0,10,'Shipping Details ',0,0,'R');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,0,'L');
    $pdf->Cell(0,6,'Invoice #: '.$buyerOrderCab['order_number'],0,1,'R');  
    
   
    
    if ($userSessionID != 318) {
       $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');
    }else{
       $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');  
    }
          
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,1,'L');        
    $pdf->Cell(70,6,'-'.$buy['company'],0,1,'L');  
    
    $pdf->Ln(10);

    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Stem/Bunch',0,0,'C');
    $pdf->Cell(25,6,'Sale Price',0,0,'C');    
    $pdf->Cell(25,6,'Qty',0,0,'C');
    $pdf->Cell(25,6,'Subtotal',0,1,'C'); 
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = "";
    
    while($row = mysqli_fetch_assoc($result))  {
        
        
         // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type from product where name = '" . $row['prod_name'] . "' ";
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
            
              if ($bunch_stem['box_type'] == 0) {
                    $Subtotal= $row['steams'] * $row['bunchqty'] * $row['salesPriceCli'];
                    $qtyFac = $row['bunchqty']*$row['steams'];
                    $unitFac = "STEMS";                     
              }else{
                    $Subtotal=  $row['bunchqty'] * $row['salesPriceCli'];
                    $qtyFac = $row['bunchqty']; 
                    $unitFac = "BUNCHES";                   
              }        
                        
               $subtotalStems= $row['steams'] * $row['bunchqty'] ;  
         
         if ($row['prod_name'] != $tmp_idorder) {
               $pdf->SetFont('Arial','B',8);                   
               if ($sw == 1) { 
                      $pdf->Cell(70,6,"  ",0,1,'L');                       
                      $pdf->Cell(70,6,"Total Stems Variety : ".$subStemsGrower,0,1,'L');                                 

               }
                      $sw = 1;             
                      $pdf->Cell(70,6,"  ",0,1,'L'); 

                      $pdf->Cell(70,6,$row['prod_name'],0,1,'L');   

                      $pdf->Cell(70,6,"  ",0,1,'L'); 
                      $subStemsGrower = 0;                      
                      $subTotalGrower = 0;                       
         }

  $pdf->SetFont('Arial','',8);
  
         $pdf->Cell(70,4,$row['prod_name']." ".$row['size']." cm ".$row['steams']." st/bu",0,0,'L');            
         $pdf->Cell(25,6,$unitFac,0,0,'C');                              
         $pdf->Cell(25,6,number_format($row['salesPriceCli'], 2, '.', ','),0,0,'C'); 

if ($bunch_stem['box_type'] == 0) {         
         $pdf->Cell(25,6,$row['bunchqty']*$row['steams'],0,0,'C');                                       
}else{
         $pdf->Cell(25,6,$row['bunchqty'],0,0,'C');                                           
}         

         $pdf->Cell(25,6,$row['name_grower'],0,1,'L');  
         
            $totalCal = $totalCal + $Subtotal;
            $totalStems = $totalStems + $subtotalStems;            
            $tmp_idorder = $row['prod_name'];
            $subStemsGrower= $subStemsGrower + ($row['steams'] *$row['bunchqty']) ; 
            $subTotalGrower= $subTotalGrower + ($row['steams'] *$row['bunchqty']*$row['salesPriceCli']) ;             
    }

    
                      $pdf->SetFont('Arial','B',8);                   
                      $pdf->Cell(70,6,"  ",0,1,'L');                       
                      $pdf->Cell(70,6,"Total Stems Variety : ".$subStemsGrower,0,1,'L'); 

    
    $pdf->Ln(2);
    
    $pdf->SetFont('Arial','B',10);             
    $pdf->Cell(35,6,'Total Stems:'.number_format($totalStems, 0, '.', ','),0,1,'R');              

                            
  $pdf->Output();
  ?>