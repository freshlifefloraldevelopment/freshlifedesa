<?php	
	include "../config/config_new.php";
	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
	{
		header("location: index.php");
	}
	if(isset($_GET['delete'])) 
	{
	  $query = 'DELETE FROM vouchers WHERE  v_id= '.(int)$_GET['delete'];
	  mysqli_query($con,$query);
	}

	$v_sql="select vouchers.*, buyers.first_name, buyers.last_name from vouchers left join buyers on vouchers.v_buyer_id = buyers.id order by v_id asc";
	$v_res=mysqli_query($con,$v_sql);
	//var_dump($v_res);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					//"sScrollXInner": "130%",

					"bJQueryUI": true,

					//"sScrollY": "536",

					"sPaginationType": "full_numbers"

				});

			} );

</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
  <?php include("includes/header_inner.php");?>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <?php include("includes/left.php");?>
          <td width="5">&nbsp;</td>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10">&nbsp;</td>
                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="5"></td>
                          </tr>
                          <tr>
                            <td class="pagetitle">Manage Discount Vouchers</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><table width="100%">
                                <tr>
                                  <td><a href="add_voucher.php" class="pagetitle" onclick="this.blur();"><span> + Add Discount Voucher </span></a></td>
                                </tr>
                              </table></td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><div id="box">
                                <div id="container">
                                  <div class="demo_jui">
                                    <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                      <thead>
                                        <tr>
                                          <th width="8%" align="center">Sr.</th>
                                          <th width="12%" align="left">Voucher Code</th>
                                          <th align="center" width="8%">Voucher Value</th>
                                          <th align="center" width="9%">Valid Till</th>
                                          <th align="center" width="12%">Buyer</th>
                                          <th align="center" width="9%">Created Date</th>
                                          <th align="center" width="9%">Updated Date</th>
                                          <th align="center" width="8%">Edit</th>
                                          <th align="center" width="8%">Delete</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php

						  	$sr=1;

						  while($vouchers=mysqli_fetch_array($v_res))

						  {

						  ?>
                                        <tr class="gradeU">
                                          <td align="center" class="text"><?=$sr;?></td>
                                          <td class="text" align="left"><?=$vouchers["v_code"]?></td>
                                          <td class="text" align="left"><?=$vouchers["v_value"]?></td>
                                          <td class="text" align="left"><?=date("d-m-Y", strtotime($vouchers["v_valid_till"]))?></td>
                                          <td class="text" align="left"><?=$vouchers["first_name"].' '.$vouchers["last_name"]?></td>
										  <td class="text" align="left"><?=date("d-m-Y", strtotime($vouchers["v_created_at"]))?></td>
										  <td class="text" align="left"><?=date("d-m-Y", strtotime($vouchers["v_updated_at"]))?></td>
                                          <td align="center" ><a href="edit_voucher.php?id=<?=$vouchers["v_id"]?>"><img src="images/edit.gif" border="0" alt="Edit" /></a></td>
                                          <td align="center" ><a href="?delete=<?=$vouchers["v_id"]?>"  onclick="return confirm('Are you sure, you want to delete this voucher ?');"><img src="images/delete.gif" border="0" alt="Delete" /></a></td>
                                        </tr>
                                        <?php

						 		$sr++;

						 	}

						 ?>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div></td>
                          </tr>
                        </table></td>
                      <td width="10">&nbsp;</td>
                    </tr>
                  </table></td>
                <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
              </tr>
              <tr>
                <td background="images/middle-leftline.gif"></td>
                <td>&nbsp;</td>
                <td background="images/middle-rightline.gif"></td>
              </tr>
              <tr>
                <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                <td background="images/middle-bottomline.gif"></td>
                <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
  <?php include("includes/footer-inner.php"); ?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
