<?php

// Verificacion con Fecha

include "../config/config_gcp.php";

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}

if (isset($_GET['sid']) && !empty($_GET['sid'])) {
    $cid = $_GET['sid'];
} else {
    header("location: connections.php");
}

$get_connection = "select * from connections where id=" . $cid;
$rs_info = mysqli_query($con, $get_connection);
$get_connection_info = mysqli_fetch_array($rs_info);
if (isset($_POST["Submit"]) && $_POST["Submit"] == "save") {

    $minimum_weight = '';
    if ($_POST["type"] == 2) {
        $minimum_weight = $_POST['minimum_weight'];
    }
    $temp = "";
    $temp = implode(',', $_POST['chk']);
    $charges_per_kilo = '';
    $charges_per_k_sum=0;
    $charges_per_shipment = '';
    if ($_POST["type"] == 1 || $_POST["type"] == 2 || $_POST["type"] == 3) {
        $to = $_POST["to"];
        $key = $_POST['charges_per_kilo_key'];
        $val = $_POST['charges_per_kilo_value'];
        $cnt = count($_POST['charges_per_kilo_key']);
        for ($i = 0; $i < $cnt; $i++) {
            if (!empty($key[$i])) {
                $charges_per_kilo[$key[$i]] = $val[$i];
                $charges_per_k_sum=$charges_per_k_sum+$val[$i];
            }
        }
        $charges_per_kilo = serialize($charges_per_kilo);

        $keys = $_POST['charges_per_shipment_key'];
        $vals = $_POST['charges_per_shipment_value'];
        $cnts = count($_POST['charges_per_shipment_key']);

        for ($j = 0; $j < $cnts; $j++) {
            if (!empty($keys[$j])) {
                $charges_per_shipment[$keys[$j]] = $vals[$j];
            }
        }
        $charges_per_shipment = serialize($charges_per_shipment);
        $addtional_rate = "0.00";
        $rate = "0.00";
        $price_per_box = "0.00";
    } else if ($_POST["type"] == 4) {
        $rate = round($_POST["price_cubic_feet"], 2);
        $addtional_rate = "0.00";
        $price_per_box = "0.00";
        $to = 0;
    }

    $sql = "SELECT id,connections,source_country,destination_country FROM shipping_method WHERE connections REGEXP '.*;s:[0-9]+:\"" . $cid . "\".*'";
    $qry = mysqli_query($con, $sql);
    while ($dt = mysqli_fetch_assoc($qry)) {
        //print_r($dt['id']);
        $cons = unserialize($dt['connections']);
        $con_count = count($cons);
        $destination_country = '';
        $source_country = '';
        $key = '';
        if ($con_count == 1) {
            if ($_POST["type"] == 1 || $_POST["type"] == 2) {
                $getAirport = "SELECT airport_country,airport_name FROM airports WHERE airport_id='" . $_POST["from"] . "'";
                $airRes = mysqli_query($con, $getAirport);
                $airportArray = array();
                $fromData = mysqli_fetch_assoc($airRes);
                $source_country = $fromData['airport_country'];

                $getAirportTo = "SELECT airport_country,airport_name FROM airports WHERE airport_id='" . $to . "'";
                $airResTo = mysqli_query($con, $getAirportTo);
                $toData = mysqli_fetch_assoc($airResTo);
                $destination_country = $toData['airport_country'];
            } else if ($_POST["type"] == 3) {
                $getAirport = "SELECT airport_country,airport_name FROM airports WHERE airport_id='" . $_POST["from"] . "'";
                $airRes = mysqli_query($con, $getAirport);
                $fromData = mysqli_fetch_assoc($airRes);
                $source_country = $fromData['airport_country'];
                $destination_country_to = $to;
                $getdestination_country_id = "SELECT * FROM country WHERE id='" . $destination_country_to . "'";
                $getdestination_country_idres = mysqli_query($con, $getdestination_country_id);
                $getdestination_country_data = mysqli_fetch_assoc($getdestination_country_idres);
                $destination_country = $getdestination_country_data['id'];
            } else if ($_POST["type"] == 4) {
                $getAirport = "SELECT airport_country,airport_name FROM airports WHERE airport_id='" . $_POST["from"] . "'";
                $airRes = mysqli_query($con, $getAirport);
                $airportArray = array();
                $fromData = mysqli_fetch_assoc($airRes);
                $source_country = $fromData['airport_country'];

                $destination_country = '0';
            }
        } else {
            $key = array_search($cid, $cons);
            $key = str_replace('connection_', '', $key);
            if ($key == '1') {
                $getAirport = "SELECT airport_country,airport_name FROM airports WHERE airport_id='" . $_POST["from"] . "'";
                $airRes = mysqli_query($con, $getAirport);
                $fromData = mysqli_fetch_assoc($airRes);
                $source_country = $fromData['airport_country'];
                $destination_country = $dt['destination_country'];
            } else if ($key == $con_count) {
                if ($_POST["type"] == 1 || $_POST["type"] == 2) {
                    $source_country = $dt['source_country'];

                    $getAirportTo = "SELECT airport_country,airport_name FROM airports WHERE airport_id='" . $to . "'";
                    $airResTo = mysqli_query($con, $getAirportTo);
                    $toData = mysqli_fetch_assoc($airResTo);
                    $destination_country = $toData['airport_country'];

                } else if ($_POST["type"] == 3) {
                    $source_country = $dt['source_country'];

                    $destination_country_to = $to;
                    $getdestination_country_id = "SELECT * FROM country WHERE id='" . $destination_country_to . "'";
                    $getdestination_country_idres = mysqli_query($con, $getdestination_country_id);
                    $getdestination_country_data = mysqli_fetch_assoc($getdestination_country_idres);
                    $destination_country = $getdestination_country_data['id'];

                } else if ($_POST["type"] == 4) {
                    $source_country = $dt['source_country'];
                    $destination_country = '0';

                }
            }
        }


        $updateQry = "UPDATE `shipping_method` SET `source_country` = '" . $source_country . "',  `destination_country` = '" . $destination_country . "' WHERE `id` = '" . $dt['id'] . "'";
        mysqli_query($con, $updateQry);
    }

    $update = "UPDATE `connections`
    SET `connection_name`       = '" . $_POST['connection_name'] . "',
        `from1`                 = '" . $_POST["from"] . "',
        `unit`                  = '" . $_POST["unit"] . "',
        `to1`                   = '" . $to . "',
        `cargo_agency`          = '" . $_POST["cargo_agency"] . "',
        `shipping_company`      = '" . $_POST["shipping_company"] . "',
        `days`                  = '" . $temp . "',
        `trasit_time`           = '" . $_POST["trasit_time"] . "',
        `type`                  = '" . $_POST["type"] . "',
        `box_weight_arranged`   = '" . $_POST["box_weight_arranged"] . "',
        `price_per_box`         = '" . $_POST["price_per_box"] . "',
        `price_extra_kilo`      = '" . $_POST["price_extra_kilo"] . "',
        `kilograms`             = '" . $_POST["kilograms"] . "',
        `rate`                  = '" . $_POST["rate"] . "',
        `fuel_surcharge`        = '" . $_POST["fuel_surcharge"] . "',
        `security`              = '" . $_POST["security"] . "',
        `awb`                   = '" . $_POST["awb"] . "',
        `fee`                   = '" . $_POST["fee"] . "',
        `fitosnary_fee`         = '" . $_POST["fitosnary_fee"] . "',
        `due_agnet`             = '" . $_POST["due_agnet"] . "',
        `minimum_cubic_feet`    = '" . $_POST["minimum_cubic_feet"] . "',
        `price_cubic_feet`      = '" . $_POST["price_cubic_feet"] . "',
        `price_per_box2`        = '" . $_POST["price_per_box2"] . "',
        `shipping_rate`         = '" . $rate . "',
        `addtional_rate`        = '" . $addtional_rate . "',
        `box_price`             = '" . $price_per_box . "',
        `charges_per_kilo`      = '" . $charges_per_kilo . "',
        `charges_per_shipment`  = '" . $charges_per_shipment . "',
        `minimum_weight`        = '" . $minimum_weight .  "',
        `gross_weight`          = '" . $_POST["gross"] .  "',
        `volume_weight`         = '" . $_POST["volume"] . "' ,
        `destiny`               = '" . $_POST["destiny"] . "',       
        `inventary`             = '" . $_POST["inventary"] . "'                   
  WHERE `id` = '" . $cid . "';";

    mysqli_query($con, $update);
    header('location:connections.php');
}
?>
<?php include("header.php"); ?>
<script type="text/javascript">
    $(document).ready(function () {
        var maxField = 10; //Input fields increment limitation
        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var fieldHTML = '<tr><td width="34%" align="left" valign="middle" class="text"><input type="text" name="charges_per_kilo_key[]" value=""/></td><td width="66%" bgcolor="#f2f2f2"><input type="text" name="charges_per_kilo_value[]" value=""/><input type="button" value="-"  class="remove_button" /><input type="button" class="add_button" value="+" onclick="add();" /><br><span class="error" id="lblkilograms"></span></td></tr>'
        wrapper.on('click', '.remove_button', function (e) { //Once remove button is clicked
            e.preventDefault();
            $(this).parent('td').parent().remove(); //Remove field html
        });
    });
    function add() {
        var wrapper1 = $('.field_wrapper'); //Input field wrapper
        //var fieldHTML = '<div><input type="text" name="charges_per_kilo[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="remove-icon.png"/></a></div>'; //New input field html 
        var fieldHTML1 = '<tr><td width="34%" align="left" valign="middle" class="text"><input type="text" name="charges_per_kilo_key[]" value=""/></td><td width="66%" bgcolor="#f2f2f2"><input type="text" name="charges_per_kilo_value[]" value=""/><input type="button" value="-"  class="remove_button" /><input type="button" class="add_button" value="+" onclick="add();" /><br><span class="error" id="lblkilograms"></span></td></tr>'
        wrapper1.append(fieldHTML1);
    }

    $(document).ready(function () {
        var maxField = 10; //Input fields increment limitation
        var addButton = $('.add_button2'); //Add button selector
        var wrapper = $('.field_wrapper2'); //Input field wrapper
        var fieldHTML = '<tr><td width="34%" align="left" valign="middle" class="text"><input type="text" name="charges_per_shipment_key[]" value=""/></td><td width="66%" bgcolor="#f2f2f2"><input type="text" name="charges_per_shipment_value[]" value=""/><input type="button" value="-"  class="remove_button2" /><input type="button" class="add_button2" value="+" onclick="add_shipment();" /><br><span class="error" id="lblkilograms"></span></td></tr>'
        wrapper.on('click', '.remove_button2', function (e) { //Once remove button is clicked
            e.preventDefault();
            $(this).parent('td').parent().remove(); //Remove field html
        });
    });
    function add_shipment() {
        var wrapper1 = $('.field_wrapper2'); //Input field wrapper
        //var fieldHTML = '<div><input type="text" name="charges_per_kilo[]" value=""/><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="remove-icon.png"/></a></div>'; //New input field html 
        var fieldHTML1 = '<tr><td width="34%" align="left" valign="middle" class="text"><input type="text" name="charges_per_shipment_key[]" value=""/></td><td width="66%" bgcolor="#f2f2f2"><input type="text" name="charges_per_shipment_value[]" value=""/><input type="button" value="-"  class="remove_button2" /><input type="button" class="add_button2" value="+" onclick="add_shipment();" /><br><span class="error" id="lblkilograms"></span></td></tr>'
        wrapper1.append(fieldHTML1);
    }


    $(document).ready(function () {
        $('select').select2();
        $('#from').select2({
            ajax: {
                url: "search_airport.php",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        s: 'from',
                    };
                },
                results: function (data, page) {
                    return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 1,
        });
        $('#to').select2({
            ajax: {
                url: "search_airport.php",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        s: 'to',
                        type: $('#type1div input[type="radio"]:checked:first').val(),
                    };
                },
                results: function (data, page) {
                    return {results: data.results};
                },
                cache: true
            },
            minimumInputLength: 1,
        });
    });
    function verify() {
        var arrTmp = new Array();
        arrTmp[0] = checkfrom();
        //arrTmp[1] = checkto();
        var i;
        _blk = true;
        for (i = 0; i < arrTmp.length; i++) {

            if (arrTmp[i] == false) {
                _blk = false;
            }
        }
        if (_blk == true) {
            return true;
        }

        else {
            return false;
        }

    }

    function trim(str) {
        if (str != null) {
            var i;
            for (i = 0; i < str.length; i++) {
                if (str.charAt(i) != " ") {
                    str = str.substring(i, str.length);
                    break;
                }
            }
            for (i = str.length - 1; i >= 0; i--) {
                if (str.charAt(i) != " ") {
                    str = str.substring(0, i + 1);
                    break;
                }
            }
            if (str.charAt(0) == " ") {
                return "";
            }
            else {
                return str;
            }

        }

    }

    function checkfrom() {
        if (trim(document.frmcat.from.value) == "") {
            document.getElementById("lblfrom").innerHTML = "Please enter Source City ";
            return false;
        }
        else {
            document.getElementById("lblfrom").innerHTML = "";
            return true;
        }
    }

    function checkto() {
        if (trim(document.frmcat.to.value) == "") {
            document.getElementById("lblto").innerHTML = "Please enter Destination ";
            return false;
        }
        else {
            document.getElementById("lblto").innerHTML = "";
            return true;
        }
    }

    function doshowtype() {
        $('#spdiv').hide();
        $('#airdiv2').hide();

        if (1) {
            $('#typediv').show();
            $('#cargodiv').show();
            $('#type1div').show();
            $('#type2div').hide();

        }
        else {
            $('#typediv').show();
            $('#cargodiv').hide();
            $('#type2div').show();
            $('#type1div').hide()
        }
    }

    function perbox() {
        $('#type1').hide();
        $('#type2').show();
    }

    function percubicfeet() {
        $('#type2').hide();
        $('#type1').show();
    }

    function doshowother2() {
        $('#fedexdiv').show();
        $('#airdiv2').show();
        $('#spdiv').hide();
        $('#airlinediv').hide();
        $('#unit_tr').hide();
        $('#minimum_weight').hide();

        $('#to_local').show();
        $('#transit').show();
        $('#transit_days').show();
    }

    function doshowother() {
        $('#airdiv2').show();
        $('#spdiv').hide();
        $('#airlinediv').show();
        $('#unit_tr').hide();
        $('#fedexdiv').show();

        $('#to_local').show();
        $('#transit').show();
        $('#transit_days').show();
        $('#minimum_weight').show();
    }

    function doshowother3() {
        $('#airdiv2').hide();
        $('#spdiv').hide();
        $('#airlinediv').hide();
        $('#unit_tr').hide();
        $('#fedexdiv').hide();
        $('#minimum_weight').hide();

        $('#to_local').hide();
        $('#transit').hide();
        //$('#transit_days').hide();
    }

    function dohideother() {
        $('#spdiv').hide();
        $('#unit_tr').show();
        $('#airdiv2').show();
        $('#airlinediv').hide();
        $('#fedexdiv').show();
        $('#minimum_weight').hide();

        $('#to_local').show();
        $('#transit').show();
        $('#transit_days').show();

    }


</script>
<?php include("includes/shipping-left.php"); ?>
<td width="5">&nbsp;</td>
<td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80"/></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="10">&nbsp;</td>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="5"></td>
                                </tr>
                                <tr>
                                    <td class="pagetitle">Edit Connection</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td><a class="pagetitle1" href="connections.php" onclick="this.blur();"><span> Manage Connections </span></a></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <td>
                                        <div id="box">
                                            <form name="frmcat" action="" method="post" onsubmit="return verify();" enctype="multipart/form-data">
                                                <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                                    <tr>
                                                        <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Connection Name</td>
                                                        <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="connection_name" id="connection_name" value="<?php echo $get_connection_info['connection_name'] ?>"/>
                                                            <br>
                                                            <span class="error" id="connection_name"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>From</td>
                                                        <td width="66%" bgcolor="#f2f2f2">
                                                            <select class="listmenu" name="from" id="from" style="width:230px;">
                                                                <option value="">--Select--</option>
                                                                <?php
                                                                echo $sel_airport_from = "select * from airports where airport_id='" . $get_connection_info['from1'] . "'";
                                                                $rs_airport_from = mysqli_query($con, $sel_airport_from);
                                                                while ($airports_from = mysqli_fetch_array($rs_airport_from)) {
                                                                    ?>
                                                                    <option value="<?php echo $airports_from["airport_id"] ?>" <?php echo ($get_connection_info['from1'] == $airports_from["airport_id"]) ? 'selected="selected"' : ''; ?>><?php echo $airports_from["airport_name"] ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                            <!--<input type="text" class="textfieldbig" name="from" id="from" value="" />-->
                                                            <br>
                                                            <span class="error" id="lblfrom"></span></td>
                                                    </tr>

                                                    <!--                                                                                                                   <tr>
                                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Mode of Transport </td>
                                                      <td width="66%" bgcolor="#f2f2f2">
                                                      <select class="listmenu" name="mode_of_transport" id="mode_of_transport" style="width:150px;" onchange="doshowtype();">
                                                      <option value="">--Select--</option>
                                                      <option value="1">Air</option>
                                                      <option value="2">Truck</option>
                                                      </select>

                                                        <br>
                                                        <span class="error" id="lblmode_of_transport"></span></td>
                                                    </tr>-->
                                                    <tr id="typediv">
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Type</td>
                                                        <td width="66%" bgcolor="#f2f2f2">

                                                            <div id="type1div">
                                                                <input type="radio" name="type" id="type" value="1" <?php echo ($get_connection_info['type'] == 1) ? 'checked' : ''; ?> onClick="dohideother();"/>&nbsp; Special Condition ( Consolidation
                                                                ) <br/>
                                                                <input type="radio" name="type" id="type" value="2" <?php echo ($get_connection_info['type'] == 2) ? 'checked' : ''; ?> onClick="doshowother();"/>&nbsp; Single Air Way Bill <br/>
                                                                <input type="radio" name="type" id="type3" value="3" <?php echo ($get_connection_info['type'] == 3) ? 'checked' : ''; ?> onClick="doshowother2();"/>&nbsp; Fedex <br/>
                                                                <input type="radio" name="type" id="type4" value="4" <?php echo ($get_connection_info['type'] == 4) ? 'checked' : ''; ?> onClick="doshowother3();"/>&nbsp; Local Delivery <br/>
                                                            </div>

                                                            <div id="type2div" style="display: none">
                                                                <input type="radio" name="type" id="type" value="3" onClick="perbox();"/>&nbsp; Per Box <br/>
                                                                <input type="radio" name="type" id="type" value="4" onClick="percubicfeet();"/>&nbsp; Per Cubic Feet <br/>
                                                            </div>

                                                            <br>
                                                            <span class="error" id="lbltype"></span></td>
                                                    </tr>

                                                    <tr id="cargodiv">
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Cargo Agency</td>
                                                        <td width="66%" bgcolor="#f2f2f2">
                                                            <select class="listmenu" name="cargo_agency" id="cargo_agency" style="width:230px;">
                                                                <option value="">--Select--</option>
                                                                <?php
                                                                $sel_cargo_agency = "select * from cargo_agency order by name";
                                                                $rs_carg_agency = mysqli_query($con, $sel_cargo_agency);
                                                                while ($cargo_agency = mysqli_fetch_array($rs_carg_agency)) {
                                                                    ?>

                                                                    <option value="<?php echo $cargo_agency["id"] ?>" <?php echo ($get_connection_info['cargo_agency'] == $cargo_agency["id"]) ? 'selected="selected"' : ''; ?> ><?php echo $cargo_agency["name"] ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>

                                                            <br>
                                                            <span class="error" id="lblcargo_agency"></span></td>
                                                    </tr>
                                                    
                                                    <tr id="destinydiv">
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Destiny</td>
                                                        <td width="66%" bgcolor="#f2f2f2">
                                                            <select class="listmenu" name="destiny" id="destiny" style="width:230px;">
                                                                <option value="">--Select--</option>
                                                                <?php
                                                                $sel_destiny = "select * from destiny order by descripcion";
                                                                $rs_destiny = mysqli_query($con, $sel_destiny);
                                                                while ($dest = mysqli_fetch_array($rs_destiny)) {
                                                                    ?>

                                                                    <option value="<?php echo $dest["id"] ?>" <?php echo ($get_connection_info['destiny'] == $dest["id"]) ? 'selected="selected"' : ''; ?> ><?php echo $dest["descripcion"] ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>

                                                            <br>
                                                            <span class="error" id="lbldestiny"></span></td>
                                                    </tr>                                                    
                                                    

                                                    <tr id="inventarydiv">
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Inventary</td>
                                                        <td width="66%" bgcolor="#f2f2f2">
                                                            <select class="listmenu" name="inventary" id="inventary" style="width:230px;">
                                                                <option value="">--Select--</option>
                                                                <?php
                                                                $sel_inven = "select * from inventary_growers order by name";
                                                                $rs_inven = mysqli_query($con, $sel_inven);
                                                                while ($inventary = mysqli_fetch_array($rs_inven)) {
                                                                    ?>

                                                                    <option value="<?php echo $inventary["id"] ?>" <?php echo ($get_connection_info['inventary'] == $inventary["id"]) ? 'selected="selected"' : ''; ?> ><?php echo $inventary["name"] ?></option>

                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>

                                                            <br>
                                                            <span class="error" id="lblinventary"></span></td>
                                                    </tr>  
                                                    
                                                    <tr id="airlinediv">
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Airline or Transport Company</td>
                                                        <td width="66%" bgcolor="#f2f2f2">
                                                            <select class="listmenu" name="shipping_company" id="shipping_company" style="width:230px;">
                                                                <option value="">--Select--</option>
                                                                <?php
                                                                $sel_cargo_agency = "select * from shipping_company order by name";
                                                                $rs_carg_agency = mysqli_query($con, $sel_cargo_agency);
                                                                while ($cargo_agency = mysqli_fetch_array($rs_carg_agency)) {
                                                                    ?>
                                                                    <option value="<?php echo $cargo_agency["id"] ?>" <?php echo ($get_connection_info['shipping_company'] == $cargo_agency["id"]) ? 'selected="selected"' : ''; ?> ><?php echo $cargo_agency["name"] ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                            <br>
                                                            <span class="error" id="lblshipping_company"></span></td>
                                                    </tr>                                                    
                                                    
                                                    <tr id="to_local" <?php echo ($get_connection_info['type'] == 4) ? 'style="display:none;"' : ''; ?> >
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>To</td>
                                                        <td width="66%" bgcolor="#f2f2f2">
                                                            <select class="listmenu" name="to" id="to" style="width:230px;">
                                                                <option value="">--Select--</option>
                                                                <?php
                                                                if ($get_connection_info['type'] == 1 || $get_connection_info['type'] == 2) {
                                                                    $sel_airport_to = "select * from airports where airport_id='" . $get_connection_info['to1'] . "'";
                                                                    $rs_airport_to = mysqli_query($con, $sel_airport_to);
                                                                    while ($airports_to = mysqli_fetch_array($rs_airport_to)) {
                                                                        ?>
                                                                        <option value="<?php echo $airports_to["airport_id"] ?>" <?php echo ($get_connection_info['to1'] == $airports_to["airport_id"]) ? 'selected="selected"' : ''; ?>><?php echo $airports_to["airport_name"] ?></option>
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    $sel_airport_to = "select * from country where id=" . $get_connection_info['to1'];
                                                                    $rs_airport_to = mysqli_query($con, $sel_airport_to);
                                                                    while ($airports_to = mysqli_fetch_array($rs_airport_to)) {
                                                                        ?>
                                                                        <option value="<?php echo $airports_to["id"] ?>" <?php echo ($get_connection_info['to1'] == $airports_to["id"]) ? 'selected="selected"' : ''; ?>><?php echo $airports_to["name"] ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                            <!--<input type="text" class="textfieldbig" name="to" id="to" value="" />-->
                                                            <br>
                                                            <span class="error" id="lblto"></span></td>
                                                    </tr>
                                                    
                                                    
                                                 <!--   <tr id="transit" <?php echo ($get_connection_info['type'] == 4) ? 'style="display:none;"' : ''; ?> >
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Transit Days</td>
                                                        <td width="66%" bgcolor="#f2f2f2">
                                                            <select class="listmenu" name="trasit_time" id="trasit_time" style="width:230px;">
                                                                <option value="">--Select--</option>
                                                                <option value="0" <?php echo ($get_connection_info['trasit_time'] == 0) ? 'selected="selected"' : ''; ?> >1</option>                                                 
                                                                <option value="1" <?php echo ($get_connection_info['trasit_time'] == 1) ? 'selected="selected"' : ''; ?> >1</option>
                                                                <option value="2" <?php echo ($get_connection_info['trasit_time'] == 2) ? 'selected="selected"' : ''; ?> >2</option>
                                                                <option value="3" <?php echo ($get_connection_info['trasit_time'] == 3) ? 'selected="selected"' : ''; ?> >3</option>
                                                                <option value="4" <?php echo ($get_connection_info['trasit_time'] == 4) ? 'selected="selected"' : ''; ?> >4</option>
                                                                <option value="5" <?php echo ($get_connection_info['trasit_time'] == 5) ? 'selected="selected"' : ''; ?> >5</option>
                                                                <option value="6" <?php echo ($get_connection_info['trasit_time'] == 6) ? 'selected="selected"' : ''; ?> >6</option>
                                                                <option value="7" <?php echo ($get_connection_info['trasit_time'] == 7) ? 'selected="selected"' : ''; ?> >7</option>
                                                                <option value="8" <?php echo ($get_connection_info['trasit_time'] == 8) ? 'selected="selected"' : ''; ?> >8</option>                                                                
                                                                <option value="9" <?php echo ($get_connection_info['trasit_time'] == 9) ? 'selected="selected"' : ''; ?> >9</option>
                                                                <option value="10" <?php echo ($get_connection_info['trasit_time'] == 10) ? 'selected="selected"' : ''; ?> >10</option> 
                                                                <option value="11" <?php echo ($get_connection_info['trasit_time'] == 11) ? 'selected="selected"' : ''; ?> >11</option>
                                                                <option value="12" <?php echo ($get_connection_info['trasit_time'] == 12) ? 'selected="selected"' : ''; ?> >12</option>
                                                                <option value="13" <?php echo ($get_connection_info['trasit_time'] == 13) ? 'selected="selected"' : ''; ?> >13</option>
                                                                <option value="14" <?php echo ($get_connection_info['trasit_time'] == 14) ? 'selected="selected"' : ''; ?> >14</option>
                                                                <option value="15" <?php echo ($get_connection_info['trasit_time'] == 15) ? 'selected="selected"' : ''; ?> >15</option>
                                                                <option value="16" <?php echo ($get_connection_info['trasit_time'] == 16) ? 'selected="selected"' : ''; ?> >16</option>
                                                                <option value="17" <?php echo ($get_connection_info['trasit_time'] == 17) ? 'selected="selected"' : ''; ?> >17</option>
                                                                <option value="18" <?php echo ($get_connection_info['trasit_time'] == 18) ? 'selected="selected"' : ''; ?> >18</option>                                                                
                                                                <option value="19" <?php echo ($get_connection_info['trasit_time'] == 19) ? 'selected="selected"' : ''; ?> >19</option>
                                                                <option value="20" <?php echo ($get_connection_info['trasit_time'] == 20) ? 'selected="selected"' : ''; ?> >20</option> 
                                                                <option value="21" <?php echo ($get_connection_info['trasit_time'] == 21) ? 'selected="selected"' : ''; ?> >21</option>
                                                                <option value="22" <?php echo ($get_connection_info['trasit_time'] == 22) ? 'selected="selected"' : ''; ?> >22</option>
                                                                <option value="23" <?php echo ($get_connection_info['trasit_time'] == 23) ? 'selected="selected"' : ''; ?> >23</option>
                                                                <option value="24" <?php echo ($get_connection_info['trasit_time'] == 24) ? 'selected="selected"' : ''; ?> >24</option>
                                                                <option value="25" <?php echo ($get_connection_info['trasit_time'] == 25) ? 'selected="selected"' : ''; ?> >25</option>
                                                                <option value="26" <?php echo ($get_connection_info['trasit_time'] == 26) ? 'selected="selected"' : ''; ?> >26</option>
                                                                <option value="27" <?php echo ($get_connection_info['trasit_time'] == 27) ? 'selected="selected"' : ''; ?> >27</option>
                                                                <option value="28" <?php echo ($get_connection_info['trasit_time'] == 28) ? 'selected="selected"' : ''; ?> >28</option>                                                                
                                                                <option value="29" <?php echo ($get_connection_info['trasit_time'] == 29) ? 'selected="selected"' : ''; ?> >29</option>
                                                                <option value="30" <?php echo ($get_connection_info['trasit_time'] == 30) ? 'selected="selected"' : ''; ?> >30</option>                                                                                                                                                                                                
                                                            </select>
                                                            <br>
                                                            <span class="error" id="lbltrasit_time"></span></td>
                                                    </tr>
                                                    <tr id="transit_days">
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Days Avialable</td>
                                                        <td>
                                                            <div><?php $days = explode(',', $get_connection_info['days']); ?>
                                                                <input name="chk[]" <?php
                                                                if (in_array(1, $days)) {
                                                                    echo 'checked';
                                                                }
                                                                ?> id="chk1" type="checkbox" value="1"/>&nbsp;&nbsp;Monday<br/>
                                                                <input name="chk[]" <?php
                                                                if (in_array(2, $days)) {
                                                                    echo 'checked';
                                                                }
                                                                ?> id="chk2" type="checkbox" value="2"/>&nbsp;&nbsp;Tuesday<br/>
                                                                <input name="chk[]" <?php
                                                                if (in_array(3, $days)) {
                                                                    echo 'checked';
                                                                }
                                                                ?> id="chk3" type="checkbox" value="3"/>&nbsp;&nbsp;Wednesday<br/>
                                                                <input name="chk[]" <?php
                                                                if (in_array(4, $days)) {
                                                                    echo 'checked';
                                                                }
                                                                ?> id="chk4" type="checkbox" value="4"/>&nbsp;&nbsp;thursday<br/>
                                                                <input name="chk[]" <?php
                                                                if (in_array(5, $days)) {
                                                                    echo 'checked';
                                                                }
                                                                ?> id="chk5" type="checkbox" value="5"/>&nbsp;&nbsp;Friday<br/>
                                                                <input name="chk[]" <?php
                                                                if (in_array(6, $days)) {
                                                                    echo 'checked';
                                                                }
                                                                ?> id="chk6" type="checkbox" value="6"/>&nbsp;&nbsp;Saturday<br/>
                                                                <input name="chk[]" <?php
                                                                if (in_array(0, $days)) {
                                                                    echo 'checked';
                                                                }
                                                                ?> id="chk0" type="checkbox" value="0"/>&nbsp;&nbsp;Sunday<br/>
                                                            </div>
                                                        </td>
                                                    </tr>-->
                                                    
                                                    


                                                    <tr id="day_conn_more">
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Days Avialable</td>
                                                        <td width="66%" bgcolor="#f2f2f2">
                                                        <a href="more_days_conn.php?id=<?php echo $get_connection_info["id"]?>" style="color:#000; font-weight:bold;" >+ Add days...</a></td>                                                                                                                                                                
                                                    </tr>  
                                                    
                                                    
                                                    

                                                    <tr id="minimum_weight" <?php echo ($get_connection_info['type'] != 2) ? 'style="display:none;"' : ''; ?>>
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Minimum Weight</td>
                                                        <td width="66%" bgcolor="#f2f2f2">
                                                            <input type="text" class="textfieldbig" id="minimum_weight1" name="minimum_weight" value="<?php echo $get_connection_info['minimum_weight'] ?>"/>
                                                            <br>
                                                            <span class="error" id="lblshipping_company"></span></td>
                                                    </tr>
                                                    <tr id="unit_tr" <?php echo ($get_connection_info['type'] == 1) ? '' : 'style="display:none;"'; ?>>
                                                        <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Unit</td>
                                                        <td width="66%" bgcolor="#f2f2f2">
                                                            <select class="listmenu" name="unit" id="unit" style="width:230px;">
                                                                <option value="">--Select--</option>
                                                                <option value="lbs" <?php echo ($get_connection_info['unit'] == 'lbs') ? 'selected="selected"' : ''; ?>>Pounds (lbs)</option>
                                                                <option value="kg" <?php echo ($get_connection_info['unit'] == 'kg') ? 'selected="selected"' : ''; ?>>Kilograms (kg)</option>
                                                                <option value="ft3" <?php echo ($get_connection_info['unit'] == 'ft3') ? 'selected="selected"' : ''; ?>>Cubic Foot (ft3)</option>
                                                            </select>
                                                            <br>
                                                            <span class="error" id="lblunit"></span></td>
                                                    </tr>
                                                    <!--                                    <tr>
                                                      <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Unit</td>
                                                      <td width="66%" bgcolor="#f2f2f2">
                                                      <select class="listmenu" name="unit" id="unit" style="width:150px;">
                                                      <option value="">--Select--</option>
                                                      <option value="1">Weight</option>
                                                      <option value="2">Cubic Feet</option>
                                                      </select>
                                                     
                                                        <br>
                                                        <span class="error" id="lblunit"></span></td>
                                                    </tr>-->
                                                    <tr style="display:none;" id="spdiv">
                                                        <td colspan="2" style="padding-left:20px;">
                                                            <br/> <b> Special Consolidation :</b> <br/>
                                                            <table style="width:100%">

                                                                <tr>
                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Box Weight Arranged</td>
                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="box_weight_arranged" id="box_weight_arranged"
                                                                                                             value="<?php echo $get_connection_info['box_weight_arranged'] ?>"/>
                                                                        <br>
                                                                        <span class="error" id="lblbox_weight_arranged"></span></td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Price Per Box</td>
                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="price_per_box" id="price_per_box" value="<?php echo $get_connection_info['price_per_box'] ?>"/>
                                                                        <br>
                                                                        <span class="error" id="lblprice_per_box"></span></td>
                                                                </tr>

                                                                <tr>
                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Price Extra Kilo.</td>
                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="price_extra_kilo" id="price_extra_kilo"
                                                                                                             value="<?php echo $get_connection_info['price_extra_kilo'] ?>"/>
                                                                        <span class="error" id="lblprice_extra_kilo"></span></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr <?php echo ($get_connection_info['type'] == 2 || $get_connection_info['type'] == 3 || $get_connection_info['type'] == 1) ? '' : 'style="display:none;"'; ?> id="airdiv2">
                                                        <td colspan="2" style="padding-left:20px;">
                                                            <br/> <b> Single Air Way Bill :</b> <br/>
                                                            <table style="width:100%" class="field_wrapper">
                                                                <tr>
                                                                    <td colspan="2"><br/><b>Charges Per Kilo</b></td>
                                                                </tr>
                                                                <?php
                                                                $get_charges_per_kilo = unserialize($get_connection_info['charges_per_kilo']);
                                                                if (!empty($get_charges_per_kilo)) {
                                                                    foreach ($get_charges_per_kilo as $key => $value) {
                                                                        ?>
                                                                        <tr>
                                                                            <td width="34%" align="left" valign="middle" class="text"><input type="text" name="charges_per_kilo_key[]" value="<?php echo $key; ?>"/></td>
                                                                            <td width="66%" bgcolor="#f2f2f2">
                                                                                <input type="text" name="charges_per_kilo_value[]" value="<?php echo $value; ?>"/><input type="button" value="-" class="remove_button"/>
                                                                                <br>
                                                                                <span class="error" id="lblkilograms"></span>
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                                <tr>
                                                                    <td width="34%" align="left" valign="middle" class="text"><input type="text" name="charges_per_kilo_key[]" value=""/></td>
                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                        <input type="text" name="charges_per_kilo_value[]" value=""/>
                                                                        <input type="button" class="add_button" value="+" onclick="add();"/>
                                                                        <br>
                                                                        <span class="error" id="lblkilograms"></span>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                            <table>
                                                                <tr>
                                                                    <td colspan="2"><br/><b>Charges Per Shipment</b></td>
                                                                </tr>
                                                                <table class="field_wrapper2" style="width:100%">
                                                                    <?php
                                                                    $get_charges_per_shipment = unserialize($get_connection_info['charges_per_shipment']);
                                                                    if (!empty($get_charges_per_shipment)) {
                                                                        foreach ($get_charges_per_shipment as $keys => $values) {
                                                                            ?>
                                                                            <tr>
                                                                                <td width="34%" align="left" valign="middle" class="text"><input type="text" name="charges_per_shipment_key[]" value="<?php echo $keys; ?>"/></td>
                                                                                <td width="66%" bgcolor="#f2f2f2">
                                                                                    <input type="text" name="charges_per_shipment_value[]" value="<?php echo $values; ?>"/><input type="button" value="-" class="remove_button2"/>
                                                                                    <br>
                                                                                    <span class="error" id="lblkilograms"></span>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <tr>
                                                                        <td width="34%" align="left" valign="middle" class="text"><input type="text" name="charges_per_shipment_key[]" value=""/></td>
                                                                        <td width="66%" bgcolor="#f2f2f2">
                                                                            <input type="text" name="charges_per_shipment_value[]" value=""/>
                                                                            <input type="button" class="add_button" value="+" onclick="add_shipment();"/>
                                                                            <br>
                                                                            <span class="error" id="lblkilograms"></span>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </table>
                                                        </td>
                                                    </tr>

                                            <!--        <tr id="fedexdiv">
                                                        <td colspan="2" style="padding-left:20px;">
                                                            <br/> <b> Weight :</b> <br/>
                                                            <table style="width:100%">
                                                                <tr>
                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Gross</td>
                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="gross" id="gross" value="<?php echo $get_connection_info['gross_weight'] ?>"/>
                                                                        <span class="error" id="lblawb"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Volume</td>
                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="volume" id="volume" value="<?php echo $get_connection_info['volume_weight'] ?>"/> <span
                                                                                class="error" id="lbldue_agnet"></span></td>
                                                                </tr>


                                                            </table>
                                                        </td>
                                                    </tr>  -->

                                                    <tr style="display:none;" id="type1">
                                                        <td colspan="2">
                                                            <b>Price per Cubic Feet </b> <br/>
                                                            <table style="width:100%">
                                                                <tr>
                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Minimum Cubic Feet</td>
                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                        <input type="text" class="textfieldbig" name="minimum_cubic_feet" id="minimum_cubic_feet" value="<?php echo $get_connection_info['minimum_cubic_feet'] ?>"/>
                                                                        <br>
                                                                        <span class="error" id="lblminimum_cubic_feet"></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Price Per Cubic Feet</td>
                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                        <input type="text" class="textfieldbig" name="price_cubic_feet" id="price_cubic_feet" value="<?php echo $get_connection_info['price_cubic_feet'] ?>"/>
                                                                        <br>
                                                                        <span class="error" id="lblprice_cubic_feet"></span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr style="display:none;" id="type2">
                                                        <td colspan="2">
                                                            <b> Price per Box </b> <br/>
                                                            <table style="width:100%">
                                                                <tr>
                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Price per Box</td>
                                                                    <td width="66%" bgcolor="#f2f2f2">
                                                                        <input type="text" class="textfieldbig" name="price_per_box2" id="price_per_box2" value="<?php echo $get_connection_info['price_per_box2'] ?>"/> <br>
                                                                        <span class="error" id="lblprice_per_box2"></span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td colspan="2"><input name="Submit" type="Submit" class="buttongrey" value="save"/></td>
                                                    </tr>
                                                </table>
                                            </form>
                                        </div>
                                    </td>
                                </tr>

                            </table>
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80"/></td>
        </tr>
        <?php include("footer.php"); ?>
