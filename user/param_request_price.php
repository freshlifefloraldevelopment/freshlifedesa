<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {

    header("location: index.php");

}

if ($_SESSION['grower_id'] != 0) {
    header("location: growers.php?id=" . $_SESSION['grower_id']);
}

if (isset($_POST['change_ppic'])) {
    $today = date('mdyHis');
    if ($_FILES['ppic']['name'] != "") {
        $tmp1 = $_FILES['ppic']['name'];
        $ext1 = explode('.', $tmp1);
        $image = 0;
        $uploaddir = '../includes/assets/profile_pictures/';
        $filename = $today . "-" . $ext1[0] . "." . $ext1[1];
        $uploadfile1 = $uploaddir . $filename;

        move_uploaded_file($_FILES['ppic']['tmp_name'], $uploadfile1);
        $sql = "UPDATE admin SET picture = '" . $filename . "' WHERE id = " . $_SESSION['tomodachi-admin'];
        $res = mysqli_query($con, $sql);
        if ($res) {
            header("Location:home.php");
        }
    }

}


$sql_data = "SELECT uname, report as picture FROM admin WHERE id= " . $_SESSION['tomodachi-admin'] . " AND isadmin = 1";
$data_res = mysqli_query($con, $sql_data);
$row = mysqli_fetch_assoc($data_res);

$pic_path = SITE_URL . 'user/packing1.png';

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Admin Area</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>    
    
    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery/jquery-2.2.3.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery/jquery.plugin.js"></script>    
    <script type="text/javascript" src="assets/js/app.js"></script>    

</head>


<body>
    <form name="forma" id="forma" action="print_request_price_detal.php" method="post" >
        
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

    <?php include("includes/header_inner.php"); ?>
    <tr>
        <td height="5"></td>
    </tr>
    <tr>

        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr>

                    <?php include("includes/left.php"); ?>

                    <td width="5">&nbsp;</td>

                    <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>

                                <td width="10" valign="top" background="images/images_front/middle-leftline.gif"><img src="images/images_front/middle-topleft.gif" width="10" height="80"/></td>

                                <td valign="top" background="images/images_front/middle-topshade.gif"
                                    style="background-repeat:repeat-x;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                        <tr>

                                            <td width="10">&nbsp;</td>

                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                    <tr>
                                                        <td height="5"></td>

                                                    </tr>

                                                    <tr>
                                                        <td class="pagetitle">REQUEST VS PRICE (Details)</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>
                                                        <td><img src="<?php echo $pic_path; ?>" alt="Profile Picture"
                                                                 width="140" height="140"/></td>

                                                    </tr>
                                                    
                        <tr>
                                <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> &nbsp; Buyer:</td>
                                <td width="66%" bgcolor="#f2f2f2">
                                    
                                        <select name="codbuy" id="codbuy" class="form-control select2 cls_filter">
                                              <option value=""> -- Select Buyer -- </option>
                                          <?php
                        		         $sel_buy="select id,first_name,last_name 
                                                              from buyers 
                                                             where active = 'active' 
                                                             order by id ";
                                                 
        					 $rs_buy=mysqli_query($con,$sel_buy);
						 while($idbuyer=mysqli_fetch_array($rs_buy)) {
                                           ?>
                                                <option value="<?php echo $idbuyer["id"]?>">
                                                    <?php echo $idbuyer["first_name"]." ".$idbuyer["last_name"]?>
                                               </option>
                                          <?php } ?>
                                        </select>
                        </tr>                                                                                                          
                                                    

                                                    
                        <tr>
                                <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> &nbsp; Requests:</td>
                                <td width="66%" bgcolor="#f2f2f2">
                                   
                                        <select multiple name="codgrow[]">      
                                              <option value=""> -- Select Request -- </option>
                                          <?php
                        		         $sel_grow="select bo.id        , bo.buyer_id    , bo.order_number, bo.order_date     , bo.shipping_method, bo.del_date, 
                                                                   bo.date_range, bo.order_serial, bo.seen        , bo.delivery_dates , bo.lfd_grower     , 
                                                                   bo.qucik_desc as descrip      , b.first_name   , b.last_name       , bo.join_order
                                                              from buyer_orders bo
                                                             inner join buyers b  ON bo.buyer_id = b.id          
                                                             where bo.del_date > date_add(curdate(), INTERVAL -60 DAY) 
                                                             order by bo.id desc";
        					 $rs_grow=mysqli_query($con,$sel_grow);
						 while($idgrower=mysqli_fetch_array($rs_grow)) {
                                           ?>
                                                <option value="<?php echo $idgrower["id"]?>">
                                                    <?php echo $idgrower["order_number"]." ".$idgrower["first_name"]." ".$idgrower["last_name"]." ".$idgrower["del_date"]."  (".$idgrower["id"].")"?>
                                               </option>
                                          <?php } ?>
                                        </select>
                        </tr>     
                        
                    
                        
                        <tr>
                                <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> &nbsp; Products:</td>
                                <td width="66%" bgcolor="#f2f2f2">
                                    
                                        <select name="codprod[]" id="codprod" class="ui fluid search dropdown" multiple="">
                                              <option value=""> -- Select Products -- </option>
                                          <?php
                        		         $sel_prod="select br.id_order , br.product , p.name as nameprod, s.name as subcategory
                                                              from buyer_requests br
                                                             inner join product      p on br.product=p.id 
                                                             inner join category    pc on p.categoryid=pc.id 
                                                             inner join subcategory  s on p.subcategoryid=s.id  
                                                             where br.id_order >= 850
                                                               and br.product != 0
                                                             group by p.name";
                                                 
        					 $rs_prod =mysqli_query($con,$sel_prod);
						 while($codigoId=mysqli_fetch_array($rs_prod)) {
                                           ?>
                                                <option value="<?php echo $codigoId["product"]?>">
                                                    <?php echo $codigoId["nameprod"]." ".$codigoId["subcategory"]?>
                                               </option>
                                          <?php } ?>
                                        </select>
                        </tr>                                                                              
                                                    
                        <!--tr>                        
                                <td align="left" class="text" valign="top">Fecha Inicial : </td>
                        </tr>                    
                        
                        <tr>                                                  
                                <td bgcolor="#f2f2f2" class="text"><input type="date" name="fecha_ini" id="fecha_ini" />
                        </tr>                    
                        
                        <tr>                        
                                <td align="left" class="text" valign="top">Fecha Final : </td>
                        </tr>                    
                        
                        <tr>                                                  
                                <td bgcolor="#f2f2f2" class="text"><input type="date" name="fecha_fin" id="fecha_fin" />
                        </tr-->                                            

                                <input type="hidden" name="order" id="order" value="">
                        
                        
                            <tr> 
                              <button type="submit" class="btn btn-3d btn-purple" style="background-color:#06B120!important;">Print <i class="fa fa-chevron-right"></i></button>
                            </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                </table>
                                            </td>

                                            <td width="10">&nbsp;</td>

                                        </tr>
                                    </table>
                                </td>

                                <td width="10" valign="top" background="images/images_front/middle-rightline.gif"><img
                                            src="images/images_front/middle-topright.gif" width="10" height="80"/></td>

                            </tr>

                            <tr>

                                <td background="images/images_front/middle-leftline.gif"></td>

                                <td>&nbsp;</td>

                                <td background="images/images_front/middle-rightline.gif"></td>

                            </tr>

                            <tr>

                                <td height="10"><img src="images/images_front/middle-bottomleft.gif" width="10" height="10"/></td>

                                <td background="images/images_front/middle-bottomline.gif"></td>

                                <td><img src="images/images_front/middle-bottomright.gif" width="10" height="10"/></td>

                            </tr>

                        </table>
                    </td>

                </tr>

            </table>
        </td>

    </tr>

    <tr>

        <td height="10"></td>

    </tr>

    <?php include("includes/footer-inner.php"); ?>

    <tr>

        <td>&nbsp;</td>

    </tr>

</table>

</body>
</form>

</html>
<script>
    
    function  verify() {
        alert ("hola");
           var order_val ="";
        //var order_val = $('#filter_order :selected').val();
        //$("#order").val($("#codvend option:selected").html());

         order_val = $('#codvend :selected').val();

        alert(" xxxxxx");
        
        if (order_val==""){

            alert ("Please choose an option to proceed");
            return false;
        }
        else{
            return true;
        }

    }    
    

</script>

