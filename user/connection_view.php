<?php
include "../config/config_gcp.php";

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}

//    $sel_info="select * from shipping_method where id='".$_GET["id"]."'";
//	$rs_info=mysqli_query($con,$sel_info);
//	$info=mysqli_fetch_array($rs_info);

$sel_connection = "select * from connections where id='" . $_GET["sid"] . "'";
$rs_connection = mysqli_query($con, $sel_connection);
$connection = mysqli_fetch_array($rs_connection);

$avdays = explode(",", $connection["days"]);
$result = array_unique($avdays);
$cunav = sizeof($result);


$darrray = "";
for ($io = 0; $io <= $cunav - 1; $io++) {
    if ($result[$io] != "") {

        if ($result[$io] == "0") {
            $darrray.="Sunday,";
        } else if ($result[$io] == "1") {
            $darrray.="Monday,";
        } else if ($result[$io] == "2") {
            $darrray.="Tuesday,";
        } else if ($result[$io] == "3") {
            $darrray.="Wednesday,";
        } else if ($result[$io] == "4") {
            $darrray.="Thursday,";
        } else if ($result[$io] == "5") {
            $darrray.="Friday,";
        } else if ($result[$io] == "6") {
            $darrray.="Saturday,";
        }
    }
}


$darrray = trim($darrray, ",");
?>
<?php include("header.php"); ?>
<?php include("includes/shipping-left.php"); ?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pagetitle">View Connection  </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td><table width="100%">
                                                                        <tr>
                                                                            <td><a class="pagetitle1" href="connections.php" onclick="this.blur();"><span> Manage Connections </span></a></td>
                                                                        </tr>
                                                                    </table></td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>

                                                            <tr>
                                                                <td><div id="box">
                                                                        <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                                                            <tr>
                                                                                <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>                                    </tr>
                                                                            <tr>
                                                                                <td width="34%" align="left" valign="middle" class="text">&nbsp;Name </td>
                                                                                <td width="66%" bgcolor="#f2f2f2">

<?php echo $connection["connection_name"] ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="34%" align="left" valign="middle" class="text">&nbsp;From </td>
                                                                                <td width="66%" bgcolor="#f2f2f2">
<?php
$getairportfrom = "select * from airports where airport_id=" . $connection["from1"];
$rs_info = mysqli_query($con, $getairportfrom);
$airport_info = mysqli_fetch_array($rs_info);
?>
<?php echo $airport_info['airport_name'] ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="34%" align="left" valign="middle" class="text">&nbsp;To </td>
                                                                                <td width="66%" bgcolor="#f2f2f2">
<?php
$getairportfrom = "select * from airports where airport_id=" . $connection["to1"];
$rs_info = mysqli_query($con, $getairportfrom);
$airport_info = mysqli_fetch_array($rs_info);
?>
                                                                                    <?php echo $airport_info['airport_name'] ?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="34%" align="left" valign="middle" class="text">&nbsp;Cargo Agency </td>
                                                                                <td width="66%" bgcolor="#f2f2f2">

                                                                                    <?php
                                                                                    $sel_cargo_agency = "select * from cargo_agency where id='" . $connection["cargo_agency"] . "'";
                                                                                    $rs_carg_agency = mysqli_query($con, $sel_cargo_agency);
                                                                                    while ($cargo_agency = mysqli_fetch_array($rs_carg_agency)) {
                                                                                        echo $cargo_agency["name"];
                                                                                        ?>

                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </td>
                                                                            </tr>
                                                                            <tr <?php echo ($connection['type'] == 1) ? 'style="display:none;"' : ''; ?>>
                                                                                <td width="34%" align="left" valign="middle" class="text">&nbsp;Airline or Transport Company</td>
                                                                                <td width="66%" bgcolor="#f2f2f2">

<?php
$sel_cargo_agency = "select * from shipping_company where id='" . $connection["shipping_company"] . "'";
$rs_carg_agency = mysqli_query($con, $sel_cargo_agency);
while ($cargo_agency = mysqli_fetch_array($rs_carg_agency)) {
    ?>

                                                                                        <?php echo $cargo_agency["name"] ?>

                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </td>
                                                                            </tr>


                                                                            <tr>
                                                                                <td width="34%" align="left" valign="middle" class="text">&nbsp;Days Avialable </td>
                                                                                <td width="66%" bgcolor="#f2f2f2">
                                                                                    <?php echo $darrray; ?>
                                                                                </td>
                                                                            </tr>

<!--                                    <tr>
  <td width="34%" align="left" valign="middle" class="text">&nbsp;Mode of Transport </td>
  <td width="66%" bgcolor="#f2f2f2">
                                                                                    <?php
                                                                                    if ($connection["mode_of_transport"] == 1) {
                                                                                        echo "Air";
                                                                                    } else {
                                                                                        echo "Truck";
                                                                                    }
                                                                                    ?>
  </td>
</tr>-->

<!--                                    <tr>
  <td width="34%" align="left" valign="middle" class="text">&nbsp;Unit</td>
  <td width="66%" bgcolor="#f2f2f2">
  
  
<?php
if ($connection["unit"] == 1) {
    echo "Weight";
} else {
    echo "Cubic Feet";
}
?>
  
  </td>
</tr>-->


<!--                                    <tr>
  <td width="34%" align="left" valign="middle" class="text">&nbsp;From </td>
  <td width="66%" bgcolor="#f2f2f2"><?php echo $connection["from1"] ?></td>
</tr>
<tr>
  <td width="34%" align="left" valign="middle" class="text">&nbsp;To </td>
  <td width="66%" bgcolor="#f2f2f2"><?php echo $connection["to1"] ?></td>
</tr>-->


<?php
if ($connection["type"] == 1) {
    ?>

                                                                                <tr>
                                                                                    <td colspan="2" style="padding-left:20px;">
                                                                                        <br/> <b> Special Consolidation :</b> <br/>
                                                                                        <table style="width:100%">
                                                                                            <tr>
                                                                                                <td width="34%" align="left" valign="middle" class="text">&nbsp;Unit</td>
                                                                                                <td width="66%" bgcolor="#f2f2f2">


    <?php
    if ($connection["unit"] == 1) {
        echo "Weight";
    } else {
        echo "Cubic Feet";
    }
    ?>

                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="34%" align="left" valign="middle" class="text"> Box Weight Arranged : </td>
                                                                                                <td width="66%" bgcolor="#f2f2f2"><?php echo $connection["box_weight_arranged"] ?></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td width="34%" align="left" valign="middle" class="text"> Price Per Box </td>
                                                                                                <td width="66%" bgcolor="#f2f2f2"><?php echo $connection["price_per_box"] ?></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td width="34%" align="left" valign="middle" class="text"> Price Extra Kilo. </td>
                                                                                                <td width="66%" bgcolor="#f2f2f2"><?php echo $connection["price_extra_kilo"] ?></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>


                                                                                                    <?php
                                                                                                } else if ($connection["type"] == 2) {
                                                                                                    ?>	

                                                                                <tr ><td colspan="2" style="padding-left:20px;">
                                                                                        <br/> <b> Single Air Way Bill :</b> <br/>
                                                                                        <table style="width:100%">

                                                                                            <tr>
                                                                                                <td width="34%" align="left" valign="middle" class="text"> Kilograms </td>
                                                                                                <td width="66%" bgcolor="#f2f2f2">
    <?php echo $connection["kilograms"] ?>
                                                                                                </td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td width="34%" align="left" valign="middle" class="text"> Rate(KG) </td>
                                                                                                <td width="66%" bgcolor="#f2f2f2"><?php echo $connection["rate"] ?></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td width="34%" align="left" valign="middle" class="text"> Fuel Surcharge. </td>
                                                                                                <td width="66%" bgcolor="#f2f2f2"><?php echo $connection["fuel_surcharge"] ?></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td width="34%" align="left" valign="middle" class="text"> Sec(Security). </td>
                                                                                                <td width="66%" bgcolor="#f2f2f2"> <?php echo $connection["security"] ?></td>
                                                                                            </tr>

                                                                                            <tr>
                                                                                                <td width="34%" align="left" valign="middle" class="text"> AWB </td>
                                                                                                <td width="66%" bgcolor="#f2f2f2"><?php echo $connection["awb"] ?></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="34%" align="left" valign="middle" class="text">From a Fee(Each Form) </td>
                                                                                                <td width="66%" bgcolor="#f2f2f2"><?php echo $connection["fee"] ?></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="34%" align="left" valign="middle" class="text">Fitosnary Free </td>
                                                                                                <td width="66%" bgcolor="#f2f2f2"><?php echo $connection["fitosnary_fee"] ?></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="34%" align="left" valign="middle" class="text">Due Agent</td>
                                                                                                <td width="66%" bgcolor="#f2f2f2"><?php echo $connection["due_agnet"] ?></td>
                                                                                            </tr>
                                                                                        </table> 
                                                                                    </td>
                                                                                </tr>

<?php } else if ($connection["type"] == 4) { ?>

    <!--                                    <tr>
    <td colspan="2">
    <b>Price per Cubic Feet </b> <br/>
    <table style="width:100%">

     <tr>
      <td width="34%" align="left" valign="middle" class="text"> Minimum Cubic Feet </td>
      <td width="66%" bgcolor="#f2f2f2">
    <?php echo $connection["minimum_cubic_feet"] ?>
        </td>
    </tr>


     <tr>
      <td width="34%" align="left" valign="middle" class="text"> Price Per Cubic Feet </td>
      <td width="66%" bgcolor="#f2f2f2">
    <?php echo $connection["price_cubic_feet"] ?>
        </td>
    </tr>
     </table>
    </td>
    </tr>	  -->

<?php } else if ($connection["type"] == 3) { ?>


    <!--                                    <tr style="display:none;" id="type2">
    <td colspan="2">
    <b> Price per Box </b> <br/>
    <table style="width:100%">
     <tr>
      <td width="34%" align="left" valign="middle" class="text"> Price per Box </td>
      <td width="66%" bgcolor="#f2f2f2">
                                                                                <?php echo $connection["price_per_box2"] ?>
        </td>
    </tr>
    </table>
    </td>
    </tr>-->


<?php } ?>

<!--                                    <tr>
  <td width="34%" align="left" valign="middle" class="text"> Shipping Rate </td>
  <td width="66%" bgcolor="#f2f2f2">
<?php echo $connection["shipping_rate"] ?>   <?php
if ($connection["unit"] == 1) {
    echo "Per Kg.";
} else {
    echo "Per Cubic Feet";
}
?>
    </td>
</tr>-->
<?php
if ($connection["addtional_rate"] != "0.00") {
    ?>
    <!--                                     <tr>
      <td width="34%" align="left" valign="middle" class="text"> Additional Rate  </td>
      <td width="66%" bgcolor="#f2f2f2">
    <?php echo $connection["addtional_rate"] ?>  <?php
    if ($connection["unit"] == 1) {
        echo "Per Kg.";
    } else {
        echo "Per Cubic Feet";
    }
    ?>
        </td>
    </tr>-->
    <?php
}
?>	


                                                                            <?php
                                                                            if ($connection["box_price"] != "0.00") {
                                                                                ?>
    <!--                                     <tr>
    <td width="34%" align="left" valign="middle" class="text"> Shipping Rate Per Box </td>
    <td width="66%" bgcolor="#f2f2f2">
                                                                                <?php echo $connection["box_price"] ?> per Box
    </td>
    </tr>-->
                                                                                <?php
                                                                            }
                                                                            ?>


                                                                            <tr>
                                                                        </table>
                                                                    </div></td>
                                                            </tr>
                                                        </table></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
                                                                            <?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
