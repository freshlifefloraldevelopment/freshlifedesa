<?php
	include "../config/config_gcp.php";
	session_start();
	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}

	if(isset($_POST["Submit"]) && $_POST["Submit"]=="Save")	{			

	    $ins="update general_parameters set 
                              descripcion='".$_POST["descripcion"]."' ,
                              time='".$_POST["time"]."'    
                              where id='".$_GET["id"]."' ";

		mysqli_query($con,$ins);

		header('location:parameters_mgmt.php');
	}
	
	$sel_param="select * from general_parameters where id='".$_GET["id"]."'";

	$rs_param=mysqli_query($con, $sel_param);
	$param=mysqli_fetch_array($rs_param);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript">

	function verify(){ 

		var arrTmp=new Array();
		arrTmp[0]=checkcname();	
                arrTmp[1]=checktime();                

		var i;

		_blk=true;

		for(i=0;i<arrTmp.length;i++)		{

			if(arrTmp[i]==false)	{
			   _blk=false;
			}
		}

		if(_blk==true)	{
			return true;
		}else{
			return false;
		}
 	}	


	function trim(str) {    

		if (str != null) {        

			var i;        

			for (i=0; i<str.length; i++) 	{           

				if (str.charAt(i)!=" ") {               
					str=str.substring(i,str.length);                 
					break;            
				}        
			}            

			for (i=str.length-1; i>=0; i--)		{            

				if (str.charAt(i)!=" ")	{                
					str=str.substring(0,i+1);                
					break;            
				}         
			}                 

			if (str.charAt(0)==" ") 	{            
				return "";         
			} else 	{            
				return str;         
			}    
		}
	}

	function checkcname()	{

		if(trim(document.frmcat.cname.value) == "")	{	 
			document.getElementById("lblcname").innerHTML="Please enter color";
			return false;
		}else 	{
			document.getElementById("lblcname").innerHTML="";
			return true;
		}
	}
		
        function checktime() {

            if (trim(document.frmcat.time.value) == "") {

                document.getElementById("lbltime").innerHTML = "Please enter Time";

                return false;

            }else {
                document.getElementById("lbltime").innerHTML = "";
                return true;
            }
        }        
</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

<?php include("includes/header_inner.php");?>

  <tr>
    <td height="5"></td>
  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
       <?php include("includes/left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/images_front/middle-leftline.gif"><img src="images/images_front/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/images_front/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>
                <td width="10">&nbsp;</td>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="5"></td>
                  </tr>

                  <tr>
                    <td class="pagetitle">Edit Parameter</td>
                  </tr>

                  <tr>
                    <td>&nbsp;</td>
                  </tr>

                        <tr>
                        <td>
				<table width="100%">
					<tr>
                                           <td>
                                                <a class="pagetitle1" href="parameters_mgmt.php" onclick="this.blur();"><span> Manage Parameters</span></a>
                                           </td>
					</tr>

				</table>

			</td>
                        </tr>

                  <tr>
                    <td>&nbsp;</td>
                  </tr>

                <form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data" action="parameters_edit.php?id=<?php echo $_GET["id"]?>">
                <tr>
                    <td><div id="box">

                    <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
			  <tr>                        
                                <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory  </td>
                          </tr>

			<tr>
                            <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Parameter</td>
				<td width="66%" bgcolor="#f2f2f2">
				<input type="text" class="textfieldbig" name="descripcion" id="descripcion" value="<?php echo $param["descripcion"]?>" />
				<br>
                                    <span class="error" id="lbldescripcion"></span>	
                                </td>
                        </tr>  
                        
			<tr>
                            <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span>Time</td>
				<td width="66%" bgcolor="#f2f2f2">
				<input type="text" class="textfieldbig" name="time" id="time" value="<?php echo $param["time"]?>" />
				<br>
                                    <span class="error" id="lbltime"></span>	
                                </td>
                        </tr>                                           

                <tr>
                  <td>&nbsp;</td>
                  <td><input name="Submit" type="Submit" class="buttongrey" value="Save" />                  </td>
                </tr>

              </table>

            </div>
           </td>
          </tr>

	    </form>

            </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/images_front/middle-rightline.gif"><img src="images/images_front/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>
            <td background="images/images_front/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/images_front/middle-rightline.gif"></td>
          </tr>

          <tr>
            <td height="10"><img src="images/images_front/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/images_front/middle-bottomline.gif"></td>
            <td><img src="images/images_front/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>

        </table>
        </td>
      </tr>
    </table>
    </td>

  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>

</html>