<?php

// PO 2018-09-21 Cambio Eduardo

require_once("../config/config_gcp.php");
session_start();

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
	header("location: index.php");
}

if($_SESSION['grower_id']!=0){
	header("location: growers.php?id=".$_SESSION['grower_id']);
}  
			

	$qsel="select bo.id        , bo.buyer_id    , bo.order_number, bo.order_date     , bo.shipping_method, bo.del_date, 
                      bo.date_range, bo.order_serial, bo.seen        , bo.delivery_dates , bo.lfd_grower     , 
                      bo.qucik_desc as descrip      , b.first_name   , b.last_name       , bo.join_order
                 from buyer_orders bo
                inner join buyers b  ON bo.buyer_id = b.id          
                where bo.del_date > date_add(curdate(), INTERVAL -60 DAY) 
                order by bo.id ";	

	 $rs=mysqli_query($con,$qsel);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
			} );
</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
 <?php include("includes/header_inner.php");?>

  <tr>
    <td height="5"></td>
  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/agent-left.php");?>

        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td height="5"></td>
                   </tr>

                  <tr>
                    <td class="pagetitle">Orders</td>
                  </tr>
                 
                  <tr>
                    <td>&nbsp;</td>
                  </tr>

                  <tr>

                <td><div id="box">

		<div id="container">			
                    <div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>       
                    <th align="right" width="6%" >Id</th>             
                    <th align="left" width="25%" >Buyer</th>  
                    <th align="left" width="12%">Order </th>                    
                    <th align="left" width="20%">Descrip.</th>                    
                    <th align="left" width="18%">Delivery</th>                                        
                    <th align="left" width="18%">LFD</th>                                                            
                    <th align="left" width="15%">Select</th>                                                            
		</tr>

</thead>

	<tbody>
		<?php

		  	$sr=1;
        		  while($product=mysqli_fetch_array($rs))  {	
                                                            
                                $qsellfd="select id_order , lfd , count(*) 
                                            from buyer_requests
                                           where id_order = '" . $product["id"] . "'
                                           group by lfd 
                                           order by count(*)";	

                                 $rsllfd = mysqli_query($con,$qsellfd);                              
                                 
                                 while($lfd_id=mysqli_fetch_array($rsllfd))  {	
                                         $lfd_date =   $lfd_id["lfd"];
                                 }
                                 
                                $qsisy="select isy
                                            from buyer_requests
                                           where id_order = '" . $product["id"] . "'
                                           group by isy  ";	

                                 $rsisy = mysqli_query($con,$qsisy);                              
                                 
                                 while($isy_id=mysqli_fetch_array($rsisy))  {	
                                         $estado =   $isy_id["isy"];
                                 }   
                                 
                                 
                                  if ($estado == 1) {
                                        $disp_id = "Hide";
                                  } else {
                                        $disp_id = "Show";
                                  }                                                                   
                                 
                                  if ($product["join_order"] != 0) {
                                        $joinedId = "(Joined ".$product["join_order"].")";
                                  } else {
                                        $joinedId = " ";
                                  }                                                                   

                              

		?>
                          <tr class="gradeU">                          
                              <?php
                              
                              if ($product['del_date'] >= date("Y-m-d")) {  ?>
                              
                                    <td class="text" align="right"><?php echo $product["id"]?> </td>                                                                                                                         
                                    <td class="text" align="left"><?php echo $product["first_name"]?> <?php echo $product["last_name"]?> <?php echo $joinedId?></td>                                                                                  
                                    <td class="text" align="left"><?php echo $product["order_number"]?>  </td>                                                                                                                  
                                    <td class="text" align="left"><?php echo $product["descrip"]?> </td>                                                           
                                    <td class="text" align="left"><?php echo $product["del_date"]?> </td>  

                              
                                  
                              <?php }else{ ?>
                                    <td class="text" align="right"><?php echo $product["id"]?> </td>                                                                                                                         
                                    <td class="text" align="left"><font color="red"><?php echo $product["first_name"]?> <?php echo $product["last_name"]?> <?php echo $joinedId?> </font></td>                                                                                  
                                    <td class="text" align="left"><font color="red"><?php echo $product["order_number"]?> </font></td>                                                                                                                  
                                    <td class="text" align="left"><font color="red"><?php echo $product["descrip"]?> </font></td>                                                           
                                    <td class="text" align="left"><font color="red"><?php echo $product["del_date"]?> </font></td>  
                              <?php }
                              
                              ?>


                                <td class="text" align="center"><a href="more_days_conn_buyer.php?id_ship=<?php echo$product["shipping_method"]."&buy=".$product["buyer_id"]?>" ><?php echo $lfd_date ?></a> </td>
                                
                                 <?php
                              
                                    if ($estado == 1) {  ?>                                
                                            <td class="text" align="center"><a href="buy_flowers_edit.php?id_fact=<?php echo $product["id"]?>" ><h3><?php echo $disp_id ?></h3></a> </td>
                                    <?php }else{ ?>        
                                            <!--td class="text" align="left"><?php echo $disp_id ?> </td-->  
                                            <td class="text" align="center"><a href="buy_flowers_edit.php?id_fact=<?php echo $product["id"]?>" ><?php echo $disp_id ?></a> </td>
                                    <?php }
                                    ?>
                                
                                <td class="text" align="center"><a href="buyer_requests.php?id_fact=<?php echo $product["id"]?>" >Requests</a> </td>                                                                
                                <td class="text" align="center"><a href="print_offer2.php?b=<?php echo $product["id"]."&id_buy=".$product["buyer_id"]?> " >Offer</a> </td>                                                                   
                                <td class="text" align="center"><a href="buyer_requests_cart.php?id_fact=<?php echo $product["id"]?>" >Cart</a> </td>
                                
                                <!--td align="center" ><a href="buy_flowers_edit.php?id_fact=<?php echo $product["id"] ?> "><img src="images/edit.gif" border="0" alt="Edit" /></a></td-->                                                                                                                                                                    

                                <!--td class="text" align="left"><?php echo $lfd_date ?> </td-->                                
                                <!--td class="text" align="center"><a href="print_packing_customer.php?b=<?php echo $product["id"]."&id_buy=".$product["buyer_id"]?> " >N.A.</a> </td-->                                                                                                                                   
                                <!--td class="text" align="center"><a href="print_offer3.php?b=<?php echo $product["id"]."&id_buy=".$product["buyer_id"]?> "  >Variety</a> </td-->                                   
                                <!--td class="text" align="center"><a href="print_request_offer.php?b=<?php echo $product["id"]."&id_buy=".$product["buyer_id"]?> "  >Check</a> </td-->                                                                                                   

                          </tr>
				 <?php
			     $sr++;
			   } ?> 	
	</tbody>
</table>
                                </div>
                            </div>
			</div>
		</td>
                </tr>
                </table>
                </td>

                <td width="10">&nbsp;</td>

                    </tr>
                </table>                    
            </td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>

          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>

          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table>
        </td>
      </tr>

    </table>
    </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>