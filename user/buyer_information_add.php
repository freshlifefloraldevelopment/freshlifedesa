<?php
include "../config/config_gcp.php";

$idBuyer = $_GET["idby"];

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}

$get_connection = " select id     , cnumber , datex , verifica , cpostal , 
                           status , buyer_id
                      from buyer_information
                     where buyer_id='" . $idBuyer . "'" ;

$rs_info = mysqli_query($con, $get_connection);
$get_connection_info = mysqli_fetch_array($rs_info);

if (isset($_POST["Submit"]) && $_POST["Submit"] == "Add") {
    
        
    $ins = "insert into buyer_information set 
                        cnumber  ='" . $_POST["cnumber"] . "'            ,
                        datex    ='" . $_POST["datex"] . "'  ,
                        verifica ='" . $_POST["verifica"] . "'         ,
                        cpostal  ='" . $_POST["cpostal"] . "'  ,
                        buyer_id ='" . $idBuyer . "'     ";
    
    mysqli_query($con, $ins);
    header('location:buyer_information.php?id='.$idBuyer);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="js/select2/select2.min.js"></script>
        <link href="js/select2/select2.min.css" rel="stylesheet" type="text/css" />
        
<script type="text/javascript">
            $(document).ready(function () {
                $('select').select2();
            });
            
            function verify()         {
                var arrTmp = new Array();
                arrTmp[0] = checkcard();                
                arrTmp[1] = checkdatex();
                arrTmp[2] = checkverifi();
                arrTmp[3] = checkcpostal();                
                
                var i;
                _blk = true;
                
                for (i = 0; i < arrTmp.length; i++)        {
                    if (arrTmp[i] == false)      {
                        _blk = false;
                    }
                }
                if (_blk == true)    {
                    return true;
                } else   {
                    return false;
                }
            }

            function trim(str) {
                if (str != null)     {
                    var i;
                    for (i = 0; i < str.length; i++)     {
                        if (str.charAt(i) != " ")    {
                            str = str.substring(i, str.length);
                            break;
                        }
                    }
                    for (i = str.length - 1; i >= 0; i--)        {
                        if (str.charAt(i) != " ")       {
                            str = str.substring(0, i + 1);
                            break;
                        }
                    }
                    if (str.charAt(0) == " ")    {
                        return "";
                    }  else  {
                        return str;
                    }
                }
            }

            // Inicio Campos
            function checkcard() {
                if (trim(document.frmcat.cnumber.value) == "")    {
                    document.getElementById("lblcnumber").innerHTML = "Please enter Card Number";
                    return false;
                }  else   {
                    document.getElementById("lblcnumber").innerHTML = ""; 
                    return true;
                }
            }

            function checkdatex(){
                if (trim(document.frmcat.datex.value) == "")    {
                    document.getElementById("lbldatex").innerHTML = "Please enter Expiration Date";
                    return false;
                }  else {
                    document.getElementById("lbldatex").innerHTML = "";
                    return true;
                }
            }

            function checkverifi()  {
                if (trim(document.frmcat.connections.value) == "")  {
                    document.getElementById("lblconnection_name").innerHTML = "Please select atleast one connection";
                    return false;
                } else   {
                    document.getElementById("lblconnection_name").innerHTML = "";
                    return true;
                }
            }
            
            function checkcpostal()  {
                if (trim(document.frmcat.connections.value) == "")  {
                    document.getElementById("lblconnection_name").innerHTML = "Please select atleast one connection";
                    return false;
                } else   {
                    document.getElementById("lblconnection_name").innerHTML = "";
                    return true;
                }
            }            
</script>

</head>
    
<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
<?php include("includes/header_inner.php"); ?>
            <tr>
                <td height="5"></td>
            </tr>
    
<tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
<?php include("includes/shipping-left.php"); ?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><form name="frmcat" method="post" onsubmit="return verify();" enctype="multipart/form-data">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td height="5"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="pagetitle">Add Information</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><table width="100%">
                                                                            <tr>
                                                                                <td><a class="pagetitle1" href="buyer_information.php" onclick="this.blur();"><span> Buyer Information </span></a></td>
                                                                            </tr>
                                                                        </table></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                </tr>

                                                                <tr>
                                                                    <td><div id="box">
                                                                            <input type="hidden" id="count" name="count" value="1" />
                                                                            <table style="border-collapse:collapse;" width="100%" border="1" cellpadding="2" cellspacing="0" bordercolor="#e4e4e4">
                                                                                <tr>
                                                                                    <td colspan="2" align="left" class="text">&nbsp;&nbsp;Fieds Marked with (<span class="error">*</span>) are Mandatory </td>
                                                                                </tr>
                                                                                
                                                                                <!--tr> Fields  </tr-->

                                                                                <tr>
                                                                                    <td width="34%" align="left" valign="middle" class="text">&nbsp;<span class="error">* </span> Card Number </td>
                                                                                    <td width="66%" bgcolor="#f2f2f2"><input type="text" class="textfieldbig" name="cnumber" id="cnumber" value="<?php echo $get_connection_info['cnumber'] ?>" />
                                                                                        <br>
                                                                                            <span class="error" id="lblcnumber"></span></td>
                                                                                </tr>                                                                                                                                                                                                                                                                                                                                
                                                                                
                                                                                
                                                                                
                                                                                <!--tr> End Fields  </tr-->
                                       

                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td><input name="Submit" type="Submit" class="buttongrey" value="Add" /></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div></td>
                                                                </tr>

                                                            </table>
                                                        </form></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
<?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
