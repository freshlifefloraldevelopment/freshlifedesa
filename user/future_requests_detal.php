<?php

// PO 2018-09-21 

require_once("../config/config_gcp.php");
session_start();

$order_number  = $_GET['id_fact'];

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
	header("location: index.php");
}

if($_SESSION['grower_id']!=0){
	header("location: growers.php?id=".$_SESSION['grower_id']);
}  

	if(isset($_GET['delete'])) {
	  $querydel = 'DELETE FROM order_subclient WHERE  id= '.(int)$_GET['delete'];
	  mysqli_query($con,$querydel);
	}
			

	$qsel="select os.id        , os.id_week   , os.id_client , os.product   , os.prod_name , os.buyer     ,
                      os.grower_id , os.qty_pack  , os.size      , os.steams    , os.comment   , os.id_state  ,
                      os.date_ship , os.date_del  , os.fact_id   , os.cost      , os.price     , cli.name as namecli,
                      s.name as subcate, p.name as variety , os.estado
                 from order_subclient os 
                 INNER JOIN product p on os.product = p.id 
                 INNER join subcategory s on p.subcategoryid = s.id 
                 INNER JOIN sub_client cli on os.id_client = cli.id 
                 where os.fact_id = '" . $order_number . "' ";	

	 $rs=mysqli_query($con,$qsel);
         
         
	$qselcli="select  os.date_ship , os.date_del  , os.fact_id   , os.cost      , os.price     , cli.name as namecli,
                          s.name as subcate, p.name as variety
                    from order_subclient os 
                   INNER JOIN product p on os.product = p.id 
                   INNER join subcategory s on p.subcategoryid = s.id 
                   INNER JOIN sub_client cli on os.id_client = cli.id 
                 where os.fact_id = '" . $order_number . "' order by os.id limit 1,1";	

	 $rscli=mysqli_query($con,$qselcli);   
         
            $info = mysqli_fetch_array($rs_cli);
            
            $titulo = $info['namecli'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
			} );
</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
 <?php include("includes/header_inner.php");?>

  <tr>
    <td height="5"></td>
  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/agent-left.php");?>

        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td height="5"></td>
                   </tr>

                  <tr>
                    <td class="pagetitle">Future Request <?php echo $info['namecli']; ?></td>
                  </tr>

                        
                        
                  <tr>
                    <td>&nbsp;</td>
                  </tr>

                  <tr>

                <td><div id="box">

		<div id="container">			
                    <div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>
            

		<tr>       
                    <th align="left" width="35%" >Variety</th>  
                    <th align="left" width="8%">Qty</th>                    
                    <th align="left" width="14%">Customer Delivery</th>                                        
                    <th align="left" width="16%">Type</th>                                                            
                    <th align="left" width="16%">State</th>                                                            
		</tr>

</thead>

	<tbody>
		<?php

		  	$sr=1;
        		  while($product=mysqli_fetch_array($rs))  {
                              
                              
                              if ($product["estado"] == 0) {
                                    $state = "Request";
                              }elseif ($product["estado"] == 1){
                                    $state = "PROCESSED";
                              } else {
                                  $state = "PENDING";
                              }
                              
		?>
                          <tr class="gradeU">                          
                                <td class="text" align="left"><?php echo $product["variety"]." ".$product["subcate"]." ".$product["size"]." cm."?></td>                                                                                  
                                <td class="text" align="left"><?php echo $product["qty_pack"]?> </td>                                                           
                                <td class="text" align="left"><?php echo $product["date_del"]?> </td>  
                                <td class="text" align="left"><?php echo $product["comment"]?> </td> 
                                <td class="text" align="left"><?php echo $state?> </td> 
                                
                                <td class="text" align="center"><a href="future_requests_total.php?id_prod=<?php echo $product["product"]?>" >TOTAL VARIETY</a> </td>	                                
                                <td align="center" ><a href="future_requests_edit.php?idor=<?php echo $product["id"]."&id_fact=".$product["fact_id"] ?> "><img src="images/edit.gif" border="0" alt="Edit" /></a></td>                                
                                <td align="center" ><a href="?delete=<?php  echo $product["id"]?>"  onclick="return confirm('You want to delete this REQUEST? CHECK if you have offers.');"><img src="images/delete.gif" border="0" alt="Delete" /></a></td>                                

                          </tr>
				 <?php 
			     $sr++;
			   } ?> 	
	</tbody>
</table>
                                </div>
                            </div>
			</div>
		</td>
                </tr>
                </table>
                </td>

                <td width="10">&nbsp;</td>

                    </tr>
                </table>                    
            </td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>

          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>

          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table>
        </td>
      </tr>

    </table>
    </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>