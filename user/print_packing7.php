<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_SESSION["buyer"];
    $idbuy = $_GET['id_buy'];    
    $idfac = $_GET['b'];
    $cajastot = 0;

    // Datos del Buyer
    $buyerEntity = "select b.first_name,b.last_name,c.name , b.company
                      from buyers  b , country c 
                     where b.id = '" . $idbuy . "'  
                       and c.id=b.country" ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    
    // Datos de la Orden
   $buyerOrder = "select id_fact          , buyer_id         , order_number, 
                         order_date       , shipping_method  , del_date    , 
                         date_range       , is_pending       , order_serial, 
                         seen             , delivery_dates   , lfd_grower  , 
                         quick_desc       , bill_number      , gross_weight, 
                         volume_weight    , freight_value    , guide_number, 
                         total_boxes      , sub_total_amount , tax_rate    , 
                         shipping_charge  , handling         , grand_total , 
                         bill_state       , date_added       , user_added  ,
                         air_waybill      , charges_due_agent,
                         credit_card_fees , per_kg                         
                         from invoice_orders
                   where buyer_id = '" . $idbuy . "'
                     and id_fact = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Packing
   
   $sqlDetalis="select ir.id_fact  ,  ir.product  , ir.prod_name      ,  ir.product_subcategory as subcate_real,
                       ir.size       , ir.steams   ,        
                       ip.prod_name ,
                       cl.name ,
                       sum(ir.qty_pack) as bunchqty         ,
                       max(ip.offer_id) as id_request
                  from invoice_packing_box ir
                 inner JOIN sub_client sc ON IFNULL(ir.cliente_id,0) = sc.id
                 inner join invoice_packing ip ON ir.id_order = ip.id      
                 inner join product p ON ir.product = p.id      
                 inner join colors cl ON p.color_id = cl.id      
                 where ir.buyer    = '" . $buyer_cab . "'
                   and ir.id_fact  = '" . $id_fact_cab . "' 
                   and sc.id in(0,1,47)
                 group by ir.id_fact ,  ir.product  , ir.prod_name ,    ir.product_subcategory,       ir.size       , ir.steams   ,        
                          ip.prod_name,cl.name
                 order by ir.product_subcategory , ir.prod_name ";

        $result   = mysqli_query($con, $sqlDetalis);    

    $pdf = new PDF();
    $pdf->AddPage();

    $pdf->SetFont('Arial','B',25);    
    $pdf->Cell(70,10,'  PACKING LIST (not assigned)',0,0,'L'); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Name ',0,0,'L');
            $pdf->Cell(0,10,'Shipping Details ',0,0,'R');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'First Name: '.$buy['first_name'],0,0,'L');
    $pdf->Cell(0,6,'Invoice #: '.$buyerOrderCab['order_number'],0,1,'R');  
       
    
    if ($userSessionID != 318) {
       $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,0,'L');
       $pdf->Cell(0,6,'Total Boxes: '.$buyerOrderCab['total_boxes'],0,1,'R');     
    }else{
       $pdf->Cell(70,6,'Last Name: '.$buy['last_name'],0,1,'L');  
    }
      
    
    $pdf->Cell(70,6,'Country: '.$buy['name'],0,0,'L');        
    $pdf->Cell(0,6,'Gross Weight: '.$buyerOrderCab['gross_weight'],0,1,'R');        
    
    $pdf->Cell(70,6,'-'.$buy['company'],0,0,'L');  
    $pdf->Cell(0,6,'Volume Weight: '.$buyerOrderCab['volume_weight'],0,1,'R');  
    
    $pdf->Ln(10);
    //$pdf->Cell(40,6,'Box Number',0,0,'C');    
    $pdf->Cell(70,6,'Variety',0,0,'L');
    $pdf->Cell(35,6,'Bunch/Box1',0,1,'C');

    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',8);
    
    $tmp_idorder = 0;
    
    while($row = mysqli_fetch_assoc($result))  {
        
        
        
        //  Features
        $sel_feature = "select br.feature , f.name as features, br.id , br.product ,  br.qty , br.cod_order , 
                               br.buyer
                          from buyer_requests br
                         INNER JOIN features f  ON br.feature = f.id
                         where br.id_order = '" . $row['id_fact'] . "'
                           and br.id = '" . $row['id_request'] . "' ";
        $rs_features = mysqli_query($con,$sel_feature);       
          
        
        $feature_name = " ";
        while($cod_feature = mysqli_fetch_array($rs_features))  {
                    $feature_name = $cod_feature['features'];
        }        
        
        
         // Verificacion Stems/Bunch
        $sel_bu_st = "select box_type , subcategoryid 
                       from product 
                      where name = '" . $row['prod_name'] . "' ";        
        
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
            
              if ($bunch_stem['box_type'] == 0) {
                    $qtyFac = $row['bunchqty'];                   
                    $unitFac = "STEMS";                     
              }else{
                    $qtyFac = $row['bunchqty']; 
                    $unitFac = "BUNCHES";                   
              }                        

        $sel_subcatego = "select name from subcategory where id = '" . $bunch_stem['subcategoryid'] . "' ";
        $rs_subcatego = mysqli_query($con,$sel_subcatego);       
        $subc = mysqli_fetch_array($rs_subcatego);              
                         
                 $subtotalStems= $row['bunchqty'] ;      
   /*               
         if ($row['idcli'] != $tmp_idorder) {
               $pdf->SetFont('Arial','B',8);                   
               if ($sw == 1) { 
                      $pdf->Cell(70,6,"  ",0,1,'L');                       
                      $pdf->Cell(70,6,"Total Bunches  : ".$subStemsGrower,0,1,'L');                                 
               }
                      $sw = 1;             
                      $pdf->Cell(70,6,"  ",0,1,'L'); 
                      $pdf->Cell(70,6,$row['cliente'],0,1,'L');   
                      
                              $cajastot =  $cajastot + $cajas;
                      $pdf->Cell(70,6,"  ",0,1,'L'); 
                      $subStemsGrower = 0;                      
         }*/

  $pdf->SetFont('Arial','',8);
        // $pdf->Cell(40,4,$row['box_name'],0,0,'C'); 
         if ($subc['name'] == 'Snapdragon'||$subc['name'] == 'Alstroemeria'||$subc['name'] == 'Carnations'||$subc['name'] == 'Mini Carnations') { 
                $pdf->Cell(70,4,$row['subcate_real']." ".$row['prod_name']." ".$row['steams']." st/bu  ".$feature_name." ".$row['name'],0,0,'L');            
         }else{
                $pdf->Cell(70,4,$row['subcate_real']." ".$row['prod_name']." ".$row['size']." cm ".$row['steams']." st/bu  ".$feature_name." ".$row['name'],0,0,'L');            
         }         
         
                           
         $pdf->Cell(35,4,$row['bunchqty'],0,1,'C');                                                   
                 
            $totalStems = $totalStems + $subtotalStems;            
            $tmp_idorder = $row['idcli'];
            $subStemsGrower= $subStemsGrower + ($row['bunchqty']) ;             
    }
        
    
                      $pdf->SetFont('Arial','B',8);                   
                      $pdf->Cell(70,6,"  ",0,1,'L');                       
                      $pdf->Cell(70,6,"Total Bunches: ".$subStemsGrower,0,1,'L');  
    
    $pdf->Ln(2);
    $pdf->SetFont('Arial','B',15);
    $pdf->Cell(70,10,'Contact Details ',0,1,'L');
    
            $pdf->SetFont('Arial','B',10);            
            $pdf->Cell(70,6,'Av. Interoceanica OE6-73 y Gonzalez Suarez',0,0,'L'); 
            $pdf->Cell(0,6,'Total Bunches Packing:'.number_format($totalStems, 0, '.', ','),0,1,'R');                    
  
            $pdf->Cell(70,6,'Quito, Ecuador',0,1,'L');   
            $pdf->Cell(70,6,'Phone: +593 602 2630',0,1,'L'); 
            $pdf->Cell(70,6,'Email:info@freshlifefloral.com',0,0,'L');   
    
  $pdf->Output();
  ?>