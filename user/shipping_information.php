<?php

	include "../config/config_gcp.php";
        $id_cargo  = $_GET['id_ca'];
        $buyer = $_GET['buy'];
        
	session_start();

	$qsel="select id   , name, logo, contact_name, phone ,
                      coordination
                 from cargo_agency 
                where id = '" . $id_cargo . "'
                order by id";

	$rs=mysqli_query($con, $qsel);           

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}  

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<link href="css/demo_page.css" rel="stylesheet" type="text/css" />

<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />

<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					"bJQueryUI": true,

					"sPaginationType": "full_numbers"
				});
			} );

</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

 <?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/shipping-left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/images_front/middle-leftline.gif"><img src="images/images_front/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/images_front/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>

                    <td height="5"></td>

                  </tr>

                  <tr>
                    <td class="pagetitle">Shipping Information</td>
                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

                                                      <!-- right options -->
		  <tr>
                    <td>
				<table width="100%">
					<tr>
                                            <td>
                                                <a class="pagetitle" href="shipping.php?id=<?php echo $buyer?>" onclick="this.blur();"><span><u>Manage Shipping Method</u></span></a>                                               
                                            </td>
					</tr>
				</table>
		    </td>
                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

                  <tr>

                    <td><div id="box">

		<div id="container">			

			<div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>

			<th width="8%" align="center">Sr.</th>

			<th width="45%" align="left">Cargo Agency</th>
			<th width="45%" align="left">Contact Name</th>
			<th width="45%" align="left">Coordination Name</th>                        
			<th width="45%" align="left">Phone</th>                        

            <th align="center" width="12%">Edit</th>


		</tr>

	</thead>

	<tbody>

		<?php
		  	$sr=1;
			  while($shipping=mysqli_fetch_array($rs))	  {
		  ?>

                        <tr class="gradeU">
                          <td align="center" class="text"><?php echo $sr;?></td>

                          <td class="text" align="left"><?php echo $shipping["name"]?></td>
                          <td class="text" align="left"><?php echo $shipping["contact_name"]?></td>
                          <td class="text" align="left"><?php echo $shipping["coordination"]?></td>                          
                          <td class="text" align="left"><?php echo $shipping["phone"]?></td>                          

                           <td align="center" ><a href="shipping_infor_edit.php?id=<?php echo $shipping["id"]?>"><img src="images/images_front/edit.gif" border="0" alt="Edit" /></a></td>

                          </tr>
						 <?php
						 		$sr++;
						 	}
						 ?> 


	</tbody>

</table>



			</div>

			</div>



			</div>

		</td>

                  </tr>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/images_front/middle-rightline.gif"><img src="images/images_front/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/images_front/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/images_front/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/images_front/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/images_front/middle-bottomline.gif"></td>

            <td><img src="images/images_front/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>

</body>

</html>
