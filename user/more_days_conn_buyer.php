<?php
include "../config/config_gcp.php";

$idBuy = $_GET["buy"];

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {
    header("location: index.php");
}

if (isset($_GET['delete'])) {
    $query = 'DELETE FROM more_days_connection WHERE  id= ' . (int) $_GET['delete'];
    mysqli_query($con, $query);
}


$qsel = "select id,connections,id_conn,
       case 
  when days = 1 then 'Monday'
  when days = 2 then 'Tuesday'
  when days = 3 then 'Wednesday'
  when days = 4 then 'Thursday'
  when days = 5 then 'Friday'
  when days = 6 then 'Saturday'
  when days = 0 then 'Sunday'
  else 'Week'
end as daysWeek,
       days,
       trasit_time ,
MOD ((days+trasit_time), 7) delivery,
       case 
  when MOD ((days+trasit_time), 7) = 1 then 'Monday'
  when MOD ((days+trasit_time), 7) = 2 then 'Tuesday'
  when MOD ((days+trasit_time), 7) = 3 then 'Wednesday'
  when MOD ((days+trasit_time), 7) = 4 then 'Thursday'
  when MOD ((days+trasit_time), 7) = 5 then 'Friday'
  when MOD ((days+trasit_time), 7) = 6 then 'Saturday'
  when MOD ((days+trasit_time), 7) = 0 then 'Sunday'
  else 'Week'
end as deliveryDay
from more_days_connection  ";
$rs = mysqli_query($con, $qsel);

$sel_info = "select bsm.id         , bsm.buyer_id , bsm.shipping_method_id , bsm.country , bsm.cargo_agency_id , 
                    bsm.first_name , bsm.last_name,
                    sm.days        , sm.connect_group
               from buyer_shipping_methods bsm
               left join shipping_method sm on bsm.shipping_method_id = sm.id  
              where bsm.buyer_id = '" . $_GET["buy"] . "'
                and bsm.shipping_method_id = '" . $_GET["id_ship"] . "'   ";

$rs_info = mysqli_query($con,$sel_info);
$info = mysqli_fetch_array($rs_info);
    

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Admin Area</title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">

            $(document).ready(function () {

                oTable = $('#example').dataTable({
                    //"sScrollXInner": "130%",

                    "bJQueryUI": true,
                    //"sScrollY": "536",

                    "sPaginationType": "full_numbers"

                });

            });

        </script>
    </head>

    <body>
        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
            <?php include("includes/header_inner.php"); ?>
            <tr>
                <td height="5"></td>
            </tr>
            <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <?php include("includes/shipping-left.php"); ?>
                            <td width="5">&nbsp;</td>
                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="5"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="pagetitle">More Days Avialable</td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td><table width="100%">
                                                                        <tr>

                                                                        </tr>
                                                                        
                                                                        <tr>
                                                                                <td><a class="pagetitle1" href="shipping.php?id=<?php echo $_GET["buy"]?>" onclick="this.blur();"><span> Shipping </span></a></td>
                                                                        </tr>
                                                                        
                                                                    </table></td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td><form action="" method="post" id="form1" >  <div id="box">
                                                                            <div id="container">
                                                                                <div class="demo_jui">
                                                                                    <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <th width="8%" align="center">Sr.</th>
                                                                                                <th width="20%" align="left">Connection</th>
                                                                                                <th width="10%" align="left"> LFD</th>
                                                                                                <th width="5%" align="left"> </th>
                                                                                                <th width="10%" align="left">Transit days</th>
                                                                                                <th width="5%" align="left"> </th>
                                                                                                <th width="10%" align="left">Delivery day</th>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                            <?php
                                                                                            $sr = 1;
                                                                                            $temp = explode(",", $info["connect_group"]);
                                                                                            
                                                                                            while ($colors = mysqli_fetch_array($rs)) {
                                                                                                $getCountry = "select id, country, city, descripcion, region_id 
                                                                                                                 from destiny 
                                                                                                                where id='".$colors['destiny']."'";
                                                                                                
                                                                                                $countryRes = mysqli_query($con,$getCountry);
                                                                                                $country = mysqli_fetch_assoc($countryRes);
                                                                                                $destiny = $country['descripcion'];

                                                                                                $connections = unserialize($colors['connections']);
                                                                                                $getcon = mysqli_query($con,"select cargo_agency from connections where id='".$connections['connection_1']."'");
                                                                                                $getconRes = mysqli_fetch_assoc($getcon);
                                                                                                $cargoid = $getconRes['cargo_agency'];
                                                                                               
                                                                                                ?>
                                                                                            
                                                                                            <?php if (in_array($colors["id_conn"], $temp) != "") {?>
                                                                                            
                                                                                                <tr class="gradeU">
                                                                                                    <td align="center" class="text"><?= $sr; ?></td>
                                                                                                    <td class="text" align="left"><?= $colors["connections"] ?></td>
                                                                                                    <td class="text" align="left"><?= $colors["daysWeek"] ?></td>
                                                                                                    <td class="text" align="center"> + </td>
                                                                                                    <td class="text" align="center"><?= $colors["trasit_time"] ?></td>                                                                                                    
                                                                                                    <td class="text" align="center"> = </td>
                                                                                                    <td class="text" align="center"><?= $colors["deliveryDay"] ?></td>
                                                                                                </tr>                                                                                           
                                                                                            
                                                                                                <?php    }
                                                                                                $sr++;
                                                                                            }
                                                                                            ?>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                            </div>



                                                                        </div> 

                                                                    </form></td>
                                                            </tr>
                                                        </table></td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table></td>
                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
                                    </tr>
                                    <tr>
                                        <td background="images/middle-leftline.gif"></td>
                                        <td>&nbsp;</td>
                                        <td background="images/middle-rightline.gif"></td>
                                    </tr>
                                    <tr>
                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
                                        <td background="images/middle-bottomline.gif"></td>
                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <?php include("includes/footer-inner.php"); ?>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
