<?php

// PO 2018-09-21 

require_once("../config/config_gcp.php");
session_start();

$id_offer  = $_GET['id_offer'];

$idbuyer = $_GET['id_buy'];
$fac_id  = $_GET['id_fact'];

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1){
	header("location: index.php");
}

if($_SESSION['grower_id']!=0){
	header("location: growers.php?id=".$_SESSION['grower_id']);
}  
			

	 $qsel="select gor.id  as gorid , gor.offer_id , gor.offer_id_index , gor.grower_id  , gor.size , gor.price ,
                       gor.steams       , gor.product  , gor.boxqty         , gor.buyer_id   , gor.grower_id  ,
                       gor.bunchqty     , gor.boxtype  , gor.status         , gor.request_id , g.growers_name as gname,
                       gor.type_market  , gor.product_subcategory           , br.id_order    , gor.cliente_id ,
                       substr(cl.name,1,12) as namecli
                  from grower_offer_reply gor
                 inner join growers g on gor.grower_id = g.id
                 inner join buyer_requests br on gor.offer_id = br.id
                  left join sub_client cl ON cl.id = gor.cliente_id                 
                 where gor.buyer_id = '" . $idbuyer . "'
                   and br.id_order  = '" . $fac_id . "'
                 order by g.growers_name ";	

	 $rs=mysqli_query($con,$qsel);
         
	if(isset($_GET['delete'])) 	{
	  $queryDel = 'DELETE FROM grower_offer_reply WHERE id = '.(int)$_GET['delete'];
	  mysqli_query($con,$queryDel);
          
          header('location:grower_offer_mgmt_check.php?id_fact='.$tmp_order.'&id_buy='.$tmp_buyer);          
          
          //header('location:buy_flowers_request_others.php');          
	}                 

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/demo_page.css" rel="stylesheet" type="text/css" />
<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {
				oTable = $('#example').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
			} );
</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
 <?php include("includes/header_inner.php");?>

  <tr>
    <td height="5"></td>
  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/agent-left.php");?>

        <td width="5">&nbsp;</td>
        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>
            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td height="5"></td>
                   </tr>

                  <tr>
                    <td class="pagetitle">Offers</td>
                  </tr>
                 
                  <tr>
                    <td>&nbsp;</td>
                  </tr>

                  <tr>

                <td><div id="box">

		<div id="container">			
                    <div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>       
                    <th align="left" width="30%" >Grower</th>                      
                    <th align="left" width="25%" >Variety</th>  
                    <th align="left" width="12%">Size</th>                    
                    <th align="left" width="12%">Bun</th>                    
                    <th align="left" width="12%">St.</th>                                        
                    <th align="left" width="15%">Price</th>   
                    <th align="left" width="20%">Market</th>                                                                                
                    <th align="left" width="30%" >Client</th>                                          
		</tr>

</thead>

	<tbody>
		<?php

		  	$sr=1;
        		  while($product=mysqli_fetch_array($rs))  {
                              if ($product["type_market"] == "0") {
                                    $market = "Standing Order";
                              }else{
                                    $market = "Open Market";
                              }   
                              
                              if ($product["cliente_id"] == 0) {
                                    $soloclien = "";
                              }else{
                                    $soloclien = $product["namecli"];
                              }                                 
                              
                              $tmp_order = $product["id_order"];
                              $tmp_buyer = $product["buyer_id"];
                              
		?>
                          <tr class="gradeU">                          
                                <td class="text" align="left"><?php echo $product["gname"]?></td>                              
                                <td class="text" align="left"><?php echo $product["product"]." ".$product["product_subcategory"]?></td>                                                                                  
                                <td class="text" align="left"><?php echo $product["size"]?>  </td>                                                                                                                  
                                <td class="text" align="left"><?php echo $product["bunchqty"]?> </td>                                                           
                                <td class="text" align="left"><?php echo $product["steams"]?> </td>                                                                                           
                                <td class="text" align="left"><?php echo $product["price"]?> </td>  
                                <td class="text" align="left"><?php echo $market?> </td>  
                                <td class="text" align="left"><?php echo $soloclien?> </td>                                  
                                
                                <td align="center" ><a href="grower_offer_edit.php?idor=<?php echo $product["gorid"]."&bck=".$product["offer_id"] ?> "><img src="images/edit.gif" border="0" alt="Edit" /></a></td>                                
                                <td align="center" ><a href="?delete=<?php echo $product["gorid"]?>"  onclick="return confirm('Are you sure, you want to delete this Offer ? ');"><img src="images/images_front/delete.gif" border="0" alt="Delete" /></a></td>                                                                                

                          </tr>
				 <?php
			     $sr++;
			   } ?> 	
	</tbody>
</table>
                                </div>
                            </div>
			</div>
		</td>
                </tr>
                </table>
                </td>

                <td width="10">&nbsp;</td>

                    </tr>
                </table>                    
            </td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>
          </tr>

          <tr>
            <td background="images/middle-leftline.gif"></td>
            <td>&nbsp;</td>
            <td background="images/middle-rightline.gif"></td>
          </tr>

          <tr>
            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>
            <td background="images/middle-bottomline.gif"></td>
            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>
          </tr>
        </table>
        </td>
      </tr>

    </table>
    </td>
  </tr>

  <tr>
    <td height="10"></td>
  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>