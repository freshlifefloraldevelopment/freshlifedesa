<?php
    // PO #1  2-jul-2018
	include "../config/config_gcp.php";

if (!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1) {

    header("location: index.php");

}

if ($_SESSION['grower_id'] != 0) {
    header("location: growers.php?id=" . $_SESSION['grower_id']);
}

if (isset($_POST['change_ppic'])) {
    $today = date('mdyHis');
    if ($_FILES['ppic']['name'] != "") {
        $tmp1 = $_FILES['ppic']['name'];
        $ext1 = explode('.', $tmp1);
        $image = 0;
        $uploaddir = '../includes/assets/profile_pictures/';
        $filename = $today . "-" . $ext1[0] . "." . $ext1[1];
        $uploadfile1 = $uploaddir . $filename;

        move_uploaded_file($_FILES['ppic']['tmp_name'], $uploadfile1);
        $sql = "UPDATE admin SET picture = '" . $filename . "' WHERE id = " . $_SESSION['tomodachi-admin'];
        $res = mysqli_query($con, $sql);
        if ($res) {
            header("Location:home.php");
        }
    }

}


$sql_data = "SELECT uname, picture FROM admin WHERE id= " . $_SESSION['tomodachi-admin'] . " AND isadmin = 1";
$data_res = mysqli_query($con, $sql_data);
$row = mysqli_fetch_assoc($data_res);

$pic_path = SITE_URL . 'includes/assets/profile_pictures/' . $row['picture'];

?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>Admin Area</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" language="javascript" src="../assets/js/jquery.js"></script>

</head>


<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

    <?php include("includes/header_inner.php"); ?>
    <tr>
        <td height="5"></td>
    </tr>
    <tr>

        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr>

                    <?php include("includes/left.php"); ?>

                    <td width="5">&nbsp;</td>

                    <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>

                                <td width="10" valign="top" background="images/images_front/middle-leftline.gif"><img src="images/images_front/middle-topleft.gif" width="10" height="80"/></td>

                                <td valign="top" background="images/images_front/middle-topshade.gif"
                                    style="background-repeat:repeat-x;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                        <tr>

                                            <td width="10">&nbsp;</td>

                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                    <tr>
                                                        <td height="5"></td>

                                                    </tr>

                                                    <tr>
                                                        <td class="pagetitle">Welcome to Fresh Life Floral Admin Area Home Page</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>
                                                        <td><img src="<?php echo $pic_path; ?>" alt="Profile Picture"
                                                                 width="140" height="140"/></td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <form action="" method="post" enctype="multipart/form-data">
                                                                <input type="file" name="ppic" id="ppic"/><br><br>
                                                                <input type="submit" name="change_ppic" value="Update"/>
                                                            </form>
                                                        </td>
                                                    </tr>


                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>


                                                    <tr>

                                                        <td>&nbsp;</td>

                                                    </tr>

                                                </table>
                                            </td>

                                            <td width="10">&nbsp;</td>

                                        </tr>
                                    </table>
                                </td>

                                <td width="10" valign="top" background="images/images_front/middle-rightline.gif"><img
                                            src="images/images_front/middle-topright.gif" width="10" height="80"/></td>

                            </tr>

                            <tr>

                                <td background="images/images_front/middle-leftline.gif"></td>

                                <td>&nbsp;</td>

                                <td background="images/images_front/middle-rightline.gif"></td>

                            </tr>

                            <tr>

                                <td height="10"><img src="images/images_front/middle-bottomleft.gif" width="10" height="10"/></td>

                                <td background="images/images_front/middle-bottomline.gif"></td>

                                <td><img src="images/images_front/middle-bottomright.gif" width="10" height="10"/></td>

                            </tr>

                        </table>
                    </td>

                </tr>

            </table>
        </td>

    </tr>

    <tr>

        <td height="10"></td>

    </tr>

    <?php include("includes/footer-inner.php"); ?>

    <tr>

        <td>&nbsp;</td>

    </tr>

</table>

</body>

</html>

