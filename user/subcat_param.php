<?php
	include "../config/config_gcp.php";

	session_start();

	if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)	{
		header("location: index.php");
	}
        
        $sel_info = "select * from subcategory where id='" . $_GET["ids"] . "'  ";
        $rs_info = mysqli_query($con,$sel_info);
        $info = mysqli_fetch_array($rs_info);  

        $sel_card = "select * from packing_card where id_subcate='" . $_GET["ids"] . "'  ";
        $rs_card = mysqli_query($con,$sel_card);
        $info_card = mysqli_fetch_array($rs_card);          

	if(isset($_GET['delete'])) 	{
	  $query = 'DELETE FROM grower_parameter WHERE  id= '.(int)$_GET['delete'];
	  mysqli_query($con,$query);
          
	}
        
	

	$qsel="select gp.id   , gp.idsc , gp.subcategory , gp.size , gp.stem_bunch  , gp.description ,
                      gp.type , gp.feature , gp.factor , gp.id_ficha ,gp.price_adm ,
                      s.name as namesize   , f.name as namefeature , b.name as namestems
                 from grower_parameter gp 
                inner JOIN sizes s ON gp.size = s.id 
                inner JOIN bunch_sizes b ON gp.stem_bunch = b.id 
                 left JOIN features f  ON gp.feature = f.id   
                where gp.idsc = '" . $info["id"] . "'  
                  and gp.id_ficha = '" . $_GET["id_card"] . "'    
                order by s.name";
        
        
               
	$rs=mysqli_query($con,$qsel);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<title>Admin Area</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />

<link href="css/demo_page.css" rel="stylesheet" type="text/css" />

<link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />

<link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" language="javascript" src="js/jquery.js"></script>

<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>

<script type="text/javascript" charset="utf-8">

			$(document).ready(function() {

				oTable = $('#example').dataTable({

					"bJQueryUI": true,

					"sPaginationType": "full_numbers"

				});

			} );

</script>

</head>

<body>

<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

 <?php include("includes/header_inner.php");?>

  <tr>

    <td height="5"></td>

  </tr>

  <tr>

    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

      <tr>

       <?php include("includes/product-left.php");?>

        <td width="5">&nbsp;</td>

        <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

            <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

              <tr>

                <td width="10">&nbsp;</td>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>

                    <td height="5"></td>

                  </tr>

                  <tr>
                       <td class="pagetitle">Parameters Sub Categories - <?php echo $info["name"] ?></td>
                  </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

		<tr>
                    <td>
			<table width="100%">
				<tr>
				    <td>
					<a href="subcat_param_add.php?ids=<?php echo $_GET["ids"]."&id_card=".$_GET["id_card"] ?> " class="pagetitle" onclick="this.blur();"><span> + Add New Sub Category Parameters </span></a>
                                        <br>   
                                        <br>
					<a href="subcat_mgmt.php" class="pagetitle" ><span> Return Sub Category </span></a>
				    </td>                                    
				</tr>
			</table>
		    </td>
                </tr>

                  <tr>

                    <td>&nbsp;</td>

                  </tr>

                  <tr>

                    <td><div id="box">

		<div id="container">			

			<div class="demo_jui">

<table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

	<thead>

		<tr>

			<th width="8%" align="center">Sr.</th>

			<th width="25%" align="left">Subcategory</th>
                        
			<th width="10%" align="left">Card</th>                        
                        
			<th width="25%" align="left">Sizes</th>

                        <th width="14%" align="left">Features</th>

                        <th width="25%" align="left">St/Bu</th>

                        <th width="20%" align="left">Factor</th>
                        
                        <th width="20%" align="left">Price</th>                        
                                                                        
                        <th align="center" width="8%">Edit</th>

			<th align="center" width="10%">Delete</th>

		</tr>

	</thead>

	<tbody>

		<?php
			  	$sr=1;

			while($subcategory=mysqli_fetch_array($rs))	  {

		 ?>

                       <tr class="gradeU">

                          <td align="center" class="text"><?php echo $sr;?></td>
                          <a href="subcat_param.php"></a>
                          <td class="text" align="left"><?php echo $subcategory["subcategory"]?></td>                          
                          
                          <td class="text" align="left"><?php echo $subcategory["id_ficha"]?></td>                                                    

                          <td class="text" align="left"><?php echo $subcategory["namesize"]." cm"?></td>
                          
                          <td class="text" align="left"><?php echo $subcategory["namefeature"]?></td>
                           
                          <td class="text" align="left"><?php echo $subcategory["namestems"]?></td>                           
                          
                          <td class="text" align="left"><?php echo $subcategory["factor"]?></td>     
                          
                          <td class="text" align="left"><?php echo $subcategory["price_adm"]?></td>                               
                          
                          <!--td class="text" align="center"><a href="packing_card_generate1.php?ids=<?php echo $subcategory["id"]?> "  >Generate </a> </td-->                                                      
                          
                          <!--td class="text" align="center"><a href="packing_card_mgmt.php?ids=<?php echo $subcategory["id"]?> "  >Card </a> </td-->                            
                           
                          <td align="center" ><a href="subcat_param_edit.php?id_param=<?php echo $subcategory["id"]?>"><img src="images/edit.gif" border="0" alt="Edit" /></a></td>

			  <td align="center" ><a href="?delete=<?php echo $subcategory["id"]?>"  onclick="return confirm('Are you sure, you want to delete this Parameter ?');"><img src="images/delete.gif" border="0" alt="Delete" /></a></td>

                       </tr>

			 <?php

				 		$sr++;

				}

			 ?> 

	

	</tbody>

</table>



			</div>

			</div>



			</div>

		</td>

                  </tr>

                </table></td>

                <td width="10">&nbsp;</td>

              </tr>

            </table></td>

            <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

          </tr>

          <tr>

            <td background="images/middle-leftline.gif"></td>

            <td>&nbsp;</td>

            <td background="images/middle-rightline.gif"></td>

          </tr>

          <tr>

            <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

            <td background="images/middle-bottomline.gif"></td>

            <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

          </tr>

        </table></td>

      </tr>

    </table></td>

  </tr>

  <tr>

    <td height="10"></td>

  </tr>

  <?php include("includes/footer-inner.php"); ?>

  <tr>

    <td>&nbsp;</td>

  </tr>

</table>

</body>

</html>
