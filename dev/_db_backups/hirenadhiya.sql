-- MySQL dump 10.11
--
-- Host: 68.178.139.39    Database: hirenadhiya
-- ------------------------------------------------------
-- Server version	5.0.96-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `uname` varchar(150) collate latin1_general_ci NOT NULL,
  `pass` varchar(150) collate latin1_general_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('admin','admin123');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(300) NOT NULL,
  `cat_image` varchar(255) NOT NULL,
  `cat_desc` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (9,'Roses','cateimage/022613055256ha.jpg','Roses are probably the most commonly known and grown flowers worldwide but nothing compares to the splendor and elegance of Ecuadorian roses. The Ecuadorian roses are larger and more fragrant than the ordinary ones. An absolute classic, roses are great for all occasions and are the ideal flowers for decorators and floral artists. We have a huge selection of roses that differs in colors, sizes, and varieties. Red, white, pink, champagne, yellow, orange, brown, and even blue, we have them all.'),(10,'Exotics Blooms and Foliages','cateimage/021413113028ha.jpg','Found in areas with tropical and subtropical climate, this family of colourful flowers are used in decorative arrangements. With the stem usually consists of folded blades, these flowers make great ornaments and sometimes have a sweet smelling aroma. We have Red Ginger Lily, Ginger Shampoo, Ginger Torch which is sometimes called the walking-stick ginger, and more.  '),(12,'Greens, Foliages and Branches','cateimage/021413113218ha.jpg','Greens are a necessity for every florist! Greenery is available in all shades of green, sizes and textures. They are fillers that add colour, contrast and character to designs. Brightly coloured flowers come alive in floral arrangements against the green background. They also help make arrangements appear fuller. Greenery or foliage is a cost effective method to hide those unsightly floral foams.'),(13,'Preserved and Dried ','cateimage/021413113710ha.jpg','Preserved flowers are real flowers! Unlike \"dried flowers,\" preserved flowers and foliage are natural materials selected and cut at the peak of blooming beauty. The natural plants sap is then gradually replaced, causing the flower or plant to maintain its original, fresh-cut state.\r\n\r\nAfter going through this \"preserving\" process, flowers and other foliage remain soft and keep their colors brilliant for many years. The flowers keep that just-cut texture and color without any maintenance (no water or light required).');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colors` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'Green'),(2,'Red'),(3,'Yellow'),(6,'Pink '),(7,'Hot Pink'),(8,'White & Cream'),(9,'Orange');
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enquiry`
--

DROP TABLE IF EXISTS `enquiry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enquiry` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(400) NOT NULL,
  `email` varchar(400) NOT NULL,
  `company` varchar(400) NOT NULL,
  `contactno` varchar(400) NOT NULL,
  `message` text NOT NULL,
  `product` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enquiry`
--

LOCK TABLES `enquiry` WRITE;
/*!40000 ALTER TABLE `enquiry` DISABLE KEYS */;
/*!40000 ALTER TABLE `enquiry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery_mgmt`
--

DROP TABLE IF EXISTS `gallery_mgmt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery_mgmt` (
  `id` int(11) NOT NULL auto_increment,
  `image_path` varchar(250) collate latin1_general_ci NOT NULL,
  `thumbnail` varchar(250) collate latin1_general_ci NOT NULL,
  `title` varchar(400) collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery_mgmt`
--

LOCK TABLES `gallery_mgmt` WRITE;
/*!40000 ALTER TABLE `gallery_mgmt` DISABLE KEYS */;
INSERT INTO `gallery_mgmt` VALUES (1,'gallery-image/big/102311011151_crop.jpg','gallery-image/small/102311011151_crop.jpg','Frist Image'),(2,'gallery-image/big/102311011152_crop2.jpg','gallery-image/small/102311011152_crop2.jpg','Second Image'),(3,'gallery-image/big/102311011152_crop3.jpg','gallery-image/small/102311011152_crop3.jpg','Third Image'),(4,'gallery-image/big/102311011230_crop.jpg','gallery-image/small/102311011230_crop.jpg','Fourth Image'),(5,'gallery-image/big/102311011231_crop2.jpg','gallery-image/small/102311011231_crop2.jpg','Fifth Image'),(6,'gallery-image/big/102311011232_crop3.jpg','gallery-image/small/102311011232_crop3.jpg','SIxth Image'),(7,'gallery-image/big/102311080435_crop.jpg','gallery-image/small/102311080435_crop.jpg','Seventh Image'),(8,'gallery-image/big/102311080436_crop2.jpg','gallery-image/small/102311080436_crop2.jpg','Eighth Image'),(9,'gallery-image/big/103111225845_crop.png','gallery-image/small/103111225845_crop.png','Title 1'),(10,'gallery-image/big/103111225845_crop2.png','gallery-image/small/103111225845_crop2.png','Title 2'),(11,'gallery-image/big/103111225845_crop3.png','gallery-image/small/103111225845_crop3.png','Title 3');
/*!40000 ALTER TABLE `gallery_mgmt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `home_slideshow`
--

DROP TABLE IF EXISTS `home_slideshow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `home_slideshow` (
  `id` int(11) NOT NULL auto_increment,
  `image_path` varchar(250) collate latin1_general_ci NOT NULL,
  `slide_title` varchar(255) collate latin1_general_ci NOT NULL,
  `slide_description` text collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `home_slideshow`
--

LOCK TABLES `home_slideshow` WRITE;
/*!40000 ALTER TABLE `home_slideshow` DISABLE KEYS */;
INSERT INTO `home_slideshow` VALUES (28,'slideshow/021713064953ha.png','OUR GROWERS','We seek out mutually beneficial, long term relationships with our farms. We hold our farms and growers to very high\r\nstandards of quality, transparency and ethical responsibility, to ensure quality on behalf of our clients'),(27,'slideshow/021713064902ha.png','INTERNATIONAL SHIPPING PARTNERS','At Fresh Life Floral, we work closely with a number of shipping providers in order\r\nto offer our order fulfillment clients the best shipping options available.\"');
/*!40000 ALTER TABLE `home_slideshow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `homebox_mgmt`
--

DROP TABLE IF EXISTS `homebox_mgmt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `homebox_mgmt` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(350) collate latin1_general_ci NOT NULL,
  `description` varchar(350) collate latin1_general_ci NOT NULL,
  `image_path` varchar(150) collate latin1_general_ci NOT NULL,
  `safari_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `homebox_mgmt`
--

LOCK TABLES `homebox_mgmt` WRITE;
/*!40000 ALTER TABLE `homebox_mgmt` DISABLE KEYS */;
INSERT INTO `homebox_mgmt` VALUES (7,'FREIGHT HANDLING ','At Fresh Life Floral, we want our customers to receive the most convenient and comfortable buying experience. Enjoy worry-free product shipment as our company take care of all the paperwork that involves in the product shipment. We pay close attention to all related forms, letters, and reports, ensuring...','home-box/021613235127_crop.jpg',0),(8,'QUALITY CONTROL','Our company strongly believes that quality should never be compromised no matter what. To uphold our products quality and standards, we have assembled a strict quality control system. This exceptional system oversees the whole operation of our business, from the flowers growing at our farms to the cutting...','home-box/021713010116_crop.jpg',0),(9,'PACKAGING AND LABELING','We are constantly improving and testing a variety of options to develop new and better packages for our products. We source the best quality materials from reliable suppliers to make our packages. Made with durable materials and skilfully crafted, our specially made packages can best withstand the harshness...','home-box/021713010222_crop.jpg',0),(10,'WEB SHOP','Everything is focused on providing an optimal service and a convenient ordering system. Our web shop, the most advanced in the industry, therefore allows you to simply and quickly make purchases online, whereby you can make your own choices from a vary diverse supply of flowers...','home-box/021713010258_crop.jpg',0);
/*!40000 ALTER TABLE `homebox_mgmt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_mgmt`
--

DROP TABLE IF EXISTS `page_mgmt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_mgmt` (
  `page_id` int(11) NOT NULL auto_increment,
  `page_name` varchar(150) collate latin1_general_ci NOT NULL,
  `page_title` varchar(150) collate latin1_general_ci NOT NULL,
  `page_desc` text collate latin1_general_ci NOT NULL,
  `meta_title` text collate latin1_general_ci NOT NULL,
  `meta_keyword` text collate latin1_general_ci NOT NULL,
  `meta_desc` text collate latin1_general_ci NOT NULL,
  PRIMARY KEY  (`page_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_mgmt`
--

LOCK TABLES `page_mgmt` WRITE;
/*!40000 ALTER TABLE `page_mgmt` DISABLE KEYS */;
INSERT INTO `page_mgmt` VALUES (1,'About Us','About Us','<p>\r\n	Known for its large and vibrant blooms, the Ecuadorian flowers are everything but the ordinary. Located at the heart of South America, right on the equator, these beautiful flowers receive the best treatment from the most passionate growers in the world. Aside from having perfect weather conditions and rich fertile soil, Ecuador is also home to some of the world&rsquo;s finest and talented floral growers, creating the finest cut flowers and lush Ecuadorian plants.</p>\r\n<p>\r\n	<strong>We saw the opportunity to offer our customers the best of the bests, so we did.</strong><br />\r\n	Having the means and knowledge of exporting high quality Ecuadorian flowers, we at Fresh Life Floral specialize in distributing and selling these flowers to markets and individuals both local and international.</p>\r\n<p>\r\n	We offer a wide range of options. We have a beautiful array of flower varieties varying in colours and sizes. Some of these flowers are one-of-a-kind and can only be found in our farms. Every order is nicely wrapped, packaged, and labelled accordingly. From our regular solid, one colored boxes to more complex, mix colored boxes, our boxes and packaging supplies are made of top quality materials, acquired from our most trusted suppliers. These are durable and long-lasting packaging to make sure the flowers are still fresh and beautiful after shipping.</p>\r\n<p>\r\n	<strong>Customer satisfaction is our motivation. </strong><br />\r\n	We have perfected our working process over the years. Processed and handled with utmost care by our team of professionals, all our products pass by an extensive quality control measures before being shipped out. We ensure that all flowers are delivered fresh and on time. We work hard to offer the best quality products with the best quality service as we take pride in sending products that exceed our customers&rsquo; expectations.</p>\r\n<p>\r\n	<strong>We&rsquo;re high-class but not high-priced. </strong><br />\r\n	Here at Fresh Life Floral, we believe high quality products doesn&rsquo;t necessarily come with a high price tag. Because of our efficient business structure and good working relationship with our suppliers, we guarantee the freshest and most beautiful flowers at a reasonable price. Fast, fresh, perfect, and reasonably priced flowers are what we offer our customers.</p>\r\n<p>\r\n	<strong>Find us worldwide.</strong><br />\r\n	We have been providing different types of flowers to wholesalers, supermarkets, and flower designers across the globe. It is our goal to make Ecuadorian flowers available in every continent, sharing the vibrant colors and lush greens with everyone everywhere. Our flowers can be found all over the world with clients in Japan, USA, Germany, Switzerland, Canada, the Netherlands, Taiwan, Korea, India, China, Singapore, Russia, New Zealand and more.</p>\r\n<p>\r\n	<strong>We are dedicated to serve.</strong><br />\r\n	Fresh Life Floral has a great reputation for going the extra mile just to meet our customers&rsquo; diverse floral demands. We exert all effort to provide our buyers with their choice flowers at their chosen place and time. Our broad selection of shipping and delivery services also makes buying and ordering easy and convenient to anyone in the world. In addition to that, we&rsquo;re constantly improving our products and services to better serve our customers.</p>\r\n','Flowers from ecuador','Flowers from ecuador','Flowers from ecuador'),(2,'Our Flowers','Our Flowers','<p>\r\n	Introduction to Ecuadorian flowers</p>\r\n<p>\r\n	A diverse country located right on the equator along the Pacific Coast, Ecuador has several tropical cloud forests, dry forests, rainforests, and mangrove swamps, making it the ideal home to more than 25,000 species of plants and flowers. Ecuadorian plants and flowers are said to represent ten percent of the world&rsquo;s known plants. Distinctively beautiful and vibrant, Ecuadorian flowers are the favorite of many flower designers worldwide. Among the flowers found in Ecuador are those of the orchid kind, passion flowers, and bromeliads.</p>\r\n<p>\r\n	<strong>What we have</strong><br />\r\n	Fresh Life Floral is the top source for the finest Ecuadorian flowers.</p>\r\n<p>\r\n	Here at Fresh Life Floral, we are passionate with our flowers. Simply put, we love our Ecuadorian flowers and want to share the exquisiteness and beauty of each and every Ecuadorian flower to the world.</p>\r\n<p>\r\n	We have a variety of flowers for all sorts of occasion. Browse through our list of available flowers and plants to help you with your decision. Our farm carries an assortment of flowers that include, but not limited to, the following:</p>\r\n','Flowers From Ecuador','Flowers From Ecuador','Flowers From Ecuador'),(3,'Buying Strategy','Buying Strategy','<p>\r\n	We offer not only the best flowers but also the best in service. The moment you avail of our service, we make sure we handpick and harvest the freshest flowers for you. From our growers to our sales team and shipping partners, Fresh Life Floral are passionate in serving our customers with the finest goods and services. What makes us different from other companies is that we don&rsquo;t carry inventories, ensuring the freshest product out there. Our system is beyond compare thus we stand out from the rest.</p>\r\n<p>\r\n	We sell the highest quality products at the most reasonable price. A daily price list is available for download or you can also just contact us directly. Composed of the most driven and accommodating consultants, our sales team is always available and ready to take your call and assist you with your every need. They are highly knowledgeable on the floral industry and can give you more detailed information about our products and services. You can reach them through phone, fax, or email. You may also follow our standard purchasing process to get the most out of our service. This step-by-step process assures you of a smooth buying experience.</p>\r\n<ol>\r\n	<li>\r\n		First, get the updated list of our available products and their prices. Download from our website or get it directly from our sales group.</li>\r\n	<li>\r\n		Make the order. Let us know what you need and we&rsquo;ll get it for you. You can choose from our wide variety of flowers, find the colors and sizes that you require. Hard to find or rare flowers are also available by request. Email, call, or send us a fax, we&rsquo;ll get right on it.</li>\r\n	<li>\r\n		Make the necessary payments. You can pay using cash, credit card, or check. We also accept payments through bank deposits and international money transfer, whichever is more convenient for you.</li>\r\n	<li>\r\n		We&rsquo;ll process your order. By picking the best flowers at the farm and customizing the packaging and labelling accordingly, we&rsquo;ll make sure we meet all your requests. Our quality control group will see to it that you&rsquo;ll receive the best of our products and services.</li>\r\n	<li>\r\n		Our well-trained sales associates will assist you with the shipping details, making sure all forms, letters, and important shipping documents are properly filed and released.</li>\r\n	<li>\r\n		Fresh Life Floral has several trusted freight agents that can give you the most excellent shipping treatment. We&rsquo;ll make sure your orders reach you in good shape and on time.</li>\r\n	<li>\r\n		Let us know how we did. We welcome your opinions and suggestions to help us improve our products and services.</li>\r\n</ol>\r\n<p>\r\n	Fresh Life Floral is expanding our reach and providing our service online. For your convenience, we are now equipped with our very own Fresh Life E-Shop system to make your floral shopping experience with us fast, easy, and safe. You can now place your orders in a breeze with just a few clicks. Safe and secure, transactions made through our Fresh Life E-Shop are carefully managed by our top sales consultants.</p>\r\n','Flowers From Ecuador','Flowers From Ecuador','Flowers From Ecuador'),(4,'Services','Services','<p>\r\n	We offer the finest service to match our high quality products.</p>\r\n<p>\r\n	<strong>Freight Handling</strong><br />\r\n	At Fresh Life Floral, we want our customers to receive the most convenient and comfortable buying experience. Enjoy worry-free product shipment as our company take care of all the paperwork that involves in the product shipment. We pay close attention to all related forms, letters, and reports, ensuring you a seamless shipment of your orders.</p>\r\n<p>\r\n	Your orders are in good hands with our wide selection of shipping partners. Over the years, we&rsquo;ve maintained good relationships with our trusted freight agents, airline and cargo partners to give you the best treatment and the fastest transit time available.</p>\r\n<p>\r\n	We are open to the following shipping arrangements:</p>\r\n<ol>\r\n	<li>\r\n		<strong>Freight on Board (FOB).</strong> The exporter delivers the goods at his cost. As soon as the responsibility of the shipper ends, that of the buyer begins which means the freight and other expenses for outbound traffic are shouldered by the importer.</li>\r\n	<li>\r\n		<strong>Airport of Destination.</strong> Cost, Insurance and Freight (CIF) &ndash; The exporter pays the insurance and freight to the port or airport of destination. However, the responsibility of the shipper ends at the port or airport of destination.</li>\r\n	<li>\r\n		<strong>Door-to-door.</strong> Delivered Duty Paid (DDP) &ndash; The exporter is responsible for all costs, expenses, and the goods until delivery has been made.</li>\r\n</ol>\r\n<p>\r\n	You may also call our sales consultants by phone or email them to know which direct logistical solution is best for you. They will also advise you on other shipping concerns such as the routing and points on clearing and handling at the airport of destination.</p>\r\n<p>\r\n	<strong>Quality Control</strong><br />\r\n	Our company strongly believes that quality should never be compromised no matter what. To uphold our products&rsquo; quality and standards, we have assembled a strict quality control system. This exceptional system oversees the whole operation of our business, from the flowers growing at our farms to the cutting and packaging of the final goods at our facility, to assure our customers that they get only the best of the bests. Our company has also employed a group of experts that focuses solely on quality control.</p>\r\n<p>\r\n	<strong>Packaging and Labelling</strong><br />\r\n	We are constantly improving and testing a variety of options to develop new and better packages for our products. We source the best quality materials from reliable suppliers to make our packages. Made with durable materials and skilfully crafted, our specially made packages can best withstand the harshness of long distance shipping. We can also customize the packages and labels based on your needs.</p>\r\n<p>\r\n	<strong>Payment Options</strong><br />\r\n	We offer several different payment options so you can make the most convenient and safest choice. You can send your payment via the following:</p>\r\n<ol>\r\n	<li>\r\n		<strong>Credit/Debit Card.</strong><br />\r\n		We accept VISA, MasterCard, American Express and the like.</li>\r\n	<li>\r\n		<strong>Check</strong><br />\r\n		Send via fax.</li>\r\n	<li>\r\n		<strong>Bank Deposit</strong><br />\r\n		This is for USA Accounts only. <a href=\"contact-us.php\">Contact us</a> for the details.</li>\r\n	<li>\r\n		<strong>International Bank Transfer</strong></li>\r\n</ol>\r\n<p>\r\n	Orders will be served after the confirmation of your payment. You may <a href=\"contact-us.php\">contact</a> our sales team to know more about our payment terms and other conditions.</p>\r\n','Flowers From Ecuador','Flowers From Ecuador','Flowers From Ecuador'),(5,'SiteMap','SiteMap','<div class=\"sitemap\">\r\n	<ul>\r\n		<li>\r\n			<a href=\"http://www.freshlifefloral.com/index.html\">Home</a></li>\r\n		<li>\r\n			<a href=\"http://www.freshlifefloral.com/about-us.html\">About us</a></li>\r\n		<li>\r\n			<a href=\"http://www.freshlifefloral.com/our-flowers.html\">Our Flower</a></li>\r\n		<li>\r\n			<a href=\"http://www.freshlifefloral.com/services.html\">Services</a></li>\r\n		<li>\r\n			<a href=\"http://www.freshlifefloral.com/buying-strategy.html\">Buying Strategy</a></li>\r\n		<li>\r\n			<a href=\"http://www.freshlifefloral.com/privacy-page.html\">Privacy Page</a></li>\r\n		<li>\r\n			<a href=\"http://www.freshlifefloral.com/terms-of-use.html\">Terms of Use</a></li>\r\n		<li>\r\n			<a href=\"http://www.freshlifefloral.com/earning-disclaimer.html\">Earnings Disclaimer</a></li>\r\n		<li>\r\n			<a href=\"http://www.freshlifefloral.com/copyright-info.html\">Copyright info</a></li>\r\n		<li>\r\n			<a href=\"http://www.freshlifefloral.com/sitemap.html\">SiteMap</a></li>\r\n		<li>\r\n			<a href=\"http://www.freshlifefloral.com/contact-us.php\">Contact us</a></li>\r\n	</ul>\r\n</div>\r\n','Flowers From Ecuador','Flowers From Ecuador','Flowers From Ecuador'),(6,'Privacy Page','Privacy Page','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n','','',''),(7,'Terms of Use','Terms of Use','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n','Flowers From Ecuador','Flowers From Ecuador','Flowers From Ecuador'),(8,'Earnings Disclaimer','Earnings Disclaimer','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n','Flowers From Ecuador','Flowers From Ecuador','Flowers From Ecuador'),(9,'Copyright info','Copyright info','<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<p>\r\n	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n','Flowers From Ecuador','Flowers From Ecuador','Flowers From Ecuador'),(10,'Home','Home','','Flowers from Ecuador','Flowers from Ecuador','Flowers from Ecuador');
/*!40000 ALTER TABLE `page_mgmt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL auto_increment,
  `categoryid` int(11) NOT NULL,
  `subcategoryid` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `image_path` varchar(300) NOT NULL,
  `color_id` int(11) NOT NULL,
  `tab_title1` varchar(255) NOT NULL,
  `tab_title2` text NOT NULL,
  `tab_title3` varchar(255) NOT NULL,
  `tab_desc1` text NOT NULL,
  `tab_desc2` text NOT NULL,
  `tab_desc3` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (8,9,2,'CABARET','product-image/big/022213132850_crop.jpg',3,'Overview','Features','Description','<p>\r\n	Saw polecat than took bankrupt good hillbilly stew, crazy, fancy and hillbilly heap rodeo, pappy. Thar range saw me him sherrif nothin shiney dirt, pigs sheep city-slickers everlastin shotgun driveway. Promenade catfight fart fiddle jiggly gonna tarnation, fence, what quarrel dirty, if. Pot grandma crop kinfolk jezebel diesel coonskin hoosegow wirey fixin shack good roped in. Reckon stew tax-collectors, grandpa tobaccee hayseed good wash tired caboodle burnin landlord.</p>\r\n','<p>\r\n	Saw polecat than took bankrupt good hillbilly stew, crazy, fancy and hillbilly heap rodeo, pappy. Thar range saw me him sherrif nothin shiney dirt, pigs sheep city-slickers everlastin shotgun driveway. Promenade catfight fart fiddle jiggly gonna tarnation, fence, what quarrel dirty, if. Pot grandma crop kinfolk jezebel diesel coonskin hoosegow wirey fixin shack good roped in. Reckon stew tax-collectors, grandpa tobaccee hayseed good wash tired caboodle burnin,landlord.</p>\r\n','<p>\r\n	Saw polecat than took bankrupt good hillbilly stew, crazy, fancy and hillbilly heap rodeo, pappy. Thar range saw me him sherrif nothin shiney dirt, pigs sheep city-slickers everlastin shotgun driveway. Promenade catfight fart fiddle jiggly gonna tarnation, fence, what quarrel dirty, if. Pot grandma crop kinfolk jezebel diesel coonskin hoosegow wirey fixin shack good roped in. Reckon stew tax-collectors, grandpa tobaccee hayseed good wash tired caboodle burnin landlord.</p>\r\n'),(41,9,0,'BLACK MAGIC','product-image/big/022613062307_crop.jpg',2,'','','','','',''),(42,9,2,'SEXY RED','product-image/big/022613062959_crop.jpg',2,'','','','','',''),(43,9,2,'ROUGER BAISER','product-image/big/022613063529_crop.jpg',2,'','','','','',''),(44,9,2,'DEVOTION','product-image/big/022613063805_crop.jpg',2,'','','','','',''),(45,9,2,'ROYAL BACCARA','product-image/big/022613064039_crop.jpg',2,'','','','','',''),(46,9,2,'ANNA','product-image/big/022613064858_crop.jpg',6,'','','','','',''),(47,9,2,'ENGAGEMENT','product-image/big/022613065113_crop.jpg',6,'','','','','',''),(48,9,2,'FAITH','product-image/big/022613065319_crop.jpg',6,'','','','','',''),(49,9,2,'NENAL','product-image/big/022613065728_crop.jpg',6,'','','','','',''),(50,9,2,'SAMBA PA TI','product-image/big/022613065941_crop.jpg',6,'','','','','',''),(51,9,2,'TITANIC','product-image/big/022613070412_crop.jpg',6,'','','','','',''),(52,9,2,'CHERRY O','product-image/big/022613071016_crop.jpg',7,'','','','','',''),(53,9,2,'TOPAZ','product-image/big/022613071253_crop.jpg',7,'','','','','',''),(54,9,2,'HOT PARIS ','product-image/big/022613085110_crop.jpg',7,'','','','','',''),(55,9,2,'ORCHESTRA','product-image/big/022613085449_crop.jpg',7,'','','','','',''),(56,9,2,'ROVEL ','product-image/big/022613085931_crop.jpg',7,'','','','','',''),(57,9,2,'DEJA VU ','product-image/big/022613094209_crop.jpeg',3,'','','','','',''),(58,9,2,'MOHANA','product-image/big/022613090335_crop.jpg',3,'','','','','',''),(59,9,2,'TARA','product-image/big/022613090455_crop.jpg',3,'','','','','',''),(60,9,2,'BUTTERSCOTCH','product-image/big/022613090747_crop.jpg',3,'','','','','',''),(61,9,2,'HUMMER','product-image/big/022613090902_crop.jpg',3,'','','','','',''),(62,9,2,'SKYLINE','product-image/big/022613091017_crop.jpg',3,'','','','','',''),(63,9,2,'POLAR STAR','product-image/big/022613091330_crop.jpg',8,'','','','','',''),(64,9,2,'ANASTASIA ','product-image/big/022613091453_crop.jpg',8,'','','','','',''),(65,9,2,'PROUND','product-image/big/022613091740_crop.jpg',8,'','','','','',''),(66,9,2,'MONDIAL','product-image/big/022613091918_crop.jpg',8,'','','','','',''),(67,9,2,'TIBET','product-image/big/022613092024_crop.jpg',8,'','','','','',''),(68,9,2,'VENDELA','product-image/big/022613092303_crop.jpg',8,'','','','','',''),(69,9,2,'WHITE DOVE','product-image/big/022613092727_crop.jpg',8,'','','','','',''),(70,9,2,'MONITOU','product-image/big/022613093537_crop.jpg',8,'','','','','',''),(71,9,2,'VOODOO','product-image/big/022613094458_crop.jpg',9,'','','','','',''),(72,9,2,'MOVIE STAR','product-image/big/022613094831_crop.jpg',9,'','','','','',''),(73,9,2,'MILVA','product-image/big/022613095048_crop.jpg',9,'','','','','',''),(40,9,2,'FOREVER YOUNG','product-image/big/022613062635_crop.jpg',2,'','','','','','');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategory`
--

DROP TABLE IF EXISTS `subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategory` (
  `id` int(11) NOT NULL auto_increment,
  `cat_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `subcat_image` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategory`
--

LOCK TABLES `subcategory` WRITE;
/*!40000 ALTER TABLE `subcategory` DISABLE KEYS */;
INSERT INTO `subcategory` VALUES (2,9,'Standard Rose','NICE AND BEAUTIFUL','subcateimage/022613055611ha.jpg');
/*!40000 ALTER TABLE `subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonial`
--

DROP TABLE IF EXISTS `testimonial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL auto_increment,
  `description` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `place` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonial`
--

LOCK TABLES `testimonial` WRITE;
/*!40000 ALTER TABLE `testimonial` DISABLE KEYS */;
INSERT INTO `testimonial` VALUES (2,'Thanks for the shipment. Good work! Thats why we call you  ','Terry Mackenzie, APEX FLORAL ','Mississauga, ON, Canada '),(3,'Amazing flowers; amazing service at an amazing price! Thank you, thank you! ','Rose Trujillo, ULTIMATE AFFAIRE','Long Beach, CA, USA');
/*!40000 ALTER TABLE `testimonial` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-03-06  4:14:29
