<?php 
      $page_id=6;
	  include("config/config.php");
// print_r($_REQUEST);
// print_r($_POST);
if (isset($_REQUEST['submit'])) {
	
	
	 if (empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['captcha'])) != $_SESSION['captcha']) {
		 
		  $captcha_message=1;
	 }
	 
	 else
	 {
	
		//Process a new form submission in HubSpot in order to create a new Contact.
		$hubspotutk = $_COOKIE['hubspotutk']; //grab the cookie from the visitors browser.
		$ip_addr = $_SERVER['REMOTE_ADDR']; //IP address too.
		$hs_context = array(
		'hutk' => $hubspotutk,
		'ipAddress' => $ip_addr,
		'pageUrl' => 'http://freshlifefloral.com/cms/contactus',
		'pageName' => 'Contact Us'
		);
		$hs_context_json = json_encode($hs_context);
		//Need to populate these varilables with values from the form.
		$str_post = "firstname=" . urlencode($_POST['first_name'])
		. "&email=" . urlencode($_POST['emailid'])
		. "&country=" . urlencode($_POST['country'])
		. "&markets=" . urlencode($_POST['markets'])
		. "&live_id=" . urlencode($_POST['liveid'])
		. "&phone_number=" . urlencode($_POST['phone'])
		. "&information=" . urlencode($_POST['description'])
		. "&company_name=" . urlencode($_POST['company'])
		. "&hs_context=" . urlencode($hs_context_json); //Leave this one be :)
		//replace the values in this URL with your portal ID and your form GUID
		$endpoint = 'https://forms.hubspot.com/uploads/form/v2/295496/69df5aed-3e03-40bb-b85e-429a6784fd97';
		$ch = @curl_init();
		@curl_setopt($ch, CURLOPT_POST, true);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
		@curl_setopt($ch, CURLOPT_URL, $endpoint);
		@curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = @curl_exec($ch); //Log the response from HubSpot as needed.
		@curl_close($ch);
// echo $response;
    
	 $first_name = $_POST['first_name']; // required
    $company = $_POST['company']; // required
	$from = $_POST['emailid']; // required
    $country = $_POST['country']; // required
    $markets = $_POST['markets']; // required
	$telephone = $_POST['phone']; // required
    $skypeid = $_POST['skypeid']; // required
	$liveid = $_POST['liveid']; // required
    $comments = $_POST['description']; // required
     
    // EDIT THE 2 LINES BELOW AS REQUIRED
	$to = "info@freshlifefloral.com" ;
    $subject = "Contact us Form";  
  //  $body = "Contact us Form.<br /><br />";
    $body .= "First Name: ".$first_name."<br />";
    $body .= "Company: ".$company."<br />";
    $body .= "Email: ".$from."<br />";
    $body .= "Country: ".$country."<br />";
    $body .= "Markets: ".$markets."<br />";
    $body .= "Phone Number: ".$telephone."<br />";
	$body .= "Skype Id: ".$skypeid."<br />";
    $body .= "Live id: ".$liveid."<br /><br />";
	$body .= "Description: ".$comments."<br />";
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	$headers .= "From: $from" . "\r\n";
	mail($to,$subject,$body,$headers);
	header("Location: http://freshlifefloral.com/contact-us.php?success=true");
	
	 }
	

}
?>
<!DOCTYPE html>
<html lang="en">
<?php include("include/head.php"); ?>
<body>
<!-- main wrapper starts -->
<div class="wrapper">
  <!-- header starts -->
   <?php include("include/header.php"); ?>
  <!-- header ends -->
  <div class="cl"></div>
  <!-- About us banner starts -->
  <!-- <div class="aboutus-banner">
    <div class="banner-about">
      <div class="ban-image"><img src="images/contact-ban-img.jpg" width="485" height="267" alt=""> </div>
      <div class="ban-content">
        <h3>Exporting high quality Ecuadorian flowers</h3>
        <p>Ecuador is also home to some of the world&rsquo;s finest and talented floral growers, creating the finest cut flowers and lush Ecuadorian plants. </p>
      </div>
    </div>
  </div> -->
  <!-- body starts -->
  <div class="cl"></div>
  <!-- inner-content starts -->
  <div class="content-container inner-content" style="clear:both; margin-top:25px; background:none;">
    <div class="brad" style="display:none;"><a href="index.php" class="no-active">Home</a> / <a href="contact-us.php">Contact us</a></div>
    <div class="cl"></div>
    <!-- Left Starts -->
    <div class="left-container">
   <?php
            if($_SESSION["lang"]!="ru") {  ?>
      <div class="left-mid" style="background:none;">
        <div class="content_area">
          <h2>Contact us</h2>
          <div class="contact-form">
          	<?php if($_REQUEST['success']=="true"){?>
		<div class="thankuMsg">
        <h3>Thanks for contact.</h3>
        <p>Our representative will get in touch with you max within 24 hours during weekdays and 48 hours during weekends.</p>
        </div>
		
		<?php } else {?>
        
       
          	<?php if($captcha_message==1){?>
		<div class="thankuMsg">
        <p style="color:red; font-size:14px;">Invalid Capcha</p>
        </div>
		
		<?php } ?>
        
<form action="" method="post" name="proposal" accept-charset="utf-8" onSubmit="return validate_press()" >
        <div class="form-row">
        <label>Your name<span>*</span>  </label><div class="right-form">
        <div class="textbox-bg">
        <input type="text" name="first_name"  /></div></div><div class="cl"></div>
        </div>
        
        <div class="form-row">
        <label>Your company name</label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="company" /></div></div><div class="cl"></div>
        </div>
        <div class="form-row">
        <label>Your email address<span>*</span>  </label><div class="right-form"><div class="textbox-bg">
        <input type="text" name="emailid" /></div></div><div class="cl"></div>
        </div>
        <div class="form-row">
        <label>Your Country &amp; City<span>*</span>  </label><div class="right-form"><div class="textbox-bg">
        <input type="text" name="country" /></div></div><div class="cl"></div>
        </div>
        <div class="form-row">
        <label>Your Markets</label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="markets" /></div></div><div class="cl"></div>
        </div>
        <div class="form-row">
        <label>Your Phone</label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="phone" /></div></div><div class="cl"></div>
        </div>
        <div class="form-row">
        <label>Skype ID</label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="skypeid" /></div></div><div class="cl"></div>
        </div>
        <div class="form-row">
        <label>Live messenger ID</label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="liveid" /></div></div><div class="cl"></div>
        </div>
          <div class="form-row">
        <label>Information Requested<span>*</span>  </label><div class="right-form"><div class="textarea-bg">
        <textarea name="description"></textarea></div></div><div class="cl"></div>
        </div>
        
         <div class="form-row">
        <label>Captcha  </label><div class="right-form">
        <img src="http://freshlifefloral.com/captcha.php" id="captcha" /></div><div class="cl"></div>
        </div>
        
         <div class="form-row">
        <label>Enter Captcha  <span>*</span></label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="captcha" id="captcha-form" autocomplete="off" /></div></div><div class="cl"></div>
        </div>
        
        
              <div class="form-row">
        <label>&nbsp; </label><input type="submit" name="submit" class="submit" value="" /><div class="cl"></div></div>
        </form>
        	<?php }?>
       </div>
			<div class="cont-mail">          
           Email us at <a href="mailto:info@freshlifefloral.com">info@freshlifefloral.com</a>
       		</div>
        </div>
        <div class="cl"></div>
      </div>
   <?php } else { ?>
   
     <div class="left-mid" style="background:none;">
        <div class="content_area">
          <h2>Связаться с Нами</h2>
          <div class="contact-form">
          	<?php if($_REQUEST['success']=="true"){?>
		<div class="thankuMsg">
        <h3>Спасибо за контакт.</h3>
        <p>Наш представитель свяжется с вами макс в течение 24 часов в рабочие дни и 48 часов в выходные.</p>
        </div>
		
		<?php } else {?>
        
       
          	<?php if($captcha_message==1){?>
		<div class="thankuMsg">
        <p style="color:red; font-size:14px;">Неверный Capcha</p>
        </div>
		
		<?php } ?>
        
<form action="" method="post" name="proposal" accept-charset="utf-8" onSubmit="return validate_press()" >
        <div class="form-row">
        <label>Ваше имя<span>*</span>  </label><div class="right-form">
        <div class="textbox-bg">
        <input type="text" name="first_name"  /></div></div><div class="cl"></div>
        </div>
        
        <div class="form-row">
        <label>Название вашей компании</label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="company" /></div></div><div class="cl"></div>
        </div>
        <div class="form-row">
        <label>Ваш электронный адрес<span>*</span>  </label><div class="right-form"><div class="textbox-bg">
        <input type="text" name="emailid" /></div></div><div class="cl"></div>
        </div>
        <div class="form-row">
        <label>Ваша страна &amp; City<span>*</span>  </label><div class="right-form"><div class="textbox-bg">
        <input type="text" name="country" /></div></div><div class="cl"></div>
        </div>
        <div class="form-row">
        <label>Ваши рынки</label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="markets" /></div></div><div class="cl"></div>
        </div>
        <div class="form-row">
        <label>Ваш телефон</label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="phone" /></div></div><div class="cl"></div>
        </div>
        <div class="form-row">
        <label>Skype ID</label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="skypeid" /></div></div><div class="cl"></div>
        </div>
        <div class="form-row">
        <label>Live messenger ID</label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="liveid" /></div></div><div class="cl"></div>
        </div>
          <div class="form-row">
        <label>информация просила<span>*</span>  </label><div class="right-form"><div class="textarea-bg">
        <textarea name="description"></textarea></div></div><div class="cl"></div>
        </div>
        
         <div class="form-row">
        <label>Captcha  </label><div class="right-form">
        <img src="http://freshlifefloral.com/captcha.php" id="captcha" /></div><div class="cl"></div>
        </div>
        
         <div class="form-row">
        <label>Enter Captcha  <span>*</span></label>
        <div class="right-form"><div class="textbox-bg">
        <input type="text" name="captcha" id="captcha-form" autocomplete="off" /></div></div><div class="cl"></div>
        </div>
        
        
              <div class="form-row">
        <label>&nbsp; </label><input type="submit" name="submit" class="submit" value="" /><div class="cl"></div></div>
        </form>
        	<?php }?>
       </div>
			<div class="cont-mail">          
           Пишите нам по адресу <a href="mailto:info@freshlifefloral.com">info@freshlifefloral.com</a>
       		</div>
        </div>
        <div class="cl"></div>
      </div>
   
   <? } ?>   
      <div class="bot"></div>
      <div class="cl"></div>
    </div>
    <!-- Left ends -->
    <!-- Right Starts -->
    <?php include("include/right.php"); ?>
    <!-- Right Ends -->
    <div class="cl"></div>
  </div>
  <!-- inner-content ends -->
  <!-- footer starts -->
  <div class="footerwraper">
    <?php include("include/footer.php"); ?>
  </div>
  <!-- footer ends -->
   <?php include("include/copyright.php"); ?>
  <div class="cl"></div>
  <!-- body ends -->
</div>
<!-- main wrapper -->
<!-- Fixed-div starts -->
<?php include("include/fixeddiv.php"); ?>
<!-- Fixed-div ends -->
</body>
</html>
