<? 
    
	include("config/config.php");	
	
	echo $sel_image="select file_path5 from growers where active!='deactive'";
	$rs_image=mysql_query($sel_image);
	
	while($image=mysql_fetch_array($rs_image))
	{
	 $img = imagecreatefrompng("user/".$image["file_path5"]);

    // find the trimmed image border
     $box = imageTrimmedBox($img);

    // copy cropped portion
    $img2 = imagecreate($box['w'], $box['h']);
    imagecopy($img2, $img, 0, 0, $box['l'], $box['t'], $box['w'], $box['h']);

    // output cropped image to the browser
    //header('Content-Type: image/png');
    //$r=imagepng($img2);
	$k=explode("/",$image["file_path5"]);
	imagejpeg($img2, "user/logo2/".$k[1]);
    }
    //imagedestroy($img);
    //imagedestroy($img2);


   function imageTrimmedBox($img, $hex=null){
    if($hex == null) $hex = imagecolorat($img, 0,0);
    $width = imagesx($img);
    $height = imagesy($img);
    $b_top = 0;
    $b_lft = 0;
    $b_btm = $height - 1;
    $b_rt = $width - 1;

    //top
    for(; $b_top < $height; ++$b_top) {
        for($x = 0; $x < $width; ++$x) {
            if(imagecolorat($img, $x, $b_top) != $hex) {
                break 2;
            }
        }
    }

    // return false when all pixels are trimmed
    if ($b_top == $height) return false;

    // bottom
    for(; $b_btm >= 0; --$b_btm) {
        for($x = 0; $x < $width; ++$x) {
            if(imagecolorat($img, $x, $b_btm) != $hex) {
                break 2;
            }
        }
    }

    // left
    for(; $b_lft < $width; ++$b_lft) {
        for($y = $b_top; $y <= $b_btm; ++$y) {
            if(imagecolorat($img, $b_lft, $y) != $hex) {
                break 2;
            }
        }
    }

    // right
    for(; $b_rt >= 0; --$b_rt) {
        for($y = $b_top; $y <= $b_btm; ++$y) {
            if(imagecolorat($img, $b_rt, $y) != $hex) {
                break 2;
            }
        }
    }

    $b_btm++;
    $b_rt++;
    return array(
        'l' => $b_lft,
        't' => $b_top,
        'r' => $b_rt,
        'b' => $b_btm,
        'w' => $b_rt - $b_lft,
        'h' => $b_btm - $b_top
    );
}
?>

