<?

    $page_id=111;

    include("config/config.php");

	$sel_page="select * from page_mgmt where page_id=1";

	$rs_page=mysql_query($sel_page);

	$page=mysql_fetch_array($rs_page);

	$pageid=1;

?>

<!DOCTYPE html>

<html lang="en">

<?php include("include/head.php"); ?>

<body>

<!-- main wrapper starts -->

<div class="wrapper">

  <!-- header starts -->

 <?php include("include/header.php"); ?>

  <!-- header ends -->

  <div class="cl"></div>

  <!-- About us banner starts -->

  

  <!-- body starts -->

  <div class="cl"></div>

  <!-- inner-content starts -->

  <div class="content-container inner-content"  style="clear:both; margin-top:25px; background:none;">

    <div class="brad" style=" display:none;"><a href="<?=$siteurl?>" class="no-active">Home</a> / <a href="<?=$siteurl?>how-it-works.php">How it works</a></div>

    <div class="cl"></div>

    <!-- Left Starts -->

     <div class="left-container" >


      <div class="left-mid" style="min-height:1550px; background:none;">
  <?php
            if($_SESSION["lang"]!="ru") {  ?>
        <div class="content_area" style="">




     
          <h2>How it works</h2>
         
         
          
          <div style="color:#000;font-family:font-family: arial, helvetica, sans-serif; font-size:14px; line-height:20px; line-height:18px; margin-top:20px; margin-bottom:35px; text-align:justify;">
          Fresh Life Floral gives its participating buyers and growers a unique advantage – a multinational sales engine that allows the two to interact quickly and efficiently.  Fast transactions, “feet on the ground” supply management and quality control in Ecuador, and a sales staff that understands from a buyer’s perspective, not just the grower’s view.
          </div>
          
          
         
          
          <div>
          
          <div style="float:left; width:300px; margin-top:35px;"> <img src="<?=$siteurl?>images/himage1.jpg" width="300" height="193" /></div>
          <div style="float:right;width:300px; margin-right:20px;">
          
          <div style="margin-top:35px;">
          
            <div style=" clear:both; font-family:Arial,Helvetica,sans-serif; color:#333; margin-bottom:20px; font-size:20px; font-weight:normal;"><span style="color:#75297e;">1. &nbsp;</span>Review Immediately Available Product </div>
            
          <div><a class=" signup cboxElement" href="http://content.freshlifefloral.com/online-access" style="color:#75297e; font-family:font-family: arial, helvetica, sans-serif;; font-weight:bold; font-size:15px;">Register is free </a></div>
          <div style="color:#000;font-family:font-family: arial, helvetica, sans-serif;; font-size:14px; line-height:20px; margin-top:20px;">
          Once registered you will be able to buy product from the growers’ live inventory, ask for quotations on product you are looking for, or create offers for the price you want to pay to multiple growers.
          </div>
          </div>
          
          </div>
          
          </div>
          
          
          <div style="clear:both;"></div>
          
          <div style="margin-top:40px;">
          
         
          
           <div style="clear:both;"></div>
          
          <div style="float:left;width:300px;">
          
          <div>
          
           <div style="font-family:Arial,Helvetica,sans-serif; color:#333; margin-bottom:20px;  font-size:20px; font-weight:normal;"><span style="color:#75297e;">2. &nbsp;</span>Review Quotations. </div>
         
          <div style="color:#000;font-family:font-family: arial, helvetica, sans-serif;; font-size:14px; line-height:20px; ">
          Review any quotations from growers and your offers to the growers and confirm both based on your requirements and preferences. 
          </div>
          
          <div style="color:#000;font-family:font-family: arial, helvetica, sans-serif;; font-size:14px; line-height:20px; margin-top:20px; ">
          We simplify the confirmation process to make it seamless, fast and effortless.
          </div>
          
          
          </div>
          
          </div>
          <div style="float:left; width:300px; margin-left:20px; "> <img src="<?=$siteurl?>images/himage2.jpg" width="300" height="193" /></div>
          </div>
          
           <div style="clear:both;"></div>
           
           <div style="margin-top:40px;">
           
          
          
           <div style="clear:both;"></div>
           
          <div style="float:left; width:300px;"> <img src="<?=$siteurl?>images/himage3.jpg" width="300" height="193" /></div>
          <div style="float:right;width:300px; margin-right:20px;">
          
           <div style="width:300px; font-family:Arial,Helvetica,sans-serif; color:#333; margin-bottom:20px;  font-size:20px; font-weight:normal;"><span style="color:#75297e;">3. &nbsp;</span>Quality Control.</div>
          
          <div style="color:#000;font-family:font-family: arial, helvetica, sans-serif;; font-size:14px; line-height:20px; margin-top:20px;">
          Fresh Life Floral employees will check your orders for freshness and quality to make sure they’re in perfect condition before they ship. 
          </div>
          
          
          <div style="color:#000;font-family:font-family: arial, helvetica, sans-serif;; font-size:14px; line-height:20px; margin-top:20px;">
          Have no fear: professional agronomical engineers make sure everything you buy is carefully checked and rated 100% awesome before it ever leaves the country of origin. 
          </div>
          
          </div>
          
          </div>
          
          
            <div style="clear:both;"></div>
          
          <div style="margin-top:40px;">
          
          
          <div style="float:left;width:300px;">
          
          <div>
          
          
          <div style="width:300px; font-family:Arial,Helvetica,sans-serif; color:#333; margin-bottom:20px; font-size:20px; font-weight:normal;"><span style="color:#75297e;">4. &nbsp;</span>Consolidation.</div>
         
          <div style="color:#000; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin-top:20px;">
         We will consolidate your orders (put everything together sensibly as we prepare them for shipment) for you so your shipment arrives to the destination(s) without undue delay. 
          </div>
         
          </div>
          
          </div>
          <div style="float:left; width:300px; margin-left:20px; "> <img src="<?=$siteurl?>images/himage4.jpg" width="300" height="193"/></div>
          </div>
          
          
          
           <div style="clear:both;"></div>
           
           
            <div style="margin-top:40px;">
            
          
             <div style="clear:both;"></div>
          
          <div style="float:left; width:300px; margin-top:20px;"> <img src="<?=$siteurl?>images/himage5.jpg" width="300" height="193"/></div>
          <div style="float:right;width:300px; margin-right:20px;">
          
            <div style="font-family:Arial,Helvetica,sans-serif; color:#333;  font-size:20px; margin-bottom:20px; margin-top:20px; font-weight:normal;"><span style="color:#75297e;">5. &nbsp;</span>Small Fee.</div>
          
          <div style="color:#000;font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin-top:20px;">
         The quote your grower submits will include a small service fee for Fresh Life Floral. After your flowers are delivered and payment is made, we duct our commission and transfer the balance to your grower. 
          </div>
          
          
          
          </div>
          
          </div>
          
          
            <div style="clear:both;"></div>
           
          <div style="height:25px;">&nbsp;</div>
          
          </div>
          
          <?php } else { ?>
          
          
            <div class="content_area" style="">




     
          <h2>Как это работает</h2>
         
         
          
          <div style="color:#000;font-family:font-family: arial, helvetica, sans-serif; font-size:14px; line-height:20px; line-height:18px; margin-top:20px; margin-bottom:35px; text-align:justify;">
         Наша система Fresh Life Floral дает преимущество покупателям и производителям уникальную возможность – многоканальный двигатель продаж, приводимый к быстрому и эффективному взаимодействию. Произведение быстрых сделок, “полная реализация действий” управление контроля и качества в Эквадоре,  за которым наблюдает наш департамент продаж и понимает процедуры с точки зрения покупателя, а не только с точки зрения производителя.
          </div>
          
          
         
          
          <div>
          
          <div style="float:left; width:300px; margin-top:35px;"> <img src="<?=$siteurl?>images/himage1.jpg" width="300" height="193" /></div>
          <div style="float:right;width:300px; margin-right:20px;">
          
          <div style="margin-top:35px;">
          
            <div style=" clear:both; font-family:Arial,Helvetica,sans-serif; color:#333; margin-bottom:20px; font-size:20px; font-weight:normal;"><span style="color:#75297e;">1. &nbsp;</span>Незамедлительный обзор выявленного продукта  </div>
            
          <div><a class="example9 signup cboxElement" href="<?=$siteurl?>sign-up.php" style="color:#75297e; font-family:font-family: arial, helvetica, sans-serif;; font-weight:bold; font-size:15px;">Свободная регистрация  </a></div>
          <div style="color:#000;font-family:font-family: arial, helvetica, sans-serif;; font-size:14px; line-height:20px; margin-top:20px;">
          Зарегистрировавшись однажды, у вас будет постоянная возможность закупать свеже доступный продукт у поставщика, ценовой запрос на продукт, который вы ищите или предложение своих расценок для ряда производителей.
          </div>
          </div>
          
          </div>
          
          </div>
          
          
          <div style="clear:both;"></div>
          
          <div style="margin-top:40px;">
          
         
          
           <div style="clear:both;"></div>
          
          <div style="float:left;width:300px;">
          
          <div>
          
           <div style="font-family:Arial,Helvetica,sans-serif; color:#333; margin-bottom:20px;  font-size:20px; font-weight:normal;"><span style="color:#75297e;">2. &nbsp;</span>Обзор котировок. </div>
         
          <div style="color:#000;font-family:font-family: arial, helvetica, sans-serif;; font-size:14px; line-height:20px; ">
         Обзор любых котировок от производителя и ваши предложения производителю, с подтверждением базирующихся на ваших пожеланиях и предпочтениях.  
          </div>
          
          <div style="color:#000;font-family:font-family: arial, helvetica, sans-serif;; font-size:14px; line-height:20px; margin-top:20px; ">
         Мы упростили процесс подтверждения, чтобы сделать его максимально удобным, быстрым и без лишних усилий.
          </div>
          
          
          </div>
          
          </div>
          <div style="float:left; width:300px; margin-left:20px; "> <img src="<?=$siteurl?>images/himage2.jpg" width="300" height="193" /></div>
          </div>
          
           <div style="clear:both;"></div>
           
           <div style="margin-top:40px;">
           
          
          
           <div style="clear:both;"></div>
           
          <div style="float:left; width:300px;"> <img src="<?=$siteurl?>images/himage3.jpg" width="300" height="193" /></div>
          <div style="float:right;width:300px; margin-right:20px;">
          
           <div style="width:300px; font-family:Arial,Helvetica,sans-serif; color:#333; margin-bottom:20px;  font-size:20px; font-weight:normal;"><span style="color:#75297e;">3. &nbsp;</span>Контроль качества.</div>
          
          <div style="color:#000;font-family:font-family: arial, helvetica, sans-serif;; font-size:14px; line-height:20px; margin-top:20px;">
          Сотрудники Fresh Life Floral проверяют ваш заказ на качество и свежесть продукта, чтобы быть уверенными в идеальных условиях перед отправкой груза.  
          </div>
          
          
          <div style="color:#000;font-family:font-family: arial, helvetica, sans-serif;; font-size:14px; line-height:20px; margin-top:20px;">
          Без сомнений: профессиональные инженеры агрикультур стараются придавать уверенности всему, что вы покупаете, подвергая процесс тщательной проверке и 100% рейтингу качества, перед отправкой из стран производителей.
          </div>
          
          </div>
          
          </div>
          
          
            <div style="clear:both;"></div>
          
          <div style="margin-top:40px;">
          
          
          <div style="float:left;width:300px;">
          
          <div>
          
          
          <div style="width:300px; font-family:Arial,Helvetica,sans-serif; color:#333; margin-bottom:20px; font-size:20px; font-weight:normal;"><span style="color:#75297e;">4. &nbsp;</span>Консолидация..</div>
         
          <div style="color:#000; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin-top:20px;">
        Мы можем отправить ваш груз консолидировано (организуя общую погрузку всего продукта) для вас так, чтобы груз прибыл до места назначения без задержек и лишних затрат.
          </div>
         
          </div>
          
          </div>
          <div style="float:left; width:300px; margin-left:20px; "> <img src="<?=$siteurl?>images/himage4.jpg" width="300" height="193"/></div>
          </div>
          
          
          
           <div style="clear:both;"></div>
           
           
            <div style="margin-top:40px;">
            
          
             <div style="clear:both;"></div>
          
          <div style="float:left; width:300px; margin-top:20px;"> <img src="<?=$siteurl?>images/himage5.jpg" width="300" height="193"/></div>
          <div style="float:right;width:300px; margin-right:20px;">
          
            <div style="font-family:Arial,Helvetica,sans-serif; color:#333;  font-size:20px; margin-bottom:20px; margin-top:20px; font-weight:normal;"><span style="color:#75297e;">5. &nbsp;</span>Минимальные затраты.</div>
          
          <div style="color:#000;font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; margin-top:20px;">
          Сервис, оказываемый нашими партнерами, будет включать небольшой сбор для Fresh Life Floral. После того как ваши цветы будут оплачены и доставлены до места назначения. Мы производим денежные переводы поставщикам, включающую нашу комиссию.
          </div>
          
          
          
          </div>
          
          </div>
          
          
            <div style="clear:both;"></div>
           
          <div style="height:25px;">&nbsp;</div>
          
          </div>
          
          
          
          <? } ?>
          
      </div>

       <div class="cl"></div>

        </div>

    <!-- Left ends -->

    <!-- Right Starts -->

    <?php include("include/right.php"); ?>

    <!-- Right Ends -->

    <div class="cl"></div>

  </div>

  <!-- inner-content ends -->

  <!-- footer starts -->

  <div class="footerwraper">

  <?php include("include/footer.php"); ?>

  </div>

  <!-- footer ends -->

   <?php include("include/copyright.php"); ?>

  <div class="cl"></div>
</div>
<?php include("include/fixeddiv.php"); ?>
</body>
</html>
