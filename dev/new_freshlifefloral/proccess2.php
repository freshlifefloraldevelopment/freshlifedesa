<?php
if (isset($_REQUEST['submit'])) {
    
	$first_name = $_POST['first_name']; // required
	$last_name = $_POST['last_name']; // required
    $company = $_POST['company']; // required
	
    $website = $_POST['web_site']; // required
	$from = $_POST['email']; // required
    $country = $_POST['country']; // required
	$skype = $_POST['skype']; // required
	$telephone = $_POST['phone']; // required
    $comments = $_POST['description']; // required
	
	
	
	//Process a new form submission in HubSpot in order to create a new Contact.
		$hubspotutk = $_COOKIE['hubspotutk']; //grab the cookie from the visitors browser.
		$ip_addr = $_SERVER['REMOTE_ADDR']; //IP address too.
		$hs_context = array(
		'hutk' => $hubspotutk,
		'ipAddress' => $ip_addr,
		'pageUrl' => 'http://freshlifefloral.com/cms/contactus',
		'pageName' => 'Request for Proposal'
		);
		$hs_context_json = json_encode($hs_context);
		//Need to populate these varilables with values from the form.
		$str_post = "firstname=" . urlencode($_POST['first_name'])
		. "&lastname=" . urlencode($_POST['last_name'])
		. "&email=" . urlencode($_POST['email'])
		. "&website=" . urlencode($_POST['web_site'])
		. "&skype=" . urlencode($_POST['skype'])
		. "&phone_number=" . urlencode($_POST['phone'])
		. "&information=" . urlencode($_POST['description'])
		. "&company_name=" . urlencode($_POST['company'])
		. "&hs_context=" . urlencode($hs_context_json); //Leave this one be :)
		//replace the values in this URL with your portal ID and your form GUID
		$endpoint = 'https://forms.hubspot.com/uploads/form/v2/295496/f13fe380-28ee-4603-a118-7a64f2f7c904';
		$ch = @curl_init();
		@curl_setopt($ch, CURLOPT_POST, true);
		@curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
		@curl_setopt($ch, CURLOPT_URL, $endpoint);
		@curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = @curl_exec($ch); //Log the response from HubSpot as needed.
		@curl_close($ch);
		
		
    if ($company=='Company Name (optional)')
	{
		$company="";
	}
	 if ($website=='Website (optional)')
	{
		$website="";
	}
    // EDIT THE 2 LINES BELOW AS REQUIRED
	$to = "info@freshlifefloral.com" ;
    $subject = "Request Call Back ";  
  //  $body = "Contact us Form.<br /><br />";
    $body .= "First Name: ".$first_name."<br />";
    $body .= "Last Name: ".$last_name."<br />";
    $body .= "Company: ".$company."<br />";
    $body .= "Website: ".$website."<br />";
    $body .= "Email: ".$from."<br />";
	$body .= "Skype: ".$skype."<br />";
    $body .= "Country: ".$country."<br />";
    $body .= "Phone Number: ".$telephone."<br />";
	$body .= "Description: ".$comments."<br />";
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	$headers .= "From: $from" . "\r\n";
	mail($to,$subject,$body,$headers);
	header("Location:requestcallback-lightbox.php?success=true");
}
else {
echo "mail send fail";
}


?>
