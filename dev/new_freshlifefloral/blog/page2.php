<?php
?>
<!DOCTYPE html>
<html lang="en">
<?php include("include/head.php"); ?>
<body>
<!-- main wrapper starts -->
<div class="wrapper">
  <!-- header starts -->
 <?php include("include/header.php"); ?>
  <!-- header ends -->
  <div class="cl"></div>
  <!-- About us banner starts -->
  <div class="aboutus-banner">
    <div class="banner-about">
      <div class="ban-image"><img src="images/abt-ban-img.jpg" width="485" height="267" alt=""> </div>
      <div class="ban-content">
        <h3>Exporting high quality Ecuadorian flowers</h3>
        <p>Ecuador is also home to some of the world’s finest and talented floral growers, creating the finest cut flowers and lush Ecuadorian plants. </p>
      </div>
    </div>
  </div>
  <!-- body starts -->
  <div class="cl"></div>
  <!-- inner-content starts -->
  <div class="content-container inner-content">
    <div class="brad"><a href="index.html" class="no-active">Home</a> / <a href="about-us.html">About us</a></div>
    <div class="cl"></div>
    <!-- Left Starts -->
    <div class="left-container">
      <div class="top"></div>
      <div class="left-mid">
        <div class="content_area">
          <h2>About Us</h2>
          <p>Known for its large and vibrant blooms, the Ecuadorian flowers are everything but the ordinary.  Located at the heart of South America, right on the equator, these beautiful flowers receive the best treatment from the most passionate growers in the world.  Aside from having perfect weather conditions and rich fertile soil, Ecuador is also home to some of the world’s finest and talented floral growers, creating the finest cut flowers and lush Ecuadorian plants.</p>
          <p> <strong>We saw the opportunity to offer our customers the best of the bests, so we did.</strong><br>
            Having the means and knowledge of exporting high quality Ecuadorian flowers, we at Fresh Life Floral specialize in distributing and selling these flowers to markets and individuals both local and international. </p>
          <p> We offer a wide range of options.  We have a beautiful array of flower varieties varying in colours and sizes.  Some of these flowers are one-of-a-kind and can only be found in our farms.  Every order is nicely wrapped, packaged, and labelled accordingly.  From our regular solid, one colored boxes to more complex, mix colored boxes, our boxes and packaging supplies are made of top quality materials, acquired from our most trusted suppliers.  These are durable and long-lasting packaging to make sure the flowers are still fresh and beautiful after shipping. </p>
          <p> <strong>Customer satisfaction is our motivation. </strong><br>
            We have perfected our working process over the years.  Processed and handled with utmost care by our team of professionals, all our products pass by an extensive quality control measures before being shipped out.   We ensure that all flowers are delivered fresh and on time.  We work hard to offer the best quality products with the best quality service as we take pride in sending products that exceed our customers’ expectations. </p>
          <p> <strong>We’re high-class but not high-priced. </strong><br>
            Here at Fresh Life Floral, we believe high quality products doesn’t necessarily come with a high price tag.  Because of our efficient business structure and good working relationship with our suppliers, we guarantee the freshest and most beautiful flowers at a reasonable price.  Fast, fresh, perfect, and reasonably priced flowers are what we offer our customers. </p>
          <p> <strong>Find us worldwide.</strong><br>
            We have been providing different types of flowers to wholesalers, supermarkets, and flower designers across the globe.  It is our goal to make Ecuadorian flowers available in every continent, sharing the vibrant colors and lush greens with everyone everywhere.  Our flowers can be found all over the world with clients in Japan, USA, Germany, Switzerland, Canada, the Netherlands, Taiwan, Korea, India, China, Singapore, Russia, New Zealand and more. </p>
          <p> <strong>We are dedicated to serve.</strong><br>
            Fresh Life Floral has a great reputation for going the extra mile just to meet our customers’ diverse floral demands.   We exert all effort to provide our buyers with their choice flowers at their chosen place and time.  Our broad selection of shipping and delivery services also makes buying and ordering easy and convenient to anyone in the world.  In addition to that, we’re constantly improving our products and services to better serve our customers. </p>
        </div>
        <div class="cl"></div>
      </div>
      <div class="bot"></div>
      <div class="cl"></div>
    </div>
    <!-- Left ends -->
    <!-- Right Starts -->
    <div class="right-container">
      <h2>Testimonials</h2>
      <div class="testi-inner">
        <p class="left-sign">Thanks for the shipment. Good work! That's why we call you <span class="right-sign">&nbsp;</span> </p>
        <div class="cl"></div>
        <div class="name_inner"> <span class="head_inner">- Terry Mackenzie, APEX FLORAL</span> <br>
          <span class="grey_inner">- Mississauga, ON, Canada </span> </div>
        <div class="cl"></div>
      </div>
      <div class="form-inner">
        <div class="form-top"></div>
        <div class="form-mid">
          <h4>Quick Contact</h4>
       
         
          <form action="mail.php" method="post" name="qform" onSubmit="return validate_qcontact()">
            <div class="input_box_inner">
              <input type="text" class="input_text_inner" value="Full Name" name="name" id="qfull_name" >
            </div>
            <div class="input_box_inner">
              <input type="text" class="input_text_inner" value="Email Address" name="email" id="qemail_id" >
            </div>
            <div class="input_box_inner">
              <input type="text" class="input_text_inner" value="Phone No." name="phone" id="qphone_no" >
            </div>
            <div class="area_box_inner">
              <textarea name="comment" class="area_text_inner" id="qdescription" cols="" rows="" >Your Brief</textarea>
            </div><input type="submit" name="submit" value="" class="submit-btn">
          </form>
          <div class="cl"></div>
        </div>
        <div class="form-bot"></div>
      </div>
    </div>
    <!-- Right Ends -->
    <div class="cl"></div>
  </div>
  <!-- inner-content ends -->
  <!-- footer starts -->
  <div class="footerwraper">
  <?php include("include/footer.php"); ?>
  </div>
  <!-- footer ends -->
   <?php include("include/copyright.php"); ?>
  <div class="cl"></div>
  <!-- body ends -->
</div>
<!-- main wrapper -->
<!-- Fixed-div starts -->
<?php include("include/fixeddiv.php"); ?>
<!-- Fixed-div ends -->
</body>
</html>
