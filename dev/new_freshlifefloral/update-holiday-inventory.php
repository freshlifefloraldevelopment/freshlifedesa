<?php
    $menuoff=1;
	$page_id=575;  
    include("config/config.php");	
	
	if($_SESSION["login"]!=1 && $_SESSION["grower"]=="" )
	{
	   header("location:".$siteurl);
	}
	
	
    $week_no=1;
	
	if(isset($_POST["dateranges"]))
	{
	  $week_no=$_POST["dateranges"];
	}
	
	        
     function exportMysqlToCsv()
     {
			$csv_terminated = "\n";
			$csv_separator = ",";
			$csv_enclosed = '"';
			$csv_escaped = "\\";
			$sql_query = "select gpb.prodcutid,gpb.id as gid,gpb.future_price".$str_to_apply." as gprice,gpb.qty,gpb.sizeid,gpb.feature,gpb.type as bv,gpb.boxname as bvname,gpb.growerid,p.id,p.name as pname,p.color_id,p.image_path,s.name as subs,g.file_path5,g.growers_name,sh.name as sizename,ff.name as featurename,b.name as boxname,bs.name as bname,bt.name as boxtype,c.name as colorname from grower_product_box_packing gpb
					  left join product p on gpb.prodcutid = p.id
					  left join subcategory s on p.subcategoryid=s.id  
					  left join colors c on p.color_id=c.id 
					  left join features ff on gpb.feature=ff.id
					  left join sizes sh on gpb.sizeid=sh.id 
					  left join boxes b on gpb.box_id=b.id
					  left join boxtype bt on b.type=bt.id
					  left join growers g on gpb.growerid=g.id
					  left join bunch_sizes bs on gpb.bunch_size_id=bs.id
					  where g.active!='deactive' and gpb.growerid='".$_SESSION["grower"]."' and gpb.type!=2  and p.name is not null order by      p.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER)";
 

			$result = mysql_query($sql_query);
			$fields_cnt = mysql_num_fields($result);
			$schema_insert = '';
	
	
	$title1="Id";
    $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title1)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;
 
   
   $title1="Product";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title1)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;
	
   $title2="Variety";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;	

   $title2="Size";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;
		
   $title2="Box";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;
		
  $title2="Stems";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;
		
 $title2="Comment";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;		
		
  $title2="Stem Price ( In USD )";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;
		
   $title2="Sunday Cases Offered";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;	
   
   $title2="Monday Cases Offered";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;	
		
  $title2="Tuesday Cases Offered";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;	
		
  $title2="Wednesday Cases Offered";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;
		
  $title2="Thursday Cases Offered";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;
		
   $title2="Friday Cases Offered";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;
		
  $title2="Saturday Cases Offered";
   $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
			stripslashes($title2)) . $csv_enclosed;
		$schema_insert .= $l;
		$schema_insert .= $csv_separator;														
  	
		
   
	$out = trim(substr($schema_insert, 0, -1));
	$out .= $csv_terminated;
	
	$out .= $csv_terminated;
 
    $i=1;	
	while ($row = mysql_fetch_array($result))
	{
		$schema_insert = '';
		
		if ($csv_enclosed == '')
				{
					$schema_insert.=$row["gid"];
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row["gid"]) . $csv_enclosed;
		}
		
		$schema_insert .= ',';
		
		if ($csv_enclosed == '')
				{
					$schema_insert.=$row["subs"];
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row["subs"]) . $csv_enclosed;
		}
		
		
		$schema_insert .= ',';
		
		
		if ($csv_enclosed == '')
				{
					$schema_insert.=$row["pname"]." ".$row["featurename"];
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row["pname"]." ".$row["featurename"]) . $csv_enclosed;
		}
		
		$schema_insert .= ',';
		
		if ($csv_enclosed == '')
				{
					$schema_insert.=$row["pname"]." ".$row["featurename"];
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row["sizename"]."cm") . $csv_enclosed;
		}
		
		
		$schema_insert .= ',';
		
		if ($csv_enclosed == '')
				{
					$schema_insert.=$row["boxtype"];
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,$row["boxtype"]) . $csv_enclosed;
		}
		
		
		$schema_insert .= ',';
		
		if ($csv_enclosed == '')
				{
					$schema_insert.=$row["bname"]*$row["qty"];
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,$row["bname"]*$row["qty"]) . $csv_enclosed;
		}
		
		$schema_insert .= ',';
		
		
		$price_list="";
		if ($csv_enclosed == '')
				{
					$schema_insert.=$price_list;
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,$price_list) . $csv_enclosed;
		}
		
		
		$schema_insert .= ',';
	
		$price_list="0.00";
		if ($csv_enclosed == '')
				{
					$schema_insert.=$price_list;
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,$price_list) . $csv_enclosed;
		}
		
		$schema_insert .= ',';
		
		$stocklist_list="0";
		if ($csv_enclosed == '')
				{
					$schema_insert.=$stocklist_list;
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,$stocklist_list) . $csv_enclosed;
		}
		
		$schema_insert .= ',';
		
		$stocklist_list="0";
		if ($csv_enclosed == '')
				{
					$schema_insert.=$stocklist_list;
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,$stocklist_list) . $csv_enclosed;
		}
		
		$schema_insert .= ',';
		
		$stocklist_list="0";
		if ($csv_enclosed == '')
				{
					$schema_insert.=$stocklist_list;
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,$stocklist_list) . $csv_enclosed;
		}
		
		$schema_insert .= ',';
		
		$stocklist_list="0";
		if ($csv_enclosed == '')
				{
					$schema_insert.=$stocklist_list;
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,$stocklist_list) . $csv_enclosed;
		}
		
		
		$schema_insert .= ',';
		
		$stocklist_list="0";
		if ($csv_enclosed == '')
				{
					$schema_insert.=$stocklist_list;
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,$stocklist_list) . $csv_enclosed;
		}
		
		$schema_insert .= ',';
		
		$stocklist_list="0";
		if ($csv_enclosed == '')
				{
					$schema_insert.=$stocklist_list;
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,$stocklist_list) . $csv_enclosed;
		}
		
		$schema_insert .= ',';
		
		$stocklist_list="0";
		if ($csv_enclosed == '')
				{
					$schema_insert.=$stocklist_list;
				}
		else
		{
		$schema_insert .= $csv_enclosed .
		str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,$stocklist_list) . $csv_enclosed;
		}
		
		
		$out .= $schema_insert;
		$out .= $csv_terminated;
	} // end while
 
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Length: " . strlen($out));
	// Output to browser with appropriate mime type, you choose ;)
	header("Content-type: text/x-csv");
	//header("Content-type: text/csv");
	//header("Content-type: application/csv");
	header("Content-Disposition: attachment; filename=holidaystock_".date("m-d-y").".csv");
	echo $out;
	exit;
 
}

		  if(isset($_POST["submitu"]))
		  {
			  exportMysqlToCsv();
		  }
  
		  
		$destinationid=",";
		$sdestinationid=",";
		$sdestinationid2=",";
		$sdestinationid3=",";
		$sdestinationid4=",";
		$sdestinationid5=",";
		$sdestinationid6=",";
	
	if(isset($_REQUEST["sfilter"]))
	{
	   $strdelete = $_POST["categorycombo"];	   
	   $strdelete1 = $_POST["subcategorycombo"];
	   $strdelete2 = $_POST["sizecombo"];
	   $strdelete3 = $_POST["specialcombo"];;
	   $strdelete4 = $_POST["productcombo"];
	   $strdelete5 = $_POST["countrycombo"];
	   $strdelete6 = $_POST["growercombo"];
	}
	
	if($_POST["categories"]!="")
	{
	   $strdelete = $_POST["categories"];
	} 
	if($_POST["subcategories"]!="")
	{
	   $strdelete1 = $_POST["subcategories"];
	}
	if($_POST["sizes"]!="")
	{
	   $strdelete2 = $_POST["sizes"];
	}  
	if($_POST["features"]!="")
	{
	   $strdelete3 = $_POST["features"];
	} 
	if($_POST["products"]!="")
	{
	   $strdelete4 = $_POST["products"];
	} 
	if($_POST["countries"]!="")
	{
	   $strdelete5 = $_POST["countries"];
	}
	if($_POST["growers"]!="")
	{
	   $strdelete6 = $_POST["growers"];
	}
?>
<!DOCTYPE html>
<html lang="en">
<?php include("include/head.php"); ?>
 <style>
 .inner-content
 {
    margin-top:30px !important;
 }
 .brad
 {    
	margin-bottom:0px;
	text-align:right;
	margin-right:269px;
  
 }
 .left-container .left-mid
 {
   margin-top:-25px;
 }
 </style>
<body>
<div class="wrapper">
 <?php include("include/header.php"); 
            $temp_starting_date=explode("-",$holiday["s_startdate1"]);
			$orginal_starting_date=$temp_starting_date[1]."-".$temp_starting_date[2]."-".$temp_starting_date[0];
			
			$temp_end_date=explode("-",$holiday["s_enddate1"]);
			$orginal_end_date=$temp_end_date[1]."-".$temp_end_date[2]."-".$temp_end_date[0];
			
			$temp_starting_date=explode("-",$holiday["s_startdate2"]);
			$orginal_starting_date1=$temp_starting_date[1]."-".$temp_starting_date[2]."-".$temp_starting_date[0];
			
			$temp_end_date=explode("-",$holiday["s_enddate2"]);
			$orginal_end_date1=$temp_end_date[1]."-".$temp_end_date[2]."-".$temp_end_date[0];
			
			
			$temp_starting_date=explode("-",$holiday["s_startdate3"]);
			$orginal_starting_date2=$temp_starting_date[1]."-".$temp_starting_date[2]."-".$temp_starting_date[0];
			
			$temp_end_date=explode("-",$holiday["s_enddate3"]);
			$orginal_end_date2=$temp_end_date[1]."-".$temp_end_date[2]."-".$temp_end_date[0];
			
 
 ?>
  <div class="cl"></div>
  <div class="cl"></div>
  <div class="content-container inner-content">
       <div class="left-container" style="width:990px;">
      <div class="left-mid" style="width:990px; background:none; ">
        <div class="content_area" style="width:990px;">
        <form id="filterdates" name="filterdates" style=" float:left; margin-left:5px;" action="" method="post">
          <h2 style=" width:980px; padding-bottom:8px;"> Holiday Inventory for <?=$holiday["name"]?> 
          <select name="dateranges" id="dateranges" onChange="dodateschange();" style="font-size:25px; color:#a4ba3a;" >
          <option value="1"  <? if($week_no==1) { echo "selected"; } ?> > from <?=$orginal_starting_date?> to <?=$orginal_end_date?></option>
           <option value="2" <? if($week_no==2) { echo "selected"; } ?> > from <?=$orginal_starting_date1?> to <?=$orginal_end_date1?></option>
           <option value="3" <? if($week_no==3) { echo "selected"; } ?>> from <?=$orginal_starting_date2?> to <?=$orginal_end_date2?></option>
          </select>
          </h2>
          </form> 
          <?
             if($_GET["msg"]==1)
			 {
		 ?>
          <div style="margin:15px; text-align:center; clear:both; background:#060; padding:15px; color:#fff; font-size:14px; font-weight:bold;"> Your holiday inventory has been uploaded successfully . </div>
          <? } ?>
           <?
			  $sel_products = "select gpb.id from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join growers g on gpb.growerid=g.id
			  right join holiday_inventory hi on gpb.id=hi.pack_id
			  where g.active!='deactive' and gpb.growerid='".$_SESSION["grower"]."'  and p.name is not null";	

			  if($strdelete!="" )
			  {
			     $sel_products.=" and  p.categoryid IN (".$strdelete.")";
			  } 
			  
			   if($strdelete1!="" )
			  {
			     $sel_products.=" and  p.subcategoryid IN (".$strdelete1.")";
			  } 
			  
			  if($strdelete2!="" )
			  {
			     $sel_products.=" and  gpb.sizeid IN (".$strdelete2.")";
			  }
			  
			  if($strdelete3!="" )
			  {
			     $sel_products.=" and  gpb.feature IN (".$strdelete3.")";
			  } 
			  
			  if($strdelete4!="" )
			  {
			     $sel_products.=" and  gpb.prodcutid IN (".$strdelete4.")";
			  } 
			  
			  if($strdelete5!="" )
			  {
			     $sel_products.=" and  g.country_id IN (".$strdelete5.")";
			  } 
			  
			  if($strdelete6!="" )
			  {
			     $sel_products.=" and  gpb.growerid IN (".$strdelete6.")";
			  }	 
			 
			  if($week_no!="")
			  {
				  $sel_products.=" and hi.weekno=".$week_no;
			  } 	  
              
		      $rs_prodcuts=mysql_query($sel_products);						
			  $total=mysql_num_rows($rs_prodcuts);

							 $num_record = $total;

							  $display=50; 

							  $XX = '<div class="notfound">No Item Found !</div>';

							  if(isset($_POST["startrow"]) && $_POST["startrow"]!="")

							  {

									$sr=$_POST["startrow"]+1;

							$query2 = "select gpb.prodcutid,gpb.id as gid,gpb.future_price".$str_to_apply." as gprice,gpb.qty,gpb.sizeid,gpb.feature,gpb.type as bv,gpb.boxname as bvname,gpb.growerid,p.id,p.name,p.color_id,p.image_path,s.name as subs,g.file_path5,g.growers_name,sh.name as sizename,ff.name as featurename,b.name as boxname,bs.name as bname,bt.name as boxtype,c.name as colorname,hi.day_price,hi.day_price,hi.stock_date,hi.day_stock from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join subcategory s on p.subcategoryid=s.id  
	          left join colors c on p.color_id=c.id 
			  left join features ff on gpb.feature=ff.id
			  left join sizes sh on gpb.sizeid=sh.id 
			  left join boxes b on gpb.box_id=b.id
			  left join boxtype bt on b.type=bt.id
			  left join growers g on gpb.growerid=g.id
			  left join bunch_sizes bs on gpb.bunch_size_id=bs.id
			  right join holiday_inventory hi on gpb.id=hi.pack_id
			  where g.active!='deactive' and gpb.growerid='".$_SESSION["grower"]."'  and gpb.type!=2  and p.name is not null ";	
			  
			  
			  
			  if($week_no!="")
			  {
				  $query2.=" and hi.weekno=".$week_no;
			  }
			  
			  
			  if($strdelete!="" )
			  {
			     $query2.=" and  p.categoryid IN (".$strdelete.")";
			  } 
			  
			   if($strdelete1!="" )
			  {
			     $query2.=" and  p.subcategoryid IN (".$strdelete1.")";
			  } 									
									
			
			  if($strdelete2!="" )
			  {
			     $query2.=" and  gpb.sizeid IN (".$strdelete2.")";
			  } 
			  
			  if($strdelete3!="" )
			  {
			     $query2.=" and  gpb.feature IN (".$strdelete3.")";
			  } 
			  
			  if($strdelete4!="" )
			  {
			     $query2.=" and  gpb.prodcutid IN (".$strdelete4.")";
			  }	
			  
			  
			  if($strdelete5!="" )
			  {
			     $query2.=" and  g.country_id IN (".$strdelete5.")";
			  } 
			  
			  
			  if($strdelete6!="" )
			  {
			     $query2.=" and gpb.growerid IN (".$strdelete6.")";
			  } 							 
							

									$query2.=" "; 

									$query2.= " order by p.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER),hi.stock_date LIMIT ".$_POST["startrow"].",$display"; 		

									$result2 = mysql_query($query2);

							  }

							  else
							  {
									if (empty($startrow)) 
									{ 
    									$startrow=0; 
										$sr=1;
									}

									$query2 = "select gpb.prodcutid,gpb.id as gid,gpb.future_price".$str_to_apply." as gprice,gpb.qty,gpb.sizeid,gpb.feature,gpb.type as bv,gpb.boxname as bvname,gpb.growerid,p.id,p.name,p.color_id,p.image_path,s.name as subs,g.file_path5,g.growers_name,sh.name as sizename,ff.name as featurename,b.name as boxname,bs.name as bname,bt.name as boxtype,c.name as colorname,hi.day_price,hi.day_price,hi.stock_date,hi.day_stock from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join subcategory s on p.subcategoryid=s.id  
	          left join colors c on p.color_id=c.id 
			  left join features ff on gpb.feature=ff.id
			  left join sizes sh on gpb.sizeid=sh.id 
			  left join boxes b on gpb.box_id=b.id
			  left join boxtype bt on b.type=bt.id
			  left join growers g on gpb.growerid=g.id
			  left join bunch_sizes bs on gpb.bunch_size_id=bs.id
			  right join holiday_inventory hi on gpb.id=hi.pack_id
			  where g.active!='deactive' and gpb.growerid='".$_SESSION["grower"]."' and gpb.type!=2  and p.name is not null ";	
			  
			 
			  
			  if($week_no!="")
			  {
				  $query2.=" and hi.weekno=".$week_no;
			  }
			  
			  
			  if($strdelete!="" )
			  {
			     $query2.=" and  p.categoryid IN (".$strdelete.")";
			  } 						
			  
			  
			  if($strdelete1!="" )
			  {
			     $query2.=" and  p.subcategoryid IN (".$strdelete1.")";
			  }   
			  
			  
			  if($strdelete2!="" )
			  {
			     $query2.=" and  gpb.sizeid IN (".$strdelete2.")";
			  } 								              
				
				
			  if($strdelete3!="" )
			  {
			     $query2.=" and  gpb.feature IN (".$strdelete3.")";
			  } 
			  
			  if($strdelete4!="" )
			  {
			     $query2.=" and  gpb.prodcutid IN (".$strdelete4.")";
			  }	
			  
			  
			  if($strdelete5!="" )
			  {
			     $query2.=" and  g.country_id IN (".$strdelete5.")";
			  } 
			  		
					
			  if($strdelete6!="" )
			  {
			     $query2.=" and gpb.growerid IN (".$strdelete6.")";
			  }				
                                        $query2.=" ";
									    $query2.= " order by p.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER),hi.stock_date LIMIT 0,$display"; 		
										$result2 = mysql_query($query2);
							  }
							  
			?>				  
			 
           <div style="clear:both"></div>
         
           <div style=" margin-bottom:10px; float:left; width:650px;">
         <form name="frmfilter" method="post" action="http://freshlifefloral.com/vendor/update-holiday-inventory">
         <?
		     if(isset($_POST["dateranges"]) && $_POST["dateranges"]!="" )
			 {
		?>
        
        <input type="hidden" name="dateranges" id="dateranges" value="<?=$_POST["dateranges"]?>" />
        
         <?		 
			 }
        ?>
         <div style="float:left; margin-left:10px; width:170px;">
         <h2 style="border:none; font-size:15px; font-weight:bold; color:#666; margin:0; padding:0">Products</h2>
         <select name="subcategorycombo" id="subcategorycombo" onChange="frmsubmite()" style="width:170px; border:1px solid #999; font-family:Verdana, Arial, Helvetica, sans-serif;">
         <option value="">All</option>
       <?php  
	          $sel_testi="select gpb.id,s.name as sname,s.id as sid from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join subcategory s on p.subcategoryid=s.id
			  left join growers g on gpb.growerid=g.id
			  right join holiday_inventory hi on gpb.id=hi.pack_id
			  where g.active!='deactive' and gpb.growerid='".$_SESSION["grower"]."'  and s.id > 0 ";
			  
			  
			 
			  
			  if($week_no!="")
			  {
				  $sel_testi.=" and hi.weekno=".$week_no;
			  }
			  
			  
			  if($strdelete!="" )
			  {
			     $sel_testi.=" and  p.categoryid IN (".$strdelete.")";
			  } 
			  
			  
			  if($strdelete2!="" )
			  {
			     $sel_testi.=" and  gpb.sizeid IN (".$strdelete2.")";
			  } 								              
				
				
			  if($strdelete3!="" )
			  {
			     $sel_testi.=" and  gpb.feature IN (".$strdelete3.")";
			  } 
			  
			  if($strdelete4!="" )
			  {
			     $sel_testi.=" and  gpb.prodcutid IN (".$strdelete4.")";
			  }	
			  
			  if($strdelete5!="" )
			  {
			     $sel_testi.=" and  g.country_id IN (".$strdelete5.")";
			  }	
			  
			  if($strdelete6!="" )
			  {
			     $sel_testi.=" and  g.id IN (".$strdelete6.")";
			  }	
			  
			   $sel_testi.=" group by p.subcategoryid order by p.categoryid,s.name ";
			  $rs_testi=mysql_query($sel_testi);
			  $temp=explode(",",$strdelete1); 	   
			  $j=1;
			  while($testi=mysql_fetch_array($rs_testi))
			  {
	    ?>	
       <option value="<?=$testi["sid"]?>" <? if($testi["sid"]==$strdelete1) { echo "selected"; }?> ><?=$testi["sname"]?></option>
      <?
	       $j++;
	      }
	  ?>
      </select>
     </div>
         <div style="float:left; margin-left:10px; width:170px;">
         <h2 style="border:none; font-size:15px; font-weight:bold; color:#666; margin:0; padding:0">Variety</h2>
         <select name="productcombo" id="productcombo" onChange="frmsubmite()" style="width:170px; border:1px solid #999; font-family:Verdana, Arial, Helvetica, sans-serif;">
         <option value="">All</option>
         
       <?php  
	          $sel_testi="select gpb.id,p.name as pname,p.id as pid from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join growers g on gpb.growerid=g.id
			  right join holiday_inventory hi on gpb.id=hi.pack_id
			  where g.active!='deactive' and gpb.growerid='".$_SESSION["grower"]."' and p.name is not NULL ";
			  
			  if($week_no!="")
			  {
				  $sel_testi.=" and hi.weekno=".$week_no;
			  }
			  
			  if($strdelete!="" )
			  {
			     $sel_testi.=" and  p.categoryid IN (".$strdelete.")";
			  } 
			  
			   if($strdelete1!="" )
			  {
			     $sel_testi.=" and  p.subcategoryid IN (".$strdelete1.")";
			  }   
			  
			  
			  if($strdelete2!="" )
			  {
			     $sel_testi.=" and  gpb.sizeid IN (".$strdelete2.")";
			  } 								              
				
				
			  if($strdelete3!="" )
			  {
			     $sel_testi.=" and  gpb.feature IN (".$strdelete3.")";
			  } 
			  
			  if($strdelete5!="" )
			  {
			     $sel_testi.=" and  g.country_id IN (".$strdelete5.")";
			  }	
			  
			  if($strdelete6!="" )
			  {
			     $sel_testi.=" and  g.id IN (".$strdelete6.")";
			  }	
			  
			  $sel_testi.=" group by gpb.prodcutid order by p.name";
			  $rs_testi=mysql_query($sel_testi);
			  $temp=explode(",",$strdelete4); 	   
			  $m=1;
			  while($testi=mysql_fetch_array($rs_testi))
			  {
	    ?>	
                 <option value="<?=$testi["pid"]?>" <? if($testi["pid"]==$strdelete4) { echo "selected"; }?> ><?=$testi["pname"]?></option>
             <?
				   $j++;
				  }
	         ?>
      </select>
     </div>
          <div style="float:left; margin-left:10px; width:150px;">
         <h2 style="border:none; font-size:15px; font-weight:bold; color:#666; margin:0; padding:0">Size</h2>
         <select name="sizecombo" id="sizecombo" onChange="frmsubmite()" style="width:150px; border:1px solid #999; font-family:Verdana, Arial, Helvetica, sans-serif;">
         <option value="">All</option>
       <?php  $sel_testi="select gpb.id,s.name as sname,s.id as sid from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join sizes s on gpb.sizeid=s.id
			  left join growers g on gpb.growerid=g.id
			   right join holiday_inventory hi on gpb.id=hi.pack_id
			  where g.active='active' and gpb.growerid='".$_SESSION["grower"]."' and s.name is not NULL ";
			  
			 if($week_no!="")
			  {
				  $sel_testi.=" and hi.weekno=".$week_no;
			  }
			  
			  if($strdelete!="" )
			  {
			     $sel_testi.=" and  p.categoryid IN (".$strdelete.")";
			  } 
			   if($strdelete1!="" )
			  {
			     $sel_testi.=" and  p.subcategoryid IN (".$strdelete1.")";
			  }   
			  if($strdelete3!="" )
			  {
			     $sel_testi.=" and  gpb.feature IN (".$strdelete3.")";
			  } 
			  if($strdelete4!="" )
			  {
			     $sel_testi.=" and  gpb.prodcutid IN (".$strdelete4.")";
			  }	
			  
			  
			   $sel_testi.=" group by gpb.sizeid order by CONVERT(SUBSTRING(s.name,1), SIGNED INTEGER)";
			   $rs_testi=mysql_query($sel_testi);
			
			
			  $temp=explode(",",$strdelete2); 	   
			  $k=1;
			  while($testi=mysql_fetch_array($rs_testi))
			  {
	          ?>		  
      
       <option value="<?=$testi["sid"]?>" <? if($testi["sid"]==$strdelete2) { echo "selected"; }?> ><?=$testi["sname"]?>cm</option>
       
      <?
	       $j++;
	      }
	  ?>
      
      </select>
     </div>
     
     
      <?php  $sel_testi="select gpb.id,s.name as sname,s.id as sid from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join features s on gpb.feature=s.id
			  left join growers g on gpb.growerid=g.id
			  right join holiday_inventory hi on gpb.id=hi.pack_id
			  where g.active='active' and gpb.growerid='".$_SESSION["grower"]."' and s.name is not NULL ";
			  
			 if($week_no!="")
			  {
				  $sel_testi.=" and hi.weekno=".$week_no;
			  }
			 
			  if($strdelete!="" )
			  {
			     $sel_testi.=" and  p.categoryid IN (".$strdelete.")";
			  } 
			  
			   if($strdelete1!="" )
			  {
			     $sel_testi.=" and  p.subcategoryid IN (".$strdelete1.")";
			  }   
			  
			  if($strdelete2!="" )
			  {
			     $sel_testi.=" and  gpb.sizeid IN (".$strdelete2.")";
			  } 								              
			 
			  if($strdelete4!="" )
			  {
			     $sel_testi.=" and  gpb.prodcutid IN (".$strdelete4.")";
			  }
			  
			   $sel_testi.=" group by gpb.feature order by s.name";
			   $rs_testi=mysql_query($sel_testi);
			   $total_testi=mysql_num_rows($rs_testi);
				if($total_testi>=1)
				{	   
				  ?>			   
                       <div style="float:left; margin-left:10px; width:160px;">
                      <h2 style="border:none; font-size:15px; font-weight:bold; color:#666; margin:0; padding:0">Special Feature</h2>
                         <select name="specialcombo" id="specialcombo" onChange="frmsubmite()" style="width:160px; border:1px solid #999; font-family:Verdana, Arial, Helvetica, sans-serif;">
                         
                         <option value="">All</option>
                        <?   while($testi=mysql_fetch_array($rs_testi))
                              {
                        ?>	
                      
                       <option value="<?=$testi["sid"]?>" <? if($testi["sid"]==$strdelete3) { echo "selected"; }?> ><?=$testi["sname"]?></option>
                      
                      <?
                               $j++;
                          }
                      ?>
                      
                      </select>
                 </div>
       <? } ?>    
       
     
            <div style="float:left;width:113px; margin-top:25px; margin-left:15px;">
         
         <a href="http://freshlifefloral.com/buyer/grower-availability" style="font-family:Verdana, Geneva, sans-serif; color:rgb(139, 43, 133); font-size:16px; font-weight:bold;"><img src="<?=$siteurl?>images/clear.png" border="0" ></a>
     </div>
         <input type="hidden" name="sfilter" value="sfilter" />
          <br/>
         </form>
         </div> 
           <div style="float:left; width:200px;">
           <form name="frmupload" id="frmupload" method="post" action="http://freshlifefloral.com/vendor/update-holiday-inventory"> 
           <input type="submit" id="submitu" name="submitu" value="" style="background: url('http://www.freshlifefloral.com/images/upload-template.png') no-repeat scroll 0 0 transparent;
    border: 0 none;
    cursor: pointer;
    float: none;
    height: 36px;
    width: 207px;">
           </form>
           <br/>
           <a href="http://freshlifefloral.com/vendor/upload-holiday-inventory">
           <img src="http://www.freshlifefloral.com/images/inventory.png" />
          </a>
           </div>        
          <div style="clear:both"></div>
          
          <? if($total>=1)
			  {
		  ?>
          
          <div class="fright" style="margin-top:25px; background:#f0f0f0; width:100; text-align:center; padding-top:10px; padding-bottom:10px;">
						<?php
							
						    if ($_POST["startrow"] != 0) 
							{ 
								 $prevrow = $_POST["startrow"] - $display; 
								 print("<span class='linkwhite'> <a href=\"javascript:onclick=funPage($prevrow)\" class='link-sample'>Previous </a> </span> <span class='linkwhite'>  ...</span>");  
							} 

							   $pages = intval($num_record / $display);
								if ($num_record % $display) 
								{ 	
									$pages++; 
								} 

								$numofpages = $pages;
							    $cur_page=$_POST["startrow"]/$display;
								$range=5; 
								$range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2; 
								$range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min; 
								$page_min = $cur_page- $range_min; 
								$page_max = $cur_page+ $range_max; 
								$page_min = ($page_min < 1) ? 1 : $page_min; 
								$page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max; 
								if ($page_max > $numofpages) 
								{ 
									$page_min = ($page_min > 1) ? $numofpages - $range + 1 : 1; 
									$page_max = $numofpages;
								} 
							if ($pages > 1) 
							{ 	
								print("&nbsp;");
								for ($i=$page_min; $i <=$page_max; $i++) 
								{ 
									if($cur_page+1==$i)
									{
									   $nextrow = $display * ($i - 1); 
									   print("<span class='link-page'> <span class='linkwhite'>  </span><b>$i</b> <span class='linkwhite'> </span> ");  		                                    }

									else
									{

									  $nextrow = $display * ($i - 1); 
									  print("<span class='linkwhite'></span> <a href=\"javascript:onclick=funPage($nextrow)\"  class='link-sample'> $i </a><span class='linkwhite'>  </span>");      
									}

								} 	

								print("&nbsp;");

							} 

							if ($pages > 1) 
							{ 

								if (!(($_POST["startrow"] / $display) == $pages-1) && $pages != 1 )
								{ 

									$nextrow = $_POST["startrow"] + $display; 

									print("<span class='linkwhite'>...  </span> <a href=\"javascript:onclick=funPage($nextrow)\" class='link-sample'> Next</a><span class='linkwhite'>  </span> "); 

								} 

							}

							

							if ($num_record < 1)

							 { 

								print("<span class='text'>".$XX."</span>"); 

							 } 

							?>

						</div>

              <div style="clear:both; margin-bottom:20px;"></div>          
               
       <form name="frmrequest" id="frmrequest" method="post" action="http://freshlifefloral.com/vendor/update-holiday-inventory">  
       <div style="margin:10px; margin-right:0px; text-align:right">
     
    </div>
   
    <input type="hidden" name="startrow" value="<?=$_POST["startrow"];?>" >
          <input type="hidden" name="total" id="total" value="<?=$total?>">  
          <input type="hidden" name="categories" value="<?=$strdelete;?>" >
                        <input type="hidden" name="subcategories" value="<?=$strdelete1;?>" >
                        <input type="hidden" name="sizes" value="<?=$strdelete2;?>" >
                         <input type="hidden" name="features" value="<?=$strdelete3;?>" >
                          <input type="hidden" name="products" value="<?=$strdelete4;?>" >
                          <input type="hidden" name="countries" value="<?=$strdelete5;?>" >
                          <input type="hidden" name="growers" value="<?=$strdelete6;?>" >
                          <input type="hidden" name="dateranges" value="<?=$week_no;?>" >
                          
        
        
          <div>
           <div style="float:left; width:99px; margin-left:15px;"><b>Product</b></div>
            <div style="float:left; width:130px; margin-left:19px;"><b>Variety</b></div>
             <div style="float:left; width:55px; margin-left:15px;"><b>Size</b></div>
              <div style="float:left; width:60px; margin-left:15px;"><b>Color</b></div>
              <div style="float:left; width:145px; margin-left:44px;"><b>Box</b></div>
              <div style="float:left; width:50px; margin-left:15px;"><b>Stems</b></div>
               <div style="float:left; width:73px; margin-left:10px;"><b>Box Type</b></div>
                <div style="float:left; width:50px; margin-left:10px;"><b>Price</b></div>
                <div style="float:left; width:50px; margin-left:10px;"><b>Stock</b></div>
                <div style="float:left; width:50px; margin-left:10px;"><b>Date</b></div>
             
           </div>           
            <div style="clear:both; margin-bottom:10px;"></div> 
           <div id="gallery" >
            <ul style="border:1px solid #f0f0f0; width:990px;">
              <?php 
                $i=1;
			    while($producs=mysql_fetch_array($result2))
			    {
			  ?>

             <li style="float:none; overflow:hidden;  clear:both; height:auto; width:989px; <? if($i%2!="0") { ?> background:#f3f3f3; <? } ?> margin:0px; padding:0px; margin-bottom:5px;"> 
            
             <div style="float:left; width:440px; margin-top:7px;">
             <? if($producs["bv"]!=2) { ?>
             
             <div style="float:left; width:435px;">
                    <? if($producs["bv"]!=2) { ?>
             <div style=" padding-left:10px;width:435px;text-align:left; float:left; ">
			  <div style="float:left; width:100px;"> <?=$producs["subs"]?>  <input type="hidden" name="subcategoryname-<?=$producs["gid"]?>" value="<?=$producs["subs"]?>" > </div>
              <div style="float:left; width:167px;"><a  href="javascript:void(0);" style="font-family:verdana;color:#000;font-size:12px; font-weight:normal;" ><div style="float:left; width:15px; margin-right:7px; margin-top:5px;"></div> <?=$producs["name"]?>
             
               <?=$producs["featurename"]?></a> </div> 
			  <div style="float:left; width:50px;"> <?=$producs["sizename"]?>cm </div> 
              <div style="float:left; width:75px; margin-left:15px;"> <?=$producs["colorname"]?>  </div> 
            </div>
            <? } else { 
			$sel_products_box="select * from grower_box_products where box_id ='".$producs["gid"]."'";
		    $rs_product_box=mysql_query($sel_products_box);
			$total_product_box=mysql_num_rows($rs_product_box);
			$sel_products_box_price="SELECT SUM(price*bunchqty),SUM(bunchsize*bunchqty) FROM grower_box_products where box_id ='".$producs["gid"]."'";
			$rs_products_box_price=mysql_query($sel_products_box_price);
			$products_box_price=mysql_fetch_array($rs_products_box_price);
			$final_multi_price_qty=$products_box_price['SUM(bunchsize*bunchqty)'];
			$final_multi_price=($products_box_price['SUM(price*bunchqty)'] /$final_multi_price_qty);
			 } ?>
                  </div>
            <?php } ?>
            </div>
            <div style=" padding-left:10px;width:155px;text-align:left; float:left;font-family:arial;font-weight:normal;color:#000;font-size:11px; font-weight:bold; height:13px; overflow:hidden;"><?=$producs["boxname"]?></div> <div style=" padding-left:10px;width:50px;text-align:left; float:left;font-family:arial;font-weight:normal;color:#000;font-size:11px; font-weight:bold;"><?=$producs["bname"]*$producs["qty"]?> </div>
            <div style=" padding-left:10px;width:79px;text-align:left; float:left;font-family:arial;font-weight:normal;color:#000;font-size:11px; font-weight:bold;"><?=$producs["boxtype"] ?></div>
            <div style="float:left; width:50px; margin-right:20px; font-family:verdana;color:#000;font-size:12px; font-weight:normal;"> $
			  <?=$producs["day_price"]?> </div>
            <div style="float:left; width:50px;font-family:verdana;font-weight:normal;color:#000;font-size:12px; font-weight:bold;">
              <?=$producs["day_stock"]?>
             <input type="hidden" name="pro-<?=$i?>" value="<?=$producs["gid"]?>" />
             <input type="hidden" name="grower-<?=$producs["gid"]?>" value="<?=$producs["growerid"]?>" >
            </div>
            
            <div style="float:left; width:50px;font-family:verdana;font-weight:normal;color:#000;font-size:12px; font-weight:bold;">
            <?php 
			$temp_date=explode("-",$producs["stock_date"]);
			$sdate=$temp_date[1]."-".$temp_date[2]."-".$temp_date[0];
			?>
            <?=$sdate?>
            </div>
            <br><div id="ermsg-<?=$producs["gid"]?>" style="font-size:12px; font-weight:bold;  color:red; float:left; margin-left:521px;"></div>
             </li>
             <?
                  $i++;
			     }
			  ?>
            </ul>
          </div >
         <input type="hidden" name="totalrow" value="<?=$i?>" />
          <div style="clear:both"></div>
           <div style="margin:10px; margin-right:0px; text-align:right">
    </div>
      </form>
<div style="clear:both"></div>
          <div class="fright" style="margin-top:25px; background:#f0f0f0; width:100; text-align:center; padding-top:10px; padding-bottom:10px;">

						<?

							if ($_POST["startrow"] != 0) 
							{ 
								 $prevrow = $_POST["startrow"] - $display; 
								 print("<span class='linkwhite'><a href=\"javascript:onclick=funPage($prevrow)\" class='link-sample'>Previous </a>  </span> <span class='linkwhite'>  ...</span>");  

							} 

							   $pages = intval($num_record / $display);
							   
							if ($num_record % $display) 
							{ 	
								$pages++; 
							}
							
								$numofpages = $pages;
							    $cur_page=$_POST["startrow"]/$display;
								$range =5; 
								$range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2; 
								$range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min; 
								$page_min = $cur_page- $range_min; 
								$page_max = $cur_page+ $range_max; 
								$page_min = ($page_min < 1) ? 1 : $page_min; 
								$page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max; 
								if ($page_max > $numofpages) 

								{ 

									$page_min = ($page_min > 1) ? $numofpages - $range + 1 : 1; 

									$page_max = $numofpages;

								} 

							if ($pages > 1) 

							{ 	

								print("&nbsp;");

								for ($i=$page_min; $i <=$page_max; $i++) 
								{ 

									if($cur_page+1==$i)
									{

									   $nextrow = $display * ($i - 1); 

									   print("<span class='link-page'> <span class='linkwhite'>  </span><b>$i</b> <span class='linkwhite'> </span> ");  		                                    }

									else

									{

									  $nextrow = $display * ($i - 1); 

									print("<span class='linkwhite'></span> <a href=\"javascript:onclick=funPage($nextrow)\"  class='link-sample'> $i </a><span class='linkwhite'>  </span>");   		}

								} 	

								print("&nbsp;");

							} 

							if ($pages > 1) 
							{ 
								if (!(($_POST["startrow"] / $display) == $pages-1) && $pages != 1 )
								{ 
									$nextrow = $_POST["startrow"] + $display; 
									print("<span class='linkwhite'>...   <a href=\"javascript:onclick=funPage($nextrow)\" class='link-sample'> Next</a> </span> <span class='linkwhite'>  </span> "); 

								} 

							}

							 if ($num_record < 1)
							 { 
								print("<span class='text'>".$XX."</span>"); 
							 } 

							?>
						</div>
          <?php
		     }
			  else
			  {
			     echo $XX ;
			  }
		  ?>
						<form  method="post" name="frmfprd" action="http://freshlifefloral.com/vendor/update-holiday-inventory">
						<input type="hidden" name="startrow" value="<?= $_POST["startrow"];?>" >
                        <input type="hidden" name="categories" value="<?=$strdelete;?>" >
                        <input type="hidden" name="subcategories" value="<?=$strdelete1;?>" >
                        <input type="hidden" name="sizes" value="<?=$strdelete2;?>" >
                         <input type="hidden" name="features" value="<?=$strdelete3;?>" >
                          <input type="hidden" name="products" value="<?=$strdelete4;?>" >
                          <input type="hidden" name="countries" value="<?=$strdelete5;?>" >
                          <input type="hidden" name="growers" value="<?=$strdelete6;?>" >
                          <input type="hidden" name="dateranges" value="<?=$week_no;?>" >
						</form>
      </div>
       <div class="cl"></div>
      </div>
      <div class="bot"></div>
      <div class="cl"></div>
    </div>
    <div class="cl"></div>
  </div>
  <div class="footerwraper">
  <?php include("include/footer.php"); ?>
  </div>
  <?php include("include/copyright.php"); ?>
  <div class="cl"></div>
</div>
<?php include("include/fixeddiv.php"); ?>
</body>
</html>
<script tpe="text/javascript">
	function funPage(pageno)
	{
		document.frmfprd.startrow.value=pageno;
		document.frmfprd.submit();
	}

	function docolorchange()
	{
	   window.location.href='<?=$siteurl?>buy.php?id=<?=$_GET["id"]?>&categories=<?=$strdelete?>&l=<?=$_GET["l"]?>&c='+$('#color').val();
	}
	
	function frmsubmite()
	{
	  document.frmfilter.submit();
    }
	
	
	function dodateschange()
	{
		document.filterdates.submit();
	}
	
	function doadd(id)
	{
	   var check = 0 ;
	   if($('#qty-'+id).val()=="" )
	   {
	       $('#ermsg-'+id).html("please enter box qty.")
		   check = 1 ;
	   }
	  
	   if(check==0)
	   {
	     $('#ermsg-'+id).html("");
		 $('#gid6').val(id);
		 $('#frmrequest').submit();
	   }	 
	}
</script>
