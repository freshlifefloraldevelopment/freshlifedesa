<?	
	include "../config/config.php";
	session_start();

	$chk=0;
	if(isset($_POST['Submit']) && $_POST["Submit"]=="Login")
	{				
		$login_query = "SELECT * FROM admin WHERE uname='".trim($_POST["txtuname"])."' AND pass ='".trim($_POST["txtpass"])."'";	
		$login_result = mysql_query($login_query) or die(mysql_error());
		if(mysql_num_rows($login_result)==1)
		{
                        $user=mysql_fetch_array($login_result);
			            $_SESSION['tomodachi-admin']=1;
						if($user["isadmin"]==0)
						{
                        $_SESSION['uid']=$user["id"];
						}
						else
						{
						    $_SESSION['uid'] = 1 ;
						}
						$_SESSION['utype']=$user["type"];
						$_SESSION['grower_id']=$user["grower_id"];
			if($_SESSION['grower_id']==0)
			{			
			  header("location: home.php");
			}
			else
			{
			   header("location: growers.php?id=".$_SESSION['grower_id']);
			}  
		}
		else
		{
			$chk=1;
		}
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Area</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script language="javascript">
	function verify()
	{ 
		var arrTmp=new Array();
		arrTmp[0]=checkUname();
		arrTmp[1]=checkPass();
	
		var i;
		_blk=true;
		for(i=0;i<arrTmp.length;i++)
		{
			if(arrTmp[i]==false)
			{
			   _blk=false;
			}
		}
		if(_blk==true)
		{
			return true;
		}
		else
		{
			return false;
		}
	
 	}
	function trim(str) 
	{    
		if (str != null) 
		{        
			var i;        
			for (i=0; i<str.length; i++) 
			{           
				if (str.charAt(i)!=" ") 
				{               
					str=str.substring(i,str.length);                 
					break;            
				}        
			}            
			for (i=str.length-1; i>=0; i--)
			{            
				if (str.charAt(i)!=" ") 
				{                
					str=str.substring(0,i+1);                
					break;            
				}         
			}                 
			if (str.charAt(0)==" ") 
			{            
				return "";         
			} 
			else 
			{            
				return str;         
			}    
		}
	}
	function checkUname()
	{
		
		if(trim(document.frmadminlogin.txtuname.value) == "")
		{	 
			document.getElementById("lbluname").innerHTML="  Please enter login ID  ";
			return false;
		}
		else
		{
			document.getElementById("lbluname").innerHTML="";
			return true;
		}
	}
	function checkPass()
	{
		if(trim(document.frmadminlogin.txtpass.value) == "")
		{	 
			document.getElementById("lblpass").innerHTML="  Please enter password  ";
			return false;
		}
		else
		{
			document.getElementById("lblpass").innerHTML="";
			return true;
		}
	}
</script>
</head>

<body>
<table width="960" border="0" align="center" cellpadding="0" cellspacing="0">
  <? include("includes/header1.php");?>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td background="images/loginbox-bg.gif" style="background-position:center top; background-repeat:no-repeat; height:274px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="330" height="5">&nbsp;</td>
        <td>&nbsp;</td>
        <td width="330">&nbsp;</td>
      </tr>
	   <form action="index.php" method="post" name="frmadminlogin" onSubmit="return verify();">
      <tr>
        <td>&nbsp;</td>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2" align="center">
								<?
							  	if($chk==1)
								{
							  		echo '<span class="error">Invalid login ID or password </span>';
								}
							   ?>&nbsp;</td>
            </tr>
			<tr>
			<td colspan="2">&nbsp;</td>
			</tr>
          <tr>
            <td width="41%" class="text">Login Name : </td>
            <td width="59%"><input name="txtuname" maxlength="20" id="txtuname"  type="text" class="textfieldbig" style="width: 96%;" value="<?= $_POST["txtuname"]?>" /><span id="lbluname" class="error"></span></td>
          </tr>
          <tr>
            <td colspan="2" height="5"></td>
            </tr>
          <tr>
            <td class="text">Password : </td>
            <td><input name="txtpass" maxlength="20" id="txtpass" type="password" class="textfieldbig" style="width: 96%;" /><span id="lblpass" class="error"></span></td>
          </tr>
          <tr>
            <td colspan="2" align="center" height="5"></td>
          </tr>
          <tr>
		    <td>&nbsp;</td>
            <td><input name="Submit" value="Login" id="Submit" class="buttongrey" admintext="Log In" type="submit" /></td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;</td>
          </tr>
        </table></td>
        <td>&nbsp;</td>
      </tr>
	  </form>
    </table></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
 <? include("includes/footer.php");?>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>
