<?php

require_once("config/config_gcp.php");

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login-type"] == 'buyer') {
    header("location:" . SITE_URL . "buyer/buyers-account.php");
    die;
}
if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor-account.php");
    die;
}


            $update_menu = "update control_menu
                                set menu = 2
                              where id = 1 ";
            
            mysqli_query($con, $update_menu);
            
            
#############QUERY TO FETCH PAGE DETAILS testing by  KD Sharma. All working good chetan###################STARTS###########################################################################
$pageId = 17;//VARIETY PAGE ID 
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################

#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL.'assets/css/essentials-flfv3.css', SITE_URL.'assets/css/layout-flfv3.css', 
                            SITE_URL.'assets/css/header-1.css', SITE_URL.'assets/css/layout-shop.css', SITE_URL.'assets/css/color_scheme/blue.css' );
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################
require_once 'includes/header.php';


?>


<section class="page-header page-header-xs">
    <div class="container">

        <h1>LOGIN</h1>

        <!-- breadcrumbs -->
        <ol class="breadcrumb">
            <?php if ($_SESSION["login"] != 1) { ?>
                <li><a href="#">Home</a></li>
                <li><a href="#">Pages</a></li>
                <li class="active">Login</li>
            <?php } else { ?>
                <li><a href="#">Home</a></li>
                <li><a href="<?= SITE_URL ?>sign-out.php">My Account</a></li>
                <li class="active"><a href="<?= SITE_URL ?>sign-out.php">Logout</a></li>
            <?php } ?>	
        </ol><!-- /breadcrumbs -->

        <!-- page tabs -->
        <?php if ($_SESSION["login"] != 1) { ?>
            <ul class="page-header-tabs list-inline">
                <li class="active"><a href="#login_as_buyers" id="login_as_buyers" data-toggle="tab" aria-expanded="true">BUYERS</a></li>
                <li class=""><a href="#login_as_growers" id="login_as_growers" data-toggle="tab" aria-expanded="false">GROWERS</a></li>
            </ul><!-- /page tabs -->
        <?php } ?>		

    </div>
</section>



<section>
    <div class="container">


        <div class="tab-content">
            <?php if ($_SESSION["login"] != 1) { ?>
                <!-- LOGIN -->
                <div class="tab-pane fade in active">

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-md-offset-3 col-sm-offset-3">

                            <!-- ALERT -->
                            <div class="alert alert-mini margin-bottom-30" id="checkLoginErr">
                                    <!--<strong>Oh snap!</strong> Login Incorrect!-->
                            </div><!-- /ALERT -->

                            <div class="box-static box-transparent box-bordered padding-30">
                                <div class="box-title margin-bottom-30">
                                    <h2 class="size-20">Login as <span id="textlogin">Buyers</span></h2>
                                </div>

                                <form class="sky-form" id="loginFormID" action="<?= SITE_URL ?>login.php" method="post" autocomplete="off">
                                    <div class="clearfix">
                                        <?php if ($_SESSION["lang"] != "ru") { ?>
                                            <!-- Email -->
                                            <div class="form-group">
                                                <label>Email</label>
                                                <label class="input margin-bottom-10">
                                                    <i class="ico-append fa fa-envelope"></i>
                                                    <input required="true" name="email"  id="email" type="email" placeholder="Email">
                                                    <b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
                                                </label>
                                            </div>

                                            <!-- Password -->
                                            <div class="form-group">
                                                <label>Password</label>
                                                <label class="input margin-bottom-10">
                                                    <i class="ico-append fa fa-lock"></i>
                                                    <input required="true" type="password" name="password" placeholder="Password">
                                                    <b class="tooltip tooltip-bottom-right">Type your account password</b>
                                                </label>
                                            </div>

                                        <?php } else { ?>
                                            <!-- Email -->
                                            <div class="form-group">
                                                <label>Email</label>
                                                <label class="input margin-bottom-10">
                                                    <i class="ico-append fa fa-envelope"></i>
                                                    <input required="true" name="email"  id="email" type="email" placeholder="Email">
                                                    <b class="tooltip tooltip-bottom-right">Needed to verify your account</b>
                                                </label>
                                            </div>

                                            <!-- Password -->
                                            <div class="form-group">
                                                <label>Пароль</label>
                                                <label class="input margin-bottom-10">
                                                    <i class="ico-append fa fa-lock"></i>
                                                    <input required="true" type="password" name="password" placeholder="Пароль">
                                                    <b class="tooltip tooltip-bottom-right">Type your account password</b>
                                                </label>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-6 col-sm-6 col-xs-6">

                                            <!-- Inform Tip -->                                        
                                            <div class="form-tip pt-20">
                                                <a class="no-text-decoration size-13 margin-top-10 block" id="forgot_id" href="<?php echo SITE_URL?>forgot.php">Forgot Password?</a>
                                            </div>

                                        </div>

                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">

                                            <button class="btn btn-primary"><i class="fa fa-check"></i> LOGIN</button>

                                        </div>

                                    </div>
                                    <input type="hidden" name="usrtype" id="usrtypeID" value="buyer">	
                                    <input type="hidden" name="submit" value="_login">
                                </form>
								<hr>
								<div class="text-center"><span style="color: #979797;">Don't have an account yet? </span><a href="http://content.freshlifefloral.com/register-form?hs_preview=dleYBCdQ-4328719101" style="color: #8AB933; font-weight: bold;">Click to register</a></div>
                                <!--<hr>

                                <div class="text-center">
                                    <div class="margin-bottom-20">&ndash; OR &ndash;</div>

                                    <a class="btn btn-block btn-social btn-facebook margin-top-10">
                                        <i class="fa fa-facebook"></i> Sign in with Facebook
                                    </a>

                                    <a class="btn btn-block btn-social btn-twitter margin-top-10">
                                        <i class="fa fa-twitter"></i> Sign in with Twitter
                                    </a>

                                </div>-->

                            </div>

                        </div>
                    </div>

                </div><!-- /LOGIN -->
            <?php } ?>
        </div>

    </div>
</section>
<?php require_once("includes/footer.php"); ?>	
<script type="text/javascript">
    $("#login_as_growers").click(function (event) {
        $("#textlogin").html('Growers');
        $("#usrtypeID").val('grower');
        var url_s = "<?php echo SITE_URL;?>grower/forgot-grower.php";
        $("#forgot_id").attr("href",url_s);
    });
    $("#login_as_buyers").click(function (event) {
        $("#textlogin").html('Buyers');
        $("#usrtypeID").val('buyer');
        var url_s = "<?php echo SITE_URL;?>grower/forgot-grower.php";
        $("#forgot_id").attr("href",url_s);
    });
    $(function () {
        $("#loginFormID").submit(function (event) {
            $('#checkLoginErr').removeClass("alert-danger");
            $('#checkLoginErr').removeClass("alert-success");
            $('#checkLoginErr').html('');
            event.preventDefault();
            $.ajax({
                url: '<?php echo SITE_URL; ?>savelogin.php',
                type: 'POST',
                data: $(this).serialize(),
                success: function (result) {
                     if (result == 1) {
                        $('#checkLoginErr').html('Login success!');
                        $('#checkLoginErr').addClass("alert-success");
                        //window.location.href = window.location.href;
                        window.location.href = "<?php echo SITE_URL; ?>buyer/buyers-account.php?menu=1";
						
                    } else {
                        
                         if (result == 2) {
                        $('#checkLoginErr').html('Login success!');
                        $('#checkLoginErr').addClass("alert-success");
                        //window.location.href = window.location.href;
                        //window.location.href = "<?php echo SITE_URL; ?>growers/growers-account-summery.php";
                        window.location.href = "<?php echo SITE_URL; ?>file/vendor-account.php";                        
                            }  else{
                        $('#checkLoginErr').addClass("alert-danger");
                        $('#checkLoginErr').html(result);
                    }
                    }
                }
            });
        });
    });
</script>		
