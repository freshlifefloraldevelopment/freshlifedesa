<?php
if(isset($_POST['page'])){
    //Include pagination class file
    include('Pagination.php');
    
    //Include database configuration file
    include('../config/config_gcp.php');
    
    $start = !empty($_POST['page'])?$_POST['page']:0;
    $limit = 6;
   
    //get number of rows
    $queryNum = mysqli_query($con, "SELECT DISTINCT(user_ratings.ur_user_idFk), COUNT(user_ratings.ur_id) as postNum FROM user_ratings WHERE ur_status =1 AND ur_grower_idFk = " . $_SESSION['grower_id']);
    $resultNum = mysqli_fetch_assoc($queryNum);
    $rowCount = $resultNum['postNum'];
    //initialize pagination class
    $pagConfig = array('baseURL'=>'pagination/getUserComments.php', 'totalRows'=>$rowCount, 'currentPage'=>$start, 'perPage'=>$limit, 'contentDiv'=>'posts_content');
    $pagination =  new Pagination($pagConfig);
    
    //get rows
    //$query = mysqli_query($con, "SELECT DISTINCT(user_ratings.ur_user_idFk), user_ratings.ur_comment, user_ratings.ur_final_rating, ur_create_datetime, users.u_first_name, users.u_last_name, users.u_gender, users.u_picture FROM user_ratings LEFT JOIN users ON user_ratings.ur_user_idFk = users.u_id WHERE ur_grower_idFk = ". $_SESSION['grower_id']." ORDER BY ur_id DESC LIMIT $start, $limit");
    $query = mysqli_query($con, "SELECT DISTINCT(ur_user_idFk), ur_user_type, ur_comment, ur_final_rating, ur_create_datetime FROM user_ratings WHERE ur_status =1 AND ur_grower_idFk = ".$_SESSION['grower_id']." ORDER BY ur_id DESC LIMIT $start, $limit");
    
    if(mysqli_num_rows($query) > 0){ ?>
        <div class="posts_list">
        <?php
            while($row_user_rating = mysqli_fetch_assoc($query)){ 
                if($row_user_rating['ur_user_type'] == 'admin'){
                                               
                    $row_user_rating['u_first_name'] = 'Admin';
                    $row_user_rating['u_last_name'] = '';
                    $row_user_rating['u_picture'] = '';
                    $row_user_rating['u_gender'] = 'male';
                    $pic_path = $pic_path = SITE_URL.'includes/assets/profile_pictures/default_male.jpg';
                 }
                 else if($row_user_rating['ur_user_type'] == 'buyer'){
                     //echo 'it is buyer';
                     $sql_data = "SELECT first_name, last_name, profile_image FROM buyers WHERE id= ".$row_user_rating['ur_user_idFk'];
                     $data_res = mysqli_query($con, $sql_data);
                     $row = mysqli_fetch_assoc($data_res);

                     $row_user_rating['u_first_name'] = $row['first_name'];
                     $row_user_rating['u_last_name'] = $row['last_name'];
                     $row_user_rating['u_picture'] = $row['profile_image'];
                     $row_user_rating['u_gender'] = 'male';
                     if($row['profile_image'] != ''){
                         $pic_path = SITE_URL.'profile_images/'.$row_user_rating['u_picture'];
                     }
                     else{
                         $pic_path = SITE_URL.'includes/assets/profile_pictures/default_male.jpg';
                     }


                 }else if($row_user_rating['ur_user_type'] == 'user'){
                     $sql_data = "SELECT u_first_name, u_last_name, u_picture FROM users WHERE u_id= ".$row_user_rating['ur_user_idFk'];
                     $data_res = mysqli_query($con, $sql_data);
                     $row = mysqli_fetch_array($data_res);

                     $row_user_rating['u_first_name'] = $row['u_first_name'];
                     $row_user_rating['u_last_name'] = $row['u_last_name'];
                     $row_user_rating['u_picture'] = $row['u_picture'];
                     $row_user_rating['u_gender'] = $row['u_gender'];

                     if (strpos($row_user_rating['u_picture'], 'https://') !== false) {
                         $pic_path = $row_user_rating['u_picture'];
                     }
                     else{
                         $pic_path = SITE_URL.'includes/assets/profile_pictures/default_male.jpg';
                         if($row_user_rating['u_gender'] == 'female'){
                             $pic_path = SITE_URL.'includes/assets/profile_pictures/default_female.jpg';
                         } 
                     }

                 }
                
        ?>
            <!-- First Comment -->
            <div class="row">
                <div class="col-md-2 col-sm-2 hidden-xs">
                    <figure class="thumbnail">
                        <img class="img-responsive" src="<?php echo $pic_path; ?>" alt="User Pic">
                        <figcaption class="text-center"><?php echo $row_user_rating['u_first_name'] . ' ' . $row_user_rating['u_last_name']; ?></figcaption>
                    </figure>
                </div>
                <div class="col-md-10 col-sm-10">
                    <div class="panel panel-default arrow left">
                        <div class="panel-body">
                            <header class="text-left">
                                <div class="comment-user"><i class="fa fa-user"></i> <?php echo $row_user_rating['u_first_name'] . ' ' . $row_user_rating['u_last_name']; ?></div>
                                <div>
                                <?php
                                for ($i = 1; $i <= $row_user_rating['ur_final_rating']; $i++) {
                                ?>
                                    <img alt="1" src="<?php echo SITE_URL; ?>images/star-on.png">
                                <?php
                                }
                                ?>
                                </div>
                                <span class="comment-date"><i class="fa fa-clock-o"></i> <?php echo date('F d, Y', strtotime($row_user_rating['ur_create_datetime'])); ?></span>

                            </header>
                            <div class="comment-post">
                                <p>
                                    <?php echo $row_user_rating['ur_comment']; ?>
                                </p>
                            </div>
                            <!--<p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>-->
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
        <div class="col-md-12">
            <?php echo $pagination->createLinks(); ?>
        </div>    
<?php }
}
?>
