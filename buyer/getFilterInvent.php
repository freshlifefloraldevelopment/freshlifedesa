<?php

require_once("../config/config_gcp.php");

include("/pagination/pagination.php");


if (isset($_GET["lang"]) && $_GET["lang"] != "") {
$_SESSION["lang"] = $_GET["lang"];

}

if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}


if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor-account.php");
    die;
}

$userSessionID = $_SESSION["buyer"];
$shippingMethod = $_SESSION['shippingMethod'];
$getShippingMethod = "select connections from shipping_method where id = '" . $shippingMethod . "'";
$shippingMethodRes = mysqli_query($con, $getShippingMethod);
$shippingMethodData = mysqli_fetch_assoc($shippingMethodRes);
$shippingMethodArray = unserialize($shippingMethodData['connections']);
$firstConnection = $shippingMethodArray['connection_1'];
$getConnectionType = "select type from connections where id='" . $firstConnection . "'";
$conType = mysqli_query($con, $getConnectionType);
$connectionsType = mysqli_fetch_assoc($conType);
$connectionType = $connectionsType['type'];


foreach ($shippingMethodArray as $connections) {

    $getConnections = "select charges_per_kilo,charges_per_shipment from connections where id='" . $connections . "'";
    $conDatas = mysqli_query($con, $getConnections);
    $connectionDatas = mysqli_fetch_assoc($conDatas);
    $cpk = unserialize($connectionDatas['charges_per_kilo']);
    foreach ($cpk as $perkilo) {
        $chargesPerKilo = $chargesPerKilo + $perkilo;
    }
    $cps = unserialize($connectionDatas['charges_per_shipment']);

    foreach ($cps as $pership) {
        $chargesPerShip = $chargesPerShip + $pership;
    }
}

/*********get the data of session user****************/

if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {

    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();

    if (empty($userID)) {

        /*********If not exist send to home page****************/
        header("location:" . SITE_URL);
        die;
    }

} else {

    /*********If not statement send to home page****************/

    header("location:" . SITE_URL);

    die;

}

$img_url = 'profile_images/noavatar.jpg';

if ($profile_image) {
    $img_url = 'profile_images/' . $profile_image;
}

$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);
$shipping_method = trim($info["shipping"], ",");

if (!isset($_REQUEST["gid6"])) {

    $sel_login_check = "select * from activity where buyer='" . $info["id"] . "' and ldate='" . date("Y-m-d") . "' and ltime='" . $hm . "' and type='buyer' and atype='li'";
    $rs_login_check = mysqli_query($con, $sel_login_check);
    $login_check = mysqli_num_rows($rs_login_check);

    if ($login_check >= 1) {

    } else {

        $name = $info["first_name"] . " " . $info["last_name"];
        $activity = "Live Inventory at " . $hm;
        $insert_login = "insert into activity set buyer='" . $info["id"] . "',ldate='" . date("Y-m-d") . "',type='buyer',name='" . $name . "',activity='" . $activity . "',ltime='" . $hm . "',atype='li'";
        mysqli_query($con, $insert_login);
    }

}

$sel_testi = "select gpb.id,g.growers_name as gname,g.id as gid,g.blockstate as gbs from grower_product_box_packing gpb
                left join product p on gpb.prodcutid = p.id
                left join growers g on gpb.growerid=g.id		  
               where g.active='active' and g.growers_name is not NULL ";

$sel_testi .= " group by g.id order by p.name, g.growers_name";/**/
$rs_testi = mysqli_query($con, $sel_testi);
$total_testi = mysqli_num_rows($rs_testi);

if ($total_testi >= 1) {

    $check_grower = "";

    while ($testi = mysqli_fetch_array($rs_testi)) {

        if ($info["state"] > 0 && $testi["gbs"] != "") {

            $temp47 = explode(",", $testi["gbs"]);

            if (in_array($info["state"], $temp47)) {

            } else {
                $check_grower .= $testi["gid"] . ",";
            }

        } else {
            $check_grower .= $testi["gid"] . ",";
        }
    }
}

$check_grower = rtrim($check_grower, ',');

if ($shipping_method > 0) {

    $select_shipping_info = "select name,description from shipping_method where id='" . $shipping_method . "'";
    $rs_shipping_info = mysqli_query($con, $select_shipping_info);
    $shipping_info = mysqli_fetch_array($rs_shipping_info);
}

$sel_connections = "select days from connections where shipping_id='" . $shipping_method . "' order by id desc limit 0,1";
$rs_connections = mysqli_query($con, $sel_connections);
$connections = mysqli_fetch_array($rs_connections);
$days = explode(",", $connections["days"]);
$count1 = sizeof($days);
$res = "";


for ($io = 0; $io <= $count1 - 1; $io++) {

    if ($days[$io] == 0 || $days[$io] == 1) {
        $res .= "6";
        $res .= ",";
    } else {
        $res .= $days[$io] - 1;
        $res .= ",";
    }
}

$avdays = explode(",", $res);
$result = array_unique($avdays);
$cunav = sizeof($result);
$datedd = $shpping_onr;
$tdate = date("Y-m-d");
$date1 = date_create($datedd);
$date2 = date_create($tdate);
$interval = $date2->diff($date1);
$checka2 = $interval->format('%R%a');

if ($checka2 > 0 || $checka2 < 0) {

    $weekday = date('l', strtotime($datedd));

    if ($weekday == 'Monday') {
        $opq = 1;
    } else if ($weekday == 'Tuesday') {
        $opq = 2;
    } else if ($weekday == 'Wednesday') {
        $opq = 3;
    } else if ($weekday == 'Thursday') {
        $opq = 4;
    } else if ($weekday == 'Friday') {
        $opq = 5;
    } else if ($weekday == 'Saturday') {
        $opq = 6;
    } else if ($weekday == 'Sunday') {
        $opq = 0;
    }

    if (in_array($opq, $result)) {
        $next = $opq;
    } else {

        for ($i = 1; $i <= 6; $i++) {

            $opq = $opq + 1;

            if (in_array($opq, $result)) {
                $next = $opq;
                break;
            }

            if ($opq == 6) {
                $opq = 1;
            }
        }
    }


    if ($next == 1) {
        $dayname = "monday";
    } else if ($next == 2) {
        $dayname = "tuesday";
    } else if ($next == 3) {
        $dayname = "wednesday";
    } else if ($next == 4) {
        $dayname = "thursday";
    } else if ($next == 5) {
        $dayname = "friday";
    } else if ($next == 6) {
        $dayname = "saturday";
    }

    $shpping_onr = date('Y-m-d', strtotime('next ' . $dayname));

} else {

    $tommorow = mktime(date("H"), date("i"), date("s"), date("m"), date("d") + 1, date("Y"));
    $shpping_onr = date("Y-m-d", $tommorow);
}

$tempk = explode("-", $shpping_onr);
$shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
$today = date("Y-m-d");

function getWeekday($date)
{
    return date('w', strtotime($date));
}

$day_of_week = getWeekday($today); // returns 4

if ($day_of_week == 0) {

    $starting_date = date('Y-m-d', strtotime($today . ' +8 day'));
    $days_add = 8;
    $days_add2 = 13;
}

if ($day_of_week == 1) {
    $starting_date = date('Y-m-d', strtotime($today . ' +7 day'));
    $days_add = 7;
    $days_add2 = 12;
}

if ($day_of_week == 2) {
    $starting_date = date('Y-m-d', strtotime($today . ' +6 day'));
    $days_add = 6;
    $days_add2 = 11;
}

if ($day_of_week == 3) {
    $starting_date = date('Y-m-d', strtotime($today . ' +5 day'));
    $days_add = 5;
    $days_add2 = 10;
}

if ($day_of_week == 4) {
    $starting_date = date('Y-m-d', strtotime($today . ' +11 day'));
    $days_add = 11;
    $days_add2 = 16;
}

if ($day_of_week == 5) {
    $starting_date = date('Y-m-d', strtotime($today . ' +10 day'));
    $days_add = 10;
    $days_add2 = 15;
}

if ($day_of_week == 6) {
    $starting_date = date('Y-m-d', strtotime($today . ' +9 day'));
    $days_add = 9;
    $days_add2 = 14;
}

if ($day_of_week == 7) {
    $starting_date = date('Y-m-d', strtotime($today . ' +7 day'));
    $days_add = 7;
    $days_add2 = 12;
}

$end_date = date('Y-m-d', strtotime($starting_date . '+5 day'));

if ($starting_date != "" && $end_date != "") {

    function week_number($date)
    {
        return ceil(date('j', strtotime($date)) / 7);
    }

    $week_no = week_number($starting_date);

    if ($week_no == 1) {
        $option_update = 1;
    } else if ($week_no == 2) {
        $option_update = 2;
    } else if ($week_no == 3) {
        $option_update = 1;
    } else if ($week_no == 4) {
        $option_update = 4;
    } else if ($week_no == 5) {
        $option_update = 5;
    }


    $temp_starting_date = explode("-", $starting_date);
    $orginal_starting_date = $temp_starting_date[1] . "-" . $temp_starting_date[2] . "-" . $temp_starting_date[0];
    $temp_end_date = explode("-", $end_date);
    $orginal_end_date = $temp_end_date[1] . "-" . $temp_end_date[2] . "-" . $temp_end_date[0];
}

$page_request = "inventory";

?>

<?php

$wh = "";

if ($_REQUEST['filter_category'] != "") {
    $wh .= " AND s.cat_id=" . $_REQUEST['filter_category'];
}

if ($_REQUEST['filter_grower'] != "") {
    $wh .= " AND gpb.growerid=" . $_REQUEST['filter_grower'];/*126*/
}

if ($_REQUEST['filter_pack'] != "") {
    $wh .= " AND s.cat_id=" . $_REQUEST['filter_pack'];
}

if ($_REQUEST['filter_size'] != "") {
    $wh .= " AND sh.id  =" . $_REQUEST['filter_size'];
}

if ($_REQUEST['filter_variety'] != "") {
    $wh .= " AND gpb.prodcutid=" . $_REQUEST['filter_variety'];
}


/*
if ($_REQUEST['filter_perbunch']!=""){

    //echo '<script language="javascript">alert("'.$_REQUEST['filter_perbunch'].'");</script>';
    $wh.=" And bs.id=".$_REQUEST['filter_perbunch'];//////////aqui  estamos
}

*/
//echo '<script language="javascript">alert("hola1'.$_REQUEST['filter_date'].'");</script>';
/*-------------------------------------------------------------------------------*/

$sel_products = "select gpb.id from grower_product_box_packing gpb
                   left join product p on gpb.prodcutid = p.id
                   left join growers g on gpb.growerid=g.id
                  where g.active='active' and gpb.stock > 0 and p.name is not null order by p.name";

$rs_prodcuts = mysqli_query($con, $sel_products);
$total = mysqli_num_rows($rs_prodcuts);
$num_record = $total;

$from=0;
$display=50;
$page = (int) (!isset($_REQUEST["page_number"]) ? 1 : $_REQUEST["page_number"]);
$page = ($page == 0 ? 1 : $page);
$from = ($page - 1) * $display;

$dates = date("jS F Y");
if ($_REQUEST['filter_date']!="") {
    $variable = $_REQUEST['filter_date'];
    $fech = date("Y-n-j");

    if (strtotime($fech) == strtotime($variable)) {
        $dates = date("jS F Y");
        $query2 = "select gpb.prodcutid,gpb.comment,gpb.id as gid,gpb.stock as stock,gpb.qty,
                          gpb.sizeid,gpb.feature,gpb.type as bv,gpb.boxname as bvname,
                          gpb.box_type as bvtype,gpb.price,gpb.growerid,p.id,p.name,p.color_id,
                          p.image_path,p.box_type as p_box_type,s.name as subs,g.file_path5,
                          g.growers_name,sh.name as sizename,ff.name as featurename,b.name as boxname,
                          b.width,b.length,b.height,bs.name as bname,bt.name as boxtype,b.type,
                          c.name as colorname,gpb.box_id from grower_product_box_packing gpb
                     left join product p on gpb.prodcutid = p.id
                     left join subcategory s on p.subcategoryid=s.id  
                     left join colors c on p.color_id=c.id 
                     left join features ff on gpb.feature=ff.id
                     left join sizes sh on gpb.sizeid=sh.id 
                     left join boxes b on gpb.box_id=b.id
                     left join boxtype bt on b.type=bt.id
                     left join growers g on gpb.growerid=g.id
                     left join bunch_sizes bs on gpb.bunch_size_id=bs.id
                    where g.active='active' and gpb.stock > 0 and p.name is not null $wh";

        $query2 .= " order by p.name,s.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER),g.growers_name  LIMIT $from,$display";
        $result2 = mysqli_query($con, $query2);

    } else {

        $date = new DateTime($variable);

        $dates = $date->format('jS F Y');

        $query2 = "select gpb.comment,gpb.prodcutid,gpb.id as gid,gpb.stock as stock,
                          gpb.qty,gpb.sizeid,gpb.feature,gpb.type as bv,gpb.boxname as bvname,
                          gpb.box_type as bvtype,gpb.price,gpb.growerid,p.id,p.name,p.color_id,
                          p.image_path,p.box_type as p_box_type,
                          s.name as subs,g.file_path5,g.growers_name,sh.name as sizename,
                          ff.name as featurename,b.name as boxname,
                          b.width,b.length,b.height,bs.name as bname,bt.name as boxtype,b.type,
                          c.name as colorname,gpb.box_id 
                     from history_gpbp gpb
                     left join product p on gpb.prodcutid = p.id
                     left join subcategory s on p.subcategoryid=s.id  
                     left join colors c on p.color_id=c.id 
                     left join features ff on gpb.feature=ff.id
                     left join sizes sh on gpb.sizeid=sh.id 
                     left join boxes b on gpb.box_id=b.id
                     left join boxtype bt on b.type=bt.id
                     left join growers g on gpb.growerid=g.id
                     left join bunch_sizes bs on gpb.bunch_size_id=bs.id            
                    where g.active='active' and gpb.stock > 0 and (p.name is not null) $wh";

            /*where g.active='active' and gpb.stock > 0 and (p.name is not null) and gpb.date='" . $variable . "'";*/

        $query2 .= " ";

        $query2 .= " order by p.name,s.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER),g.growers_name  LIMIT $from,$display";

        $result2 = mysqli_query($con, $query2);

    }

} else {

    $query2 = "select gpb.prodcutid,gpb.comment,gpb.id as gid,gpb.stock as stock,
                      gpb.qty,gpb.sizeid,gpb.feature,gpb.type as bv,
                      gpb.boxname as bvname,gpb.box_type as bvtype,
                      gpb.price,gpb.growerid,
                      p.id,p.name,p.color_id,p.image_path,p.box_type as p_box_type,
                      s.name as subs,
                      g.file_path5,
                      g.growers_name,sh.name as sizename,
                      ff.name as featurename,b.name as boxname,b.width,b.length,b.height,
                      bs.name as bname,bt.name as boxtype,
                      b.type,c.name as colorname, gpb.box_id 
                 from grower_product_box_packing gpb
                 left join product p on gpb.prodcutid = p.id
                 left join subcategory s on p.subcategoryid=s.id  
                 left join colors c on p.color_id=c.id 
                 left join features ff on gpb.feature=ff.id
                 left join sizes sh on gpb.sizeid=sh.id 
                 left join boxes b on gpb.box_id=b.id
                 left join boxtype bt on b.type=bt.id
                 left join growers g on gpb.growerid=g.id
                 left join bunch_sizes bs on gpb.bunch_size_id=bs.id
                where g.active='active' and gpb.stock > 0 and p.name is not null $wh";

    $query2 .= " ";

    $query2 .= " order by p.name,s.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER),g.growers_name  LIMIT $from,$display";

    $result2 = mysqli_query($con, $query2);
}


?>


<?php


$i = 0;

$tp = mysqli_num_rows($result2);

if ($tp == 0) {

    echo $XX = '<div class="notfound" style="margin-top:30px;">No Item Found !</div>';

} else {



    while ($products = mysqli_fetch_array($result2)) {

        //echo '<pre>';print_r($products);die;

        $price["price"] = $products["price"];

        if ($products["growerid"] != "407") {

            $sel_weight = "select weight from grower_product_box_weight where growerid='" . $products["growerid"] . "' and prodcutid='" . $products["prodcutid"] . "' and sizeid='" . $products["sizeid"] . "' and feature='" . $products["feature"] . "' and box_id='" . $products["box_id"] . "' order by id desc limit 0,1";

            $rs_weight = mysqli_query($con, $sel_weight);

            $weight = mysqli_fetch_array($rs_weight);

        } else {

            $sel_weight = "select weight from grower_product_box_weight where growerid='" . $products["growerid"] . "' and prodcutid='" . $products["prodcutid"] . "' and sizeid='" . $products["sizeid"] . "' and feature='" . $products["feature"] . "' order by id desc limit 0,1";

            $rs_weight = mysqli_query($con, $sel_weight);

            $weight = mysqli_fetch_array($rs_weight);

        }


        $growers["box_weight"] = $weight["weight"];

        $growers["box_volumn"] = round(($products["width"] * $products["length"] * $products["height"]) / 1728, 2);

        $growers["shipping_method"] = $shipping_method;

        if ($growers["shipping_method"] > 0) {

            $total_shipping = 0;

            $sel_connections = "select type,shipping_rate,addtional_rate,box_price,box_weight_arranged,price_per_box from connections where shipping_id='" . $growers["shipping_method"] . "'";

            $rs_connections = mysqli_query($con, $sel_connections);

            while ($connections = mysqli_fetch_array($rs_connections)) {

                if ($connections["type"] == 2) {

                    $total_shipping = $total_shipping + round(($growers["box_weight"] * $connections["shipping_rate"]), 2);

                } else if ($connections["type"] == 4) {

                    $total_shipping = $total_shipping + round(($growers["box_volumn"] * $connections["shipping_rate"]), 2);

                } else if ($connections["type"] == 1) {

                    $box_type_calc = $products["boxtype"];

                    if ($box_type_calc == 'HB') {

                        $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2;

                        $connections["price_per_box"] = $connections["price_per_box"] / 2;

                    }

                    if ($box_type_calc == 'JUMBO') {

                        $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2;

                        $connections["price_per_box"] = $connections["price_per_box"] / 2;

                    } else if ($box_type_calc = 'QB') {

                        $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 4;

                        $connections["price_per_box"] = $connections["price_per_box"] / 4;

                    } else if ($box_type_calc = 'EB') {

                        $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 8;

                        $connections["price_per_box"] = $connections["price_per_box"] / 8;

                    }

                    $connections["shipping_rate"] = $connections["price_per_box"] / $connections["box_weight_arranged"];

                    if ($growers["box_weight"] <= $connections["box_weight_arranged"]) {

                        $total_shipping = $total_shipping + round(($growers["box_weight"] * $connections["shipping_rate"]), 2);

                    } else {

                        $total_shipping = $total_shipping + round(($connections["box_weight_arranged"] * $connections["shipping_rate"]), 2);

                        $remaining = round($growers["box_weight"] - $connections["box_weight_arranged"], 2);

                        $total_shipping = $total_shipping + round(($remaining * $connections["addtional_rate"]), 2);

                    }

                } else if ($connections["type"] == 3) {

                    $total_shipping = $total_shipping + $connections["box_price"];

                }

            }

        }

        $k = explode("/", $products["file_path5"]);

        $data = getimagesize(SITE_URL . "user/logo2/" . $k[1]);

        $logourl = SITE_URL . "user/logo/" . $k[1];

        ?>

        <input type="hidden" id="bv-<?= $products["gid"] ?>" value="<?php echo $products["bv"]; ?>"/>

        <?php if ($products["bv"] != 2) { ?>

            <?php

            $total_price = $price["price"];

            $total_taxs = 0;

            $total_handling_feess = 0;

            $total_product_price = $price["price"];

            if ($info["tax"] != 0) {

                $total_price = $total_price + (($price["price"] * $info["tax"]) / 100);

                $total_taxs = ($price["price"] * $info["tax"]) / 100;

            }

            if ($info["handling_fees"] != 0) {

                $total_price = $total_price + (($price["price"] * $info["handling_fees"]) / 100);

                $total_handling_feess = ($price["price"] * $info["handling_fees"]) / 100;

            }

            $total_price = $total_price + $chargesPerKilo;

            $total_price = round(($total_price), 2);

            $final_price2 = $total_price;

            ?>

            <input type="hidden" id="subcategoryname-<?php echo $products["gid"] ?>" value="<?php echo $products["subs"] ?>">

            <input type="hidden" id="productname-<?php echo $products["gid"] ?>"  value="<?php echo $products["name"] ?> <?php echo $products["featurename"] ?>">

            <input type="hidden" id="featurename-<?php echo $products["gid"] ?>"  value="<?php echo $products["featurename"] ?>">

            <input type="hidden" id="sizename-<?php echo $products["gid"] ?>"  value="<?php echo $products["sizename"] ?> cm">

            <input type="hidden" id="boxweight-<?php echo $products["gid"] ?>" value="<?php echo $weight["weight"] ?>">

            <input type="hidden" id="boxvolumn-<?php echo $products["gid"] ?>"  value="<?php echo ($products["width"] * $products["length"] * $products["height"]) ?>">

            <input type="hidden" id="bunchsize-<?php echo $products["gid"] ?>"  value="<?php echo $products["bname"] ?> Stems">

            <input type="hidden" id="bunchqty-<?php echo $products["gid"] ?>"   value="<?php echo $products["qty"] ?>">

            <input type="hidden" id="shipping-<?php echo $products["gid"] ?>"   value="<?php echo $product_shippings ?>">

            <input type="hidden" id="handling-<?php echo $products["gid"] ?>"   value="<?php echo $total_handling_feess ?>">

            <input type="hidden" id="tax-<?php echo $products["gid"] ?>" value="<?php echo $total_taxs ?>">

            <input type="hidden" id="totalprice-<?php echo $products["gid"] ?>" value="<?php echo $final_price2 ?>">

            <input type="hidden" id="ooprice-<?php echo $products["gid"] ?>"  value="<?php echo $price["price"] ?>">

            <input type="hidden" id="boxtypename-<?php echo $products["gid"] ?>"  value="<?php echo $products["boxtype"] ?>">

            <input type="hidden" id="stock-<?php echo $products["gid"] ?>"   value="<?php echo $products["stock"] ?>">

            <input type="hidden" id="boxtype-<?php echo $products["gid"] ?>"  value="<?php echo $products["type"] ?>-">

            <input type="hidden" id="product-<?php echo $products["gid"] ?>"  value="<?php echo $products["prodcutid"] ?>">

            <input type="hidden" id="sizeid-<?php echo $products["gid"] ?>" value="<?php echo $products["sizeid"] ?>">

            <input type="hidden" id="feature-<?php echo $products["gid"] ?>"  value="<?php echo $products["feature"] ?>">

            <input type="hidden" id="grower-<?php echo $products["gid"] ?>"    value="<?php echo $products["growerid"] ?>">

            <?php

        } else {

            $sel_products_box = "select * from grower_box_products where box_id ='" . $products["gid"] . "'";
            $rs_product_box = mysqli_query($con, $sel_products_box);
            $total_product_box = mysqli_num_rows($rs_product_box);

            $sel_products_box_price = "SELECT SUM(price*bunchqty),SUM(bunchsize*bunchqty) FROM grower_box_products where box_id ='" . $products["gid"] . "'";
            $rs_products_box_price = mysqli_query($con, $sel_products_box_price);
            $products_box_price = mysqli_fetch_array($rs_products_box_price);

            $final_multi_price_qty = $products_box_price['SUM(bunchsize*bunchqty)'];

            $final_multi_price = ($products_box_price['SUM(price*bunchqty)'] / $final_multi_price_qty);

            ?>

            <input type="hidden" id="gprice-<?php echo $products["gid"] ?>"   value="<?php echo sprintf("%.2f", ($final_multi_price)) ?>">

            <input type="hidden" id="boxtype-<?php echo $products["gid"] ?>"  value="<?php echo $products["bvtype"] ?>">

            <input type="hidden" id="stock-<?php echo $products["gid"] ?>"  value="<?php echo $products["stock"] ?>">

            <input type="hidden" id="product-<?php echo $products["gid"] ?>" value="<?php echo $products["prodcutid"] ?>">

            <input type="hidden" id="sizeid-<?php echo $products["gid"] ?>"  value="<?php echo $products["sizeid"] ?>">

            <input type="hidden" id="feature-<?php echo $products["gid"] ?>" value="<?php echo $products["feature"] ?>">

            <input type="hidden" id="grower-<?php echo $products["gid"] ?>"  value="<?php echo $products["growerid"] ?>">

            <input type="hidden" id="multi-<?php echo $products["gid"] ?>" value="1">

        <?php } ?>

        <tr>

            <!--Grower-->

            <td>

                <?php

                $getprofile = "SELECT profile_image,file_path5 FROM growers WHERE id='" . $products["growerid"] . "'";

                $row_getprofile = mysqli_query($con, $getprofile);

                $row_Profile = mysqli_fetch_assoc($row_getprofile);

                //echo "<pre>";print_r($row_Profile);echo "</pre>";

                if ($row_Profile['profile_image'] != "") { ?>

                    <img width="50"   src="<?php echo SITE_URL . "user/" . $row_Profile['profile_image']; ?>">

                <?php } else if ($row_Profile['file_path5'] != "") { ?>

                    <img width="50"  src="<?php echo SITE_URL . "user/" . $row_Profile['file_path5']; ?>">

                <?php } ?>

                <!--Modal image for 3452-->

                <div class="modal fade bs-example-modal-sm"  id="single_product_<?php echo $products["gid"] ?>"
                            tabindex="-1" role="dialog"  aria-labelledby="mySmallModalLabel" aria-hidden="true"  style="display: none;">

                    <div class="modal-dialog modal-sm">
                        
                        <div class="modal-content">

                            <div class="modal-header">

                                <button aria-hidden="true" data-dismiss="modal"  class="close" type="button">X </button>

                                <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;"><img width="100%"  src="<?php echo $logourl ?>"/> </h4>
                            </div>

                        </div>

                    </div>

                </div>

                <!---->

            </td>

            <!--Product-->
            <td><?php echo $products["subs"] ?></td>                                                    
            <td>

                <a href="" data-toggle="modal" data-target="#single_product_modal<?php echo $products["gid"] ?>">

                    <?php

                    echo $products['name'] . " " . $products["colorname"] .

                        " " . $products["sizename"] . " CM " . $products['featurename'] . $products['bname'] . " St/Bu";

                    ?> </a>

                <!--Modal image for single product-->

                <div class="modal fade bs-example-modal-sm"  id="single_product_modal<?php echo $products["gid"] ?>"
                             tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"  style="display: none;">

                    <div class="modal-dialog modal-sm">

                        <div class="modal-content">

                            <div class="modal-header">

                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>

                                <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;"><?php echo $products["name"] ?><?php echo $products["featurename"] ?></h4>

                            </div>

                            <div class="modal-body">

                                <div class="row">

                                    <div class="col-md-12">

                                        <img  src="<?php echo SITE_URL . $products["image_path"]; ?>"  width="100%">

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <!---->
            </td>

            <!--Pack-->

            <td><?php echo $products["bname"] * $products["qty"] ?></td>

            <!--<td>160</td>-->

            <?php

            $total_price = $price["price"];

            $total_taxs = 0;

            $total_handling_feess = 0;

            if ($info["tax"] != 0) {

                $total_price = $total_price + (($price["price"] * $info["tax"]) / 100);

                $total_taxs = ($price["price"] * $info["tax"]) / 100;
            }

            if ($info["handling_fees"] != 0) {

                $total_price = $total_price + (($price["price"] * $info["handling_fees"]) / 100);

                $total_handling_feess = ($price["price"] * $info["handling_fees"]) / 100;
            }

            if ($product_shipping > 0) {

                $total_price = $total_price + $product_shipping;

                $product_shippings = $product_shipping;
            }

            $total_price = $total_price + $chargesPerKilo;

            $total_price = round(($total_price), 2);

            ?>

            <!--Price-->

            <td>

                <a href="" data-toggle="modal"  data-target="#single_product_price<?php echo $products["gid"] ?>">$<?php echo sprintf("%.2f", $total_price) ?> </a>

                <!--Modal image for 3452-->

                <div class="modal fade bs-example-modal-sm"  id="single_product_price<?php echo $products["gid"] ?>"
                            tabindex="-1" role="dialog"  aria-labelledby="mySmallModalLabel" aria-hidden="true"  style="display: none;">

                    <div class="modal-dialog modal-sm">

                        <div class="modal-content">

                            <div class="modal-header">

                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>

                                <h4 id="myLargeModalLabel3" class="modal-title"  style="font-size: 14px;">ALL IN PRICE</h4>

                            </div>

                            <div class="modal-body">

                                <div class="row">

                                    <div class="col-md-12">

                                        <p>Grower Price.............$<?php echo sprintf("%.2f", $price["price"]) ?></p>

                                        <p>Shipping.....................$<?php echo sprintf("%.2f", $chargesPerKilo) ?></p>

                                        <p>Tax..............................$<?php echo sprintf("%.2f", $total_taxs) ?></p>

                                        <p>Handling(8,5%).........$<?php echo sprintf("%.2f", $total_handling_feess) ?></p>

                                        <hr>

                                        <p>Final  Price..................$<?php echo sprintf("%.2f", $total_price) ?></p>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <!----></td>

            <!--Bunch or Stem-->

            <td>

                <?php


                if ($products['p_box_type'] == "0") {
                    echo "Stem";
                } else {
                    echo "Bunch";
                }
                ?>
            </td>
            <!--Boxes Avaliable-->
            <td align="center"><?php echo $products["stock"]; ?><?php echo $products["boxtype"]; ?></td>
            <td>
                <?php
                $products['featurename'];
                ?>
            </td>
            <!--Send  Request-->
            <td>
                <button type="button" class="btn btn-default btn-xs modal_click_1"  data-toggle="modal"  data-target=".qty_modal_<?php echo $i; ?>" style="background-color:#212f3d;color:white">
                    <i  class="fa fa-send"></i> Request</button>
                
                
                <!--Quantity Modal Start-->
                <div class="modal fade qty_modal_<?php echo $i; ?>"
                     tabindex="-1" role="dialog"
                     aria-labelledby="mySmallModalLabel" aria-hidden="true"
                     style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button aria-hidden="true" data-dismiss="modal"
                                        class="close" type="button">×
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Order
                                    Details</h4>
                            </div>

                            <div class="modal-body">
                                <h4>Please Select Quantity</h4>
                                <input type="text" value="2" min="0" max="1000"
                                       class="form-control stepper" id="qty_amount">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="margin-bottom-20">Select your price
                                            
                                                <input type="text"  id="select_price_cls"  class="form-control select_price_cls">
                                            
                                        </div>
                                        <div class="slider-wrapper black-slider">
                                            <div id="slider5"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive"
                                             style="margin-top:30px;">
                                            <table class="table table-hover">

                                                <thead>
                                                <tr>
                                                    <th>Bunch/Stem----</th>
                                                    <th>Grower Price</th>
                                                    <th>Shipping Cost</th>
                                                    <th>Tax</th>
                                                    <th>Handling</th>
                                                    <th>Final Price</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Stem</td>
                                                    <td>$0.10</td>
                                                    <td>$0.10</td>
                                                    <td>$0.10</td>
                                                    <td>$0.10</td>
                                                    <td>$0.10</td>

                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <hr>
                                        
                                            
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default"
                                        data-dismiss="modal">Close
                                </button>
                                <button type="button"
                                        class="btn btn-primary save_d"
                                        style="background:#8a2b83!important;"
                                        data-dismiss="modal">Send Request
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Quantity Modal End-->
            </td>
        </tr>
        <?php
        $i++;
}
}
?>