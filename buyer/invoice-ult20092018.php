<!doctype html>

<?php

// PO 2018-08-24

require_once("../config/config_gcp.php");

//require_once("../functions.php");
//include "../Encryption.class.php";
//$key = "23c34eWrg56fSdrt"; // Encryption Key
//$crypt = new Encryption($key);


if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}
if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor-account.php");
    die;
}
$userSessionID = $_SESSION["buyer"];
$idfac = $_GET['idi'];

if (isset($_GET['sd']) && !empty($_GET['sd'])) {
    $id = $_GET['sd'];
    $qry = "DELETE FROM `buyer_shipping_methods` WHERE `id` = '$id'";


    echo '<script language="javascript">alert("' . $id . '");</script>';
    if (mysqli_query($con, $qry)) {
        header("location:" . SITE_URL . "buyer/buyers-account.php");
        die;
    }
}
if ($_POST['boton'] == 'Send Message') {
    $affair = $_POST['af'];
    $message = $_POST['area'];/*info@freshlifefloral.com*/
    $suer_data = "select (concat_ws(' ',last_name,first_name)) as name ,email,phone from  buyers WHERE id= " . $userSessionID;
    $user_res = mysqli_query($con, $suer_data);
    $user = mysqli_fetch_array($user_res);
    $message = "Hi FreshLifeFloral Admin ,  Buyer:" . $user['name'] . "  Email: " . $user['email'] . " Phone:" . $user['phone'] . "  Mesage : ";
    $message .= $_POST['area'];
    mail("info@freshlifefloral.com", $affair, $message);

}
function time_elapsed_string($ptime)
{
    $etime = $ptime - time();

    if ($etime < 1) {
        return '0 seconds';
    }

    $a = array(365 * 24 * 60 * 60 => 'year',
        30 * 24 * 60 * 60 => 'month',
        7 * 24 * 60 * 60 => 'week',
        24 * 60 * 60 => 'day',
        60 * 60 => 'hour',
        60 => 'minute',
        1 => 'second'
    );
    $a_plural = array('year' => 'years',
        'month' => 'months',
        'week' => 'weeks',
        'day' => 'days',
        'hour' => 'hours',
        'minute' => 'minutes',
        'second' => 'seconds'
    );

    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' left';
        }
    }
}


//update facturas 0  submit facturas
if (isset($_POST['update_factura_0']) ) {

    $no_error_1     = 1;
    
    $id_fact        = $_POST['id_fact'];
    $per_kg         = $_POST['per_kg'];
    
    $volume_weight  = $_POST['volume_weight'];
    $gross_weight   = $_POST['gross_weight'];    

    $total_boxes       = $_POST['total_boxes'];
    $subtotalCal       = $_POST['totalCal'];
    
    $air_waybill       = $_POST['air_waybill'];        
    $charges_due_agent = $_POST['charges_due_agent'];            
    
    $air_waybill_adm       = $_POST['air_waybill_adm'];        
    $charges_due_agent_adm = $_POST['charges_due_agent_adm'];                
    

  //  if (empty($coordination)) {
  //      $update_error_1 = "Please fill Coordination";
  //      $no_error_1 = 0;
  //  }


    if ($no_error_1 == 1) {
        
      //  $freight_value = $gross_weight * 2.55;
        
        $freight_value = $gross_weight * $per_kg;     
        
        $handling         = ( ($subtotalCal*(9/100))   + ($freight_value*(8/100))   + ($air_waybill*(8/100))   + ($charges_due_agent*(8/100)) );
        $credit_card_fees = ( ($subtotalCal*(2.9/100)) + ($freight_value*(2.9/100)) + ($air_waybill*(2.9/100)) + ($charges_due_agent*(2.9/100)) );        
        $grand_total      = $subtotalCal + $freight_value + $air_waybill + $charges_due_agent + $handling + $credit_card_fees;
            

        $handling_adm         = ( ($subtotalCal*(9/100))   + ($freight_value*(8/100))   + ($air_waybill_adm*(8/100))   + ($charges_due_agent_adm*(8/100)) );
        $credit_card_fees_adm = ( ($subtotalCal*(2.9/100)) + ($freight_value*(2.9/100)) + ($air_waybill_adm*(2.9/100)) + ($charges_due_agent_adm*(2.9/100)) );        
        $grand_total_adm      = $subtotalCal + $freight_value + $air_waybill_adm + $charges_due_agent_adm + $handling_adm + $credit_card_fees_adm;
        
        
            //  Tiene prioridad el ADMIN
        
                     mysqli_query($con, "UPDATE invoice_orders 
                                            SET volume_weight     ='" . $volume_weight . "',    
                                                gross_weight      ='" . $gross_weight . "' , 
                                                total_boxes       ='" . $total_boxes . "' ,                                        
                                                freight_value     ='" . $freight_value . "' ,                                                                               
                                                air_waybill       ='" . $air_waybill . "' ,                                                                                                                      
                                                charges_due_agent ='" . $charges_due_agent . "' , 
                                                handling          ='" . $handling_adm . "' ,                                       
                                                credit_card_fees  ='" . $credit_card_fees_adm . "' ,                                                                              
                                                grand_total       ='" . $grand_total_adm . "' ,                                                                                                                     
                                                bill_state        = 'F'  
                                          WHERE id_fact='" . $id_fact . "' ");   
        
        if ($grand_total > 0) {
                    mysqli_query($con, "UPDATE invoice_orders 
                                           SET volume_weight     ='" . $volume_weight . "',    
                                               gross_weight      ='" . $gross_weight . "' , 
                                               total_boxes       ='" . $total_boxes . "' ,                                        
                                               freight_value     ='" . $freight_value . "' ,                                                                               
                                               air_waybill       ='" . $air_waybill . "' ,                                                                                                                      
                                               charges_due_agent ='" . $charges_due_agent . "' , 
                                               handling          ='" . $handling . "' ,                                       
                                               credit_card_fees  ='" . $credit_card_fees . "' ,                                                                              
                                               grand_total       ='" . $grand_total . "' ,                                                                                                                     
                                               bill_state        = 'F'  
                                         WHERE id_fact='" . $id_fact . "' ");   
        
        }
        
     
         echo'<script>window.location="'. SITE_URL . 'buyer/buyer_invoices.php";</script>';
 end();
          
    }
}	
// End Submit Facturacion

//Delete Avatar
if (isset($_GET['delete_avatar']) && (!empty($_GET['id']))) {
    $user_id = $_GET['id'];
    if ($stmt = $con->prepare("SELECT id, profile_image FROM buyers WHERE id =$userSessionID")) {
        $stmt->bind_param('i', $user_id);
        $stmt->execute();
        $stmt->bind_result($userID, $profile_image);
        $stmt->fetch();
        $stmt->close();

        unlink('/images/profile_images/' . $profile_image);

        $stmt = $con->prepare("UPDATE buyers SET profile_image='' WHERE id=$userSessionID");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $stmt->close();
        mysqli_query($con, "UPDATE profile_completion SET status='0' WHERE buyer_id='" . $userSessionID . "' AND profile='profile-picture'");
        header("location:buyer/buyers-account.php?uam=1");
    }
}


/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,company_role,country,state_text,city,zip,shipping_country,shipping_state_text,shipping_city,
shipping_zip,coordination,address,address2,shipping_address,shipping_address2,name_on_card,credit_card_type,credit_card,cvv,expiration_month,
expiration_year,is_public,biographical_info,profile_image FROM buyers WHERE id =$userSessionID")
) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $company_role, $country, $state_text, $city, $zip, $shipping_country, $shipping_state_text, $shipping_city, $shipping_zip, $coordination, $address, $address2, $shipping_address, $shipping_address2, $name_on_card, $credit_card_type, $credit_card, $cvv, $expiration_month, $expiration_year, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    echo "else";
    exit();
    header("location:" . SITE_URL);
    die;
}
$img_url = '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}
$page_request = "dashboard";
?>
<?php require_once '../includes/profile-header.php'; ?>
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

    <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>

<style type="text/css">
    .comment-author .g_title {
        color: #337ab7;
        font-weight: normal;
    }
</style>
<style type="text/css">
    .comment-body.cls_comment_body {
        padding-left: 65px;
    }

    li.grower-comments .comment-body {
        padding-left: 0px;
    }

    .status_modal.in li.comment .comment-body {
        margin-left: 60px;
    }
</style>

<?php


   $buyerEntity = "select b.first_name,b.last_name,c.name from buyers  b , country c where b.id = '" . $userSessionID . "'  and c.id=b.country" ;

   $buyer = mysqli_query($con, $buyerEntity);
   $buy = mysqli_fetch_array($buyer);
   
   
   $buyerOrder = "select id_fact         , buyer_id         , order_number, 
                         order_date      , shipping_method  , del_date    , 
                         date_range      , is_pending       , order_serial, 
                         seen            , delivery_dates   , lfd_grower  , 
                         quick_desc      , bill_number      , gross_weight, 
                         volume_weight   , freight_value    , guide_number, 
                         total_boxes     , sub_total_amount , tax_rate    , 
                         shipping_charge , handling         , grand_total , 
                         bill_state      , date_added       , user_added  ,
                         air_waybill     , charges_due_agent,
                         credit_card_fees, per_kg                         
                         from invoice_orders
                   where buyer_id = '" . $userSessionID . "'
                     and id_fact = '" . $idfac . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab);
   
$id_fact_cab = $buyerOrderCab['id_fact'];
$buyer_cab   = $buyerOrderCab['buyer_id'];

   $sqlDetalis="select id_fact            , id_order       , order_serial  , 
                       cod_order          , product        , sizeid        , 
                       qty                , buyer          , date_added    , 
                       bunches            , box_name       , lfd           , 
                       comment            , box_id         , shpping_method, 
                       mreject            , bunch_size     , unseen        , 
                       inventary          , offer_id       , prod_name     , 
                       product_subcategory, size           , boxtype       , 
                       bunchsize          , boxqty         , bunchqty      , 
                       steams             , gorPrice       , box_weight    , 
                       box_volumn         , grower_box_name, reject        , 
                       reason             , coordination   , cargo         , 
                       color_id           , gprice         , tax           , 
                       cost_ship          , round(handling,0) as handling       , grower_id     , offer_id_index,
                       substr(rg.growers_name,1,19) as name_grower        
                  from invoice_requests ir
                 INNER JOIN growers rg     ON ir.grower_id = rg.id                  
                 where buyer    = '" . $buyer_cab . "'
                   and id_fact  = '" . $id_fact_cab . "' order by grower_id ";

    $result = mysqli_query($con, $sqlDetalis);     


 ?>

<form class="form-horizontal" id="profile_form" name="profile_form" method="post" enctype="multipart/form-data" action="invoice.php" >
    
	<body>
		
			<section id="middle">

				<!-- page title -->
				<header id="page-header">
					<h1>Invoice Page</h1>
					<ol class="breadcrumb">
						<li><a href="#">Pages</a></li>
						<li class="active">Invoice Page</li>
					</ol>
				</header>
				<!-- /page title -->


				<div id="content" class="padding-20">

					<div class="panel panel-default">
						<div class="panel-body">

							<div class="row">

								<div class="col-md-6 col-sm-6 text-left">

									<h4><strong>Client</strong> Details.</h4>
									<ul class="list-unstyled">
										<li><strong>First Name   :</strong><?php echo $buy['first_name']; ?></li>
										<li><strong>Last Name    :</strong><?php echo $buy['last_name']; ?> </li>
										<li><strong>Country      :</strong><?php echo $buy['name']; ?></li>
										<li><strong>Delivery Date:</strong> <?php echo $buyerOrderCab['del_date']; ?></li>
                                                                                
                                                                             
                                                                                
									</ul>

								</div>

								<div class="col-md-6 col-sm-6 text-right">
									<h4><strong>Shipping</strong> Details</h4>
									<ul class="list-unstyled">
										<li><strong>P.O. NUMBER:</strong><?php echo $buyerOrderCab['order_number']; ?></li>

                                                                                <li><strong>Total Boxes:</strong>
                                                                                    <input type="text" maxlength="6"  size="4"  id="total_boxes" name="total_boxes"  value="<?php echo $total_boxes; ?>">
                                                                                </li>
                                                                                
                                                                                <li><strong>Gross Weight:</strong>
                                                                                    <input type="text" maxlength="6"  size="4"  id="gross_weight" name="gross_weight"  value="<?php echo $gross_weight; ?>">
                                                                                </li>
                                                                                
                                                                                <li><strong>Volume Weight:</strong>
                                                                                    <input type="text" maxlength="6"  size="4"  id="volume_weight" name="volume_weight" value="<?php echo $volume_weight; ?>">
                                                                                </li>
                                                                                
                                                                                <li>                                                                                    
                                                                                    <div class="col-md-6 col-sm-6 text-right"></div>  
                                                                                </li>
									</ul>

								</div>

							</div>

							<div class="table-responsive">
								<table class="table table-condensed nomargin">
									<thead>
										<tr>
											<th>Variety</th>
											<th>Stem</th>
											<th>Price</th>
											<th>Qty</th>
											<th>Subtotal</th>
										</tr>
									</thead>
                                                                        
									<tbody>																					

									<?php $gname="A";
                                                                              while($row = mysqli_fetch_assoc($result))  {

                                                                            ?>
										<tr>
											<td>

                                                                            <?php
                                                                            if ($row['handling']== 1) {
                                                                                    $res = "";
                                                                                if ($row['boxtype'] == "HB") {
                                                                                    $res = "Half Boxes";
                                                                                } else if ($row['boxtype'] == "EB") {
                                                                                    $res = "Eight Boxes";
                                                                                } else if ($row['boxtype'] == "QB") {
                                                                                    $res = "Quarter Boxes";
                                                                                } else if ($row['boxtype'] == "JB") {
                                                                                    $res = "Jumbo Boxes";
                                                                                }
                                                                                ?>
										     <strong><?php echo $row['name_grower'] ?></strong>        <br>   
										   <!--  <strong><?php echo $row['boxqty']."  ".$res ?></strong> -->                                                                                      
                                                                            <?php }
                                                                            ?>                                                                                       
                                                                                     
												<div><?php echo $row['prod_name']." ".$row['size']." cm"." ".$row['steams']." st/bu"; ?></div>
												<small><?php echo $row['product_subcategory'] ?></small>
											</td>
                                                                                                                  
                                                                                                                                                                                
											<td><?php echo $row['steams'] ?></td>											
											<td><?php echo "$".number_format($row['gorPrice'], 2, '.', ',') ; ?></td>
											<td><?php echo $row['bunchqty'];?></td>
											    <?php $Subtotal= $row['steams'] *$row['bunchqty'] * $row['gorPrice']; ?>
											<td><?php echo "$".number_format($Subtotal, 2, '.', ',');?></td>
                                                                                        
										</tr>										
										
											<?php 

											$totalCal = $totalCal + $Subtotal; 
                                                                                        
                                                                                        $gname = $row['name_grower'];
                                                                              } ?>
									</tbody>
								</table>
							</div>

							<hr class="nomargin-top" />

							<div class="row">

								<div class="col-sm-6 text-left">
									<h4><strong>Contact</strong> Details</h4>

									<p class="nomargin nopadding">
										<strong>Note:</strong> 
										<?php echo $buyerOrderCab['quick_desc']; ?>
									</p><br><!-- no P margin for printing - use <br> instead -->

									<address>
                                                                                Av. Interoceanica OE6-73 y Gonzalez Suarez<br>
                                                                                Quito, Ecuador<br>
                                                                                Phone: +593 602 2630<br>
                                                                                Email:info@freshlifefloral.com                                                                                
									</address>

								</div>
                                                            
								<div class="col-sm-6 text-right">
									<ul class="list-unstyled">
                                                                            <li>
                                                                                <strong>Sub - Total Amount:         </strong> <?php echo "$".number_format($totalCal, 2, '.', ',');  ?>
                                                                                <input type="hidden" maxlength="9"  size="6"  id="totalCal" name="totalCal" value="<?php echo $totalCal; ?>">
                                                                                <input type="hidden" maxlength="9"  size="6"  id="per_kg" name="per_kg" value="<?php echo $buyerOrderCab['per_kg']; ?>">                                                                                
                                                                                <input type="hidden" maxlength="9"  size="8"  id="air_waybill_adm"       name="air_waybill_adm"       value="<?php echo $buyerOrderCab['air_waybill']; ?>">
                                                                                <input type="hidden" maxlength="9"  size="8"  id="charges_due_agent_adm" name="charges_due_agent_adm" value="<?php echo $buyerOrderCab['charges_due_agent']; ?>">                                                                                
                                                                            </li>                                                                            
                                                                            
                                                                            <li><strong>Air Waybill AWC:</strong>
                                                                                    <input type="text" maxlength="9"  size="6"  id="air_waybill" name="air_waybill"  value="<?php echo $air_waybill; ?>">
                                                                            </li>    
                                                                            
                                                                            <li><strong>Charges Dues Agent AWA:</strong>
                                                                                    <input type="text" maxlength="9"  size="6"  id="charges_due_agent" name="charges_due_agent"  value="<?php echo $charges_due_agent; ?>">
                                                                            </li>                                                                                                                                                            
									</ul>     								
								</div>
                                                            
                                                                <div class="col-md-9 col-md-offset-3">
                                                                        <input type="hidden" name="id_fact" value="<?php echo $buyerOrderCab['id_fact']; ?>">
                                                                        <input type="hidden" name="update_factura_0" value="1">                                                                    
                                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                                        <button type="reset" class="btn btn-default">Reset</button>
                                                                </div>                                                            

							</div>

						</div>
					</div>

					

				</div>
			</section>
			<!-- /MIDDLE -->

		</div>



	
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
		<script type="text/javascript" src="assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="assets/js/app.js"></script>

	</body>
        </form>
</html>
