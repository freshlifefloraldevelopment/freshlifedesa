<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */

    // get the HTML
    ob_start();
    //include(dirname(__FILE__).'/res/exemple02.php');
    echo'Hello <b>vdfg</b> your invoice no.5 details are as below. <br/><br/><table style="border-collapse:collapse;font-size:11px; " cellpadding="5" cellpadding="5" border="1" width="900" >
							
							<tr><td colspan="6" style="padding:20px; font-size:16px; text-align:center;"> <img src="http://freshlifefloral.com/images/logo.png" />
							<br/>
							Camino Real y Callejon D. OE5-371
							<br/>
							<br/>
							<b>Barrio El Chilpesito / Quito - Ecuador</b>
							</td><td colspan="7" style="padding:20px;font-size:16px;">
							<table width="100%">
							
							<tr><td style="width:25%" align="left" ><b>Invoice No:</b>	</td> <td style="width:75%">5</td></tr>
							<tr><td style="width:25%" align="left" ><b>DATE:</b>	</td> <td style="width:75%">08-23-2014</td></tr>
							<tr><td style="width:25%" align="left" ><b>CLIENT:</b></td> <td style="width:75%">vdfg</td> </tr>
							<tr><td style="width:25%" align="left" ><b>ADDRESS:</b></td> <td style="width:75%">this is address</td></tr>
							<tr><td style="width:25%" align="left" ><b>PHONE:</b></td> <td style="width:75%">8980069189</td> </tr>
							<tr><td style="width:25%" align="left" ><b>Email:</b></td> <td style="width:50%">adhiyahiren108@gmail.com</td> </tr>
							</table>
							</td></tr>
							  <tr>
							 
							 <td width="25" align="center" > No of Boxes </td>
							 
							<td  width="25" align="center"> Boxes </td>
							<td  width="25" align="center"> Box Type </td>
							<td width="50" align="center"> Varieties </td>
							<td width="25" align="center" > CM </td>
							<td width="25" align="center"> Bunch / box </td>
							<td width="60" align="center"> Total Bunches </td>
							<td width="25" align="center"> S/B </td>
							<td width="25" align="center"> Total Stems </td>
							<td width="25" align="center">  Unit Price  </td>
							<td width="25" align="center">  Total  </td>
							<td width="50" align="center"> Box Label </td>
							<td width="60" align="center"> Growers </td> </tr><tr><td >1 - 1</td><td >1</td><td    >HB</td><td    >3D </td><td    >60 </td><td    >6 </td><td    >6 </td><td    >25 </td><td    >150 </td><td    >1.11</td><td    >166.5</td><td   >vdfg</td><td    >FLORANA FARMS S.A.</td></tr><tr><td   rowspan=1 >2 - 2</td><td  rowspan=1>1</td><td  rowspan=1 >QB</td><td  >3D  </td><td  >60 </td><td  >2  </td><td  >2 </td><td  >25 </td><td  >50</td><td  >1.24</td><td  >62</td><td  rowspan=1 >vdfg</td><td  rowspan=1 >FLORANA FARMS S.A.</td></tr><tr><td >3 - 5</td><td >3</td><td    >HB</td><td    >Absurda </td><td    >40 </td><td    >8 </td><td    >24 </td><td    >25 </td><td    >600 </td><td    >1.2</td><td    >720</td><td   >vdfg</td><td    >FLORANA FARMS S.A.</td></tr><tr><td  colspan=6 align=right> <b>Total</b></td><td ><b>32</b></td><td ></td><td ><b>800</b></td><td ><b>1.186</b></td><td ><b>948.5</b></td><td ></td><td ></td></tr></table><br/><table width=300 border=1 style="border-collapse:collapse;" ><tr>
						 <td>Half Box</td> <td>4</td></tr>
						 <tr> <td>Quater Box</td> <td>1</td></tr>
						  <tr> <td>Total Pieces </td> <td>5</td></tr>
						    <tr> <td>Full Box </td> <td>2.25</td>
						 </tr></table><br/><br/>
						 
						 <p>All quality problems must be reported in writing and sending pictures within 48 hours after receipt of											
	 the product. No claims for freight will be accepted. We will not be responsible for shipping problems,											
	ACCEPTANCE OF THIS SHIPMENT CONSTITUES AGREEMENT TO ALL OF THE ABOVE TERMS AND 											
	CONDITIONS. If this invoice is not cancelled by its due date, our company will charge you a											
	legal fee as well as the cost.</p><br/><br/>Please click on following link to login. <br/><a href="http://freshlifefloral.com/">http://freshlifefloral.com/</a><br/><br/>Thanks!<br/><b>Freshlife Floral Team</b>';
	$content = ob_get_clean();

    // convert in PDF
    require_once(dirname(__FILE__).'/../html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', array(15, 5, 15, 5));
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('exemple02.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }

