<?php
require_once("../config/config_gcp.php");
// PO 2018-07-02

$menuoff = 1;
$page_id = 5;
$message = 0;

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../imagenes/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../imagenes/profile_images/' . $profile_image;
}
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

if ($info["active"] != "demo" && $info["active"] != "suspended") {
    if (isset($_GET["offerid"]) && isset($_GET["replyid"])) {
        //print 'dfhdfh';
        $update = "update grower_offer_reply set status=1,accept_date='" . date("Y-m-d") . "' where id='" . $_GET["replyid"] . "' and offer_id='" . $_GET["offerid"] . "'";
        mysqli_query($con, $update);

        $sel_grower = "select grower_id from grower_offer_reply where id='" . $_GET["replyid"] . "' and offer_id='" . $_GET["offerid"] . "'";
        $rs_grower = mysqli_query($con, $sel_grower);
        $grower = mysqli_fetch_array($rs_grower);

        $sel_mail_email = "select contact1email,growers_name from growers where id='" . $grower["grower_id"] . "'";
        $rs_mail_email = mysqli_query($con, $sel_mail_email);
        $mail_email = mysqli_fetch_array($rs_mail_email);
        $email_address = $mail_email["contact1email"];

        $er = "select gpb.id as cartid,gpb.type,gpb.lfd,gpb.noofstems,gpb.bunches,gpb.qty,gpb.boxtype,p.id,p.name,s.name as subs,sh.name as sizename,
                      ff.name as featurename 
                 from buyer_requests gpb
                 left join product p     on gpb.product     = p.id
		 left join subcategory s on p.subcategoryid = s.id  
		 left join features ff   on gpb.feature     = ff.id
		 left join sizes sh      on gpb.sizeid      = sh.id 
                where gpb.id='" . $_GET["offerid"] . "' ";
        
        $ers = mysqli_query($con, $er);

        while ($er5 = mysqli_fetch_array($ers)) {

            $text .= "You proposal for following offer has been accepted. <br/> <br/>";
            if ($er5["type"] != "3") {

                if ($er5["type"] == 2) {
                    $text .= "Name Your Price - &nbsp;&nbsp;";
                }

                if ($er5["type"] == 0) {
                    $text .= "Request for Product Quote - &nbsp;&nbsp;";
                }


                $text .= "For ";
                $temp_lfd = explode("-", $er5["lfd"]);
                $text .= $temp_lfd[1] . "-" . $temp_lfd[2] . "-" . $temp_lfd[0];
                $text .= " - " . $er5["qty"] . " ";
                $temp = explode("-", $er5["boxtype"]);
                $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                $rs_box_type = mysqli_query($con, $sel_box_type);
                $box_type = mysqli_fetch_array($rs_box_type);
                $text .= $box_type["name"] . " Box(es)";
                $text .= "<br/>";

                $text .= $er5["subs"] . " - " . $er5["name"] . " - ";
                if ($er5["featurename"] != "") {
                    $text .= $er5["featurename"] . " - ";
                }

                $text .= $er5["sizename"] . " cm ";

                $text .= "For ";
                $temp_lfd = explode("-", $er5["lfd"]);
                $text .= $temp_lfd[1] . "-" . $temp_lfd[2] . "-" . $temp_lfd[0];

                if ($er5["noofstems"] > 0) {
                    $text .= " - " . $er5["noofstems"] . " Stems";
                } else {
                    $text .= " - " . $er5["qty"] . " ";

                    $temp = explode("-", $er5["boxtype"]);
                    $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                    $rs_box_type = mysqli_query($con, $sel_box_type);
                    $box_type = mysqli_fetch_array($rs_box_type);
                    $text .= $box_type["name"] . " Box(es)";
                }
            } else {
                $text .= "By the Bunch - &nbsp;&nbsp;";

                $text .= "For ";
                $temp_lfd = explode("-", $er5["lfd"]);
                $text .= $temp_lfd[1] . "-" . $temp_lfd[2] . "-" . $temp_lfd[0];

                $text .= " - " . $er5["qty"] . " ";

                $temp = explode("-", $er5["boxtype"]);
                $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                $rs_box_type = mysqli_query($con, $sel_box_type);
                $box_type = mysqli_fetch_array($rs_box_type);
                $text .= $box_type["name"] . " Box(es)";

                $text .= "<br/>";
                $text .= "Selected Bunch(es)<br/><br/>";


                $templ = explode(",", $er5["bunches"]);
                $count = sizeof($templ);
                for ($io = 0; $io <= $count - 2; $io++) {
                    $r = explode(":", $templ[$io]);
                    $query2 = "select gpb.growerid,gpb.prodcutid,gpb.sizeid,gpb.feature ,p.name as productname,s.name as subs,sh.name as sizename,ff.name as featurename,bs.name as bname from grower_product_box_packing gpb
				  left join product p on gpb.prodcutid = p.id
				   left join subcategory s on p.subcategoryid=s.id  
				  left join features ff on gpb.feature=ff.id
				  left join sizes sh on gpb.sizeid=sh.id 
				  left join bunch_sizes bs on gpb.bunch_size_id=bs.id
				  where gpb.id='" . $r[0] . "'";
                    $rs2 = mysqli_query($con, $query2);
                    while ($product_box = mysqli_fetch_array($rs2)) {

                        $text .= '&nbsp;' . $product_box["subs"] . '&nbsp;&nbsp;  
				  ' . $product_box["productname"] . ' ' . $product_box["featurename"] . ' &nbsp;&nbsp; - ' . $product_box["sizename"] . ' cm &nbsp;&nbsp; - ' . $product_box["bname"] . ' Stems &nbsp;&nbsp;-' . $r[1] . ' Bunch(es) &nbsp;
						  <br/>';
                    }
                }

                $text .= '<br/>';

                $text .= "<br/><br/>";
            }

            $text .= "<br/>";
        }

        $to = $email_address;
        $subject = "Your proposal has been accepted at freshlifefloral.com ";

        $body .= "Hello <b>" . $mail_email["growers_name"] . "</b><br/><br/>";

        $body .= $text;

        $body .= "<br/><br/>Please click on following link to login to view proposal.<br/>";

        $body .= "<a href='http://staging.freshlifefloral.com/'>http://staging.freshlifefloral.com/</a><br/><br/>";

        $body .= "Thanks!";

        $body .= "<br/><b>Freshlife Floral Team<b>";

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= "From:info@freshlifefloral.com" . "\r\n";
        //mail($to, $subject, $body, $headers);
        header("location:" . SITE_URL . "my-offers.php");
    }
}

$today = date("m-d-Y H:i:s", strtotime("+2 hours"));
$today1 = explode(" ", $today);
$today_date = $today1["0"];
$time = $today1["1"];
$hours_array = explode(":", $time);

if ($hours_array[0] <= 12) {
    if ($info["live_days"] == 0) {
        $shpping_on = date("Y-m-d");
        $shpping_onr = $shpping_on;
        $tempk = explode("-", $shpping_on);
        $shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
    } else {
        $shpping_on = date('Y-m-j', strtotime($info["live_days"] . ' weekdays'));
        $shpping_onr = $shpping_on;
        $tempk = explode("-", $shpping_on);
        $shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
    }
} else {
    if ($info["live_days"] == 0) {
        $shpping_on = date('Y-m-j', strtotime('1 weekdays'));
        $shpping_onr = $shpping_on;
        $tempk = explode("-", $shpping_on);
        $shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
    } else {
        $addone = $info["live_days"] + 1;
        $shpping_on = date('Y-m-j', strtotime($addon . ' weekdays'));
        $shpping_onr = $shpping_on;
        $tempk = explode("-", $shpping_on);
        $shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
    }
}
$page_request = "growers_offers";
function box_type($box)
{
    $box_type_s = "";
    if ($box == "0") {
        $box_type_s = "Stems";
    } else if ($box == "1") {
        $box_type_s = "Bunch";
    }
    return $box_type_s;

}

function box_type_name($id_box_type){
    global $con;
   // $sel_box_type = "select * from boxtype where id='" . $id_box_type . "'";
    
    $sel_box_type = "select code as name from units where id='" . $id_box_type . "'";
    $rs_box_type = mysqli_query($con, $sel_box_type);
    $box_type = mysqli_fetch_array($rs_box_type);
    return $box_type["name"];
}

function box_descrip($id_box_type){
    global $con;    
    $sel_unit_type = "select descrip from units where id='" . $id_box_type . "'";
    $rs_unit_type = mysqli_query($con, $sel_unit_type);
    $box_descrip_type = mysqli_fetch_array($rs_unit_type);
    return $box_descrip_type["descrip"];
}

function box_name($box_type)
{
    //$box_type["name"]
    $res = "";
    if ($box_type == "HB") {
        $res = "half boxes";
    } else if ($box_type == "EB") {
        $res = "eight boxes";
    } else if ($box_type == "QB") {
        $res = "quarter boxes";
    } else if ($box_type == "JB") {
        $res = "jumbo boxes";
    }
    return $res;

}

function query_main($userSessionID)
{
    global $con;
    $query2 = "select gpb.*, gpb.id as cartid,gpb.date_added ,p.id,p.name,p.color_id,p.image_path,p.box_type,s.name as subs,sh.name as sizename,
                      ff.name as featurename,
                      (select substr(qucik_desc,1,100) from buyer_orders where id = gpb.id_order) numped ,
	              (select del_date from buyer_orders where id=gpb.id_order) del_date,
                      (select shipping_code from buyer_shipping_methods where buyer_id = gpb.buyer and shipping_method_id = gpb.shpping_method) shipping_code
		from buyer_requests gpb
		left join product p     on gpb.product     = p.id
		left join subcategory s on p.subcategoryid = s.id  
		left join features ff   on gpb.feature     = ff.id
		left join sizes sh      on gpb.sizeid      = sh.id 
	       where gpb.buyer='" . $userSessionID . "' 
                 and gpb.isy = '0' 
               order by gpb.id desc ";
    
    // and gpb.lfd>='" . date("Y-m-d") . "' order by gpb.id desc ";  Pedido por Eduardo
    // 	
    //and( gpb.type!=1 and gpb.type!=7 and gpb.type!=5 )
    //echo $query2;
    $result2 = mysqli_query($con, $query2);
    return $result2;

}


?>
<?php include("../includes/profile-header.php"); ?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>
<?php include("../includes/left_sidebar_buyer.php"); ?>
<style type="text/css">
    .not_set_border td {
        border: none !important;
    }

</style>
<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Growers Offers</h1>
        <ol class="breadcrumb">
            <li><a href="#">Offers</a></li>
            <li class="active">Products</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>Offers</strong> <!-- panel title -->
                </span>

                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a>
                    </li>
                </ul>
                <!-- /right options -->
            </div>            
            
            <!-- panel content inicio po -->
            <div class="panel-body">
                                    <div id='loading'>
                                        <div class=" cssload-spin-box">
                                         <!--   <img src="<?php echo SITE_URL; ?>../includes/assets/images/loaders/5.gif"> -->
                                        </div>
                                    </div>                
                
                
                <div  class="dataRequest">

                                        <?php
                                            $i = 1;
                                            $result2 = query_main($userSessionID);
                                            $tp = mysqli_num_rows($result2);
                                        ?>
                                        <input type="hidden" name="tp" id="tp" value="<?= $tp ?>">
                                        <input type="hidden" name="current" id="current" value="">
                                        <input type="hidden" name="id_user" id="id_user" value="<?= $userSessionID ?>">                                        
                                        
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                        <tr>
                            <th class="width-30">PRODUCT</th>
                            <th>ORDER</th>
                            <th>REQUEST</th>
                            <th>MISSING</th>
                            <th>% FULLFILLED</th>                            
                            <th>DATE</th>
                            <th>P.O. NUMBER</th>
                            <th>OFFERS</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                       // po $i = 1;
                       // po  $result2 = query_main($userSessionID);
                       //  $tp = mysqli_num_rows($result2);
                        
                        if ($tp >= 1) {
                            while ($producs = mysqli_fetch_array($result2)) {
                                $sel_check = "select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,
                                                     cr.flag , (gr.bunchqty*gr.steams) as totstems
                                                from grower_offer_reply gr 
                                                left join growers g  on gr.grower_id = g.id 
                                                left join country cr on g.country_id = cr.id
                                               where gr.request_id='" . $producs["cartid"] . "' 
                                                 and gr.buyer_id='" . $userSessionID . "' 
                                                 and reject in (0) 
                                                 and g.id is not NULL 
                                               order by gr.grower_id , gr.offer_id_index";

                                $rs_growers  = mysqli_query($con, $sel_check);
                                $rs_growers1 = mysqli_query($con, $sel_check);
                                $totalio = mysqli_num_rows($rs_growers);

                                $sel_id = " select count(*)  as ct from (  select distinct offer_id_index,grower_id  as ct from grower_offer_reply 
                                             where reject in (0) and  request_id ='" . $producs["cartid"] . " '
                                             group by  offer_id_index,grower_id) as conteo";

                                $rs_sel_id = mysqli_query($con, $sel_id);
                                $fila = mysqli_fetch_assoc($rs_sel_id);
                                
                                $box_type_name = "";
                                
                                if ($producs["boxtype"] != "") {
                                    $temp = explode("-", $producs["boxtype"]);
                                    $box_type_name = box_type_name($temp[0]);
                                    $box_descrip = box_descrip($temp[0]);                                    
                                }
                                /***************Process count boxes required************************/
                                $bq = 0;
                                $bunst = 0;
                                
                                while ($growers_1 = mysqli_fetch_assoc($rs_growers1)) {
                                    if ($growers_1['status'] == 1) {
                                        $bq    += $growers_1['boxqty'] ;
                                        $bunst += $growers_1['totstems'] ;
                                        $type_unit = $growers_1['unit'] ;
                                    }
                                }
                                

                              /*
                                if ($type_unit == 'ST') {                                                                    
                                    $cant_required = ($producs["qty"] - $bunst)." " .$box_type_name;                                                               
                                    $cumpli        =(($producs["qty"] - $bunst)/$producs["qty"])*100;                                
                                } else {
                                    $cant_required = ($producs["qty"] - $bq)." " .$box_type_name;                                                               
                                    $cumpli        =(($producs["qty"] - $bq)/$producs["qty"])*100;
                                }
                                */
                                
                                if ($type_unit == 'ST') {                                                                    
                                    $cant_required = ($bunst)." " .$box_type_name;                                                               
                                    $cumpli        =(($bunst)/$producs["qty"])*100;                                
                                } else {
                                    $cant_required = ($bq)." " .$box_type_name;                                                               
                                    $cumpli        =(($bq)/$producs["qty"])*100;
                                }                                
                                
                                
                                if ($cant_required < 0) {
                                    $cant_required = 0;
                                }
                                /******************************************************************/
                                /**************************Process LDF***************************************/
                                $date_lftd = "";
                                if ($producs["lfd2"] == '0000-00-00') {
                                    $date_lftd = date("F j, Y", strtotime($producs["del_date"]));
                                } else {
                                    $date_lftd = date("F j, Y", strtotime($producs["del_date"])) . " ";
                                }
                                /****************************Close LFD***************************************/
                                ?>
                                <tr>
                                    <td class="text-center"><a data-toggle="modal" data-target=".flower_smal_<?php echo $i; ?>"><img src="<?php echo SITE_URL . $producs['image_path']; ?>" alt="" width="60"></a>
                                        <div class="modal fade flower_smal_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <!-- header modal -->
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="mySmallModalLabel"><?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] . " cm " ?></h4>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body">
                                                        <img src="<?php echo SITE_URL . $producs['image_path']; ?>" alt="" width="100%">
                                                    </div>
                                                    <h5>Fresh Life Floral</h5>

                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                                        <!-- Columnas Stems -->
                                                        <td width="200">
                                                            <a href="#" data-toggle="modal" data-target="#text_modal_1<?= $i; ?>"><?= $producs["subs"] ?> <?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] . " cm " ?></a>
                                                            <div class="modal fade" id="text_modal_1<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
                                                                 test="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            <h4 class="modal-title" id="mySmallModalLabel">Description Units</h4>
                                                                        </div>
                                                                        <div class="modal-body"><?= $box_descrip; ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>                                                                        
                                    
                                    <td><?= $producs["qty"] ?> <?= $box_type_name ?></td>
                                    <td><?= $cant_required ?></td>
                                    <td align="left"><?= round($cumpli,2)." %" ?></td>                                    
                                    <td><?= $date_lftd ?></td>
                                    
                                    
                                                        <!--Pedido-->
                                                        <td width="200">
                                                            <a href="#" data-toggle="modal" data-target="#text_modal_<?= $i; ?>"><?= $producs["cod_order"] ?></a>
                                                            <div class="modal fade" id="text_modal_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
                                                                 test="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            <h4 class="modal-title" id="mySmallModalLabel">Orden</h4>
                                                                        </div>
                                                                        <div class="modal-body"><?= $producs["numped"]; ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>                                    
                                    
                                    <td><a data-toggle="modal" data-target=".open_offer_modal<?= $i ?>" class="btn btn-success btn-xs relative">Offer<span
                                                    class="badge badge-dark badge-corner radius-0"><?= $totalio_gro . $fila['ct']; ?></span></a>
                                        <!--Offer Modal Start-->
                                        <div class="modal fade open_offer_modal<?= $i ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                                        <h4 class="modal-title" id="myLargeModalLabel"><?= $producs['qty'] . " " . $box_type["name"] ?>
                                                            Units <?= $producs["subs"] ?> <?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] ?> cm</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Grower</th>
                                                                    <th>Boxes offered</th>
                                                                    <th align="center"> Product Size</th>
                                                                    <th>Price</th>
                                                                    <th>Confirm/Status</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <form action="" method="post" id="add_to_cart_form" class="add_to_cart_form">
                                                                    <?php
                                                                    $al_ready_gro_in = array();
                                                                    $cn = 1;
                                                                    while ($growers = mysqli_fetch_assoc($rs_growers)) {
                                                                        $k = explode("/", $growers["file_path5"]);
                                                                        if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                            echo "<tr class=\"set_border\">";
                                                                        } else {
                                                                            echo "<tr class=\"not_set_border\">";
                                                                        }

                                                                        //-------------------------------------------------------------//
                                                                        $total_bunchs="";
                                                                        $boxes_type="";
                                                                        if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                            $getGrowerReply = "SELECT offer_id , bunchqty , boxqty 
                                                                                                 FROM grower_offer_reply 
                                                                                                where reject in (0) 
                                                                                                  and request_id  ='" . $producs['cartid'] . "'
                                                                                                  and grower_id ='" . $growers['grower_id'] . "' 
                                                                                                  and offer_id_index='" . $growers["offer_id_index"] . "' limit 1";
                                                                            
                                                                            $rs_getGrowerReply = mysqli_query($con, $getGrowerReply);
                                                                            $tot_bunch_qty = 0;
                                                                            while ($row_rs_getGrowerReply = mysqli_fetch_array($rs_getGrowerReply)) {
                                                                                $tot_bunch_qty = $tot_bunch_qty + $row_rs_getGrowerReply['boxqty'];
                                                                            }
                                                                            $total_bunchs= $tot_bunch_qty;
                                                                            if ($producs["boxtype"] != "") {
                                                                                $temp = explode("-", $producs["boxtype"]);
                                                                                $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                                                                                $rs_box_type = mysqli_query($con, $sel_box_type);
                                                                                $box_type = mysqli_fetch_array($rs_box_type);
                                                                                $type_box = box_name($box_type["name"]);
                                                                                $boxes_type= $type_box;
                                                                            }
                                                                        }
                                                                        //---------------------------------------------------------------------//

                                                                        //---------------------------------------------------------------------//
                                                                        $getbunchSize = "SELECT is_bunch_value,bunch_value,is_bunch  FROM grower_product_bunch_sizes WHERE grower_id = '" . $growers['grower_id'] . "' AND product_id = '" . $producs['product'] . "' 
                                                                            AND sizes = '" . $producs['sizeid'] . "'";
                                                                        $rs_bunchSize = mysqli_query($con, $getbunchSize);
                                                                        $row_bunchSize = mysqli_fetch_array($rs_bunchSize);
                                                                        $bunch_size_s = "";

                                                                        if ($row_bunchSize['bunch_value'] == 0) {
                                                                            $bunch_size_s = $row_bunchSize['is_bunch_value'];
                                                                        } else {
                                                                            //$bunch_size_s = $row_bunchSize['bunch_value'];
                                                                        }


                                                                        //---------------------------------------------------------------------//


                                                                        ?>
                                                                        <td>
                                                                            <?php
                                                                            if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                                ?>
                                                                                <img src="<?php echo SITE_URL; ?>user/logo/<?php echo $k[1]; ?>" width="65">
                                                                            <?php }
                                                                            ?>
                                                                        </td>
                                                                        
                                                                        <td><?=$total_bunchs.' '.$boxes_type ?></td>
                                                                        
                                                                            <?php
                                                                            if ($row_bunchSize['bunch_value'] == 0) {
                                                                                ?>                                                                                                                                                
                                                                                    <td><?php $box_type_s = box_type($producs['box_type']); ?>
                                                                                        <?= $growers["product"]. " " ?><?= $growers["size"] . " cm " ?><?= $growers["bunchqty"] . " Bunchess " ?><?= $bunch_size_s . " st/bu "; ?>
                                                                                    </td>                                                                        
                                                                            <?php }else{?>
                                                                                    <td><?php $box_type_s = box_type($producs['box_type']); ?>
                                                                                        <?= $growers["product"]. " " ?><?= $growers["size"] . " cm " ?><?= $growers["bunchqty"] . " Bunches " ?><?= $growers["steams"] . " st/bu "; ?>
                                                                                    </td>                                                                                                                                                        
                                                                            <?php }
                                                                            ?>                                                                        
                                                                        
                                                                        <td>$<?= $growers['price']; ?></td>
                                                                        <td>
                                                                            <input type="hidden" name="off-id-<?= $cn ?>" value="<?php echo $producs['cartid']; ?>"/>
                                                                            <input type="hidden" name="offer_id[]" value="<?php echo $producs['cartid']; ?>"/>
                                                                            <input type="hidden" name="grid_id[]" value="<?php echo $growers['grid']; ?>"/>
                                                                            <input type="hidden" name="grower_id[]" value="<?php echo $growers['grower_id']; ?>"/>
                                                                            <input type="hidden" name="grower_pic[]" value="<?php echo $growers['file_path5']; ?>"/>
                                                                            <input type="hidden" name="product_name[]" value="<?php echo $producs['name']; ?>"/>
                                                                            <input type="hidden" name="product_image[]" value="<?php echo $producs['image_path']; ?>"/>
                                                                            <input type="hidden" name="product_size[]" value="<?php echo $producs["sizename"]; ?>"/>
                                                                            <input type="hidden" name="bunch_size[]" value="<?php echo $bunch_size_s; ?>"/>
                                                                            <input type="hidden" name="box_qty[]" value="<?php echo $growers['boxqty']; ?>"/>
                                                                            <input type="hidden" name="box_type[]" value="<?php echo $growers['boxtype']; ?>"/>
                                                                            <input type="hidden" name="is_bunch[]" value="<?php echo $row_bunchSize['is_bunch']; ?>"/>
                                                                            <input type="hidden" name="product_price[]" value="<?php echo $growers['price']; ?>"/>
                                                                            <input type="hidden" name="lfd[]" value="<?php echo $producs["del_date"]; ?>"/>
                                                                            <input type="hidden" name="btn_add_to_cart" value="Submit"/>
                                                                            <?php
                                                                            //before if (!in_array($growers['grower_id'], $al_ready_offer)) {
                                                                            if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                                if ($growers["reject"] != 1) {
                                                                                    $tdate2 = date("Y-m-d");
                                                                                    $date12 = date_create($grower_offer["date_added"]);
                                                                                    $date22 = date_create($tdate2);
                                                                                    $interval = $date22->diff($date12);
                                                                                    $checka1 == $interval->format('%R%a');;
                                                                                    $expired = 0;
                                                                                    if ($checka1 <= 2) {
                                                                                        $sel_buyer_info = "select live_days from buyers where id='" . $grower_offer["buyer_id"] . "'";
                                                                                        $rs_buyer_info = mysqli_query($con, $sel_buyer_info);
                                                                                        $buyer_info = mysqli_fetch_array($rs_buyer_info);
                                                                                        if ($buyer_info["live_days"] > 0) {
                                                                                            $tdate = date("Y-m-d");
                                                                                            $tdate;
                                                                                            $date1 = date_create($producs['lfd']);
                                                                                            $date2 = date_create($tdate);
                                                                                            $interval = $date2->diff($date1);
                                                                                            $checka2 = $interval->format('%R%a');
                                                                                            if ($checka2 == 0) {
                                                                                                $expired = 1;
                                                                                            }
                                                                                            if ($checka2 >= $buyer_info["live_days"]) {
                                                                                                $expired = 0;
                                                                                            } else {
                                                                                                $expired = 1;
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        $expired = 1;
                                                                                    }
                                                                                    if ($growers["status"] > 0) {
                                                                                        ?>
                                                                                        <a href="javascript:void(0);" class="btn btn-success btn-xs relative" style="border:0px solid #fff; background-color: #d68910">Confirmed </a>
                                                                                        <?php

                                                                                    } else if ($growers["status"] == 0) {
                                                                                        if ($info["active"] == 'suspended') {
                                                                                            if ($expired == 0) {
                                                                                                ?>
                                                                                                <a href="<?php echo SITE_URL; ?>suspend.php?id=<?= $info["id"] ?>" class="btn btn-success btn-xs relative"
                                                                                                   style="border:0px solid #fff; background-color: #f4d03f ">Confirm</a>

                                                                                            <?php } else { ?>
                                                                                                <span style=" font-size:18px|bold;  display:block; color: #FF0000;">Bid closed</span>
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            if ($expired == 0) {
                                                                                                ?>
                                                                                                <input type="submit" name="btn_add_to_cart" class="btn btn-success btn-xs relative" style="border:0px solid #fff; " value="Confirm"
                                                                                                       onclick="add_to_cart(<?= $producs['cartid'] ?>,<?= $userSessionID ?>,<?= $growers['grower_id']; ?>,<?= $growers["offer_id_index"]; ?>,<?= $producs['id_order'] ?>,<?= $growers["req_group"]; ?> );"/>

                                                                                            <?php } else { ?>
                                                                                                <span style="  font-size:18px|bold; display:block; color: #FF0000;">Bid closed</span>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        ?>
                                                                                        <span style=" font-size:10px|normal;  display:block; color:#333">Accept Date</span>
                                                                                        <?php
                                                                                        $temp_offer_date = explode("-", $growers["accept_date"]);
                                                                                        $temp_offer = $temp_offer_date[1] . "-" . $temp_offer_date[2] . "-" . $temp_offer_date[0];
                                                                                        echo $temp_offer;
                                                                                    }
                                                                                } else {
                                                                                    ?>
                                                                                    <a class="btn btn-danger btn-xs relative" style="border:0px solid #fff;">Rejected : <?= $growers["reason"] ?></a>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </td>
                                                                        <?php array_push($al_ready_gro_in, $growers['grower_id'] . "-" . $growers["offer_id_index"]);

                                                                        $cn++;
                                                                        ?>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </form>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!--Offer Modal End-->
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            
                            
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>

<?php require_once '../includes/footer_new.php'; ?>

<script tpe="text/javascript">
    $(document).ready(function () {
        var iduser = $('#id_user').val();
        var productsearch = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '<?php echo SITE_URL; ?>/includes/autosuggest.php?limit=10&search=%QUERY',
                wildcard: '%QUERY'
            }
        });
        $('.autosuggest #typeSearch').typeahead(null, {
            name: 'productsearch',
            source: productsearch
        });
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/search_request_page_by_name_offers.php',
                data: 'name=' + suggestion + '&id_buyer=' + iduser,
                success: function (data) {
                    $("#pagination_nav").hide();
                    $('.imgDiv').hide();
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();

                }
            });
        });

        $body = $("body");
        function call_Ajax_Fliter() {
            jQuery("#btn_filter").click(function () {

                var check_value = 0;
                var data_ajax = "";
                if (jQuery("#filter_category").val() == "" && jQuery("#filter_variety").val() == "" && jQuery("#filter_color").val() == "" && jQuery("#filter_grower").val() == "" && jQuery("#filter_size").val() == "" && jQuery("#filter_pack").val() == "") {
                    check_value = 1;
                }
                if (check_value == 1) {
                    alert("Please select any one option.");
                }
                else {
                    var filter_category = jQuery("#filter_category").val();
                    var filter_variety = jQuery("#filter_variety").val();
                    var filter_color = jQuery("#filter_color").val();
                    var filter_grower = jQuery("#filter_grower").val();
                    var filter_size = jQuery("#filter_size").val();

                    var filter_category_text = jQuery("#filter_category option:selected").text();
                    var filter_variety_text = jQuery("#filter_variety option:selected").text();
                    var filter_color_text = jQuery("#filter_color option:selected").text();
                    var filter_grower_text = jQuery("#filter_grower option:selected").text();
                    var filter_size_text = jQuery("#filter_size option:selected").text();
                    
                    jQuery(".ajax_loader_s").css("display", "inline-block");
                    $.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>buyer/getFilterResults.php',
                        data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_color=' + filter_color + '&filter_grower=' + filter_grower + '&filter_size=' + filter_size,
                        success: function (data) {

                            jQuery("#tab_for_filter").html("");
                            $("#pagination_fresh").hide();
                            jQuery('.search_modal_open').modal('hide');
                            jQuery("#listing_product_s").html(data);
                            var pass_delete_f = "";
                            if (filter_category == "") {
                                var filter_category_temp = "0";
                            }
                            else {
                                var filter_category_temp = filter_category;
                            }
                            if (filter_variety == "") {
                                var filter_variety_temp = "0";
                            }
                            else {
                                var filter_variety_temp = filter_variety;
                            }
                            if (filter_color == "") {
                                var filter_color_temp = "0";
                            }
                            else {
                                var filter_color_temp = filter_color;
                            }
                            if (filter_grower == "") {
                                var filter_grower_temp = "0";
                            }
                            else {
                                var filter_grower_temp = filter_grower;
                            }
                            if (filter_size == "") {
                                var filter_size_temp = "0";
                            }
                            else {
                                var filter_size_temp = filter_size;
                            }
                            pass_delete_f += "'" + filter_category_text + "'" + ',' + filter_category_temp + ',';
                            pass_delete_f += "'" + filter_variety_text + "'" + ',' + filter_variety_temp + ',';
                            pass_delete_f += "'" + filter_color_text + "'" + ',' + filter_color_temp + ',';
                            pass_delete_f += "'" + filter_grower_text + "'" + ',' + filter_grower_temp + ',';
                            pass_delete_f += "'" + filter_size_text + "'" + ',' + filter_size_temp;
                            

                            if (filter_category != "") {
                                var pass_click_cate = "'" + filter_category_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_category_filter" cate_label="' + filter_category_text + '" name="hdn_selected_category_filter" value="' + filter_category + '" />' + filter_category_text);
                            }
                            if (filter_variety != "") {
                                var pass_click_cate = "'" + filter_variety_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_variety_filter" cate_label="' + filter_category_text + '" name="hdn_selected_variety_filter" value="' + filter_variety + '" />' + filter_variety_text);
                            }
                            if (filter_color != "") {
                                var pass_click_cate = "'" + filter_color_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_color_filter" cate_label="' + filter_category_text + '" name="hdn_selected_color_filter" value="' + filter_color + '" />' + filter_color_text);
                            }
                            if (filter_grower != "") {
                                var pass_click_cate = "'" + filter_grower_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_grower_filter" cate_label="' + filter_category_text + '" name="hdn_selected_grower_filter" value="' + filter_grower + '" />' + filter_grower_text);
                            }
                            if (filter_size != "") {
                                var pass_click_cate = "'" + filter_size_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_size_filter" cate_label="' + filter_category_text + '" name="hdn_selected_size_filter" value="' + filter_size + '" />' + filter_size_text);
                            }
                            jQuery("#tab_for_filter").show();

                            jQuery.ajax({
                                type: 'post',
                                url: '<?php echo SITE_URL; ?>buyer/getFilterPagination.php',
                                data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_color=' + filter_color + '&filter_grower=' + filter_grower + '&filter_size=' + filter_size,
                                success: function (data) {
                                    jQuery("#pagination_nav").html(data);
                                    jQuery(".ajax_loader_s").hide();
                                }
                            });
                        }
                    });
                }
            });
        }

        call_Ajax_Fliter();        
    });
</script>

<script>


    function send_offer(cartid, buyer, product, product_subcategory, sizename, price, qty) {
        var boxtype = $("#boxtype-" + cartid).val();
        var k = confirm('Are you sure you want send offer');
        if (k == true) {
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/grower/growers-send-offers-ajax-direct.php',
                data: 'offer=' + cartid + '&buyer=' + buyer + '&product=' + product + '&product_subcategory=' + product_subcategory + '&boxtype=' + boxtype + '&sizename' + sizename + '&price=' + price + '&qty=' + qty,
                success: function (data) {
                    alert('Offer has been sent.');
                    location.reload();
                }
            });
        }
    }
    
    function add_to_cart(offer, buyer, grower, index,id_order,request_group) {

        var k = confirm('You have completed your boxes, would you like to leave this request open to receive more offers?');
        if (k == true) { 
            alert('Confirmed Pre-Factura Requests'); 
            //console.log('uno');
            console.log("data " + "offer " + offer + "buyer " + buyer + "grower " + grower + " index " + index + " id_order " + id_order + " request_group " + request_group );
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/buyer/viaje.php',
                data: 'offer=' + offer + '&buyer=' + buyer + '&grower=' + grower + '&index=' + index + '&id_order=' + id_order + '&request_group=' + request_group,
                success: function (data) {
                    alert('Offer has been sent.');
                    location.reload();
                }   
            });
        }

    }

   
</script>
