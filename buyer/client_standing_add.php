<?php 
require_once("../config/config_gcp.php");



$userSessionID = $_SESSION["buyer"];
$ayear = $_GET['idyear'];
$aweek = $_GET['idweek'];
$ascli = $_GET['subcli'];

//update profile data
if (isset($_POST['update_profile_1']) ) {
    echo "Revisar";
    $no_error = 1;
    $update_error = '';

    $id = $_POST['id'];
    $first_name = $_POST['first_name'];
    
    
                            //product   = '".$_POST["product"]."',
                            //qty_pack  = '".$_POST["qty_pack"]."',
                            //size      = '".$_POST["size"]."' , 

       $insert="insert into standing_order_subclient set
                            id_year   = '".$ayear."'  ,
                            id_week   = '".$aweek."'  ,    
                            id_client = '".$ascli."'  ,                
                            product   = '".$_POST["product"]."' ,
                            prod_name = '0'   ,
                            qty_pack  = '".$_POST["qty_pack"]."',
                            size      = '".$_POST["size"]."' , 
                            steams    = '25'  ,    
                            id_state  = '0'   ,
                            comment = 'CALENDAR'    ";
       
	   mysqli_query($con,$insert);
           
	   header('location:client_standing.php?idwk='.$aweek.'&id_cli='.$ascli.'&idyr='.$ayear);           

}
//manage labels for form area 
if ($_SESSION["lang"] == "ru") {

} else {
    $labels_arr['variety'] = 'Variety';
    $labels_arr['bunches'] = 'Bunches';
    $labels_arr['size']    = 'Size';    
    $labels_arr['buyer']   = 'Buyer';
}
?>
<script>
    function validate_profile_form(theForm) {

        var error = 0;
        if (theForm.first_name.value == "" || theForm.first_name.value == "Full Name") {
            $('.error_first_name').html('Please fill the First Name');
            theForm.first_name.focus();
            $("#first_name").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }
        if (theForm.last_name.value == "" || theForm.last_name.value == "Last Name") {
            $('.error_last_name').html('Please fill the Last Name');
            theForm.last_name.focus();
            $("#last_name").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }
        if (theForm.phone.value == "" || theForm.phone.value == "Phone") {
            $('.error_phone').html('Please fill the Phone');
            theForm.phone.focus();
            $("#phone").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }

        if (error == 1) {
            return false;
        } else {
            return true;
        }
    }

    function remove_validate_text(id) {
        $('.error_' + id).html('');
        $("#" + id).removeAttr("style");
    }

</script>
<?php require_once '../includes/profile-header.php'; ?>
<?php require_once "../includes/left_sidebar_buyer.php"; ?>
<!-- /ASIDE -->

<!--
            MIDDLE
        -->
<section id="middle">
    <div id="content" class="padding-20">

        <div class="page-profile">
            <?php
            if ($update_msg) {
                echo '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>' . $update_msg . '</div>';
            } elseif ($update_error) {
                echo '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>' . $update_error . '</div>';
            }
            ?>
            <div class="row">



                <!-- COL 2 -->
                <div class="col-md-8 col-lg-6">

                    <div class="tabs white nomargin-top">
                        <ul class="nav nav-tabs tabs-primary">

                            <li class="active">
                                <a href="#edit" data-toggle="tab">Add</a>
                            </li>
                        </ul>

                        <div class="tab-content">


                            <?php 
                            $first = $name1[0];
                            $last_name = $name2[1]; ?>
                            <!-- Edit -->
                            <div id="edit" class="active tab-pane">

                                <form class="form-horizontal" id="profile_form" name="profile_form" method="post" enctype="multipart/form-data" onsubmit="javascript: return validate_profile_form(profile_form);">
                                    <h4>Information</h4>
                                    <fieldset>
                                        
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_first_name"></div>
                                            <div style="clear:both;"></div>

                                            <label class="col-md-3 control-label" for="product"><?php echo $labels_arr['variety']; ?></label>
                                            <div class="col-md-8">
                                                    <select style="width: 100%; diplay: none;" name="product" id="product" class="form-control select2 fancy-form-select" tabindex="-1">
                                                            <option value="">Select Product</option>    
                                                                  <?php      $sql_prod = "select id , name 
                                                                                           from product 
                                                                                          order by name";
                                                                                             
                                                                        $result_prod = mysqli_query($con, $sql_prod);
                                                                        while ($row_prod = mysqli_fetch_assoc($result_prod)) { ?>
                                                                              <option value="<?php echo $row_prod['id']; ?>"><?= $row_prod['name']; ?></option>
                                                                  <?php }  ?>
                                                    </select>                                                                        
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_first_name"></div>
                                            <div style="clear:both;"></div>

                                            <label class="col-md-3 control-label" for="size"><?php echo $labels_arr['size']; ?></label>
                                            <div class="col-md-8">
                                                    <select style="width: 100%; diplay: none;" name="size" id="size" class="form-control select2 fancy-form-select" tabindex="-1">
                                                            <option value="">Select Size</option>    
                                                                  <?php      $sql_size = "select id,cast(name as int) newsize from  sizes 
                                                                                                        order by newsize";
                                                                                             
                                                                        $result_size = mysqli_query($con, $sql_size);
                                                                        while ($row_size = mysqli_fetch_assoc($result_size)) { ?>
                                                                              <option value="<?php echo $row_size['id']; ?>"><?= $row_size['newsize']; ?></option>
                                                                  <?php }  ?>
                                                    </select>                                                                        
                                            </div>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_last_name"></div>
                                            <div style="clear:both;"></div>

                                            <label class="col-md-3 control-label" for="qty_pack"><?php echo $labels_arr['bunches']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="qty_pack" name="qty_pack" value="<?php echo $_POST["qty_pack"] ?>"  onkeypress="javascript:remove_validate_text('qty_pack');">
                                            </div>
                                        </div>



                                    </fieldset>


                                    <hr>



                                    <div class="row">
                                        <div class="col-md-9 col-md-offset-3">
                                            <input type="hidden" name="id" value="<?php echo $userID; ?>">
                                            <input type="hidden" name="update_profile_1" value="1">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>

                </div><!-- /COL 2 -->                

            </div>

        </div>

    </div>
</section>
<?php require_once("../includes/profile-footer.php"); ?>
