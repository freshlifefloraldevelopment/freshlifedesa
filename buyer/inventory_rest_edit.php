<?php

require_once("../config/config_gcp.php");


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}

$userSessionID = $_SESSION["buyer"];
$cliente_inv   = $_GET['id_cli'];
$swcab         = $_GET['sw'];
$idfac         = $_GET['fac_id'];


/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,company_role,country,state_text,city,zip,shipping_country,shipping_state_text,shipping_city,
shipping_zip,coordination,address,address2,shipping_address,shipping_address2,name_on_card,credit_card_type,credit_card,cvv,expiration_month,
expiration_year,is_public,biographical_info,profile_image FROM buyers WHERE id =$userSessionID")
) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $company_role, $country, $state_text, $city, $zip, $shipping_country, $shipping_state_text, $shipping_city, $shipping_zip, $coordination, $address, $address2, $shipping_address, $shipping_address2, $name_on_card, $credit_card_type, $credit_card, $cvv, $expiration_month, $expiration_year, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    echo "else";
    exit();
    header("location:" . SITE_URL);
    die;
}


//  Actualizacion en Grupo

if (isset($_REQUEST["total"])) {
    
    for ($i = 1; $i <= $_POST["totalrow"] - 1; $i++) {
              
            $update = "update buyer_requests 
                          set num_box = '" . $_POST["boxp-". $_POST["pro-" . $i]] . "' 
                        where id      = '" . $_POST["pro-" . $i] . "'  ";                               
    
            mysqli_query($con, $update);                                                            
    }    
  }    
      
      
$img_url = '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}
$page_request = "dashboard";
?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

    <!--script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script-->

    
<style type="text/css">
    .comment-author .g_title {
        color: #337ab7;
        font-weight: normal;
    }
</style>
<style type="text/css">
    .comment-body.cls_comment_body {
        padding-left: 65px;
    }

    li.grower-comments .comment-body {
        padding-left: 0px;
    }

    .status_modal.in li.comment .comment-body {
        margin-left: 60px;
    }
</style>

<?php         
    
    $sqlDetalis="select br.id as gid      , br.id_order         , br.order_serial   , br.cod_order        , br.product        ,
                        br.sizeid         , br.feature          , br.noofstems      , (gor.bunchqty-gor.reserve) as qty_pack  , br.buyer          ,
                        br.boxtype        , br.date_added       , br.type           , br.bunches          , br.box_name       ,
                        br.lfd            , br.lfd2             , br.comment        , br.box_id           , br.shpping_method ,
                        br.isy            , br.mreject          , br.bunch_size     , br.unseen           , br.req_qty        ,
                        br.bunches2       , br.discount         , br.inventary      , br.type_price       , br.id_client      ,
                        br.id_grower      , br.special_order    , br.price          , br.num_box          , br.date_ship      ,
                        br.date_del       , br.cost             , br.id_reser       , br.assign           ,
                        gor.product_subcategory as psubcatego , 
                        p.name as name_product ,
                        cli.name as subclient  ,  
                        gor.size as size_name   , 
                        c.name as colorname     
                   from buyer_requests br
                  inner join grower_offer_reply gor on gor.offer_id = br.id
                  inner join buyer_orders bo ON br.id_order = bo.id
                  inner join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name 
                  inner join growers g on gor.grower_id = g.id
                  inner join colors c on p.color_id = c.id
                  inner JOIN sub_client cli on br.id_client = cli.id 
                   left join features f on br.feature = f.id 
                   left JOIN buyer_requests res ON gor.request_id = res.id
                  where g.active = 'active' 
                    and br.buyer = '" . $userSessionID . "' 
                    and bo.assigned = 1     
                    and (gor.bunchqty-gor.reserve) > 0   
                    and (res.id_client = 0 or res.id_client is null)  
                  order by gor.product_subcategory  ";

    $result = mysqli_query($con, $sqlDetalis);  



 ?>
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
 
</head>
<section id="middle">
            <!-- panel content inicio po -->            
            <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">
			<!--section id="middle"-->

				<!-- page title -->
				<header id="page-header">
					<h1>Inventory </h1>
					<ol class="breadcrumb">
						<li><a href="#">Inventory</a></li>
						<li class="active">Rest</li>
					</ol>
				</header>
				<!-- /page title -->


				<div id="content" class="padding-20">

					<div class="panel panel-default">
            <div class="panel-heading">

                 <a href="<?php echo SITE_URL; ?>buyer/client_calendar_special.php" class="btn btn-success btn-xs relative">Back </a>                
                                 
                 
                 
                <!-- right options -->
                <ul class="options pull-right list-inline">
					<ul class="list-unstyled">

					</ul>  
                </ul>
                <!-- /right options -->
                
                                                        <!--Price Modal Start-->
                                                        <div class="modal fade price_modal"  role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                            
                                                            <div class="modal-dialog modal-lg">
                                                                
                                                                <div class="modal-content">                                                                                                                                        
                                                                                                                                                                                                                                                                                                                                                   
                                                                    
                                                                    
                                                                    <div class="modal-body request_product_modal_hide">

                                                                        </label>

                                                                        <!--<br>-->
                                                                        <!--</div>-->
                                                                        <div class="row margin-bottom-10">
                                                                            <div class="col-md-12">
                                                                                <h4 style="clear: both;margin-top: 20px;margin-left: 14px;margin-bottom: 0px;">Select Product</h4>
                                                                                <div class="product_price_add2"></div>
                                                                                <span id="add_product_section1" style="display: inherit!important;">
                                                                                    
                                                                                    
                                                                                    <!--Select  Box Type (2) -->
                                                                                    <div class="col-md-6 margin-top-10">

                                                                                        <div class="fancy-form fancy-form-select">
                                                                                        <select style="width: 100%; diplay: none;" name="filter_category" id="box_type_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value="">Select product</option>                                                                                            
                                                                                            <?php
                                                                                            $sql_unit = "select * from  units where id = 1000";
                                                                                            $result_units = mysqli_query($con, $sql_unit);
                                                                                            while ($row_category = mysqli_fetch_assoc($result_units)) { ?>
                                                                                                <option value="<?php echo $row_category['id']; ?>"><?= $row_category['descrip']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>
                                                                                        <i class="fancy-arrow"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                     <!--Select  Order (3) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        

                                                                                        <div class="fancy-form fancy-form-select">
                                                                                            <select style="width: 100%; diplay: none;" name="filter_order" id="box_order_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1"    >
                                                                                            <option value="">Select order</option>
                                                                                                <?php
                                                                                                $category_sql = "select  id,qucik_desc  from  buyer_orders where  buyer_id='" . $userSessionID . "' and  del_date >= '" . date("Y-m-d") . "' and is_pending=11";
                                                                                                $result_category = mysqli_query($con, $category_sql);
                                                                                                while ($row_category = mysqli_fetch_assoc($result_category)) { ?>
                                                                                                    <option value="<?php echo $row_category['id']; ?>"><?= $row_category['qucik_desc']; ?></option>
                                                                                                <?php }
                                                                                                ?>
                                                                                        </select>
                                                                                            
                                                                                            
                                                                                        <i class="fancy-arrow"></i>
                                                                                             <input type="hidden" id="ship" name="ship" value="">
                                                                                             <div id="erMsg" style="display:none;color:red;">Please select Order.</div>
                                                                                        </div>
                                                                                    </div>
                                                                                                                                                                        

                                                                        
                                                                            </div>
                                                                        </div>
                                                                                                                                                                                                                                                                                                
                                                                        <!-- Comment (6) -->
                                                                        <div class="col-md-12">
                                                                            <hr>
                                                                            <form class="validate" action="" method="post" enctype="multipart/form-data" novalidate="novalidate">
                                                                                <fieldset>
                                                                                    <div class="row">
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-12 col-sm-12">
                                                                                                <label>Comment</label>
                                                                                                <textarea id="comment_<?php echo $ir; ?>" name="contact[experience]" rows="4" class="form-control required"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </fieldset>
                                                                                <!--input name="is_ajax" value="true" type="hidden"></form-->                                                                                                                                                                 
                                                                                                                                                                
                                                                     </div>                                                                                                                                                
                                                                        
                                                                        
                                                                    </div>
                                                                    
                                                                    <hr>
                                                                    <div class="modal-footer request_product_modal_hide_footer">
                                                                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                                                        <button style="background:#8a2b83!important;" onclick="requestProduct_xc('<?php echo $ir ?>')" class="btn btn-primary" type="button">Save</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--Price Modal End-->
                
                
            </div>                                            

						<div class="panel-body">

							<div class="row">                                                                             
								<div class="col-md-6 col-sm-6 text-left">

								
								<ul class="list-unstyled">
                                                                                                                                       
                            

                                    
                                         <input type="hidden" name="sname" id="sname" value="<?php echo $_POST["sname"];?>" /> 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
                                                                        
                                         <input type="hidden" name="qty_ped" id="qty_ped"  style="margin-top:10px; height:35px; padding:3px; width:250px;border-radius: 5px;" value="<?php echo $_POST["qty_ped"];?>" />                                                
                                                                        

								</ul>
                                                                    
								</div>
                                                            
                                                            
								<div class="col-md-6 col-sm-6 text-right">
                                                                    <ul class="list-unstyled">

                                                                     <li><strong> </strong>  
                                                                         
                                                                                 <div class="error-box">
                                                                                        <span id="errormsg-" style="color: white;"></span>
                                                                                 </div>
                                                                         
                                                                            
                                                                            <!--img id="my_image" src="https://app.freshlifefloral.com//includes/assets/images/logo.png" width="200"-->
                                                                                                                                                     
                                                                              
                                                                            <!--button style="background:#8a2b83!important;" onclick="imagen()" class="btn btn-success btn-xs relative" type="button">View Asign</button>                                                                                                                                                                         
                                                                            <br>
                                                                            <button style="background:#8a2b83!important;" onclick="imagen_req()" class="btn btn-success btn-xs relative" type="button">View Request</button>    
                                                                            <br-->
                                                                            
                                                                            
                                                                            <input type="hidden" name="imgpath" id="imgpath" value="<?php echo $_POST["imgpath"];?>" />     
                                                                            
                                                                            <div class="error-box">
                                                                                    <span id="errormsg-1" style="color:#FF0000; font-size:14px; font-weight:bold; font-family:arial; display:block; clear:both;"></span>
                                                                            </div>                                                                            
                                                                     </li>
                                                                    
                                                                    </ul>
								</div>                                                            
                                                            


							</div>

							<div class="table-responsive">
								<table class="table table-condensed nomargin">
									<thead>
										<tr>
											<th>Variety</th>
                                                                                        <th>Size</th>
                                                                                        <th>Bun</th>                                                                                        
                                                                                        <!--th>Price</th-->
                                                                                        <th> </th>
                                                                                        <th>Box</th>
										</tr>
									</thead>
                                                                        
									<tbody>																					

									<?php 
                                                                                 $tmp_idorder = 0;
                                                                                 
                                                                                 $cn=1;
                                                                                 
                                         while($row = mysqli_fetch_assoc($result))  {
                                                                                  
                                                             if ( $row['assign']== 10 )  {                                                                    
                                                                 $estado = '**';                                                                 
                                                             } else {
                                                                 $estado = ' ';                                                                 
                                                             }                             
                                                                                          
                                            ?>
							<tr>
                                                                                    
                                                                                      
                                                                                      
							<td>
                                             <?php

                
        
         // Costos  manejar por colores para que siempre despliegue el ultimo    
                                                                                        
        $sel_costo = "select (id),id_fact,  prod_name, product_subcategory, (price_cad) as price_cad , date_added
                        from invoice_packing_box
                       where buyer     = '" . $userSessionID . "'
                         and prod_name = '" . $row['name_product'] . "'
                         and product_subcategory = '" . $row['psubcatego'] . "' 
                         and id_fact   = '".$idfac."'
                       order by id desc limit 0,1";                                                                                        
        
        $rs_costo = mysqli_query($con,$sel_costo);       
        $costopack = mysqli_fetch_array($rs_costo);        
        
        
        $sel_ultprice = "select product, prod_name,cliente_id ,price_quick
                           from invoice_requests_subcli
                          where product    = '" . $row['product'] . "'
                            and cliente_id = '" . $row['id_client'] . "'
                          order by id desc limit 0,1";                                                                                        
        
        $rs_ultprice = mysqli_query($con,$sel_ultprice);       
        $last_price = mysqli_fetch_array($rs_ultprice);   
        
          if ( $last_price['price_quick']== 0.00 )  {                                                                    
                $priceinv = $row['price'];                                                                 
          } else {
                $price_inv = $last_price['price_quick'];                                                                 
          }

                          ?>

                                                                                <div><?php echo $row['psubcatego']." ".$row['name_product']." ".$row['colorname']." ".$row['featurename']; ?></div>
										</td> 
                                                                                                                            
                                                                                    
										<td>
                                                                                        <?php echo $row['size_name']."  cm." ; ?>
                                                                                </td>                                                                                    
                                                                                    
										<td>                                                                                                    
                                                                                        <div><?php echo $row['qty_pack']; ?></div>                                                                 
                                                                                </td>
                                                                                
                                                                               
                                                                                
                                                                                    <!-- Ultimo Precio Anterior-->
                                                                                <!--td>    
                                                                                        <div><?php echo number_format($last_price['price_quick'], 2, '.', ','); ?></div>                                                                                    
                                                                                </td--> 
                                                                                
                                                                                
                                                                                
                                                                                    <!-- Price -->
                                                                                <!--td>                                                                                       
                                                                                        <input type="text" class="form-control" name="price-<?php echo $row['gid'] ?>" id="price-<?php echo $row['gid'] ?>" value="<?php echo $row['price'] ?>" style="margin-top:5px; height:25px; padding:3px; width:80px;border-radius: 5px;">                                                                                          
                                                                                </td-->                                                                                                                                                                  
                                                                                
                                                                                
                                                                                <td>
                                                                                        <input type="hidden" class="form-control" name="id_reser" id="id_reser" value="<?php echo $row['id_reser'] ?>" style="margin-top:5px; height:25px; padding:3px; width:100px;border-radius: 5px;">      
                                                                                </td> 
                                                                                
                                                                                <td>
                                                                                        <input type="text" class="form-control" name="boxp-<?php echo $row['gid'] ?>" id="boxp-<?php echo $row['gid'] ?>" value="<?php echo $row['num_box'] ?>" style="margin-top:5px; height:25px; padding:3px; width:80px;border-radius: 5px;">      
                                                                                        <input type="hidden" class="form-control" name="pro-<?php echo $cn ?>" value="<?php echo $row["gid"] ?>"/>
                                                                                </td>                                                                                                                                                                  

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                                                                                    
                                                                                    
                                                                                 
										</tr>																				
											<?php 
                                                                                                $totalCal    = $totalCal + $Subtotal;   
                                                                                                $total_bunch = $total_bunch + $row['qty_pack'];   
                                                                                                
                                                                                                $cn++;
                                                                                                                                                                      
                                                                             } ?>
									</tbody>
								</table>
							</div>

							<hr class="nomargin-top" />

							<div class="row">

								<div class="col-sm-6 text-left">
									<h4><strong>-</strong> </h4>

								</div>
                                                            
								<div class="col-sm-6 text-right">
									<ul class="list-unstyled">
                                                                            <li> <strong><h3>Total Bunch :  <?php echo $total_bunch;  ?></h3></strong> </li>                                                                                                                                                                                                              
                                                                            <!--li> <strong>Total Amount : </strong> <?php echo "$".number_format($total_fact, 2, '.', ',');  ?> </li-->                                                                                                                                  
									</ul>  
								</div>
                                                           
							</div>

						</div>
					</div>					

					<div class="panel panel-default text-right">

                                            
						<div class="panel-body">                                                            
                                                            
				                        <input type="submit" id="submitu" class="btn btn-success" name="submitu" value="Save">     
                                                            
							<a class="btn btn-success" href="<?php echo SITE_URL; ?>buyer/print_etiqueta_inventory.php?b=<?php echo $idfac ?>" target="_blank"><i class="fa fa-print"></i> LABEL </a>                                                                                                           
                                                        
                                                       <!--button id="assig" style="background:#8a2b83!important;" onclick="requestProduct('<?php echo $ir ?>','<?php  echo $info['id_client'];  ?>','<?php  echo $info['date_del'];  ?>',<?php  echo $info['id_order'];  ?> )" class="btn btn-primary" type="button">Assigned Customer</button-->                                                       
						</div>
					</div>                                                                                                                    
                                    
                                    
				</div>
                                
			<!--/section-->
                        
			<!-- /MIDDLE -->

		</div>



	
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
		<script type="text/javascript" src="assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="assets/js/app.js"></script>

	<input type="hidden" name="totalrow" value="<?php echo $cn ?>"/>
        </form>
</section>
<?php require_once '../includes/footer_new.php'; ?>
<script type='text/javascript'>
    $(window).load(function () {
        $('#loading').css("display", "none");
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        
          $("#assig").click(function(){
              $("#assig").prop("disabled", true);
          });                
        
       $('select').select2();
       
                $('#productvar').select2({
                    ajax: {
                        url: "search_variety_customer.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });   
                

       
       ///////////
       
        var delDate = $('#cls_date_ship').find("input").val();
                      
         $('input[name=sname]').val(delDate);     
         
         console.log(delDate);
                      
                $('#var_add_x').select2({
                    ajax: {
                        url: "search_date_filter.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });        
        
               checkOptionship();
    });

</script>




<script>
    
       
    
    function boxQuantityChange(id, cartid, main_tr, with_a) {
        
        var selected_text = $("#request_qty_box_" + main_tr + " option:selected").text();
        var selected_val  = $("#request_qty_box_" + main_tr + " option:selected").val();
                        
		var nbox = selected_val.split('-');
                
                var tbox = $.trim(selected_text).split(" ");
                
		$('input[name=boxcant]').val(nbox[0]);
                
                $('input[name=boxtypen]').val(tbox[1]);
                                                
        $('input[name=product_su]').val(selected_val);
        $(".cls_hidden_selected_qty").val(selected_val);                                 
    }    
    
    
    // opcion que funciona del calendario pot
    
    function checkOptionship() {
    
        var shippingMethod = '132';
        
        var fecha_fin = $('#cls_date_del').find("input").val();
        console.log(fecha_fin);        
        
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/file/get_date_modal_cliship.php',
                data: 'shippingMethod=' + shippingMethod + '&fecha_fin=' + fecha_fin,
                success: function (data) {
                    $('.cls_date_start_dates').html(data);
                    $('#nextOpt').click();
                    _pickers();//show calendar
                }
            });        
    }
    
    function checkOptiondel(fecha_ini) {
    
       // var shippingMethod = '132';
           
       //console.log(fecha_ini);
        
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/file/get_date_modal_clidel.php',
                data: 'fecha_ini=' + fecha_ini,
                success: function (data) {
                    $('.cls_date_start_dated').html(data);
                    $('#nextOpt').click();
                    _pickers();//show calendar
                }
            });        
    } 
     function send_date() {
        
        var delDate = $('#cls_date_ship').find("input").val();
       
                      
         $('input[name=sname]').val(delDate);     
         
         console.log("Date");
                      
                $('#var_add').select2({
                    ajax: {
                        url: "search_date_filter.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });        


     }   
     
     
    /*Button send request */
    function requestProduct(aa,cod_cli,del_date,orderid) {     
        

                
         var shipDate = $('#cls_date_ship').find("input").val();                     
         $('input[name=sname]').val(shipDate);        
        
        var buyer     = '<?= $_SESSION["buyer"]; ?>'; // codigo  del  buyer
        var flag_s = true; 
        var dateRange = "";              
       
        var tag            = cod_cli;                               
        var date_del       = del_date;                                       
        var order_val      = orderid;      

        var productId      = $('#var_add' + ' :selected').val();                     
        var productText    = $('#var_add' + ' :selected').text();  
        
        
        var productId_requ      = $('#productvar' + ' :selected').val();                     
        var productText_requ    = $('#productvar' + ' :selected').text();                                              
                             
                
        var box_quantity   = $('#qty_pack').val() ;         
        
        var type_box       = $('#box_type_'      + aa + ' :selected').val();                        
        var type           = $('#type_req_'      + aa).val();        
        var comment_pro    = $('#comment_'       + aa).val();
        var req_grow       = $('#grow_'          + aa).val();                         
                        
        var date_ship      = $('#sname').val();           
       
                        
        if (order_val != "") {
            $('#erMsg').hide();
        }else {
            flag_s = false;
            alert("Please select Order");            
            $('#erMsg').show();
        }
        
        if ($('#qty_pack').val() < 1) {
            $('#errormsg-1').html("Quantity error");
            flag_s = false;
        }
        
        var qytmax = $.trim(productText).split(" ");
                
        $('input[name=qty_ped]').val(qytmax[0]);   
        
        var qty_x = 0;        
        var qty_control = 0;
        
          qty_x = $('#qty_pack').val();        
         qty_control = $('#qty_ped').val();

                
        if (parseInt(qty_x) > parseInt(qty_control)) {
            $('#errormsg-1').html("Error Qty");
            flag_s = false;
        }      
        
                 //  alert (qty_x);
                 //  alert (qty_control);
                   
        
    if (flag_s == true) {           
        console.log("flag_s "  + flag_s);
        console.log("order_val "   + order_val);  
        console.log("productId "   + productId);  
        
        $.ajax({
            type: 'post',
            url: 'https://app.freshlifefloral.com/buyer/request_product_ajax_subcliente.php',
            data: 'date_range=' + dateRange +
                   '&order_val=' + order_val + 
                   '&productId=' + productId + 
              '&productId_requ=' + productId_requ +                                                          
                '&box_quantity=' + box_quantity + 
                       '&buyer=' + buyer + 
                    '&type_box=' + type_box + 
                        '&type=' + type + 
                 '&comment_pro=' + comment_pro +                   
                 '&productText=' + productText + 
            '&productText_requ=' + productText_requ +                                                     
                         '&tag=' + tag +                          
                   '&date_ship=' + date_ship +                          
                    '&date_del=' + date_del +                          
                    '&req_grow=' + req_grow,       
            success: function (data_s) {
                
                if (data_s == 'true') {

                } else {
                    alert('There is some error. Please try again 1');
                }
                         location.reload();

            }
        });
    }

    } 
    
    
    /*Button send DELETE */
    function reverasign(deleteid,idgor,qtypack,orderid) {                                  
                       
        var flag_s = true; 
                     
        var id_delete      = deleteid;                     
        var id_offer_reply = idgor;                                              
        var quantity       = qtypack;                                              
        var numped         = orderid;                                              
                                                     
    if (flag_s == true) {           
        
        $.ajax({
            type: 'post',
            url: 'https://app.freshlifefloral.com/buyer/reversar_stock_subcliente.php',
            data: 'id_delete=' + id_delete +
            '&id_offer_reply=' + id_offer_reply +       
                  '&quantity=' + quantity+
                    '&numped=' + numped,       
            success: function (data_s) {
                
                if (data_s == 'true') {
                       alert('OK');
                } else {
                    //alert('There is some error. Please try again 1');
                }
                         location.reload();

            }
        });
    }

    }  
function imagen(){       
        
	  <?php

		$sel_img="select br.id as idbr     , br.lfd        ,br.product as idcod  ,  p.id as codvar,
                                         gor.id as idgor   , gor.offer_id  , gor.offer_id_index , gor.grower_id   , gor.buyer_id , 
                                         gor. product as  productname , 
                                         gor.product_subcategory, gor.size as size_name  , (gor.bunchqty-gor.reserve) as  bunchqty  ,   gor.steams ,
                                         g.growers_name , br.feature, f.name as featurename,
                                         br.id_order,p.image_path
                                    from buyer_requests br
                                   inner join grower_offer_reply gor on gor.offer_id = br.id
                                   inner join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name 
                                    left join growers g on gor.grower_id = g.id
                                    left join features f on br.feature = f.id 
                                   where g.active = 'active' 
                                     and (gor.bunchqty-gor.reserve) > 0
                                     and br.id_order  >= '822' ";

		$res_img=mysqli_query($con,$sel_img);

		while($rw_imagen=mysqli_fetch_array($res_img))	{

	  ?>
                               
                var idprod = $('#var_add' + ' :selected').val();
                
                
               // alert(ordenid);
                
                if(idprod=="<?php echo $rw_imagen["codvar"]?>")	{
                     
                        $('#errormsg-').html("<?= $rw_imagen["image_path"]?>")  
                        
                        var span_Text = document.getElementById("errormsg-").innerText;
                        //alert (span_Text);
                        
                        var  url_url= 'https://app.freshlifefloral.com/' + span_Text;
                        
                        $('input[name=idimg]').val(span_Text); 
                        
                        $('#my_image').attr('src',url_url);
	  	}
                                              
	  <?php	}   ?>	

	}    
function imagen_req(){       
    
        var productText_requ = $('#productvar' + ' :selected').text();    
        
        var path = productText_requ.split("+", 2);        
        $('input[name=imgpath]').val(path[1]);        
        
        var pimage = $('#imgpath').val();  
       
        //alert(pimage);
        
                        $('#errormsg-').html(pimage)  
                        
                        var span_Text = document.getElementById("errormsg-").innerText;
                        
                        var  url_url= 'https://app.freshlifefloral.com/' + span_Text;
                        
                        $('input[name=idimg]').val(span_Text); 
                        
                        $('#my_image').attr('src',url_url);
                                              	  
	}                
</script>    
    
    