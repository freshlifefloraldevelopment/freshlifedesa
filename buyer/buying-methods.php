<?php
require_once("../config/config_new.php");

#############QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################
$pageId = 3; //VARIETY PAGE ID 
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL . '../includes/assets/css/essentials-flfv5.css', SITE_URL . 'includes/assets/css/layout-flfv5.css',
    SITE_URL . '../includes/assets/css/header-1-flfv5.css', SITE_URL . 'includes/assets/css/color_scheme/blue.css');
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################

require_once '../includes/header.php';
?>
<script>
$(function(){
  $(".heresym").mask("99,ZZ", {
    translation: {
        'Z': {
          pattern: /[0]/, 
          fallback: '0'
        }
      }
  });
})
</script>
<section class="page-header page-header-xs">
    <div class="container">

        <!-- breadcrumbs -->
        <ol class="breadcrumb breadcrumb-inverse">
            <li><a href="<?php echo SITE_URL; ?>">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Buying Method</li>
        </ol><!-- /breadcrumbs -->

    </div>
</section>
<!-- /PAGE HEADER -->

<section>
    <div class="container">

        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-6">

                <!-- OWL SLIDER -->
                <div class="owl-carousel buttons-autohide controlls-over nomargin box-shadow-1" data-plugin-options='{"items": 1, "autoHeight": true, "navigation": true, "pagination": true, "transitionStyle":"fade", "progressBar":"true"}'>
                    <?php
                    $ram = 0;
                    $para2 = explode(":", $pageData["slide_2"]);
                    for ($i = 0; $i < count($para2); $i++) {

                        if ($para2[$i] != '') {
                            $ram+=1;
                            //echo $ram;
                            ?>

                            <div>
                                <img class="img-responsive" src="<?php echo SITE_URL; ?>user/<?= $para2[$i] ?>" alt="">
                            </div>
                        <?php }
                    } ?>
                </div>
                <!-- /OWL SLIDER -->

            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="heading-title heading-border-bottom">
                    <h3><?php echo $pageData["heading1"];?></h3>
                </div>

                <p class="heresym"><?php echo $pageData["para_1"];?></p>
                <p class="heresym"><?php echo $pageData["para_2"];?></p>
                <blockquote>
                    <p><?php echo $pageData["client_review"];?></p>
                    <cite><?php echo $pageData["client_name"];?></cite>
                </blockquote>

            </div>

        </div>

    </div>
</section>
<!-- / -->
<!-- -->
<section>
    <div class="container">

        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-6">

                <div class="heading-title heading-border-bottom">
                    <h3><?php echo $pageData["contact_us"];?></h3>
                </div>

                <ul class="nav nav-tabs nav-clean">
                    <li class="active"><a href="#tab1" data-toggle="tab"><?php echo $pageData["fb1"];?></a></li>
                    <li><a href="#tab2" data-toggle="tab"><?php echo $pageData["fb2"];?></a></li>
                    <li><a href="#tab3" data-toggle="tab"><?php echo $pageData["fb3"];?></a></li>
                    <li><a href="#tab4" data-toggle="tab"><?php echo $pageData["fb4"];?></a></li>
                </ul>

                <div class="tab-content">
                    <div id="tab1" class="tab-pane fade in active">
                        <img class="pull-left" src="<?php echo "user/".$pageData["add1"];?>" width="200" alt="" />
                        <p><?php echo $pageData["tw1"];?><p>
                        <p><?php echo $pageData["gp1"];?></p>
                    </div>
                    <div id="tab2" class="tab-pane fade">
                        <img class="pull-right" src="<?php echo "user/".$pageData["add2"];?>" width="200" alt="" />
                        <p><?php echo $pageData["tw2"];?><p>
                        <p><?php echo $pageData["gp2"];?></p>  </div>
                    <div id="tab3" class="tab-pane fade">
                        <p><?php echo $pageData["tw3"];?><p>
                        <p><?php echo $pageData["gp3"];?></p></div>
                    <div id="tab4" class="tab-pane fade">
                        <p><?php echo $pageData["tw4"];?><p>
                        <p><?php echo $pageData["gp4"];?></p> </div>
                </div>

            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">

                <div class="heading-title heading-border-bottom">
                    <h3><?php echo $pageData["latest_post"];?></h3>
                </div>
				<?php for ($l=1;$l<10;$l++){ 
				if($pageData["post".$l."_title"] != ''){
				if($l==1){ $cl="progress-bar-warning progress-bar-striped"; 
				}else if($l==2){ $cl="progress-bar-danger progress-bar-striped"; 
				}else if($l==3){ $cl="progress-bar-success progress-bar-striped"; 
				}else if($l==4){ $cl="progress-bar-info progress-bar-striped"; 
				}else{
				$cl="progress-bar-warning progress-bar-striped";
				}
				
				
				?>
                <div class="progress progress-lg"><!-- progress bar -->
                    <div class="progress-bar <?php echo $cl;?> active text-left" role="progressbar" aria-valuenow="<?php echo $pageData["post".$l."_link"];?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $pageData["post".$l."_link"];?>%; min-width: 2em;">
                        <span><?php echo $pageData["post".$l."_title"];?>  <?php echo $pageData["post".$l."_link"];?>%</span>
                    </div>
                </div><!-- /progress bar -->
				<?php }}?>

            </div>

        </div>

    </div>
</section>
<!-- / -->
<!-- PARALLAX -->
<section class="parallax parallax-2" style="background-image: url('assets/images/demo/wall3-min.jpg');">
    <div class="overlay dark-5"><!-- dark overlay [1 to 9 opacity] --></div>

    <div class="container">

        <div class="row">

            <div class="col-md-3">

                <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center">
                    <div class="front">
                        <div class="box1" style="background-color:#73b9dc;">
                            <div class="box-icon-title">
                                <i class="fa fa-file-text"></i>
                                <h2><?php echo $pageData["box1_title"];?></h2>
                            </div>
                            <p><?php echo $pageData["box1_desc"];?></p>
                        </div>
                    </div>

                    <div class="back">
                        <div class="box2" style="background-color:#73b9dc;">
                            <h4><?php echo $pageData["box2_title"];?></h4>
                            <hr />
                            <p><?php echo $pageData["box2_desc"];?></p>
                            <a href="<?php echo $pageData["usefullink2_link"];?>" class="btn btn-translucid btn-lg btn-block"><?php echo $pageData["usefullink2_title"];?></a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-3">

                <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center">
                    <div class="front">
                        <div class="box1" style="background-color:#a0ce4e;">
                            <div class="box-icon-title">
                                <i class="fa fa-gavel"></i>
                                <h2><?php echo $pageData["box3_title"];?></h2>
                            </div>
                            <p><?php echo $pageData["box3_desc"];?></p>
                        </div>
                    </div>

                    <div class="back">
                        <div class="box2" style="background-color:#a0ce4e;">
                            <h4><?php echo $pageData["box4_title"];?></h4>
                            <hr />
                            <p><?php echo $pageData["box4_desc"];?></p>
<a href="<?php echo $pageData["usefullink4_link"];?>" class="btn btn-translucid btn-lg btn-block"><?php echo $pageData["usefullink4_title"];?></a>                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-3">

                <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center">
                    <div class="front">
                        <div class="box1" style="background-color:#cb9536;">
                            <div class="box-icon-title">
                                <i class="fa fa-cubes"></i>
                                <h2><?php echo $pageData["box5_title"];?></h2>
                            </div>
                            <p><?php echo $pageData["box5_desc"];?></p>
                        </div>
                    </div>

                    <div class="back">
                        <div class="box2" style="background-color:#cb9536;">
                            <h4><?php echo $pageData["box6_title"];?></h4>
                            <hr />
                            <p><?php echo $pageData["box6_desc"];?></p>
<a href="<?php echo $pageData["usefullink6_link"];?>" class="btn btn-translucid btn-lg btn-block"><?php echo $pageData["usefullink6_title"];?></a>                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-3">

                <div class="box-flip box-color box-icon box-icon-center box-icon-round box-icon-large text-center">
                    <div class="front">
                        <div class="box1" style="background-color:#73b9dc;">
                            <div class="box-icon-title">
                                <i class="fa fa-binoculars"></i>
                                <h2><?php echo $pageData["box7_title"];?></h2>
                            </div>
                            <p><?php echo $pageData["box7_desc"];?></p>
                        </div>
                    </div>

                    <div class="back">
                        <div class="box2" style="background-color:#73b9dc;">
                            <h4><?php echo $pageData["box8_title"];?></h4>
                            <hr />
                            <p><?php echo $pageData["box8_desc"];?></p>
<a href="<?php echo $pageData["usefullink8_link"];?>" class="btn btn-translucid btn-lg btn-block"><?php echo $pageData["usefullink8_title"];?></a>                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

</section>
<!-- /PARALLAX -->
<!-- CLIENTS -->
<section class="padding-xs">
    <div class="container">

        <header class="text-center margin-bottom-10">
            <h3><?php echo $pageData["cap_2_title"]; ?></h3>
        </header>

        <hr class="margin-bottom-60" />

        <!-- 
                controlls-over		= navigation buttons over the image 
                buttons-autohide 	= navigation buttons visible on mouse hover only
                
                data-plugin-options:
                        "singleItem": true
                        "autoPlay": true (or ms. eg: 4000)
                        "navigation": true
                        "pagination": true
        -->
        <div class="owl-carousel nomargin text-center" data-plugin-options='{"singleItem": false, "stopOnHover":false, "autoPlay":4000, "autoHeight": false, "navigation": false, "pagination": true}'>
<?php 
						$para2=explode(":",$pageData["cap_2"]);
							for($i=0;$i<count($para2);$i++){
							if($para2[$i] != ''){
						?>           
		   <div>
                <img class="img-responsive" src="<?php echo SITE_URL; ?>user/<?php echo $para2[$i];?>" alt="">
            </div>
            <?php }}?>
        </div>

    </div>
</section>
<!-- /CLIENTS -->
<?php
require_once '../includes/footer.php';
?>
