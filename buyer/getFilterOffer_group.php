<?php

require_once("../config/config_gcp.php");

//include("../pagination/pagination.php");

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}

if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}

$userSessionID  = $_SESSION["buyer"];
$shippingMethod = $_SESSION['shippingMethod'];

/*********get the data of session user****************/

if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {

    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();

    if (empty($userID)) {
        header("location:" . SITE_URL);
        die;
    }

} else {
    header("location:" . SITE_URL);
    die;
}

$img_url = 'profile_images/noavatar.jpg';

if ($profile_image) {
    $img_url = 'profile_images/' . $profile_image;
}

$page_request = "inventory";

?>


<?php

$wh = "";



if ($_REQUEST['filter_variety'] != "0") {
    $wh .= " AND p.id in (" . $_REQUEST['filter_variety'].")";      
}

if ($_REQUEST['filter_category'] != "") {
    $wh .= " AND bo.id=" . $_REQUEST['filter_category'];
}

if ($_REQUEST['filter_grower'] != "") {
    $wh .= " AND gr.cliente_id=" . $_REQUEST['filter_grower'];
}

if ($_REQUEST['filter_size'] != "") {
    $wh .= " AND gr.grower_id  =" . $_REQUEST['filter_size'];
}



$from=0;
$display=50;
$page = (int) (!isset($_REQUEST["page_number"]) ? 1 : $_REQUEST["page_number"]);
$page = ($page == 0 ? 1 : $page);
$from = ($page - 1) * $display;

$dates = date("jS F Y");

if ($_REQUEST['filter_date']!="") {
    
    $variable = $_REQUEST['filter_date'];
    $fech     = date("Y-n-j");

    if (strtotime($fech) == strtotime($variable)) {
                
        $dates = date("jS F Y");
        $query2 = "select g.growers_name , g.id as grower_cod,
                     gr.id as idbox  ,  
                     gr.bunchqty  as qty_pack  , 
                     gr.steams      ,
                     gr.offer_id    ,                      
                     gr.product     , 
                     gr.request_id  , 
                     gr.cliente_id  ,                     
                     gr.branch  ,                     
                     gr.price       ,                      
                     gr.grower_id   ,                                           
                     gr.size ,                     
                     gr.product_subcategory as subs,                     
                     (gr.bunchqty*gr.steams) as totstems ,                      
                     if(gr.type_market=1,'OM','SO') as tmarquet,
                     p.id as prod_cod , 
                     p.color_id ,
                     p.image_path as img_url , 
                     scl.name as client_name ,
                     bo.order_number as embarque ,                      
                     bo.qucik_desc , 
                     bo.id as po ,
                     br.feature  , 
                     br.id_order ,                      
                     f.name as features , 
                     bra.name as branch_name                                          
                from grower_offer_reply gr 
               left join buyer_requests br  on gr.offer_id = br.id 
               left join product p          on gr.product = p.name 
               inner join buyer_orders bo    on br.id_order = bo.id                  
               inner join growers g          on gr.grower_id = g.id 
                left join sub_client scl     on gr.cliente_id = scl.id
                left join sub_client_branch bra on gr.branch = bra.id
                left join features f            on br.feature = f.id                
               where gr.buyer_id='" . $userSessionID . "'
                 and br.id_order in ('728')
                 $wh  ";

          $query2 .= " order by gr.product_subcategory,gr.product  LIMIT $from,$display";
        
        $result2 = mysqli_query($con, $query2);

    } else {       
         
        $date = new DateTime($variable);
        $dates = $date->format('jS F Y');

        $query2 = "select g.growers_name , g.id as grower_cod,
                     gr.id as idbox  ,  
                     gr.bunchqty  as qty_pack  , 
                     gr.steams      ,
                     gr.offer_id    ,                      
                     gr.product     , 
                     gr.request_id  , 
                     gr.cliente_id  ,                     
                     gr.branch  ,                     
                     gr.price       ,                      
                     gr.grower_id   ,                                           
                     gr.size ,                     
                     gr.product_subcategory as subs,                     
                     (gr.bunchqty*gr.steams) as totstems ,                      
                     if(gr.type_market=1,'OM','SO') as tmarquet,
                     p.id as prod_cod , 
                     p.color_id ,
                     p.image_path as img_url , 
                     scl.name as client_name ,
                     bo.order_number as embarque ,                      
                     bo.qucik_desc , 
                     bo.id as po ,
                     br.feature  , 
                     br.id_order ,                      
                     f.name as features , 
                     bra.name as branch_name                                          
                from grower_offer_reply gr 
               left join buyer_requests br  on gr.offer_id = br.id 
               left join product p          on gr.product = p.name 
               inner join buyer_orders bo    on br.id_order = bo.id                  
               inner join growers g          on gr.grower_id = g.id 
                left join sub_client scl     on gr.cliente_id = scl.id
                left join sub_client_branch bra on gr.branch = bra.id
                left join features f            on br.feature = f.id                
               where gr.buyer_id='" . $userSessionID . "'
                 and br.id_order in ('728')
                 $wh ";

        $query2 .= " ";

        $query2 .= " order by gr.product_subcategory,gr.product  LIMIT $from,$display";

        $result2 = mysqli_query($con, $query2);
    }


} else {

         
      $query2 = "select g.growers_name , g.id as grower_cod,
                     gr.id as idbox  ,  
                     gr.bunchqty  as qty_pack  , 
                     gr.steams      ,
                     gr.offer_id    ,                      
                     gr.product     , 
                     gr.request_id  , 
                     gr.cliente_id  ,                     
                     gr.branch  ,                     
                     gr.price       ,                      
                     gr.grower_id   ,                                           
                     gr.size ,                     
                     gr.product_subcategory as subs,                     
                     (gr.bunchqty*gr.steams) as totstems ,                      
                     if(gr.type_market=1,'OM','SO') as tmarquet,
                     p.id as prod_cod , 
                     p.color_id ,
                     p.image_path as img_url , 
                     scl.name as client_name ,
                     bo.order_number as embarque ,                      
                     bo.qucik_desc , 
                     bo.id as po ,
                     br.feature  , 
                     br.id_order ,                      
                     f.name as features , 
                     bra.name as branch_name                                          
                from grower_offer_reply gr 
               left join buyer_requests br  on gr.offer_id = br.id 
               left join product p          on gr.product = p.name 
               inner join buyer_orders bo    on br.id_order = bo.id                  
               inner join growers g          on gr.grower_id = g.id 
                left join sub_client scl     on gr.cliente_id = scl.id
                left join sub_client_branch bra on gr.branch = bra.id
                left join features f            on br.feature = f.id                
               where gr.buyer_id ='" . $userSessionID . "'
                 and br.id_order in ('728')
              $wh 
                  ";

    $query2 .= " ";

    $query2 .= " order by gr.product_subcategory,gr.product  LIMIT $from,$display";

    $result2 = mysqli_query($con, $query2);
}

?>
<!--div class="padding-20">    
<div class="panel-body">                                
<div class="dataReques11t"-->                                    
<table class="table table-hover table-vertical-middle nomargin">   
                        <thead>
                            <tr>                            
                                <th> </th>
                                <th>SubCategory</th>
                                <th>Ord.</th>                                 
                                <th>Type</th>                                 
                                <th>Grower</th>  
                                <th>Box</th>  
                                <th>Variety</th>
                                <th> </th>
                                <th>Change Qty</th>
                                <th>Client</th>                                                                   
                            </tr>                        
                        </thead>        
<tbody id="list_inventory">
     
<?php

$i = 1;

$tp = mysqli_num_rows($result2);

if ($tp == 0) {

    echo $XX = '<div class="notfound" style="margin-top:30px;">No Item Found '.$_REQUEST['filter_variety'].' ! </div>';

} else {

    while ($products = mysqli_fetch_array($result2)) {
       
        $k = explode("/", $products["file_path5"]);

        $data = getimagesize(SITE_URL . "user/logo2/" . $k[1]);

        $logourl = SITE_URL . "user/logo/" . $k[1];

        ?>

        <?php if ($products["bv"] != 2) { ?>


        <?php } else { ?>


        <?php } 

      $boxPacking = $products['box_name'];
      $box_num = "";
      
        if ($box_num != "") {
            $boxPacking= $box_num."box";  
        }
        
                                        $get_img = "SELECT id, product_id, grower_id, categoryid, subcaegoryid, colorid, image_path as img_url , active
                                                      FROM grower_product 
                                                     WHERE grower_id  = '" . $products['grower_cod'] . "' 
                                                       AND product_id = '" . $products['prod_cod']   . "'   ";
                                        
                                        $rs_img = mysqli_query($con, $get_img);
                                        $row_imagen = mysqli_fetch_array($rs_img);  
                                        
                                        /////////////// Colores
                                        $get_color = "SELECT id, name as color_variety
                                                        FROM colors 
                                                       WHERE id = '" . $products['color_id']   . "'   ";
                                        
                                        $rs_color = mysqli_query($con, $get_color);
                                        $row_color = mysqli_fetch_array($rs_color);                                                                                
        
        
        ?>

        <tr>

            <!--Grower-->
            <td><img src="https://app.freshlifefloral.com/<?= $row_imagen["img_url"]; ?>" width="70"></td>
            <td><?php echo $products["subs"]; ?> </td>                                    
            <td><?php echo $products["po"];  ?>  </td>
            <td><?php echo $products["tmarquet"];  ?>  </td>            
            <td><?php echo $products['growers_name']; ?></td>            
            <td><?php echo $boxPacking ?></td>                                                

            <!--Pack-->

                         <?php 
                                  
                               if ($products["type_market"] == "0") {
                                    $market = "Stand";
                                }else{
                                    $market = "Open";
                                }
                             ?>                   
            

            <!--Variety pot-->
            <td> <?php echo  $products['product']." ".$products['size']." cm ".$products['qty_pack']." Bunches ".$products['steams']." st/bu  " .$row_color['color_variety']."  ".$products["features"] ?> </td>
            
            <td>               
                <!--a href="" data-toggle="modal" data-target="#single_product_price<?php echo $products["idbox"] ?>">$<?php echo sprintf("%.2f", number_format($products['price_cad'], 2, '.', ',') ) ?> </a-->
                                                        <!--Modal image for 3452-->
                                                        <div class="modal fade bs-example-modal-sm" id="single_product_price<?php echo $products["idbox"] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
                                                             style="display: none;">
                                                            <div class="modal-dialog modal-sm">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                                                                        <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;">ALL IN PRICE</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <p>Farm Price...........................$<?= sprintf("%.2f", number_format($products['gorPrice'], 2, '.', ',') ) ?></p>
                                                                                <p>Duties 22%...........................$<?= sprintf("%.2f", number_format($products['duties'], 2, '.', ',')) ?></p>
                                                                                <p>Subtotal................................$<?= sprintf("%.2f", number_format(($products['gorPrice']+$products['duties']), 2, '.', ',')) ?></p>
                                                                                <p>Shipping................................$<?= sprintf("%.2f", number_format($products['ship_cost'], 2, '.', ',')) ?></p>
                                                                                <p>Subtotal Shipping.................$<?= sprintf("%.2f", number_format(($products['gorPrice']+$products['duties']+$products['ship_cost']), 2, '.', ',') ) ?></p>
                                                                                <p>FLF Handling 8%...................$<?= sprintf("%.2f", $products['handling_pro']) ?></p>
                                                                                <p>Total......................................$<?= sprintf("%.2f", number_format(($products['gorPrice']+$products['duties']+$products['ship_cost']+$products['handling_pro']), 2, '.', ',')) ?></p>
                                                                                <div class="divider divider-dotted"><!-- divider --></div>
                                                                                
                                                                                <p style="color: #273746"><strong>FinalPrice in CAD...............$<?= sprintf("%.2f", number_format($products['price_cad'], 2, '.', ',') ) ?></strong></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                
            </td>  
            
                                                    <!-- Subclientes -->
                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="change_<?php echo $products["idbox"] ; ?>" id="change_<?php echo $products["idbox"] ; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                               <?php
                                                                                                    $boxDataShow = $products["qty_pack"];
                                                                                                    for ($ii = $boxDataShow; $ii >= 1; $ii--) {
                                                                                                ?>
                                                                                            <option value="<?= $ii ?>"><?= $ii ?></option>
                                                                                            <?php } ?>                                         
                                                                                        </select>                                                                        
                                                                                    </td>                                                                        
                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="client_<?php echo $products["idbox"] ; ?>" id="client_<?php echo $products["idbox"] ; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value=""><?= $products['client_name']; ?></option>    
                                                                                            <?php
                                                                                            $sql_cli = "select sc.id ,   CONCAT(sc.name ,'  ',p.name,'  ',s.name,' (',sum(qty),') ',sizeid) as name
                                                                                                          from sub_client sc
                                                                                                          inner join buyer_requests br  on br.id_client = sc.id 
                                                                                                          inner join product p  on br.product = p.id 
                                                                                                          inner join subcategory s  on p.subcategoryid = s.id 
                                                                                                          where sc.id not in (0,1) 
                                                                                                            and sc.buyer = '" . $userSessionID . "'
                                                                                                            and br.id_order in (728)
                                                                                                          group by sc.id , sc.name , br.product
                                                                                                          order by sc.name";
                                                                                            
                                                                                            $result_cli = mysqli_query($con, $sql_cli);
                                                                                            while ($row_client = mysqli_fetch_assoc($result_cli)) { ?>
                                                                                                <option value="<?php echo $row_client['id']; ?>"><?= $row_client['name']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>   
                                                                                    </td> 
                                                                                    
                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="branch_<?php echo $products["idbox"] ; ?>" id="branch_<?php echo $products["idbox"] ; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value=""><?= $products['branch_name']; ?></option>    
                                                                                            <?php
                                                                                            $sql_bra = "select id,name 
                                                                                                          from sub_client_branch 
                                                                                                         order by id";
                                                                                            
                                                                                            $result_bra = mysqli_query($con, $sql_bra);
                                                                                            
                                                                                            while ($row_branch = mysqli_fetch_assoc($result_bra)) { ?>
                                                                                                <option value="<?php echo $row_branch['id']; ?>"><?= $row_branch['name']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>                                                                        
                                                                                    </td>                                                                                                                                                                         
                                                                                    
                                                                                     <td>
                                                                                        <input type="hidden" class="form-control" name="pro-<?php echo $i ?>" value="<?php echo $products["idbox"] ?>"/>
                                                                                    </td>
                                                                                                                                                    
                      
                
           <!-- </td>-->
        </tr>
        <?php
        $i++;
}

}
?>
        
          </tbody>
          </table>                            
          <!--/div>       
       </div> 
  </div-->              
    

<script type="text/javascript">
    $(document).ready(function () {
        
        
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            //console.log('Selection: ' + suggestion);
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/inventory_search_product.php',
                data: 'name=' + suggestion,
                success: function (data) {
                    $("#pagination_nav").hide();
                    $('.imgDiv').hide();
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });
        

 });
 
</script>
