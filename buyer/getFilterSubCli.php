<?php

require_once("../config/config_gcp.php");

//include("../pagination/pagination.php");

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}

if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}

$userSessionID  = $_SESSION["buyer"];
$shippingMethod = $_SESSION['shippingMethod'];

/*********get the data of session user****************/

if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {

    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();

    if (empty($userID)) {
        header("location:" . SITE_URL);
        die;
    }

} else {
    header("location:" . SITE_URL);
    die;
}

$img_url = 'profile_images/noavatar.jpg';

if ($profile_image) {
    $img_url = 'profile_images/' . $profile_image;
}

$page_request = "inventory";

?>


<?php

$wh = "";

/*
if ($_REQUEST['filter_variety'] != "") {
    $wh .= " AND p.id in (" . $_REQUEST['filter_variety'].")";      
}  */

if ($_REQUEST['filter_category'] != "") {
    $wh .= " AND bo.id=" . $_REQUEST['filter_category'];
}

if ($_REQUEST['filter_grower'] != "") {
    $wh .= " AND sc.id =" . $_REQUEST['filter_grower'];
}


$from=0;
$display=50;
$page = (int) (!isset($_REQUEST["page_number"]) ? 1 : $_REQUEST["page_number"]);
$page = ($page == 0 ? 1 : $page);
$from = ($page - 1) * $display;

$dates = date("jS F Y");


        
        $query2 = "select sc.id, sc.name, sc.buyer ,company
                     from sub_client sc
                    inner join buyers b on sc.buyer = b.id
                    where sc.buyer = '".$userSessionID."'  $wh  ";

                $query2 .= " order by sc.name LIMIT $from,$display";
        
        $result2 = mysqli_query($con, $query2);


?>

<table class="table table-hover table-vertical-middle nomargin">   
                        <thead>
                            <tr>                            
                                <th>Sr.</th>
                                <th>Clientxxx</th>
                                <th>Information</th>
                                <th>Last Date</th>
                            </tr>                        
                        </thead>        
<tbody id="list_inventory">
     
<?php

$i = 1;

$tp = mysqli_num_rows($result2);

if ($tp == 0) {

    echo $XX = '<div class="notfound" style="margin-top:30px;">No Item Found !</div> '.$wh;

} else {

    while ($client = mysqli_fetch_array($result2)) {
        
                                                          $sel_mov="select id , id_fact , buyer_id , id_client , order_number  
                                                              from invoice_orders_subcli 
                                                             where buyer_id  = '".$userSessionID."'    
                                                               and id_client = '".$client["id"]."'  " ;
                                
                                                 $rs_mov = mysqli_query($con,$sel_mov);   
                                                 
                                                 $total_mov = mysqli_num_rows($rs_mov);
                                                 
                                                  $sel_datemax="select DATE_FORMAT(max(date_added), '%d-%m-%y') final_date
                                                                  from invoice_orders_subcli  
                                                                 where buyer_id  = '".$userSessionID."'    
                                                                   and id_client = '".$client["id"]."'  " ;
                                
                                                 $rs_datemax = mysqli_query($con,$sel_datemax);   
                                                 
                                                 $date_max = mysqli_fetch_assoc($rs_datemax); 
                                                 
                                                 $last_mov = $date_max["final_date"];
       

        ?>

        <?php if ($products["bv"] != 2) { ?>


        <?php } 

      $boxPacking = $products['box_name'];
      $box_num = "";
      
        if ($box_num != "") {
            $boxPacking= $box_num."box";  
        }
        
                                        $get_img = "SELECT id, product_id, grower_id, categoryid, subcaegoryid, colorid, image_path as img_url , active
                                                      FROM grower_product 
                                                     WHERE grower_id  = '" . $products['grower_cod'] . "' 
                                                       AND product_id = '" . $products['prod_cod']   . "'   ";
                                        
                                        $rs_img = mysqli_query($con, $get_img);
                                        $row_imagen = mysqli_fetch_array($rs_img);  
                                        
                                        /////////////// Colores
                                        $get_color = "SELECT id, name as color_variety
                                                        FROM colors 
                                                       WHERE id = '" . $products['color_id']   . "'   ";
                                        
                                        $rs_color = mysqli_query($con, $get_color);
                                        $row_color = mysqli_fetch_array($rs_color);                                                                                
        
        
        ?>

        <tr>
                                        <td><?php echo $client['id']; ?></td>
                                        <td><?php echo $client['name']; ?></td>
                                        
                                        <?php if ($total_mov < 1) { ?>
                                              <td> <a href="javascript:void(0);" class="btn btn-success btn-xs relative" style="border:0px solid #fff; background-color: #d68910">No Moving</a></td>
                                        <?php }else{?>   
                                              <td> <a href="javascript:void(0);" class="btn btn-success btn-xs relative" style="border:0px solid #fff; background-color: #00b300">Last Sale</a></td>
                                        <?php }  ?> 
                                              
                                        <td><?php echo $last_mov; ?></td>      
 
                                        <td><a href="<?php echo SITE_URL; ?>buyer/client_calendar.php?id_cli=<?php echo $client["id"]?>" class="btn btn-success btn-xs relative"> Calendar </a></td>                                                                                                                                                                        
                                        <td><a href="<?php echo SITE_URL; ?>buyer/customer_list_branch.php?id_cli=<?php echo $client["id"]?>" class="btn btn-success btn-xs relative"> Branches </a></td>                                        
                                        <td><a href="<?php echo SITE_URL; ?>buyer/customer_pre_invoice.php?id_cli=<?php echo $client["id"]?>" class="btn btn-success btn-xs relative"> Invoice </a></td>                                                                                                                                
                                                                                    
            <td>
                   <input type="hidden" class="form-control" name="pro-<?php echo $i ?>" value="<?php echo $products["idbox"] ?>"/>
            </td>
                                                                                                                                                                                         
        </tr>
        <?php
        $i++;
}

}
?>
        
          </tbody>
          </table>                            
          <!--/div>       
       </div> 
  </div-->              
    

<script type="text/javascript">
    $(document).ready(function () {
        
        
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            //console.log('Selection: ' + suggestion);
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/inventory_search_product.php',
                data: 'name=' + suggestion,
                success: function (data) {
                    $("#pagination_nav").hide();
                    $('.imgDiv').hide();
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });
        

 });
 
</script>
