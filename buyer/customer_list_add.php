<?php 
require_once("../config/config_gcp.php");



$userSessionID = $_SESSION["buyer"];

//update profile data
if (isset($_POST['update_profile_1']) ) {

    $no_error = 1;
    $update_error = '';

    $id = $_POST['id'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];


    /*if (empty($first_name)) {
        $update_error = "Please fill the First Name";
        $no_error = 0;
    }*/

   // if ($no_error == 1) {
       $insert="insert into sub_client set 
                            name       = '".$_POST["name"]."' ,
                            type_order = '".$_POST["type_order"]."' ,    
                            buyer      = '".$userSessionID."'   ";
       
	   mysqli_query($con,$insert);
           
	   header("location:customer_list.php");           

   // }
}
//manage labels for form area 
if ($_SESSION["lang"] == "ru") {
    $labels_arr['first_name'] = '????????????';
    $labels_arr['last_name'] = '???????';
    $labels_arr['email'] = 'Email';
    $labels_arr['phone'] = '???????';
    $labels_arr['address'] = '?????';
    $labels_arr['city'] = '?????';
    $labels_arr['state_text'] = '???????????';

} else {
    $labels_arr['first_name'] = 'Client';
    $labels_arr['last_name'] = 'Comment';
    $labels_arr['email'] = 'Email';
    $labels_arr['phone'] = 'Phone';
    $labels_arr['address'] = 'Address';
    $labels_arr['city'] = 'City';
    $labels_arr['state_text'] = 'State';
    
    $labels_arr['buyer'] = 'Buyer';

}
?>
<script>
    function validate_profile_form(theForm) {

        var error = 0;
        if (theForm.first_name.value == "" || theForm.first_name.value == "Full Name") {
            $('.error_first_name').html('Please fill the First Name');
            theForm.first_name.focus();
            $("#first_name").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }
        if (theForm.last_name.value == "" || theForm.last_name.value == "Last Name") {
            $('.error_last_name').html('Please fill the Last Name');
            theForm.last_name.focus();
            $("#last_name").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }
        if (theForm.phone.value == "" || theForm.phone.value == "Phone") {
            $('.error_phone').html('Please fill the Phone');
            theForm.phone.focus();
            $("#phone").attr("style", "border:1px solid #FF0000;");
            error = 1;
        }

        if (error == 1) {
            return false;
        } else {
            return true;
        }
    }

    function remove_validate_text(id) {
        $('.error_' + id).html('');
        $("#" + id).removeAttr("style");
    }

    function check_password(current_element) {
        var pass_val = $('#password').val();
        var rep_pass_val = $('#repeat_password').val();

        console.log(pass_val.length);
        if (pass_val.length > 0) {
            if (current_element == 'password') {
                if (pass_val.length <= 5) {
                    $('.error_password').html("Password should be greater then 5 character");
                    $('#password').attr("style", "border:1px solid #FF0000;");
                } else {
                    $('.error_password').html("");
                    $('#password').removeAttr("style");
                }
            }

            if (current_element == 'repeat_password') {
                if (pass_val != rep_pass_val) {
                    $('.error_repeat_password').html("Password did not match!");
                    $('#repeat_password').attr("style", "border:1px solid #FF0000;");
                } else {
                    $('.error_repeat_password').html("");
                    $('#repeat_password').removeAttr("style");
                }
            }
        }
    }
</script>
<?php require_once '../includes/profile-header.php'; ?>
<?php require_once "../includes/left_sidebar_buyer.php"; ?>
<!-- /ASIDE -->

<!--
            MIDDLE
        -->
<section id="middle">
    <div id="content" class="padding-20">

        <div class="page-profile">
            <?php
            if ($update_msg) {
                echo '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>' . $update_msg . '</div>';
            } elseif ($update_error) {
                echo '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>' . $update_error . '</div>';
            }
            ?>
            <div class="row">



                <!-- COL 2 -->
                <div class="col-md-8 col-lg-6">

                    <div class="tabs white nomargin-top">
                        <ul class="nav nav-tabs tabs-primary">

                            <li class="active">
                                <a href="#edit" data-toggle="tab">Edit</a>
                            </li>
                        </ul>

                        <div class="tab-content">


                            <?php 
                            $first = $name1[0];
                            $last_name = $name2[1]; ?>
                            <!-- Edit -->
                            <div id="edit" class="active tab-pane">

                                <form class="form-horizontal" id="profile_form" name="profile_form" method="post" enctype="multipart/form-data" onsubmit="javascript: return validate_profile_form(profile_form);">
                                    <h4>Information</h4>
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_first_name"></div>
                                            <div style="clear:both;"></div>

                                            <label class="col-md-3 control-label" for="name"><?php echo $labels_arr['first_name']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $_POST["name"] ?>" onkeypress="javascript:remove_validate_text('name');">
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_last_name"></div>
                                            <div style="clear:both;"></div>

                                            <label class="col-md-3 control-label" for="comment"><?php echo $labels_arr['last_name']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="comment" value="<?php echo $comment; ?>"  onkeypress="javascript:remove_validate_text('comment');">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-3"></label>
                                            <div class="col-md-8 error error_first_name"></div>
                                            <div style="clear:both;"></div>

                                            <label class="col-md-3 control-label" for="buyer"><?php echo $labels_arr['buyer']; ?></label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="buyer" name="buyer" value="<?php echo $_POST["buyer"] ?>" onkeypress="javascript:remove_validate_text('buyer');">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="profileBio">Type</label>
                                            <div class="col-md-8">
                                                       <select name="type_order" id="availability" class="listmenu">
                                                                    <option value="0" <?php  echo "selected";?>>Standing Order</option>
                                                                    <option value="1" <?php  echo "selected";?>>Open Market</option>
                                                       </select>                                                                                        
                                            </div>
                                        </div>


                                    </fieldset>


                                    <hr>



                                    <div class="row">
                                        <div class="col-md-9 col-md-offset-3">
                                            <input type="hidden" name="id" value="<?php echo $userID; ?>">
                                            <input type="hidden" name="update_profile_1" value="1">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>

                </div><!-- /COL 2 -->                

            </div>

        </div>

    </div>
</section>
<?php require_once("../includes/profile-footer.php"); ?>
