<?php
require_once("../config/config_gcp.php");
// PO 2018-07-02

$menuoff = 1;
$page_id = 5;
$message = 0;

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];


$img_url = '../imagenes/profile_images/noavatar.jpg';

if ($profile_image) {
    $img_url = '../imagenes/profile_images/' . $profile_image;
}

$today = date("m-d-Y H:i:s", strtotime("+2 hours"));
$today1 = explode(" ", $today);
$today_date = $today1["0"];
$time = $today1["1"];
$hours_array = explode(":", $time);

// Paginacion
$sel_pagina = "select gr.*
                  from grower_offer_reply gr 
                 where gr.buyer_id='" . $userSessionID . "'
                   and gr.offer_id >= '6731'
                   and gr.offer_id <= '6827'                 
                   and gr.reject in (0) ";

$rs_pagina = mysqli_query($con, $sel_pagina);
$total_pagina = mysqli_num_rows($rs_pagina);
$num_record = $total_pagina;
$display = 50;
$XX = '<div class="notfound">No Item Found !</div>';


function box_type($box){
    $box_type_s = "";
    if ($box == "0") {
        $box_type_s = "Stems";
    } else if ($box == "1") {
        $box_type_s = "Bunch";
    }
    return $box_type_s;
}

function box_type_name($id_box_type){
    global $con;
      
    $sel_box_type = "select code as name from units where id='" . $id_box_type . "'";
    $rs_box_type = mysqli_query($con, $sel_box_type);
    $box_type = mysqli_fetch_array($rs_box_type);
    return $box_type["name"];
}

function box_descrip($id_box_type){
    global $con;    
    $sel_unit_type = "select descrip from units where id='" . $id_box_type . "'";
    $rs_unit_type = mysqli_query($con, $sel_unit_type);
    $box_descrip_type = mysqli_fetch_array($rs_unit_type);
    return $box_descrip_type["descrip"];
}

function box_name($box_type){
    
    $res = "";
    if ($box_type == "HB") {
        $res = "half boxes";
    } else if ($box_type == "EB") {
        $res = "eight boxes";
    } else if ($box_type == "QB") {
        $res = "quarter boxes";
    } else if ($box_type == "JB") {
        $res = "jumbo boxes";
    }
    return $res;
}

function category(){
    global $con;
    $category_sql = "SELECT id,name from category order by  name";
    $result_category = mysqli_query($con, $category_sql);
    return $result_category;
}

function query_main($userSessionID){
    global $con;
   
    
    $query2 = "select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,
                      cr.flag , (gr.bunchqty*gr.steams) as totstems , scl.name as client_name
                 from grower_offer_reply gr 
                 left join growers g  on gr.grower_id = g.id 
                 left join country cr on g.country_id = cr.id
                 left join sub_client scl on gr.cliente_id = scl.id
           where gr.buyer_id='" . $userSessionID . "' 
             and offer_id >= '6731'
             and offer_id <= '6827'                 
             and reject in (0) 
             and g.id is not NULL 
           order by gr.grower_id , gr.offer_id_index ";
    

    $result2 = mysqli_query($con, $query2);
    return $result2;

}


?>
<?php include("../includes/profile-header.php"); ?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>
<?php include("../includes/left_sidebar_buyer.php"); ?>
<style type="text/css">
    .not_set_border td {
        border: none !important;
    }

</style>
<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Clients Offers</h1>
        <ol class="breadcrumb">
            <li><a href="#">Offers</a></li>
            <li class="active">Products</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>Offers</strong> <!-- panel title -->
                </span>

                <!-- right options -->
               <?php $dates = date("jS F Y");?>
                
                                    <ul class="options pull-right relative list-unstyled">
                                        <div class="tags_selected" id="tab_for_filter" style="float: left; margin-top: 4px; margin-right: 4px; display: none;"></div>
                                        <li>
                                          <!--  <a class="date_filter btn btn-danger btn-xs white" id="dates"><?= $_POST['order'] ?></a>-->
                                            <a class="date_filter btn btn-danger btn-xs white" id="dates"><?php echo $dates; ?></a>
                                            <a href="#" class="btn btn-primary btn-xs white" data-toggle="modal" data-target=".search_modal_open"><i class="fa fa-filter"></i> Filter</a>
                                        </li>
                                    </ul>
                                        <div class="modal fade search_modal_open" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <form name="form1" id="form1" method="post" action="buyer/my-offers.php" class="modal fade search_modal_open">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">                                    
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Choose Filter</h4>
                                                        </div>
                                                        <!-- Modal Body -->
                                                        <div class="modal-body">
                                                            <div class="panel-body">                                                        

                                                                <label>Category</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_category" id="filter_category" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Category</option>
                                                                        <?php
                                                                        $result_category = category();
                                                                        while ($row_category = mysqli_fetch_assoc($result_category)) { ?>
                                                                            <option value="<?= $row_category['id']; ?>"><?= $row_category['name']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div>                                                                
                                                                
                                                                <label>Order</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_variety" id="filter_variety" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Order</option>
                                                                        <?php
                                                                        $subcategory_sql = "select  id, qucik_desc  from  buyer_orders where  buyer_id='" . $userSessionID . "' order by id desc";
                                                                        $result_subcategory = mysqli_query($con, $subcategory_sql);
                                                                        while ($row_subcategory = mysqli_fetch_assoc($result_subcategory)) { ?>
                                                                            <option value="<?php echo $row_subcategory['id']; ?>"><?php echo $row_subcategory['qucik_desc']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div>                                                                

                                                            </div>
                                                        </div>                                                                
                                                        <!-- Modal Footer -->
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            <button id="btn_filter" class="btn btn-primary apply" type="button">Apply</button>
                                                            <img class="ajax_loader_s" style="display: none;" src="<?php echo SITE_URL; ?>images/ajax-loader.gif"/>
                                                        </div>                                                        
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                <!-- /right options -->
            </div>            
            
            <!-- panel content inicio po -->
            <div class="panel-body">
                                    <div id='loading'>
                                        <div class=" cssload-spin-box">
                                         <!--   <img src="<?php echo SITE_URL; ?>../includes/assets/images/loaders/5.gif"> -->
                                        </div>
                                    </div>                
                
                
                <div  class="dataRequest">

                                        <?php
                                            $cn = 1;
                                            $result2 = query_main($userSessionID);
                                            $tp = mysqli_num_rows($result2);
                                        ?>
                                        <input type="hidden" name="tp" id="tp" value="<?= $tp ?>">
                                        <input type="hidden" name="current" id="current" value="">
                                        <input type="hidden" name="id_user" id="id_user" value="<?= $userSessionID ?>">  
                                        
                                        
                                        
                                        
                                        
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                        <tr>
                         <!--   <th class="width-30"> </th>-->
                            <th>Grower</th>
                            <th align="center"> Product </th>
                            <th>Price</th>
                            <th>Change Qty</th>
                            <th>Client</th>                                                                   
                            <th> </th>
                        </tr>
                        </thead>
                        <tbody id="list_inventory">
                        <?php

                        
                        if ($tp >= 1) {
                            while ($producs = mysqli_fetch_array($result2)) {
                                $sel_check = "select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,
                                                     cr.flag , (gr.bunchqty*gr.steams) as totstems , scl.name as client_name
                                                from grower_offer_reply gr 
                                                left join growers g  on gr.grower_id = g.id 
                                                left join country cr on g.country_id = cr.id
                                                left join sub_client scl on gr.cliente_id = scl.id
                                               where gr.request_id='" . $producs["cartid"] . "' 
                                                 and gr.buyer_id='" . $userSessionID . "' 
                                                 and reject in (0) 
                                                 and g.id is not NULL 
                                               order by gr.grower_id , gr.offer_id_index";

                                $rs_growers  = mysqli_query($con, $sel_check);                                                                                                                                                                                                                                                                                   

                                ?>
                                <tr>

                                                              <?php
                                                                        $total_bunchs="";
                                                                        $boxes_type="";
                                                                        if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                            $getGrowerReply = "SELECT offer_id , bunchqty , boxqty 
                                                                                                 FROM grower_offer_reply 
                                                                                                where reject in (0) 
                                                                                                  and request_id  ='" . $producs['cartid'] . "'
                                                                                                  and grower_id ='" . $growers['grower_id'] . "' 
                                                                                                  and offer_id_index='" . $growers["offer_id_index"] . "' limit 1";
                                                                            
                                                                            $rs_getGrowerReply = mysqli_query($con, $getGrowerReply);
                                                                            $tot_bunch_qty = 0;
                                                                            while ($row_rs_getGrowerReply = mysqli_fetch_array($rs_getGrowerReply)) {
                                                                                $tot_bunch_qty = $tot_bunch_qty + $row_rs_getGrowerReply['boxqty'];
                                                                            }
                                                                            $total_bunchs= $tot_bunch_qty;
                                                                            if ($producs["boxtype"] != "") {
                                                                                $temp = explode("-", $producs["boxtype"]);
                                                                                $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                                                                                $rs_box_type = mysqli_query($con, $sel_box_type);
                                                                                $box_type = mysqli_fetch_array($rs_box_type);
                                                                                $type_box = box_name($box_type["name"]);
                                                                                $boxes_type= $type_box;
                                                                            }
                                                                        }
                                                                        //---------------------------------------------------------------------//

                                                                        //---------------------------------------------------------------------//
                                                                        $getbunchSize = "SELECT is_bunch_value,bunch_value,is_bunch  FROM grower_product_bunch_sizes WHERE grower_id = '" . $growers['grower_id'] . "' AND product_id = '" . $producs['product'] . "' 
                                                                            AND sizes = '" . $producs['sizeid'] . "'";
                                                                        $rs_bunchSize = mysqli_query($con, $getbunchSize);
                                                                        $row_bunchSize = mysqli_fetch_array($rs_bunchSize);
                                                                        $bunch_size_s = "";

                                                                        if ($row_bunchSize['bunch_value'] == 0) {
                                                                            $bunch_size_s = $row_bunchSize['is_bunch_value'];
                                                                        } else {
                                                                            //$bunch_size_s = $row_bunchSize['bunch_value'];
                                                                        }


                                                                        //---------------------------------------------------------------------//


                                                                        ?>

                                    
                                    <td><?= $producs["growers_name"] ?></td>
                                    
                                                                        
                                                                            <?php
                                                                            if ($row_bunchSize['bunch_value'] == 0) {
                                                                                ?>                                                                                                                                                
                                                                                    <td><?php $box_type_s = box_type($producs['box_type']); ?>
                                                                                        
                                                                                        <?= $producs["product"]. " " ?><?= $producs["size"] . " cm " ?><?= $producs["bunchqty"] . " Bunchess " ?><?= $producs["steams"] . " st/bu "; ?>
                                                                                    </td>                                                                        
                                                                            <?php }else{?>
                                                                                    <td><?php $box_type_s = box_type($producs['box_type']); ?>
                                                                                        <?= $producs["product"]. " " ?><?= $producs["size"] . " cm " ?><?= $producs["bunchqty"] . " Bunches " ?><?= $producs["steams"] . " st/bu "; ?>
                                                                                    </td>                                                                                                                                                        
                                                                            <?php }
                                                                            ?>                                                                        
                                                                        
                                                                        <td>$<?= $producs['price']; ?></td>                                    

                                                                        <td>
                                                                                        <select style="width: 100%; diplay: none;" name="filter_change" id="change_<?php echo $producs["grid"] . '_' . $cn; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                <?php
                                                                                     $boxDataShow = $producs["bunchqty"];
                                                                                for ($ii = $boxDataShow; $ii >= 1; $ii--) {
                                                                                ?>
                                                                                       <option value="<?= $ii ?>"><?= $ii ?></option>
                                                                                <?php } ?>                                         
                                                                                        </select>                                                                        
                                                                        </td>                                                                        
                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="filter_customer" id="client_<?php echo $producs["grid"] . '_' . $cn; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value=""><?= $producs['client_name']; ?></option>    
                                                                                            <?php
                                                                                            $sql_cli = "select id,name from sub_client order by name";
                                                                                            $result_cli = mysqli_query($con, $sql_cli);
                                                                                            while ($row_client = mysqli_fetch_assoc($result_cli)) { ?>
                                                                                                <option value="<?php echo $row_client['id']; ?>"><?= $row_client['name']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>                                                                        
                                                                                    </td>                                                                        

                                                                        <td>
                                                                            
                                                                            <input type="hidden" name="off-id-<?= $cn ?>" value="<?php echo $producs['cartid'];         ?>"/>
                                                                            <input type="hidden" name="offer_id[]"        value="<?php echo $producs['cartid'];         ?>"/>                                                                            
                                                                            <input type="hidden" name="product_name[]"    value="<?php echo $producs['name'];           ?>"/>
                                                                            <input type="hidden" name="product_size[]"    value="<?php echo $producs["sizename"];       ?>"/>                                                                            


                                                                            
                                                                            <input type="hidden" name="grid_id[]"         value="<?php echo $producs['grid']; ?>"/>
                                                                            <input type="hidden" name="grower_id[]"       value="<?php echo $producs['grower_id']; ?>"/>
                                                                            <input type="hidden" name="box_qty[]"         value="<?php echo $producs['boxqty']; ?>"/>
                                                                            <input type="hidden" name="box_type[]"        value="<?php echo $producs['boxtype']; ?>"/>
                                                                            <input type="hidden" name="product_price[]"   value="<?php echo $producs['price']; ?>"/>                                                                            
                                                                            <input type="hidden" name="bunch_size[]"      value="<?php echo $bunch_size_s; ?>"/>
                                                                            <input type="hidden" name="is_bunch[]"        value="<?php echo $row_bunchSize['is_bunch']; ?>"/>
                                                                            
                                                                            <input type="hidden" name="btn_add_to_cart"   value="Submit"/>

                                                                       </td>



                                    
                                    

                                                                                                                                                
                                    <td>
                                        
                                        <input type="submit" name="btn_add_to_cart" class="btn btn-success btn-xs relative" style="border:0px solid #fff; " value="Save"   
                                              onclick="add_to_cart(<?= $producs['cartid'] ?>,<?= $userSessionID ?>,<?= $producs['grower_id']; ?>,<?= $producs["grid"]; ?>,<?= $producs['id_order'] ?>,<?= $producs["request_id"]; ?>,<?= $cn ?> );"/>
                                                    
                                                    
                                                                                                                                                            
                                                    
                                        <!--Offer Modal Start xxxxxxxxxxxxxxxxxxx-->
                                        <div class="modal fade open_offer_modal<?= $i ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                                        <h4 class="modal-title" id="myLargeModalLabel"><?= $producs['qty'] . " " . $box_type["name"] ?>
                                                            Units <?= $producs["subs"] ?> <?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] ?> cm</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Grower</th>
                                                                 <!--   <th>Boxes offered</th> -->
                                                                    <th align="center"> Product </th>
                                                                    <th>Price</th>
                                                                    <th>Change Qty</th>
                                                                    <th>Client</th>                                                                   
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <form action="" method="post" id="add_to_cart_form" class="add_to_cart_form">
                                                                    <?php
                                                                    $al_ready_gro_in = array();
                                                                    $cn = 1;
                                                                    while ($growers = mysqli_fetch_assoc($rs_growers)) {
                                                                        $k = explode("/", $growers["file_path5"]);
                                                                        if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                            echo "<tr class=\"set_border\">";
                                                                        } else {
                                                                            echo "<tr class=\"not_set_border\">";
                                                                        }

                                                                        //-------------------------------------------------------------//
                                                                        $total_bunchs="";
                                                                        $boxes_type="";
                                                                        if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                                            $getGrowerReply = "SELECT offer_id , bunchqty , boxqty 
                                                                                                 FROM grower_offer_reply 
                                                                                                where reject in (0) 
                                                                                                  and request_id  ='" . $producs['cartid'] . "'
                                                                                                  and grower_id ='" . $growers['grower_id'] . "' 
                                                                                                  and offer_id_index='" . $growers["offer_id_index"] . "' limit 1";
                                                                            
                                                                            $rs_getGrowerReply = mysqli_query($con, $getGrowerReply);
                                                                            $tot_bunch_qty = 0;
                                                                            while ($row_rs_getGrowerReply = mysqli_fetch_array($rs_getGrowerReply)) {
                                                                                $tot_bunch_qty = $tot_bunch_qty + $row_rs_getGrowerReply['boxqty'];
                                                                            }
                                                                            $total_bunchs= $tot_bunch_qty;
                                                                            if ($producs["boxtype"] != "") {
                                                                                $temp = explode("-", $producs["boxtype"]);
                                                                                $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                                                                                $rs_box_type = mysqli_query($con, $sel_box_type);
                                                                                $box_type = mysqli_fetch_array($rs_box_type);
                                                                                $type_box = box_name($box_type["name"]);
                                                                                $boxes_type= $type_box;
                                                                            }
                                                                        }
                                                                        //---------------------------------------------------------------------//

                                                                        //---------------------------------------------------------------------//
                                                                        $getbunchSize = "SELECT is_bunch_value,bunch_value,is_bunch  FROM grower_product_bunch_sizes WHERE grower_id = '" . $growers['grower_id'] . "' AND product_id = '" . $producs['product'] . "' 
                                                                            AND sizes = '" . $producs['sizeid'] . "'";
                                                                        $rs_bunchSize = mysqli_query($con, $getbunchSize);
                                                                        $row_bunchSize = mysqli_fetch_array($rs_bunchSize);
                                                                        $bunch_size_s = "";

                                                                        if ($row_bunchSize['bunch_value'] == 0) {
                                                                            $bunch_size_s = $row_bunchSize['is_bunch_value'];
                                                                        } else {
                                                                            //$bunch_size_s = $row_bunchSize['bunch_value'];
                                                                        }


                                                                        //---------------------------------------------------------------------//


                                                                        ?>
                                                                        <td><?= $growers['growers_name']; ?></td>
                                                                        
                                                                        <!--<td><?=$total_bunchs.' '.$boxes_type ?></td>-->
                                                                        
                                                                            <?php
                                                                            if ($row_bunchSize['bunch_value'] == 0) {
                                                                                ?>                                                                                                                                                
                                                                                    <td><?php $box_type_s = box_type($producs['box_type']); ?>
                                                                                        <?= $growers["product"]. " " ?><?= $growers["size"] . " cm " ?><?= $growers["bunchqty"] . " Bunchess " ?><?= $bunch_size_s . " st/bu "; ?>
                                                                                    </td>                                                                        
                                                                            <?php }else{?>
                                                                                    <td><?php $box_type_s = box_type($producs['box_type']); ?>
                                                                                        <?= $growers["product"]. " " ?><?= $growers["size"] . " cm " ?><?= $growers["bunchqty"] . " Bunches " ?><?= $growers["steams"] . " st/bu "; ?>
                                                                                    </td>                                                                                                                                                        
                                                                            <?php }
                                                                            ?>                                                                        
                                                                        
                                                                        <td>$<?= $growers['price']; ?></td>
                                                                        
                                                                        <td>
                                                                                        <select style="width: 100%; diplay: none;" name="filter_change" id="change_<?php echo $growers["grid"] . '_' . $cn; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                <?php
                                                                                     $boxDataShow = $growers["bunchqty"];
                                                                                for ($ii = $boxDataShow; $ii >= 1; $ii--) {
                                                                                ?>
                                                                                       <option value="<?= $ii ?>"><?= $ii ?></option>
                                                                                <?php } ?>                                         
                                                                                        </select>                                                                        
                                                                        </td>                                                                        
                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="filter_customer" id="client_<?php echo $growers["grid"] . '_' . $cn; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value=""><?= $growers['client_name']; ?></option>    
                                                                                            <?php
                                                                                            $sql_cli = "select id,name from sub_client order by name";
                                                                                            $result_cli = mysqli_query($con, $sql_cli);
                                                                                            while ($row_client = mysqli_fetch_assoc($result_cli)) { ?>
                                                                                                <option value="<?php echo $row_client['id']; ?>"><?= $row_client['name']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>                                                                        
                                                                                    </td>
                                                                        
                                                                        
                                                                        <td>
                                                                            <input type="hidden" name="off-id-<?= $cn ?>" value="<?php echo $producs['cartid']; ?>"/>
                                                                            <input type="hidden" name="offer_id[]" value="<?php echo $producs['cartid']; ?>"/>
                                                                            <input type="hidden" name="grid_id[]" value="<?php echo $growers['grid']; ?>"/>
                                                                            <input type="hidden" name="grower_id[]" value="<?php echo $growers['grower_id']; ?>"/>
                                                                            <input type="hidden" name="grower_pic[]" value="<?php echo $growers['file_path5']; ?>"/>
                                                                            <input type="hidden" name="product_name[]" value="<?php echo $producs['name']; ?>"/>
                                                                            <input type="hidden" name="product_image[]" value="<?php echo $producs['image_path']; ?>"/>
                                                                            <input type="hidden" name="product_size[]" value="<?php echo $producs["sizename"]; ?>"/>
                                                                            <input type="hidden" name="bunch_size[]" value="<?php echo $bunch_size_s; ?>"/>
                                                                            <input type="hidden" name="box_qty[]" value="<?php echo $growers['boxqty']; ?>"/>
                                                                            <input type="hidden" name="box_type[]" value="<?php echo $growers['boxtype']; ?>"/>
                                                                            <input type="hidden" name="is_bunch[]" value="<?php echo $row_bunchSize['is_bunch']; ?>"/>
                                                                            <input type="hidden" name="product_price[]" value="<?php echo $growers['price']; ?>"/>
                                                                            <input type="hidden" name="lfd[]" value="<?php echo $producs["del_date"]; ?>"/>
                                                                            <input type="hidden" name="btn_add_to_cart" value="Submit"/>
                                                                            <?php

                                                                                                                                       
                                                                                if ($growers["reject"] != 1) {
                                                                                    $tdate2 = date("Y-m-d");
                                                                                    $date12 = date_create($grower_offer["date_added"]);
                                                                                    $date22 = date_create($tdate2);
                                                                                    $interval = $date22->diff($date12);
                                                                                    $checka1 == $interval->format('%R%a');;
                                                                                    $expired = 0;
                                                                                    
                                                                                    if ($checka1 <= 2) {
                                                                                        $sel_buyer_info = "select live_days from buyers where id='" . $grower_offer["buyer_id"] . "'";
                                                                                        $rs_buyer_info = mysqli_query($con, $sel_buyer_info);
                                                                                        $buyer_info = mysqli_fetch_array($rs_buyer_info);
                                                                                        
                                                                                        if ($buyer_info["live_days"] > 0) {
                                                                                            $tdate = date("Y-m-d");
                                                                                            $tdate;
                                                                                            $date1 = date_create($producs['lfd']);
                                                                                            $date2 = date_create($tdate);
                                                                                            $interval = $date2->diff($date1);
                                                                                            $checka2 = $interval->format('%R%a');
                                                                                            if ($checka2 == 0) {
                                                                                                $expired = 1;
                                                                                            }
                                                                                            if ($checka2 >= $buyer_info["live_days"]) {
                                                                                                $expired = 0;
                                                                                            } else {
                                                                                                $expired = 1;
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        $expired = 1;
                                                                                    }
                                                                                    if ($growers["status"] > 0) {
                                                                                        ?>
                                                                                        <a href="javascript:void(0);" class="btn btn-success btn-xs relative" style="border:0px solid #fff; background-color: #d68910">Save </a>
                                                                                        <?php

                                                                                    } else if ($growers["status"] == 0) {
                                                                                        if ($info["active"] == 'suspended') {
                                                                                            if ($expired == 0) {
                                                                                                ?>
                                                                                                <a href="<?php echo SITE_URL; ?>suspend.php?id=<?= $info["id"] ?>" class="btn btn-success btn-xs relative"
                                                                                                   style="border:0px solid #fff; background-color: #f4d03f ">Confirm</a>

                                                                                            <?php } else { ?>
    
                                                                                                <?php
                                                                                            }
                                                                                        } else {
                                                                                            if ($expired == 0) {
                                                                                                ?>
                                                                                                <input type="submit" name="btn_add_to_cart" class="btn btn-success btn-xs relative" style="border:0px solid #fff; " value="Save"   
                                                                                                       onclick="add_to_cart(<?= $producs['cartid'] ?>,<?= $userSessionID ?>,<?= $growers['grower_id']; ?>,<?= $growers["grid"]; ?>,<?= $producs['id_order'] ?>,<?= $growers["request_id"]; ?>,<?= $cn ?> );"/>

                                                                                            <?php } else { ?>
 
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                    } else {
                                                                                        ?>

                                                                                        <?php
                                                                                        $temp_offer_date = explode("-", $growers["accept_date"]);
                                                                                        $temp_offer = $temp_offer_date[1] . "-" . $temp_offer_date[2] . "-" . $temp_offer_date[0];
                                                                                        echo $temp_offer;
                                                                                    }
                                                                                } else {
                                                                                    ?>
                                                                                    <a class="btn btn-danger btn-xs relative" style="border:0px solid #fff;">Rejected : <?= $growers["reason"] ?></a>
                                                                                    <?php
                                                                                }
                                                                                
                                                                      //      }     fin quiebre
                                                                            
                                                                            
                                                                            ?>
                                                                        </td>
                                                                        <?php array_push($al_ready_gro_in, $growers['grower_id'] . "-" . $growers["offer_id_index"]);

                                                                        $cn++;
                                                                        ?>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </form>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!--Offer Modal End-->
                                    </td>
                                </tr>
                                <?php
                                $cn++;
                            }
                            
                            
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>

<?php require_once '../includes/footer_new.php'; ?>

<script type='text/javascript'>
    $(window).load(function () {
        $('#loading').css("display", "none");
    });
</script>
<script type="text/javascript">
    jQuery(window).ready(function () {
        loadScript('/assets/js/jquery/jquery-ui.min.js', 
        function () {
            /** jQuery UI **/
            loadScript('/assets/js/jquery/jquery.ui.touch-punch.min.js', 
            function () {
                /** Mobile Touch Slider **/
                loadScript(plugin_path + 'form.slidebar/jquery-ui-slider-pips.min.js', 
                function () { /** Slider Script **/
                    /** Slider 5******************** **/
                    var id = "";
                    jQuery(".slider5").slider({
                        //value: 0.01, 
                        animate: true,
                        min: 0.05,
                        max: 2,
                        step: 0.01,
                        range: "min",
                        slide: function (event, ui) {

                              jQuery(".select_price_cls").val(ui.value);                         
                            var idx = $(this).attr("id");
                            var arr = idx.split('_');
                            
                              $("#grower_price_" + arr[1]).val(+ui.value); 
                                                        
                            var taxes = $("#tax_porcent_" + arr[1]).val();
                            var red = (ui.value * taxes) / 100;
                            var handling = $("#ha_" + arr[1]).val();
                            var reda = (ui.value * handling) / 100;
                                                       
                            $("#final_price_" + arr[1]).val(ui.value);
                            
                            var grower_pricex = (ui.value / (1 + (taxes / 100) + (handling / 100))).toFixed(2);
                            var handling = (grower_pricex * handling / 100).toFixed(2);
                            var tax = (ui.value); 
                            
                            $("#grower_price_" + arr[1]).val(+grower_pricex);
                            $("#handling_" + arr[1]).val(+handling);
                            $("#tax_" + arr[1]).val(+tax);                            
                        }
                    });

                });
            });
        });
    });
    $(".clss").keypress(function (e) {
        if (e.which == 13) {
            // Acciones a realizar, por ej: enviar formulario.
            //$('#frm').submit();
            var id = $(".clss").attr("id");
            var i = id.split('_');
            var final_price = $("#select_price_cls_" + i[3]).val();
            $("#slider5_" + i[3]).slider("value", final_price);
        }
    });

    function send_request(i) {
        var qty = $('#qty_amount_' + i).val();
        var boxtype = $('#box_type_' + i).val();
        var price = $("#grower_price_" + i).val();
        var ship_cost = $("#ship_cost_" + i).val();
        var tax = $("#tax_" + i).val();
        var handling = $("#handling_" + i).val();
        var final_price = $("#final_price_" + i).val();
        var product_id = $("#productid_" + i).val();
        var order_id = $("#order_" + i).val();
        var grower_id = $("#grower_id_" + i).val();
        var size_id = $("#size_id_" + i).val();
        var comments = $("#contact_" + i).val();
		var totalRes =1;
		 console.log("order_id" + order_id);
		 console.log("Griwer" + grower_id);

        console.log("Este  es el  tax:" + tax);

        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>buyer/re_prod_invent.php',
            data: 'sizeId=' + size_id + '&box_quantity=' + qty + '&productId=' + product_id + '&grower_id=' + grower_id + '&buyerPrice=' + price +
            '&comment=' + comments + '&order_val=' + order_id + '&type_box=' + boxtype +
            '&tax=' + tax + '&ship_cost=' + ship_cost + '&hand_l=' + handling,
            success: function (data_s) {

                console.log(data_s);
                 jQuery("#msg_count").html("0");
                 $(".count").html(totalRes);
                 setTimeout(function () {
                 jQuery("#msg_count").html(totalRes);
                 jQuery('.notify_1').show();
                 }, 7000);
                 jQuery(".progress-bar").animate({width: "100%"}, 6000);
                 $('.count').each(function () {
                 $(this).prop('Counter', 0).animate(
                 {Counter: $(this).text()},
                 {
                 duration: 7000, easing: 'swing', step: function (now) {
                 $(this).text(Math.ceil(now));
                 }
                 });
                 });
                 

            }
        });

        function buynow(gid) {
            var bv = $('#bv-' + gid).val();
            // console.log(bv);
            var fields_name = {};
            fields_name['gid6'] = gid;
            fields_name['bv'] = bv;
            if (bv != 2) {
                var subcategoryname = $('#subcategoryname-' + gid).val();
                var productname = $('#productname-' + gid).val();
                var featurename = $('#featurename-' + gid).val();
                var sizename = $('#sizename-' + gid).val();
                var boxweight = $('#boxweight-' + gid).val();
                var boxvolumn = $('#boxvolumn-' + gid).val();
                var bunchsize = $('#bunchsize-' + gid).val();
                var bunchqty = $('#bunchqty-' + gid).val();
                var shipping = $('#shipping-' + gid).val();
                var handling = $('#handling-' + gid).val();
                var tax = $('#tax-' + gid).val();
                var totalprice = $('#totalprice-' + gid).val();
                var ooprice = $('#ooprice-' + gid).val();
                var boxtypename = $('#boxtypename-' + gid).val();
                var stock = $('#stock-' + gid).val();
                var boxtype = $('#boxtype-' + gid).val();
                var product = $('#product-' + gid).val();
                var sizeid = $('#sizeid-' + gid).val();
                var feature = $('#feature-' + gid).val();
                var grower = $('#grower-' + gid).val();
                fields_name['subcategoryname'] = subcategoryname;
                fields_name['productname'] = productname;
                fields_name['featurename'] = featurename;
                fields_name['sizename'] = sizename;
                fields_name['boxweight'] = boxweight;
                fields_name['boxvolumn'] = boxvolumn;
                fields_name['bunchsize'] = bunchsize;
                fields_name['bunchqty'] = bunchqty;
                fields_name['shipping'] = shipping;
                fields_name['handling'] = handling;
                fields_name['tax'] = tax;
                fields_name['totalprice'] = totalprice;
                fields_name['ooprice'] = ooprice;
                fields_name['boxtypename'] = boxtypename;
                fields_name['stock'] = stock;
                fields_name['boxtype'] = boxtype;
                fields_name['product'] = product;
                fields_name['sizeid'] = sizeid;
                fields_name['feature'] = feature;
                fields_name['grower'] = grower;


            } else {
                var gprice = $('#gprice-' + gid).val();
                var boxtype = $('#boxtype-' + gid).val();
                var stock = $('#stock-' + gid).val();
                var product = $('#product-' + gid).val();
                var sizeid = $('#sizeid-' + gid).val();
                var feature = $('#feature-' + gid).val();
                var grower = $('#grower-' + gid).val();
                var multi = $('#multi-' + gid).val();

                fields_name['gprice'] = gprice;
                fields_name['boxtype'] = boxtype;
                fields_name['stock'] = stock;
                fields_name['product'] = product;
                fields_name['sizeid'] = sizeid;
                fields_name['feature'] = feature;
                fields_name['grower'] = grower;
                fields_name['multi'] = multi;
            }
            //console.log(fields_name);
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/inventory_buy_ajax.php',
                data: fields_name,

                success: function (data) {
                    if (data == 'true') {
                        alert('Your reuqest has been sent...');
                    } else {
                        alert('There is some error. Please try again');
                    }
                }
            });

        }

    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        var productsearch = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '<?php echo SITE_URL; ?>/includes/autosuggest.php?limit=10&search=%QUERY',
                wildcard: '%QUERY'
            }
        });
        $('.autosuggest #typeSearch').typeahead(null, {
            name: 'productsearch',
            source: productsearch
        });
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            //console.log('Selection: ' + suggestion);
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/inventory_search_product.php',
                data: 'name=' + suggestion,
                success: function (data) {
                    $("#pagination_nav").hide();
                    $('.imgDiv').hide();
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });

        /*Josse P*/

        $body = $("body");
        function call_Ajax_Fliter() {
            jQuery("#btn_filter").click(function () {
                var check_value = 0;
                var data_ajax = "";
                if (jQuery("#filter_category").val() == "" &&
                    jQuery("#filter_variety").val() == "" ) {
                    check_value = 1;
                }
                if (check_value == 1) {
                    alert("Please select any one option.");
                }


                else {
                    var filter_category = jQuery("#filter_category").val();
                    var filter_variety = jQuery("#filter_variety").val();

                    jQuery(".ajax_loader_s").css("display", "inline-block");
                    $.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>buyer/getFilterClient.php',
                        data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety  ,
                        success: function (data) {
                            jQuery("#tab_for_filter").html("");
                            $("#pagination_main").hide();
                            jQuery('.search_modal_open').modal('hide');
                            jQuery("#list_inventory").html(data);   //nombre del  id  del <tbody>
                            var pass_delete_f = "";
                            
                            if (filter_category == "") {
                                var filter_category_temp = "0";
                            }
                            else {
                                var filter_category_temp = filter_category;
                            }
                            if (filter_variety == "") {
                                var filter_variety_temp = "0";
                            }
                            else {
                                var filter_variety_temp = filter_variety;
                            }
                            if (filter_color == "") {
                                var filter_color_temp = "0";
                            }
                            else {
                                var filter_color_temp = filter_color;
                            }
                            if (filter_grower == "") {
                                var filter_grower_temp = "0";
                            }
                            else {
                                var filter_grower_temp = filter_grower;
                            }
                            if (filter_size == "") {
                                var filter_size_temp = "0";
                            }
                            else {
                                var filter_size_temp = filter_size;

                            }
                            if (filter_perbunch == "") {
                                var filter_perbunch_temp = "0";
                            } else {
                                var filter_perbunch_temp = filter_perbunch;
                            }
                            if (filter_sfeat == "") {
                                var filter_sfeat_temp = "0";
                            }
                            else {
                                var filter_sfeat_temp = filter_sfeat;

                            }
                            pass_delete_f += "'" + filter_category_text + "'" + ',' + filter_category_temp + ',';
                            pass_delete_f += "'" + filter_variety_text + "'" + ',' + filter_variety_temp + ',';
                            pass_delete_f += "'" + filter_color_text + "'" + ',' + filter_color_temp + ',';
                            pass_delete_f += "'" + filter_grower_text + "'" + ',' + filter_grower_temp + ',';
                            pass_delete_f += "'" + filter_size_text + "'" + ',' + filter_size_temp + ',';
                            pass_delete_f += "'" + filter_sfeat_text + "'" + ',' + filter_sfeat_temp + ',';
                            pass_delete_f += "'" + filter_perbunch_text + "'" + ',' + filter_perbunch_temp;
                            //alert(pass_delete_f);
                            if (filter_category != "") {
                                var pass_click_cate = "'" + filter_category_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_category_filter" cate_label="' + filter_category_text + '" name="hdn_selected_category_filter" value="' + filter_category + '" />' + filter_category_text);


                            }
                            if (filter_variety != "") {
                                var pass_click_cate = "'" + filter_variety_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_variety_filter" cate_label="' + filter_category_text + '" name="hdn_selected_variety_filter" value="' + filter_variety + '" />' + filter_variety_text);


                            }
                            if (filter_color != "") {
                                var pass_click_cate = "'" + filter_color_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_color_filter" cate_label="' + filter_category_text + '" name="hdn_selected_color_filter" value="' + filter_color + '" />' + filter_color_text);


                            }
                            if (filter_grower != "") {
                                var pass_click_cate = "'" + filter_grower_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i>' +
                                    '<input type="hidden" id="hdn_selected_grower_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_grower_filter" ' +
                                    'value="' + filter_grower + '" />' + filter_grower_text);


                            }
                            if (filter_perbunch != "") {
                                var pass_click_cate = "'" + filter_perbunch_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i><input type="hidden" id="hdn_selected_perbunch_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_perbunch_filter" ' +
                                    'value="' + filter_perbunch + '" />' + filter_perbunch_text);
                            }
                            if (filter_size != "") {
                                var pass_click_cate = "'" + filter_size_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i>' +
                                    '<input type="hidden" id="hdn_selected_size_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_size_filter" ' +
                                    'value="' + filter_size + '" />' + filter_size_text);
                            }

                            if (filter_sfeat != "") {
                                var pass_click_cate = "'" + filter_sfeat_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i><input type="hidden" id="hdn_selected_sfeat_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_sfeat_filter" ' +
                                    'value="' + filter_sfeat + '" />' + filter_sfeat_text);

                            }
                            jQuery("#tab_for_filter").show();
                            jQuery.ajax({
                                type: 'post',
                                url: '<?php echo SITE_URL; ?>buyer/getFilterInventPa.php',
                                data: 'filter_category=' + filter_category +
                                '&filter_variety=' + filter_variety +
                                '&filter_color=' + filter_color +
                                '&filter_grower=' + filter_grower +
                                '&filter_size=' + filter_size +
                                '&filter_sfeat=' + filter_sfeat +
                                '&filter_perbunch=' + filter_perbunch,
                                success: function (data) {
                                    jQuery("#pagination_nav").html(data);
                                    jQuery(".ajax_loader_s").hide();
                                }
                            });
                        }
                    });
                }
            });
        }

        call_Ajax_Fliter();
        /* 21-10-2016 */
    });
    /*aumentado por mi*/
    function click_Ajax(page_number) {
        var check_value = 0;
        var data_ajax = "";
        if (jQuery("#filter_category").val() == "" && jQuery("#filter_variety").val() == "" && jQuery("#filter_color").val() == "" && jQuery("#filter_grower").val() == ""
            && jQuery("#filter_size").val() == "" && jQuery("#filter_sfeat").val() == "" && jQuery("#filter_perbunch").val() == "") {
            check_value = 1;
        }
        if (check_value == 1) {
            alert("Please select any one option.");
        }

        else {
            var filter_category = jQuery("#hdn_selected_category_filter").val();
            if (typeof filter_category === "undefined") {
                filter_category = "";
            }
            var filter_variety = jQuery("#hdn_selected_variety_filter").val();
            if (typeof filter_variety === "undefined") {
                filter_variety = "";


            }
            var filter_color = jQuery("#hdn_selected_color_filter").val();
            if (typeof filter_color === "undefined") {
                filter_color = "";
            }
            var filter_grower = jQuery("#hdn_selected_grower_filter").val();
            if (typeof filter_grower === "undefined") {
                filter_grower = "";
            }
            var filter_size = jQuery("#hdn_selected_size_filter").val();
            if (typeof filter_size === "undefined") {
                filter_size = "";
            }

            var filter_perbunch = jQuery("#hdn_selected_perbunch_filter").val();
            if (typeof filter_perbunch === "undefined") {
                filter_perbunch = "";
            }

            var filter_sfeat = jQuery("#hdn_selected_sfeat_filter").val();
            if (typeof filter_sfeat === "undefined") {
                filter_sfeat = "";
            }

            $body.addClass("loading");

            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/getFilterInvent.php',
                data: 'filter_category=' + filter_category +
                '&filter_variety=' + filter_variety +
                '&filter_color=' + filter_color +
                '&filter_grower=' + filter_grower +
                '&filter_size=' + filter_size +
                '&filter_perbunch=' + filter_perbunch +
                '&filter_sfeat=' + filter_sfeat +
                '&page_number=' + page_number,

                success: function (data) {
                    jQuery("#tab_for_filter").html("");
                    $("#pagination_main").hide();
                    /************************************************************aqui deseleccione  con todo  lo  de arriba*/
                    jQuery('.search_modal_open').modal('hide');
                    jQuery("#list_inventory").html(data);
                    jQuery("#tab_for_filter").show();
                    jQuery.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>buyer/getFilterInventPa.php',
                        data: 'filter_category=' + filter_category +
                        '&filter_variety=' + filter_variety +
                        '&filter_color=' + filter_color +
                        '&filter_grower=' + filter_grower +
                        '&filter_size=' + filter_size +
                        '&filter_perbunch=' + filter_perbunch +
                        '&filter_sfeat=' + filter_sfeat +
                        '&page_number=' + page_number,
                        success: function (data) {
                            jQuery("#pagination_nav").html(data);
                            $body.removeClass("loading");

                        }

                    });

                }

            });

        }

    }


    function deleteFilter(pass_click_cate, filter_category_text, filter_category, filter_variety_text, filter_variety, filter_color_text, filter_color,
                          filter_grower_text, filter_grower, filter_size_text, filter_size, filter_perbunch_text, filter_perbunch, filter_sfeat_text, filter_sfeat) {
        var a_tag_html = $("#tab_for_filter").find("a").length
        if (a_tag_html == "1") {
            location.reload();
        }
        else {
            if (pass_click_cate == filter_category_text) {
                filter_category = "";
            }
            if (pass_click_cate == filter_variety_text) {
                filter_variety = "";
            }
            if (pass_click_cate == filter_color_text) {
                filter_color = "";
            }
            if (pass_click_cate == filter_grower_text) {
                filter_grower = "";
            }
            if (pass_click_cate == filter_size_text) {
                filter_size = "";
            }
            if (pass_click_cate == filter_perbunch_text) {
                filter_perbunch = "";
            }
            if (pass_click_cate == filter_sfeat_text) {
                filter_sfeat = "";
            }
            var check_value = 0;
            var data_ajax = "";
            if (filter_category == "" &&
                filter_variety == "" &&
                filter_color == "" &&
                filter_grower == "" &&
                filter_size == "" &&
                filter_perbunch == "" &&
                filter_sfeat == "") {
                check_value = 1;
            }
            if (check_value == 1) {
                alert("Please select any one option.");
            }
            else {
                if (filter_category == "0") {
                    filter_category = "";
                }
                if (filter_variety == "0") {
                    filter_variety = "";
                }
                if (filter_color == "0") {
                    filter_color = "";
                }
                if (filter_grower == "0") {
                    filter_grower = "";
                }
                if (filter_size == "0") {
                    filter_size = "";
                }
                if (filter_perbunch == "0") {
                    filter_perbunch = "";
                }
                if (filter_sfeat == "0") {
                    filter_sfeat = "";
                }
                $body.addClass("loading");
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>buyer/getFilterInvent.php',
                    data: 'filter_category=' + filter_category +
                    '&filter_variety=' + filter_variety +
                    '&filter_color=' + filter_color +
                    '&filter_grower=' + filter_grower +
                    '&filter_size=' + filter_size +
                    '&filter_perbunch=' + filter_perbunch +
                    '&filter_sfeat=' + filter_sfeat +
                    '&page_number=1',
                    success: function (data) {
                        $body.removeClass("loading");
                        jQuery("#tab_for_filter").html("");
                        $("#pagination_main").hide();
                        jQuery('.search_modal_open').modal('hide');
                        jQuery("#list_inventory").html(data);
                        var pass_delete_f = "";
                        if (filter_category == "") {
                            var filter_category_temp = "0";
                        }
                        else {
                            var filter_category_temp = filter_category;

                        }
                        if (filter_variety == "") {
                            var filter_variety_temp = "0";

                        }
                        else {
                            var filter_variety_temp = filter_variety;
                        }
                        if (filter_color == "") {
                            var filter_color_temp = "0";
                        }
                        else {
                            var filter_color_temp = filter_color;
                        }
                        if (filter_grower == "") {
                            var filter_grower_temp = "0";
                        }
                        else {
                            var filter_grower_temp = filter_grower;
                        }
                        if (filter_size == "") {
                            var filter_size_temp = "0";
                        }
                        else {
                            var filter_size_temp = filter_size;
                        }
                        if (filter_perbunch == "") {
                            var filter_perbunch_temp = "0";
                        } else {
                            var filter_perbunch_temp = filter_perbunch;


                        }
                        if (filter_sfeat == "") {

                            var filter_sfeat_temp = "0";


                        } else {

                            var filter_sfeat_temp = filter_sfeat;
                        }
                        pass_delete_f += "'" + filter_category_text + "'" + ',' + filter_category_temp + ',';
                        pass_delete_f += "'" + filter_variety_text + "'" + ',' + filter_variety_temp + ',';
                        pass_delete_f += "'" + filter_color_text + "'" + ',' + filter_color_temp + ',';
                        pass_delete_f += "'" + filter_grower_text + "'" + ',' + filter_grower_temp + ',';
                        pass_delete_f += "'" + filter_size_text + "'" + ',' + filter_size_temp + ',';
                        pass_delete_f += "'" + filter_sfeat_text + "'" + ',' + filter_sfeat_temp + ',';
                        pass_delete_f += "'" + filter_perbunch_text + "'" + ',' + filter_perbunch_temp;
                        if (filter_category != "") {
                            var pass_click_cate = "'" + filter_category_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_category_filter" cate_label="' + filter_category_text + '" name="hdn_selected_category_filter" value="' + filter_category + '" />' + filter_category_text);
                        }
                        if (filter_variety != "") {
                            var pass_click_cate = "'" + filter_variety_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_variety_filter" cate_label="' + filter_category_text + '" name="hdn_selected_variety_filter" value="' + filter_variety + '" />' + filter_variety_text);
                        }
                        if (filter_color != "") {
                            var pass_click_cate = "'" + filter_color_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_color_filter" cate_label="' + filter_category_text + '" name="hdn_selected_color_filter" value="' + filter_color + '" />' + filter_color_text);
                        }
                        if (filter_grower != "") {
                            var pass_click_cate = "'" + filter_grower_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i>' +
                                '<input type="hidden" id="hdn_selected_grower_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_grower_filter" ' +
                                'value="' + filter_grower + '" />' + filter_grower_text);
                        }
                        if (filter_perbunch != "") {
                            var pass_click_cate = "'" + filter_perbunch_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i><input type="hidden" id="hdn_selected_perbunch_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_perbunch_filter" ' +
                                'value="' + filter_perbunch + '" />' + filter_perbunch_text);
                        }
                        if (filter_size != "") {
                            var pass_click_cate = "'" + filter_size_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i>' +
                                '<input type="hidden" id="hdn_selected_size_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_size_filter" ' +
                                'value="' + filter_size + '" />' + filter_size_text);
                        }
                        if (filter_sfeat != "") {
                            var pass_click_cate = "'" + filter_sfeat_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i><input type="hidden" id="hdn_selected_sfeat_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_sfeat_filter" ' +
                                'value="' + filter_sfeat + '" />' + filter_sfeat_text);
                        }

                        jQuery("#tab_for_filter").show();
                        jQuery.ajax({
                            type: 'post',
                            url: '<?php echo SITE_URL; ?>buyer/getFilterInventPa.php',
                            data: 'filter_category=' + filter_category +
                            '&filter_variety=' + filter_variety +
                            '&filter_color=' + filter_color +
                            '&filter_grower=' + filter_grower +
                            '&filter_size=' + filter_size +
                            '&filter_perbunch=' + filter_perbunch +
                            '&filter_sfeat=' + filter_sfeat +
                            '&page_number=1',
                            success: function (data) {
                                jQuery("#pagination_nav").html(data);
                                jQuery(".ajax_loader_s").hide();

                            }

                        });


                    }

                });

            }

        }

    }
    /*fin aumentado por mi deleter  filter*/
    $(function () {
        $('form.searchProduct').on('submit', function (e) {
            e.preventDefault();
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/search_request_page.php',
                data: $('form.searchProduct').serialize(),
                success: function (data) {
                    $('.imgDiv').hide();
                    $("#show_hide").toggle("slow");
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });
    });
</script>

<script>   
    
    function add_to_cart(offer, buyer, grower, gor_id ,id_order,request_group,cn) {
    
        var changeQty = $('#change_' + gor_id + '_' + cn).val();
        var subClient = $('#client_' + gor_id + '_' + cn).val();        
        
        
        var k = confirm('You want to assign Bunches to a client?');
            console.log("data " + "offer " + offer + "buyer " + buyer + "grower " + grower + " gor_id " + gor_id + " id_order " + id_order + " request_group " + request_group + " changeQty " + changeQty + " subClient " + subClient + " cn " + cn);        
        if (k == true) { 
            alert('Confirmed assignment'); 
            console.log("data " + "offer " + offer + "buyer " + buyer + "grower " + grower + " gor_id " + gor_id + " id_order " + id_order + " request_group " + request_group + " changeQty " + changeQty + " subClient " + subClient);
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/buyer/offers-viaje.php',
                data: 'offer=' + offer + '&buyer=' + buyer + '&grower=' + grower + '&gor_id=' + gor_id + '&id_order=' + id_order + '&request_group=' + request_group + '&changeQty=' + changeQty + '&subClient=' + subClient,
                success: function (data) {
                    location.reload();
                }   
            });
        }

    }

   
</script>