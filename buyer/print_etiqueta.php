<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
   
    $userSessionID = $_SESSION["buyer"];
    $idcli = $_GET['id_cli'];    
    $idfac = $_GET['b'];
    
    $loteid = $_GET['id_lotp'];
    
    

    // Datos del Client
    $buyerEntity = "select  cl.id , cl.name as namecli, cl.buyer , cl.type_order 
                      from sub_client cl
                     where cl.id = '" . $idcli . "'   " ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
         
   // Datos del Packing            
    
    
    $sqlDetalis="select irs.id             , irs.id_order         , irs.order_serial   , irs.cod_order      , irs.product        ,
                        irs.sizeid         , irs.feature          , irs.noofstems  as steams    , irs.qty as qty_pack  , irs.buyer          ,
                        irs.boxtype        , irs.date_added       , irs.type           , irs.bunches        , irs.box_name       ,
                        irs.lfd            , irs.lfd2             , irs.comment        , irs.box_id         , irs.shpping_method ,
                        irs.isy            , irs.mreject          , irs.bunch_size     , irs.unseen         , irs.req_qty        ,
                        irs.bunches2       , irs.discount         , irs.inventary      , irs.type_price     , irs.id_client      ,
                        irs.id_grower      , irs.special_order    , irs.price          , irs.num_box  as box_qty_pack  , irs.date_ship      ,
                        irs.date_del       , irs.cost             , irs.id_reser       , irs.assign         ,
                        s.price_client     , s.name as subcate_real , p.name as prod_name ,
                        cli.name as subclient,  
                        sz.name as size_name , c.name as colorname
                  from reser_requests irs
                 INNER JOIN product p on irs.product = p.id 
                 INNER join subcategory s on p.subcategoryid = s.id 
                 INNER JOIN sub_client cli on irs.id_client = cli.id 
                 INNER JOIN sizes sz on irs.sizeid = sz.id 
                  left JOIN colors c on p.color_id = c.id 
                 where irs.id_order  = '".$idfac."' 
                   and irs.id_client = '".$idcli."' 
                   and irs.lote      = '".$loteid."'     
                   union
                 select byr.id             , byr.id_order         , byr.order_serial   , byr.cod_order      , byr.product        ,
                        byr.sizeid         , byr.feature          , byr.noofstems  as steams    , byr.qty as qty_pack  , byr.buyer          ,
                        byr.boxtype        , byr.date_added       , byr.type           , byr.bunches        , byr.box_name       ,
                        byr.lfd            , byr.lfd2             , byr.comment        , byr.box_id         , byr.shpping_method ,
                        byr.isy            , byr.mreject          , byr.bunch_size     , byr.unseen         , byr.req_qty        ,
                        byr.bunches2       , byr.discount         , byr.inventary      , byr.type_price     , byr.id_client      ,
                        byr.id_grower      , byr.special_order    , byr.price          , byr.num_box  as box_qty_pack      , byr.date_ship      ,
                        byr.date_del       , byr.cost             , byr.id_reser       , byr.assign         ,
                        s.price_client     , s.name as subcate_real , p.name as prod_name ,
                        cli.name as subclient,  
                        sz.name as size_name , c.name as colorname
                  from buyer_requests byr
                 INNER JOIN product p on byr.product = p.id 
                 INNER join subcategory s on p.subcategoryid = s.id 
                 INNER JOIN sub_client cli on byr.id_client = cli.id 
                 INNER JOIN sizes sz on byr.sizeid = sz.id 
                  left JOIN colors c on p.color_id = c.id 
                 where byr.id_order   = '".$idfac."' 
                   and byr.id_client  = '".$idcli."' 
                   and byr.comment like 'SubClie%'
                 order by subcate_real";
    
      $result   = mysqli_query($con, $sqlDetalis);       
        
      $sqlcount=" select  irs.num_box        
                    from reser_requests irs
                   INNER JOIN product p on irs.product = p.id 
                   INNER join subcategory s on p.subcategoryid = s.id 
                   INNER JOIN sub_client cli on irs.id_client = cli.id 
                   INNER JOIN sizes sz on irs.sizeid = sz.id 
                    left JOIN colors c on p.color_id = c.id 
                   where irs.id_order  = '".$idfac."' 
                     and irs.id_client = '".$idcli."' 
                     and irs.lote      = '".$loteid."'     
                   union
                  select byr.num_box        
                    from buyer_requests byr
                   INNER JOIN product p on byr.product = p.id 
                   INNER join subcategory s on p.subcategoryid = s.id 
                   INNER JOIN sub_client cli on byr.id_client = cli.id 
                   INNER JOIN sizes sz on byr.sizeid = sz.id 
                    left JOIN colors c on p.color_id = c.id 
                   where byr.id_order   = '".$idfac."' 
                     and byr.id_client  = '".$idcli."' 
                     and byr.comment like 'SubClie%'
                   group by num_box";                

        $numpag       = mysqli_query($con, $sqlcount);           
                
        $total_pagina = mysqli_num_rows($numpag);
        
    /////////////////////////////////////////////////////////////
        
                        $sqlbuch0="select sum(irs.qty) as qtypack
                                    from reser_requests irs
                                   INNER JOIN product p on irs.product = p.id 
                                   INNER join subcategory s on p.subcategoryid = s.id 
                                   INNER JOIN sub_client cli on irs.id_client = cli.id 
                                   INNER JOIN sizes sz on irs.sizeid = sz.id 
                                    left JOIN colors c on p.color_id = c.id 
                                   where irs.id_order  = '".$idfac."' 
                                     and irs.id_client = '".$idcli."' 
                                     and irs.lote      = '".$loteid."'     
                                    union
                                   select sum(byr.qty) as qtypack
                                    from buyer_requests byr
                                   INNER JOIN product p on byr.product = p.id 
                                   INNER join subcategory s on p.subcategoryid = s.id 
                                   INNER JOIN sub_client cli on byr.id_client = cli.id 
                                   INNER JOIN sizes sz on byr.sizeid = sz.id 
                                    left JOIN colors c on p.color_id = c.id 
                                   where byr.id_order   = '".$idfac."' 
                                     and byr.id_client  = '".$idcli."' 
                                     and byr.comment like 'SubClie%'
                                   group by id_client";                

                        $totbuches0  = mysqli_query($con, $sqlbuch0); 
                         while($totcab0 = mysqli_fetch_assoc($totbuches0))  {
                             $totalb0 = $totcab0['qtypack'];
                         }        
        
        

    
    //$pdf->AddPage();    
    
//$pdf = new FPDF('P','mm',array(100,127)); // Tama�o tickt 100mm x 127 mm (largo aprox)
 
     $pdf = new PDF();
$pdf->AddPage();   
        $numpag = 1;
        $namecli = $buy['namecli'];    
        $nametot = explode(" ", $namecli);
        
        $name1 = $nametot[0];
        $name2 = $nametot[1];  
        $name3 = $nametot[2];  
        
        $longitud1 = strlen($name1); 
        $longitud2 = strlen($name2);  
        
         if (($longitud1+$longitud2) <= 7) {  
             $name12 = $name1." ".$name2;  
             $name34 = $name3;             
         } else {
             $name12 = $name1;
             $name34 = $name2." ".$name3;             
         }
        
        
        
        
    $pdf->SetFont('Arial','B',30);                    
    $pdf->Cell(60,4,substr($buy['namecli'],0,22),0,1,'L');  
        $pdf->Image('vic44.png',170,5); 
    
  //  $pdf->Cell(60,4,$name12,0,1,'L');                      
  //  $pdf->Ln(8);    
  //  $pdf->Cell(60,4,substr($name34,0,9),0,1,'L');                          
    $pdf->Ln(11);
    //$pdf->SetFont('Arial','B',45);                    
    //$pdf->Cell(20,4,$numpag."/".$total_pagina,0,1,'L'); 
    //                                            $pdf->Ln(5);    
    
    $pdf->SetFont('Arial','B',25);                                                                    
    //$pdf->Cell(25,4,"              bunch: ".$totalb0,0,1,'L');                          
    //$pdf->Ln(8);
    $pdf->SetFont('Arial','B',10);                    
    $pdf->Cell(100,6,'Variety',0,0,'L');
    $pdf->Cell(10,6,'Bun.',0,1,'L');
    //$pdf->Cell(10,4,'Box',0,1,'L');


    $pdf->Cell(70,6,'_______________________________________________________________________________',0,1,'L');  
    
        $tmp_box = 1;
    
    
    while($row = mysqli_fetch_assoc($result))  {

        if ($row['box_qty_pack'] != $tmp_box) {  

                        
                        $pdf->AddPage(); 
                        $numpag = $numpag + 1;

                        $sqlbuch="select sum(irs.qty) as qtypack
                                    from reser_requests irs
                                   INNER JOIN product p on irs.product = p.id 
                                   INNER join subcategory s on p.subcategoryid = s.id 
                                   INNER JOIN sub_client cli on irs.id_client = cli.id 
                                   INNER JOIN sizes sz on irs.sizeid = sz.id 
                                    left JOIN colors c on p.color_id = c.id 
                                   where irs.id_order  = '".$idfac."' 
                                     and irs.id_client = '".$idcli."' 
                                     and irs.lote      = '".$loteid."'        
                                    union
                                   select sum(byr.qty) as qtypack
                                    from buyer_requests byr
                                   INNER JOIN product p on byr.product = p.id 
                                   INNER join subcategory s on p.subcategoryid = s.id 
                                   INNER JOIN sub_client cli on byr.id_client = cli.id 
                                   INNER JOIN sizes sz on byr.sizeid = sz.id 
                                    left JOIN colors c on p.color_id = c.id 
                                   where byr.id_order   = '".$idfac."' 
                                     and byr.id_client  = '".$idcli."' 
                                     and byr.comment like 'SubClie%'
                                   group by id_client";                

                        $totbuches  = mysqli_query($con, $sqlbuch); 
                         while($totcab = mysqli_fetch_assoc($totbuches))  {
                             $totalb = $totcab['qtypack'];
                         }
                        
                        ////////////////////////////////////////////////////////
                        $pdf->SetFont('Arial','B',43);                    
                        $pdf->Cell(60,4,substr($name12,0,16),0,1,'L');                   
                        $pdf->Ln(8);                        
                        $pdf->Cell(60,4,substr($name34,0,9),0,1,'L');                                           
                        $pdf->Ln(11);
                        $pdf->SetFont('Arial','B',45);                                    
                        $pdf->Cell(20,4,$numpag."/".$total_pagina,0,1,'L');
                                                $pdf->Ln(5);
    $pdf->SetFont('Arial','B',25);                                                                                                                    
                        $pdf->Cell(25,4,"              bunch: ".$totalb,0,1,'L');
                        $pdf->Ln(8);                                
                        $pdf->SetFont('Arial','B',10);                    
                        $pdf->Cell(60,4,'Variety',0,0,'L');
                        $pdf->Cell(10,4,'Bun.',0,0,'L');
                        $pdf->Cell(10,4,'Box',0,1,'L');
                        
                        $subStemsGrower=0;    
                   
        }
        
        
        
        $pdf->SetFont('Arial','',10);                                                              
        $pdf->Cell(100,6,$row['subcate_real']." ".$row['prod_name']." ".$row['size_name']." cm ".$row['steams']." st/bu  ".$feature_name." ".$row['name'],0,0,'L');                    
                           
        $pdf->Cell(10,6,$row['qty_pack'],0,1,'L');                                                   
        
       // $pdf->Cell(10,4,$row['box_qty_pack'],0,1,'L');                                                           
                 
        $subStemsGrower= $subStemsGrower + ($row['qty_pack']) ;                   
                
        $tmp_box = $row['box_qty_pack'];   

    }
            
  $pdf->Output('label.pdf','i');
  ?>