<?php
// PO   2-jul-2018
require_once("../config/config_gcp.php");

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}

if (isset($_GET["id"]) && $_GET["id"] != "") {
   $idOrden = $_GET["id"];
}else{
	$idOrden=0;
}

if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}
if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor-account.php");
    die;
}
if (isset($_POST['filter_order']) || isset($_SESSION["filter_order"])) {
    if (isset($_POST['filter_order'])) {
        $_SESSION["filter_order"] = $_POST['filter_order'];
        $_SESSION["order"] = $_POST['order'];
    } else {
        $_POST['filter_order'] = $_SESSION["filter_order"];
        $_POST['order'] = $_SESSION["order"];
    }
} else {
    header("location:" . SITE_URL . "buyer/invent.php?idOrder=".$idOrden); 
    die;
}




//here  upload  main
function pagination($num_record, $per_page, $page,$orden)
{
	$_POST['order']=$orden;
    $total = $num_record;
    $adjacents = "2";
    $page = ($page == 0 ? 1 : $page);

    $start = ($page - 1) * $per_page;
    $counter = 1;
    $prev = $page - 1;
    $next = $page + 1;
    $lastpage = ceil($total / $per_page);
    $lpm1 = $lastpage - 1;
    $pagination = "";

    //echo $page."<".$counter." - 1";

    if ($lastpage > 1) {
        $pagination .= "<ul class='pagination'>";
        if ($page > $counter) {
            $pagination .= "<li><a href='?page=1'>First</a></li>";
            $pagination .= "<li><a href='?page=$prev'>Pervious</a></li>";
        } else {
            $pagination .= "<li class=''><a href='javascript:void(0);' >First</a></li>";
            $pagination .= "<li class=''><a href='javascript:void(0);' >Pervious</a></li>";
        }
        if ($lastpage < 7 + ($adjacents * 2)) {
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page){
                    $pagination .= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
                }else
                     $pagination .= "<li><a href='?order=$orden&page=$counter'>$counter</a></li>";
            }
        } elseif ($lastpage > 5 + ($adjacents * 2)) {
            if ($page < 1 + ($adjacents * 2)) {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class='active'><a href='javascript:void(0);'>$counter</a></li>";
                    else
                         $pagination .= "<li><a href='?order=$orden&page=$counter'>$counter</a></li>";
                }
                $pagination .= "<li class='dot'>...</li>";
                $pagination .= "<li><a href='?page=$lpm1'>$lpm1</a></li>";
                $pagination .= "<li><a href='?page=$lastpage'>$lastpage</a></li>";
            } elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                $pagination .= "<li><a href='?page=1'>1</a></li>";
                $pagination .= "<li><a href='?page=2'>2</a></li>";
                $pagination .= "<li class='dot'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
                    else
                        $pagination .= "<li><a href='?order=$orden&page=$counter'>$counter</a></li>";
                }
                $pagination .= "<li class='dot'>..</li>";
                $pagination .= "<li><a href='?page=$lpm1'>$lpm1</a></li>";
                $pagination .= "<li><a href='?page=$lastpage'>$lastpage</a></li>";
            } else {
                $pagination .= "<li><a href='?page=1'>1</a></li>";
                $pagination .= "<li><a href='?page=2'>2</a></li>";
                $pagination .= "<li class='dot'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination .= "<li class='active'><a href='javascript:void(0);'>$counter</a></li>";
                    else
                         $pagination .= "<li><a href='?order=$orden&page=$counter'>$counter</a></li>";
                }
            }
        }
        if ($page < $counter - 1) {
            $pagination .= "<li><a href='?page=$next'>Next</a></li>";
            $pagination .= "<li><a href='?page=$lastpage'>Last</a></li>";
        } else {
            $pagination .= "<li class=''><a href='javascript:void(0);'>Next</a></li>";
            $pagination .= "<li class=''><a href='javascript:void(0);'>Last</a></li>";
        }
        $pagination .= "</ul>\n";
    }
    return $pagination;
}

/*if ((!isset($_POST['filter_order'])) || (!isset($_SESSION["filter_order"]))) {
    header("location:" . SITE_URL . "invent.php");
    die;
} else {
$_SESSION["filter_order"] = $_POST['filter_order'];
*/
$userSessionID = $_SESSION["buyer"];
$shippingMethod = $_SESSION['shippingMethod'];
$getShippingMethod = "select connections from shipping_method where id = '" . $shippingMethod . "'";
$shippingMethodRes = mysqli_query($con, $getShippingMethod);
$shippingMethodData = mysqli_fetch_assoc($shippingMethodRes);
$shippingMethodArray = unserialize($shippingMethodData['connections']);
$firstConnection = $shippingMethodArray['connection_1'];
$getConnectionType = "select type from connections where id='" . $firstConnection . "'";
$conType = mysqli_query($con, $getConnectionType);
$connectionsType = mysqli_fetch_assoc($conType);
$connectionType = $connectionsType['type'];
foreach ($shippingMethodArray as $connections) {
    $getConnections = "select charges_per_kilo,charges_per_shipment from connections where id='" . $connections . "'";
    $conDatas = mysqli_query($con, $getConnections);
    $connectionDatas = mysqli_fetch_assoc($conDatas);
    $cpk = unserialize($connectionDatas['charges_per_kilo']);
    foreach ($cpk as $perkilo) {
        $chargesPerKilo = $chargesPerKilo + $perkilo;
    }
    $cps = unserialize($connectionDatas['charges_per_shipment']);
    foreach ($cps as $pership) {
        $chargesPerShip = $chargesPerShip + $pership;
    }
}
/*********get the data of session user****************/
if ($stmt = $con->prepare("SELECT id,first_name,last_name,state,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image,handling_fees,shipping,tax FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $state, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image, $handling_fees, $shipping, $tax);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*********If not exist send to home page****************/
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*********If not statement send to home page****************/
    header("location:" . SITE_URL);
    die;
}
$img_url = '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}
$shipping_method = trim($shipping, ",");

if (!isset($_REQUEST["gid6"])) {
    $sel_login_check = "select * from activity where buyer='" . $userID . "' and ldate='" . date("Y-m-d") . "' and ltime='" . $hm . "' and type='buyer' and atype='li'";
    $rs_login_check = mysqli_query($con, $sel_login_check);
    $login_check = mysqli_num_rows($rs_login_check);
    if ($login_check >= 1) {
    } else {
        $name = $first_name . " " . $last_name;
        $activity = "Live Inventory at " . $hm;
        $insert_login = "insert into activity set buyer='" . $userID . "',ldate='" . date("Y-m-d") . "',type='buyer',name='" . $name . "',activity='" . $activity . "',ltime='" . $hm . "',atype='li'";
        mysqli_query($con, $insert_login);
    }
}
$sel_testi = "select gpb.id,g.growers_name as gname,g.id as gid,g.blockstate as gbs 
                from grower_product_box_packing gpb
                left join product p on gpb.prodcutid = p.id
                left join growers g on gpb.growerid=g.id		  
               where g.active='active' and Date_format(gpb.date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y') and Date_format(gpb.date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y') and g.growers_name is not NULL ";
$sel_testi .= " group by g.id order by p.name, g.growers_name";/**/
$rs_testi = mysqli_query($con, $sel_testi);
$total_testi = mysqli_num_rows($rs_testi);

if ($total_testi >= 1) {
    $check_grower = "";
    while ($testi = mysqli_fetch_array($rs_testi)) {
        if ($state > 0 && $testi["gbs"] != "") {
            $temp47 = explode(",", $testi["gbs"]);
            if (in_array($state, $temp47)) {
            } else {
                $check_grower .= $testi["gid"] . ",";
            }
        } else {
            $check_grower .= $testi["gid"] . ",";
        }
    }
}
$check_grower = rtrim($check_grower, ',');
if ($shipping_method > 0) {
    $select_shipping_info = "select name,description from shipping_method where id='" . $shipping_method . "'";
    $rs_shipping_info = mysqli_query($con, $select_shipping_info);
    $shipping_info = mysqli_fetch_array($rs_shipping_info);
}
$sel_connections = "select days from connections where shipping_id='" . $shipping_method . "' order by id desc limit 0,1";
$rs_connections = mysqli_query($con, $sel_connections);
$connections = mysqli_fetch_array($rs_connections);
$days = explode(",", $connections["days"]);
$count1 = sizeof($days);
$res = "";
for ($io = 0; $io <= $count1 - 1; $io++) {
    if ($days[$io] == 0 || $days[$io] == 1) {
        $res .= "6";
        $res .= ",";
    } else {
        $res .= $days[$io] - 1;
        $res .= ",";
    }

}
$avdays = explode(",", $res);
$result = array_unique($avdays);
$cunav = sizeof($result);
$datedd = $shpping_onr;
$tdate = date("Y-m-d");
$date1 = date_create($datedd);
$date2 = date_create($tdate);
$interval = $date2->diff($date1);
$checka2 = $interval->format('%R%a');
if ($checka2 > 0 || $checka2 < 0) {
    $weekday = date('l', strtotime($datedd));


    switch ($weekday) {
        case 'Monday':
            $opq = 1;
            break;
        case 'Tuesday':
            $opq = 2;
            break;
        case 'Wednesday':
            $opq = 3;
            break;
        case 'Thursday':
            $opq = 4;
            break;
        case 'Friday' :
            $opq = 5;
            break;
        case 'Saturday':
            $opq = 6;
            break;
        case 'Sunday':
            $opq = 0;
            break;
    }
    if (in_array($opq, $result)) {
        $next = $opq;
    } else {
        for ($i = 1; $i <= 6; $i++) {
            $opq = $opq + 1;
            if (in_array($opq, $result)) {
                $next = $opq;
                break;
            }
            if ($opq == 6) {
                $opq = 1;
            }
        }
    }

    switch ($next) {

        case 1:
            $dayname = "monday";
            break;
        case 2:
            $dayname = "tuesday";
            break;
        case 3:
            $dayname = "wednesday";
            break;
        case 4:
            $dayname = "thursday";
            break;
        case 5:
            $dayname = "friday";
            break;
        case 6:
            $dayname = "saturday";

    }

    $shpping_onr = date('Y-m-d', strtotime('next ' . $dayname));
} else {
    $tommorow = mktime(date("H"), date("i"), date("s"), date("m"), date("d") + 1, date("Y"));
    $shpping_onr = date("Y-m-d", $tommorow);
}
$tempk = explode("-", $shpping_onr);
$shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
$today = date("Y-m-d");
function getWeekday($date)
{
    return date('w', strtotime($date));
}

$day_of_week = getWeekday($today); // returns 4
switch ($day_of_week) {
    case 0:
        $starting_date = date('Y-m-d', strtotime($today . ' +8 day'));
        $days_add = 8;
        $days_add2 = 13;
        break;
    case 1:
        $starting_date = date('Y-m-d', strtotime($today . ' +7 day'));
        $days_add = 7;
        $days_add2 = 12;
        break;
    case 2:
        $starting_date = date('Y-m-d', strtotime($today . ' +6 day'));
        $days_add = 6;
        $days_add2 = 11;
        break;
    case 3:
        $starting_date = date('Y-m-d', strtotime($today . ' +5 day'));
        $days_add = 5;
        $days_add2 = 10;
        break;
    case 4:
        $starting_date = date('Y-m-d', strtotime($today . ' +11 day'));
        $days_add = 11;
        $days_add2 = 16;
        break;
    case 5:
        $starting_date = date('Y-m-d', strtotime($today . ' +10 day'));
        $days_add = 10;
        $days_add2 = 15;
        break;
    case 6:
        $starting_date = date('Y-m-d', strtotime($today . ' +9 day'));
        $days_add = 9;
        $days_add2 = 14;
        break;
    case 7:
        $starting_date = date('Y-m-d', strtotime($today . ' +7 day'));
        $days_add = 7;
        $days_add2 = 12;
        break;
}
$end_date = date('Y-m-d', strtotime($starting_date . '+5 day'));
if ($starting_date != "" && $end_date != "") {
    function week_number($date)
    {
        return ceil(date('j', strtotime($date)) / 7);
    }

    $week_no = week_number($starting_date);
    switch ($week_no) {
        case 1:
            $option_update = 1;
            break;
        case 2:
            $option_update = 2;
            break;
        case 3:
            $option_update = 1;
            break;
        case 4:
            $option_update = 4;
            break;
        case 5:
            $option_update = 5;
            break;
    }
    $temp_starting_date = explode("-", $starting_date);
    $orginal_starting_date = $temp_starting_date[1] . "-" . $temp_starting_date[2] . "-" . $temp_starting_date[0];
    $temp_end_date = explode("-", $end_date);
    $orginal_end_date = $temp_end_date[1] . "-" . $temp_end_date[2] . "-" . $temp_end_date[0];
}
$page_request = "inventory";
?>
<?php
require_once '../includes/profile-header.php';
require_once "../includes/left_sidebar_buyer.php";
include '/pagination/Pagination.php';
?>

<?php
$sel_products = "select gpb.id from grower_product_box_packing gpb
                   left join product p on gpb.prodcutid = p.id
                   left join growers g on gpb.growerid=g.id
                  where g.active='active' and Date_format(gpb.date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y') and gpb.stock > 0 and p.name is not null order by p.name";

$rs_prodcuts = mysqli_query($con, $sel_products);
$total = mysqli_num_rows($rs_prodcuts);
$num_record = $total;
$display = 50;
/*aqui  esta  el  contador*/

if (!empty($_GET['page'])) {
    $page = $_GET['page'];
    $from = (50 * $page) - 50;
    $display = 50;
    //echo "dentro  aqui";

} else {
    $page = 1;
    $from = 0;
    $display = 50;
}
$dates = date("jS F Y");
$query2 = "select gpb.prodcutid,gpb.comment,gpb.id as gid,gpb.stock as stock,gpb.qty,gpb.sizeid,gpb.feature,gpb.type as bv,
                  gpb.boxname as bvname,
                  gpb.box_type as bvtype,gpb.price,gpb.growerid,p.id,p.name,p.color_id,p.image_path,p.box_type as p_box_type,
                  s.name as subs, g.id as grower_id,g.file_path5,g.growers_name,sh.name as sizename,ff.name as featurename,
                  b.name as boxname,
                  b.width,
                  b.length,
                  b.height,
                  bs.name as bname,bt.name as boxtype,b.type,c.name as colorname,gpb.box_id 
             from grower_product_box_packing gpb
             left join product p      on gpb.prodcutid     = p.id
             left join subcategory s  on p.subcategoryid   = s.id  
             left join colors c       on p.color_id        = c.id 
             left join features ff    on gpb.feature       = ff.id
             left join sizes sh       on gpb.sizeid        = sh.id 
             left join boxes b        on gpb.box_id        = b.id
             left join boxtype bt     on b.type            = bt.id
             left join growers g      on gpb.growerid      = g.id
             left join bunch_sizes bs on gpb.bunch_size_id = bs.id
            ";
if (isset($_POST['add'])) {
    $variable = $_POST['datepicker'];
    $fech = date("Y-n-j");
    if (strtotime($fech) == strtotime($variable)) {
        $dates = date("jS F Y");
        $query2 .= " where g.active='active' and gpb.stock > 0 and p.name is not null  ";
        $query2 .= " order by p.name,s.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER),g.growers_name  LIMIT $from,$display";
        $result2 = mysqli_query($con, $query2);
    } else {
        $date = new DateTime($variable);
        $dates = $date->format('jS F Y');
        $query2 .= "where g.active='active' and gpb.stock > 0  and p.name is not null  ";
        $query2 .= "and gpb.date='" . $variable . "' ";
        $query2 .= " order by p.name,s.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER),g.growers_name  LIMIT $from,$display";
        $result2 = mysqli_query($con, $query2);
    }
} else {
    $query2 .= "where g.active='active' and gpb.stock > 0 and p.name is not null   ";
    $query2 .= " order by p.name,s.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER),g.growers_name  LIMIT $from,$display";
    $result2 = mysqli_query($con, $query2);
}

//$_POST['filter_order']

$sql_order = "select connections from buyer_orders  as  bu  
                left join buyer_shipping_methods  bys on bu.shipping_method=bys.shipping_method_id
                left join shipping_method s  on  bys.shipping_method_id= s.id
               where  bu.id='" . $_POST['filter_order'] . "'";


$row_sql_perkilo = mysqli_query($con, $sql_order);
$row_perkilo = mysqli_fetch_assoc($row_sql_perkilo);

$conections = $row_perkilo['connections'];
$ids_connections = unserialize($conections);
$ids_connect = array();

foreach ($ids_connections as $key => $value) {

    $ids_connect[] = $value;
}
$cost_ship = 0;

for ($i = 0; $i < count($ids_connect); $i++) {
   // $sqlids = "select id,charges_per_kilo,sum_charges_per_kilo  from connections where id='" . $ids_connect[$i] . "'";
    $sqlids = "select id,charges_per_kilo from connections where id='" . $ids_connect[$i] . "'";    
    $row_sqlids = mysqli_query($con, $sqlids);
    $row_sql = mysqli_fetch_assoc($row_sqlids);
    $cost = $row_sql['charges_per_kilo'];
    $cost_un = unserialize($cost);
    $cost_sum = 0;
    foreach ($cost_un as $key => $value) {
        $cost_sum = $cost_sum + $value;
    }
    $cost_ship =  $cost_sum;
}
function weight_query($grower_id, $product_id, $size_id, $feature, $box_id)
{
    $sel_weight = "select weight from grower_product_box_weight 
    where growerid='" . $grower_id . "' and 
          prodcutid='" . $product_id . "' and 
          sizeid='" . $size_id . "' and 
          feature='" . $feature . "' and 
          box_id='" . $box_id . "' 
          order by id desc limit 0,1";
    return $sel_weight;
}

function category()
{
    global $con;
    $category_sql = "SELECT id,name from category order by  name";
    $result_category = mysqli_query($con, $category_sql);
    return $result_category;
}

function subcategory()
{
    global $con;
    $subcategory_sql = "SELECT id,name from subcategory order by  name";
    $result_subcategory = mysqli_query($con, $subcategory_sql);
    return $result_subcategory;
}

function colors()
{
    global $con;
    $getColor = "select * from colors order by name";
    $ColorRes = mysqli_query($con, $getColor);
    return $ColorRes;

}


function growers()
{

    global $con;
    $getGrowers = "select id,growers_name from growers where active = 'active' order by growers_name";
    $GrowersRes = mysqli_query($con, $getGrowers);
    return $GrowersRes;

}

function sizes()
{
    global $con;
    $getSize = "select * from sizes order by length(name),name";
    $SizeRes = mysqli_query($con, $getSize);
    return $getSize;
}

function special_feature()
{
    global $con;
    $getFeatures = "select * from features order by name";
    $featuresRes = mysqli_query($con, $getFeatures);
    return $featuresRes;

}

function bunches()
{
    global $con;
    $getbunch_sizes = "select * from bunch_sizes order by length(name),name;";
    $bunch_sRes = mysqli_query($con, $getbunch_sizes);
    return $bunch_sRes;

}


?>

<link rel="stylesheet" type="text/css" href="/assets/css/loading.css">
<style type="text/css">
    /*This css is fir fixing select dropdown size just only on name-your-price.php page*/
    .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__rendered, .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 40px !important;
        line-height: 36px !important;
        font-size: 14px !important;
        font-family: 'Open Sans', Arial, sans-serif !important;
    }

    /*This css is fir fixing select dropdown size just only on name-your-price.php page*/
    .select2-container--default .select2-selection--single,
    .select2-container--default .select2-selection--single .select2-selection__rendered,
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 40px !important;
        line-height: 36px !important;
        font-size: 14px !important;
        font-family: 'Open Sans', Arial, sans-serif !important;
    }

    td {
        font-size: 14px !important;
        font-family: 'Open Sans', Arial, sans-serif !important;
    }

    .modal_k {
        display: none;
        position: fixed;
        z-index: 1000;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: rgba(255, 255, 255, .8) url('images/ajax-loader.gif') 50% 50% no-repeat;
    }

    body.loading {
        overflow: hidden;
    }

    body.loading .modal_k {
        display: block;
    }

    .pagination > li {
        float: left;
    }

    #middle div.panel-heading {
        height: auto;
    }

    .xs {
        style = "background-color: white";
    }

    .inventory_table img {
        width: 100px;
    }

    .inventory_table td {
        vertical-align: inherit !important;
    }
</style>
<style type="text/css">
    #dvLoading {
        /*/background: #000 url(







    <?php echo SITE_URL; ?>        includes/assets/images/loaders/5.gif"></div>) no-repeat center center;*/
        height: 100px;
        width: 100px;
        position: fixed;
        z-index: 1000;
        left: 50%;
        top: 50%;
        margin: -25px 0 0 -25px;
    }
</style>
<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Price and Availability</h1>
        <ol class="breadcrumb">
            <li><a href="#">Inventory</a></li>
            <li class="active">Buy Flowers</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">

                    <!-- LEFT -->
                    <div class="col-md-12">
                        <div id="content" class="padding-20">
                            <div id="panel-2" class="panel panel-default">
                                <div class="panel-heading">
                                    <span class="title elipsis">
                                        <br>
                                        <strong>Inventory </strong> <!-- panel title -->
                                    </span>
                                    <!-- right options -->
                                    <ul class="options pull-right relative list-unstyled">
                                        <div class="tags_selected" id="tab_for_filter" style="float: left; margin-top: 4px; margin-right: 4px; display: none;"></div>
                                        <li>
                                            <a class="date_filter btn btn-danger btn-xs white" id="dates"><?= $_POST['order'] ?></a>
                                            <a class="date_filter btn btn-danger btn-xs white" id="dates"><?php echo $dates; ?></a>
                                            <a href="#" class="btn btn-primary btn-xs white" data-toggle="modal" data-target=".search_modal_open"><i class="fa fa-filter"></i> Filter</a>
                                        </li>
                                        <div class="modal fade search_modal_open" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <form name="form1" id="form1" method="post" action="buyer/inventory.php" class="modal fade search_modal_open">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                                        </div>
                                                        <!-- Modal Body -->
                                                        <div class="modal-body">
                                                            <div class="panel-body">
                                                                <label>Category</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_category" id="filter_category" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Category</option>
                                                                        <?php
                                                                        $result_category = category();
                                                                        while ($row_category = mysqli_fetch_assoc($result_category)) { ?>
                                                                            <option value="<?= $row_category['id']; ?>"><?= $row_category['name']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <label>Variety</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_variety" id="filter_variety" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Variety</option>
                                                                        <?php
                                                                        $result_subcategory = subcategory();
                                                                        while ($row_subcategory = mysqli_fetch_assoc($result_subcategory)) { ?>
                                                                            <option value="<?php echo $row_subcategory['id']; ?>"><?php echo $row_subcategory['name']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <label>Color</label>
                                                                <div class="form-group">
                                                                    <select name="filter_color" id="filter_color" class="form-control pointer required select2" style="width:100%;">
                                                                        <option value="">Select Color</option>
                                                                        <?php
                                                                        $ColorRes = colors();
                                                                        while ($color = mysqli_fetch_array($ColorRes)) { ?>
                                                                            <option value="<?php echo $color['id'] ?>"><?php echo $color['name'] ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <label>Growers</label>
                                                                <div class="form-group">
                                                                    <select name="filter_grower" id="filter_grower" class="form-control pointer required select2" style="width:100%;">
                                                                        <option value="">Select Growers</option>
                                                                        <?php
                                                                        $GrowersRes = growers();
                                                                        while ($growers = mysqli_fetch_array($GrowersRes)) { ?>
                                                                            <option value="<?php echo $growers['id'] ?>"><?php echo $growers['growers_name'] ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <label>Size</label>
                                                                <div class="form-group">
                                                                    <select name="filter_size" id="filter_size" class="form-control pointer required select2" style="width:100%;">
                                                                        <option value="">Select Size</option>
                                                                        <?php
                                                                        $SizeRes = sizes();
                                                                        while ($featursi = mysqli_fetch_array($SizeRes)) { ?>
                                                                            <option value="<?php echo $featursi['id'] ?>"><?php echo $featursi['name'] ?>cm</option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <label>Special feature</label>
                                                                <!--filter_special_feature-->
                                                                <div class="form-group">
                                                                    <select name="filter_sfeat" id="filter_sfeat" class="form-control pointer required select2" style="width:100%;">
                                                                        <option value="">Select Special Feature</option>
                                                                        <?php
                                                                        $features = special_feature();
                                                                        while ($features = mysqli_fetch_array($featuresRes)) { ?>
                                                                            <option value="<?php echo $features['id'] ?>"><?php echo $features['name'] ?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <label>Stems Per Bunch</label>
                                                                <div class="form-group">
                                                                    <select name="filter_perbunch" id="filter_perbunch" class="form-control pointer required select2" style="width:100%;">
                                                                        <option value="">Stems Per Bunch</option>
                                                                        <?php
                                                                        $bunch_sRes = bunches();
                                                                        while ($features = mysqli_fetch_array($bunch_sRes)) { ?>
                                                                            <option value="<?php echo $features['id'] ?>"><?php echo $features['name'] ?>&nbsp;st/bu</option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div>
                                                                <label>Date</label>
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control datepicker" data-format="yyyy-mm-dd" data-lang="en" data-RTL="false" id="datepicker" name="datepicker">
                                                                </div>
                                                                <label>Order</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_variety" id="filter_variety" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Order</option>
                                                                        <?php
                                                                        $subcategory_sql = "select  id, qucik_desc  from  buyer_orders where  buyer_id='" . $userSessionID . "'";
                                                                        $result_subcategory = mysqli_query($con, $subcategory_sql);
                                                                        while ($row_subcategory = mysqli_fetch_assoc($result_subcategory)) { ?>
                                                                            <option value="<?php echo $row_subcategory['id']; ?>"><?php echo $row_subcategory['qucik_desc']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <!-- Modal Footer -->
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            <button id="btn_filter" class="btn btn-primary apply" type="button">Apply</button>
                                                            <img class="ajax_loader_s" style="display: none;" src="<?php echo SITE_URL; ?>images/ajax-loader.gif"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                </div>
                                <!-- panel content -->
                                <div class="panel-body">
                                    <div id='loading'>
                                        <div class=" cssload-spin-box">
                                            <img src="<?php echo SITE_URL; ?>/includes/assets/images/loaders/5.gif">

                                        </div>
                                    </div>
                                    <!--class  for  search-->
                                    <form class="clearfix well well-sm search-big nomargin" action="#" method="get">
                                        <div class="autosuggest fancy-form input-group" data-minLength="1" data-queryURL="<?php echo SITE_URL ?>/includes/autosuggest.php?limit=10&search=">
                                            <div class="input-group-btn">
                                                <button data-toggle="dropdown" class="btn btn-default input-lg dropdown-toggle noborder-right" type="button" aria-expanded="false">Filter <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li class="active"><a href="#"><i class="fa fa-check"></i> Everything</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="j#">Growers</a></li>
                                                    <li><a href="j#">Product</a></li>
                                                    <li><a href="j#">Category</a></li>
                                                    <li><a href="#">Other</a></li>
                                                </ul>
                                            </div>
                                            <input type="text" name="src" id="typeSearch" placeholder="Search From Here..." class="form-control typeahead"/>
                                            <div class="input-group-btn">
                                                <button class="btn btn-default input-lg noborder-left" type="submit"><i class="fa fa-search fa-lg nopadding"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="dataRequest">
                                        <table class="table table-hover inventory_table" id="tablex" name="tablex">
                                            <thead>
                                            <tr>
                                                <th>Grower</th>
                                                <th>Category</th>
                                                <th>Product</th>
                                                <th>Packs</th>
                                                <th align="center">Grower<br>Price</th>                                                
                                                <th align="center">Stem/<br>Bunch</th>
                                                <th align="center">Boxes<br> Available</th>
                                                <th align="center"> Box size</th>
                                                <th>Send Request</th>
                                            </tr>
                                            </thead>
                                            <tbody id="list_inventory">
                                            <?php
                                            $i = 0;
                                            while ($products = mysqli_fetch_array($result2)) {
                                                $price["price"] = $products["price"];
                                                $sel_weight = weight_query($products["growerid"], $products["prodcutid"], $products["sizeid"], $products["feature"], $products["box_id"]);
                                                $rs_weight = mysqli_query($con, $sel_weight);
                                                $weight = mysqli_fetch_array($rs_weight);
                                                $growers["box_weight"] = $weight["weight"];
                                                if ($growers["shipping_method"] > 0) {
                                                    $total_shipping = 0;
                                                    $sel_connections = "select type,shipping_rate,addtional_rate,box_price,box_weight_arranged,price_per_box from connections where shipping_id='" . $growers["shipping_method"] . "'";
                                                    $rs_connections = mysqli_query($con, $sel_connections);
                                                    while ($connections = mysqli_fetch_array($rs_connections)) {
                                                    }
                                                }
                                                $k = explode("/", $products["file_path5"]);
                                                $data = getimagesize(SITE_URL . "user/logo2/" . $k[1]);
                                                $logourl = SITE_URL . "user/logo/" . $k[1];
                                                ?>
                                                <input type="hidden" id="bv-<?= $products["gid"] ?>" value="<?php echo $products["bv"]; ?>"/>
												 <input type="hidden" id="<?= $products["gid"] ?>" value="<?= $products["gid"] ?>"/>
                                                <?php if ($products["bv"] != 2) { ?>
                                                    <?php
                                                    $total_price = $price["price"];
                                                    $total_taxs = 0;
                                                    $total_handling_feess = 0;
                                                    $total_product_price = $price["price"];
                                                    //before  $info["tax"] $tax
                                                    if ($tax != 0) {
                                                        $total_price = $total_price + (($price["price"] * $tax) / 100);
                                                        $total_taxs = ($price["price"] * $tax) / 100;
                                                    }
                                                    //before  $info["handling_fees"]
                                                    if ($handling_fees != 0) {
                                                        $total_price = $total_price + (($price["price"] * $handling_fees) / 100);
                                                        $total_handling_feess = ($price["price"] * $handling_fees) / 100;
                                                    }
                                                    $total_price = $total_price + $chargesPerKilo;
                                                    $total_price = round(($total_price), 2);
                                                    $final_price2 = $total_price;


                                                } else {
                                                    $sel_products_box = "select * from grower_box_products where box_id ='" . $products["gid"] . "'";
                                                    $rs_product_box = mysqli_query($con, $sel_products_box);
                                                    $total_product_box = mysqli_num_rows($rs_product_box);
                                                    $sel_products_box_price = "SELECT SUM(price*bunchqty),SUM(bunchsize*bunchqty) 
                                                    FROM grower_box_products where box_id ='" . $products["gid"] . "'";
                                                    $rs_products_box_price = mysqli_query($con, $sel_products_box_price);
                                                    $products_box_price = mysqli_fetch_array($rs_products_box_price);
                                                    $final_multi_price_qty = $products_box_price['SUM(bunchsize*bunchqty)'];
                                                    $final_multi_price = ($products_box_price['SUM(price*bunchqty)'] / $final_multi_price_qty);
                                                }

                                                $total_price = $price["price"];
                                                $total_taxs = 0;
                                                $total_handling_feess = 0;
                                                //before $info["tax"] $tax
                                                if ($tax != 0) {
                                                    $total_price = $total_price + (($price["price"] * $tax) / 100);
                                                    $total_taxs = ($price["price"] * $tax) / 100;
                                                }
                                                if ($handling_fees != 0) {
                                                    //$info["handling_fees"] $handling_fees
                                                    $total_price = $total_price + (($price["price"] * $handling_fees) / 100);
                                                    $total_handling_feess = ($price["price"] * $handling_fees) / 100;
                                                }
                                                $w = ($products["width"] * $products["length"] * $products["height"] * $cost_ship) / 6000;
                                                $z = $products["bname"] * $products["qty"];
                                                $product_shipping = round(($w / $z), 2);

                                                if ($product_shipping > 0) {
                                                    $total_price = $total_price + $product_shipping;
                                                    $product_shippings = $product_shipping;
                                                }
                                                $total_price = $total_price + $chargesPerKilo;
                                                $total_price = round(($total_price), 2);
                                                if ($products['p_box_type'] == "0") {
                                                    $typ_pro_st_bu = "Stem";
                                                } else {
                                                    $typ_pro_st_bu = "Bunch";
                                                }


                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        $getprofile = "SELECT profile_image,file_path5 FROM growers WHERE id='" . $products["growerid"] . "'";
                                                        $row_getprofile = mysqli_query($con, $getprofile);
                                                        $row_Profile = mysqli_fetch_assoc($row_getprofile);
                                                        
                                                        if ($row_Profile['profile_image'] != "") { ?>
                                                            <img width="50" src="<?php echo SITE_URL . "user/" . $row_Profile['file_path5']; ?>">
                                                        <?php } else if ($row_Profile['file_path5'] != "") { ?>
                                                            <img width="100" src="<?php echo SITE_URL . "user/" . $row_Profile['file_path5']; ?>">
                                                        <?php } ?>
                                                            
                                                        <!--Modal image for 3452 00-->
                                                        <div class="modal fade bs-example-modal-sm" id="single_product_<?php echo $products["gid"] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
                                                             style="display: none;">
                                                            <div class="modal-dialog modal-sm">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                                                                        <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;"><img width="100%" src="<?php echo $logourl ?>"/></h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!---->
                                                    </td>
                                                    <td><?= $products["subs"] ?></td>
                                                    <td>
                                                        <a href="" data-toggle="modal" data-target="#single_product_modal<?php echo $products["gid"] ?>">
                                                            <?php echo $products['name'] . " " . $products["colorname"] . " " . $products["sizename"] . " cm " . $products['featurename'] . $products['bname'] . " St/Bu"; ?>
                                                        </a>
                                                        <div class="modal fade bs-example-modal-sm" id="single_product_modal<?php echo $products["gid"] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog modal-sm" style="opacity: 0.80%">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                                                                        <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;"><?= $products["name"] ?> <?= $products["colorname"] ?> <?= $products["featurename"] ?></h4>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <img src="<?php echo SITE_URL . $products["image_path"] ?>" style="width: 250px;height: 250px">
                                                                            <h7 id="myLargeModalLabel3" class="modal-header" style="font-size: 14px;">Fresh Life Floral</h7>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-body"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><?= $products["bname"] * $products["qty"] ?></td>
                                                    <td>
                                                        <a href="" data-toggle="modal" data-target="#single_product_price<?php echo $products["gid"] ?>">$<?php echo sprintf("%.2f", $price["price"]) ?> </a>
                                                        <!--Modal image for 3452-->
                                                        <div class="modal fade bs-example-modal-sm" id="single_product_price<?php echo $products["gid"] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
                                                             style="display: none;">
                                                            <div class="modal-dialog modal-sm">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                                                                        <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;">ALL IN PRICE</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <p>Grower Price.....................$<?= sprintf("%.2f", $price["price"]) ?></p>
                                                                                <p>Shipping............................$<?= sprintf("%.2f", $product_shipping) ?></p>
                                                                                <p>Tax.....................................$<?= sprintf("%.2f", $total_taxs) ?></p>
                                                                                <p>Handling............................$<?= sprintf("%.2f", $total_handling_feess) ?></p>
                                                                                <!--<hr>-->
                                                                                <div class="divider divider-dotted"><!-- divider --></div>
                                                                                <p style="color: #273746"><strong>FinalPrice.......................$<?= sprintf("%.2f", $total_price) ?></strong></p>
                                                                                <!--<?= $cost_ship ?>-->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td><?= $typ_pro_st_bu ?></td>
                                                    <td><?= $products["stock"] . "" . $products["boxtype"] ?></td>
                                                    <td><?= $products['width'] ?>x<?= $products['length'] ?>X<?= $products['height'] ?></td>
                                                    <td>
                                                        <button onclick="set_price(<?= $i ?>,<?= $total_price ?>)" id="btn_<?php echo $i ?>" class="btn btn-default btn-xs modal_click_1 btn_modal" data-toggle="modal" data-target=".qty_modal_<?php echo $i; ?>" style="background-color:#212f3d;color:white">
                                                            <i class="fa fa-send"></i> Request
                                                        </button>
                                                        <div class="modal fade qty_modal_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                            <div class="modal-dialog modal-lg">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
                                                                        <h4 class="modal-title" id="myModalLabel">Order Details</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <h4>Please Select Quantity</h4>
                                                                        <input type="text" value="<?= $products["stock"] ?>" min="1" max="<?= $products["stock"] ?>" class="form-control stepper" id="qty_amount_<?= $i ?>">
                                                                        <h4>Price</h4>
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="margin-bottom-20">Select your price
                                                                                    <input type="text" id="select_price_cls_<?= $i ?>" class="form-control select_price_cls clss">

                                                                                </div>
                                                                                <div class="slider-wrapper black-slider">
                                                                                    <div id="slider5_<?= $i ?>" class="slider5"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="table-responsive" style="margin-top:30px;">
                                                                                    <table class="table table-hover" id="<?= $products["gid"] . "_" ?><?= $i ?>">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th>Bunch/Stem</th>
                                                                                            <th>Grower Price</th>
                                                                                            <th>Shipping Cost</th>
                                                                                            <th>Tax</th>
                                                                                            <th>Handling</th>
                                                                                            <th>Final Price</th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td style="background-color: white"><input id="bs_st" name="bs_st" class="form-control xs" style="border-width: 0px;" disabled type="text" value="Stem"></td>
                                                                                            <td><input style="background-color: white" id="grower_price_<?= $i ?>" name="grower_price_<?= $i ?>" class="form-control " style="border: none" disabled type="text" name="price" value="<?= sprintf("%.2f", $price["price"]) ?>"></td>
                                                                                            <td style="background-color: white"><input style="background-color: white" id="ship_cost_<?= $i ?>" name="ship_cost_<?= $i ?>" class="form-control" style="border-width: 0px;" disabled type="text" value="<?= sprintf("%.2f", $product_shipping) ?>"></td>
                                                                                            <td style="background-color: white"><input style="background-color: white" id="tax_<?= $i ?>" name="tax_<?= $i ?>" class="form-control" style="border-width: 0px;" disabled type="text" value="<?= sprintf("%.2f", $total_taxs) ?>"></td>
                                                                                            <td><input style="background-color: white" id="handling_<?= $i ?>" name="handling_<?= $i ?>" class="form-control" style="border-width: 0px;" disabled type="text" value="<?= sprintf("%.2f", $total_handling_feess) ?>"></td>
                                                                                            <td>
                                                                                                <input style="background-color: white" id="final_price_<?= $i ?>" name="final_price_<?= $i ?>" class="form-control" style="border-width: 0px;" disabled type="text" value="<?= sprintf("%.2f", $total_price) ?> ">
                                                                                                <input type="hidden" id="weight_<?= $i ?>" name="weight_<?= $i ?>" value="<?= $products['width'] ?>">
                                                                                                <input type="hidden" id="length_<?= $i ?>" name="length_<?= $i ?>" value="<?= $products['length'] ?>">
                                                                                                <input type="hidden" id="height_<?= $i ?>" name="height_<?= $i ?>" value="<?= $products['height'] ?>">
                                                                                                <input type="hidden" id="tax_porcent_<?= $i ?>" name="tax_porcent_<?= $i ?>" value="<?= $tax ?>">
                                                                                                <input type="hidden" id="ha_<?= $i ?>" name="ha_<?= $i ?>" value="<?php echo $handling_fees ?>" ">
                                                                                                <input type="hidden" id="box_type_<?= $i ?>" name="box_type_<?= $i ?>" value="<?= $products["type"] ?>">
                                                                                                <input type="hidden" id="productid_<?= $i ?>" name="productid_<?= $i ?>" value="<?= $products["prodcutid"] ?>">
                                                                                                <input type="hidden" id="order_<?= $i ?>" name="order_<?= $i ?>" value="<?= $_SESSION["filter_order"] ?>">
                                                                                                <input type="hidden" id="grower_id_<?= $i ?>" name="grower_id_<?= $i ?>" value="<?= $products['grower_id'] ?>">
                                                                                                <input type="hidden" id="size_id_<?= $i ?>" name="size_id_<?= $i ?>" value="<?= $products['sizeid'] ?>">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>


                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <div class="col-md-10"></div>
                                                                                    <div class="col-md-2" align="rigth">

                                                                                        <!--  data-dismiss="modal"-->
                                                                                        <button type="button" class="btn btn-primary " onclick="return restore_price(<?= $i ?>,<?= $price["price"] ?>,<?= $product_shipping ?>,<?= sprintf("%.2f", $total_taxs) ?>,<?= sprintf("%.2f", $total_handling_feess) ?>,<?= sprintf("%.2f", $total_price) ?>  ) "
                                                                                                style="background:#8a2b83!important;">Restore Price
                                                                                        </button>

                                                                                    </div>


                                                                                </div>
                                                                                <hr>
                                                                                <form class="validate" action="" method="post" enctype="multipart/form-data">
                                                                                    <fieldset>
                                                                                        <!-- required [php action request] -->
                                                                                        <div class="row">
                                                                                            <div class="form-group">
                                                                                                <div class="col-md-12 col-sm-12">
                                                                                                    <h4>Let your growers know if you have any special requirements</h4>
                                                                                                    <label>Comment</label>
                                                                                                    <textarea name="contact_<?= $i ?>" id="contact_<?= $i ?>" rows="4" class="form-control required"></textarea>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </fieldset>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


                                                                        <button type="button" data-toggle="modal" data-target="#modal-send-inventory" class="btn btn-primary " onclick="return send_request(<?= $i ?>)"
                                                                                style="background:#8a2b83!important;" data-dismiss="modal">Send Request
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                               
                                                </tr>
                                                  <input type="hidden" id="totalRes_<?php echo $i; ?>" name="totalRes_<?php echo $i; ?>" value=""/>
                                                <input type="hidden" id="grower_all_send_number<?php echo $i; ?>" name="grower_all_send_number<?php echo $i; ?>" value=""/>
                                                <?php
                                                $i++;
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <nav id="pagination_main">
                                        <?php
                                        $page = (int)(!isset($_GET["page"]) ? 1 : $_GET["page"]);
                                        $pag = pagination($num_record, 50, $page, $_POST['filter_order']);
                                        echo $pag; //echo "<script>console.log('.$pag.')</script>";
                                        ?><!--$_POST['filter_order']-->
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade request_modal_1" id="modal-send-inventory" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- header modal -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <span class="modal-title"
                              id="myLargeModalLabel">We are now sending your request to different growers</span>
                        <div class="pull-right" style="width:150px;margin-right:100px;">
                            <div class="progress progress-striped active">
                                <!--Here all number asign as 54 is variable which need to be dynamic-->
                                <div class="progress-bar progress-bar-success" style="width:0%"><span class="count">85</span>&nbsp;Growers
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- body modal -->
                    <div class="modal-body">
                        <div>
                            <div class="alert alert-warning notify_1">
                                Your order has been sent to <span id="msg_count">54</span> growers
                            </div>
                            <div class="text-right" style="border-top: rgba(0,0,0,0.02) 1px solid;">
                                <ul class="pagination">
                                    <a class="btn btn-purple pull-right btn-sm nomargin-top nomargin-bottom"
                                       href="<?php echo SITE_URL . "buyer/my-offers.php" ?>" style="background-color:#8a2b83;">Go to grower's offers</a>
                                    <a class="btn btn-success pull-right btn-sm nomargin-top nomargin-bottom"
                                       data-dismiss="modal" href="javascript:void(0);">Continue buying</a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>


<script type='text/javascript'>
    //$(document).ready(function () {});
    $(window).load(function () {
        $('#loading').css("display", "none");
    });
</script>


<script type='text/javascript'>
    /*$(document).ready(function () {
     $('#loading').css("display", "none");
     });

     $(".load").click(function (event) {
     $('#loading').css("display", "block");
     });*/
</script>

<script type="text/javascript">
    jQuery(window).ready(function () {
        loadScript('/assets/js/jquery/jquery-ui.min.js', 
        function () {
            /** jQuery UI **/
            loadScript('/assets/js/jquery/jquery.ui.touch-punch.min.js', 
            function () {
                /** Mobile Touch Slider **/
                loadScript(plugin_path + 'form.slidebar/jquery-ui-slider-pips.min.js', 
                function () { /** Slider Script **/
                    /** Slider 5******************** **/
                    var id = "";
                    //jQuery(".ajax_loader_s").css("display", "inline-block");
                    //$body.addClass("loading");
                    jQuery(".slider5").slider({
                        //value: 0.01, 
                        animate: true,
                        min: 0.05,
                        max: 2,
                        step: 0.01,
                        range: "min",
                        slide: function (event, ui) {
                           // jQuery(".select_price_cls").val(ui.value); po
                              jQuery(".select_price_cls").val(ui.value);                         
                            var idx = $(this).attr("id");
                            var arr = idx.split('_');
                            
                           // $("#grower_price_" + arr[1]).val(+ui.value); po
                              $("#grower_price_" + arr[1]).val(+ui.value); 
                              
                          //  $("#ship_cost_" + arr[1]).val(+ui.value);
                          
                            var taxes = $("#tax_porcent_" + arr[1]).val();
                            var red = (ui.value * taxes) / 100;
                            var handling = $("#ha_" + arr[1]).val();
                            var reda = (ui.value * handling) / 100;
                            
                           // $("#ship_cost_" + arr[1]).val(0);
                            // $("#final_price_" + arr[1]).val("$" + (red+reda+ui.value).toFixed(2));
                            
                            $("#final_price_" + arr[1]).val(ui.value);
                            
                            var grower_pricex = (ui.value / (1 + (taxes / 100) + (handling / 100))).toFixed(2);
                            var handling = (grower_pricex * handling / 100).toFixed(2);
                            //var tax = (grower_pricex * taxes / 100).toFixed(2); 
                            var tax = (ui.value); 
                            
                            $("#grower_price_" + arr[1]).val(+grower_pricex);
                            $("#handling_" + arr[1]).val(+handling);
                            $("#tax_" + arr[1]).val(+tax);
                            
                            //select_price_cls_<?= $i ?>
                            //$("#select_price_cls_" + arr[1]).val(ui.value);
                            // $("#select_price_cls_0").val('10');
                        }
                    });
                    /*jQuery(".select_price_cls").val(jQuery('.slider5').slider("value"));
                     jQuery(".select_price_cls").blur(function () {
                     jQuery(".select_price_cls").slider("value", jQuery(this).val());
                     });*/


                });
            });
        });
    });
    $(".clss").keypress(function (e) {
        if (e.which == 13) {
            // Acciones a realizar, por ej: enviar formulario.
            //$('#frm').submit();
            var id = $(".clss").attr("id");
            var i = id.split('_');
            var final_price = $("#select_price_cls_" + i[3]).val();
            $("#slider5_" + i[3]).slider("value", final_price);
        }
    });


    function set_price($i, $price) {
        console.log($i);
        $("#select_price_cls_" + $i).val($price);
        $("#slider5_" + $i).slider("value", $price);
    }

    function restore_price(i, price, shipping, tax, handling, final_price) {
        $("#grower_price_" + i).val(price);
        $("#ship_cost_" + i).val(shipping);
        $("#tax_" + i).val(tax);
        $("#handling_" + i).val(handling);
        $("#final_price_" + i).val(final_price);
        $("#select_price_cls_" + i).val(final_price);
        $("#slider5_" + i).slider("value", final_price);
    }

    function send_request(i) {
        //qty= boxes quantity
        //boxtype=cod  type  of  box
        var qty = $('#qty_amount_' + i).val();
        var boxtype = $('#box_type_' + i).val();
        var price = $("#grower_price_" + i).val();
       var ship_cost = $("#ship_cost_" + i).val();
        var tax = $("#tax_" + i).val();
        var handling = $("#handling_" + i).val();
        var final_price = $("#final_price_" + i).val();
        var product_id = $("#productid_" + i).val();
        var order_id = $("#order_" + i).val();
        var grower_id = $("#grower_id_" + i).val();
        var size_id = $("#size_id_" + i).val();
        var comments = $("#contact_" + i).val();
		var totalRes =1;
		 console.log("order_id" + order_id);
		 console.log("Griwer" + grower_id);

        console.log("Este  es el  tax:" + tax);

        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>buyer/re_prod_invent.php',
            data: 'sizeId=' + size_id + '&box_quantity=' + qty + '&productId=' + product_id + '&grower_id=' + grower_id + '&buyerPrice=' + price +
            '&comment=' + comments + '&order_val=' + order_id + '&type_box=' + boxtype +
            '&tax=' + tax + '&ship_cost=' + ship_cost + '&hand_l=' + handling,
            success: function (data_s) {

                console.log(data_s);
                 jQuery("#msg_count").html("0");
                 $(".count").html(totalRes);
                 setTimeout(function () {
                 jQuery("#msg_count").html(totalRes);
                 jQuery('.notify_1').show();
                 }, 7000);
                 jQuery(".progress-bar").animate({width: "100%"}, 6000);
                 $('.count').each(function () {
                 $(this).prop('Counter', 0).animate(
                 {Counter: $(this).text()},
                 {
                 duration: 7000, easing: 'swing', step: function (now) {
                 $(this).text(Math.ceil(now));
                 }
                 });
                 });
                 

            }
        });
        //$('#qty_modal_1').text(qty + ' ' + boxtype + ' @ $' + price);

        function buynow(gid) {
            var bv = $('#bv-' + gid).val();
            // console.log(bv);
            var fields_name = {};
            fields_name['gid6'] = gid;
            fields_name['bv'] = bv;
            if (bv != 2) {
                var subcategoryname = $('#subcategoryname-' + gid).val();
                var productname = $('#productname-' + gid).val();
                var featurename = $('#featurename-' + gid).val();
                var sizename = $('#sizename-' + gid).val();
                var boxweight = $('#boxweight-' + gid).val();
                var boxvolumn = $('#boxvolumn-' + gid).val();
                var bunchsize = $('#bunchsize-' + gid).val();
                var bunchqty = $('#bunchqty-' + gid).val();
                var shipping = $('#shipping-' + gid).val();
                var handling = $('#handling-' + gid).val();
                var tax = $('#tax-' + gid).val();
                var totalprice = $('#totalprice-' + gid).val();
                var ooprice = $('#ooprice-' + gid).val();
                var boxtypename = $('#boxtypename-' + gid).val();
                var stock = $('#stock-' + gid).val();
                var boxtype = $('#boxtype-' + gid).val();
                var product = $('#product-' + gid).val();
                var sizeid = $('#sizeid-' + gid).val();
                var feature = $('#feature-' + gid).val();
                var grower = $('#grower-' + gid).val();
                fields_name['subcategoryname'] = subcategoryname;
                fields_name['productname'] = productname;
                fields_name['featurename'] = featurename;
                fields_name['sizename'] = sizename;
                fields_name['boxweight'] = boxweight;
                fields_name['boxvolumn'] = boxvolumn;
                fields_name['bunchsize'] = bunchsize;
                fields_name['bunchqty'] = bunchqty;
                fields_name['shipping'] = shipping;
                fields_name['handling'] = handling;
                fields_name['tax'] = tax;
                fields_name['totalprice'] = totalprice;
                fields_name['ooprice'] = ooprice;
                fields_name['boxtypename'] = boxtypename;
                fields_name['stock'] = stock;
                fields_name['boxtype'] = boxtype;
                fields_name['product'] = product;
                fields_name['sizeid'] = sizeid;
                fields_name['feature'] = feature;
                fields_name['grower'] = grower;


            } else {
                var gprice = $('#gprice-' + gid).val();
                var boxtype = $('#boxtype-' + gid).val();
                var stock = $('#stock-' + gid).val();
                var product = $('#product-' + gid).val();
                var sizeid = $('#sizeid-' + gid).val();
                var feature = $('#feature-' + gid).val();
                var grower = $('#grower-' + gid).val();
                var multi = $('#multi-' + gid).val();

                fields_name['gprice'] = gprice;
                fields_name['boxtype'] = boxtype;
                fields_name['stock'] = stock;
                fields_name['product'] = product;
                fields_name['sizeid'] = sizeid;
                fields_name['feature'] = feature;
                fields_name['grower'] = grower;
                fields_name['multi'] = multi;


            }
            //console.log(fields_name);
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/inventory_buy_ajax.php',
                data: fields_name,

                success: function (data) {
                    if (data == 'true') {
                        alert('Your reuqest has been sent...');
                    } else {
                        alert('There is some error. Please try again');
                    }
                }
            });


        }


    }


    function evaluate_price() {
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        var productsearch = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '<?php echo SITE_URL; ?>/includes/autosuggest.php?limit=10&search=%QUERY',
                wildcard: '%QUERY'
            }
        });
        $('.autosuggest #typeSearch').typeahead(null, {
            name: 'productsearch',
            source: productsearch
        });
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            //console.log('Selection: ' + suggestion);
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/inventory_search_product.php',
                data: 'name=' + suggestion,
                success: function (data) {
                    $("#pagination_nav").hide();
                    $('.imgDiv').hide();
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });

        /*Josse P*/

        $body = $("body");
        function call_Ajax_Fliter() {
            jQuery("#btn_filter").click(function () {
                var check_value = 0;
                var data_ajax = "";
                if (jQuery("#filter_category").val() == "" &&
                    jQuery("#filter_variety").val() == "" &&
                    jQuery("#filter_color").val() == "" &&
                    jQuery("#filter_grower").val() == "" &&
                    jQuery("#filter_size").val() == "" &&
                    jQuery("#datepicker").val() == "" &&
                    jQuery("#filter_perbunch").val() == "" &&
                    jQuery("#filter_sfeat").val() == "") {
                    check_value = 1;
                }
                if (check_value == 1) {
                    alert("Please select any one option 1.");
                }


                else {
                    var filter_category = jQuery("#filter_category").val();
                    var filter_variety = jQuery("#filter_variety").val();
                    var filter_color = jQuery("#filter_color").val();
                    var filter_grower = jQuery("#filter_grower").val();
                    var filter_size = jQuery("#filter_size").val();
                    var filter_date = jQuery("#datepicker").val();
                    var filter_perbunch = jQuery("#filter_perbunch").val();
                    var filter_sfeat = jQuery("#filter_sfeat").val();
                    var filter_category_text = jQuery("#filter_category option:selected").text();
                    var filter_variety_text = jQuery("#filter_variety option:selected").text();
                    var filter_color_text = jQuery("#filter_color option:selected").text();
                    var filter_grower_text = jQuery("#filter_grower option:selected").text();
                    var filter_size_text = jQuery("#filter_size option:selected").text();
                    var filter_perbunch_text = jQuery("#filter_perbunch option:selected").text();
                    var filter_sfeat_text = jQuery("#filter_sfeat option:selected").text();
                    jQuery(".ajax_loader_s").css("display", "inline-block");
                    $.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>buyer/getFilterInvent.php',
                        data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_color=' + filter_color + '&filter_grower=' + filter_grower +
                        '&filter_size=' + filter_size + '&filter_date=' + filter_date + '&filter_sfeat=' + filter_sfeat + '&filter_perbunch=' + filter_perbunch,
                        success: function (data) {
                            jQuery("#tab_for_filter").html("");
                            $("#pagination_main").hide();
                            jQuery('.search_modal_open').modal('hide');
                            jQuery("#list_inventory").html(data);   //nombre del  id  del <tbody>
                            var pass_delete_f = "";
                            if (filter_category == "") {
                                var filter_category_temp = "0";
                            }
                            else {
                                var filter_category_temp = filter_category;
                            }
                            if (filter_variety == "") {
                                var filter_variety_temp = "0";
                            }
                            else {
                                var filter_variety_temp = filter_variety;
                            }
                            if (filter_color == "") {
                                var filter_color_temp = "0";
                            }
                            else {
                                var filter_color_temp = filter_color;
                            }
                            if (filter_grower == "") {
                                var filter_grower_temp = "0";
                            }
                            else {
                                var filter_grower_temp = filter_grower;
                            }
                            if (filter_size == "") {
                                var filter_size_temp = "0";
                            }
                            else {
                                var filter_size_temp = filter_size;

                            }
                            if (filter_perbunch == "") {
                                var filter_perbunch_temp = "0";
                            } else {
                                var filter_perbunch_temp = filter_perbunch;
                            }
                            if (filter_sfeat == "") {
                                var filter_sfeat_temp = "0";
                            }
                            else {
                                var filter_sfeat_temp = filter_sfeat;

                            }
                            pass_delete_f += "'" + filter_category_text + "'" + ',' + filter_category_temp + ',';
                            pass_delete_f += "'" + filter_variety_text + "'" + ',' + filter_variety_temp + ',';
                            pass_delete_f += "'" + filter_color_text + "'" + ',' + filter_color_temp + ',';
                            pass_delete_f += "'" + filter_grower_text + "'" + ',' + filter_grower_temp + ',';
                            pass_delete_f += "'" + filter_size_text + "'" + ',' + filter_size_temp + ',';
                            pass_delete_f += "'" + filter_sfeat_text + "'" + ',' + filter_sfeat_temp + ',';
                            pass_delete_f += "'" + filter_perbunch_text + "'" + ',' + filter_perbunch_temp;
                            //alert(pass_delete_f);
                            if (filter_category != "") {
                                var pass_click_cate = "'" + filter_category_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_category_filter" cate_label="' + filter_category_text + '" name="hdn_selected_category_filter" value="' + filter_category + '" />' + filter_category_text);


                            }
                            if (filter_variety != "") {
                                var pass_click_cate = "'" + filter_variety_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_variety_filter" cate_label="' + filter_category_text + '" name="hdn_selected_variety_filter" value="' + filter_variety + '" />' + filter_variety_text);


                            }
                            if (filter_color != "") {
                                var pass_click_cate = "'" + filter_color_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_color_filter" cate_label="' + filter_category_text + '" name="hdn_selected_color_filter" value="' + filter_color + '" />' + filter_color_text);


                            }
                            if (filter_grower != "") {
                                var pass_click_cate = "'" + filter_grower_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i>' +
                                    '<input type="hidden" id="hdn_selected_grower_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_grower_filter" ' +
                                    'value="' + filter_grower + '" />' + filter_grower_text);


                            }
                            if (filter_perbunch != "") {
                                var pass_click_cate = "'" + filter_perbunch_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i><input type="hidden" id="hdn_selected_perbunch_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_perbunch_filter" ' +
                                    'value="' + filter_perbunch + '" />' + filter_perbunch_text);
                            }
                            if (filter_size != "") {
                                var pass_click_cate = "'" + filter_size_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i>' +
                                    '<input type="hidden" id="hdn_selected_size_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_size_filter" ' +
                                    'value="' + filter_size + '" />' + filter_size_text);
                            }

                            if (filter_sfeat != "") {
                                var pass_click_cate = "'" + filter_sfeat_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i><input type="hidden" id="hdn_selected_sfeat_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_sfeat_filter" ' +
                                    'value="' + filter_sfeat + '" />' + filter_sfeat_text);

                            }
                            jQuery("#tab_for_filter").show();
                            jQuery.ajax({
                                type: 'post',
                                url: '<?php echo SITE_URL; ?>buyer/getFilterInventPa.php',
                                data: 'filter_category=' + filter_category +
                                '&filter_variety=' + filter_variety +
                                '&filter_color=' + filter_color +
                                '&filter_grower=' + filter_grower +
                                '&filter_size=' + filter_size +
                                '&filter_sfeat=' + filter_sfeat +
                                '&filter_perbunch=' + filter_perbunch,
                                success: function (data) {
                                    jQuery("#pagination_nav").html(data);
                                    jQuery(".ajax_loader_s").hide();
                                }
                            });
                        }
                    });
                }
            });
        }

        call_Ajax_Fliter();
        /* 21-10-2016 */
    });
    /*aumentado por mi*/
    function click_Ajax(page_number) {
        var check_value = 0;
        var data_ajax = "";
        if (jQuery("#filter_category").val() == "" && jQuery("#filter_variety").val() == "" && jQuery("#filter_color").val() == "" && jQuery("#filter_grower").val() == ""
            && jQuery("#filter_size").val() == "" && jQuery("#filter_sfeat").val() == "" && jQuery("#filter_perbunch").val() == "") {
            check_value = 1;
        }
        if (check_value == 1) {
            alert("Please select any one option 2.");
        }

        else {
            var filter_category = jQuery("#hdn_selected_category_filter").val();
            if (typeof filter_category === "undefined") {
                filter_category = "";
            }
            var filter_variety = jQuery("#hdn_selected_variety_filter").val();
            if (typeof filter_variety === "undefined") {
                filter_variety = "";


            }
            var filter_color = jQuery("#hdn_selected_color_filter").val();
            if (typeof filter_color === "undefined") {
                filter_color = "";
            }
            var filter_grower = jQuery("#hdn_selected_grower_filter").val();
            if (typeof filter_grower === "undefined") {
                filter_grower = "";
            }
            var filter_size = jQuery("#hdn_selected_size_filter").val();
            if (typeof filter_size === "undefined") {
                filter_size = "";
            }

            var filter_perbunch = jQuery("#hdn_selected_perbunch_filter").val();
            if (typeof filter_perbunch === "undefined") {
                filter_perbunch = "";
            }

            var filter_sfeat = jQuery("#hdn_selected_sfeat_filter").val();
            if (typeof filter_sfeat === "undefined") {
                filter_sfeat = "";
            }

            $body.addClass("loading");

            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/getFilterInvent.php',
                data: 'filter_category=' + filter_category +
                '&filter_variety=' + filter_variety +
                '&filter_color=' + filter_color +
                '&filter_grower=' + filter_grower +
                '&filter_size=' + filter_size +
                '&filter_perbunch=' + filter_perbunch +
                '&filter_sfeat=' + filter_sfeat +
                '&page_number=' + page_number,

                success: function (data) {
                    jQuery("#tab_for_filter").html("");
                    $("#pagination_main").hide();
                    /************************************************************aqui deseleccione  con todo  lo  de arriba*/
                    jQuery('.search_modal_open').modal('hide');
                    jQuery("#list_inventory").html(data);
                    jQuery("#tab_for_filter").show();
                    jQuery.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>buyer/getFilterInventPa.php',
                        data: 'filter_category=' + filter_category +
                        '&filter_variety=' + filter_variety +
                        '&filter_color=' + filter_color +
                        '&filter_grower=' + filter_grower +
                        '&filter_size=' + filter_size +
                        '&filter_perbunch=' + filter_perbunch +
                        '&filter_sfeat=' + filter_sfeat +
                        '&page_number=' + page_number,
                        success: function (data) {
                            jQuery("#pagination_nav").html(data);
                            $body.removeClass("loading");

                        }

                    });

                }

            });

        }

    }


    function deleteFilter(pass_click_cate, filter_category_text, filter_category, filter_variety_text, filter_variety, filter_color_text, filter_color,
                          filter_grower_text, filter_grower, filter_size_text, filter_size, filter_perbunch_text, filter_perbunch, filter_sfeat_text, filter_sfeat) {
        var a_tag_html = $("#tab_for_filter").find("a").length
        if (a_tag_html == "1") {
            location.reload();
        }
        else {
            if (pass_click_cate == filter_category_text) {
                filter_category = "";
            }
            if (pass_click_cate == filter_variety_text) {
                filter_variety = "";
            }
            if (pass_click_cate == filter_color_text) {
                filter_color = "";
            }
            if (pass_click_cate == filter_grower_text) {
                filter_grower = "";
            }
            if (pass_click_cate == filter_size_text) {
                filter_size = "";
            }
            if (pass_click_cate == filter_perbunch_text) {
                filter_perbunch = "";
            }
            if (pass_click_cate == filter_sfeat_text) {
                filter_sfeat = "";
            }
            var check_value = 0;
            var data_ajax = "";
            if (filter_category == "" &&
                filter_variety == "" &&
                filter_color == "" &&
                filter_grower == "" &&
                filter_size == "" &&
                filter_perbunch == "" &&
                filter_sfeat == "") {
                check_value = 1;
            }
            if (check_value == 1) {
                alert("Please select any one option 3.");
            }
            else {
                if (filter_category == "0") {
                    filter_category = "";
                }
                if (filter_variety == "0") {
                    filter_variety = "";
                }
                if (filter_color == "0") {
                    filter_color = "";
                }
                if (filter_grower == "0") {
                    filter_grower = "";
                }
                if (filter_size == "0") {
                    filter_size = "";
                }
                if (filter_perbunch == "0") {
                    filter_perbunch = "";
                }
                if (filter_sfeat == "0") {
                    filter_sfeat = "";
                }
                $body.addClass("loading");
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>buyer/getFilterInvent.php',
                    data: 'filter_category=' + filter_category +
                    '&filter_variety=' + filter_variety +
                    '&filter_color=' + filter_color +
                    '&filter_grower=' + filter_grower +
                    '&filter_size=' + filter_size +
                    '&filter_perbunch=' + filter_perbunch +
                    '&filter_sfeat=' + filter_sfeat +
                    '&page_number=1',
                    success: function (data) {
                        $body.removeClass("loading");
                        jQuery("#tab_for_filter").html("");
                        $("#pagination_main").hide();
                        jQuery('.search_modal_open').modal('hide');
                        jQuery("#list_inventory").html(data);
                        var pass_delete_f = "";
                        if (filter_category == "") {
                            var filter_category_temp = "0";
                        }
                        else {
                            var filter_category_temp = filter_category;

                        }
                        if (filter_variety == "") {
                            var filter_variety_temp = "0";

                        }
                        else {
                            var filter_variety_temp = filter_variety;
                        }
                        if (filter_color == "") {
                            var filter_color_temp = "0";
                        }
                        else {
                            var filter_color_temp = filter_color;
                        }
                        if (filter_grower == "") {
                            var filter_grower_temp = "0";
                        }
                        else {
                            var filter_grower_temp = filter_grower;
                        }
                        if (filter_size == "") {
                            var filter_size_temp = "0";
                        }
                        else {
                            var filter_size_temp = filter_size;
                        }
                        if (filter_perbunch == "") {
                            var filter_perbunch_temp = "0";
                        } else {
                            var filter_perbunch_temp = filter_perbunch;


                        }
                        if (filter_sfeat == "") {

                            var filter_sfeat_temp = "0";


                        } else {

                            var filter_sfeat_temp = filter_sfeat;
                        }
                        pass_delete_f += "'" + filter_category_text + "'" + ',' + filter_category_temp + ',';
                        pass_delete_f += "'" + filter_variety_text + "'" + ',' + filter_variety_temp + ',';
                        pass_delete_f += "'" + filter_color_text + "'" + ',' + filter_color_temp + ',';
                        pass_delete_f += "'" + filter_grower_text + "'" + ',' + filter_grower_temp + ',';
                        pass_delete_f += "'" + filter_size_text + "'" + ',' + filter_size_temp + ',';
                        pass_delete_f += "'" + filter_sfeat_text + "'" + ',' + filter_sfeat_temp + ',';
                        pass_delete_f += "'" + filter_perbunch_text + "'" + ',' + filter_perbunch_temp;
                        if (filter_category != "") {
                            var pass_click_cate = "'" + filter_category_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_category_filter" cate_label="' + filter_category_text + '" name="hdn_selected_category_filter" value="' + filter_category + '" />' + filter_category_text);
                        }
                        if (filter_variety != "") {
                            var pass_click_cate = "'" + filter_variety_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_variety_filter" cate_label="' + filter_category_text + '" name="hdn_selected_variety_filter" value="' + filter_variety + '" />' + filter_variety_text);
                        }
                        if (filter_color != "") {
                            var pass_click_cate = "'" + filter_color_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_color_filter" cate_label="' + filter_category_text + '" name="hdn_selected_color_filter" value="' + filter_color + '" />' + filter_color_text);
                        }
                        if (filter_grower != "") {
                            var pass_click_cate = "'" + filter_grower_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i>' +
                                '<input type="hidden" id="hdn_selected_grower_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_grower_filter" ' +
                                'value="' + filter_grower + '" />' + filter_grower_text);
                        }
                        if (filter_perbunch != "") {
                            var pass_click_cate = "'" + filter_perbunch_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i><input type="hidden" id="hdn_selected_perbunch_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_perbunch_filter" ' +
                                'value="' + filter_perbunch + '" />' + filter_perbunch_text);
                        }
                        if (filter_size != "") {
                            var pass_click_cate = "'" + filter_size_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i>' +
                                '<input type="hidden" id="hdn_selected_size_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_size_filter" ' +
                                'value="' + filter_size + '" />' + filter_size_text);
                        }
                        if (filter_sfeat != "") {
                            var pass_click_cate = "'" + filter_sfeat_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i><input type="hidden" id="hdn_selected_sfeat_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_sfeat_filter" ' +
                                'value="' + filter_sfeat + '" />' + filter_sfeat_text);
                        }

                        jQuery("#tab_for_filter").show();
                        jQuery.ajax({
                            type: 'post',
                            url: '<?php echo SITE_URL; ?>buyer/getFilterInventPa.php',
                            data: 'filter_category=' + filter_category +
                            '&filter_variety=' + filter_variety +
                            '&filter_color=' + filter_color +
                            '&filter_grower=' + filter_grower +
                            '&filter_size=' + filter_size +
                            '&filter_perbunch=' + filter_perbunch +
                            '&filter_sfeat=' + filter_sfeat +
                            '&page_number=1',
                            success: function (data) {
                                jQuery("#pagination_nav").html(data);
                                jQuery(".ajax_loader_s").hide();

                            }

                        });


                    }

                });

            }

        }

    }
    /*fin aumentado por mi deleter  filter*/
    $(function () {
        $('form.searchProduct').on('submit', function (e) {
            e.preventDefault();
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/search_request_page.php',
                data: $('form.searchProduct').serialize(),
                success: function (data) {
                    $('.imgDiv').hide();
                    $("#show_hide").toggle("slow");
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });
    });
    /*no tocar*/


</script>
<script type="text/javascript">
    $(".tags_selected").hide();
    var count = 0;
    $("#addRows").click(function () {
        count++;
        $("#place_row").append("<label for='donation'><div class='fancy_from'><select class='form-control select2' style='width: 415px!important;'><option value=''>Select Shipping Option</option><option value=''>Direct Flight from Ecuador to Miami with American Airlines coordinated by GyG Cargo</option><option value=''>Direct Flight from Ecuador to Miami with American Airlines coordinated by GyG Cargo</option><option value=''>Direct Flight from Ecuador to Miami with American Airlines coordinated by GyG Cargo</option><option value=''>Direct Flight from Ecuador to Miami with American Airlines coordinated by GyG Cargo</option><option value=''>Direct Flight from Ecuador to Miami with American Airlines coordinated by GyG Cargo</option></select></div></label>&nbsp;<label class='field'><div class='fancy-file-upload fancy-file-primary' style='overflow: inherit!important;width:415px;margin-left:30px;'><input type='text' name='ok' id='dt_" + count + "' value='' class='form-control datepicker required' data-format='yyyy-mm-dd' placeholder='Select Date'><span class='button'>Delete Date</span></div></label><br>");
        $("#dt_5").datepicker();
        $("#dt_6").datepicker();
        $("#dt_7").datepicker();
        $("#dt_8").datepicker();
        $("#dt_9").datepicker();
        $("#dt_10").datepicker();
        $("#dt_11").datepicker();
        $("#dt_12").datepicker();
        $("#dt_13").datepicker();
        $('select').select2();


    });
    $('.apply').click(function () {

        $(".tags_selected").show("slow");
        var meses = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        var valor = document.getElementById("datepicker").value;
        var d = new Date(valor);
        var dm = d.getDate() + 1 + " " + meses[d.getMonth()] + " " + d.getFullYear();


        if (valor == "") {
            var f = new Date();
            //document.write(f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
            var val = f.getDate() + " " + meses[f.getMonth()] + " " + f.getFullYear();
            document.getElementById("dates").innerHTML = val;
        }
        else {
            document.getElementById("dates").innerHTML = dm;
        }
    });
    /*no tocar*/


    function requestProductffff(productId, i) {
        var delDate = $('.cls_custom_date').val();
        var delDate = $('#cls_date_' + productId + '_' + i).find("input").val();
        var shippingMethod = $('#shipping_id_' + productId + '_' + i).val();
        var qucik_desc = $('#qty_desc_' + productId + '_' + i).val();
        var dateRange = "";
        var flag_s = "true";
        if (delDate == "") {
            alert("Please select delivery date.");
            flag_s = "false";
        }
        else if (shippingMethod == "") {
            alert("Please select shippingMethod.");
            flag_s = "false";
        }
        else if (qucik_desc == "") {
            alert("Please enter quick description.");
            flag_s = "false";
        }
        //flag_s = "false";
        if (flag_s == "true") {
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/redirectrequest.php',
                data: 'delDate=' + delDate + '&dateRange=' + dateRange + '&shippingMethod=' + shippingMethod + '&qucik_desc=' + qucik_desc,
                success: function (data) {
                    if (data == 'true') {
                        jQuery('.notify_1').hide();
                        jQuery(".price_modal_" + i).modal('toggle');
                        jQuery(".request_modal_1").modal('show');
                        var totalRes = $('#totalRes_' + i).val();
                        var sizename = $('#sizename_' + productId + '_' + i).val();
                        //alert(sizename);
                        var sizeId = $('#sizeId_' + productId + '_' + i).val();
                        var productName = $('#productName_' + productId + '_' + i).val();
                        var featureId = $('#featureId_' + productId + '_' + i).val();
                        var qty_boxtype = $('#box_quantity_' + productId + '_' + i).val() + "_" + $('#box_type_' + productId + '_' + i).val();
                        var box_quantity = qty_boxtype;
                        var cId = $('#productComment_' + productId + '_' + i).val();
                        var buyer = '<?php echo $_SESSION["buyer"]; ?>';
                        var date_range = $('.picker2').val();
                        var shippingMethod = $('#shipping_id_' + productId + '_' + i).val();
                        var buyerPrice = $('#buyer_price_' + productId + '_' + i).val();
                        var conType = $('#conType_' + productId + '_' + i).val();
                        var shippingFee = $('#shippingFee_' + productId + '_' + i).val();
                        var comment_pro = $('#comment_' + productId + '_' + i).val();
                        var growers = $("#grower_all_send_number" + i).val();
                        $.ajax({
                            type: 'post',
                            url: '<?php echo SITE_URL; ?>buyer/request_product_ajax.php',
                            data: 'shippingFee=' + shippingFee + '&conType=' + conType + '&sizeId=' + sizeId + '&featureId=' + featureId +
                            '&box_quantity=' + box_quantity + '&productId=' + productId + '&cId=' + cId + '&buyer=' + buyer +
                            '&delivery_date=' + delDate + '&date_range=' + dateRange + '&growers=' + growers + '&productName=' + productName +
                            '&shippingMethod=' + shippingMethod + '&buyerPrice=' + buyerPrice + '&comment_pro=' + comment_pro,
                            success: function (data_s) {
                                if (data_s == 'true') {
                                    //alert('Your reuqest has been sent...');
                                    //location.reload();

                                    jQuery("#msg_count").html("0");
                                    $(".count").html(totalRes);
                                    //alert(totalRes);
                                    setTimeout(function () {
                                        //totalRes
                                        jQuery("#msg_count").html(totalRes);
                                        jQuery('.notify_1').show();
                                    }, 7000);
                                    jQuery(".progress-bar").animate({width: "100%"}, 6000);
                                    $('.count').each(function () {
                                        $(this).prop('Counter', 0).animate(
                                            {Counter: $(this).text()},
                                            {
                                                duration: 7000, easing: 'swing', step: function (now) {
                                                $(this).text(Math.ceil(now));
                                            }
                                            });
                                    });
                                } else {
                                    alert('There is some error. Please try again');
                                }

                            }
                        });
                    }
                    else {
                        alert("Something wrong please try again!!!");
                    }
                }
            });
        }

    }


</script>
<!--<script type="text/javascript" src="assets/js/custom.js"></script>-->
<?php include("../includes/footer_new.php"); ?>
