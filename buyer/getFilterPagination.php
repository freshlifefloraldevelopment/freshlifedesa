<?php 
include("../config/config_new.php");
$wh="";
if($_REQUEST['filter_category'] != "")
{
    $wh.=" AND s.cat_id=".$_REQUEST['filter_category'];
}
if($_REQUEST['filter_color'] != "")
{
    $wh.=" AND p.color_id=".$_REQUEST['filter_color'];
}
if($_REQUEST['filter_grower'] != "")
{
    $wh.=" AND gpb.growerid=".$_REQUEST['filter_grower'];
}
if($_REQUEST['filter_pack'] != "")
{
    //$wh.=" AND s.cat_id=".$_REQUEST['filter_pack'];
}
if($_REQUEST['filter_size'] != "")
{
    $wh.=" AND b.type =".$_REQUEST['filter_size'];
}
if($_REQUEST['filter_variety'] != "")
{
    $wh.=" AND gpb.prodcutid=".$_REQUEST['filter_variety'];
}
$from=0;
$display=50;
$query2 = "select gpb.prodcutid,gpb.id as gid,gpb.qty,gpb.sizeid,gpb.feature,gpb.growerid,p.id,p.subcategoryid,p.name,p.color_id,p.image_path,s.name as subs,s.cat_id,g.file_path5,g.growers_name,sh.name as sizename,ff.name as featurename,b.width,b.length,b.height,b.id as boxid,b.name as boxname,b.type as boxtypeid,bs.id as bunchsizeid,bs.name as bname,c.name as colorname from grower_product_box_packing gpb
                    LEFT join product p on gpb.prodcutid = p.id
                    LEFT join subcategory s on p.subcategoryid=s.id  
                    INNER join colors c on p.color_id=c.id 
                    LEFT join features ff on gpb.feature=ff.id
                    LEFT join sizes sh on gpb.sizeid=sh.id 
                    LEFT join boxes b on gpb.box_id=b.id
                    INNER join growers g on gpb.growerid=g.id
                    LEFT join bunch_sizes bs on gpb.bunch_size_id=bs.id
                    where g.active='active' $wh and p.name is not null and sh.name is not null ";
$query2.=" ";
$query2.= "group by gpb.prodcutid,gpb.sizeid,gpb.feature  order by p.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER)";
//LIMIT $from,$display
$result2 = mysqli_query($con, $query2);
$i = 1;
$tp = mysqli_num_rows($result2);

//Pagination code
$per_page = 50;
$total=$tp;
$adjacents = "2"; 
$page = (int) (!isset($_REQUEST["page_number"]) ? 1 : $_REQUEST["page_number"]);
$page = ($page == 0 ? 1 : $page);  

$start = ($page - 1) * $per_page;                               
$counter=1;
$prev = $page - 1;                          
$next = $page + 1;
$lastpage = ceil($total/$per_page);

$lpm1 = $lastpage - 1;
$pagination = "";

//echo $page."<".$counter." - 1";

if($lastpage > 1)
{   
    $pagination .= "<ul class='pagination'>";
    if ($page > $counter){ 
        $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax(1)'>First</a></li>";
        $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax($prev)'>Pervious</a></li>";
    }else{
        $pagination.= "<li class=''><a href='javascript:void(0);' >First</a></li>";
        $pagination.= "<li class=''><a href='javascript:void(0);' >Pervious</a></li>";
    }
    if ($lastpage < 7 + ($adjacents * 2))
    {   
        for ($counter = 1; $counter <= $lastpage; $counter++)
        {
            if ($counter == $page)
                $pagination.= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
            else
               $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax($counter)'>$counter</a></li>";                    
        }
    }
    elseif($lastpage > 5 + ($adjacents * 2))
    {
        if($page < 1 + ($adjacents * 2))        
        {
            for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<li class='active'><a href='javascript:void(0);'>$counter</a></li>";
                else
                    $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax($counter)'>$counter</a></li>";                    
            }
            $pagination.= "<li class='dot'>...</li>";
            $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax($lpm1)'>$lpm1</a></li>";
            $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax($lastpage)'>$lastpage</a></li>";      
        }
        elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
        {
            $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax(1)'>1</a></li>";
            $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax(2)'>2</a></li>";
            $pagination.= "<li class='dot'>...</li>";
            for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<li class='active'><a href='javascript:void(0);' >$counter</a></li>";
                else
                    $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax($counter)'>$counter</a></li>";                    
            }
            $pagination.= "<li class='dot'>..</li>";
            $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax($lpm1)'>$lpm1</a></li>";
            $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax($lastpage)'>$lastpage</a></li>";      
        }
        else
        {
            $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax(1)'>1</a></li>";
            $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax(2)'>2</a></li>";
            $pagination.= "<li class='dot'>..</li>";
            for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<li class='active'><a href='javascript:void(0);'>$counter</a></li>";
                else
                    $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax($counter)'>$counter</a></li>";                    
            }
        }
    }
    if ($page < $counter - 1){ 
        $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax($next)'>Next</a></li>";
        $pagination.= "<li><a href='javascript:void(0);' onclick='click_Ajax($lastpage)'>Last</a></li>";
    }else{
        $pagination.= "<li class=''><a href='javascript:void(0);'>Next</a></li>";
        $pagination.= "<li class=''><a href='javascript:void(0);'>Last</a></li>";
    }
    $pagination.= "</ul>\n";        
}
echo $pagination;  
?>
