<?php

include "../config/config_gcp.php";

$row        = array();
$return_arr = array();
$row_array  = array();

if (isset($_GET['q']) && strlen($_GET['q']) > 0) {
    
        if (isset($_GET['q'])) {
            $getVar = $_GET['q'];
            
            $whereClause = " p.name LIKE '%" . $getVar . "%' ";
        }
        
       // $sql = "SELECT id , name FROM cities WHERE $whereClause and $andClause1  ";
        
         $sql = "SELECT p.name as variety , p.id as idvar ,
                        gs.sizes ,  gs.is_bunch_value , s.name AS size_name, fe.id as idfea,
                        fe.name as featurename ,  gr.bunch_size_id , bs.name as bunchesv   ,
                        gs.categoryid , gs.sid, sc.name as subcate
                   FROM grower_product_bunch_sizes gs
                  INNER JOIN product AS p   ON gs.product_id=p.id
                  INNER JOIN subcategory AS sc   ON gs.sid = sc.id and gs.categoryid = sc.cat_id
                  INNER JOIN growers AS g   ON gs.grower_id=g.id
                  INNER JOIN grower_product_box_packing  gr ON( gr.sizeid=gs.sizes AND gr.bunch_size_id=gs.bunch_sizes AND gr.growerid=gs.grower_id AND gr.prodcutid=gs.product_id)
                  INNER JOIN sizes AS s     ON gs.sizes=s.id
                  INNER JOIN boxes AS b     ON b.id=gr.box_id
                  INNER JOIN boxtype AS bt  ON b.type=bt.id
                   LEFT JOIN features AS fe ON fe.id=gr.feature
                  INNER JOIN bunch_sizes AS bs  ON gr.bunch_size_id=bs.id
                  where g.active = 'active'
                    and $whereClause
                  group by p.name , p.id,
                           gs.sizes ,  gs.is_bunch_value , s.name ,fe.id ,
                           fe.name  ,  gr.bunch_size_id , bs.name 
                  ORDER BY p.name, s.name  ";                                                                                                                
        
        
        
                        
        $result = mysqli_query($con, $sql);
        
        while ($row = mysqli_fetch_array($result)) {
            $row_array['id']   = $row['idvar'];
            $row_array['text'] = $row['variety']."-".$row['subcate']."-".$row['size_name']."-cm ".$row['bunchesv']."st/bu-".$row['featurename'];
            array_push($return_arr, $row_array);
            
        }
   
} else {
    $row_array['id'] = 0;
    $row_array['text'] = 'Start Typing....';
    array_push($return_arr, $row_array);
}


$ret = array();
$ret['results'] = $return_arr;
echo json_encode($ret);
?>
