<?php
require_once("../config/config_gcp.php");
// PO 2018-07-02

$menuoff = 1;
$page_id = 5;
$message = 0;

$today   = date('Y-m-d');
$sw      = '0';

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];

//$_SESSION['inicio'] = time();

//echo 'Pasaron ' , (time() - $_SESSION['inicio']) ,  segundos; 





/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}


//  Actualizacion en Grupo
if (isset($_REQUEST["total"])) {

    for ($i = 1; $i <= $_POST["totalrow"] - 1; $i++) {
        
        
        $qryMax="select (max(id)+1) as id from grower_offer_reply";
        $dataMaximo = mysqli_query($con, $qryMax);
        
	while ($dt = mysqli_fetch_assoc($dataMaximo)) {
                                $IdMaximo= $dt['id'];
	}

        ////// Insert Inicio
        
$qrt_offer  = "select id   , reject2    , reason2       , offer_id      , offer_id_index , grower_id      , buyer_id    , status    , 
                      product           , product_subcategory,
                      size , boxtype    , bunchsize     , boxqty        , bunchqty       , steams         , total_stems , price     , 
                      total_price_st_bu , date_added    , 
                      type , accept_date, box_weight    , box_volumn    , grower_box_name, shipping_method, reject      , reason    , 
                      shipping          , handling      , 
                      tax  , totalprice , totalprice_box, totalstems_box, atprice_box    , atqty_box      , finalprice  , paid      ,
                      billing_add       , coordination  , 
                      marks , cargo     , invoice       , inventary     , unit           , req_group      , request_id  , cliente_id, 
                      type_market       , num_box, branch
                 from grower_offer_reply 
                where id     ='" . $_POST["pro-" . $i]  . "'    ";

$grower_offer   = mysqli_query($con, $qrt_offer);

while ($growoffrep = mysqli_fetch_array($grower_offer)) {  
   // echo "D";
    
    
    if ($_POST["client_". $_POST["pro-" . $i]]==47){
        
        $update_gen = "update grower_offer_reply 
                          set marks = 'GROUP-OTHER'            ,
                              cliente_id     = '0',
                              bunchqty       = '" . $growoffrep['qty_pack'] . "' 
                       where id='" . $_POST["pro-" . $i] . "'  ";
                                       
            mysqli_query($con, $update_gen);
        
    }
    
            $update = "update grower_offer_reply 
                          set marks = 'GROUP-UPDATE'  ,                        
                              cliente_id     = '" . $_POST["client_". $_POST["pro-" . $i]] . "',
                              bunchqty       = '" . $_POST["change_". $_POST["pro-" . $i]] . "',
                              branch         = '" . $_POST["branch_". $_POST["pro-" . $i]] . "'        
                       where id='" . $_POST["pro-" . $i] . "'  
                        and cliente_id in ('0','1',47)";                               
    
        mysqli_query($con, $update);  
        
                $id_off          = $IdMaximo; 
                $marks            = "NEW"; 
                $date_added      = $today;                  

                
                $reject2            = $growoffrep['reject2'];   
                $reason2            = $growoffrep['reason2'];   
                $offer_id           = $growoffrep['offer_id'];                
                $offer_id_index     = $growoffrep['offer_id_index'];   
                $grower_id          = $growoffrep['grower_id'];   
                $buyer_id           = $growoffrep['buyer_id'];  
                $status             = $growoffrep['status'];   
                $product            = $growoffrep['product'];   
                $product_subcategory= $growoffrep['product_subcategory'];  
                $size               = $growoffrep['size'];   
                $boxtype            = $growoffrep['boxtype'];   
                $bunchsize          = $growoffrep['bunchsize'];   
                $boxqty             = $growoffrep['boxqty'];   
                $bunchqty           = $growoffrep['bunchqty'];   
                $steams             = $growoffrep['steams'];   
                $total_stems        = $growoffrep['total_stems'];   
                $price              = $growoffrep['price'];   
                $total_price_st_bu  = $growoffrep['total_price_st_bu'];                   
                $type               = $growoffrep['type'];   
                $accept_date        = $growoffrep['accept_date'];   
                $box_weight         = $growoffrep['box_weight'];   
                $box_volumn         = $growoffrep['box_volumn'];   
                $grower_box_name    = $growoffrep['grower_box_name'];   
                $shipping_method    = $growoffrep['shipping_method'];   
                $reject             = $growoffrep['reject'];  
                $reason             = $growoffrep['reason'];  
                $shipping           = $growoffrep['shipping'];   
                $handling           = $growoffrep['handling'];   
                $tax                = $growoffrep['tax'];   
                $totalprice         = $growoffrep['totalprice'];   
                $totalprice_box     = $growoffrep['totalprice_box'];   
                $totalstems_box     = $growoffrep['totalstems_box'];   
                $atprice_box        = $growoffrep['atprice_box'];   
                $atqty_box          = $growoffrep['atqty_box'];   
                $finalprice         = $growoffrep['finalprice'];   
                $paid               = $growoffrep['paid'];   
                $billing_add        = $growoffrep['billing_add'];   
                $coordination       = $growoffrep['coordination'];   
                $cargo              = $growoffrep['cargo'];   
                $invoice            = $growoffrep['invoice'];   
                $inventary          = $growoffrep['inventary'];   
                $unit               = $growoffrep['unit'];   
                $req_group          = $growoffrep['req_group'];   
                $request_id         = $growoffrep['request_id'];   
                $cliente_id         = $growoffrep['cliente_id'];   
                $type_market        = $growoffrep['type_market'];   
                $num_box            = $growoffrep['num_box'];   
                $branch             = $growoffrep['branch'];                 
                                                                
                
                $cliente_id      = 0;   
                $processed       = 'N';          
                $bunchqty        = $growoffrep['bunchqty'] - $_POST["change_". $_POST["pro-" . $i]]; 
                                                
                
                if ($_POST["client_". $_POST["pro-" . $i]]!=47){
                    $sw=0;
                }                            
              
  if ($bunchqty != '0') {

    if ($sw == '0') {
                
            $insert_offerChange = "INSERT INTO grower_offer_reply
                           (id                   , reject2                , reason2                       , offer_id              , offer_id_index          , grower_id             , buyer_id    , 
                            status               , product                , product_subcategory           , size                  , boxtype                 , bunchsize             , boxqty      ,
                            bunchqty             , steams                 , total_stems                   , price                 , total_price_st_bu       , date_added            , type        , 
                            accept_date          , box_weight             , box_volumn                    , grower_box_name       , shipping_method         , reject                , reason      , 
                            shipping             , handling               , tax                           , totalprice            , totalprice_box          , totalstems_box        , atprice_box , 
                            atqty_box            , finalprice             , paid                          , billing_add           , coordination            , marks                 , cargo       ,
                            invoice              , inventary              , unit                          , req_group             , request_id              , cliente_id            , type_market , 
                            num_box              , branch)                                         
                    VALUES ('".$id_off."'        , '" . $reject2 .  "'    , '" . $reason2 . "'            , '".$offer_id."'       , '".$offer_id_index."'   , '".$grower_id."'      , '".$buyer_id."'    ,
                            '".$status."'        , '" . $product .  "'    , '" . $product_subcategory . "', '".$size."'           , '".$boxtype."'          , '".$bunchsize."'      , '".$boxqty."'      ,
                            '".$bunchqty."'      , '" . $steams . "'      , '" . $total_stems . "'        , '".$price."'          , '".$total_price_st_bu."', '".$date_added."'     , '".$type."'        ,
                            '".$accept_date."'   , '" . $box_weight. "'   , '" . $box_volumn . "'         , '".$grower_box_name."', '".$shipping_method."'  , '".$reject."'         , '".$reason."'      ,                                            
                            '".$shipping."'      , '" . $handling . "'    , '" . $tax . "'                , '".$totalprice."'     , '".$totalprice_box."'   , '".$totalstems_box."' , '".$atprice_box."' ,
                            '".$atqty_box."'     , '" . $finalprice . "'  , '" . $paid . "'               , '".$billing_add."'    , '".$coordination."'     , '".$marks."'          , '".$cargo."'       ,
                            '".$invoice."'       , '" . $inventary . "'   , '" . $unit . "'               , '".$req_group."'      , '".$request_id."'       , '".$cliente_id."'     , '".$type_market."' ,                                  
                            '".$num_box."'       , '" . $branch . "'  )";     
            

            mysqli_query($con, $insert_offerChange);
    }
    
    if ($_POST["client_". $_POST["pro-" . $i]]==47){
          $sw=1;
     }            
  }                                                                                        
    
  }
       
}
}

//    Fin submit

//$today = date("m-d-Y H:i:s", strtotime("+2 hours"));
//$today1 = explode(" ", $today);
//$today_date = $today1["0"];
//$time = $today1["1"];
//$hours_array = explode(":", $time);

// Paginacion
$sel_pagina = "select gr.*
                  from grower_offer_reply gr 
                 where gr.buyer_id='" . $userSessionID . "'
                   and gr.offer_id >= '6731'
                   and gr.reject in (0) ";

$rs_pagina    = mysqli_query($con, $sel_pagina);
$total_pagina = mysqli_num_rows($rs_pagina);
$num_record   = $total_pagina;

$display = 50;
$XX = '<div class="notfound">No Item Found !</div>';


function box_type($box){
    $box_type_s = "";
    if ($box == "0") {
        $box_type_s = "Stems";
    } else if ($box == "1") {
        $box_type_s = "Bunch";
    }
    return $box_type_s;
}

function box_type_name($id_box_type){
    global $con;
      
    $sel_box_type = "select code as name from units where id='" . $id_box_type . "'";
    $rs_box_type = mysqli_query($con, $sel_box_type);
    $box_type = mysqli_fetch_array($rs_box_type);
    return $box_type["name"];
}

function box_descrip($id_box_type){
    global $con;    
    $sel_unit_type = "select descrip from units where id='" . $id_box_type . "'";
    $rs_unit_type = mysqli_query($con, $sel_unit_type);
    $box_descrip_type = mysqli_fetch_array($rs_unit_type);
    return $box_descrip_type["descrip"];
}

function box_name($box_type){
    
    $res = "";
    if ($box_type == "HB") {
        $res = "half boxes";
    } else if ($box_type == "EB") {
        $res = "eight boxes";
    } else if ($box_type == "QB") {
        $res = "quarter boxes";
    } else if ($box_type == "JB") {
        $res = "jumbo boxes";
    }
    return $res;
}

function category(){
    global $con;
    $category_sql = "SELECT id,name from product order by  name";
    $result_category = mysqli_query($con, $category_sql);
    return $result_category;
}

function query_main($user, $init, $display){ 

    $query = "select g.growers_name , g.id as grower_cod,
                     gr.id as idbox  ,  
                     gr.bunchqty  as qty_pack  , 
                     gr.steams      ,
                     gr.offer_id    ,                      
                     gr.product     , 
                     gr.request_id  , 
                     gr.cliente_id  ,                     
                     gr.branch  ,                     
                     gr.price       ,                      
                     gr.grower_id   ,                                           
                     gr.size ,                     
                     gr.product_subcategory as subs,                     
                     (gr.bunchqty*gr.steams) as totstems ,                      
                     if(gr.type_market=1,'OM','SO') as tmarquet,
                     p.id as prod_cod , 
                     p.color_id ,
                     p.image_path as img_url , 
                     scl.name as client_name ,
                     bo.order_number as embarque ,                      
                     bo.qucik_desc , 
                     bo.id as po ,
                     br.feature  , 
                     br.id_order ,                      
                     f.name as features , 
                     bra.name as branch_name                                          
                from grower_offer_reply gr 
               left join buyer_requests br  on gr.offer_id = br.id 
               left join product p          on gr.product = p.name 
               inner join buyer_orders bo    on br.id_order = bo.id                  
               inner join growers g          on gr.grower_id = g.id 
                left join sub_client scl     on gr.cliente_id = scl.id
                left join sub_client_branch bra on gr.branch = bra.id
                left join features f            on br.feature = f.id                
               where gr.buyer_id='" . $user . "'
                 and br.id_order in ('728')
               order by gr.product_subcategory,gr.product  LIMIT " . $init . ",$display";
    
    return $query;    
}

if (isset($_POST["startrow"]) && $_POST["startrow"] != "") {
    $sr = $_POST["startrow"] + 1;
    $query2 = query_main($userSessionID, $_POST["startrow"], $display);
    $result2 = mysqli_query($con, $query2);
} else {
    if (empty($startrow)) {
        $startrow = 0;
        $sr = 1;
    }
    $query2 = query_main($userSessionID, 0, $display);
    $result2 = mysqli_query($con, $query2);
}


?>
<?php require_once("../includes/profile-header.php"); ?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>
<?php include("../includes/left_sidebar_buyer.php"); ?>
<style type="text/css">
    .not_set_border td {
        border: none !important;
    }

</style>
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
 
</head>
<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Customer</h1>
        <ol class="breadcrumb">
            <li><a href="#">Box</a></li>
            <li class="active">Packing</li>
        </ol>
    </header>
    
    <!-- /page title -->
    <div id="content" class="padding-20">
        
        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong><h4>Offers Customer (Subclient)</h4></strong> <!-- panel title -->
                </span>

                <!-- right options -->
               <?php $dates = date("jS F Y");?>
                
                                    <ul class="options pull-right relative list-unstyled">
                                        <div class="tags_selected" id="tab_for_filter" style="float: left; margin-top: 4px; margin-right: 4px; display: none;"></div>
                                        <li>
                                          <!--  <a class="date_filter btn btn-danger btn-xs white" id="dates"><?= $_POST['order'] ?></a>-->
                                            <!--a class="date_filter btn btn-danger btn-xs white" id="dates"><?php echo $dates; ?></a-->
                                            <a href="#" class="btn btn-primary btn-xs white" data-toggle="modal" data-target=".search_modal_open"><i class="fa fa-filter"></i> Filter</a>
                                        </li>
                                    </ul>
                                        <div class="modal fade search_modal_open" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <form name="form1" id="form1" method="post" action="buyer/my-offers.php" class="modal fade search_modal_open">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">                                    
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Choose Filter</h4>
                                                        </div>
                                                        <!-- Modal Body -->
                                                        <div class="modal-body">
                                                            <div class="panel-body">                                                        

                                                                <label>Variety</label>
                                                                <div class="form-group">
                                                                    
                                                                    
                                                                    <select class="ui fluid search dropdown"   multiple="" name="filter_variety"  id="filter_variety" style="width: 100%; diplay: none;">
                                                                        <option value="">Select Variety1</option>
                                                                    </select>                                                                    
                                                                    
                                                                </div>                                                                
                                                                
                                                                <label>shipment</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_category" id="filter_category" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select shipment</option>
                                                                        <?php
                                                                        $buyer_sql = "select  id, order_number  from  buyer_orders where  buyer_id='" . $userSessionID . "' order by id desc";
                                                                        $result_buyer = mysqli_query($con, $buyer_sql);
                                                                        while ($row_buyer = mysqli_fetch_assoc($result_buyer)) { ?>
                                                                            <option value="<?php echo $row_buyer['id']; ?>"><?php echo $row_buyer['order_number']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div> 
                                                                
                                                                <label>Subclient</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_grower" id="filter_grower" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Subclient</option>
                                                                        <?php
                                                                        $subcategory_sql = "select sb.id , sb.name 
                                                                                              from sub_client sb
                                                                                             where  sb.id != 1 
                                                                                               and sb.buyer = '" . $userSessionID . "' 
                                                                                              order by sb.name ";
                                                                        
                                                                        $result_subcategory = mysqli_query($con, $subcategory_sql);
                                                                        
                                                                        while ($row_subcategory = mysqli_fetch_assoc($result_subcategory)) { ?>
                                                                            <option value="<?php echo $row_subcategory['id']; ?>"><?php echo $row_subcategory['name']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div>                                                                 
                                                                
                                                                <label>Grower</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_size" id="filter_size" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Grower</option>
                                                                        <?php
                                                                        $grower_sql = "select  id, growers_name  from  growers where active = 'active' order by growers_name ";
                                                                        $result_grower = mysqli_query($con, $grower_sql);
                                                                        while ($row_grower = mysqli_fetch_assoc($result_grower)) { ?>
                                                                            <option value="<?php echo $row_grower['id']; ?>"><?php echo $row_grower['growers_name']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div>                                                                 

                                                                
                                                                                                                                                                                             
                                                                

                                                            </div>
                                                        </div>                                                                
                                                        <!-- Modal Footer -->
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            <button id="btn_filter" class="btn btn-primary apply" type="button">Apply</button>
                                                            <img class="ajax_loader_s" style="display: none;" src="<?php echo SITE_URL; ?>images/ajax-loader.gif"/>
                                                        </div>                                                        
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                <!-- /right options -->
            </div>            
            
            <!-- panel content inicio po -->            
            <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">
                     
                    <div class="panel-body">
                    <input type="submit" id="submitu" class="btn btn-success btn-sm" name="submitu" value="Update Changes">     
                    
                                    <div id='loading'>
                                        <div class=" cssload-spin-box">
                                         <!--   <img src="<?php echo SITE_URL; ?>../includes/assets/images/loaders/5.gif"> -->
                                        </div>
                                    </div>                
                
                
                    <div  class="dataRequest">
                                                                                                                                                            
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                            <tr>                            
                                <!--th width="2"><font color="teal"> </font></th-->
                                <!--th width="200"><font color="teal">SubCategory / Grower / Variety</font></th>
                                <!--th width="100"><font color="teal">Ord.</font></th>                                 
                                <th width="200"><font color="teal">Grower</font></th>  
                                <th width="100"><font color="teal">Box</font></th>  
                                <th width="300"><font color="teal">Product</font></th>
                                <th width="100"><font color="teal">Change Qty</font></th>
                                <th width="100"><font color="teal">Client</font></th-->                                                                   
                            </tr>                        
                        </thead>
                        
                        <tbody id="list_inventory">
                        <?php

                        $cn = 1;
                        $quiebre = 0;  

                        while ($producs = mysqli_fetch_array($result2)) {
                        ?>
                                <tr>
                                <?php
                                        $total_bunchs="";
                                        $boxes_type="";
                                        if (!in_array($growers['grower_id'] . "-" . $growers["offer_id_index"], $al_ready_gro_in)) {
                                                $getGrowerReply = "select offer_id , bunchqty , boxqty 
                                                                     from grower_offer_reply 
                                                                    where reject in (0) 
                                                                      and request_id    ='" . $producs['cartid'] . "'
                                                                      and grower_id     ='" . $growers['grower_id'] . "' 
                                                                      and offer_id_index='" . $growers["offer_id_index"] . "' limit 1";
                                                                            
                                                $rs_getGrowerReply = mysqli_query($con, $getGrowerReply);
                                                $tot_bunch_qty = 0;
                                                                            
                                                while ($row_rs_getGrowerReply = mysqli_fetch_array($rs_getGrowerReply)) {
                                                    $tot_bunch_qty = $tot_bunch_qty + $row_rs_getGrowerReply['boxqty'];
                                                }
                                                $total_bunchs= $tot_bunch_qty;
                                                if ($producs["boxtype"] != "") {
                                                    $temp = explode("-", $producs["boxtype"]);
                                                    $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                                                    $rs_box_type = mysqli_query($con, $sel_box_type);
                                                    $box_type = mysqli_fetch_array($rs_box_type);
                                                    $type_box = box_name($box_type["name"]);
                                                    $boxes_type= $type_box;
                                                }
                                        }

                                                                        //---------------------------------------------------------------------//
                                        $getbunchSize = "SELECT is_bunch_value,bunch_value,is_bunch  
                                                           FROM grower_product_bunch_sizes 
                                                          WHERE grower_id  = '" . $growers['grower_id'] . "' 
                                                            AND product_id = '" . $producs['product'] . "'   
                                                            AND sizes      = '" . $producs['sizeid'] . "'";
                                        
                                        $rs_bunchSize = mysqli_query($con, $getbunchSize);
                                        $row_bunchSize = mysqli_fetch_array($rs_bunchSize);
                                        $bunch_size_s = "";

                                        if ($row_bunchSize['bunch_value'] == 0) {
                                                    $bunch_size_s = $row_bunchSize['is_bunch_value'];
                                        } else {
                                                    //$bunch_size_s = $row_bunchSize['bunch_value'];
                                        }
                                        /////////////// Fotos Finca
                                        $get_img = "SELECT id, product_id, grower_id, categoryid, subcaegoryid, colorid, image_path as img_url , active
                                                      FROM grower_product 
                                                     WHERE grower_id  = '" . $producs['grower_cod'] . "' 
                                                       AND product_id = '" . $producs['prod_cod']   . "'   ";
                                        
                                        $rs_img = mysqli_query($con, $get_img);
                                        $row_imagen = mysqli_fetch_array($rs_img);   
                                        
                                        /////////////// Colores
                                        $get_color = "SELECT id, name as color_variety
                                                      FROM colors 
                                                     WHERE id = '" . $producs['color_id']   . "'   ";
                                        
                                        $rs_color = mysqli_query($con, $get_color);
                                        $row_color = mysqli_fetch_array($rs_color);                                        
                                                                        //---------------------------------------------------------------------//
                                                         ?>                                                                                                          
                         <?php

                        $boxPacking = $producs['box_name'];      
                        $box_num = "";
      
                        while ($row_BoxReply = mysqli_fetch_array($rs_getBoxReply)) {
                            $box_num .=  $row_BoxReply['qty_box_packing']."-";         // acumular
                        }
                
                        if ($box_num != "") {
                            $boxPacking= $box_num."box";  
                        }
                         
                        if ($producs["po"] == $quiebre) {
                          ?>                                   
                                    <td><img src="https://app.freshlifefloral.com/<?= $row_imagen["img_url"]; ?>" width="70"></td>
                                    <td><?= $producs["subs"]; ?> </td>                                    
                                    <td><?= $producs["po"] ?></td>                                    
                                    <td><?= $producs["tmarquet"] ?></td>                                    
                                    <td><?= $producs["growers_name"] ?></td>
                                    <td><?= $boxPacking ?></td>                                    

                         <?php }else{?>     
                                    <td><img src="https://app.freshlifefloral.com/<?= $row_imagen["img_url"]; ?>" width="70"></td>
                                    <td><h5><?= $producs["subs"]; ?></h5> </td>                                                                        
                                    <td><h5><?= $producs["po"] ?></h5></td>
                                    <td><h5><?= $producs["tmarquet"] ?></h5></td>
                                    <td><h5><?= $producs["growers_name"] ?></h5></td>
                                    <td><h5><?= $boxPacking ?></h5></td>                                    
                         <?php } 
                                  $quiebre = $producs["po"]; 
                                  
                               if ($producs["type_market"] == "0") {
                                    $market = "Stand";
                              }else{
                                    $market = "Open";
                              }
                             ?>       
                                    

                                                                                                                                                                                                                        
                                                                            <?php
                                                                            if ($row_bunchSize['bunch_value'] == 0) {
                                                                                ?>                                                                                                                                                
                                                                                    <td><?php $box_type_s = box_type($producs['box_type']); ?>
                                                                                        
                                                                                        <?= $producs["product"]. " " ?><?= $producs["size"] . " cm " ?><?= $producs["qty_pack"] . " Bunchess " ?><?= $producs["steams"] . " st/bu "; ?><?= $producs["features"]."  " ?><?= $row_color["color_variety"] ; ?>
                                                                                    </td>                                                                        
                                                                            <?php }else{?>
                                                                                    <td><?php $box_type_s = box_type($producs['box_type']); ?>
                                                                                        <?= $producs["product"]. " " ?><?= $producs["size"] . " cm " ?><?= $producs["qty_pack"] . " Bunches " ?><?= $producs["steams"] . " st/bu  "; ?><?= $row_color["color_variety"] ; ?>
                                                                                    </td>                                                                                                                                                        
                                                                            <?php }
                                                                            ?>                                                                        

                                                                                    
                                                                        <td>                                                                            
                                                                            <!--a href="" data-toggle="modal" data-target="#single_product_price<?php echo $producs["idbox"] ?>">$<?php echo sprintf("%.2f", number_format($producs['price_cad'], 2, '.', ',') ) ?> </a-->
                                                        <!--Modal image for 3452-->
                                                        <div class="modal fade bs-example-modal-sm" id="single_product_price<?php echo $producs["idbox"] ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
                                                             style="display: none;">
                                                            <div class="modal-dialog modal-sm">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                                                                        <h4 id="myLargeModalLabel3" class="modal-title" style="font-size: 14px;">ALL IN PRICE</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <p>Farm Price...........................$<?= sprintf("%.2f", number_format($producs['gorPrice'], 2, '.', ',') ) ?></p>
                                                                                <p>Duties 22%...........................$<?= sprintf("%.2f", number_format($producs['duties'], 2, '.', ',')) ?></p>
                                                                                <p>Subtotal................................$<?= sprintf("%.2f", number_format(($producs['gorPrice']+$producs['duties']), 2, '.', ',')) ?></p>
                                                                                <p>Shipping................................$<?= sprintf("%.2f", number_format($producs['ship_cost'], 2, '.', ',')) ?></p>
                                                                                <p>Subtotal Shipping.................$<?= sprintf("%.2f", number_format(($producs['gorPrice']+$producs['duties']+$producs['ship_cost']), 2, '.', ',') ) ?></p>
                                                                                <p>FLF Handling 8%...................$<?= sprintf("%.2f", $producs['handling_pro']) ?></p>
                                                                                <p>Total.......................................$<?= sprintf("%.2f", number_format(($producs['gorPrice']+$producs['duties']+$producs['ship_cost']+$producs['handling_pro']), 2, '.', ',')) ?></p>
                                                                                <div class="divider divider-dotted"><!-- divider --></div>
                                                                                
                                                                                <p style="color: #273746"><strong>FinalPrice in CAD................$<?= sprintf("%.2f", number_format($producs['price_cad'], 2, '.', ',') ) ?></strong></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                                            
                                                                 </td>  
                                                                                    
                                                                        

                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="change_<?php echo $producs["idbox"]; ?>" id="change_<?php echo $producs["idbox"] ; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                                <?php
                                                                                                    $boxDataShow = $producs["qty_pack"];
                                                                                                    for ($ii = $boxDataShow; $ii >= 1; $ii--) {
                                                                                                ?>
                                                                                            <option value="<?= $ii ?>"><?= $ii ?></option>
                                                                                            <?php } ?>                                         
                                                                                        </select>                                                                        
                                                                                    </td>                                                                        
                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="client_<?php echo $producs["idbox"] ; ?>" id="client_<?php echo $producs["idbox"] ; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value=""><?= $producs['client_name']; ?></option>    
                                                                                            <?php
                                                                                            $sql_cli = "select sc.id ,   CONCAT(sc.name ,'  ',p.name,'  ',s.name,' (',sum(qty),') ',sizeid) as name
                                                                                                          from sub_client sc
                                                                                                          inner join buyer_requests br  on br.id_client = sc.id 
                                                                                                          inner join product p  on br.product = p.id 
                                                                                                          inner join subcategory s  on p.subcategoryid = s.id 
                                                                                                          where sc.id not in (0,1) 
                                                                                                            and sc.buyer = '" . $userSessionID . "'
                                                                                                            and br.id_order in (728)
                                                                                                          group by sc.id , sc.name , br.product
                                                                                                          order by sc.name";
                                                                                             
                                                                                            $result_cli = mysqli_query($con, $sql_cli);
                                                                                            while ($row_client = mysqli_fetch_assoc($result_cli)) { ?>
                                                                                                <option value="<?php echo $row_client['id']; ?>"><?= $row_client['name']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>                                                                        
                                                                                    </td> 
                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="branch_<?php echo $producs["idbox"] ; ?>" id="branch_<?php echo $producs["idbox"] ; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value=""><?= $producs['branch_name']; ?></option>    
                                                                                            <?php
                                                                                            $sql_bra = "select id,name 
                                                                                                          from sub_client_branch 
                                                                                                         order by id";
                                                                                            
                                                                                            $result_bra = mysqli_query($con, $sql_bra);
                                                                                            
                                                                                            while ($row_branch = mysqli_fetch_assoc($result_bra)) { ?>
                                                                                                <option value="<?php echo $row_branch['id']; ?>"><?= $row_branch['name']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>                                                                        
                                                                                    </td>                                                                                     
                                                                                    <td>
                                                                                        <input type="hidden" class="form-control" name="pro-<?php echo $cn ?>" value="<?php echo $producs["idbox"] ?>"/>
                                                                                    </td>                                                                                    
                                                                                    <td>
                                                                                        <input type="hidden" class="form-control" name="pro-<?php echo $cn ?>" value="<?php echo $producs["idbox"] ?>"/>
                                                                                    </td>

                                                                        <td>
                                                                            
                                                                            <input type="hidden" name="off-id-<?= $cn ?>" value="<?php echo $producs['cartid'];         ?>"/>
                                                                            <input type="hidden" name="offer_id[]"        value="<?php echo $producs['cartid'];         ?>"/>                                                                            
                                                                            <input type="hidden" name="product_name[]"    value="<?php echo $producs['name'];           ?>"/>
                                                                            <input type="hidden" name="product_size[]"    value="<?php echo $producs["sizename"];       ?>"/>                                                                            
                                                                            
                                                                            <input type="hidden" name="grid_id[]"         value="<?php echo $producs['grid']; ?>"/>
                                                                            <input type="hidden" name="grower_id[]"       value="<?php echo $producs['grower_id']; ?>"/>
                                                                            <input type="hidden" name="box_qty[]"         value="<?php echo $producs['boxqty']; ?>"/>
                                                                            <input type="hidden" name="box_type[]"        value="<?php echo $producs['boxtype']; ?>"/>
                                                                            <input type="hidden" name="product_price[]"   value="<?php echo $producs['price']; ?>"/>                                                                            
                                                                            <input type="hidden" name="bunch_size[]"      value="<?php echo $bunch_size_s; ?>"/>
                                                                            <input type="hidden" name="is_bunch[]"        value="<?php echo $row_bunchSize['is_bunch']; ?>"/>
                                                                            
                                                                            <input type="hidden" name="btn_add_to_cart"   value="Submit"/>

                                                                       </td>
                                                                      
                                                                                                                                                
                                </tr>
                                <?php
                                $cn++;
                            }
                            
                            
   
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
                    <input type="hidden" name="totalrow" value="<?php echo $cn ?>"/>
         </form>  
            <!-- /panel content -->

        </div>
        <!-- /PANEL -->

            <nav>
                <ul class="pagination">
                    <?php
                    if ($_POST["startrow"] != 0) {

                        $prevrow = $_POST["startrow"] - $display;

                        print("<li><a href=\"javascript:onclick=funPage($prevrow)\" class='link-sample'>Previous </a></li>");
                    }
                    $pages = intval($num_record / $display);

                    if ($num_record % $display) {

                        $pages++;
                    }
                    $numofpages = $pages;
                    $cur_page = $_POST["startrow"] / $display;
                    $range = 5;
                    $range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2;
                    $range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min;
                    $page_min = $cur_page - $range_min;
                    $page_max = $cur_page + $range_max;
                    $page_min = ($page_min < 1) ? 1 : $page_min;
                    $page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max;
                    if ($page_max > $numofpages) {
                        $page_min = ($page_min > 1) ? $numofpages - $range + 1 : 1;
                        $page_max = $numofpages;
                    }
                    if ($pages > 1) {
                        print("&nbsp;");
                        for ($i = $page_min; $i <= $page_max; $i++) {
                            if ($cur_page + 1 == $i) {
                                $nextrow = $display * ($i - 1);
                                print("<li class='active'><a href='javascript:void();'>$i</a></li>");
                            } else {

                                $nextrow = $display * ($i - 1);
                                print("<li><a href=\"javascript:onclick=funPage($nextrow)\"  class='link-sample'> $i </a></li>");
                            }
                        }
                        print("&nbsp;");
                    }
                    if ($pages > 1) {

                        if (!(($_POST["startrow"] / $display) == $pages - 1) && $pages != 1) {

                            $nextrow = $_POST["startrow"] + $display;

                            print("<li><a href=\"javascript:onclick=funPage($nextrow)\" class='link-sample'> Next</a></li> ");
                        }
                    }

                    if ($num_record < 1) {
                        print("<span class='text'>" . $XX . "</span>");
                    }
                    ?></ul>
            </nav>        
        
        
    </div>
    
        <form method="post" name="frmfprd" action="">
            <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
        </form>    
</section>

<?php require_once '../includes/footer_new.php'; ?>

<script type='text/javascript'>
    $(window).load(function () {
        $('#loading').css("display", "none");
    });
</script>
<script type="text/javascript">
    jQuery(window).ready(function () {
        loadScript('/assets/js/jquery/jquery-ui.min.js', 
        function () {
            /** jQuery UI **/
            loadScript('/assets/js/jquery/jquery.ui.touch-punch.min.js', 
            function () {
                /** Mobile Touch Slider **/
                loadScript(plugin_path + 'form.slidebar/jquery-ui-slider-pips.min.js', 
                function () { /** Slider Script **/
                    /** Slider 5******************** **/
                    var id = "";
                    jQuery(".slider5").slider({
                        //value: 0.01, 
                        animate: true,
                        min: 0.05,
                        max: 2,
                        step: 0.01,
                        range: "min",
                        slide: function (event, ui) {

                              jQuery(".select_price_cls").val(ui.value);                         
                            var idx = $(this).attr("id");
                            var arr = idx.split('_');
                            
                              $("#grower_price_" + arr[1]).val(+ui.value); 
                                                        
                            var taxes = $("#tax_porcent_" + arr[1]).val();
                            var red = (ui.value * taxes) / 100;
                            var handling = $("#ha_" + arr[1]).val();
                            var reda = (ui.value * handling) / 100;
                                                       
                            $("#final_price_" + arr[1]).val(ui.value);
                            
                            var grower_pricex = (ui.value / (1 + (taxes / 100) + (handling / 100))).toFixed(2);
                            var handling = (grower_pricex * handling / 100).toFixed(2);
                            var tax = (ui.value); 
                            
                            $("#grower_price_" + arr[1]).val(+grower_pricex);
                            $("#handling_" + arr[1]).val(+handling);
                            $("#tax_" + arr[1]).val(+tax);                            
                        }
                    });

                });
            });
        });
    });
    $(".clss").keypress(function (e) {
        if (e.which == 13) {
            // Acciones a realizar, por ej: enviar formulario.
            //$('#frm').submit();
            var id = $(".clss").attr("id");
            var i = id.split('_');
            var final_price = $("#select_price_cls_" + i[3]).val();
            $("#slider5_" + i[3]).slider("value", final_price);
        }
    });
        function funPage(pageno) {
            document.frmfprd.startrow.value = pageno;
            document.frmfprd.submit();
        }
    
</script>
<script type="text/javascript">
    $(document).ready(function () {
        
        
                $('#filter_variety').select2({
                    ajax: {
                        url: "search_variety_filter.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });        
        
        
        
        
        var productsearch = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '<?php echo SITE_URL; ?>/includes/autosuggest.php?limit=10&search=%QUERY',
                wildcard: '%QUERY'
            }
        });
        $('.autosuggest #typeSearch').typeahead(null, {
            name: 'productsearch',
            source: productsearch
        });
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            //console.log('Selection: ' + suggestion);
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/inventory_search_product.php',
                data: 'name=' + suggestion,
                success: function (data) {
                    $("#pagination_nav").hide();
                    $('.imgDiv').hide();
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });

        /*Josse P*/

        $body = $("body");
        function call_Ajax_Fliter() {
            jQuery("#btn_filter").click(function () {
                var check_value = 0;
                var data_ajax = "";
                if (jQuery("#filter_category").val() == "" &&
                    jQuery("#filter_grower").val() == "" &&                        
                    jQuery("#filter_size").val() == "" &&                                            
                    jQuery("#filter_box").val() == "" &&                                                                
                    jQuery("#filter_variety").val() == "" ) {
                    check_value = 1;
                }
                if (check_value == 1) {
                    alert("Please select any one option.");
                }


                else {
                    var filter_category = jQuery("#filter_category").val();
                    var filter_grower = jQuery("#filter_grower").val();                    
                    var filter_size = jQuery("#filter_size").val();                                        
                    var filter_box = jQuery("#filter_box").val();                                                            
                    var filter_variety = jQuery("#filter_variety").val();
                    

//alert(jQuery("#filter_variety").val());


alert(filter_variety);

                    jQuery(".ajax_loader_s").css("display", "inline-block");
                    $.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>buyer/getFilterOffer_group.php',
                        data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_grower=' + filter_grower + '&filter_size=' + filter_size + '&filter_box=' + filter_box,
                        success: function (data) {
                            jQuery("#tab_for_filter").html("");
                            $("#pagination_main").hide();
                            jQuery('.search_modal_open').modal('hide');
                            jQuery("#list_inventory").html(data);   //nombre del  id  del <tbody>
                            var pass_delete_f = "";
                            
                            if (filter_category == "") {
                                var filter_category_temp = "0";
                            }
                            else {
                                var filter_category_temp = filter_category;
                            }
                            
                            if (filter_variety == "") {
                                var filter_variety_temp = "0";
                            }
                            else {
                                var filter_variety_temp = filter_variety;
                            }                                                        
                                                       
                            if (filter_grower == "") {
                                var filter_grower_temp = "0";
                            }
                            else {
                                var filter_grower_temp = filter_grower;
                            }
                            
                            if (filter_size == "") {
                                var filter_size_temp = "0";
                            }
                            else {
                                var filter_size_temp = filter_size;
                            }

                            if (filter_box == "") {
                                var filter_box_temp = "0";
                            }
                            else {
                                var filter_box_temp = filter_box;
                            }



                            pass_delete_f += "'" + filter_category_text + "'" + ',' + filter_category_temp + ',';
                            pass_delete_f += "'" + filter_variety_text + "'" + ',' + filter_variety_temp + ',';
                            pass_delete_f += "'" + filter_grower_text + "'" + ',' + filter_grower_temp + ',';
                            pass_delete_f += "'" + filter_size_text + "'" + ',' + filter_size_temp + ',';
                            pass_delete_f += "'" + filter_box_text + "'" + ',' + filter_box_temp + ',';                            


                            if (filter_category != "") {
                                var pass_click_cate = "'" + filter_category_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_category_filter" cate_label="' + filter_category_text + '" name="hdn_selected_category_filter" value="' + filter_category + '" />' + filter_category_text);
                            }
                            
                            if (filter_variety != "") {
                                var pass_click_cate = "'" + filter_variety_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_variety_filter" cate_label="' + filter_category_text + '" name="hdn_selected_variety_filter" value="' + filter_variety + '" />' + filter_variety_text);
                            }

                            if (filter_grower != "") {
                                var pass_click_cate = "'" + filter_grower_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i>' +
                                    '<input type="hidden" id="hdn_selected_grower_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_grower_filter" ' +
                                    'value="' + filter_grower + '" />' + filter_grower_text);
                            }

                            if (filter_size != "") {
                                var pass_click_cate = "'" + filter_size_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i>' +
                                    '<input type="hidden" id="hdn_selected_size_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_size_filter" ' +
                                    'value="' + filter_size + '" />' + filter_size_text);
                            } 
                            

                            if (filter_box != "") {
                                var pass_click_cate = "'" + filter_box_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                    'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                    'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                    '<i class="fa fa-times"></i>' +
                                    '<input type="hidden" id="hdn_selected_box_filter" ' +
                                    'cate_label="' + filter_category_text + '" name="hdn_selected_box_filter" ' +
                                    'value="' + filter_box + '" />' + filter_box_text);
                            } 
                            
                            

                            jQuery("#tab_for_filter").show();
                            jQuery.ajax({
                                type: 'post',
                                url: '<?php echo SITE_URL; ?>buyer/getFilterInventPa.php',
                                data: 'filter_category=' + filter_category +
                                      '&filter_variety=' + filter_variety +
                                      '&filter_grower=' + filter_grower +
                                      '&filter_box=' + filter_box +                                      
                                '&filter_size=' + filter_size ,
                                success: function (data) {
                                    jQuery("#pagination_nav").html(data);
                                    jQuery(".ajax_loader_s").hide();
                                }
                            });
                        }
                    });
                }
            });
        }

        call_Ajax_Fliter();
        /* 21-10-2016 */
    });


    function deleteFilter(pass_click_cate, filter_category_text, filter_category, filter_variety_text, filter_variety, 
                          filter_grower_text, filter_grower, filter_size_text, filter_size,
                          filter_box_text, filter_box) {
                              
        var a_tag_html = $("#tab_for_filter").find("a").length
        if (a_tag_html == "1") {
            location.reload();
        }
        else {
            
            if (pass_click_cate == filter_category_text) {
                filter_category = "";
            }
            
            if (pass_click_cate == filter_variety_text) {
                filter_variety = "";
            }

            if (pass_click_cate == filter_grower_text) {
                filter_grower = "";
            }
            
            if (pass_click_cate == filter_size_text) {
                filter_size = "";
            }
            
            if (pass_click_cate == filter_box_text) {
                filter_box = "";
            }            

            var check_value = 0;
            var data_ajax = "";
            if (filter_category == "" &&
                filter_variety == "" &&
                filter_size == "" &&                
                filter_box == "" &&                                
                filter_grower == "" ) {     // falta filter_size
                check_value = 1;
            }
            if (check_value == 1) {
                alert("Please select any one option.");
            }
            else {
                if (filter_category == "0") {
                    filter_category = "";
                }
                if (filter_variety == "0") {
                    filter_variety = "";
                }

                if (filter_grower == "0") {
                    filter_grower = "";
                }
                
                if (filter_size == "0") {
                    filter_size = "";
                }

                if (filter_box == "0") {
                    filter_box = "";
                }
                $body.addClass("loading");
                $.ajax({
                    type: 'post',
                    url: '<?php echo SITE_URL; ?>buyer/getFilterClient.php',
                    data: 'filter_category=' + filter_category +
                          '&filter_variety=' + filter_variety +
                           '&filter_grower=' + filter_grower +
                    '&filter_size=' + filter_size +
                    '&filter_box=' + filter_box +                    
                    '&page_number=1',
                    success: function (data) {
                        $body.removeClass("loading");
                        jQuery("#tab_for_filter").html("");
                        $("#pagination_main").hide();
                        jQuery('.search_modal_open').modal('hide');
                        jQuery("#list_inventory").html(data);
                        var pass_delete_f = "";
                        
                        if (filter_category == "") {
                            var filter_category_temp = "0";
                        }
                        else {
                            var filter_category_temp = filter_category;
                        }
                        
                        if (filter_variety == "") {
                            var filter_variety_temp = "0";

                        }
                        else {
                            var filter_variety_temp = filter_variety;
                        }

                        
                        if (filter_grower == "") {
                            var filter_grower_temp = "0";
                        }
                        else {
                            var filter_grower_temp = filter_grower;
                        }
                        
                        if (filter_size == "") {
                            var filter_size_temp = "0";
                        }
                        else {
                            var filter_size_temp = filter_size;
                        }

                        if (filter_box == "") {
                            var filter_box_temp = "0";
                        }
                        else {
                            var filter_box_temp = filter_box;
                        }

                        pass_delete_f += "'" + filter_category_text + "'" + ',' + filter_category_temp + ',';
                        pass_delete_f += "'" + filter_variety_text + "'" + ',' + filter_variety_temp + ',';
                        pass_delete_f += "'" + filter_grower_text + "'" + ',' + filter_grower_temp + ',';
                        pass_delete_f += "'" + filter_size_text + "'" + ',' + filter_size_temp + ',';
                        pass_delete_f += "'" + filter_box_text + "'" + ',' + filter_box_temp + ',';                        
                       
                        if (filter_category != "") {
                            var pass_click_cate = "'" + filter_category_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_category_filter" cate_label="' + filter_category_text + '" name="hdn_selected_category_filter" value="' + filter_category + '" />' + filter_category_text);
                        }
                        
                        if (filter_variety != "") {
                            var pass_click_cate = "'" + filter_variety_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_variety_filter" cate_label="' + filter_category_text + '" name="hdn_selected_variety_filter" value="' + filter_variety + '" />' + filter_variety_text);
                        }

                        if (filter_grower != "") {
                            var pass_click_cate = "'" + filter_grower_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i>' +
                                '<input type="hidden" id="hdn_selected_grower_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_grower_filter" ' +
                                'value="' + filter_grower + '" />' + filter_grower_text);
                        }

                        if (filter_size != "") {
                            var pass_click_cate = "'" + filter_size_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i>' +
                                '<input type="hidden" id="hdn_selected_size_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_size_filter" ' +
                                'value="' + filter_size + '" />' + filter_size_text);
                        }


                        if (filter_box != "") {
                            var pass_click_cate = "'" + filter_box_text + "'";
                            jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" ' +
                                'onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" ' +
                                'href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;">' +
                                '<i class="fa fa-times"></i>' +
                                '<input type="hidden" id="hdn_selected_box_filter" ' +
                                'cate_label="' + filter_category_text + '" name="hdn_selected_box_filter" ' +
                                'value="' + filter_box + '" />' + filter_box_text);
                        }


                        jQuery("#tab_for_filter").show();
                        jQuery.ajax({
                            type: 'post',
                            url: '<?php echo SITE_URL; ?>buyer/getFilterInventPa.php',
                            data: 'filter_category=' + filter_category +
                                  '&filter_variety=' + filter_variety +
                                  '&filter_grower=' + filter_grower +
                            '&filter_size=' + filter_size +
                            '&filter_box=' + filter_box +                            
                            '&page_number=1',
                            success: function (data) {
                                jQuery("#pagination_nav").html(data);
                                jQuery(".ajax_loader_s").hide();

                            }

                        });


                    }

                });

            }

        }

    }
    /*fin aumentado por mi deleter  filter*/
    $(function () {
        $('form.searchProduct').on('submit', function (e) {
            e.preventDefault();
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/search_request_page.php',
                data: $('form.searchProduct').serialize(),
                success: function (data) {
                    $('.imgDiv').hide();
                    $("#show_hide").toggle("slow");
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });
    });
</script>

<script>   
    
    function add_to_cart(offer, buyer, grower, gor_id ,idbox,request_group,cn) {
    
        var changeQty = $('#change_' + gor_id).val();
        var subClient = $('#client_' + gor_id).val();        
        
        
        var k = confirm('You want to assign Bunches to a client?');
            console.log("data " + "offer " + offer + "buyer " + buyer + "grower " + grower + " gor_id " + gor_id + " idbox " + idbox + " request_group " + request_group + " changeQty " + changeQty + " subClient " + subClient + " cn " + cn);        
        if (k == true) { 
            alert('Confirmed assignment'); 
            console.log("data " + "offer " + offer + "buyer " + buyer + "grower " + grower + " gor_id " + gor_id + " idbox " + idbox + " request_group " + request_group + " changeQty " + changeQty + " subClient " + subClient);
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/buyer/packing-viaje.php',
                data: 'offer=' + offer + '&buyer=' + buyer + '&grower=' + grower + '&gor_id=' + gor_id + '&idbox=' + idbox + '&request_group=' + request_group + '&changeQty=' + changeQty + '&subClient=' + subClient,
                success: function (data) {
                    location.reload();
                }   
            });
        }

    }

   
</script>