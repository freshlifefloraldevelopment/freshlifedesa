<?php

require_once("../config/config_gcp.php");

//include("../pagination/pagination.php");

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}

if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}

$userSessionID  = $_SESSION["buyer"];
$shippingMethod = $_SESSION['shippingMethod'];

/*********get the data of session user****************/

if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {

    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();

    if (empty($userID)) {
        header("location:" . SITE_URL);
        die;
    }

} else {
    header("location:" . SITE_URL);
    die;
}

$img_url = 'profile_images/noavatar.jpg';

if ($profile_image) {
    $img_url = 'profile_images/' . $profile_image;
}

$page_request = "inventory";

?>


<?php

$wh = "";


//if ($_REQUEST['filter_variety'] != "") {
//    $wh .= " AND p.id in (" . $_REQUEST['filter_variety'].")";  
//}  if (is_null($_REQUEST['filter_variety'])=== true) {

//$prueba2 = $_REQUEST['filter_variety'];


//if ((isset($_REQUEST['filter_variety'])) && (!empty($_REQUEST['filter_variety']))) {

if ($_REQUEST['filter_variety'] != "0") {
     echo "AAAAAAAAAAA";
    $wh .= " AND p.id in (" . $_REQUEST['filter_variety'].")";      
}else{
    echo "BBBBBBBBBBBB";
}

if ($_REQUEST['filter_category'] != "") {
    $wh .= " AND bo.id=" . $_REQUEST['filter_category'];
}

if ($_REQUEST['filter_grower'] != "") {
    $wh .= " AND ipx.cliente_id=" . $_REQUEST['filter_grower'];
}

if ($_REQUEST['filter_size'] != "") {
    $wh .= " AND gr.grower_id  =" . $_REQUEST['filter_size'];
}

if ($_REQUEST['filter_box'] != "") {
    $wh .= " AND ipx.qty_box_packing  =" . $_REQUEST['filter_box'];
}


$from=0;
$display=50;
$page = (int) (!isset($_REQUEST["page_number"]) ? 1 : $_REQUEST["page_number"]);
$page = ($page == 0 ? 1 : $page);
$from = ($page - 1) * $display;

$dates = date("jS F Y");

if ($_REQUEST['filter_date']!="") {
    $variable = $_REQUEST['filter_date'];
    $fech = date("Y-n-j");

    if (strtotime($fech) == strtotime($variable)) {
                
        $dates = date("jS F Y");
        $query2 = "select  gr.id as grid,g.growers_name,gr.bunchqty,gr.steams,
                     (gr.bunchqty*gr.steams) as totstems , 
                     scl.name as client_name,
                     gr.offer_id , br.id_order , bo.qucik_desc , 
                     bo.id as po,
                     br.feature , 
                     f.name as features, ipx.grower_id,gr.price, ipx.size,
                     bo.order_number as embarque , gr.product  ,ipx.box_name , ipx.qty_pack,
                     ipx.id as idbox,
                     gr.request_id , ipx.cliente_id,
                     p.image_path as img_url , s.name as subs
                from grower_offer_reply gr 
               inner join buyer_requests br on gr.offer_id = br.id 
               inner join product p on gr.product = p.name 
               left join subcategory s  on p.subcategoryid   = s.id                              
               inner join buyer_orders bo on br.id_order = bo.id                  
               inner join growers g on gr.grower_id = g.id 
               inner join invoice_packing ip on (bo.id = ip.id_fact and gr.grower_id = ip.grower_id and gr.id = ip.id_grower_offer )
               inner join invoice_packing_box ipx  ON ip.id = ipx.id_order               
                left join sub_client scl on ipx.cliente_id = scl.id
                left join features f ON br.feature = f.id                
               where gr.buyer_id='" . $userSessionID . "'
                 and gr.offer_id >= '6731'
                 and br.id_order = '579'
                 and reject in (0) 
                 and g.id is not NULL $wh group by ipx.id";

        $query2 .= " order by bo.id desc , gr.grower_id , gr.product LIMIT $from,$display";
        
        $result2 = mysqli_query($con, $query2);

    } else {       
         
        $date = new DateTime($variable);
        $dates = $date->format('jS F Y');

        $query2 = "select  gr.id as grid,g.growers_name,gr.bunchqty,gr.steams,
                     (gr.bunchqty*gr.steams) as totstems , 
                     scl.name as client_name,
                     gr.offer_id , br.id_order , bo.qucik_desc , 
                     bo.id as po,
                     br.feature , 
                     f.name as features, ipx.grower_id,gr.price, ipx.size,
                     bo.order_number as embarque , gr.product  ,ipx.box_name , ipx.qty_pack,
                     ipx.id as idbox,
                     gr.request_id, ipx.cliente_id,
                     p.image_path as img_url , s.name as subs
                from grower_offer_reply gr 
               inner join buyer_requests br on gr.offer_id = br.id 
               inner join product p on gr.product = p.name 
               left join subcategory s  on p.subcategoryid   = s.id                              
               inner join buyer_orders bo on br.id_order = bo.id                  
               inner join growers g on gr.grower_id = g.id 
               inner join invoice_packing ip on (bo.id = ip.id_fact and gr.grower_id = ip.grower_id and gr.id = ip.id_grower_offer )
               inner join invoice_packing_box ipx  ON ip.id = ipx.id_order               
                left join sub_client scl on ipx.cliente_id = scl.id
                left join features f ON br.feature = f.id                
               where gr.buyer_id='" . $userSessionID . "'
                 and gr.offer_id >= '6731'
                 and br.id_order = '579'
                 and reject in (0) 
                 and g.id is not NULL $wh group by ipx.id";

        $query2 .= " ";

        $query2 .= " order by bo.id desc ,gr.grower_id , gr.product LIMIT $from,$display";

        $result2 = mysqli_query($con, $query2);
    }


} else {

         
      $query2 = "select  gr.id as grid,g.growers_name,gr.bunchqty,gr.steams,
                     (gr.bunchqty*gr.steams) as totstems , 
                     scl.name as client_name,
                     gr.offer_id , br.id_order , bo.qucik_desc , 
                     bo.id as po,
                     br.feature , 
                     f.name as features, ipx.grower_id,gr.price, ipx.size,
                     bo.order_number as embarque , gr.product  ,ipx.box_name , ipx.qty_pack,
                     ipx.id as idbox,
                     gr.request_id, ipx.cliente_id,
                     p.image_path as img_url , s.name as subs
                from grower_offer_reply gr 
               inner join buyer_requests br on gr.offer_id = br.id 
               inner join product p on gr.product = p.name 
               left join subcategory s  on p.subcategoryid   = s.id               
               inner join buyer_orders bo on br.id_order = bo.id                  
               inner join growers g on gr.grower_id = g.id 
               inner join invoice_packing ip on (bo.id = ip.id_fact and gr.grower_id = ip.grower_id and gr.id = ip.id_grower_offer )
               inner join invoice_packing_box ipx  ON ip.id = ipx.id_order               
                left join sub_client scl on ipx.cliente_id = scl.id
                left join features f ON br.feature = f.id                
               where gr.buyer_id='" . $userSessionID . "'
                 and gr.offer_id >= '6731'
                 and br.id_order = '579'
                 and reject in (0) 
                 and g.id is not NULL $wh group by ipx.id";

    $query2 .= " ";

    $query2 .= " order by bo.id desc,gr.grower_id , gr.product LIMIT $from,$display";

    $result2 = mysqli_query($con, $query2);
}

?>
<!--div class="padding-20">    
<div class="panel-body">                                
<div class="dataReques11t"-->                                    
<table class="table table-hover table-vertical-middle nomargin"> 
                        <thead>
                            <tr>                            
                                <th> </th>
                                <th>SubCategory</th>
                                <th>Ord.</th>                                 
                                <th>Grower</th>  
                                <th>Box</th>  
                                <th>Product</th>
                                <th>Change Qty</th>
                                <th>Client</th>                                                                   
                            </tr>                        
                        </thead>    
<tbody id="list_inventory">
     
<?php

$i = 1;

$tp = mysqli_num_rows($result2);

if ($tp == 0) {

    echo $XX = '<div class="notfound" style="margin-top:30px;">No Item Found !</div>';

} else {

    while ($products = mysqli_fetch_array($result2)) {
       
        $k = explode("/", $products["file_path5"]);

        $data = getimagesize(SITE_URL . "user/logo2/" . $k[1]);

        $logourl = SITE_URL . "user/logo/" . $k[1];

        ?>

        <?php if ($products["bv"] != 2) { ?>


        <?php } else { ?>


        <?php } 

      $boxPacking = $products['box_name'];
      $box_num = "";
      
        if ($box_num != "") {
            $boxPacking= $box_num."box";  
        }
        
        
        ?>

        <tr>

            <!--Grower-->
            <td><img src="https://app.freshlifefloral.com/<?= $products["img_url"]; ?>" width="70"></td>
            <td><?php echo $products["subs"]; ?> </td>                                    
            <td><?php echo $products["po"];  ?>  </td>
            <td><?php echo $products['growers_name']; ?></td>            
            <td><?php echo $boxPacking ?></td>                                                

            <!--Pack-->

                         <?php 
                                  
                               if ($products["type_market"] == "0") {
                                    $market = "Stand";
                                }else{
                                    $market = "Open";
                                }
                             ?>                   
            

            <!--Variety pot-->
            <td> <?php echo  $products['product']." ".$products['size']." cm ".$products['qty_pack']." Bunches ".$products['steams']." st/bu "  ?> </td>
                                 
                                                    <!-- Subclientes -->
                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="change_<?php echo $products["idbox"] ; ?>" id="change_<?php echo $products["idbox"] ; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                               <?php
                                                                                                    $boxDataShow = $products["qty_pack"];
                                                                                                    for ($ii = $boxDataShow; $ii >= 1; $ii--) {
                                                                                                ?>
                                                                                            <option value="<?= $ii ?>"><?= $ii ?></option>
                                                                                            <?php } ?>                                         
                                                                                        </select>                                                                        
                                                                                    </td>                                                                        
                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="client_<?php echo $products["idbox"] ; ?>" id="client_<?php echo $products["idbox"] ; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value=""><?= $products['client_name']; ?></option>    
                                                                                            <?php
                                                                                            $sql_cli = "select id,name from sub_client where id != 1 order by name";
                                                                                            $result_cli = mysqli_query($con, $sql_cli);
                                                                                            while ($row_client = mysqli_fetch_assoc($result_cli)) { ?>
                                                                                                <option value="<?php echo $row_client['id']; ?>"><?= $row_client['name']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>   
                                                                                     <td>
                                                                                        <input type="hidden" class="form-control" name="pro-<?php echo $i ?>" value="<?php echo $products["idbox"] ?>"/>
                                                                                    </td>
                                                                                                                                                    
                      
                
           <!-- </td>-->
        </tr>
        <?php
        $i++;
}

}
?>
        
          </tbody>
          </table>                            
          <!--/div>       
       </div> 
  </div-->              
    

<script type="text/javascript">
    $(document).ready(function () {
        
        
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            //console.log('Selection: ' + suggestion);
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/inventory_search_product.php',
                data: 'name=' + suggestion,
                success: function (data) {
                    $("#pagination_nav").hide();
                    $('.imgDiv').hide();
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();
                }
            });
        });
        

 });
 
</script>
