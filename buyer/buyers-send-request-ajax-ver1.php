<?php

require_once("../config/config_gcp.php");

if ($_SESSION["login"] != 1 && $_SESSION["grower"] == "") {
    header("location:" . SITE_URL);
}

$userSessionID = $_SESSION["buyer"];


if (isset($_POST["offer"]) && $_POST["offer"] != "") {

    $sel_info = "select shipping ,handling_fees , tax
                   from buyers 
                  where id='" . $_POST["buyer"] . "'";
    
    $rs_info = mysqli_query($con,$sel_info);
    $info = mysqli_fetch_array($rs_info);
    
    $shipping_method = trim($info["shipping"], ",");
    $box_info   = explode("-", $_POST["boxtype-" . $_POST["offer"]]);
    $box_volumn = round($box_info[2] / 1728, 2);

    $growers["shipping_method"] = $shipping_method;
    $growers["box_weight"]      = $box_info[3];
    $growers["box_volumn"]      = $box_volumn;

    if ($growers["shipping_method"] > 0) {
        
        $total_shipping = 0;
        $sel_connections = "select type               , shipping_rate , addtional_rate , box_price,
                                   box_weight_arranged, price_per_box 
                              from connections 
                              where shipping_id='" . $growers["shipping_method"] . "'";
        
        $rs_connections = mysqli_query($con,$sel_connections);
        
        while ($connections = mysqli_fetch_array($rs_connections)) {
            
            if ($connections["type"] == 2) {
                $total_shipping = $total_shipping + round(($growers["box_weight"] * $connections["shipping_rate"]), 2);
            } else if ($connections["type"] == 4) {
                $total_shipping = $total_shipping + round(($growers["box_volumn"] * $connections["shipping_rate"]), 2);
            } else if ($connections["type"] == 1) {
                
                $box_type_calc = $box_info[0];
                if ($box_type_calc == 'HB') {
                    $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2;
                    $connections["price_per_box"] = $connections["price_per_box"] / 2;
                }
                if ($box_type_calc == 'JUMBO') {
                    $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2;
                    $connections["price_per_box"]       = $connections["price_per_box"] / 2;
                } else if ($box_type_calc = 'QB') {
                    $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 4;
                    $connections["price_per_box"]       = $connections["price_per_box"] / 4;
                } else if ($box_type_calc = 'EB') {
                    $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 8;
                    $connections["price_per_box"]       = $connections["price_per_box"] / 8;
                }

                $connections["shipping_rate"] = $connections["price_per_box"] / $connections["box_weight_arranged"];


                if ($growers["box_weight"] <= $connections["box_weight_arranged"]) {
                    $total_shipping = $total_shipping + round(( $growers["box_weight"] * $connections["shipping_rate"]), 2);
                } else {
                    $total_shipping = $total_shipping + round(($connections["box_weight_arranged"] * $connections["shipping_rate"]), 2);
                    $remaining = round($growers["box_weight"] - $connections["box_weight_arranged"], 2);
                    $total_shipping = $total_shipping + round(($remaining * $connections["addtional_rate"]), 2);
                }
            } else if ($connections["type"] == 3) {
                $total_shipping = $total_shipping + $connections["box_price"];
            }
        }
    }

    $bsize = explode("Stems", $_POST["bunchsize-" . $_POST["offer"]]);
    $product_shipping = round($total_shipping / ($bsize[0] * $_POST["bunchqty-" . $_POST["offer"]]), 2);


    if ($info["tax"] != 0) {
        $total_taxs = round(($_POST["price-" . $_POST["offer"]] * $info["tax"]) / 100, 2);
    }
    if ($info["handling_fees"] != 0) {
        $total_handling_feess = round(($_POST["price-" . $_POST["offer"]] * $info["handling_fees"]) / 100, 2);
    }

    $total_price = ($_POST["price-" . $_POST["offer"]] + $product_shipping + $total_taxs + $total_handling_feess);

    $boxqty_count=count($_POST['boxqty']);       
        
  if (!isset($_POST["rimol"])) {

            $arrayDesc = explode(",", $_POST["prod_name"]);
            $arrayMed  = explode(",", $_POST["s_name"]);            
            $arrayCaja = explode(",", $_POST["b_type"]);                        
            
    for($i=0;$i<$boxqty_count;$i++) {
                
              $qryMax="select (max(id)+1) as id 
                         from grower_offer_reply";
	      $dataMaximo = mysqli_query($con, $qryMax);
              
	       while ($dt = mysqli_fetch_assoc($dataMaximo)) {
                           $IdMaximo= $dt['id'];
		}
                                                                                                
        $sel_cargo = "select shipping_method_id  , shipping_code , cargo_agency_id  
                        from buyer_shipping_methods 
                       where shipping_method_id='" . $shipping_method . "' 
                         and shipping_code     ='" . $_POST["code_order"] . "' ";
        
        $rs_cargo = mysqli_query($con,$sel_cargo);       
        $cargo = mysqli_fetch_array($rs_cargo);
        
        $sel_agency = "select name 
                         from cargo_agency 
                        where id ='" . $cargo["cargo_agency_id"] . "'";
        
        $rs_agency = mysqli_query($con,$sel_agency);       
        $agency = mysqli_fetch_array($rs_agency);    
                                    
                $insert="insert into grower_offer_reply set 
		           id                  ='" . $IdMaximo . "',                               
                           reject2             = 0,
			   reason2             ='',
			   reason              ='',                               
                           offer_id            ='" . $_POST["offer"] . "',
                           offer_id_index      ='" . $_POST["hdn_offer_id_index"]. "',
                           grower_id           ='" . $_SESSION["grower"] . "',
                           buyer_id            ='" . $_POST["buyer"] . "',
                           boxtype             ='" . $arrayCaja[$i] . "',
                           grower_box_name     ='A',
                           box_weight          = 0,
                           box_volumn          ='" . $box_volumn . "',
                           shipping            = 0,
                           handling            = 0,
                           tax                 = 0,
                           totalprice          = 0,    
                           status              = 0,
                           type                = 0,
                           size                ='" . $arrayMed[$i] . "', 
                           product             ='" . $arrayDesc[$i]. "',
                           price               ='" . $_POST["price"][$i] . "',
                           steams              ='" . $_POST["stems"]."',
                           totalprice_box      = 0,                               
                           bunchsize           ='0',
                           bunchqty            ='" . $_POST["bunchqty"][$i] . "',
                           boxqty              ='" . $_POST["boxcant"] . "',
                           total_stems         = 0,
                           totalstems_box      = 0,
                           total_price_st_bu   = 0,
                           atprice_box         = 0,
                           atqty_box           = 0,
                           finalprice          = 0,
                           billing_add         = '0',
                           coordination        ='B',
                           marks               ='" . $_SESSION['usuario'] . "',
                           cargo               ='" . $agency['name']."',                               
                           shipping_method     = 132,
                           product_subcategory ='" . $_POST["product_subcategory"] . "',
                           date_added          ='" . date("Y-m-d") . "',
                           accept_date         ='" . date("Y-m-d") . "',
                           invoice             = 0 ,
                           req_group           = 0 ,                           
                           unit                ='" . $_POST["boxtype"]      . "' , 
                           cliente_id          ='" . $_POST["id_client"]    . "' ,                               
                           request_id          ='" . $_POST["requestb"][$i] . "' ,
                           type_market         ='" . $_POST["tmarquet"][$i] . "'     ";               
                                                   
                mysqli_query($con,$insert);

            }  
            
        } 
}