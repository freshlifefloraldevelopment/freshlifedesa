<?php

require_once("config/config_new.php");
if ($_SESSION["login"] != 1 && $_SESSION["grower"] == "") {
    header("location:" . SITE_URL);
}

//print_r($_POST);die;

$userSessionID = $_SESSION["grower"];
/* * *******get the data of session user*************** */
$sel_info = "select * from growers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);


if (isset($_POST["offer"]) && $_POST["offer"] != "") {

    $sel_info = "select shipping from buyers where id='" . $_POST["buyer"] . "'";
    $rs_info = mysqli_query($con,$sel_info);
    $info = mysqli_fetch_array($rs_info);
    $shipping_method = trim($info["shipping"], ",");
    $box_info = explode("-", $_POST["boxtype"]);
    $box_volumn = round($box_info[2] / 1728, 2);


    if (!isset($_POST["rimol"])) {
        $activity = "Sent Praposal (" . $text . " ) at " . $hm;
        $insert_login = "insert into activity set grower='" . $_SESSION["grower"] . "',ldate='" . date("Y-m-d") . "',type='grower',activity='" . $activity . "',ltime='" . $hm . "',atype='sp'";
        mysqli_query($con,$insert_login);
    }
    
    $growers["shipping_method"] = $shipping_method;
    $growers["box_weight"] = $box_info[3];
    $growers["box_volumn"] = $box_volumn;

    if ($growers["shipping_method"] > 0) {
        $total_shipping = 0;
        $sel_connections = "select type,shipping_rate,addtional_rate,box_price,box_weight_arranged,price_per_box from connections where shipping_id='" . $growers["shipping_method"] . "'";
        $rs_connections = mysqli_query($con,$sel_connections);
        while ($connections = mysqli_fetch_array($rs_connections)) {
            if ($connections["type"] == 2) {
                $total_shipping = $total_shipping + round(($growers["box_weight"] * $connections["shipping_rate"]), 2);
            } else if ($connections["type"] == 4) {
                $total_shipping = $total_shipping + round(($growers["box_volumn"] * $connections["shipping_rate"]), 2);
            } else if ($connections["type"] == 1) {
                $box_type_calc = $box_info[0];
                if ($box_type_calc == 'HB') {
                    $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2;
                    $connections["price_per_box"] = $connections["price_per_box"] / 2;
                }
                if ($box_type_calc == 'JUMBO') {
                    $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 2;
                    $connections["price_per_box"] = $connections["price_per_box"] / 2;
                } else if ($box_type_calc = 'QB') {
                    $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 4;
                    $connections["price_per_box"] = $connections["price_per_box"] / 4;
                } else if ($box_type_calc = 'EB') {
                    $connections["box_weight_arranged"] = $connections["box_weight_arranged"] / 8;
                    $connections["price_per_box"] = $connections["price_per_box"] / 8;
                }

                $connections["shipping_rate"] = $connections["price_per_box"] / $connections["box_weight_arranged"];


                if ($growers["box_weight"] <= $connections["box_weight_arranged"]) {
                    $total_shipping = $total_shipping + round(( $growers["box_weight"] * $connections["shipping_rate"]), 2);
                } else {
                    $total_shipping = $total_shipping + round(($connections["box_weight_arranged"] * $connections["shipping_rate"]), 2);
                    $remaining = round($growers["box_weight"] - $connections["box_weight_arranged"], 2);
                    $total_shipping = $total_shipping + round(($remaining * $connections["addtional_rate"]), 2);
                }
            } else if ($connections["type"] == 3) {
                $total_shipping = $total_shipping + $connections["box_price"];
            }
        }
    }


    $bsize = explode("Stems", $_POST["bunchsize-" . $_POST["offer"]]);
    $product_shipping = round($total_shipping / ($bsize[0] * $_POST["bunchqty-" . $_POST["offer"]]), 2);

    $sel_info = "select * from buyers where id='" . $_POST["buyer"] . "'";
    $rs_info = mysqli_query($con,$sel_info);
    $info = mysqli_fetch_array($rs_info);

    if ($info["tax"] != 0) {
        $total_taxs = round(($_POST["price-" . $_POST["offer"]] * $info["tax"]) / 100, 2);
    }
    if ($info["handling_fees"] != 0) {
        $total_handling_feess = round(($_POST["price-" . $_POST["offer"]] * $info["handling_fees"]) / 100, 2);
    }

    $total_price = ($_POST["price-" . $_POST["offer"]] + $product_shipping + $total_taxs + $total_handling_feess);

    $allowpost = 1;


    $boxqty_count=count($_POST['boxqty']);


    if ($allowpost == 1) {

        if (!isset($_POST["rimol"])) {

                $insert = "insert into grower_offer_reply set 
                           offer_id='" . $_POST["offer"] . "',
                           grower_id='" . $_SESSION["grower"] . "',
                           buyer_id='" . $_POST["buyer"] . "',
                           boxtype='" . $box_info[0] . "',
                           grower_box_name='" . $box_info[1] . "',
                           box_weight='" . $box_info[3] . "',
                           box_volumn='" . $box_volumn . "',
                           shipping='" . $product_shipping . "',
                           handling='" . $total_handling_feess . "',
                           tax='" . $total_taxs . "',
                           totalprice='" . $total_price . "',    
                           status=0,
                           type=0,
                           size='" . $_POST["sizename"]. "',
                           product='" . $_POST["product"] . "',
                           price='" . $_POST["price"] . "',
                           boxqty='" . $_POST["qty"] . "',
                           shipping_method='" . $shipping_method . "',
                           product_subcategory='" . $_POST["product_subcategory"] . "',
                           date_added='" . date("Y-m-d") . "'";          
                mysqli_query($con,$insert);

                $last = mysqli_insert_id($con);        
            
        } else {
            $last = $_POST["rimol"];
        }
        $update_state = "update buyer_requests set unseen=1 where id='" . $_POST["offer"] . "'";
        mysqli_query($con,$update_state);

        if ($_POST["addano"] == 0) {

            $message = 1;
        }

        if ($_POST["addanomo"] == 1) {

            $sizeo = explode("cm", $_POST["productsize-" . $_POST["offer"]]);
            $pro = $_POST["product"] . " " . $sizeo[0] . " cm";
            $minsert = "insert into grower_reply_box set reply_id='" . $last . "',product='" . $pro . "',price='" . $_POST["price-" . $_POST["offer"]] . "',bunchsize='" . $_POST["bunchsize-" . $_POST["offer"]] . "',bunchqty='" . $_POST["bunchqty-" . $_POST["offer"]] . "',offer_id='" . $_POST["offer"] . "',boxtype='" . $box_info[0] . "',boxqty='" . $_POST["boxqty-" . $_POST["offer"]] . "',boxname='" . $last . "'";

            mysqli_query($con,$minsert);

            $up = "update grower_offer_reply set type=2 ,product='',size='',price='',bunchsize='',bunchqty='' where id='" . $last . "'";
            mysqli_query($con,$up);

            $rim = 1;
        }


        $sel_mail_email = "select email,first_name from buyers where id='" . $_POST["buyer"] . "'";
        $rs_mail_email = mysqli_query($con,$sel_mail_email);
        $mail_email = mysqli_fetch_array($rs_mail_email);
        $email_address = $mail_email["email"];


        $er = "select gpb.id as cartid,gpb.type,gpb.lfd,gpb.noofstems,gpb.qty,gpb.boxtype,p.id,p.name,s.name as subs,sh.name as sizename,ff.name as featurename from buyer_requests gpb
                     left join product p on gpb.product = p.id
                     left join subcategory s on p.subcategoryid=s.id  
                     left join features ff on gpb.feature=ff.id
                     left join sizes sh on gpb.sizeid=sh.id where gpb.id='" . $_POST["offer"] . "' ";
        $ers = mysqli_query($con,$er);

        while ($er5 = mysqli_fetch_array($ers)) {

            // $text.="You have a new  proposal for your offer at freshlifefloral.com , Your offer deails are as follow <br/> <br/>"; 

            if ($er5["type"] != "3") {

                if ($er5["type"] == 2) {
                    $text.="Name Your Price - &nbsp;&nbsp;";
                }

                if ($er5["type"] == 0) {
                    $text.="Request for Product Quote - &nbsp;&nbsp;";
                }


                $text.="For ";
                $temp_lfd = explode("-", $er5["lfd"]);
                $text.=$temp_lfd[1] . "-" . $temp_lfd[2] . "-" . $temp_lfd[0];
                $text.=" - " . $er5["qty"] . " ";
                $temp = explode("-", $er5["boxtype"]);
                $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                $rs_box_type = mysqli_query($con,$sel_box_type);
                $box_type = mysqli_fetch_array($rs_box_type);
                $text.= $box_type["name"] . " Box(es)";
                $text.="<br/>";

                $text.=$er5["subs"] . " - " . $er5["name"] . " - ";
                if ($er5["featurename"] != "") {
                    $text.=$er5["featurename"] . " - ";
                }

                $text.=$er5["sizename"] . " cm ";

                $text.="For ";
                $temp_lfd = explode("-", $er5["lfd"]);
                $text.=$temp_lfd[1] . "-" . $temp_lfd[2] . "-" . $temp_lfd[0];

                if ($er5["noofstems"] > 0) {
                    $text.=" - " . $er5["noofstems"] . " Stems";
                } else {
                    $text.=" - " . $er5["qty"] . " ";

                    $temp = explode("-", $er5["boxtype"]);
                    $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                    $rs_box_type = mysqli_query($con,$sel_box_type);
                    $box_type = mysqli_fetch_array($rs_box_type);
                    $text.= $box_type["name"] . " Box(es)";
                }
            } else {
                $text.=" Assorted Box";
            }

            $text.="<br/>";
        }

        // $to = $email_address;
        // $to = $to.",logging@freshlifefloral.com,dan@freshlifefloral.com";
        // $subject = "You have a new  proposal for your offer at freshlifefloral.com ";
        // $body .= "Hello <b>".$mail_email["first_name"]."</b><br/><br/>";
        // $body .= $text;
        // $body .="<br/><br/>Please click on following link to login to view proposal.<br/>";
        // $body .="<a href='http://staging.freshlifefloral.com/'>http://staging.freshlifefloral.com/</a><br/><br/>";
        // $body .="Thanks!";
        // $body .="<br/><b>Freshlife Floral Team<b>";
        // $headers = "MIME-Version: 1.0" . "\r\n";
        // $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        // $headers .= "From:info@freshlifefloral.com". "\r\n";
        // mail($to,$subject,$body,$headers); 

        if ($_POST["addano"] == 0) {

            $insert_email = "insert into buyer_emails set buyer='" . $_POST["buyer"] . "',mbody='" . $text . "',eto='" . $email_address . "',fname='" . $mail_email["first_name"] . "'";
            mysqli_query($con,$insert_email);
        }
    }
}




if (isset($_POST["delete"])) {
    $delete = "delete from grower_reply_box where id='" . $_POST["delete"] . "'";
    mysqli_query($con,$delete);
}

if (isset($_POST["moffer"]) && $_POST["moffer"] != "") {
    if ($_POST["msave"] != 1) {
        $sel_info = "select shipping from buyers where id='" . $_POST["buyer"] . "'";
        $rs_info = mysqli_query($con,$sel_info);
        $info = mysqli_fetch_array($rs_info);
        $shipping_method = trim($info["shipping"], ",");
        $_POST["mboxtype-" . $_POST["moffer"]];
        $box_info = explode("-", $_POST["mboxtype-" . $_POST["moffer"]]);
        $box_volumn = round($box_info[2] / 1728, 2);
        $insert = "insert into grower_offer_reply set 
       offer_id='" . $_POST["moffer"] . "',
       grower_id='" . $_SESSION["grower"] . "',
       buyer_id='" . $_POST["buyer"] . "',
       boxtype='" . $box_info[1] . "',
       grower_box_name='" . $box_info[0] . "',
       box_weight='" . $box_info[3] . "',
       box_volumn='" . $box_volumn . "',
       shipping_method='" . $shipping_method . "',
       boxqty='" . $_POST["mboxqty-" . $_POST["moffer"]] . "',
       status=0,
       type=2,
       date_added='" . date("Y-m-d") . "'";
        mysqli_query($con,$insert);
        $last = mysqli_insert_id($con);

        $update_state = "update buyer_requests set unseen=1 where id='" . $_POST["moffer"] . "'";
        mysqli_query($con,$update_state);

        $total_stems = 0;

        $_POST["mboxqty-" . $_POST["moffer"]] = 1;

        for ($i = 1; $i <= $_POST["count-" . $_POST["moffer"]]; $i++) {

            if ($_POST["mproduct-" . $i . $_POST["moffer"]] != "" && $_POST["mprice-" . $i . $_POST["moffer"]] != "" && $_POST["mbunchsize-" . $i . $_POST["moffer"]] != "" && $_POST["mbunchqty-" . $i . $_POST["moffer"]] != "") {

                $temp[0] = $_POST["mproduct-" . $i . $_POST["moffer"]] . " " . $_POST["msize-" . $i . $_POST["moffer"]] . " cm";

                $minsert = "insert into grower_reply_box set reply_id='" . $last . "',product='" . $temp[0] . "',price='" . $_POST["mprice-" . $i . $_POST["moffer"]] . "',bunchsize='" . $_POST["mbunchsize-" . $i . $_POST["moffer"]] . "',bunchqty='" . $_POST["mbunchqty-" . $i . $_POST["moffer"]] . "',offer_id='" . $_POST["moffer"] . "',boxname='" . $_POST["mboxname-" . $_POST["moffer"]] . "-" . date("Y-m-d h:i:s") . "',boxtype='" . $box_info[1] . "',boxqty='" . $_POST["mboxqty-" . $_POST["moffer"]] . "'";

                $temp_box_stems = explode("Stems", $_POST["mbunchsize-" . $i . $_POST["moffer"]]);
                $total_stems = $total_stems + ($_POST["mbunchqty-" . $i . $_POST["moffer"]] * $temp_box_stems[0]);
                mysqli_query($con,$minsert);
            }
        }

        $update = "update grower_reply_box set reply_id='" . $last . "' where reply_id=0 and offer_id='" . $_POST["moffer"] . "'";
        mysqli_query($con,$update);
        $message = 1;
    }

    $update = "update grower_offer_reply set total_stems='" . $total_stems . "' where offer_id='" . $_POST["moffer"] . "'";
    mysqli_query($con,$update);

    $sel_mail_email = "select email,first_name from buyers where id='" . $_POST["buyer"] . "'";
    $rs_mail_email = mysqli_query($con,$sel_mail_email);
    $mail_email = mysqli_fetch_array($rs_mail_email);
    $email_address = $mail_email["email"];


    $er = "select gpb.id as cartid,gpb.type,gpb.lfd,gpb.bunches,gpb.noofstems,gpb.qty,gpb.boxtype,p.id,p.name,s.name as subs,sh.name as sizename,ff.name as featurename from buyer_requests gpb
                     left join product p on gpb.product = p.id
                     left join subcategory s on p.subcategoryid=s.id  
                     left join features ff on gpb.feature=ff.id
                     left join sizes sh on gpb.sizeid=sh.id where gpb.id='" . $_POST["moffer"] . "' ";
    $ers = mysqli_query($con,$er);

    while ($er5 = mysqli_fetch_array($ers)) {

        //  $text.="You have a new  proposal for your offer at freshlifefloral.com , Your offer deails are as follow <br/> <br/>"; 
        $text.="By the Bunch - &nbsp;&nbsp;";

        $text.="For ";
        $temp_lfd = explode("-", $er5["lfd"]);
        $text.=$temp_lfd[1] . "-" . $temp_lfd[2] . "-" . $temp_lfd[0];
        $text.=" - " . $er5["qty"] . " ";

        $temp = explode("-", $er5["boxtype"]);
        $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
        $rs_box_type = mysqli_query($con,$sel_box_type);
        $box_type = mysqli_fetch_array($rs_box_type);
        $text.= $box_type["name"] . " Box(es)";

        $text.="<br/>";
        $text.="Selected Bunch(es)<br/><br/>";



        $templ = explode(",", $er5["bunches"]);
        $count = sizeof($templ);
        for ($io = 0; $io <= $count - 2; $io++) {
            $r = explode(":", $templ[$io]);
            $query2 = "select gpb.growerid,gpb.prodcutid,gpb.sizeid,gpb.feature ,p.name as productname,s.name as subs,sh.name as sizename,ff.name as featurename,bs.name as bname from grower_product_box_packing gpb
              left join product p on gpb.prodcutid = p.id
               left join subcategory s on p.subcategoryid=s.id  
              left join features ff on gpb.feature=ff.id
              left join sizes sh on gpb.sizeid=sh.id 
              left join bunch_sizes bs on gpb.bunch_size_id=bs.id
              where gpb.id='" . $r[0] . "'";
            $rs2 = mysqli_query($con,$query2);
            while ($product_box = mysqli_fetch_array($rs2)) {

                $text.='&nbsp;' . $product_box["subs"] . '&nbsp;&nbsp;  
              ' . $product_box["productname"] . ' ' . $product_box["featurename"] . ' &nbsp;&nbsp; - ' . $product_box["sizename"] . 'cm &nbsp;&nbsp; - ' . $product_box["bname"] . ' Stems &nbsp;&nbsp;-' . $r[1] . ' Bunch(es) &nbsp;
                      <br/>';
            }
        }

        $text.='<br/>';
        $text.="<br/><br/>";
    }

    /* $to = $email_address;
      $subject = "You have a new  proposal for your offer at freshlifefloral.com ";

      $body .= "Hello <b>".$mail_email["first_name"]."</b><br/><br/>";

      $body .= $text;

      $body .="<br/><br/>Please click on following link to login to view proposal.<br/>";

      $body .="<a href='http://staging.freshlifefloral.com/'>http://staging.freshlifefloral.com/</a><br/><br/>";

      $body .="Thanks!";

      $body .="<br/><b>Freshlife Floral Team<b>";

      $body ;

      $headers = "MIME-Version: 1.0" . "\r\n";
      $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
      $headers .= "From:info@freshlifefloral.com". "\r\n";
      mail($to,$subject,$body,$headers); */


    $insert_email = "insert into buyer_emails set buyer='" . $_POST["buyer"] . "',mbody='" . $text . "',eto='" . $email_address . "',fname='" . $mail_email["first_name"] . "'";
    mysqli_query($con,$insert_email);
}
