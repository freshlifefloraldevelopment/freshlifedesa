<?php

require_once("../config/config_gcp.php");

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}

$userSessionID = $_SESSION["buyer"];

/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';

if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}

            $sel_info = "select * from buyers where id='" . $userSessionID . "'";
            $rs_info = mysqli_query($con, $sel_info);
            $info = mysqli_fetch_array($rs_info);
        $page_request = "buyer_invoices";
?>

<!DOCTYPE html>
<head>
    
    <style>
            .StripeElement {
              box-sizing: border-box;

              height: 40px;

              padding: 10px 12px;

              border: 1px solid transparent;
              border-radius: 4px;
              background-color: white;

              box-shadow: 0 1px 3px 0 #e6ebf1;
              -webkit-transition: box-shadow 150ms ease;
              transition: box-shadow 150ms ease;
            }

            .StripeElement--focus {
              box-shadow: 0 1px 3px 0 #cfd7df;
            }

            .StripeElement--invalid {
              border-color: #fa755a;
            }

            .StripeElement--webkit-autofill {
              background-color: #fefde5 !important;}                
    </style>        
        
    <script src="https://js.stripe.com/v3/"></script>
</head>    
    

<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php require_once "../includes/left_sidebar_buyer.php"; ?>



<section id="middle">

    <header id="page-header">
        <h1>Credit or debit card</h1>
        <ol class="breadcrumb">
            <li><a href="#">Choose</a></li>
            <li class="active">Payment</li>
        </ol>
    </header>


    <div id="content" class="padding-20">


        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>Enter Card Number</strong> <!-- panel title -->
                </span>

            </div>

            <!-- panel content -->
            <div class="panel-body">

                            <form action="CreateCharge.php" method="post" id="payment-form">
                              <div class="form-row">
                                <label for="card-element">
                                  Credit or debit card
                                </label>
                                <div id="card-element">
                                  <!-- A Stripe Element will be inserted here. -->
                                </div>

                                <!-- Used to display form errors. -->
                                <div id="card-errors" role="alert"></div>
                              </div>

                              <button><font color="red">Payment</font></button>
                            </form>
                
                
                
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>
<?php require_once '../includes/footer_new.php'; ?>

<script type="text/javascript">
        // Create a Stripe client.
        var stripe = Stripe('pk_test_sR6sgRJBK7flsLGf6evuDkPR');

        // Create an instance of Elements.
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
          base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
              color: '#aab7c4'
            }
          },
          invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
          }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');
        // Handle real-time validation errors from the card Element.
        card.on('change', function(event) {
          var displayError = document.getElementById('card-errors');
          if (event.error) {
            displayError.textContent = event.error.message;
          } else {
            displayError.textContent = '';
          }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
          event.preventDefault();

          stripe.createToken(card).then(function(result) {
            if (result.error) {
              // Inform the user if there was an error.
              var errorElement = document.getElementById('card-errors');
              errorElement.textContent = result.error.message;
            } else {
              // Send the token to your server.
              stripeTokenHandler(result.token);
            }
          });
        });

        // Submit the form with the token ID.
        function stripeTokenHandler(token) {
          // Insert the token ID into the form so it gets submitted to the server
          var form = document.getElementById('payment-form');
          var hiddenInput = document.createElement('input');
          hiddenInput.setAttribute('type', 'hidden');
          hiddenInput.setAttribute('name', 'stripeToken');
          hiddenInput.setAttribute('value', token.id);
          form.appendChild(hiddenInput);

          // Submit the form
          form.submit();
        }    
</script>    
