<?php

$menuoff = 1;
$page_id = 421;
$message = 0;

require_once("../config/config_gcp.php");


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];

/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}






?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

<section id="middle">


    <!-- page title -->
    <header id="page-header">
        <h1>Quickbooks</h1>
        <ol class="breadcrumb">
            <li><a href="#">Buyer</a></li>
            <li class="active">invoice</li>
        </ol>
    </header>
    <!-- /page title -->


    
    
    
    <div id="content" class="padding-20">


        <div id="panel-2" class="panel panel-default">
            
            <div class="panel-heading">
		<form action=" <?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
			<button type="submit" id="export_data" name='export_data' value="Export to excel" class="btn btn-info">Export to Excel</button>
		</form>
            </div>

            <!-- panel content -->
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                            <tr>
                                <th>*InvoiceNo</th>
                                <th>*Customer</th>
                                <th>*InvoiceDate</th>
                                <th>*DueDate</th>
                            </tr>
                        </thead>
                        
                        
                        <tbody>
                            <?php
                            
                          
                            $sql = 'select (irs.id_fact + 4000) as inv  ,
                                           cl.name as name_client ,
                                           date_format(date_added, "%d-%m-%Y") as date  ,
                                           date_format(ADDDATE(date_added, INTERVAL 30 DAY),"%d-%m-%Y") as due_date  ,
                                           "Net 30" as Terms,
                                           "" as Loc,
                                           c.name as Memo , 
                                           s.name as Item ,
                                           irs.prod_name as description ,
                                          (irs.qty_pack*irs.steams) as qty     ,  
                                           round(irs.price_cad,2) as rate ,
                                           round((irs.qty_pack*irs.steams*irs.price_cad),2) as amount  ,
                                           "GST" as code,
                                            round((irs.qty_pack*irs.steams*irs.price_cad*0.05),2) as taxamount,
                                           "CAD" as Curr 
                                      from invoice_requests_subcli irs
                                     INNER JOIN sub_client cl ON cl.id = irs.cliente_id
                                     INNER JOIN product p ON irs.product = p.id
                                     INNER JOIN category c ON p.categoryid = c.id
                                     INNER JOIN subcategory s ON p.subcategoryid = s.id and p.categoryid = s.cat_id
                                     where irs.buyer      = 318
                                       and irs.id_fact    = 2 
                                       and irs.cliente_id = 56 ';
                            
			    $resultado = mysqli_query($con, $sql);
                                   $libros = array();                       
				while( $rows = mysqli_fetch_assoc($resultado) ) {
                                        $libros[] = $rows;
                                }

                                     foreach($libros as $libro) { 
				?>
                                    <tr>
                                        <td><?php echo $libro ['inv']; ?></td>
                                        <td><?php echo $libro ['name_client']; ?></td>
                                        <td><?php echo $libro ['due_date']; ?></td>
                                        <td><?php echo $libro ['due_date']; ?></td>
                                    </tr>
				<?php
                                    }
                                    
if(isset($_POST["export_data"])) {
	
	if(!empty($libros)) {
header('Pragma: public'); 
header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past    
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); 
header('Cache-Control: no-store, no-cache, must-revalidate'); // HTTP/1.1 
header('Cache-Control: pre-check=0, post-check=0, max-age=0'); // HTTP/1.1 
header('Pragma: no-cache'); 
header('Expires: 0'); 
header('Content-Transfer-Encoding: none'); 
header('Content-Type: application/vnd.ms-excel'); // This should work for IE & Opera 
header('Content-type: application/x-msexcel'); // This should work for the rest 
header('Content-Disposition: attachment; filename="nombre.xls"');


			$mostrar_columnas = false; 
			
			foreach($libros as $libro) {
	
				if(!$mostrar_columnas) {
						echo implode("\t", array_keys($libro)) . "\n";
						$mostrar_columnas = true;
				}
			
				echo implode("\t", array_values($libro)) . "\n";
			}

	}else{
			echo 'No hay datos a exportar';
        }
exit;
}
                                    
                                    
                            ?> 
                        </tbody>
                    </table>
                </div>
                
                
                
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
    
    
</section>
<?php require_once '../includes/footer_new.php'; ?>
