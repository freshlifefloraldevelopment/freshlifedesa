<?php
    require "../config/config_gcp.php";
    include '../buyer/plantilla.php';
  
 
    $userSessionID = $_SESSION["buyer"];
    $idfac         = $_GET['idi'];
    $cliente_inv   = $_GET['id_cli'];
    
    $cajastot = 0;


    // Datos del Buyer
    $buyerEntity = "select id,name
                      from sub_client  
                     where id = '" . $cliente_inv . "'     " ;
     
    $buyer = mysqli_query($con, $buyerEntity);
    $buy = mysqli_fetch_array($buyer);
    

    
    // Datos de la Orden
   $buyerOrder = "select ios.id            , ios.id_fact          , ios.buyer_id    , ios.id_client       , ios.order_number  , 
                         ios.order_date    , ios.shipping_method  , ios.date_order  , ios.date_range      , ios.is_pending    , 
                         ios.order_serial  , ios.description      , ios.bill_number , ios.gross_weight    , ios.volume_weight , 
                         ios.freight_value , ios.sub_total_amount , ios.tax_rate    , ios.shipping_charge , ios.handling      , 
                         ios.grand_total   , ios.bill_state       , ios.date_added  , ios.user_added      , ios.tot_reg       ,
                         ios.tot_bunches   , cl.id                , cl.name as name_client 
                    from invoice_orders_subcli ios  
                   INNER JOIN sub_client cl ON cl.id = ios.id_client
                   where ios.buyer_id  = '" . $userSessionID . "'
                     and ios.id_fact   = '" . $idfac . "' 
                     and ios.id_client = '" . $cliente_inv . "' " ;

   $orderCab = mysqli_query($con, $buyerOrder);
   $buyerOrderCab = mysqli_fetch_array($orderCab); 
   
   $id_fact_cab = $buyerOrderCab['id_fact'];
   $buyer_cab   = $buyerOrderCab['buyer_id'];   
   
   
   // Datos del Requests
   
   $sqlDetalis="select irs.id           , irs.id_fact       , irs.id_order        , irs.order_number, irs.order_serial , 
                        irs.offer_id     , irs.product      , irs.prod_name       , irs.buyer       , irs.grower_id    ,
                        irs.box_qty_pack , irs.qty_pack     , irs.qty_box_packing , irs.box_type    , irs.comment      , 
                        irs.date_added   , irs.box_name     , irs.size, steams    , irs.price       , 
                        irs.cliente_id   , irs.ship_cost    , irs.cost_cad        , irs.price_cad   , irs.gorPrice     , 
                        irs.duties       , irs.handling_pro , irs.total_duties    ,
                        cl.id as idcli   , cl.name as name_client ,cl.buyer       , irs.price_quick , irs.branch       , 
                        bra.name as branch_name,
                        s.name as psubcatego   , p.color_id , co.name as color_name ,
                        irs.product_subcategory , g.growers_name, br.id_order ,
                        irs.box_name as feature_name 
                  from invoice_requests_subcli irs
                 INNER JOIN sub_client cl ON cl.id = irs.cliente_id
                 INNER JOIN product p on irs.product = p.id 
                 INNER JOIN growers g on irs.grower_id = g.id 
                  left join subcategory s on p.subcategoryid = s.id   
                  left join colors co on p.color_id = co.id               
                  left join sub_client_branch bra ON (bra.id = irs.branch )
                  left JOIN buyer_requests br ON irs.comment = br.id
                  left JOIN features ft ON br.feature = ft.id
                 where irs.buyer      = '" . $userSessionID . "'
                   and irs.id_fact    = '" . $idfac . "' 

                 order by bra.name desc , cl.name , s.name , irs.prod_name";

        $result   = mysqli_query($con, $sqlDetalis); 
        
                            // and irs.cliente_id not in (123,166)        
         //                    and irs.cliente_id in (145)        
         //  left join sub_client_branch bra ON (bra.id = irs.branch and bra.id_client = irs.cliente_id)
        
   $sqlTot="select s.name as psubcategotot   , sum(qty_pack) Totalcate
                  from invoice_requests_subcli irs
                 INNER JOIN sub_client cl ON cl.id = irs.cliente_id
                 INNER JOIN product p on irs.product = p.id 
                 INNER JOIN growers g on irs.grower_id = g.id 
                  left join subcategory s on p.subcategoryid = s.id   
                  left join colors co on p.color_id = co.id               
                  left join sub_client_branch bra ON (bra.id = irs.branch )
                  left JOIN buyer_requests br ON irs.comment = br.id
                  left JOIN features ft ON br.feature = ft.id
                 where irs.buyer      = '" . $userSessionID . "'
                   and irs.id_fact    = '" . $idfac . "' 
                 group by s.name";

        $resultTot   = mysqli_query($con, $sqlTot);                                 
        

    $pdf = new PDF();
    //$pdf->AliasPages();   
    
    //$pdf->AddPage();
    
   // $pdf->Image('logo.png',1,5,22); 
   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $tmp_idorder = 0;    
    $tmp_cli = 0;
    
    $sw = 0;
    $bb = 0;
        $numero = 0;
    
    
    while($row = mysqli_fetch_assoc($result))  {    
       
       
        if ($row['idcli'] != $tmp_cli) {  

       $sqlcoment = "select description
                       from invoice_orders_subcli
                      where buyer_id  = '" . $userSessionID . "'
                        and id_fact   = '" . $idfac . "' 
                        and id_client = '" . $row['idcli'] . "' " ;

                $rs_coment = mysqli_query($con, $sqlcoment);
                $comentario = mysqli_fetch_array($rs_coment);        
    ////////////////////////////////////////////////////
             
       $bOrder = "select ios.tot_bunches
                    from invoice_orders_subcli ios  
                   INNER JOIN sub_client cl ON cl.id = ios.id_client
                   where ios.buyer_id  = '" . $userSessionID . "'
                     and ios.id_fact   = '" . $idfac . "' 
                     and ios.id_client = '" . $row['idcli'] . "' " ;

   $cabBunch = mysqli_query($con, $bOrder);
   $totalBunch = mysqli_fetch_array($cabBunch); 
   
////////////////////////////////////////////////////////////////////////////////////////
   $sqlToclit="select s.name as psubcategotot   , sum(qty_pack) Totalcate
                  from invoice_requests_subcli irs
                 INNER JOIN sub_client cl ON cl.id = irs.cliente_id
                 INNER JOIN product p on irs.product = p.id 
                 INNER JOIN growers g on irs.grower_id = g.id 
                  left join subcategory s on p.subcategoryid = s.id   
                  left join colors co on p.color_id = co.id               
                  left join sub_client_branch bra ON (bra.id = irs.branch )
                  left JOIN buyer_requests br ON irs.comment = br.id
                  left JOIN features ft ON br.feature = ft.id
                 where irs.buyer      = '" . $userSessionID . "'
                   and irs.id_fact    = '" . $idfac . "' 
                   and irs.cliente_id = '" . $row['idcli'] . "'   
                 group by s.name";

        $resultTotcli   = mysqli_query($con, $sqlToclit);            
   
////////////////////////////////////////////////////////////////////////////////////////
                         
                 $pdf->AddPage();
                 
    $pdf->SetFont('Arial','B',40);    
    $pdf->Cell(70,10,$row['name_client'],0,0,'L'); 
    
    $pdf->Ln(10);    
    
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(70,10,'Client Details ',0,0,'L');
            $pdf->Cell(0,10,'Shipping Details ',0,0,'R');
            $pdf->Ln(10);
            
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(70,6,'Name: '.$row['name_client'],0,0,'L');
    $pdf->Cell(0,6,'Invoice #: '.$buyerOrderCab['order_number'],0,1,'R');             
    
    $pdf->Cell(70,6,'Country: CANADA',0,1,'L');        
       
    $pdf->Cell(70,6,'Delivery Date : '.$buyerOrderCab['date_added'],0,1,'L');  
    
    $pdf->Cell(100,6,'Comment : '.$comentario['description'],0,1,'L');              
    
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
     
    $cliente_bunches = 0;
     
    while($rowTotcli = mysqli_fetch_assoc($resultTotcli))  {    
        $pdf->Cell(45,6,$rowTotcli['psubcategotot'],0,0,'L');     
        $pdf->Cell(25,6,$rowTotcli['Totalcate'],0,1,'R');  
        
          $cliente_bunches = $cliente_bunches + $rowTotcli['Totalcate']; 
    }              
    $pdf->Cell(0,6,'Total Bunches: '.$cliente_bunches,0,1,'R');  
    
    $pdf->Ln(5);
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');          
    $pdf->Cell(99,6,'Variety',0,0,'L');
    $pdf->Cell(25,6,'Qty',0,0,'C');
    $pdf->Cell(35,6,'Type',0,0,'L'); 
    $pdf->Cell(25,6,'Grower',0,1,'L'); 
    $pdf->Cell(70,6,'_______________________________________________________________________________________________',0,1,'L');  
    $pdf->SetFont('Arial','',10);
    
      $subStemsGrower = 0;

      
//    $tmp_idorder = 0;    
//    while($row = mysqli_fetch_assoc($result))  {         
//         if ($row['idcli'] != $tmp_cli) {  
             
             //// aqui ///
             
 //            $pdf->Ln(10);  
 //            $pdf->AddPage();
         }
       
         // Verificacion Stems/Bunch
        
        $sel_bu_st = "select box_type , subcategoryid from product where name = '" . $row['prod_name'] . "' "; 
        
        $rs_bu_st = mysqli_query($con,$sel_bu_st);       
        $bunch_stem = mysqli_fetch_array($rs_bu_st);
            
              if ($bunch_stem['box_type'] == 0) {
                    $Subtotal= $row['qty_pack'] * $row['price_cad'];
                    $qtyFac  = $row['qty_pack'] *$row['steams'];
                    $unitFac = "STEMS";                     
              }else{
                    $Subtotal= $row['qty_pack'] * $row['price_cad'];
                    $qtyFac  = $row['qty_pack']; 
                    $unitFac = "BUNCHES";                   
              }        
        
        $sel_subcatego = "select name from subcategory where id = '" . $bunch_stem['subcategoryid'] . "' ";
        $rs_subcatego = mysqli_query($con,$sel_subcatego);       
        $subc = mysqli_fetch_array($rs_subcatego);
        
        $numero=$numero+1;
        
        if ($numero%2==0){
                          $pdf->SetFont('Arial','B',9);
        }else{
                          $pdf->SetFont('Arial','',9);
        }
                 
  
         $variety = $row['product_subcategory']." ".$row['prod_name']." ".$row['size']." cm ".$row['steams']." st/bu ".$row['color_name']." ".$row['feature_name'];
         
         $variety = strtoupper($variety);
         
         $pdf->Cell(99,4,$variety,0,0,'L');                     
         
         
        // if ($row['prod_name'] == "Hot Explorer" ||$row['prod_name'] == "Red Spray" ||$row['prod_name'] == "Pink Mondial"||$row['prod_name'] == "Light Blue") {
        //         $pdf->Cell(99,4,$variety,0,0,'L');                     
        // }else{
        //         $pdf->Cell(99,4,$row['product_subcategory']." ".$row['prod_name']." ".$row['size']." cm ".$row['steams']." st/bu ".$row['color_name']." ".$row['feature_name'],0,0,'L');                                  
        // }
                               


if ($bunch_stem['box_type'] == 0) {         
         //$subTotalGrower= $subTotalGrower + ($row['steams']*$row['bunchqty']*$row['salesPriceCli']) ; 
         $subTotalGrower= $subTotalGrower + ($row['qty_pack']*$row['price_cad']) ;             
         $pdf->Cell(25,6,$row['qty_pack'],0,0,'C');
}else{
         $subTotalGrower= $subTotalGrower + ($row['qty_pack']*$row['price_cad']) ;             
         $pdf->Cell(25,6,$row['qty_pack'],0,0,'C');                                           
}         

         $pdf->Cell(35,6,substr($row['comment'],10,10),0,0,'L');              
         $pdf->Cell(25,6,$row['growers_name'],0,1,'L');     
                  

            $tmp_cli = $row['idcli'];         

            $totalCal       = $totalCal + $Subtotal;
            $totalStems     = $totalStems + $subtotalStems;            
            $tmp_idorder    = $row['grower_id'];
            $subStemsGrower = $subStemsGrower + ($row['qty_pack']) ;             
    }
                      
    
            $pdf->Ln(10);
            
            //$pdf->SetFont('Arial','B',15);
            //$pdf->Cell(70,10,'Contact Details ',0,1,'L');                        
            //$pdf->SetFont('Arial','B',10);            
            //$pdf->Cell(70,6,'Address',0,0,'L');    
            //$pdf->Cell(0,6,'Sub - Total Amount: $'.number_format($totalCal, 2, '.', ','),0,1,'R');                                
            //$pdf->Cell(70,6,'Vancouver, Canada',0,1,'L');               
            //$pdf->Cell(70,6,'Phone:',0,1,'L'); 
            //$pdf->Cell(70,6,'Email:',0,1,'L');
            // $pdf->Cell(0,6,'Total: $'.number_format($buyerOrderCab['grand_total'], 2, '.', ','),0,1,'R');    
            
$pdf->SetFont('Arial','B',10);              
            
  //           $pdf->Cell(25,6,'TOTALES',0,1,'L');   
            
   /* while($rowTot = mysqli_fetch_assoc($resultTot))  {    
        $pdf->Cell(45,6,$rowTot['psubcategotot'],0,0,'L');     
        $pdf->Cell(25,6,$rowTot['Totalcate'],0,1,'R');     
    }*/            
    
  $pdf->Output();
  ?>