<?php
require_once("../config/config_gcp.php");

$fact_number = $_GET['idi'];

if ($_SESSION["login"] != 1 && $_SESSION["grower"] == "") {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);


if (isset($_REQUEST["total"])) {

    for ($i = 1; $i <= $_POST["totalrow"] - 1; $i++) {
        
                          
        $update = "update grower_parameter 
                      set price_card     = '" . $_POST["price-" . $_POST["pro-" . $i]]   . "' ,
                          price_special  = '" . $_POST["pricesp-" . $_POST["pro-" . $i]] . "'
                    where id ='" . $_POST["pro-" . $i] . "'   ";               
        
        
        mysqli_query($con, $update);

    }
}

require_once '../includes/profile-header.php';
require_once "../includes/left_sidebar_buyer.php";
?>
<?php

$sel_products = "select gp.id   , gp.idsc , gp.subcategory , gp.size , gp.stem_bunch  , gp.description ,
                        gp.type , gp.feature , gp.factor , gp.id_ficha ,gp.price_card,
                        s.name as namesize   , f.name as namefeature , b.name as namestems
                   from grower_parameter gp 
                  inner JOIN sizes s ON gp.size = s.id 
                  inner JOIN bunch_sizes b ON gp.stem_bunch = b.id              
                   left JOIN features f  ON gp.feature = f.id 
                  where gp.idsc in (select subcaegoryid from grower_card group by subcaegoryid)   ";    


$rs_prodcuts = mysqli_query($con, $sel_products);
$total = mysqli_num_rows($rs_prodcuts);

$num_record = $total;
$display = 150;
$XX = '<div class="notfound">No Item Found !</div>';


function initial($fuser, $init, $display){
    $query = "select gp.id as gid, gp.idsc , sb.name as subcategory , gp.size , gp.stem_bunch  , gp.description ,
                     gp.type , gp.feature , gp.factor , gp.id_ficha ,gp.price_card, gp.price_special,
                     s.name as namesize   , f.name as namefeature , b.name as namestems
                from grower_parameter gp 
               inner join subcategory sb on gp.idsc = sb.id                  
               inner JOIN sizes s ON gp.size = s.id 
               inner JOIN bunch_sizes b ON gp.stem_bunch = b.id 
                left JOIN features f  ON gp.feature = f.id  
               where gp.idsc in (select subcaegoryid from grower_card group by subcaegoryid)
               order by sb.name ";
                   
    return $query;
}




if (isset($_POST["startrow"]) && $_POST["startrow"] != "") {
    $sr = $_POST["startrow"] + 1;
    $query2 = initial($fact_number, $_POST["startrow"], $display);
    $result2 = mysqli_query($con, $query2);
} else {
    if (empty($startrow)) {
        $startrow = 0;
        $sr = 1;
    }
    $query2 = initial($fact_number, 0, $display);
    $result2 = mysqli_query($con, $query2);
}


echo $display;
?>

    <section id="middle">
        <!-- page title -->
        <header id="page-header">
            <h1>Card</h1>
            <ol class="breadcrumb">
                <li><a href="#">Grower</a></li>
                <li class="active"> Price Card</li>
            </ol>
        </header>
        <!-- /page title -->
        <div id="content" class="padding-20">
            <div id="panel-2" class="panel panel-default">
                <div class="panel-heading">
                <span class="title elipsis">
                 <!--   <strong>Update Stock</strong>  panel title -->
                </span>
                    <!-- right options -->
                    <ul class="options pull-right list-inline">
                        <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                        <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                        <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                    </ul>
                    <!-- /right options -->
                    
                </div>
                <!-- panel content -->
                <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">
                    <div class="panel-body">
                        <input type="submit" id="submitu" class="btn btn-success btn-sm" name="submitu" value="Update All">
                        <div class="table-responsive">
                            <table id="sample_1" class="table table-hover table-vertical-middle nomargin dataTable" role="grid" aria-describedby="sample_1_info">
                                <thead>
                                   <tr role="row">
                                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column " style="width: 300px;">Subcategory</th>
                                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 150px;">Card</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column g" style="width: 150px;">Size</th>
                                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ag" style="width: 100px;">Stems</th>
                                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascendg" style="width: 100px;">Feature</th>
                                        
                                        <th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascendg" style="width: 100px;">Price</th>                                        
                                        <!--th class="sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascendg" style="width: 100px;">Special Price</th-->                                                                                
                                   </tr>
                                </thead>
                                
                                <tbody>
                                <?php
                                $i = 1;

                                while ($producs = mysqli_fetch_array($result2)) {
                                    

                                    if ($producs["gprice"] > 0) {
                                        $kp = $producs["gprice"];
                                    } else {
                                        $kp = $price["price"];
                                    }
                                    ?>
                                    
                                    
                                    <tr>
                                        <td><?php echo $producs["subcategory"] ?>  </td>
                                        <td><?php echo $producs["id_ficha"]  ?>  </td>                                        
                                        <td><?php echo $producs["namesize"] ?> </td>
                                        <td><?php echo $producs["namestems"] ?> </td>                                        
                                        <td><?php echo $producs["namefeature"] ?> </td>                                                                                
                                        
                                        <td><input type="text" class="form-control" name="price-<?php echo $producs["gid"] ?>" id="price-<?php echo $producs["gid"] ?>" value="<?php echo $producs["price_card"] ?>"></td>
                                        
                                        <!--td><input type="text" class="form-control" name="pricesp-<?php echo $producs["gid"] ?>" id="pricesp-<?php echo $producs["gid"] ?>" value="<?php echo $producs["price_special"] ?>"></td-->                                        
                                        
                                        
                                        <td>                                           
                                            <input type="hidden" class="form-control" name="pro-<?php echo $i ?>" value="<?php echo $producs["gid"] ?>"/>
                                            <input type="hidden" class="form-control" name="grower-<?php echo $producs["gid"] ?>" value="<?php echo $producs["growerid"] ?>">
                                        </td>
                                        
                                        <td><a href="<?php echo SITE_URL; ?>buyer/card_price_special_prod.php?idsub=<?php echo $producs["idsc"]."&idsize=".$producs["size"]."&id_gid=".$producs["gid"] ?>" class="btn btn-success btn-xs relative"> Product </a></td>                                        
                                    </tr>
                                    
                                    
                                    <?php $i++;
                                }
                                ?>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                    <input type="hidden" name="totalrow" value="<?php echo $i ?>"/>
                </form>
                <!-- /panel content -->
            </div>
            <!-- /PANEL -->
            <nav>
                <ul class="pagination">
                    <?php
                    if ($_POST["startrow"] != 0) {

                        $prevrow = $_POST["startrow"] - $display;

                        print("<li><a href=\"javascript:onclick=funPage($prevrow)\" class='link-sample'>Previous </a></li>");
                    }
                    $pages = intval($num_record / $display);

                    if ($num_record % $display) {

                        $pages++;
                    }
                    $numofpages = $pages;
                    $cur_page = $_POST["startrow"] / $display;
                    $range = 5;
                    $range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2;
                    $range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min;
                    $page_min = $cur_page - $range_min;
                    $page_max = $cur_page + $range_max;
                    $page_min = ($page_min < 1) ? 1 : $page_min;
                    $page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max;
                    if ($page_max > $numofpages) {
                        $page_min = ($page_min > 1) ? $numofpages - $range + 1 : 1;
                        $page_max = $numofpages;
                    }
                    if ($pages > 1) {
                        print("&nbsp;");
                        for ($i = $page_min; $i <= $page_max; $i++) {
                            if ($cur_page + 1 == $i) {
                                $nextrow = $display * ($i - 1);
                                print("<li class='active'><a href='javascript:void();'>$i</a></li>");
                            } else {

                                $nextrow = $display * ($i - 1);
                                print("<li><a href=\"javascript:onclick=funPage($nextrow)\"  class='link-sample'> $i </a></li>");
                            }
                        }
                        print("&nbsp;");
                    }
                    if ($pages > 1) {

                        if (!(($_POST["startrow"] / $display) == $pages - 1) && $pages != 1) {

                            $nextrow = $_POST["startrow"] + $display;

                            print("<li><a href=\"javascript:onclick=funPage($nextrow)\" class='link-sample'> Next</a></li> ");
                        }
                    }

                    if ($num_record < 1) {
                        print("<span class='text'>" . $XX . "</span>");
                    }
                    ?></ul>
            </nav>
        </div>
        <form method="post" name="frmfprd" action="">
            <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
        </form>
    </section>
    <!-- /MIDDLE -->
    <script tpe="text/javascript">
        function funPage(pageno) {
            document.frmfprd.startrow.value = pageno;
            document.frmfprd.submit();
        }
        function docolorchange() {
            window.location.href = '<?php echo SITE_URL; ?>buy.php?id=<?php echo $_GET["id"] ?>&categories=<?php echo $strdelete ?>&l=<?php echo $_GET["l"] ?>&c=' + $('#color').val();
        }
        function frmsubmite() {
            document.frmfilter.submit();
        }
        function doadd(id) {
            var check = 0;
            if ($('#qty-' + id).val() == "") {
                $('#ermsg-' + id).html("please enter box qty.")
                check = 1;
            }

            if (check == 0) {
                $('#ermsg-' + id).html("");
                $('#gid6').val(id);
                $('#frmrequest').submit();
            }
        }
    </script>
    <link href="../assets/css/layout-datatables.css" rel="stylesheet" type="text/css"/>
<?php include("../includes/footer_new.php"); ?>
