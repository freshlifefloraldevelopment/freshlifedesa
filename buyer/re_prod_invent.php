<?php
include("../config/config_gcp.php");

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}

$order_id     = $_REQUEST['order_val'];
$sizeId       = $_REQUEST['sizeId'];
$box_quantity = $_REQUEST['box_quantity'];
$boxtype      = $_REQUEST['type_box'];
$productId    = $_REQUEST['productId'];
$buyer_id     = $_SESSION["buyer"];
$today        = date('Y-m-d');
$grower_price = $_REQUEST['buyerPrice'];
$grower_id    = $_REQUEST['grower_id'];
$comments     = $_REQUEST['comment'];
$qty          = $_REQUEST['box_quantity'];

$order_serial = 1;
$discount = 0;
$featureId = "";
$noofstems = "";

if (!empty($_REQUEST['delivery_date'])) {
    $delivery_date = $_REQUEST['delivery_date'];
} else {
    $delivery_date = date('Y-m-d');
}


$tax      = $_REQUEST['tax'];
$handling = $_REQUEST['handling'];
$shipping = $_REQUEST['ship_cost'];


$sq_shpp = "select  shipping_method from  buyer_orders where  id='" . $order_id . "'";
$data_sp_sh = mysqli_query($con, $sq_shpp);
$dt_sp_sh = mysqli_fetch_assoc($data_sp_sh);
$shippingMethod = $dt_sp_sh['shipping_method'];

$qry = "select id , id_order , order_serial 
          from buyer_requests 
         where id_order='" . $order_id . "'  
         order by  order_serial DESC LIMIT 0,1";    

$data = mysqli_query($con, $qry);
$numrows = mysqli_num_rows($data);

if ($numrows > 0) {
    while ($dt = mysqli_fetch_assoc($data)) {
        $order_serial = $order_serial + $dt['order_serial'];
    }
}
$qrt = "select concat( order_number ,'-',order_serial,'-','" . $order_serial . "')  as cod   
          from  buyer_orders 
         where  id = '" . $order_id . "'";

$datas = mysqli_query($con, $qrt);
$dts = mysqli_fetch_assoc($datas);
$cod_order = $dts['cod'];

$qryMax="select (max(id)+1) as id from buyer_requests";
$dataMaximo = mysqli_query($con, $qryMax);
        while ($dt = mysqli_fetch_assoc($dataMaximo)) {
                    $IdMaximo= $dt['id'];
	}

$query = "INSERT INTO `buyer_requests`(`id`          ,
                                       `id_order`    ,
                                       `order_serial`,
                                       `cod_order`   ,
                                       `product`     ,
                                       `sizeid`      ,
                                       `feature`     ,
                                       `noofstems`   ,
                                       `qty`         ,
                                       `buyer`       ,
                                       `boxtype`     ,
                                       `date_added`  ,
                                       `type`        ,
                                       `bunches`     ,
                                       `box_name`    ,        
                                       `lfd`         ,    
                                       `lfd2`        ,
                                       `comment`     ,
                                       `box_id`      ,                  
                                       `shpping_method`,
                                       `isy`           ,
                                       `mreject`       ,
                                       `bunch_size`    ,
                                       `unseen`        ,
                                       `req_qty`       ,
                                       `bunches2`      ,
                                       `discount`      ,
                                       `inventary`)              
              VALUES ('" . $IdMaximo."'      ,
                      '" . $order_id . "'    ,  
                      '" . $order_serial . "',
                      '" . $cod_order . "'   ,        
                      '" . $productId . "'   ,
                      '" . $sizeId . "'      ,
                      '" . $featureId . "'   ,        
                      '" . $noofstems . "'   ,
                      '" . $qty . "'         ,
                      '" . $buyer_id . "'    ,        
                      '" . $boxtype . "'     ,
                      '" . $today . "'       ,
                      '1'                    ,    
                      ''                     ,
                      ''                     ,
                      '" . $delivery_date ."',
                      '" . $d[1] . "'        ,
                      '" . $comments . " '   ,
                      '',        
                      '" . $shippingMethod . "',
                      '',
                      '',    
                      '',
                      '',
                      '',
                      'paa',
                      '" . $discount . "',
                      '1')";    

mysqli_query($con, $query);

$rs = mysqli_query($con, "SELECT MAX(id) AS id FROM buyer_requests");

if ($row = mysqli_fetch_row($rs)) {
    $id = trim($row[0]);
}

$insert = "insert  into  request_growers (tax      ,
                                          gprice   ,
                                          shipping ,
                                          handling ,
                                          gid,bid  ,
                                          rid      ,
                                          mailsend ,
                                          order_view)
                                  values ('" . $tax . "'         ,
                                          '" . $grower_price . "',
                                          '" . $shipping . "'    ,
                                          '" . $handling . "'    ,
                                          '" . $grower_id . "'   ,
                                          '" . $buyer_id . "'    ,
                                          '" . $id . "'          ,
                                          0,
                                          0)";                         
mysqli_query($con, $insert);


echo $tax;
echo $insert;
echo $query;
 echo 'true';
?>