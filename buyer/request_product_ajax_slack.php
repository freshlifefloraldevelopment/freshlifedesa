<?php

// PO  2018-07-02

include("../config/config_gcp.php");

//$buyer_order_id = $_SESSION['buyer_order_id'];

$buyer_order_id = $_REQUEST['order_val'];
$sizeId         = $_REQUEST['sizeId'];
$featureId      = $_REQUEST['featureId'];
$box_quantity   = $_REQUEST['box_quantity'];
$btype          = explode('_', $_REQUEST['box_quantity']);
$order_serial   = 1;

$stems          = $_REQUEST['noofstems'];
$typereq        = $_REQUEST['type'];
$boxtype        = $_REQUEST['type_box'];
$discount       = 0;
$productId      = $_REQUEST['productId'];
$shippingFee    = $_REQUEST['shippingFee'];
$conType        = 4;                      //$_REQUEST['conType'];
$buyer          = $_REQUEST['buyer'];
$cId            = $_REQUEST['comment_pro'];
$today          = date('Y-m-d');
$productName    = $_REQUEST['productName'];
$buyerPrice     = $_REQUEST['buyerPrice'];
$discount       = 0;
$date_range     = $_REQUEST['date_range'];
$d              = explode(' - ', $date_range);

if (!empty($_REQUEST['delivery_date'])) {
    $delivery_date = $_REQUEST['delivery_date'];
} else {
    $delivery_date = date('Y-m-d');
}

$growers = $_REQUEST['growers'];
$growers = $growers.",";
$order   = $_REQUEST['order_val'];
$taxes   = $_REQUEST['tax'];
$ship_p  = $_REQUEST['ship_p'];
$had_l   = $_REQUEST['hand_l'];

// before : $shippingMethod = $_REQUEST['shippingMethod'];
// 
//------------shippingMethod----------------------
$sq_shpp        = "select  shipping_method , del_date from  buyer_orders where  id='".$order."'";
$data_sp_sh     = mysqli_query($con, $sq_shpp);
$dt_sp_sh       = mysqli_fetch_assoc($data_sp_sh);
$shippingMethod = $dt_sp_sh['shipping_method'];
$delivery_date  = $dt_sp_sh['del_date'];
/*----------------------------------------------*/



function vector($gro, $price, $taxes, $ship_p, $handling)
{
    $grower = substr($gro, 0, -1);
    //$grower = substr($gro, 0, 3);
    $prices = substr($price, 0, -1);
    $taxes  = substr($taxes, 0, -1);
    $ships  = substr($ship_p, 0, -1);
    $hand   = substr($handling, 0, -1);
    $grow   = explode(',', $grower);
    $pric   = explode(',', $prices);
    $tax    = explode(',', $taxes);
    $ship   = explode(',', $ships);
    $had    = explode(',', $hand);
    
    $array  = array();
    
    for ($i = 0; $i < count($grow); $i++) {
        array_push($array, array($grow[$i], $pric[$i], $tax[$i], $ship[$i], $had[$i]));
    }
    return $array;
}

$price  = "";
$taxx   = "";
$shipps = "";
$hand_s = "";

function menor_price($array_s)
{   //$numeros=$array_s;
    //$columnas=(count($y, COUNT_RECURSIVE)/count($y));
    
    $filas  = count($array_s);    
    $filtro = array();
    $fil    = array();

    for ($i = 0; $i < $filas; $i++) {
        $g_id   = $array_s[$i][0];
        $price  = $array_s[$i][1];
        $taxx   = $array_s[$i][2];
        $shipps = $array_s[$i][3];
        $hand_s = $array_s[$i][4];
        $j = $i + 1;
        
        for ($j; $j < $filas; $j++) {
            if ($array_s[$j][0] == $g_id) {
                if ($array_s[$j][1] < $price) {
                    $price  = $array_s[$j][1];
                    $taxx   = $array_s[$j][2];
                    $shipps = $array_s[$i][3];
                    $hand_s = $array_s[$i][4];
                }
            }
        }
        //$i=$j;
        if (!in_array($g_id, $fil)) {
            array_push($fil, $g_id);
            array_push($filtro, array($g_id, $price, $taxx, $shipps, $hand_s));
        }
        //echo  "hola".$i;
    }
    return $filtro;
}     

// Fin Funciones

$cod_prices = array();
$cod_prices = vector($growers, $buyerPrice, $taxes, $ship_p, $had_l);

/*-----------------------------------------*/

$menor_price = array();
$menor_price = menor_price($cod_prices);

/*menor price*/

$fil_prices  = array();
$fil_growers = array();
$fil_taxes   = array();
$fil_shipps  = array();
$fil_had     = array();

for ($i = 0; $i < count($menor_price); $i++) {
    $fil_prices[]  = $menor_price[$i][1];
    $fil_growers[] = $menor_price[$i][0];
    $fil_taxes[]   = $menor_price[$i][2];
    $fil_shipps[]  = $menor_price[$i][3];
    $fil_had[]     = $menor_price[$i][4];
}

//$growers = substr($growers, 0,-1);

if (!empty($d)) {

    $qry     = "select id,id_order,order_serial from  buyer_requests where  id_order='" . $order . "'  order by  order_serial DESC LIMIT 0,1";
    $data    = mysqli_query($con, $qry);
    $numrows = mysqli_num_rows($data);
    
    if ($numrows > 0) {
        while ($dt = mysqli_fetch_assoc($data)) {
            $order_serial = $order_serial + $dt['order_serial'];
        }
    }
    $qrt        = "select  concat( order_number ,'-',order_serial,'-','" . $order_serial . "')  as cod   from  buyer_orders where  id='" . $order . "'";
    $datas      = mysqli_query($con, $qrt);
    $dts        = mysqli_fetch_assoc($datas);
    $cod_order  = $dts['cod'];
    
    $qryMax     = "select (max(id)+1) as id from buyer_requests";
    $dataMaximo = mysqli_query($con, $qryMax);
         
        while ($dt = mysqli_fetch_assoc($dataMaximo)) {
            $IdMaximo= $dt['id'];
		}
                
                
     // Proceso para obtener LFD           
    $getShippingMethod = "select connections from shipping_method where id='" . $shippingMethod . "'";
    $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);
    $connections = unserialize($shippingMethodDetail['connections']);
    
 foreach ($connections as $connection) {
    $getDiasTran = mysqli_query($con, "select (trasit_time+1) trasit_time from connections where id='" . $connection . "'");
    $conTransito = mysqli_fetch_assoc($getDiasTran);                
    $tran_transito = $conTransito['trasit_time'];
   }
       
    $sqlids = "SELECT date_add('".$delivery_date."', INTERVAL -".$tran_transito." DAY) fecha";  
    $row_sqlids = mysqli_query($con, $sqlids);
                                                            
    while ($fila =mysqli_fetch_assoc($row_sqlids)) {
              $date_lfd= $fila["fecha"];
    }
    
    $fecha_tmp = $date_lfd;
    $dayofweek = date('D', strtotime($date_lfd));  
    if ($dayofweek == "Sun")  {
         $date_lfd = strtotime ('-2 day',strtotime ($fecha_tmp) );
         $date_lfd = date ( 'Y-m-j' , $date_lfd );
    }else {
         $date_lfd=$fecha_tmp;                    
     }                                                                            
	
    $query = "INSERT INTO buyer_requests
               (id             ,
                id_order       ,
                order_serial   ,
                cod_order      ,
                product        ,
                sizeid         , 
                feature        ,
                noofstems      ,
                qty            ,
                buyer          ,
                boxtype        ,
                date_added     ,
                type           ,
                bunches        ,
                box_name       ,
                lfd            ,
                lfd2           ,
                comment        ,
                box_id         ,
                shpping_method ,
                isy            ,
                mreject        ,
                bunch_size     ,
                unseen         ,
                req_qty        ,
                bunches2       ,
                discount       ,
                inventary)
        VALUES ('".$IdMaximo."' , '" . $order . "' , '" . $order_serial . "' , '" . $cod_order . "' , '" . $productId . "' , '" . $sizeId . "' , '" . $featureId . "' , '" . $stems . "' , '" . $btype [0] . "' , '" . $buyer . "' , '" . $boxtype . "' , '" . $today . "' , '" . $typereq . "' ,
              '','','" . $date_lfd . "','" . $d[1] . "','" . $cId . "','','" . $shippingMethod . "','','','','','','','" . $discount . "',0)";
    
    
             // slack

$message = "Recibio un Request de Irshad";

$room = "#general";

$icon = ":car:";

$url = "https://hooks.slack.com/services/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";

 

$data = "payload=" . json_encode(array(

"channel"       =>  "{$room}",

"text"          =>  $message,

"icon_emoji"    =>  $icon

));


$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

$result = curl_exec($ch);

curl_close($ch);    
} 

$growers = $fil_growers;
$prices = $fil_prices;

$cont_prices = 0;

if (mysqli_query($con, $query)) {
    
    $id         = $IdMaximo;
    $xs         = $id;
    
    $checkReqId = "select buyer_request_id from buyer_orders where id ='" . $IdMaximo . "'";
    $reqQry     = mysqli_query($con, $checkReqId);
    $getReqId   = mysqli_fetch_assoc($reqQry);
    
    if (!empty($getReqId['buyer_request_id'])) {
        $id = $getReqId['buyer_request_id'] . ',' . $id;
    }
    foreach ($growers as $grower) {
        
          $insert = "insert into request_growers set  tax ='" . $fil_taxes[$cont_prices] . "', shipping='" . $fil_shipps[$cont_prices] . "' ,gid='" . $grower . "'             ,rid='" . $xs . "',bid='" . $buyer . "',gprice='" . $prices[$cont_prices] . "',handling='" . $fil_had[$cont_prices] . "'";
     //  $insert = "insert into request_growers set  tax ='" . $fil_taxes[$cont_prices] . "', shipping='" . $fil_shipps[$cont_prices] . "' ,gid='" . $_REQUEST['growers'] . "',rid='" . $xs . "',bid='" . $buyer . "',gprice='" . $prices[$cont_prices] . "',handling='" . $fil_had[$cont_prices] . "'";
        mysqli_query($con, $insert);
        
        $requestId     = mysqli_insert_id($con);
        $getGrowers    = "SELECT * FROM growers WHERE id =" . $grower;
        $growerResults = mysqli_query($con, $getGrowers);
        $growerDetails = mysqli_fetch_assoc($growerResults);
        $text          = 'Buyer has sent a request for ' . $productName;
        
//      $to = $growerDetails['contact1email'];
//      $to = $to.",logging@freshlifefloral.com,dan@freshlifefloral.com";
        
        $to = 'sahil@freshlifefloral.com';
        $to = 'test.test1397@gmail.com';
        $subject = "You have receive new offer(s) at freshlifefloral.com";
        $body    .= "Hello <b>" . $growerDetails["growers_name"] . "</b><br/><br/>";
        $body    .= "We have new offers(s) from our buyers.<br/><br/><br/>";
        $body    .= $text;
        $body    .= "<br/><br/>Please click on following link to login to reply to these offers.<br/>";
        $body    .= "<a href='" . SITE_URL . "'>'" . SITE_URL . "'</a><br/><br/>";
        $body    .= "Thanks!";
        $body    .= "<br/><b>Freshlife Floral Team<b>";
        $headers  = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= "From:info@freshlifefloral.com" . "\r\n";
        
        //mail($to,$subject,$body,$headers);
        $updater = "update request_growers set mailsend=1 where id='" . $requestId . "'";
        mysqli_query($con, $updater);

        $cont_prices++;
    }
    echo 'true';


} else {
    echo 'false';
}
?>
