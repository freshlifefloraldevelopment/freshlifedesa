<?php 	

// PO # 1

include "../config/config.php";

@session_start();

if(!isset($_SESSION['tomodachi-admin']) && $_SESSION['tomodachi-admin'] != 1)
{

header("location: index.php");

}

$qsel="select g.id,max(a.date_added) date_added,g.growers_name  from growers g left join grower_offer_reply a on a.grower_id = g.id where active = 'active' ";
$qsel.= " group by g.id,g.growers_name ";
$qsel.=" order by a.date_added DESC";        
$rs=mysql_query($qsel);


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <title>Admin Area</title>

        <link href="css/style.css" rel="stylesheet" type="text/css" />
        
        <link href="css/demo_page.css" rel="stylesheet" type="text/css" />
        <link href="css/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
        
        <script type="text/javascript" charset="utf-8">       
        


            $(document).ready(function () {

                oTable = $('#example').dataTable({
                    //"sScrollXInner": "130%",

                    "bJQueryUI": true,
                    //"sScrollY": "536",

                    "sPaginationType": "full_numbers"

                });

            });

        </script>

    </head>

    <body>

        <table width="960" border="0" align="center" cellpadding="0" cellspacing="0">

            <?php  include("includes/header_inner.php");?>

            <tr>

                <td height="5"></td>

            </tr>

            <tr>

                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>

                            <?php  include("includes/left.php");?>

                            <td width="5">&nbsp;</td>

                            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                    <tr>

                                        <td width="10" valign="top" background="images/middle-leftline.gif"><img src="images/middle-topleft.gif" width="10" height="80" /></td>

                                        <td valign="top" background="images/middle-topshade.gif" style="background-repeat:repeat-x;"><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                <tr>

                                                    <td width="10">&nbsp;</td>

                                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">

                                                            <tr>

                                                                <td height="5"></td>

                                                            </tr>

                                                            <tr>

                                                                <td class="pagetitle">Growers Orders</td>

                                                            </tr>

                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>




                                                            <tr>

                                                                <td>&nbsp;</td>

                                                            </tr>

                                                            <tr>

                                                                <td><div id="box">

                                                                        <div id="container">			

                                                                            <div class="demo_jui">

                                                                                <table cellpadding="0" cellspacing="0" border="1" class="display" id="example" bordercolor="#e4e4e4">

                                                                                    <thead>

                                                                                        <tr>

                                                                                            <th width="22%" align="left">Grower Name</th> 

                                                                                            <th align="center" width="8%">Dash Board</th>
                                                                                            
                                                                                            <th align="center" width="8%">Offer Status</th>

                                                                                            <th align="center" width="8%">Pending Requests</th>
                                                                                            
                                                                                            <th align="center" width="8%">New Requests</th>                                                                                            

                                                                                        </tr>

                                                                                    </thead>

                                                                                    <tbody>

                                                                                        <?php 

                                                                                        $sr=1;

                                                                                        while($product=mysql_fetch_array($rs))  {
                                                                                                                                                                                        
                                                                                            $query1 = "SELECT gpb.id AS cartid,id_order,order_serial,cod_order,gpb.product,sizeid,feature,noofstems,qty,buyer,
                                                                                                             gpb.boxtype,gpb.date_added,gpb.type,gpb.bunches,gpb.box_name,lfd,comment, box_id,shpping_method,isy,
                                                                                                             mreject,bunch_size,unseen,req_qty,bunches2 ,gpb.date_added ,p.name,
                                                                                                             p.color_id,p.image_path,s.name AS subs,sh.name AS sizename,
                                                                                                             ff.name AS featurename,rg.gprice AS price,rg.tax AS tax ,rg.shipping AS  cost_ship,rg.handling ,gor.offer_id ,gpb.inventary
                                                                                                        FROM buyer_requests gpb
                                                                                                       INNER JOIN request_growers rg     ON gpb.id=rg.rid
                                                                                                        LEFT JOIN product p              ON gpb.product = p.id
                                                                                                        LEFT JOIN subcategory s          ON p.subcategoryid=s.id  
                                                                                                        LEFT JOIN features ff            ON gpb.feature=ff.id
                                                                                                        LEFT JOIN grower_offer_reply gor ON rg.rid=gor.offer_id AND rg.gid=gor.grower_id
                                                                                                        LEFT JOIN sizes sh               ON gpb.sizeid=sh.id                                                                                              
                                                                                                       WHERE rg.gid='" . $product["id"] . "' AND gpb.lfd>='" . date("Y-m-d") . "'  and   IFNULL(gor.reject,'-1')<>'1'  
                                                                                                       GROUP BY gpb.id ORDER BY gpb.id DESC";
                                                                                            
                                                                                            $result1 = mysql_query($query1);
                                                                                            $total_records = mysql_num_rows($result1);   
                                                                                            
                                                                                            
                                                                                                $temp=explode(",",$product["products"]);  
                                                                                                $no=count($temp);
                                                                                                
                                                                                                if($no>=3) {
                                                                                                        $no = $no-2; 
                                                                                                }else{
                                                                                                        $no = 0;
                                                                                                }

                                                                                                $temp2=explode(",",$product["farms"]);  
                                                                                                $no2=count($temp2);							 

                                                                                                if($no2<=1)  {
                                                                                                       if($no2>=3)  {
                                                                                                            $no2 = $no2-2; 
                                                                                                       }else{
                                                                                                            $no2 = 0 ;
                                                                                                        }
                                                                                                }


                                                                                        ?>
                                                                                        <div>
                                                                                        <tr class="gradeU">

                                                                                            <td class="text" align="left"><?php  echo $product["growers_name"] ?></td>

                                                                                            <td align="center" ><a href="/saveloginGrower.php?id=<?php  echo $product["id"] ?>" style="color:#000; font-weight:bold;" >Grower</a></td>

                                                                                            <td align="center" ><a href="/saveloginOfferStatus.php?id=<?php  echo $product["id"] ?>" style="color:#000; font-weight:bold;" >Offer Status</a></td>
                                                                                                                                                                                       
                                                                                            <td align="center" ><a href="javascript:void(0);" id="buyer_model" data-toggle="modal" data-target="#myModal_buyer1"><?php echo $total_records; ?></a></td>                                                                                            
                                                                                       </div> 
                                                            <!-- Buyer Modal -->
                                                            <div class="modal fade text_modal_1" id="myModal_buyer1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-sm">
                                                                    <!-- Modal content-->
                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-header">
                                                                            <h4 class="modal-title" id="myLargeModalLabel">Buyer Requests</h4>
                                                                        </div>                                                                        
                                                                    </div>

                                                                </div>                                                                
                                                            </div>
                                                            <!-- END Buyer Modal -->
                                                                                            <td align="center" ><a href="/saveloginRecievedOffersNew.php?id=<?php  echo $product["id"] ?>" style="color:#000; font-weight:bold;" >New Requests</a></td>                                                                                            

                                                                                        </tr>

                                                                                        <?php 

                                                                                        $sr++;

                                                                                        }

                                                                                        ?> 



                                                                                    </tbody>

                                                                                </table>



                                                                            </div>

                                                                        </div>



                                                                    </div>

                                                                </td>

                                                            </tr>

                                                        </table></td>

                                                    <td width="10">&nbsp;</td>

                                                </tr>

                                            </table></td>

                                        <td width="10" valign="top" background="images/middle-rightline.gif"><img src="images/middle-topright.gif" width="10" height="80" /></td>

                                    </tr>

                                    <tr>

                                        <td background="images/middle-leftline.gif"></td>

                                        <td>&nbsp;</td>

                                        <td background="images/middle-rightline.gif"></td>

                                    </tr>

                                    <tr>

                                        <td height="10"><img src="images/middle-bottomleft.gif" width="10" height="10" /></td>

                                        <td background="images/middle-bottomline.gif"></td>

                                        <td><img src="images/middle-bottomright.gif" width="10" height="10" /></td>

                                    </tr>

                                </table></td>

                        </tr>

                    </table></td>

            </tr>

            <tr>

                <td height="10"></td>

            </tr>

            <?php  include("includes/footer-inner.php"); ?>

            <tr>

                <td>&nbsp;</td>

            </tr>

        </table>

    </body>

</html>
