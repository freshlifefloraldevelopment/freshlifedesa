<?php

require_once("../config/config_gcp.php");

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}

$userSessionID = $_SESSION["buyer"];

$cliente_inv = $_GET['id_cli'];

$swcab = $_GET['sw'];
$idfac = $_GET['fac_id'];


/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,company_role,country,state_text,city,zip,shipping_country,shipping_state_text,shipping_city,
shipping_zip,coordination,address,address2,shipping_address,shipping_address2,name_on_card,credit_card_type,credit_card,cvv,expiration_month,
expiration_year,is_public,biographical_info,profile_image FROM buyers WHERE id =$userSessionID")
) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $company_role, $country, $state_text, $city, $zip, $shipping_country, $shipping_state_text, $shipping_city, $shipping_zip, $coordination, $address, $address2, $shipping_address, $shipping_address2, $name_on_card, $credit_card_type, $credit_card, $cvv, $expiration_month, $expiration_year, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    echo "else";
    exit();
    header("location:" . SITE_URL);
    die;
}      
     
$img_url = '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}
$page_request = "dashboard";
?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

    <!--script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script-->
    
<script>       
        
function selectscategory()	{

   removeAllOptions(document.frmrequest.var_add);

	addOption(document.frmrequest.var_add,"","-- Select Variety --");
        
        
	  <?php

		$sel_subcategory="select br.id as idbr     , br.lfd        ,br.product as idcod  ,  p.id as codvar,
                                         gor.id as idgor   , gor.offer_id  , gor.offer_id_index , gor.grower_id   , gor.buyer_id , 
                                         gor. product as  productname , 
                                         gor.product_subcategory, gor.size as size_name  , (gor.bunchqty-gor.reserve) as  bunchqty  ,   gor.steams ,
                                         g.growers_name , br.feature, f.name as featurename,
                                         br.id_order,p.image_path , res.id_client as control,qucik_desc
                                    from buyer_requests br
                                   inner join grower_offer_reply gor on gor.offer_id = br.id
                                   inner join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name 
                                   inner join buyer_orders bo  on br.id_order = bo.id                
                                    left join growers g on gor.grower_id = g.id
                                    left join features f on br.feature = f.id 
                                    left JOIN buyer_requests res ON gor.request_id = res.id
                                   where g.active = 'active' 
                                     and (gor.bunchqty-gor.reserve) > 0
                                     and bo.availability = 1 
                                     and br.buyer = '" . $userSessionID . "'
                                     and res.id_client = 0 order by br.id_order";

		$res_subcategory=mysqli_query($con,$sel_subcategory);

		while($rw_subcategory=mysqli_fetch_array($res_subcategory))	{

	  ?>
                               
                var ordenid = $('#order_add' + ' :selected').val();
                
               // alert(ordenid);
                
                if(ordenid=="<?php echo $rw_subcategory["id_order"]?>")	{

                          addOption(document.frmrequest.var_add,"<?php echo $rw_subcategory["codvar"];?>","<?php echo $rw_subcategory["bunchqty"]."  ".$rw_subcategory["productname"]."-*-".$rw_subcategory["size_name"]."-cm ".$rw_subcategory["growers_name"]."-".$rw_subcategory["idgor"]."-".$rw_subcategory["featurename"]."-".$rw_subcategory["product_subcategory"]."-".$rw_subcategory["qucik_desc"] ;?>");
                         
			 $('#var_add').val('<?php echo $_POST["var_add"]?>');  
                         
                         //var xx = document.
	  	}
                                              
	  <?php	}   ?>	

	}
                        

	function removeAllOptions(selectbox)	{
		var i;

		for(i=selectbox.options.length-1;i>=0;i--){
			selectbox.remove(i);
		}
	}

	function addOption(selectbox,value,text){

		var optn=document.createElement("OPTION");
		optn.text=text;
		optn.value=value;
		selectbox.options.add(optn);
	}

function imagen(){       
        
	  <?php

		$sel_img="select br.id as idbr     , br.lfd        ,br.product as idcod  ,  p.id as codvar,
                                         gor.id as idgor   , gor.offer_id  , gor.offer_id_index , gor.grower_id   , gor.buyer_id , 
                                         gor. product as  productname , 
                                         gor.product_subcategory, gor.size as size_name  , (gor.bunchqty-gor.reserve) as  bunchqty  ,   gor.steams ,
                                         g.growers_name , br.feature, f.name as featurename,
                                         br.id_order,p.image_path , res.id_client as control
                                    from buyer_requests br
                                   inner join grower_offer_reply gor on gor.offer_id = br.id
                                   inner join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name 
                                   inner join buyer_orders bo on br.id_order = bo.id
                                    left join growers g on gor.grower_id = g.id
                                    left join features f on br.feature = f.id 
                                    left JOIN buyer_requests res ON gor.request_id = res.id
                                   where g.active = 'active' 
                                     and (gor.bunchqty-gor.reserve) > 0
                                     and bo.availability = 1 
                                     and br.buyer = '" . $userSessionID . "'
                                     and res.id_client = 0 ";

		$res_img=mysqli_query($con,$sel_img);

		while($rw_imagen=mysqli_fetch_array($res_img))	{

	  ?>
                               
                var idprod = $('#var_add' + ' :selected').val();
                
                
               // alert(ordenid);
                
                if(idprod=="<?php echo $rw_imagen["codvar"]?>")	{
                     
                        $('#errormsg-').html("<?= $rw_imagen["image_path"]?>")  
                        
                        var span_Text = document.getElementById("errormsg-").innerText;
                        //alert (span_Text);
                        
                        var  url_url= 'https://app.freshlifefloral.com/' + span_Text;
                        
                        $('input[name=idimg]').val(span_Text); 
                        
                        $('#my_image').attr('src',url_url);
	  	}
                                              
	  <?php	}   ?>	

	}
        
function imagen_req(){       
    
        var productText_requ = $('#productvar' + ' :selected').text();    
        
        var path = productText_requ.split("+", 2);        
        $('input[name=imgpath]').val(path[1]);        
        
       var pimage = $('#imgpath').val();  
       
        //alert(pimage);
        
                        $('#errormsg-').html(pimage)  
                        
                        var span_Text = document.getElementById("errormsg-").innerText;
                        
                        var  url_url= 'https://app.freshlifefloral.com/' + span_Text;
                        
                        $('input[name=idimg]').val(span_Text); 
                        
                        $('#my_image').attr('src',url_url);
                                              	  
	}        
</script>   

<style type="text/css">
    .comment-author .g_title {
        color: #337ab7;
        font-weight: normal;
    }
</style>
<style type="text/css">
    .comment-body.cls_comment_body {
        padding-left: 65px;
    }

    li.grower-comments .comment-body {
        padding-left: 0px;
    }

    .status_modal.in li.comment .comment-body {
        margin-left: 60px;
    }
    
    .mySelect option { color: green; } 
    .mySelect option[value="1042"] { color: red; } 
</style>

<?php
   
/*
    $sqlDetalis="select irs.id  as gid      ,  irs.id_week   , irs.id_client , irs.product, 
                        irs.prod_name , irs.buyer   , irs.grower_id , irs.qty_pack  , irs.size   , 
                        irs.steams    , irs.comment,
                        s.name as psubcatego , p.name as name_product , sz.name as size_name,
                        cli.name as subclient,
                        irs.id_state , irs.fact_id,
                        irs.date_ship  , irs.date_del
                  from order_subclient irs
                 INNER JOIN product p on irs.product = p.id 
                 INNER JOIN sizes sz  on irs.size = sz.id 
                 INNER join subcategory s on p.subcategoryid = s.id 
                 INNER JOIN sub_client cli on irs.id_client = cli.id 
                 where irs.fact_id   = '".$idfac."' 
                 order by s.name,p.name ";

    $result = mysqli_query($con, $sqlDetalis);     
*/

 ?>
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
 
</head>
<section id="middle">
            <!-- panel content inicio po -->            
            <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">
			<!--section id="middle"-->

				<!-- page title -->
				<header id="page-header">
					<h1>Customer Order</h1>
					<ol class="breadcrumb">
						<li><a href="#">SubClient</a></li>
						<li class="active">Assign</li>
					</ol>
				</header>
				<!-- /page title -->


				<div id="content" class="padding-20">

					<div class="panel panel-default">
            <div class="panel-heading">

                 <a href="<?php echo SITE_URL; ?>buyer/client_calendar_special.php?id_cli=<?php echo $scli ?>" class="btn btn-success btn-xs relative">Back </a>                
                                                                   
                <!-- right options -->
                <!--ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-Confirmed-title="Confirmed" data-Confirmed-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul-->
                <!-- /right options -->
                
            </div>                                            

						<div class="panel-body">
                                                    <!--input type="submit" id="submiso" class="btn btn-success btn-sm" name="submiso" value="Add Order"-->     
							<div class="row">                                                                             
								<div class="col-md-6 col-sm-6 text-left">

								
								<ul class="list-unstyled">
                                                                    
								    <li><strong>Client.............:</strong>
                       
                                                                        <?php   $sql_cli = "select id,name 
                                                                                              from sub_client 
                                                                                             where id != 1 
                                                                                               and buyer = '" . $userSessionID . "'
                                                                                             order by name";
                                                                                             
                                                                                            $result_cli = mysqli_query($con, $sql_cli);
                                                                        ?>	                            
                                                                            
                                                                        <select name="cli_add" id="cli_add" onclick="checkOptionship();" style="margin-top:10px; height:35px; padding:3px; width:250px;border-radius: 5px" class="listmenu"  >
                                                                                    <option value="">--Select Client--</option>
                                                                            <?php				       
                                                                                while ($row_client = mysqli_fetch_assoc($result_cli)) {
                                                                            ?>                       
                                                                                     <option value="<?php echo $row_client['id']; ?>"><?= $row_client['name']; ?></option>
                                                                            <?php    }	?>		
                                                                        </select>
                                                                    </li>
                                                                    
                                                                    
                            <li><strong>Delivery Date.:</strong>
                                    <label class="field cls_date_start_dated" id="cls_date_del">
                                                <input class="form-control required start_date" placeholder="Select Date Delivery" autocomplete="xxx" style="margin-top:10px; height:35px; padding:3px;width: 250px!important;text-indent: 32px;border-radius: 5px;" type="text" value="<?php echo $_POST["cls_date_del"];?>">
                                    </label>                            
                            </li>                                                                                                                                                                                                                                                                                                                                                                         

                            <!--li><strong>Ship Date.......:</strong>
                                    <label class="field cls_date_start_dates" id="cls_date_ship" name="cls_date_ship" >
                                                <input class="form-control required start_date" placeholder="Select Date Shipment" style="width: 250px!important;text-indent: 32px;border-radius: 5px;" type="text" value="<?php echo $_POST["cls_date_ship"];?>" >
                                    </label>                            
                            </li-->     

                                    
                                                                        <input type="hidden" name="sname" id="sname" value="<?php echo $_POST["sname"];?>" /> 
                                                                        <input type="hidden" name="dname" id="dname" value="<?php echo $_POST["dname"];?>" /> 
                                                                        <input type="hidden" name="sizename" id="sizename" value="<?php echo $_POST["sizename"];?>" />                                     
                                                                        
                                                                        
								    <li><strong>Order.............:</strong>
                       
                                                                        <?php   $sql_order = "select id,qucik_desc  
                                                                                              from buyer_orders 
                                                                                             where buyer_id='" . $userSessionID . "' 
                                                                                               and assigned=1";
                                                                                             
                                                                                            $result_order = mysqli_query($con, $sql_order);
                                                                        ?>	                            
                                                                            
                                                                        <select name="order_add" id="order_add"  onclick="grabaFecha();"  style="margin-top:10px; height:35px; padding:3px; width:250px;border-radius: 5px" class="mySelect"  >
                                                                                    <option value="">--Select Order--</option>
                                                                            <?php				       
                                                                                while ($row_order = mysqli_fetch_assoc($result_order)) {
                                                                            ?>                       
                                                                                     <option value="<?php echo $row_order['id']; ?>">
                                                                                                    <?= $row_order['id']." ".$row_order['qucik_desc']; ?></option>
                                                                            <?php    }	?>		
                                                                        </select>
                                                                    </li>                                                                        

                                                                        <li><strong>Assign............:</strong>
                                                                                                                                                                                                        
                                                                            <select name="var_add" id="var_add"   style="margin-top:10px; height:35px; padding:3px; width:300px;border-radius: 5px;" class="listmenu" >
                                                                                    <option value="">-- Select Variety --</option>
                                                                            </select>

                                                                            <button style="background:#8a2b83!important;" onclick="selectscategory()" class="btn btn-success btn-xs relative" type="button">View Varieties</button>                                                                                             
                                                                        </li>
                                                                    
                                                                        
                                                                        
                                                                        <li><strong>Request.........:</strong>
                                                                                                                                                                                                        
                                                                                <!--Product-->
                                                                                                            
                                                                            <select class="listmenu"  name="productvar"  id="productvar" style="margin-top:10px; height:35px; padding:3px; width:400px;border-radius: 5px;">
                                                                                    <option value=""> -- Select Variety -- </option>
                                                                            </select>                                                                                                            
                                                                        </li>
                                                                        
                                                                                                          
									<li><strong>Bunch............:</strong>                                                                            
                                                                            <input type="number" name="qty_pack" id="qty_pack"  style="margin-top:10px; height:35px; padding:3px; width:250px;border-radius: 5px;" value="<?php echo $_POST["qty_pack"];?>" />                                                
                                                                        </li>     

                                                                         <input type="hidden" name="qty_ped" id="qty_ped"  style="margin-top:10px; height:35px; padding:3px; width:250px;border-radius: 5px;" value="<?php echo $_POST["qty_ped"];?>" />                                                                                                                        
                                                                                                                                               
                                                                        

								</ul>
								</div>

								<div class="col-md-6 col-sm-6 text-right">
                                                                    <ul class="list-unstyled">

                                                                     <li><strong> </strong>  
                                                                         
                                                                                 <div class="error-box">
                                                                                        <span id="errormsg-" style="color: white;"></span>
                                                                                 </div>
                                                                         
                                                                            
                                                                            <img id="my_image" src="https://app.freshlifefloral.com//includes/assets/images/logo.png" width="200">
                                                                         
                                                                              <br>   
                                                                            <button style="background:#8a2b83!important;" onclick="imagen()" class="btn btn-success btn-xs relative" type="button">View Asign</button>                                                                                                                                                                         
                                                                              <br>
                                                                            <button style="background:#8a2b83!important;" onclick="imagen_req()" class="btn btn-success btn-xs relative" type="button">View Request</button>    
                                                                            <br>
                                                                            <input type="hidden" name="imgpath" id="imgpath" value="<?php echo $_POST["imgpath"];?>" />  
                                                                            
                                                                            <div class="error-box">
                                                                                    <span id="errormsg-1" style="color:#FF0000; font-size:14px; font-weight:bold; font-family:arial; display:block; clear:both;"></span>
                                                                            </div>                                                                                                                                                        
                                                                     </li>
                                                                    
                                                                    </ul>
								</div>

							</div>

							<div class="table-responsive">
								<table class="table table-condensed nomargin">
									<thead>
										<tr>
											<th>Delivery</th>                                                                                                                                                                            
											<th>Variety</th>
                                                                                        <th>Bunches</th>
                                                                                        <th>Client</th>
										</tr>
									</thead>
                                                                        
									<tbody>																					

									<?php 
                                                                                 $tmp_idorder = 0;
                                                                                 
                                                                                 $cn=1;                                                                                 
                                                                                $ir = 1;                                                                                 
                                                                                 
                                                            while($row = mysqli_fetch_assoc($result))  {
                                                                                                                                                                                                                                                                     
                                                                            ?>

								<?php 
                                                                                $totalCal = $totalCal + $Subtotal;   
                                                                                $cn++;                                                                                                                                                                     
                                                            } ?>
									</tbody>
								</table>
							</div>

							<hr class="nomargin-top" />

							<div class="row">
								<div class="col-sm-6 text-left">
									<!--h4><strong>-</strong> </h4-->
								</div>
                                                            
								<div class="col-sm-6 text-right">
									<!--ul class="list-unstyled">
                                                                            <li> <strong>Sub - Total Amount: </strong> <?php echo "$".number_format($totalCal, 2, '.', ',');  ?>                                                                            </li>                                                                                                                                  
									</ul-->  
								</div>                                                          
							</div>

						</div>
					</div>					

					<div class="panel panel-default text-right">
						<div class="panel-body">
                                                        <button id="assig" style="background:#8a2b83!important;" onclick="requestProduct('<?php echo $ir ?>')" class="btn btn-primary" type="button">Assigned Customer</button>
						</div>
					</div>                                                                                                                    
                                    
                                    
				</div>
                                
			<!--/section-->
                        
			<!-- /MIDDLE -->

		</div>



	
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
		<script type="text/javascript" src="assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="assets/js/app.js"></script>

	<input type="hidden" name="totalrow" value="<?php echo $cn ?>"/>
        </form>
</section>
<?php require_once '../includes/footer_new.php'; ?>
<script type='text/javascript'>
    $(window).load(function () {
        $('#loading').css("display", "none");
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        
          $("#assig").click(function(){
              $("#assig").prop("disabled", true);
          });                        
        
          //   $('select').select2();
             
                $('#productvar').select2({
                    ajax: {
                        url: "search_variety_customer.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });   

              
        ////////////////////////////////////////
        
                $('#var_add_xx').select({
                    ajax: {
                        url: "search_date_filter.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                });        

    });

</script>

<script>
    function grabaFecha() {
        
        var shipDate = $('#cls_date_ship').find("input").val();
                      
         $('input[name=sname]').val(shipDate);     
         
         console.log(shipDate);
         
        var delDate = $('#cls_date_del').find("input").val();
                      
         $('input[name=dname]').val(delDate);              
                                 
    }        
    
    function bunQuantity() {
        
        var productText = $('#var_add' + ' :selected').text();  
                        		               
        var divisiones = productText.split("-", 1);
        
        var sizet = productText.split("-", 3);
        
        $('input[name=sizename]').val(sizet[2]);
                               
        $('input[name=qty_pack]').val(divisiones);     
         
         grabaFecha();
     }    


    function boxQuantityChange(id, cartid, main_tr, with_a) {
        
        var selected_text = $("#request_qty_box_" + main_tr + " option:selected").text();
        var selected_val  = $("#request_qty_box_" + main_tr + " option:selected").val();       
                
	var nbox = selected_val.split('-');
        var tbox = $.trim(selected_text).split(" ");
                
	$('input[name=boxcant]').val(nbox[0]);
                
        $('input[name=boxtypen]').val(tbox[1]);
                
        $('input[name=product_su]').val(selected_val);
        $(".cls_hidden_selected_qty").val(selected_val);
                                 
    }    
    
    
    // opcion que funciona del calendario pot
    
    function checkOptionship() {
    
        var shippingMethod = '132';
        
        var fecha_fin = $('#cls_date_del').find("input").val();
        console.log(fecha_fin);
        
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/file/get_date_modal_cliship.php',
                data: 'shippingMethod=' + shippingMethod + '&fecha_fin=' + fecha_fin,
                success: function (data) {
                    $('.cls_date_start_dates').html(data);
                    $('#nextOpt').click();
                    _pickers();//show calendar
                }
            }); 
            
            checkOptiondel();
           // selectscategory();   // cambio final
    }
    
    function checkOptiondel() {
    
      //  var shippingMethod = '132';
        
        var fecha_ini = $('#cls_date_ship').find("input").val();
        console.log(fecha_ini);
        
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/file/get_date_modal_clidel.php',
                data: 'fecha_ini=' + fecha_ini,
                success: function (data) {
                    $('.cls_date_start_dated').html(data);
                    $('#nextOpt').click();
                    _pickers();//show calendar
                }
            }); 
            
           //  send_date();     // Graba la fecha
                   
           
    } 
     function send_date() {
        
        var delDate = $('#cls_date_ship').find("input").val();
                      
         $('input[name=sname]').val(delDate);     
         
         console.log(delDate);
                      
                $('#var_add').select2({
                    
                    ajax: {
                        url: "search_date_filter.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });        
     }   
     
    /*Button send request */
    function requestProduct(i) {
    
        var buyer     = '<?= $_SESSION["buyer"]; ?>'; // codigo  del  buyer
        var flag_s = true; 
        var dateRange = "";        
                        
        var order_val      = $('#order_add' + ' :selected').val();  
        

        
        var productId    = $('#var_add' + ' :selected').val();                     
        var productText  = $('#var_add' + ' :selected').text();  
        
        
        var productId_requ      = $('#productvar' + ' :selected').val();                     
        var productText_requ    = $('#productvar' + ' :selected').text();                                      
        

        
           //alert (productId_assign)  ;
           //alert (productId_requ)  ;    
           
                                     
        
        var box_quantity   = $('#qty_pack').val() ;         
        
        var type_box       = $('#box_type_'      + i + ' :selected').val();                        
        var type           = $('#type_req_'      + i).val();        
        var comment_pro    = $('#comment_'       + i).val();
        //var req_grow       = $('#grow_'          + i).val();                         
        var req_grow       = "ADD";
        
        var tag            = $('#cli_add').val();                               
        
        var date_ship      = $('#sname').val();           
        var date_del       = $('#dname').val();                                       
                        
        if (order_val != "") {
            $('#erMsg').hide();
        }else {
            flag_s = false;
            alert("Please select Order");            
            $('#erMsg').show();
        }
        
        
        if ($('#qty_pack').val() < 1) {
            $('#errormsg-1').html("Quantity error");
            flag_s = false;
        }   
        
        // Valida Cantidad
        var qytmax = $.trim(productText).split(" ");
                
        $('input[name=qty_ped]').val(qytmax[0]);   
        
        var qty_x = 0;        
        var qty_control = 0;
        
          qty_x = $('#qty_pack').val();        
         qty_control = $('#qty_ped').val();

                
        if (parseInt(qty_x) > parseInt(qty_control)) {
            $('#errormsg-1').html("Error Qty");
            flag_s = false;
        }      
        
        
        
        
    if (flag_s == true) {           
        console.log("flag_s "  + flag_s);
        console.log("order_val "   + order_val);  
        console.log("productId "   + productId);  
        
        $.ajax({
            type: 'post',
            url: 'https://app.freshlifefloral.com/buyer/request_product_ajax_subcliente.php',
            data: 'date_range=' + dateRange +
                   '&order_val=' + order_val + 
                   '&productId=' + productId +                    
              '&productId_requ=' + productId_requ +                                       
                '&box_quantity=' + box_quantity + 
                       '&buyer=' + buyer + 
                    '&type_box=' + type_box + 
                        '&type=' + type + 
                 '&comment_pro=' + comment_pro +                   
                 '&productText=' + productText +                   
            '&productText_requ=' + productText_requ +                                    
                         '&tag=' + tag +                          
                   '&date_ship=' + date_ship +                          
                    '&date_del=' + date_del +                          
                    '&req_grow=' + req_grow,       
            success: function (data_s) {
                
                if (data_s == 'true') {

                } else {
                    alert('There is some error. Please try again 1');
                }
                         //location.reload();
                         window.location.href = '<?php echo SITE_URL; ?>buyer/client_calendar_special.php';

            }
        });
    }

    }        
</script>     