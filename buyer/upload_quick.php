<?php

// PO 2018-08-24

$menuoff = 1;
$page_id = 421;
$message = 0;


$id_client  = $_GET['id_cli'];

//include("../config/config_new.php");

require_once("../config/config_gcp.php");

$subcli_q = $_GET['idi'];


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}

$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

$sel_cli = "select * from sub_client where id='" . $id_client . "'";
$rs_cli = mysqli_query($con, $sel_cli);
$cliente = mysqli_fetch_array($rs_cli);


$page_request = "buyer_invoices";
?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

<section id="middle">


    <!-- page title -->
    <header id="page-header">
        <h1>Invoice List</h1>
        <ol class="breadcrumb">
            <li><a href="#">Customer QuickBooks</a></li>
            <li class="active">Upload</li>
        </ol>
    </header>
    <!-- /page title -->


    <div id="content" class="padding-20">


        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>QUICKBOOKS </strong>                     
                </span>

                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-Confirmed-title="Confirmed" data-Confirmed-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul>
                <!-- /right options -->

            </div>

            <!-- panel content -->
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Customer</th>                                
                                <th>Now__Date</th>
                                <th>Due___Date</th>
                                <th>Terms</th>                                 
                                <th>Location</th>                                 
                                <th>Memo</th>                                  
                                <th>Item Prod/Serv</th>                                                                                                 
                                <th>Description</th>
                                <th>Qty</th>                                                                
                                <th>Rate</th>                                
                                <th>Amount</th>                                
                                <th>TCode</th>                                
                                <th>TaxAm</th>                                
                                <th>Curr.</th>                                                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                            
                            $sql = "  select irs.fact_quick + 4000 as invoice    ,
                                             cl.name as name_client ,
                                             irs.date_added             ,
                                             ADDDATE(irs.date_added, INTERVAL 15 DAY) as due_date        ,
                                             s.code_quick as Itemquick ,
                                             if(subcate_name='CARNATIONS',CONCAT(irs.prod_name,' ',irs.steams,' st/bu ',IFNULL(irs.box_name,'')),CONCAT(irs.prod_name,' ',irs.size,' cm ',irs.steams,' st/bu ',IFNULL(irs.box_name,''))) as description ,
                                            (irs.qty_pack) as quantity     ,  
                                             round(irs.price_quick,2) as rate ,
                                             round((irs.qty_pack*irs.price_quick),2) as amount  ,
                                             round((irs.qty_pack*irs.price_quick*0.05),4) as taxamount
                                        from invoice_requests_subcli irs
                                       INNER JOIN sub_client cl ON cl.id = irs.cliente_id
                                        left JOIN product p ON irs.prod_name = p.name and irs.product_subcategory = p.subcate_name
                                        left JOIN category c ON p.categoryid = c.id          
                                        left JOIN subcategory s ON p.subcategoryid = s.id and p.categoryid = s.cat_id
                                        left JOIN buyer_requests br ON irs.comment = br.id
                                        left JOIN features ft ON br.feature = ft.id
                                       where irs.buyer   = '".$userSessionID."'
                                         and irs.id_fact = '".$subcli_q."' 
                                       order by cl.name,s.code_quick,irs.prod_name    ";
                            
							$invoices_res = mysqli_query($con, $sql);
                                                        
							while ($invoice = mysqli_fetch_assoc($invoices_res)) { 
                                                            
                                                              $terms    = "Net 15";
                                                              $taxcode  = "GST";
                                                              $currency = "CAD";
                                                                                                                            
							?>
							<tr>

                                                                <td><?php echo $invoice['invoice']; ?></td>
                                                                <td><?php echo $invoice['name_client']; ?></td>
                                                                <td><?php echo $invoice['date_added']; ?></td>                                                              
                                                                <td><?php echo $invoice['due_date']; ?></td>                                                              
                                                                <td><?php echo $terms; ?></td>                                                                <td><?php echo $newDate; ?></td>                                                                                                                                
								<td> </td>                                                                
								<td><?php echo $invoice['Itemquick']; ?></td>                                                                 
                                                                <td><?php echo $invoice['description']; ?></td>
                                                                <td><?php echo $invoice['quantity']; ?></td>
                                                                <td><?php echo $invoice['rate']; ?></td>                                                                    
                                                                <td><?php echo $invoice['amount']; ?></td>
                                                                <td><?php echo $taxcode; ?></td>
                                                                <td><?php echo $invoice['taxamount']; ?></td>
                                                                <td><?php echo $currency; ?></td>

                                                                
							</tr>
							<?php
							$i++;
                            }
                            ?> 
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>
<?php require_once '../includes/footer_new.php'; ?>
