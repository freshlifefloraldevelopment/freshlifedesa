<?php

$factId = $_GET["idi"];

require_once("../config/config_gcp.php");


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);
$page_request = "buyer_invoices";
?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

<section id="middle">


    <!-- page title -->
    <header id="page-header">
        <h1>List of documents</h1>
        <ol class="breadcrumb">
            <li><a href="#">-</a></li>
            <li class="active">-</li>
        </ol>
    </header>
    <!-- /page title -->


    <div id="content" class="padding-20">


        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>Available Downloads</strong> <!-- panel title -->
                </span>


            </div>

            <!-- panel content -->
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Document</th>
                                <th>Description </th>
                                <th> </th>
                                <th>View</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = "select id , id_fact , document_path , description
                                      from invoice_document
                                     where id_fact = '".$factId."' ";

				$invoices_res = mysqli_query($con, $sql);

                                $ii=0;

				while ($invoice = mysqli_fetch_assoc($invoices_res)) {

                                    $filedoc = substr($invoice['document_path'],4,22);
                                    $ii++;

                                    if ($invoice['description'] == 1) {
                                        $typedesc = 'Farm Invoice';
                                    }elseif ($invoice['description'] == 2) {
                                        $typedesc = 'Commercial Invoice';
                                    }elseif ($invoice['description'] == 3) {
                                        $typedesc = 'Master Airwaybill';
                                    }elseif ($invoice['description'] == 4) {
                                        $typedesc = 'House Airwaybill';
                                    }

			    ?>
							<tr>
                                                                <td><?php echo $ii; ?></td>
                                                                <td><?php echo $factId; ?></td>
								                                                <td><?php echo $typedesc; ?></td>
                                                                <!-- <td><?php //echo $filedoc; ?></td> -->
      <td><embed src="/<?php echo $invoice['document_path']; ?>" type="application/pdf" width="100%" height="400px" /><td>
      <!--  <td><a title="Visualizar Archivo" href="/<?php //echo $invoice['document_path']; ?>" download="<?php //echo $filedoc; ?>" style="color: blue; font-size:18px;"> <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> </a></td> -->
      <!--  <td><a title="Descargar Archivo" href="/<?php //echo $invoice['document_path']; ?>" download="<?php //echo $filedoc; ?>" style="color: blue; font-size:18px;"> <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> </a></td> -->
      <td><a title="Eliminar Archivo" href="Eliminar.php?name=/var/www/html/<?php echo $invoice['document_path']; ?>" style="color: red; font-size:18px;" onclick="return confirm('Are you sure, you want to delete this Document ?');"> <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> </a></td>

							</tr>
							<?php

                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>
<?php require_once '../includes/footer_new.php'; ?>
