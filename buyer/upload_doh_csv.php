<?php 

require_once("../config/config_gcp.php");

$userSessionID = $_SESSION["buyer"];
$delNow = date('Y-m-d');

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

//update profile data
    $delete1 = "delete from request_tmp ";
    mysqli_query($con,$delete1);
    

if ($_FILES["sel_file"]["name"]!="") {
    
    //get the csv file
    $file     = $_FILES["sel_file"]["tmp_name"];
    $handle   = fopen($file,"r");
    $k        = 0;
    $message  = 0;
    $insCount = 0;
    $notfound = 0;

    do {
	
	if($k>=1)   {
            
            if ($data[0] && $data[1] && $data[2] && $data[3] ) {		
		
                   $var = strtolower($data[0]);
		   $siz = strtolower($data[1]);
                   $ste = strtolower($data[2]);
		   $gro = strtolower($data[3]);
		   
		   $var = trim($var);
		   $siz = trim($siz);
		   $ste = trim($ste);
		   $gro = trim($gro);                   
                   
                   
		   	   		   	  
			$insert="insert into request_tmp set 
                                           variety     ='".$var."',
                                           size        ='".$siz."',
                                           stems       ='".$ste."',
                                           growers     ='".$gro."',   
                                           date_added  ='".$delNow."'  ";
                        
			mysqli_query($con,$insert);				 			  
			  
			$insCount=$insCount+1;	
                        
			$datai[$insCount]["var"] = $data[0] ;
			$datai[$insCount]["siz"] = $data[1] ;
			$datai[$insCount]["ste"] = $data[2] ;
			$datai[$insCount]["gro"] = $data[3] ;                                                                                                                                                                                                   
            }		
		$message=1;
	}

        
        
		$k=$k+1;
    } while ($data = fgetcsv($handle,1000,",","'"));

}


?>
<script>
   
	function verify(){ 
		var arrTmp=new Array();
		arrTmp[5]=checkimage();		
		var i;

		_blk=true;

		for(i=0;i<arrTmp.length;i++){

			if(arrTmp[i]==false){
			   _blk=false;
			}
		}

		if(_blk==true)	{
			return true;
		}else{
			return false;
		}	
 	}	

	function trim(str) {    
		if (str != null) {        
			var i;        

			for (i=0; i<str.length; i++) {           

				if (str.charAt(i)!=" ") {               
					str=str.substring(i,str.length);                 
					break;            
				}        
			}            
			for (i=str.length-1; i>=0; i--)	{            

				if (str.charAt(i)!=" ") {                
					str=str.substring(0,i+1);                
					break;            
				}         
			}                 

			if (str.charAt(0)==" ") {            
				return "";         
			} else 	{            
				return str;         
			}    
		}
	}
	
	function checkimage()	{
		if(trim(document.form1.sel_file.value) == "")	{	 
			document.getElementById("lblsel_file").innerHTML="Please upload csv file";
			return false;
		}else {
			if(!validImageFile(document.form1.sel_file.value)){
				document.getElementById("lblsel_file").innerHTML="Please select csv file";
				return false;
			}else{
				document.getElementById("lblsel_file").innerHTML="";
				return true;
			}
		}
	}	
	
	function validImageFile(strfile){

		var str = strfile;
		var pathLenth = strfile.length;
		var start = (str.lastIndexOf("."));
		var fileType = str.slice(start,pathLenth);

		fileType = fileType.toLowerCase();

		if (strfile.length > 0)	{

		   if((fileType == ".csv"))   {
				return true;
		   }else {
				return false;
		   } 
		}
	}
</script>
<?php require_once '../includes/profile-header.php'; ?>
<?php require_once "../includes/left_sidebar_buyer.php"; ?>
<!-- /ASIDE -->

<!--
            MIDDLE
        -->
<section id="middle">
    <div id="content" class="padding-20">

        <div class="page-profile">
            <?php
            if ($update_msg) {
                echo '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>' . $update_msg . '</div>';
            } elseif ($update_error) {
                echo '<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">x</a>' . $update_error . '</div>';
            }
            ?>
            <div class="row">



                <!-- COL 2 -->
                <div class="col-md-12 col-lg-10">

                    <div class="tabs white nomargin-top">
                        <ul class="nav nav-tabs tabs-primary">

                            <li class="active">
                                <a href="#edit" data-toggle="tab">Upload</a>
                            </li>
                        </ul>

                        <div class="tab-content">


                            <?php 
                            $first = $name1[0];
                            $last_name = $name2[1]; ?>
                            <!-- Edit -->
                            <div id="edit" class="active tab-pane">

                                <form class="form-horizontal" id="profile_form" name="profile_form" method="post" enctype="multipart/form-data" onsubmit="javascript: return validate_profile_form(profile_form);">
                                    <h4>Invoice</h4>
                 <table width="100%">
                  <tr height="24">
                    <td width="17%"  align="right" class="text">&nbsp;&nbsp;&nbsp;File (CSV): </td>
                    <td width="83%" bgcolor="#f2f2f2" class="text"><input type="file" name="sel_file" id="sel_file" />&nbsp;&nbsp;&nbsp;<input name="Submit" type="submit" class="buttongrey" value="Upload" /><br /><span id="lblsel_file" class="error"></span></td>
                  </tr>
                  
                  <tr>
                    <td>&nbsp;</td>
                    <td>                    </td>
                  </tr>
                  
                  <?php if($message==1) { ?>
				  
		            <tr>
                                <td width="100%" colspan="2" style="color: #006600;; font-size:14px; font-weight:bold;">
                                    <?php 
                                            if($insCount>=1) { 
                                                echo "Following ".$insCount." Products have been inseterd successfully.";                                     
                                            } 
                                    
                                    ?></td>
                            </tr>
                  
                        <tr>
                          <td width="100%" colspan="2" style="color: #006600;; font-size:14px; font-weight:bold;">
                            <table width="100%">
                            <tr><th width="150px" align="left" style="color:#000000;">Buyer</th><th width="150px" align="left" style="color:#000000;"><a href="all-records-req.php">View All Records</a></th></tr>
                    
                            <?php 
                                    for($j=1;$j<=$notfound;$j++) { 
                            ?>
                    
                                        <tr>
                                            <td><?php echo $datai[$j]["product"]?></td><td><?php echo $datai[$j]["subcategory"]?></td>
                                        </tr>
                      
                            <?php } ?>
                            </table>
                         </td>
                        </tr>
				  
		 <?php  }  ?>
                  
                  
        <?php if($notfound>=1) { ?>
		        <tr>
                                <td>&nbsp; </td>
                        </tr>
                     
		        <tr>
                                <td width="100%" colspan="2" style="color: #FF0000;; font-size:14px; font-weight:bold;"><?php if($notfound>=1) { echo "Following ".$notfound." Products Have Been Not Found Product List."; } ?></td>
                        </tr>
                  
                  <tr>
                            <td width="100%" colspan="2" style="color: #FF0000;; font-size:14px; font-weight:bold;">
                            <table width="100%">
                                <tr><th width="150px" align="left" style="color:#000000;">Buyer</th><th width="150px" align="left" style="color:#000000;">Buyer</th></tr>
                                <?php 
                                for($j=1;$j<=$notfound;$j++) { 
                                ?>                   
                                            <tr>
                                                <td><?php echo $datam[$j]["product"]?></td><td><?php echo $datam[$j]["subcategory"]?></td>
                                            </tr>                      
                                <?php }	?>
                                
                            </table>
                    </td>
                  </tr>
				  
		<?php	  }  ?>
				  
                 </table>

                                </form>

                            </div>
                        </div>
                    </div>

                </div><!-- /COL 2 -->                

            </div>

        </div>

    </div>
</section>
<?php require_once("../includes/profile-footer.php"); ?>
