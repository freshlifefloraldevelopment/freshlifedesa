<?php

require_once("../config/config_gcp.php");

// Inicio 

$orderid        = $_GET['fac_id'];
$clientid       = $_GET['id_cli'];

$grower_id      = $_POST['grower'];
$buyer_ofer     = $_POST['buyer'];
$today          = date('Y-m-d');

// MARCAR LAS ACTUALIZACIONES

// DESCONTAR LAS RESERVAS DE LA OFERTA DEL REQUEST PARA LA LISTA DE VALORES

$qrt_clisub  = "select br.date_ship , br.date_del , br.id_order , 
                       br.id_client ,br.product ,
                       sc.name as name_cli, 'Request' as marks  ,
                       bo.qucik_desc , br.qty , br.id as reqid  ,
                       br.assign , br.qty as qyt_reser
                  from reser_requests br
                 inner JOIN buyer_orders bo ON br.id_order = bo.id
                 inner JOIN sub_client sc ON IFNULL(br.id_client,0) = sc.id
                 where br.id_order  = '" . $orderid  . "'
                   and br.id_client = '" . $clientid . "'
                   and br.comment like 'SubClient%'                                            
                   and br.assign = 0
                 order by br.product  ";

$rs_clisub   = mysqli_query($con, $qrt_clisub);


$ii=1;

while ($reserva = mysqli_fetch_array($rs_clisub)) {
    
    $prod_res = $reserva['product'];    
    
              $qrt_pack  = "select  ipb.id as idpck         , ipb.id_fact             , ipb.id_order            , ipb.order_number        , ipb.order_serial        ,
                                    ipb.offer_id            , ipb.product             , ipb.prod_name           , ipb.buyer               , ipb.grower_id           ,
                                    ipb.box_qty_pack        , ipb.qty_pack            , ipb.qty_box_packing     , ipb.box_type            , ipb.comment             ,
                                    ipb.date_added          , ipb.box_name            , ipb.size                , ipb.steams              , ipb.price               ,
                                    ipb.cliente_id          , ipb.ship_cost           , ipb.cost_cad            , ipb.price_cad           , ipb.gorPrice            ,
                                    ipb.duties              , ipb.handling_pro        , ipb.total_duties        , ipb.processed           , ipb.branch              ,
                                    ipb.price_quick         , ipb.weight              , ipb.id_refer            , ipb.product_subcategory , ipb.assign              
                                from invoice_packing_box ipb
                               where ipb.id_fact = '" . $orderid . "'
                                 and ipb.product = '" . $prod_res . "'
                                 and ipb.cliente_id in (0,47)
                                 and assign  = 0
                               order by ipb.prod_name limit 0,1  ";

                $rs_pack   = mysqli_query($con, $qrt_pack);

                while ($asigncli = mysqli_fetch_array($rs_pack)) {
    
                                
                            if ( $asigncli['qty_pack']== $reserva['qyt_reser'] )  {                                                                
                                  // Update igual cantidad                                
                 
                                  $update_equ = "update invoice_packing_box 
                                                    set cliente_id = '" . $reserva['id_client'] . "'  
                                                  where id         = '" . $asigncli['idpck'] . "'   ";
                                
                                              mysqli_query($con, $update_equ);                                
                                
                            }else if ($asigncli['qty_pack']> $reserva['qyt_reser']){
                                

                                   $qtyin = $asigncli['qty_pack']-$reserva['qyt_reser'];
                                   $qtyup = $reserva['qyt_reser'];
                                   
                                  $update_dis = "update invoice_packing_box 
                                                    set qty_pack   = '" . $qtyup . "' ,
                                                        cliente_id = '" . $reserva['id_client'] . "'  
                                                  where id       = '" . $asigncli['idpck'] . "'   ";
                                
                                              mysqli_query($con, $update_dis);                                                                   
                                   
   
                                        $id_fact             = $asigncli['id_fact'];  
                                        $id_order            = $asigncli['id_order'];    
                                        $order_number        = $asigncli['order_number'];    
                                        $order_serial        = $asigncli['order_serial'];  
                                        $offer_id            = $asigncli['offer_id'];    
                                        $product             = $asigncli['product'];    
                                        $prod_name           = $asigncli['prod_name'];    
                                        $buyer               = $asigncli['buyer'];    
                                        $grower_id           = $asigncli['grower_id'];  
                                        $box_qty_pack        = $asigncli['box_qty_pack'];    
                                        $qty_pack            = $qtyin;
                                        $qty_box_packing     = $asigncli['qty_box_packing'];    
                                        $box_type            = $asigncli['box_type'];    
                                        $comment             = 'Asignacion-Requests'; 
                                        
                                        $date_added          = $today;    
                                        $box_name            = $asigncli['box_name'];    
                                        $size                = $asigncli['size'];    
                                        $steams              = $asigncli['steams'];    
                                        $price               = $asigncli['price']; 
                                        
                                        $cliente_id          = 0;    
                                        $ship_cost           = $asigncli['ship_cost'];    
                                        $cost_cad            = $asigncli['cost_cad'];    
                                        $price_cad           = $asigncli['price_cad'];    
                                        $gorPrice            = $asigncli['gorPrice'];
                                        
                                        $duties              = $asigncli['duties'];    
                                        $handling_pro        = $asigncli['handling_pro'];    
                                        $total_duties        = $asigncli['total_duties'];    
                                        $processed           = $asigncli['processed'];    
                                        $branch              = 0;  
                                        
                                        $price_quick         = $asigncli['price_quick'];    
                                        $weight              = $asigncli['weight'];    
                                        $id_refer            = $asigncli['id_refer'];    
                                        $product_subcategory = $asigncli['product_subcategory'];    
                                        $assign              = 0;  

                                   
            $insert_reser = "INSERT INTO invoice_packing_box
                          (id_fact                   ,  id_order                    ,  order_number              ,  order_serial                ,
                           offer_id                  ,  product                     ,  prod_name                 ,  buyer                       ,  grower_id           ,                           
                           box_qty_pack              ,  qty_pack                    ,  qty_box_packing           ,  box_type                    ,  comment             ,
                           date_added                ,  box_name                    ,  size                      ,  steams                      ,  price               ,                           
                           cliente_id                ,  ship_cost                   ,  cost_cad                  ,  price_cad                   ,  gorPrice            ,
                           duties                    ,  handling_pro                ,  total_duties              ,  processed                   ,  branch              ,
                           price_quick               ,  weight                      ,  id_refer                  ,  product_subcategory         ,  assign   )
                VALUES ('" . $id_fact . "'           , '" . $id_order . "'          ,  '" . $order_number . "'   ,'" . $order_serial . "'       ,
                        '" . $offer_id . "'          , '" . $product . "'           , '" . $prod_name . "'       ,'" . $buyer . "'              ,'" . $grower_id . "'       ,                            
                        '" . $box_qty_pack . "'      , '" . $qty_pack . "'          , '" . $qty_box_packing . "' ,'" . $box_type . "'           ,'" . $comment . "',
                        '" . $date_added . "'        , '" . $box_name . "'          , '" . $size . "'            ,'" . $steams . "'             ,'" . $price . "'        ,                            
                        '" . $cliente_id . "'        , '" . $ship_cost . "'         , '" . $cost_cad . "'        ,'" . $price_cad . "'          ,'" . $gorPrice . "'     ,
                        '" . $duties . "'            , '" . $handling_pro . "'      , '" . $total_duties . "'    ,'" . $processed . "'          ,'" . $branch . "',                           
                        '" . $price_quick . "'       , '" . $weight . "'            ,'" . $id_refer . "'         ,'" . $product_subcategory . "','" . $assign . "')";

            mysqli_query($con, $insert_reser);                                   
                    
                            }
    
                     
                                  $update_pck = "update invoice_packing_box 
                                                    set assign = '10'  
                                                  where id     = '" . $asigncli['idpck'] . "'
                                                    and assign = 0   ";
                                
                                              mysqli_query($con, $update_pck);

                 }    

    
                  $update_reser = "update reser_requests 
                                       set assign = '10'  ,
                                           total_reser = total_reser + '" . $reserva['qyt_reser'] . "'
                                     where id     = '" . $reserva['reqid'] . "'
                                       and assign = 0   ";

              mysqli_query($con, $update_reser);
}


/*


 
 */





		 header("location:client_calendar_special.php");

?>