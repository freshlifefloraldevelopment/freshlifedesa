<?php

require_once("../config/config_gcp.php");


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}

$userSessionID = $_SESSION["buyer"];
$cliente_inv   = $_GET['id_cli'];
$swcab         = $_GET['sw'];
$idfac         = $_GET['fac_id'];

$idcomm        = $_GET['icomm'];
$loteid        = $_GET['id_lote'];

	//if(isset($_GET['delete'])) 	{
	//  $querydel = 'DELETE FROM reser_requests WHERE  id= '.(int)$_GET['delete'];
	//  mysqli_query($con,$querydel);	
	//}


   $sel_cli = "select br.date_ship , br.date_del , br.id_order , br.id_client ,
                      sc.name as name_cli, 'Request' as marks  ,
                      bo.qucik_desc
                 from reser_requests br
                inner JOIN buyer_orders bo ON br.id_order = bo.id
                inner JOIN sub_client sc ON IFNULL(br.id_client,0) = sc.id
                where br.id_order  = '".$idfac."'
                  and br.id_client = '".$cliente_inv."'                     
                  and br.lote      = '".$loteid."'                         
                  and br.comment   like 'SubClient%'                                            
                group by br.id_order
                union
               select br2.date_ship , br2.date_del , br2.id_order , br2.id_client ,
                      sc.name as name_cli, 'Request' as marks  ,
                      bo.qucik_desc
                 from buyer_requests br2
                inner JOIN buyer_orders bo ON br2.id_order = bo.id
                inner JOIN sub_client sc ON IFNULL(br2.id_client,0) = sc.id
                where br2.id_order  = '".$idfac."'
                  and br2.id_client = '".$cliente_inv."'                     
                  and br2.comment   like 'SubClient%'                                            
                group by br2.id_order";        

   
   $rs_cli = mysqli_query($con, $sel_cli);
   
   $info = mysqli_fetch_array($rs_cli);

   $inicio = $info['date_ship'];

/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,company_role,country,state_text,city,zip,shipping_country,shipping_state_text,shipping_city,
shipping_zip,coordination,address,address2,shipping_address,shipping_address2,name_on_card,credit_card_type,credit_card,cvv,expiration_month,
expiration_year,is_public,biographical_info,profile_image FROM buyers WHERE id =$userSessionID")
) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $company_role, $country, $state_text, $city, $zip, $shipping_country, $shipping_state_text, $shipping_city, $shipping_zip, $coordination, $address, $address2, $shipping_address, $shipping_address2, $name_on_card, $credit_card_type, $credit_card, $cvv, $expiration_month, $expiration_year, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    echo "else";
    exit();
    header("location:" . SITE_URL);
    die;
}


//  Actualizacion en Grupo

if (isset($_REQUEST["total"])) {
    
    for ($i = 1; $i <= $_POST["totalrow"] - 1; $i++) {
              
            $update = "update reser_requests 
                          set num_box = '" . $_POST["boxp-". $_POST["pro-" . $i]] . "' ,
                              price   = '" . $_POST["price-". $_POST["pro-" . $i]] . "'
                        where id      = '" . $_POST["pro-" . $i] . "'  ";                               
    
            mysqli_query($con, $update);                                                            
    }    
  }    
      
      
$img_url = '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}
$page_request = "dashboard";
?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

    <!--script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script-->

    
<style type="text/css">
    .comment-author .g_title {
        color: #337ab7;
        font-weight: normal;
    }
</style>
<style type="text/css">
    .comment-body.cls_comment_body {
        padding-left: 65px;
    }

    li.grower-comments .comment-body {
        padding-left: 0px;
    }

    .status_modal.in li.comment .comment-body {
        margin-left: 60px;
    }
</style>

<?php         
    
    $sqlDetalis="select irs.id as gid      , irs.id_order         , irs.order_serial   , irs.cod_order      , irs.product        ,
                        irs.sizeid         , irs.feature          , irs.noofstems      , irs.qty as qty_pack  , irs.buyer          ,
                        irs.boxtype        , irs.date_added       , irs.type           , irs.bunches        , irs.box_name       ,
                        irs.lfd            , irs.lfd2             , irs.comment        , irs.box_id         , irs.shpping_method ,
                        irs.isy            , irs.mreject          , irs.bunch_size     , irs.unseen         , irs.req_qty        ,
                        irs.bunches2       , irs.discount         , irs.inventary      , irs.type_price     , irs.id_client      ,
                        irs.id_grower      , irs.special_order    , irs.price          , irs.num_box        , irs.date_ship      ,
                        irs.date_del       , irs.cost             , irs.id_reser       , irs.assign         ,
                        s.price_client     , s.name as psubcatego , p.name as name_product ,
                        cli.name as subclient,  
                        sz.name as size_name , c.name as colorname
                  from reser_requests irs
                 INNER JOIN product p on irs.product = p.id 
                 INNER join subcategory s on p.subcategoryid = s.id 
                 INNER JOIN sub_client cli on irs.id_client = cli.id 
                 INNER JOIN sizes sz on irs.sizeid = sz.id 
                  left JOIN colors c on p.color_id = c.id 
                 where irs.id_order   = '".$idfac."' 
                   and irs.id_client  = '".$cliente_inv."' 
                   and irs.lote       = '".$loteid."'     
                   union
                 select byr.id as gid      , byr.id_order         , byr.order_serial   , byr.cod_order      , byr.product        ,
                        byr.sizeid         , byr.feature          , byr.noofstems      , byr.qty as qty_pack  , byr.buyer          ,
                        byr.boxtype        , byr.date_added       , byr.type           , byr.bunches        , byr.box_name       ,
                        byr.lfd            , byr.lfd2             , byr.comment        , byr.box_id         , byr.shpping_method ,
                        byr.isy            , byr.mreject          , byr.bunch_size     , byr.unseen         , byr.req_qty        ,
                        byr.bunches2       , byr.discount         , byr.inventary      , byr.type_price     , byr.id_client      ,
                        byr.id_grower      , byr.special_order    , byr.price          , byr.num_box        , byr.date_ship      ,
                        byr.date_del       , byr.cost             , byr.id_reser       , byr.assign         ,
                        s.price_client     , s.name as psubcatego , p.name as name_product ,
                        cli.name as subclient,  
                        sz.name as size_name , c.name as colorname
                  from buyer_requests byr
                 INNER JOIN product p on byr.product = p.id 
                 INNER join subcategory s on p.subcategoryid = s.id 
                 INNER JOIN sub_client cli on byr.id_client = cli.id 
                 INNER JOIN sizes sz on byr.sizeid = sz.id 
                  left JOIN colors c on p.color_id = c.id 
                 where byr.id_order   = '".$idfac."' 
                   and byr.id_client  = '".$cliente_inv."' 
                   and byr.comment like 'SubClie%'
                 order by psubcatego    ";

    $result = mysqli_query($con, $sqlDetalis);  



 ?>
<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
 
</head>
<section id="middle">
            <!-- panel content inicio po -->            
            <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">
			<!--section id="middle"-->

				<!-- page title -->
				<header id="page-header">
					<h1>Customer Order (Open Market)</h1>
					<ol class="breadcrumb">
						<li><a href="#">SubClient</a></li>
						<li class="active">Assign</li>
					</ol>
				</header>
				<!-- /page title -->


				<div id="content" class="padding-20">

					<div class="panel panel-default">
            <div class="panel-heading">

                 <a href="<?php echo SITE_URL; ?>buyer/client_calendar_special.php" class="btn btn-success btn-xs relative">Back </a>                
                                 
                 
                 
                <!-- right options -->
                <ul class="options pull-right list-inline">
					<ul class="list-unstyled">
                                                   <li> <strong>Profit: </strong> <?php echo "$".number_format($totalCal, 2, '.', ',');  ?>                                                                            </li>                                                                                                                                  
					</ul>  
                </ul>
                <!-- /right options -->
                
                                                        <!--Price Modal Start-->
                                                        <div class="modal fade price_modal"  role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
                                                            
                                                            <div class="modal-dialog modal-lg">
                                                                
                                                                <div class="modal-content">                                                                                                                                        
                                                                                                                                                                                                                                                                                                                                                   
                                                                    
                                                                    
                                                                    <div class="modal-body request_product_modal_hide">

                                                                        </label>

                                                                        <!--<br>-->
                                                                        <!--</div>-->
                                                                        <div class="row margin-bottom-10">
                                                                            <div class="col-md-12">
                                                                                <h4 style="clear: both;margin-top: 20px;margin-left: 14px;margin-bottom: 0px;">Select Product</h4>
                                                                                <div class="product_price_add2"></div>
                                                                                <span id="add_product_section1" style="display: inherit!important;">
                                                                                    
                                                                                    
                                                                                    <!--Select  Box Type (2) -->
                                                                                    <div class="col-md-6 margin-top-10">

                                                                                        <div class="fancy-form fancy-form-select">
                                                                                        <select style="width: 100%; diplay: none;" name="filter_category" id="box_type_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value="">Select product</option>                                                                                            
                                                                                            <?php
                                                                                            $sql_unit = "select * from  units where id = 1000";
                                                                                            $result_units = mysqli_query($con, $sql_unit);
                                                                                            while ($row_category = mysqli_fetch_assoc($result_units)) { ?>
                                                                                                <option value="<?php echo $row_category['id']; ?>"><?= $row_category['descrip']; ?></option>
                                                                                            <?php }
                                                                                            ?>
                                                                                        </select>
                                                                                        <i class="fancy-arrow"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                    
                                                                                     <!--Select  Order (3) -->
                                                                                    <div class="col-md-6 margin-top-10">
                                                                                        

                                                                                        <div class="fancy-form fancy-form-select">
                                                                                            <select style="width: 100%; diplay: none;" name="filter_order" id="box_order_<?php echo $ir; ?>" class="form-control select2 fancy-form-select" tabindex="-1"    >
                                                                                            <option value="">Select order</option>
                                                                                                <?php
                                                                                                $category_sql = "select  id,qucik_desc  from  buyer_orders where  buyer_id='" . $userSessionID . "' and  del_date >= '" . date("Y-m-d") . "' and is_pending=11";
                                                                                                $result_category = mysqli_query($con, $category_sql);
                                                                                                while ($row_category = mysqli_fetch_assoc($result_category)) { ?>
                                                                                                    <option value="<?php echo $row_category['id']; ?>"><?= $row_category['qucik_desc']; ?></option>
                                                                                                <?php }
                                                                                                ?>
                                                                                        </select>
                                                                                            
                                                                                            
                                                                                        <i class="fancy-arrow"></i>
                                                                                             <input type="hidden" id="ship" name="ship" value="">
                                                                                             <div id="erMsg" style="display:none;color:red;">Please select Order.</div>
                                                                                        </div>
                                                                                    </div>
                                                                                                                                                                        

                                                                        
                                                                            </div>
                                                                        </div>
                                                                                                                                                                                                                                                                                                
                                                                        <!-- Comment (6) -->
                                                                        <div class="col-md-12">
                                                                            <hr>
                                                                            <form class="validate" action="" method="post" enctype="multipart/form-data" novalidate="novalidate">
                                                                                <fieldset>
                                                                                    <div class="row">
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-12 col-sm-12">
                                                                                                <label>Comment</label>
                                                                                                <textarea id="comment_<?php echo $ir; ?>" name="contact[experience]" rows="4" class="form-control required"></textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </fieldset>
                                                                                <!--input name="is_ajax" value="true" type="hidden"></form-->                                                                                                                                                                 
                                                                                                                                                                
                                                                     </div>                                                                                                                                                
                                                                        
                                                                        
                                                                    </div>
                                                                    
                                                                    <hr>
                                                                    <div class="modal-footer request_product_modal_hide_footer">
                                                                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                                                                        <button style="background:#8a2b83!important;" onclick="requestProduct_xc('<?php echo $ir ?>')" class="btn btn-primary" type="button">Save</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--Price Modal End-->
                
                
            </div>                                            

						<div class="panel-body">

							<div class="row">                                                                             
								<div class="col-md-6 col-sm-6 text-left">

								
								<ul class="list-unstyled">
                                                                    
								    <li><strong>Client..............:</strong>
                                                                           <?php  echo $info['name_cli'];  ?>	                                                                                                        
                                                                    </li>
                                                                    
                                                                    <li><strong>Delivery Date.:</strong>
                                                                            <?php  echo $info['date_del'];  ?>	                            
                                                                    </li>     
                                                                    
                                                                    <li><strong>Order...............:</strong>
                                                                            <?php  echo $info['id_order']." ".$info['qucik_desc'];  ?>	                                                                                                                                              
                                                                    
                            <!--li><strong>Delivery Date.:</strong>
                                    <label class="field cls_date_start_dated" id="cls_date_del" name="cls_date_del">
                                                <input  class="form-control required start_date" placeholder="Select Date Delivery" style="width: 250px!important;text-indent: 32px;border-radius: 5px;" type="text" value="<?php echo $info['date_del'];?>" >
                                    </label>                            
                            </li-->  
                            

                                    
                                         <input type="hidden" name="sname" id="sname" value="<?php echo $_POST["sname"];?>" /> 
                                         
                                         
                                                                        <li><strong>Assign............:</strong>
                                                                            
                                                                        <?php   $sel_subcategory="select br.id as idbr     , br.lfd        ,br.product as idcod  ,  p.id as codvar,
                                                                                                    gor.id as idgor   , gor.offer_id  , gor.offer_id_index , gor.grower_id   , gor.buyer_id , 
                                                                                                    gor. product as  productname , 
                                                                                                    gor.product_subcategory, gor.size as size_name  , (gor.bunchqty-gor.reserve) as  bunchqty  , gor.steams ,
                                                                                                    g.growers_name , br.feature, f.name as featurename,
                                                                                                    br.id_order, cl.name as colorname, res.id_client as control
                                                                                              from buyer_requests br
                                                                                                    inner join grower_offer_reply gor on gor.offer_id = br.id
                                                                                                    inner join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name 
                                                                                                    left join growers g on gor.grower_id = g.id
                                                                                                    left join colors cl on p.color_id = cl.id
                                                                                                    left join features f on br.feature = f.id 
                                                                                                    left JOIN buyer_requests res ON gor.request_id = res.id
                                                                                                where g.active = 'active' 
                                                                                                  and br.id_order  = '".$info['id_order']."' 
                                                                                                  and (gor.bunchqty-gor.reserve) > 0   
                                                                                                  and (res.id_client = 0 or res.id_client is null)";
                                                                                  
                                                                        //and res.id_client = 0
                                                                                            $res_subcategory=mysqli_query($con,$sel_subcategory);
                                                                        ?>	                                                                                                        
                                                                                                                                                                                                        
                                                                            <select name="var_add" id="var_add"   style="margin-top:10px; height:35px; padding:3px; width:450px;border-radius: 5px;" class="listmenu" >
                                                                                    <option value="">-- Select Variety --</option>
                                                                                    
                                                                            <?php				       
                                                                               		while($rw_subcategory=mysqli_fetch_array($res_subcategory))	{
                                                                            ?>                       
                                                                                     <option value="<?php echo $rw_subcategory["codvar"];?>"><?php echo $rw_subcategory["bunchqty"]."  ".$rw_subcategory["product_subcategory"]." ".$rw_subcategory["productname"]." ".$rw_subcategory["colorname"]."-*-".$rw_subcategory["size_name"]."-cm ".$rw_subcategory["steams"]."St/Bu-".$rw_subcategory["idgor"]."-".$rw_subcategory["featurename"]."-".$rw_subcategory["growers_name"];?></option>
                                                                            <?php    }	?>		                                                                                    
                                                                                    
                                                                            </select>

                                                                            <!--button style="background:#8a2b83!important;" onclick="selectscategory()" class="btn btn-success btn-xs relative" type="button">View Varieties</button-->                                                                                             
                                                                        </li>
                                         
                                    
                                                                        
                                                                        <li><strong>Request.........:</strong>
                                                                                                                                                                                                        
                                                                                <!--Product-->
                                                                                                            
                                                                            <select class="listmenu"  name="productvar"  id="productvar" style="margin-top:10px; height:35px; padding:3px; width:450px;border-radius: 5px;">
                                                                                    <option value=""> -- Select Variety -- </option>
                                                                            </select>                                                                                                            
                                                                        </li>
                                                                                                                                                                                                                                            
									<li><strong>Bunch............:</strong> 
                                                                            <input type="number" name="qty_pack" id="qty_pack"  style="margin-top:10px; height:35px; padding:3px; width:250px;border-radius: 5px;" value="<?php echo $_POST["qty_pack"];?>" />                                                
                                                                        </li>
                                                                        
                                                                            <input type="hidden" name="qty_ped" id="qty_ped"  style="margin-top:10px; height:35px; padding:3px; width:250px;border-radius: 5px;" value="<?php echo $_POST["qty_ped"];?>" />                                                
                                                                        

								</ul>
                                                                    
								</div>
                                                            
                                                            
								<div class="col-md-6 col-sm-6 text-right">
                                                                    <ul class="list-unstyled">

                                                                     <li><strong> </strong>  
                                                                         
                                                                                 <div class="error-box">
                                                                                        <span id="errormsg-" style="color: white;"></span>
                                                                                 </div>
                                                                         
                                                                            
                                                                            <img id="my_image" src="https://app.freshlifefloral.com//includes/assets/images/logo.png" width="200">
                                                                         
                                                                            
                                                                              <br>   
                                                                            <button style="background:#8a2b83!important;" onclick="imagen()" class="btn btn-success btn-xs relative" type="button">View Asign</button>                                                                                                                                                                         
                                                                              <br>
                                                                            <button style="background:#8a2b83!important;" onclick="imagen_req()" class="btn btn-success btn-xs relative" type="button">View Request</button>    
                                                                            <br>
                                                                            <input type="hidden" name="imgpath" id="imgpath" value="<?php echo $_POST["imgpath"];?>" />     
                                                                            
                                                                            <div class="error-box">
                                                                                    <span id="errormsg-1" style="color:#FF0000; font-size:14px; font-weight:bold; font-family:arial; display:block; clear:both;"></span>
                                                                            </div>                                                                            
                                                                     </li>
                                                                    
                                                                    </ul>
								</div>                                                            
                                                            


							</div>

							<div class="table-responsive">
								<table class="table table-condensed nomargin">
									<thead>
										<tr>
											<th>Delivery</th>                                                                                                                                                                            
											<th>Variety</th>
                                                                                        <th>Size</th>
                                                                                        <th>Bun</th>
                                                                                        <th>Offer</th>
                                                                                        <th>Cost</th>
                                                                                        
                                                                                        <!--th>Last Price</th-->                                                                                             
                                                                                        <th>Price</th>
                                                                                        <th>Type</th>
                                                                                        <th> </th>
                                                                                        <th>Box</th>
										</tr>
									</thead>
                                                                        
									<tbody>																					

									<?php 
                                                                                 $tmp_idorder = 0;
                                                                                 
                                                                                 $cn=1;
                                                                                 
                                         while($row = mysqli_fetch_assoc($result))  {
                                                                                  
                                                             if ( $row['assign']== 10 )  {                                                                    
                                                                 $estado = '**';                                                                 
                                                             } else {
                                                                 $estado = ' ';                                                                 
                                                             }                             
                                                                                          
                                                                            ?>
										<tr>
                                                                                    
                                                                                    <!--td><div><?php echo $row['date_ship']; ?></div></td-->   
                                                                                    
                                                                                    <td>
                                                                                              <div><?php echo $row['date_del']; ?></div>                                                                                    
                                                                                    </td>  
                                                                                      
                                                                                      
										    <td>
                                                                                        <?php

////////////////////////////////////////////////////////////////////////////////
   // Cumplimiento Ofertas
       $stemscli = 0;
       $porcumpli= 0;
       
   $sqlOffer="select gor.marks          , gor.id           , gor.offer_id       , 
                     gor.offer_id_index , gor.grower_id    , gor.buyer_id       , 
                     gor.status         , gor.product      , gor.price          , 
                     gor.size           , gor.boxtype      , gor.bunchsize      , 
                     gor.boxqty         , gor.bunchqty     , gor.steams         ,
                     gor.req_group      , gor.request_id   , g.growers_name                                            
                from grower_offer_reply gor
               inner JOIN growers g ON gor.grower_id = g.id                                                                  
               inner JOIN buyer_requests br ON gor.request_id = br.id and gor.buyer_id = br.buyer                                                                 
               where gor.request_id = '" . $row['gid'] . "'
                 and gor.buyer_id   = '" . $row['buyer'] . "'  
                 and gor.cliente_id = '" . $row['id_client'] . "'     " ;      
                       
        $result_off   = mysqli_query($con, $sqlOffer);                                                                                                    
        
                        while($rowOff = mysqli_fetch_assoc($result_off))  {
                                $stemscli = $stemscli + $rowOff['bunchqty'];                                                                 
                            }    
                            
                            
                     $porcumpli =  ($stemscli*100)/$row['qty_pack'];
        
         // Costos  manejar por colores para que siempre despliegue el ultimo    
                                                                                        
        $sel_adi = "select join_order ,  join_order1 , join_order2
                        from buyer_orders 
                       where id = '".$idfac."'  ";                                                                                        
        
        $rs_adi = mysqli_query($con,$sel_adi);       
        $aditional = mysqli_fetch_array($rs_adi);                                                                                        
        
        //////////////////////////////////////////////////////////////////////
                                                                                        
        $sel_costo = "select (id),id_fact,  prod_name, product_subcategory, (price_cad) as price_cad , date_added
                        from invoice_packing_box
                       where buyer     = '" . $userSessionID . "'
                         and prod_name = '" . $row['name_product'] . "'
                         and product_subcategory = '" . $row['psubcatego'] . "' 
                         and id_fact   = '".$idfac."'
                       order by id desc limit 0,1";                                                                                        
        
        $rs_costo = mysqli_query($con,$sel_costo);       
        $costopack = mysqli_fetch_array($rs_costo);    
        
       /* Problema Varios Embarques  27-abr-2001 */ 
        
        $sel_join = "select (id),id_fact,  prod_name, product_subcategory, (price_cad) as price_cad , date_added
                        from invoice_packing_box
                       where buyer     = '" . $userSessionID . "'
                         and prod_name = '" . $row['name_product'] . "'
                         and product_subcategory = '" . $row['psubcatego'] . "' 
                         and id_fact   = '".$aditional['join_order']."'
                       order by id desc limit 0,1";                                                                                        
                       
        $rs_join = mysqli_query($con,$sel_join);       
        $costojoin = mysqli_fetch_array($rs_join);            
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
                
        if ( $costopack['price_cad']!= "" )  {                                                                    
                    $costoultimo = $costopack['price_cad'];                                                                 
        } else {
                    $costoultimo = $costojoin['price_cad'];                                                                 
        }                                             

        
        
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        $sel_ultprice = "select product, prod_name,cliente_id ,price_quick
                           from invoice_requests_subcli
                          where product    = '" . $row['product'] . "'
                            and cliente_id = '" . $row['id_client'] . "'
                          order by id desc limit 0,1";                                                                                        
        
        $rs_ultprice = mysqli_query($con,$sel_ultprice);       
        $last_price = mysqli_fetch_array($rs_ultprice);   
        
          if ( $last_price['price_quick']== 0.00 )  {                                                                    
                $priceinv = $row['price'];                                                                 
          } else {
                $price_inv = $last_price['price_quick'];                                                                 
          }
          
          $comentario = substr($row['comment'],10,8);

                          ?>

                                                                                <div><?php echo $row['psubcatego']." ".$row['name_product']." ".$row['colorname']." ".$row['featurename']; ?></div>
										</td> 
                                                                                                                            
                                                                                    
										<td>
                                                                                        <?php echo $row['size_name']."  cm." ; ?>
                                                                                </td>                                                                                    
                                                                                    
										<td>                                                                                                    
                                                                                        <div><?php echo $row['qty_pack']; ?></div>                                                                 
                                                                                </td>
                                                                                
										<td>                                                                                                    
                                                                                        <div><?php echo $porcumpli; ?>%</div>                                                                 
                                                                                </td>                                                                                
                                                                                
                                                                                <td>
                                                                                        <div><?php echo $costoultimo; ?></div>                                                                                    
                                                                                </td>    
                                                                                
                                                                                
                                                                                    <!-- Ultimo Precio Anterior-->
                                                                                <!--td>    
                                                                                        <div><?php echo number_format($last_price['price_quick'], 2, '.', ','); ?></div>                                                                                    
                                                                                </td--> 
                                                                                
                                                                                
                                                                                
                                                                                    <!-- Price -->
                                                                                <td>
                                                                                        <!--input type="text" class="form-control" name="price-<?php echo $row['gid'] ?>" id="price-<?php echo $row['gid'] ?>" value="<?php echo number_format($priceinv, 2, '.', ',') ?>" style="margin-top:5px; height:25px; padding:3px; width:80px;border-radius: 5px;"-->      
                                                                                        
                                                                                        <input type="text" class="form-control" name="price-<?php echo $row['gid'] ?>" id="price-<?php echo $row['gid'] ?>" value="<?php echo $row['price'] ?>" style="margin-top:5px; height:25px; padding:3px; width:80px;border-radius: 5px;">                                                                                          
                                                                                </td>                                                                                                                                                                  
                                                                                
                                                                                <td>
                                                                                        <div><?php echo substr($row['comment'],10,8); ?></div>                                                                                    
                                                                                </td>  
                                                                                
                                                                                <td>
                                                                                        <input type="hidden" class="form-control" name="id_reser" id="id_reser" value="<?php echo $row['id_reser'] ?>" style="margin-top:5px; height:25px; padding:3px; width:100px;border-radius: 5px;">      
                                                                                </td> 
                                                                                
                                                                                <td>
                                                                                        <input type="text" class="form-control" name="boxp-<?php echo $row['gid'] ?>" id="boxp-<?php echo $row['gid'] ?>" value="<?php echo $row['num_box'] ?>" style="margin-top:5px; height:25px; padding:3px; width:80px;border-radius: 5px;">      
                                                                                        <input type="hidden" class="form-control" name="pro-<?php echo $cn ?>" value="<?php echo $row["gid"] ?>"/>
                                                                                </td>                                                                                                                                                                  

                                                                                
                                                                <td>
                                                                       <button style="background:#8a2b83;" onclick="reverasign('<?php echo $row["gid"]; ?>','<?php  echo $row['id_reser']; ?>','<?php  echo $row['qty_pack']; ?>','<?php  echo $row['id_order']; ?>','<?php  echo $comentario; ?>')" class="btn btn-primary" type="button">Delete</button>                                                                                                                              
                                                                </td>                                                                                                                                  
                                                                                                                                                                                                                                                                                                                                

                                                                                                                                                                                                                                                     
                                                                                    
                                                                                    
                                                                                 
										</tr>																				
											<?php 
                                                                                                $totalCal    = $totalCal + $Subtotal;   
                                                                                                $total_bunch = $total_bunch + $row['qty_pack'];   
                                                                                                
                                                                                                $cn++;
                                                                                                                                                                      
                                                                             } ?>
									</tbody>
								</table>
							</div>

							<hr class="nomargin-top" />

							<div class="row">

								<div class="col-sm-6 text-left">
									<h4><strong>-</strong> </h4>

								</div>
                                                            
								<div class="col-sm-6 text-right">
									<ul class="list-unstyled">
                                                                            <li> <strong><h3>Total Bunch :  <?php echo $total_bunch;  ?></h3></strong> </li>                                                                                                                                                                                                              
                                                                            <!--li> <strong>Total Amount : </strong> <?php echo "$".number_format($total_fact, 2, '.', ',');  ?> </li-->                                                                                                                                  
									</ul>  
								</div>
                                                           
							</div>

						</div>
					</div>					

					<div class="panel panel-default text-right">

                                            
						<div class="panel-body">
                                                            <!--button type="button"   id="price_modal"
                                                                    class="btn btn-primary" style="background:#34495E;color:white" data-toggle="modal" data-target=".price_modal"><i class="fa fa-send"></i>Standing Order
                                                            </button-->                                                       
                                                            
                                                            
				                        <input type="submit" id="submitu" class="btn btn-success" name="submitu" value="Save">     
                                                            
							<a class="btn btn-success" href="<?php echo SITE_URL; ?>buyer/print_etiqueta.php?b=<?php echo $idfac."&id_cli=".$cliente_inv."&id_lotp=".$loteid ?>" target="_blank"><i class="fa fa-print"></i> PRINT</a>                                                                                                           
                                                        
                                                       <button id="assig" style="background:#8a2b83!important;" onclick="requestProduct('<?php echo $ir ?>','<?php  echo $info['id_client'];  ?>','<?php  echo $info['date_del'];  ?>',<?php  echo $info['id_order'];  ?> )" class="btn btn-primary" type="button">Assigned Customer</button>                                                       
						</div>
					</div>                                                                                                                    
                                    
                                    
				</div>
                                
			<!--/section-->
                        
			<!-- /MIDDLE -->

		</div>



	
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
		<script type="text/javascript" src="assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="assets/js/app.js"></script>

	<input type="hidden" name="totalrow" value="<?php echo $cn ?>"/>
        </form>
</section>
<?php require_once '../includes/footer_new.php'; ?>
<script type='text/javascript'>
    $(window).load(function () {
        $('#loading').css("display", "none");
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        
          $("#assig").click(function(){
              $("#assig").prop("disabled", true);
          });                
        
       $('select').select2();
       
                $('#productvar').select2({
                    ajax: {
                        url: "search_variety_customer.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });   
                

       
       ///////////
       
        var delDate = $('#cls_date_ship').find("input").val();
                      
         $('input[name=sname]').val(delDate);     
         
         console.log(delDate);
                      
                $('#var_add_x').select2({
                    ajax: {
                        url: "search_date_filter.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });        
        
               checkOptionship();
    });

</script>




<script>
    
       
    
    function boxQuantityChange(id, cartid, main_tr, with_a) {
        
        var selected_text = $("#request_qty_box_" + main_tr + " option:selected").text();
        var selected_val  = $("#request_qty_box_" + main_tr + " option:selected").val();
                        
		var nbox = selected_val.split('-');
                
                var tbox = $.trim(selected_text).split(" ");
                
		$('input[name=boxcant]').val(nbox[0]);
                
                $('input[name=boxtypen]').val(tbox[1]);
                                                
        $('input[name=product_su]').val(selected_val);
        $(".cls_hidden_selected_qty").val(selected_val);                                 
    }    
    
    
    // opcion que funciona del calendario pot
    
    function checkOptionship() {
    
        var shippingMethod = '132';
        
        var fecha_fin = $('#cls_date_del').find("input").val();
        console.log(fecha_fin);        
        
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/file/get_date_modal_cliship.php',
                data: 'shippingMethod=' + shippingMethod + '&fecha_fin=' + fecha_fin,
                success: function (data) {
                    $('.cls_date_start_dates').html(data);
                    $('#nextOpt').click();
                    _pickers();//show calendar
                }
            });        
    }
    
    function checkOptiondel(fecha_ini) {
    
       // var shippingMethod = '132';
           
       //console.log(fecha_ini);
        
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>/file/get_date_modal_clidel.php',
                data: 'fecha_ini=' + fecha_ini,
                success: function (data) {
                    $('.cls_date_start_dated').html(data);
                    $('#nextOpt').click();
                    _pickers();//show calendar
                }
            });        
    } 
     function send_date() {
        
        var delDate = $('#cls_date_ship').find("input").val();
       
                      
         $('input[name=sname]').val(delDate);     
         
         console.log("Date");
                      
                $('#var_add').select2({
                    ajax: {
                        url: "search_date_filter.php",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term,
                                s:'cities',
                            };
                        },
                        results: function (data, page) {
                            return {results: data.results};
                            console.log("prod  "  + data);
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                });        


     }   
     
     
    /*Button send request */
    function requestProduct(aa,cod_cli,del_date,orderid) {     
        

                
         var shipDate = $('#cls_date_ship').find("input").val();                     
         $('input[name=sname]').val(shipDate);        
        
        var buyer     = '<?= $_SESSION["buyer"]; ?>'; // codigo  del  buyer
        var flag_s = true; 
        var dateRange = "";              
       
        var tag            = cod_cli;                               
        var date_del       = del_date;                                       
        var order_val      = orderid;      

        var productId      = $('#var_add' + ' :selected').val();                     
        var productText    = $('#var_add' + ' :selected').text();  
        
        
        var productId_requ      = $('#productvar' + ' :selected').val();                     
        var productText_requ    = $('#productvar' + ' :selected').text();                                              
                             
                
        var box_quantity   = $('#qty_pack').val() ;         
        
        var type_box       = $('#box_type_'      + aa + ' :selected').val();                        
        var type           = $('#type_req_'      + aa).val();        
        var comment_pro    = $('#comment_'       + aa).val();
        var req_grow       = $('#grow_'          + aa).val();                         
                        
        var date_ship      = $('#sname').val();           
       
                        
        if (order_val != "") {
            $('#erMsg').hide();
        }else {
            flag_s = false;
            alert("Please select Order");            
            $('#erMsg').show();
        }
        
        if ($('#qty_pack').val() < 1) {
            $('#errormsg-1').html("Quantity error");
            flag_s = false;
        }
        
        var qytmax = $.trim(productText).split(" ");
                
        $('input[name=qty_ped]').val(qytmax[0]);   
        
        var qty_x = 0;        
        var qty_control = 0;
        
          qty_x = $('#qty_pack').val();        
         qty_control = $('#qty_ped').val();

                
        if (parseInt(qty_x) > parseInt(qty_control)) {
            $('#errormsg-1').html("Error Qty");
            flag_s = false;
        }      
        
                 //  alert (qty_x);
                 //  alert (qty_control);
                   
        
    if (flag_s == true) {           
        console.log("flag_s "  + flag_s);
        console.log("order_val "   + order_val);  
        console.log("productId "   + productId);  
        
        $.ajax({
            type: 'post',
            url: 'https://app.freshlifefloral.com/buyer/request_product_ajax_subcliente.php',
            data: 'date_range=' + dateRange +
                   '&order_val=' + order_val + 
                   '&productId=' + productId + 
              '&productId_requ=' + productId_requ +                                                          
                '&box_quantity=' + box_quantity + 
                       '&buyer=' + buyer + 
                    '&type_box=' + type_box + 
                        '&type=' + type + 
                 '&comment_pro=' + comment_pro +                   
                 '&productText=' + productText + 
            '&productText_requ=' + productText_requ +                                                     
                         '&tag=' + tag +                          
                   '&date_ship=' + date_ship +                          
                    '&date_del=' + date_del +                          
                    '&req_grow=' + req_grow,       
            success: function (data_s) {
                
                if (data_s == 'true') {

                } else {
                    alert('There is some error. Please try again 1');
                }
                         location.reload();

            }
        });
    }

    } 
    
    
    /*Button send DELETE */
    function reverasign(deleteid,idgor,qtypack,orderid,commId) {                                  
                       
        var flag_s = true; 
                     
        var id_delete      = deleteid;                     
        var id_offer_reply = idgor;                                              
        var quantity       = qtypack;                                              
        var numped         = orderid;                                              
        var regtype        = commId;                                              
                                                     
    if (flag_s == true) {           
        
        $.ajax({
            type: 'post',
            url: '<?php echo SITE_URL; ?>buyer/reversar_stock_subcliente.php',
            data: 'id_delete=' + id_delete +
            '&id_offer_reply=' + id_offer_reply +       
                  '&quantity=' + quantity+
                    '&numped=' + numped +
                   '&typecom=' + regtype,       
            success: function (data_s) {
                
                if (data_s == 'true') {
                       alert('OK');
                } else {
                    //alert('There is some error. Please try again 1');
                }
                         location.reload();

            }
        });
    }

    }  
function imagen(){       
        
	  <?php

		$sel_img="select br.id as idbr     , br.lfd        ,br.product as idcod  ,  p.id as codvar,
                                         gor.id as idgor   , gor.offer_id  , gor.offer_id_index , gor.grower_id   , gor.buyer_id , 
                                         gor. product as  productname , 
                                         gor.product_subcategory, gor.size as size_name  , (gor.bunchqty-gor.reserve) as  bunchqty  ,   gor.steams ,
                                         g.growers_name , br.feature, f.name as featurename,
                                         br.id_order,p.image_path
                                    from buyer_requests br
                                   inner join grower_offer_reply gor on gor.offer_id = br.id
                                   inner join product p on gor.product = p.name and gor.product_subcategory = p.subcate_name 
                                    left join growers g on gor.grower_id = g.id
                                    left join features f on br.feature = f.id 
                                   where g.active = 'active' 
                                     and (gor.bunchqty-gor.reserve) > 0
                                     and br.id_order  >= '822' ";

		$res_img=mysqli_query($con,$sel_img);

		while($rw_imagen=mysqli_fetch_array($res_img))	{

	  ?>
                               
                var idprod = $('#var_add' + ' :selected').val();
                
                
               // alert(ordenid);
                
                if(idprod=="<?php echo $rw_imagen["codvar"]?>")	{
                     
                        $('#errormsg-').html("<?= $rw_imagen["image_path"]?>")  
                        
                        var span_Text = document.getElementById("errormsg-").innerText;
                        //alert (span_Text);
                        
                        var  url_url= 'https://app.freshlifefloral.com/' + span_Text;
                        
                        $('input[name=idimg]').val(span_Text); 
                        
                        $('#my_image').attr('src',url_url);
	  	}
                                              
	  <?php	}   ?>	

	}    
function imagen_req(){       
    
        var productText_requ = $('#productvar' + ' :selected').text();    
        
        var path = productText_requ.split("+", 2);        
        $('input[name=imgpath]').val(path[1]);        
        
        var pimage = $('#imgpath').val();  
       
        //alert(pimage);
        
                        $('#errormsg-').html(pimage)  
                        
                        var span_Text = document.getElementById("errormsg-").innerText;
                        
                        var  url_url= 'https://app.freshlifefloral.com/' + span_Text;
                        
                        $('input[name=idimg]').val(span_Text); 
                        
                        $('#my_image').attr('src',url_url);
                                              	  
	}                
</script>    
    
    