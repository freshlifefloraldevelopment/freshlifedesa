<?php

// PO 2018-08-24

$menuoff = 1;
$page_id = 421;
$message = 0;

//include("../config/config_new.php");

require_once("../config/config_gcp.php");


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}
$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);
$page_request = "buyer_invoices";
?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

<section id="middle">


    <!-- page title -->
    <header id="page-header">
        <h1>Invoice List</h1>
        <ol class="breadcrumb">
            <li><a href="#">Buyer</a></li>
            <li class="active">Pendings</li>
        </ol>
    </header>
    <!-- /page title -->


    <div id="content" class="padding-20">


        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>Buyer Invoices</strong> <!-- panel title -->
                </span>

                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-Confirmed-title="Confirmed" data-Confirmed-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul>
                <!-- /right options -->

            </div>

            <!-- panel content -->
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                            <tr>
                                <th>Invoice Num</th>
                                <th>Descrip</th>                                                                                                 
                                <th>Subtotal</th>
                                <th>PerKg</th>                                 
                                <th>Air Waybill</th>                                 
                                <!--th>Charge Due</th-->                                                                 
                                <th>Gross W.</th>                                                                                                 
                                <th>Delivery</th>
                                <th>St</th>                                
                                <th>List</th>                                
                                <th>View </th>
                                <th>  </th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = "SELECT id_fact         , buyer_id      , order_number   , order_date     , shipping_method , del_date   , date_range , 
                                           is_pending      , order_serial  , seen           , delivery_dates , lfd_grower      , quick_desc , bill_number, 
                                           gross_weight    , volume_weight , freight_value  , guide_number   , total_boxes     , 
                                           sub_total_amount, tax_rate      , shipping_charge, handling       , grand_total     ,
                                           bill_state      , date_added    , user_added     ,
                                           air_waybill     , charges_due_agent,
                                           credit_card_fees,per_kg
                                      FROM invoice_orders 
                                     WHERE buyer_id = '".$userSessionID."' order by id_fact desc";
                            
							$invoices_res = mysqli_query($con, $sql);
                                                        
							while ($invoice = mysqli_fetch_assoc($invoices_res)) { 
                                                              $id_fact = $invoice['id_fact'];
							?>
							<tr>


                                                                <td><?php echo $invoice['id_fact']." - ".$invoice['order_number']; ?></td>
								<td><?php echo $invoice['quick_desc']; ?></td>                                                                
								<td><?php echo number_format($invoice['sub_total_amount'], 2, '.', ','); ?></td>
                                                                <td><?php echo number_format($invoice['per_kg'], 2, '.', ','); ?></td>                                                                
                                                                <td><?php echo number_format($invoice['air_waybill'], 2, '.', ','); ?></td>                                                                
                                                                <!--td><?php echo number_format($invoice['charges_due_agent'], 2, '.', ','); ?></td-->                                                                                                                                
								<td><?php echo $invoice['gross_weight']; ?></td>                                                                
								<td><?php echo $invoice['del_date']; ?></td>
								<td><?php echo $invoice['bill_state']; ?></td> 
                                                                
                                                                 
                                                                 <td><a href="<?php echo SITE_URL; ?>buyer/document-list.php?idi=<?php echo $id_fact; ?>" class="btn btn-success btn-xs relative"> Document </a></td>                                                                                                                                                                                                                                                                         
                                                           
                                                                <?php if ($invoice['bill_state'] == "P") { ?>                                                                
                                                                        <td> <a href="javascript:void(0);" class="btn btn-success btn-xs relative" style="border:0px solid #fff; background-color: #d68910">Pending </a></td>                                                                                                                                                                                                        
                                                                <?php }else{ ?>  
                                                                        <td><a href="<?php echo SITE_URL; ?>buyer/invoice.php?idi=<?php echo $id_fact; ?>" class="btn btn-success btn-xs relative"> View </a></td>                                                                
                                                                <?php }?>
                                                                        
                                                                        <td><a href="<?php echo SITE_URL; ?>buyer/invoice-price.php?idi=<?php echo $id_fact; ?>" class="btn btn-success btn-xs relative"> Price </a></td>                                                                                                                                        
                                                                
							</tr>
							<?php
							$i++;
                            }
                            ?> 
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>
<?php require_once '../includes/footer_new.php'; ?>
