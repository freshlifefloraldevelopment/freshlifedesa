<?php

$menuoff = 1;
$page_id = 421;
$message = 0;

$id_client  = $_GET['id_cli'];

require_once("../config/config_gcp.php");


if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

$img_url = '../images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '../images/profile_images/' . $profile_image;
}

$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

/*
   $sel_cli = "select br.date_ship , br.date_del , br.id_order , br.id_client ,
                      sc.name , 'Request' as marks,br.comment
                   from reser_requests br
                  inner JOIN sub_client sc ON IFNULL(br.id_client,0) = sc.id
                  where br.comment   like 'SubClient%'                                            
                  group by br.id_order,br.id_client 
                  order by br.id_order" ;
   
      $sel_cli = "select br.date_ship , br.date_del , br.id_order , br.id_client ,
                      sc.name , 'Request' as marks,br.comment
                   from reser_requests br
                  inner JOIN buyer_orders bo ON br.id_order = bo.id
                  inner JOIN sub_client sc ON IFNULL(br.id_client,0) = sc.id
                  where br.buyer = '" . $userSessionID . "' 
                    and br.comment   like 'SubClient%'                                            
                    and bo.assigned = 1 
                  group by br.id_order,br.id_client 
                  union
                 select br2.date_ship , br2.date_del , br2.id_order , br2.id_client ,
                        sc.name , 'Request' as marks,br2.comment
                   from buyer_requests br2
                  inner JOIN buyer_orders bo ON br2.id_order = bo.id
                  inner JOIN sub_client sc ON IFNULL(br2.id_client,0) = sc.id
                  where br2.buyer = '" . $userSessionID . "' 
                    and br2.comment   like 'SubClient%'                      
                    and bo.assigned = 1 
                  group by br2.id_order,br2.id_client 
                  order by id_order desc " ;
*/

      $sel_cli = "select br.date_del , br.id_order , br.id_client , sc.name as namecli,br.label , br.lote
                   from reser_requests br
                  inner JOIN buyer_orders bo ON br.id_order = bo.id
                  inner JOIN sub_client sc ON IFNULL(br.id_client,0) = sc.id
                  where br.buyer = '" . $userSessionID . "' 
                    and br.comment   like 'SubClient%'                                            
                    and bo.assigned = 1 
                  group by br.id_order , br.id_client ,br.lote
                  union
                 select  br2.date_del , br2.id_order , br2.id_client , sc.name as namecli,br2.label , br2.lote
                   from buyer_requests br2
                  inner JOIN buyer_orders bo ON br2.id_order = bo.id
                  inner JOIN sub_client sc ON IFNULL(br2.id_client,0) = sc.id
                  where br2.buyer = '" . $userSessionID . "' 
                    and br2.comment   like 'SubClient%'                      
                    and bo.assigned = 1 
                  group by br2.id_order,br2.id_client  , br2.lote
                  order by id_order desc,namecli " ;      

$rs_cli = mysqli_query($con, $sel_cli);


$page_request = "buyer_invoices";
?>
<?php require_once '../includes/profile-header.php'; ?>
<link href="<?php echo SITE_URL; ?>../includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

<section id="middle">


    <!-- page title -->
    <header id="page-header">
        <h1>Order Customer</h1>
        <ol class="breadcrumb">
            <li><a href="#">Buyer</a></li>
            <li class="active">Requests</li>
        </ol>
    </header>
    <!-- /page title -->


    <div id="content" class="padding-20">


        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <a href="<?php echo SITE_URL; ?>buyer/client_order_sub.php?sw=<?php echo "1" ?>" class="btn btn-success btn-xs relative">Add New Order</a>                
                <a href="<?php echo SITE_URL; ?>buyer/inventory_rest_edit.php?sw=<?php echo "1" ?>" class="btn btn-success btn-xs relative">Inventory</a>  
                
                <a href="<?php echo SITE_URL; ?>buyer/print_list_price.php" class="btn btn-success btn-xs relative">Price List</a>                  
                
                <!--a href="<?php echo SITE_URL; ?>buyer/more_weeks.php?idcus=<?php echo $id_client ?>" class="btn btn-success btn-xs relative">More Weeks</a-->                
                <!--span class="title elipsis">
                    <strong>Standing Order </strong>                     
                </span-->
                <!--?php echo " (".$cliente['name'].")"; ?--><!-- panel title -->

                <!-- right options -->
                <ul class="options pull-right list-inline">

                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-Confirmed-title="Confirmed" data-Confirmed-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul>
                <!-- /right options -->

            </div>

            <!-- panel content -->
            <div class="panel-body">

                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                            <tr>
                                <th>Num.</th>                                 
                                <th>Delivery Day</th>                                 
                                <th>Order</th>                                 
                                <th>Client</th>                                                                 

                            </tr>
                        </thead>
                        <tbody>
                                                        <?php
                                                                $ii=1;
                                                                $quiebre = 0;  
                                                       
							while ($client_week = mysqli_fetch_assoc($rs_cli)) { 

							?>
							<tr>
                                                            
                                                                  <?php                                                                                                                      
							          if ($client_week['id_order'] == $quiebre) {

                                                                           if ($client_week['label'] == 0) {  
                                                                                 if ($client_week['id_order'] == 1042) {                                                                               
							           ?>
                                                                                    <td><font color="red"><?php echo $ii; ?></font></td>
                                                                                    <td><font color="red"><?php echo $client_week['date_del']; ?></font></td>                                                              
                                                                                    <td><font color="red"><?php echo $client_week['id_order']; ?> </font></td> 
                                                                                    <td><font color="red"><?php echo $client_week['namecli']; ?></font></td>                                                                                                                                                                                                                                                                        
                                                                                 <?php
                                                                                 }else{     
                                                                                 ?> 
                                                                                    <td><?php echo $ii; ?></td>
                                                                                    <td><?php echo $client_week['date_del']; ?></td>                                                              
                                                                                    <td><?php echo $client_week['id_order']; ?> </td> 
                                                                                    <td><?php echo $client_week['namecli']; ?></td>                                                                                                                                                                                                                                                                        
                                                                                    
                                                                              <?php      
                                                                                 }
                                                                              }else{
                                                                            ?>     
                                                                                    <td><font color="red"><?php echo $ii; ?></font></td>
                                                                                    <td><font color="red"><?php echo $client_week['date_del']; ?></font></td>                                                              
                                                                                    <td><font color="red"><?php echo $client_week['id_order']; ?></font></td> 
                                                                                    <td><font color="red"><?php echo $client_week['namecli']; ?></font></td>                                                                            
                                                                            <?php } ?>     
                                                                                    
                                                                   <?php }else{
                                                                                  $ii=1;
                                                                       ?>   
                                                                            <td><?php echo $ii; ?></td>
                                                                            <td><h5><?php echo $client_week['date_del']; ?></h5></td>                                                              
                                                                            <td><h5><?php echo $client_week['id_order']; ?></h5> </td>   
                                                                            <td><h5><?php echo $client_week['namecli']; ?></h5></td>                                                                                                                                          
                                                                    <?php } 
                                                                            $quiebre = $client_week['id_order'];                                   
                                                                     ?>                                                                                   
                                                                
                                                                
                                                                                                                                                                                                                                                                 
                                                                <td>
                                                                    <a href="<?php echo SITE_URL; ?>buyer/client_order_subopen_edit.php?fac_id=<?php echo $client_week["id_order"]."&id_cli=".$client_week["id_client"]."&id_lote=".$client_week["lote"] ?>" class="btn btn-success btn-xs relative">Open Market</a>
                                                                </td>         
                                                                
                                                                <td>
                                                                    <a href="<?php echo SITE_URL; ?>buyer/subclient_edit.php?fac_id=<?php echo $client_week["id_order"]."&id_cli=".$client_week["id_client"] ?>" class="btn btn-success btn-xs relative">Comment</a>
                                                                </td>                                                                         
                                                                
                                                                <td>
                                                                    <a href="<?php echo SITE_URL; ?>buyer/print_packing_customer11.php?fac_id=<?php echo $client_week["id_order"]."&id_cli=".$client_week["id_client"] ?>" class="btn btn-success btn-xs relative">VARIETY</a>
                                                                </td>                                                                    
                                                                
                                                                <td>
                                                                    <a href="<?php echo SITE_URL; ?>buyer/print_packing_customer21.php?fac_id=<?php echo $client_week["id_order"]."&id_cli=".$client_week["id_client"] ?>" class="btn btn-success btn-xs relative">CUSTOMER</a>
                                                                </td>                                                                                                                                    
                                                                
                                                                <!--td>
                                                                    <a href="<?php echo SITE_URL; ?>buyer/client_order_report.php?fac_id=<?php echo $client_week["id_order"]."&id_cli=".$client_week["id_client"] ?>" class="btn btn-success btn-xs relative">Order</a>
                                                                    <a href="<?php echo SITE_URL; ?>buyer/client_boxes.php?fac_id=<?php echo $client_week["id_order"]."&id_cli=".$client_week["id_client"] ?>" class="btn btn-success btn-xs relative">Boxes</a>
                                                                </td-->   
                                                                
                                                                <td>
                                                                    <!--a href="<?php echo SITE_URL; ?>buyer/client_assign.php?fac_id=<?php echo $client_week["id_order"]."&id_cli=".$client_week["id_client"] ?>" class="btn btn-success btn-xs relative">Procesar</a-->                
                                                                    
                                                                    <a href="<?php echo SITE_URL; ?>buyer/print_packing_customer.php?b=<?php echo $client_week["id_order"]."&id_buy=".$userSessionID ?>" class="btn btn-success btn-xs relative">Availability</a>                                                                    
                                                                </td>                                                                                                                                  
                                                                
							</tr>
							<?php
							$ii++;
                            }
                            ?> 
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>
<?php require_once '../includes/footer_new.php'; ?>
