<?php
require_once("../config/config_new.php");
if ($_SESSION["login"] != 1 && $_SESSION["grower"] == "") {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["grower"];
/* * *******get the data of session user*************** */
$sel_info = "select * from growers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

$img_url = '../images/profile_images/noavatar.jpg';
if ($info["file_path5"] != '') {
    $k = explode("/", $info["file_path5"]);
    $data = getimagesize(SITE_URL . "../user/logo2/" . $k[1]);
    $img_url = SITE_URL . "../user/logo/" . $k[1];
}

require_once '../includes/profile-header.php';
?>
<link href="<?php echo SITE_URL; ?>/includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css" />
<?php
$page_request = "order_summary";
require_once "../includes/left_sidebar_growers.php";
$query = "select gpb.*, gpb.id as cartid,gpb.date_added ,p.id,p.name,p.color_id,p.image_path,s.name as subs,sh.name as sizename,ff.name as featurename,gor.id as ererer from buyer_requests gpb
                                left join product p on gpb.product = p.id
                                left join subcategory s on p.subcategoryid=s.id  
                                left join features ff on gpb.feature=ff.id
                                left join grower_offer_reply gor on gpb.id=gor.offer_id 
                                left join sizes sh on gpb.sizeid=sh.id where gor.grower_id='" . $userSessionID . "' and gor.status=1 order by gpb.id desc";
//echo $query;
$result = mysqli_query($con, $query);
$tp = mysqli_num_rows($result);
?>
<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Bootstrap Tables</h1>
        <ol class="breadcrumb">
            <li><a href="#">Tables</a></li>
            <li class="active">Bootstrap Tables</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div id="panel-2" class="panel panel-default">
            <div class="panel-heading">
                <span class="title elipsis">
                    <strong>Send Offers</strong> <!-- panel title -->
                </span>
                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-confirm-title="Confirm" data-confirm-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul>
                <!-- /right options -->
            </div>
            <!-- panel content -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover table-vertical-middle nomargin" style="padding:8px!important;"> 
                        <thead>
                            <tr>
                                <th>Boxes</th>
                                <th>Product</th>						
                                <th>Price</th>
                                <th>Comment</th>
                                <th>LFD</th>
                                <th>Cargo Agency</th>
                                <th>Marks</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            while ($producs = mysqli_fetch_array($result)) {
                                //echo '<pre>';print_r($producs);
                                $sel_check = "select * from  grower_offer_reply where offer_id='" . $producs["cartid"] . "' and id='" . $producs["ererer"] . "' and grower_id='" . $userSessionID . "' ";
                                $rs_check = mysqli_query($con, $sel_check);
                                $check = mysqli_num_rows($rs_check);
                                $grower_offer = mysqli_fetch_assoc($rs_check);
                                $temp = explode("-", $producs["boxtype"]);
                                $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                                $rs_box_type = mysqli_query($con, $sel_box_type);
                                $box_type = mysqli_fetch_array($rs_box_type);
                                //echo '<pre>';print_r($grower_offer);
                                ?>

                                <tr>
                                    <td><?= $producs["qty"] ?> <?= $box_type["name"] ?></td>
                                    <td style="font-size:13px;">
                                        <?= $producs["subs"] ?> <?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] ?>cm
                                    </td>
                                    <td> <?php
                                        if ($grower_offer["price"] != "0.00") {
                                            ?>
                                            $ <?= sprintf("%.2f", $grower_offer["price"]) ?>
                                            <?php
                                        } else {

                                            $total_price = 0;
                                            $total_stems = 0;
                                            $avg = 0;
                                            $sel_box_products = "select id,product,price,bunchsize,bunchqty from grower_reply_box where offer_id='" . $producs["cartid"] . "' and reply_id!=0 and grower_id='" . $userSessionID . "'";
                                            $rs_box_products = mysqli_query($con, $sel_box_products);
                                            while ($box_products = mysqli_fetch_array($rs_box_products)) {
                                                $total_price = $total_price + $box_products["price"];
                                                $total_stems = $total_stems + ($box_products["bunchsize"] * $box_products["bunchqty"]);
                                            }

                                            $avg = $total_price / $total_stems;
                                            ?>
                                            $ <?= sprintf("%.2f", $avg) ?>
                                        <?php } ?>
                                    </td>
                                    <td><a href="#" data-toggle="modal" data-target=".text_modal_<?php echo $i; ?>"><?php
                                            if ($producs["comment"] != "") {
                                                echo substr($producs["comment"], 0, 15) . '...';
                                            } else {
                                                echo "No Comment";
                                            }
                                            ?>
                                        </a>
                                        <!-- Text Modal >-->
                                        <div class="modal fade text_modal_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <!-- header modal -->
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                        <h4 class="modal-title" id="mySmallModalLabel">Comment</h4>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body">
                                                        <?php echo $producs["comment"]; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Text Modal >-->
                                    </td>
                                    <td><?php
                                        if ($producs["lfd2"] == '0000-00-00') {
                                            $lfd_temp = explode("-", $producs["lfd"]);
                                            echo $lfd_temp[1] . "-" . $lfd_temp[2] . "-" . $lfd_temp[0];
                                        } else {
                                            $lfd_temp = explode("-", $producs["lfd2"]);
                                            echo $lfd_temp[1] . "-" . $lfd_temp[2] . "-" . $lfd_temp[0] . " to <br/>";
                                            $lfd_temp = explode("-", $producs["lfd"]);
                                            echo $lfd_temp[1] . "-" . $lfd_temp[2] . "-" . $lfd_temp[0] . " ";
                                        }
                                        ?></td>
                                    <td><a href="#" data-toggle="modal" data-target=".Transinternational_<?php echo $i; ?>">
                                            <?=$grower_offer["cargo"]?>
                                        </a>
                                        <!-- Transinternational Modal >-->

                                        <div class="modal fade Transinternational_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">

                                                    <!-- header modal -->
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                        <h4 class="modal-title" id="mySmallModalLabel">Transinternational</h4>
                                                    </div>

                                                    <!-- body modal -->
                                                    <div class="modal-body">		
                                                        <p> Email: info@ticargo.com</p>
                                                        <p> Phone: (593)2 2469 224/ 225/ 229 </p>
                                                        <p> Skype: trans.cargo</p>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- Transinternationa Modal >-->
                                    </td>
                                    <td><a href="javascript:void(0);"><?= $grower_offer["marks"] ?></a></td>
                                    <td><a data-toggle="modal" data-target=".open_offer_modal<?php echo $i; ?>" class="btn btn-success btn-xs relative">
                                            <?php if ($grower_offer["status"]==1) { echo 'Confirm'; }else{ echo 'In Progress'; } ?>
                                        </a>
                                        <!--Offer Modal Start-->
                                        <div class="modal fade open_offer_modal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">

                                                    <!-- header modal -->
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                        <h4 class="modal-title" id="myLargeModalLabel"><?= $producs["subs"] ?> <?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] ?>cm</h4>
                                                    </div>

                                                    <!-- body modal -->
                                                    <div class="modal-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Product</th>
                                                                        <th>Boxes offered</th>
                                                                        <th>Price</th>
                                                                        <th>Confirm/Status</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php $rs_check1 = mysqli_query($con, $sel_check); while($grower_offer1 = mysqli_fetch_assoc($rs_check1)){ //echo '<pre>';print_r($grower_offer1); ?>
                                                                    <tr>
                                                                        <td><?= $grower_offer1["product_subcategory"] ?> <?= $grower_offer1["product"] ?> <?= $grower_offer1["size"] ?> <?= $grower_offer1["bunchsize"] ?>/bunch <?=$grower_offer1['bunchqty'] ?> Bunch</td>
                                                                        <td><?= $grower_offer1["boxqty"] ?>  <?= $grower_offer1["boxtype"] ?></td>
                                                                        <td>$<?php echo $grower_offer1["price"]; ?></td>
                                                                        <td>
                                                                            <a class="btn btn-success btn-xs relative" style="background:purple;border:0px solid #fff;"><?php if($grower_offer1["status"] ==1 ) { ?> Confirmed <?php } ?></a>
                                                                        </td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Offer Modal End-->
                                    </td>
                                </tr>  
                                <?php $i++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
    </div>
</section>
<!-- /MIDDLE -->
<?php include("../includes/footer_new.php"); ?>
