
<?php
require_once("../config/config_gcp.php");
/*
if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}


if (isset($_GET["idOrder"]) && $_GET["idOrder"] != "") {
   $idOrden = $_GET["idOrder"];
}else{
	$idOrden=0;
}

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}

if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor-account.php");
    die;
}

$userSessionID = $_SESSION["buyer"];

$shippingMethod = $_SESSION['shippingMethod'];
*/
$page_request = "inventory";
?>

<?php
require_once '../includes/profile-header.php';
require_once "../includes/left_sidebar_growers_new.php";
include '/pagination/pagination.php';
?>


<style>
    /*This css is fir fixing select dropdown size just only on name-your-price.php page*/
    .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__rendered, .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 40px !important;
        line-height: 36px !important;
        font-size: 14px !important;
        font-family: 'Open Sans', Arial, sans-serif !important;
    }

    td {
        font-size: 14px !important;
        font-family: 'Open Sans', Arial, sans-serif !important;
    }

    .modal_k {
        display: none;
        position: fixed;
        z-index: 1000;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: rgba(255, 255, 255, .8) url('images/ajax-loader.gif') 50% 50% no-repeat;
    }

    body.loading {
        overflow: hidden;
    }

    body.loading .modal_k {
        display: block;
    }

    .pagination > li {
        float: left;
    }
</style>


<style>

    /*This css is fir fixing select dropdown size just only on name-your-price.php page*/

    .select2-container--default .select2-selection--single,
    .select2-container--default .select2-selection--single .select2-selection__rendered,
    .select2-container--default .select2-selection--single .select2-selection__arrow {

        height: 40px !important;

        line-height: 36px !important;

        font-size: 14px !important;

        font-family: 'Open Sans', Arial, sans-serif !important;

    }
    
    body.loading {

        overflow: hidden;

    }

    .pagination > li {

        float: left;

    }


</style>


<style>

    #middle div.panel-heading {

        height: auto;

    }

</style>

<?php

$dates = date("jS F Y");

?>


<section id="middle">


    <!-- page title -->


    <header id="page-header">

        <h1>Shipment</h1>

        <ol class="breadcrumb">

            <li><a href="#">Select</a></li>

            <li class="active">Boxes Request Offer Order</li>

        </ol>

    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <!-- LEFT -->
                    <div class="col-md-12">

                        <div id="content" class="padding-20">

                            <div id="panel-2" class="panel panel-default">

                                <div class="panel-heading">
                                    <span class="title elipsis">Shipment </span>
                                    <br>
                                </div>

                                <!-- panel content -->

                                <div class="panel-body">
                                    <div class="row">



                                        <div class="form-group col-md-4 col-md-offset-3" >

                                        <form name="forma" id="forma" action="growers-recieved-offers-new22.php" method="post" onsubmit="return verify();">

                                            <br>

                                           <h4>
                                               <label>Select Buyer</label></h4>                                            
					<?php if ($idOrden==0) { ?>
                                            <select  onchange="orderChange()"style="width: 100%; display: none;" name="filter_order" id="filter_order" class="form-control select2 cls_filter" tabindex="-1">
                                                <option value="">Select Buyer</option>
                                                <?php
                                                $subcategory_sql = "select id ,company,active from buyers where active = 'active'";
																								
                                                $result_subcategory = mysqli_query($con, $subcategory_sql);
                                                while ($row_subcategory = mysqli_fetch_assoc($result_subcategory)) { ?>
                                                    <option value="<?php echo $row_subcategory['id']; ?>"><?php echo $row_subcategory['company']; ?></option>
                                                <?php }
                                                ?>
                                            </select>

<br>
<br>     
                                           <label>Select Shipping</label>
                                            <select  onchange="orderChange()"style="width: 100%; display: none;" name="filter_order" id="filter_order" class="form-control select2 cls_filter" tabindex="-1">
                                                <option value="">Select Shipping</option>
                                                <?php

                                                $shipping_sql = "select id as idsm,name from shipping_method order by id";
												
												
                                                $result_shipping = mysqli_query($con, $shipping_sql);
                                                while ($row_shipping = mysqli_fetch_assoc($result_shipping)) { ?>
                                                    <option value="<?php echo $row_shipping['idsm']; ?>"><?php echo $row_shipping['name']; ?></option>
                                                <?php }
                                                ?>
                                            </select>                                            
					<?php }  ?>
<br>
<br>

                                            <!-- date picker -->
                                            <label>Select Date</label>
                                            <!--<?php //echo $products["prodcutid"]; ?>_<?php ///echo $i; ?>--->
                                            <label class="field cls_date_start_date" id="cls_date">
                                                <input value="" class="form-control required start_date" placeholder="Select Date" style="width: 400px!important;text-indent: 32px;border-radius: 5px;" type="text">
                                            </label>                                            
												
					<?php if ($idOrden>0) { ?>
                                            <select  onchange="orderChange()"style="width: 100%; display: none;" name="filter_order" id="filter_order" class="form-control select2 cls_filter" tabindex="-1">
                                                <?php
                                                $fld_sql = "select  id as idqu, qucik_desc  from  buyer_orders where id=".$idOrden." and  buyer_id='".$userSessionID."' and del_date >= '" . date('Y-m-d') . "' and is_pending=0";																								
                                                $result_fld = mysqli_query($con, $fld_sql);
                                                while ($row_fld = mysqli_fetch_assoc($result_fld)) { ?>
                                                    <option selected="selected"  value="<?php echo $row_fld['idqu']; ?>"><?php echo $row_subcategory['qucik_desc']; ?></option>
                                                <?php }
                                                ?>
                                            </select>
											 <?php }
                                                ?>
                                            <br><br>
                                            <input type="hidden" name="order" id="order" value="">
                                            
                                            <div class="col-md-10 col-md-offset-3"></div>
                                            <br>
                                            <br>

                                            <div class="col-md-2 col-md-offset-3">
                                                <button type="submit" class="btn btn-3d btn-purple" style="background-color:#06B120!important;">Shipment <i class="fa fa-chevron-right"></i></button>
                                            </div>


                                            </form>

                                        </div>
                                        <div class="col-md-4 col-md-offset-3">

                                        </div>
                                        <div class="col-md-3 col-md-offset-3">
                                            <br>
                                            <br>
                                           <small >Fresh life floral Ecuador 2018</small>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">


    function  verify() {
        $("#order").val($("#filter_order option:selected").html());

        var order_val = $('#filter_order :selected').val();

        //alert($('#filter_order :selected').val());
        if (order_val==""){

            alert ("Please choose an option to proceed");
            return false;
        }
        else{
            return true;
        }

    }

    function orderChange(product_id, sizename, i) {

        var order_val = $('#filter_order :selected').val();
        //verificar  la  order_val=="";  hay esta  vacio
        console.log(order_val);


    }

</script>


<?php include("../includes/footer_new.php"); ?>
