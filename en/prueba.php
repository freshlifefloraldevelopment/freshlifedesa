<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</head>
<body>

<div class="container">
  <h2>Dropdowns</h2>
  <p>The .active class adds a blue background color to the active link.</p>
  <p>The .disabled class disables a dropdown item (grey text color).</p>
  <div class="dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Tutorials
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="#">Normal</a></li>
      <li class="disabled"><a href="#">Disabled</a></li>
      <li class="active"><a href="#">Active</a></li>
      <li><a href="#">Normal</a></li>
    </ul>
  </div>
  
  
</div>

       <div class="btn-group pull-left product-opt-qty">

                            <button type="button" class="btn btn-default dropdown-toggle product-type-dd noradius" id="product-type-dd-box" data-toggle="dropdown">

                                                                                    
                                
                                <small id="product-box-type"> Box Type</small>
                                <span class="caret"></span>
								
								
								
                                <input type="hidden" value="" id="hdn_box_type" name="hdn_box_type" />
                            </button>



                            <ul id="product-type-dd-box" class="dropdown-menu" role="menu">

                                <li style="width:100%!important;"><a data-val="Jumbo Box" pass_box_type="JB" href="javascript:void(0);">Jumbo Box</a></li>

                                <li style="width:100%!important;"><a data-val="Half Box" pass_box_type="HB" href="javascript:void(0);">Half Box</a></li>

                                <li style="width:100%!important;"><a data-val="Quarter Box" pass_box_type="QB" href="javascript:void(0);">Quarter Box</a></li>

                                <li style="width:100%!important;"><a data-val="Eighth Box" pass_box_type="EB" href="javascript:void(0);">Eighth Box</a></li>

                                <!-- <li style="width:100%!important;"><a data-val="Bunch" href="javascript:void(0);">Bunch</a></li> -->



                            </ul>

                        </div>

</body>
</html>
