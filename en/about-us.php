<?php
require_once("../config/config_new.php");
############QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################
$pageId = 1; //VARIETY PAGE ID 
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL . '../includes/assets/css/essentials-flfv3.css', SITE_URL . 'includes/assets/css/layout-flfv3.css',
    SITE_URL . '../includes/assets/css/header-1-flfv3.css', SITE_URL . 'includes/assets/css/color_scheme/blue.css');
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################

require_once '../includes/header.php';
?>
<section class="page-header page-header-xs">
    <div class="container">

        <h1><?php echo $pageData["meta_title"]; ?></h1>
        <span class="font-lato size-18 weight-300 hidden-xs">We believe in Simple &amp; Creative</span>

        <!-- breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">About Us</li>
        </ol><!-- /breadcrumbs -->

    </div>
</section>
<!-- /PAGE HEADER -->
<!-- -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <p><?php echo $pageData["para_1"]; ?></p>
                <blockquote>
                    <p><?php echo $pageData["client_review"]; ?></p>
                    <cite><?php /* CEO, Jack Bour */ echo $pageData["client_name"]; ?></cite>
                </blockquote>
            </div>
            <div class="col-lg-6">
                <div class="embed-responsive embed-responsive-16by9 box-shadow-1">
                    <?php
                    if (strpos($pageData["page_video"], 'vimeo') !== false) {
                        ?>
                        <iframe src="<?php /* CEO, Jack Bour */ echo $pageData["page_video"]; ?>" class="embed-responsive-item"  width="800" height="450"></iframe> 
                    <?php
                    } else {
                        $vd = explode("v=", $pageData["page_video"]);
                        ?> 
                        <iframe class="embed-responsive-item"  width="800" height="450" src="https://www.youtube.com/embed/<?php echo$vd[1]; ?>" frameborder="0" allowfullscreen></iframe>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / -->
<!-- -->
<section>
    <div class="container">

        <div class="row">

            <div class="col-lg-4">

                <div class="heading-title heading-border-bottom">
                    <h3><?php echo $pageData["fb1"]; ?></h3>
                </div>

                <div class="toggle toggle-transparent-body toggle-accordion">

                    <div class="toggle active">
                        <label><?php echo $pageData["fb2"]; ?></label>
                        <div class="toggle-content">
                            <p><?php echo $pageData["fb5"]; ?></p>
                        </div>
                    </div>

                    <div class="toggle">
                        <label><?php echo $pageData["fb3"]; ?></label>
                        <div class="toggle-content">
                            <p><?php echo $pageData["fb6"]; ?></p>
                        </div>
                    </div>

                    <div class="toggle">
                        <label><?php echo $pageData["fb4"]; ?></label>
                        <div class="toggle-content">
                            <p><?php echo $pageData["fb7"]; ?></p>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-lg-4">

                <div class="heading-title heading-border-bottom">
                    <h3><?php echo $pageData["skills"]; ?></h3>
                </div>

                <p><?php echo $pageData["skills_desc"]; ?></p>

            </div>

            <div class="col-lg-4">

                <div class="heading-title heading-border-bottom">
                    <h3><?php echo $pageData["special_notes"]; ?></h3>
                </div>

                <p><?php echo $pageData["special_desc"]; ?></p>

                <a href="#" class="btn btn-default btn-block btn-lg">Join Us Now</a>

            </div>

        </div>

    </div>
</section>
<!-- / -->



<!-- PARALLAX -->
<section id="portfolio" class="portfolio-nogutter nopadding-bottom">
    <div class="container">

        <header class="text-center margin-bottom-10">
            <h3><?php echo $pageData["box1_title"]; ?></h3>
            <p class="font-lato weight-300 size-18 margin-top-10"><?php echo $pageData["box1_desc"]; ?></p>
        </header>

        <hr class="margin-bottom-100">

    </div>


    <!-- PORTFOLIO ITEMS -->
    <div class="row mix-grid">
        <?php for ($i = 1; $i < 6; $i++) { ?>

            <div class="col-md-5th col-sm-4 mix development mix_all" style="display: block;  opacity: 1;"><!-- item -->

                <div class="item-box">
                    <figure>
                        <span class="item-hover">
                            <span class="overlay dark-5"></span>
                            <span class="inner">

                                <!-- lightbox -->
                                <a class="ico-rounded lightbox" href="<?php echo SITE_URL; ?>user/<?php echo $pageData["box" . $i . "_img"]; ?>" data-plugin-options="{&quot;type&quot;:&quot;image&quot;}">
                                    <span class="fa fa-plus size-20"></span>
                                </a>

                                <!-- details -->
                                <a class="ico-rounded" href="#">
                                    <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                </a>

                            </span>
                        </span>
                        <img class="img-responsive" src="<?php echo SITE_URL; ?>user/team_thumb/<?php echo $pageData["box" . $i . "_img"]; ?>" width="600" height="399" alt="">
                    </figure>
                </div>

            </div><!-- /item -->
        <?php } ?>





    </div>
    <!-- /PORTFOLIO ITEMS -->

</section>
<!-- /PARALLAX -->





<!-- CLIENTS -->
<section class="padding-xs">
    <div class="container">

        <header class="text-center margin-bottom-10">
            <h3><?php echo $pageData["cap_2_title"]; ?></h3>
        </header>

        <hr class="margin-bottom-60" />

        <ul class="row clients-dotted list-inline">
            <?php
            $para2 = explode(":", $pageData["cap_2"]);
            for ($i = 0; $i < count($para2); $i++) {
                if ($para2[$i] != '') {
                    ?>
                    <li class="col-md-5th col-sm-5th col-xs-6">
                        <a href="#">
                            <img class="img-responsive" src="<?php echo SITE_URL; ?>user/<?php echo $para2[$i]; ?>" alt="client" />
                        </a>
                    </li>
                <?php }
            } ?>
        </ul>

    </div>
</section>
<!-- /CLIENTS -->
<?php
require_once 'includes/footer.php';
?>
