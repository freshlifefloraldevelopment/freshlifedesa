<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php

  // PO  2018/07/13

require_once("../config/config_gcp.php");

$userSessionID = $_SESSION["buyer"];

################QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################

$pageId = 16;//VARIETY PAGE ID 

$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";

$pageQuery = mysqli_query($con, $pageSql);

$pageData = mysqli_fetch_assoc($pageQuery);

#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################



#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################

$cssHeadArray = array(SITE_URL.'../includes/assets/css/essentials-flfv3.css', SITE_URL.'/includes/assets/css/layout-flfv3.css', 

                            SITE_URL.'../includes/assets/css/header-1.css', SITE_URL.'/includes/assets/css/layout-shop.css', SITE_URL.'/includes/assets/css/color_scheme/blue.css' );

$jsHeadArray = array();

#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################

    

require_once '../includes/header.php';

?>

<style type="text/css">
  div.mega-price-table h5 {
    background-color: rgba(0, 0, 0, 0.03);
    color: #666;
    display: block;
    font-size: 15px;
    font-weight: 700;
    margin: 0;
    padding: 7px;
    text-align: center;
  }
  div.mega-price-table .pricing{
    margin-top: 1px!important;
  }
  #v5 .pricing-title{
    height: 133px;
  }
  ul#product-size-dd li{
    width: 100%;
  }
  div.mega-price-table .pricing-head small {
    background-color: inherit;
}



</style>

    <section class="page-header page-header-xs">

        <div class="container">

            <?php

            $productId = $_GET['id'];#100
            $growerId = $_GET['id_grow'];

            $sel_product = "SELECT p.id , 
                                   p.tab_desc1 , 
                                   p.tab_desc2 , 
                                   substr(p.tab_desc2,1,78) short_desc2,
                                   substr(p.tab_desc2,79,80) short_desc3, 
                                   p.name , 
                                   p.image_path , 
                                   s.description ,
                                   c.name name_cat
                              FROM product p
                              JOIN subcategory s ON p.subcategoryid = s.id 
                              JOIN category c ON p.categoryid = c.id 
                             WHERE p.id = $productId";

            $rs_products = mysqli_query($con, $sel_product);

            $product = mysqli_fetch_assoc($rs_products);

            $name = preg_replace("![^a-z0-9]+!i", "-", trim($product["name"]));
            $name_cat = preg_replace("![^a-z0-9]+!i", "-", trim($product["name_cat"]));            

            $tab2 = $product["tab_desc2"];
            
            $sel_grower = "select substr(growers_name,1,18) namegrow  from growers where id = $growerId ";
            $rs_grower = mysqli_query($con, $sel_grower);
            $grower = mysqli_fetch_assoc($rs_grower);            
            ?>

            <h1><?php echo $name; ?></h1>


            <!-- breadcrumbs -->

            <ol class="breadcrumb">

                <li><a href="#">Home</a></li>

                <li><a href="#">Shop</a></li>

                <li class="active">Single</li>

            </ol><!-- /breadcrumbs -->


        </div>

    </section>

    <!-- /PAGE HEADER -->


    <!--MAIN BODY SECTION-->

    <section>

        <div class="container">

            <div class="row">

                <!-- IMAGE -->

                <div class="col-lg-4 col-sm-4">

                    <div class="thumbnail relative margin-bottom-3">

                        <?php

                            $absolute_path = SITE_URL.$product['image_path'];
                        ?>

                        <figure id="zoom-primary" class="zoom" data-mode="mouseover">

                            <a class="lightbox bottom-right" href="<?php echo $absolute_path; ?>" data-plugin-options='{"type":"image"}'><i class="glyphicon glyphicon-search"></i></a>

                            <img class="img-responsive" src="<?php echo $absolute_path; ?>" width="1200" height="1500" alt="This is the product title" />

                        </figure>

                    </div>

                </div>

                <!-- /IMAGE -->

                <!-- ITEM DESC -->

                <div class="col-lg-5 col-sm-8">

                    <?php
                                        
                $sel_boxes_type ="select id, grower_id, product_id, boxes, chk, box_value, categoryid, colorid, sid
                                    from grower_product_box
                                   where grower_id=$growerId
                                     and product_id=$productId";
          
                                        $boxes = mysqli_query($con, $sel_boxes_type);                                                                                                          
                    ?>

                    <!-- Templete -->
                  
							<div class="heading-title heading-border-bottom">
								<h3>Packing</h3>
							</div>

							<div class="toggle toggle-transparent-body toggle-accordion">

                                                        <?php   while ($btype = mysqli_fetch_assoc($boxes)) {?>    
                                                            
							<div class="toggle">

								<label><?php echo $btype["box_value"]?></label>
                                                             <?php   
                                                                    $sel_hb="SELECT s.name AS size_name,
                                                                                    IFNULL(fe.name, 'features') as featurename ,
                                                                                    gs.bunch_value,
                                                                                    gs.is_bunch,
                                                                                    bt.name as caja
                                                                               FROM grower_product_bunch_sizes gs
                                                                               LEFT JOIN grower_product_box_packing  gr ON( gr.sizeid=gs.sizes AND gr.bunch_size_id=gs.bunch_sizes AND gr.growerid=gs.grower_id AND gr.prodcutid=gs.product_id)
                                                                               LEFT JOIN sizes AS s     ON gs.sizes = s.id
                                                                               LEFT JOIN boxes AS b     ON b.id     = gr.box_id
                                                                               LEFT JOIN boxtype AS bt  ON b.type   = bt.id
                                                                               LEFT JOIN features AS fe ON fe.id    = gr.feature
                                                                              WHERE gs.grower_id=$growerId
                                                                                AND gs.product_id=$productId  
                                                                                AND b.id = '" . $btype['boxes'] . "'
                                                                                AND gs.bunch_value != 0
                                                                              GROUP by s.name , fe.name , gs.bunch_value, gs.is_bunch, bt.name ";
          
                                                                            $dataMedidahb = mysqli_query($con, $sel_hb);                                                                                                                               
                                                                    while ($dthb = mysqli_fetch_assoc($dataMedidahb)) 
                                                                          {?>                                                                                                                                    
								<div class="toggle-content"> 
										<p><?php echo $dthb["size_name"] . " cm ". $dthb["featurename"]." ".$dthb["bunch_value"]." st/bu " . $dthb["is_bunch"]?></p>
								</div>
                                                             <?php   }?>                                                               

							</div>

                                                        <?php   }?>                                                                        
							</div>                    
                    
                    <!-- / Templete -->

                    <!-- <hr /> ultima linea -->

                </div>

                <!-- /ITEM DESC -->

            </div>
           
        </div>

    </section>
    

    <?php 

    require_once '../includes/footer.php'; 

    ?>

<!-- PAGE LEVEL SCRIPTS -->

<script type="text/javascript" src="/includes/assets/js/view/demo.shop.js"></script>
<script type="text/javascript" src="/assets/js/Chart.js"></script>
<script src="/assets/js/highcharts.js"></script>
<script src="/assets/js/exporting.js"></script>
<?php

$pageSql_h="SELECT grower_id,verities_id,market_type,price,country,date,entry_date FROM historic_price WHERE verities_id='".$_REQUEST['id']."' AND date < Now() and date > DATE_ADD(Now(), INTERVAL- 6 MONTH)  order by date";
$pageQuery_h = mysqli_query($con, $pageSql_h);

$month_Array="";
$str_final="";
$i=0;
$rawdata = array();

while ($row_Data_h = mysqli_fetch_array($pageQuery_h)) {

    $month_Array=date("m",strtotime($row_Data_h['date']));
    $day_Array=date("d",strtotime($row_Data_h['date']));
    $year_Array=date("Y",strtotime($row_Data_h['date']));
    $ts = mktime(0, 0, 0, $month_Array,$day_Array, $year_Array);
    $str_final.=$ts.",".$row_Data_h['price']."=====";
    $price_array.=$row_Data_h['price'].",";

     $rawdata[$i] = $row_Data_h;
    $i++;

}
for($i=0;$i<count($rawdata);$i++){
    $time = $rawdata[$i]["date"];
    $date = new DateTime($time);
    $rawdata[$i]["time"]=$date->getTimestamp()*1000;
}

$f_month_Array=trim($month_Array,","); 
$f_month_Array=trim($str_final,"====="); 
echo $f_month_Array;
?>

<script type="text/javascript">

var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
var fmonth_Array='<?php echo $f_month_Array;?>';
var split_array=fmonth_Array.split("=====");
var ftotal_price='<?php echo $price_array;?>';
var split_price=ftotal_price.split(",");
var month=[];

for(i=0;i<split_array.length;i++){
    var str=split_array[i].split(",");
    var final_string=str[0]+","+parseInt(str[1]);
    month.push([final_string]);
}

var price_val=[];

for(j=0;j<split_price.length;j++){
    price_val.push(split_price[j]);    
}

var lineChartData = {

    labels : month,
    datasets : [
        {

            label: "Stading Market",
            fillColor : "rgba(220,220,220,0.2)",
            strokeColor : "rgba(220,220,220,1)",
            pointColor : "rgba(220,220,220,1)",
            pointStrokeColor : "#fff",
            pointHighlightFill : "#fff",
            pointHighlightStroke : "rgba(220,220,220,1)",
            data : price_val
        }
    ]

};

$(function(){
    Highcharts.setOptions({
         global: { 
        useUTC: false
    }
    });
    jQuery('#container').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Price'
        },
        
        xAxis: {
                type: 'datetime',
                tickPixelInterval: 150,
                dateTimeLabelFormats: {
                    month: '%Y',
                    year: '%Y'
                }

        },
        yAxis: {
            title: {
                text: 'Price Value'
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%e. %b}: {point.y:.2f}'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

        series: [{
            name: 'Price',
                data: (function() {
                   var data = [];
                    <?php
                        for($i = 0 ;$i<count($rawdata);$i++){
                    ?>
                    data.push([<?php echo $rawdata[$i]["time"];?>,<?php echo $rawdata[$i]["price"];?>]);
                    <?php } ?>
                return data;
                })()
            
        }]
    });

    jQuery(".live_price").css('visibility','hidden');

    jQuery("#open_pop").click(function(){

        jQuery(".live_price").css('visibility','visible').show("slow");

    });
    jQuery(".close").click(function(){

        jQuery(".live_price").css('visibility','hidden').show("slow");

    });


    var pid = "<?php echo $_GET['id']; ?>";
    $.ajax({

            url:"/ajax/pagination.php",

            type:"POST",

            data:"action=varietySubPageGrowersPagination&page=1&pid="+pid,

            cache: false,

            success: function(response){

                $('#grower-ajax-pagination-container').html(response);
            }
    });  

    $('#grower-ajax-pagination-container').on('click','.page-numbers',function(){

        $page = $(this).attr('href');
        $pageind = $page.indexOf('page=');
        $page = $page.substring(($pageind+5));

        $.ajax({

            url:"ajax/pagination.php",
            type:"POST",
            data:"action=varietySubPageGrowersPagination&page="+$page,
            cache: false,

            success: function(response){           

                $('#grower-ajax-pagination-container').html(response);       

            }       

        });

    return false;

    });
    
});  

$("#graph_year").change(function(){

    var selected_year=$(this).val();
    var verities_id="<?php echo $_REQUEST['id']?>";

     $.ajax({

        url:"ajax/graph_data.php",
        type:"POST",
        data:"year="+selected_year+"&verities_id="+verities_id,
        cache: false,

        success: function(response){ 

            var res_val=response.split("#####");                
            var fmonth_Array=res_val[0];
            var split_array=fmonth_Array.split(",");
            var ftotal_price=res_val[1];
            var split_price=ftotal_price.split(",");
            var month=[];

            for(i=0;i<split_array.length;i++)     {

                month.push(split_array[i]);
            }

            var price_val=[];

            for(j=0;j<split_price.length;j++)   {
                price_val.push(split_price[j]);    
            }

            var lineChartDataAjax = {

                labels : month,
                datasets : [

                    {
                        label: "Stading Market",
                        fillColor : "rgba(220,220,220,0.2)",
                        strokeColor : "rgba(220,220,220,1)",
                        pointColor : "rgba(220,220,220,1)",
                        pointStrokeColor : "#fff",
                        pointHighlightFill : "#fff",
                        pointHighlightStroke : "rgba(220,220,220,1)",
                        data : price_val
                    }
                ]
            }

            var ctx_Ajax = document.getElementById("lineChartCanvas").getContext("2d");

            window.myLine = new Chart(ctx_Ajax).Line(lineChartDataAjax, {

                responsive: true

            });

        }       

    });

});
var myLineChart=null;
function getPriceDetails(size_id,product_id_k)
{
  $("#product-size-dd").toggle();
  $("#average_price").html("");
  var ctx_Ajax1 = document.getElementById("lineChartCanvas").getContext("2d");
  if(myLineChart!=null){
        myLineChart.destroy();
    }
   $.ajax({
        url:"getPriceDetailsVariety.php",
        async: false,
        type:"POST",
        data:"action_type=getproductdetails&size_id="+size_id+"&product_id_k="+product_id_k,
        success: function(response){ 
            var res_val=response.split("#####");             
            var fmonth_Array=res_val[0];
            var split_array=fmonth_Array.split(",");
            var ftotal_price=res_val[1];
            var split_price=ftotal_price.split(",");

            if(res_val[2] != "")  {
              var res_price_ave = parseFloat(res_val[2]).toFixed(2);
              $("#average_price").html("($"+res_price_ave+")");
            } 
            var month=[];
            for(i=0;i<split_array.length;i++)      {
                month.push(split_array[i]);
            }

            var price_val=[];
            for(j=0;j<split_price.length;j++)
            {

                price_val.push(split_price[j]);    

            }
            var lineChartDataAjax1 = {
                labels : month,
                datasets : [
                    {
                        label: "Stading Market",
                        fillColor : "rgba(220,220,220,0.2)",
                        strokeColor : "rgba(220,220,220,1)",
                        pointColor : "rgba(220,220,220,1)",
                        pointStrokeColor : "#fff",
                        pointHighlightFill : "#fff",
                        pointHighlightStroke : "rgba(220,220,220,1)",
                        data : price_val
                    }
                ]
            }

            myLineChart = new Chart(ctx_Ajax1).Line(lineChartDataAjax1, {
          responsive: true,
          bezierCurveTension : 0.3
      });

      window.myLine = myLineChart;
    
            $("#product-size-dd").hide();
        }
    });    
}
$("#product-type-dd li a").click(function(){
  $("#product-box-type").html($(this).attr("data-val"));
  $("#hdn_box_type").val($(this).attr("pass_box_type"));
  $("#product-type-dd").toggle();
});
$("#product-size-dd li a").click(function(){
  $("#hdn_product_size").val($(this).attr("data-val"));
});

$("#product-qty-dd li a").click(function(){
  $("#hdn_product_qty").val($(this).attr("data-val"));
  $("#product-qty-dd").toggle();
});

$("#product-size-dd-box").click(function(){
  $("#product-size-dd").toggle();
});
$("#product-type-dd-box").click(function(){
    $("#product-type-dd").toggle();
});
$("#product-qty-dd-box").click(function(){
  $("#product-qty-dd").toggle();
});



function sendRequestPage(){
  var pass_box_type = $("#hdn_box_type").val();
  var product_size = $("#hdn_product_size").val();
  var product_qty = $("#hdn_product_qty").val();

  if(pass_box_type != "" && product_size !="" && product_qty != ""){
    var product_id = "<?php echo $_REQUEST['id'];?>";
    window.location.href = "<?php echo SITE_URL?>"+"buyer/name-your-price.php?pass_box_type="+pass_box_type+"&product_size="+product_size+"&product_qty="+product_qty+"&product_id="+product_id;
  }else{
    alert("Please select all option.");
  }
} 

</script>
<script>
  function login_status(){
    var is_login = true;
    <?php 
      if($_SESSION['login'] == ''){
    ?>
        is_login = false;
    <?php
      }
    ?>

  }
</script>
<script type="text/javascript">
  $(document).ready(function(){
    //$("#grow").trigger('click');
    $("#grow").get(0).click();
    $("#subs").get(0).click();
  })
</script>
