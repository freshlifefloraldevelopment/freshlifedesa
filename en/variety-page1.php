<?php
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 17 Feb 2021
Structure MarketPlace previous to buy
Add Protection SQL INY, XSS
**/


?>
<head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<?php


require_once("../config/config_gcp.php");

require_once("../functions/functions.php");



#############QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################
$svar=0;
$pageId = 1; //VARIETY PAGE ID
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL . '/includes/assets/css/essentials-flfv3.css', SITE_URL . '/includes/assets/css/layout-flfv3.css'
    , SITE_URL . '/includes/assets/css/header-4.css'
//    , SITE_URL . '/includes/assets/css/color_scheme/blue.css'
    , 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css'
    , SITE_URL . '/includes/assets/css/layout-datatables.css'
    , SITE_URL . '/includes/assets/css/token-input-facebook.css', SITE_URL . '/includes/assets/css/token-input.css'
    );
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################


if($_SESSION["buyer"]){

  $userSessionID = $_SESSION["buyer"];
}
  else{

    $userSessionID="N/A";
  }

require_once '../includes/header.php';
?>


<style type="text/css">
    .active{
        color: #8a2b83 !important;
        font-weight: bold;
    }
    .tag.shop-color{
        border: 1px solid #666;
        height: 23px !important;
        width: 23px !important;
    }

    .red {
  background: #8A2984;
   border-radius: 0.8em;
  -moz-border-radius: 0.8em;
  -webkit-border-radius: 0.8em;
  color: #ffffff;
  display: inline-block;
  font-weight: bold;
  line-height: 1.6em;
  margin-right: 15px;
  text-align: center;
  width: 1.6em;
}

    .overlay {
        position: absolute;
        bottom: 0;
        background: rgb(0, 0, 0);
        background: rgba(0, 0, 0, 0.5); /* Black see-through */
        color: #f1f1f1;
        width: 100%;
        transition: .5s ease;
        opacity:0;
        color: white;
        font-size: 20px;
        padding: 20px;
        text-align: center;
}

</style>


<style type="text/css">
.navbar {
  margin-bottom: 0;
  background-color: #f4511e;
  z-index: 9999;
  border: 0;
  font-size: 12px !important;
  line-height: 1.42857143 !important;
  letter-spacing: 4px;
  border-radius: 0;
  font-family: Montserrat, sans-serif;
}
.navbar li a, .navbar .navbar-brand {
  color: #f4511e !important;
}
.navbar-nav li a:hover, .navbar-nav li.active a {
  color: #f4511e !important;
  background-color: #f4511e !important;
}
.navbar-default .navbar-toggle {
  border-color: transparent;
  color: #f4511e !important;
}
.dropdown li a {
  color: #f4511e !important;
}
.navbar-default .navbar-nav>.open>a.dropdown-toggle{
  background-color: #f4511e !important;
}

</style>


    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>



<section class="page-header page-header-xs">
    <div class="container">

        <!-- breadcrumbs -->
        <ol class="breadcrumb breadcrumb-inverse">
            <li><a href="<?php echo SITE_URL; ?>">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Variety</li>
        </ol><!-- /breadcrumbs -->


    </div>


</section>
<!-- /PAGE HEADER -->
<section class="page-header page-header-xs">
    <div class="container">

        <!-- breadcrumbs -->
        <span class="logo pull-left">
            <a href="https://app.freshlifefloral.com/">
                <img src="https://app.freshlifefloral.com/user/logo.png" alt="admin panel" height="60" width="200"> </a>
            <a>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp</a>
            <a href="<?php echo SITE_URL; ?>"><FONT SIZE=4>Home</font></a>
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>
            <a href="<?php echo SITE_URL."en/variety-page.php"; ?>"><FONT SIZE=4>Browse</font></a>
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>
            <a href="<?php echo SITE_URL."en/growers-page.php"; ?>"><FONT SIZE=4>Growers</font></a>
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>
            <a href="<?php echo "https://www.freshlifefloral.com/blog/how-it-works-buyers"; ?>"><FONT SIZE=4>How it Works</font></a>
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>
            <a href="<?php echo "https://www.freshlifefloral.com/blog"; ?>"><FONT SIZE=4>Blog</font></a>
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>
            <a href="<?php echo "https://www.freshlifefloral.com/blog/contact-us"; ?>"><FONT SIZE=4>Contacts</font></a>
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>
            <?php
            if($userSessionID!="N/A"){
             ?>
            <div class="btn-group">
                <button type="button" name="price_modal" id="price_modal" class="btn btn-default btn-xs" style="background:#34495E;color:white" data-toggle="modal" data-target=".price_modal">  My cart&nbsp;&nbsp;<img src="../includes/assets/images/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
						            &nbsp;&nbsp;<span class="red" id="countProductosPrev">0</span>



                </button>

            </div>

            <?php
          }
            ?>
        </span>

        <input type="hidden" id="sessionStorage" name="sessionStorage" />

        <!-- /breadcrumbs -->






    </div>


</section>
<!-- -->
<section>

    <div class="container">

                <!--Price Modal Start--------------------------------------------------------->

                <div class="modal fade price_modal"  role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">


                    <div class="modal-dialog modal-lg">

                        <div class="modal-content">

                            <div class="modal-body request_product_modal_hide">

                                <table class="table table-hover">

                                <!--<br>-->
                                <!--</div>-->


                                        <h1 style="clear: both;margin-top: 20px;margin-left: 14px;margin-bottom: 0px;">Cart Products</h1>
                                        <br>
                                        <div class="product_price_add2"></div>

                                          <thead>
                                              <tr>
                                                  <th><strong>Product/Size</strong></th>
                                                  <th><strong>Boxes</strong></th>
                                                  <th><strong>Steams</strong></th>
                                                  <th><strong>Price</strong></th>
                                                  <th><strong>Image</strong></th>
                                                  <th><strong>Options</strong></th>
                                              </tr>
                                          </thead>
                                              <tbody id="loadDataItemsPrevious">

                                              </tbody>






                             </table>
                                <!-- Comment (6) -->
                                <h2 style="clear: both;margin-top: 20px;margin-left: 14px;margin-bottom: 0px;">Shopping Price Summary</h2><br>

                                <table style="width:50%" class="table table-hover">
                                  <tbody id="loadDataPricesSummary">

                                  </tbody>

                                </table>
                                </div>



                            <div class="modal-footer request_product_modal_hide_footer">
                                <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            <!-- <button style="background:#8a2b83!important;" onclick="requestProduct1('<?php //echo $ir ?>')" class="btn btn-primary" type="button">Checkout</button> -->
                                <button data-dismiss="modal" style="background:#8a2b83!important;" onclick="requestProduct1()" class="btn btn-primary" type="button" id="checkout_modal" class="btn btn-default btn-xs" style="background:#34495E;color:white" data-toggle="modal" data-target=".checkout_modal">Checkout</button>

                            </div>
                        </div>
                    </div>

                </div>
                <!--Price Modal End------------------------------------------------------------>

                <!--Price Modal Continue Checkout--------------------------------------------------------->

                <div class="modal fade checkout_modal"  role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">


                    <div class="modal-dialog modal-lg">

                        <div class="modal-content">

                            <div class="modal-body request_product_modal_hide">



                                <div class="row">
                                  <div class="col-md-4 order-md-2 mb-4">
                                    <h3 class="d-flex justify-content-between align-items-center mb-3">
                                      Your cart
                                      <span style="background:#8A2984; color:white" class="badge">3</span>
                                    </h3>
                                    <ul class="list-group mb-3">
                                      <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                          <h5 class="my-0">Aqua</h5>
                                          <small class="text-muted">Aqua / 100 [cm]</small>
                                        </div>
                                        <span class="text-muted">$1150</span>
                                      </li>
                                      <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                          <h5 class="my-0">Athena</h5>
                                          <small class="text-muted">Athena / 70 [cm]</small>
                                        </div>
                                        <span class="text-muted">$600</span>
                                      </li>
                                      <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                          <h5 class="my-0">Movie Star</h5>
                                          <small class="text-muted">Movie Star / 70 [cm]</small>
                                        </div>
                                        <span class="text-muted">$567</span>
                                      </li>

                                      <li class="list-group-item d-flex justify-content-between">
                                        <font color="#000000">
                                        <span>Total (USD)</span>
                                        <strong>$2,850.00</strong>
                                      </font>
                                      </li>
                                    </ul>


                                  </div>
                                  <div class="col-md-8 order-md-1">
                                      <h3 class="mb-3">Payment</h3>
                                    <form action="../buyer/CreateCharge.php" method="post" id="payment-form">

                                      <hr class="mb-4">

                                        <div class="card-errors"></div>
                                        <div class="custom-control custom-radio">

                                                  <input type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                                                  <strong><font size="3">Credit card</font></strong>

                                      </div>
                                        <hr class="mb-4">
                                      <div class="row">


                                        <div class="col-md-6 mb-3">
                                          <label for="cc-name">Name on card</label>
                                          <input type="text" id="cc-card-name"><br>
                                          <small class="text-muted">Full name as displayed on card</small>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                          <label for="cc-name">Phone number</label>
                                          <input type="text" id="cc-phone-number"><br>
                                          <small class="text-muted">Include area code</small>
                                        </div>



                                      </div>

                                      <div class="row">



                                        <div class="col-md-8 mb-12">
                                          <label for="cc-name">Credit card</label>
                                          <div id="card-element" class="field is-empty"></div>
                                        </div>


                                      </div>


                                      <hr class="mb-4">

                                      <button onclick="revForm()" type="submit" class="btn btn-primary">
                                        <i class="fa fa-cart-plus"></i>
                                        Buy it now</button>
                                    </form>
                                    <script src="https://js.stripe.com/v3/"></script>
                                  </div>
                                </div>

                                <table>
                                      <tr>
                                      <td>
                                        <div class="row">
                                          <div class="col-md-8 order-md-2 mb-4">
                                    <ul class="fa-ul" style="display: inline;">
                                        <font size="5" color="#1d9d74">Secure Payments &nbsp;&nbsp;<i class="fa fa-shield" aria-hidden="true"></i></font><br><br>
                                    </ul>
                                        <font size="1">
                                        <ul class="fa-ul" style="display: inline;">
                                          <li style="display: inline;"><i class="fa fa-check" aria-hidden="true"></i>Data protected</li>
                                          <li style="display: inline;">&nbsp;&nbsp;</li>
                                          <li style="display: inline;"><i class="fa fa-check" aria-hidden="true"></i>Consumer rights</li>
                                          <li style="display: inline;">&nbsp;&nbsp;</li>
                                          <li style="display: inline;"><i class="fa fa-check" aria-hidden="true"></i>Refund politics</li>
                                          <li style="display: inline;">&nbsp;&nbsp;</li>
                                        </ul>

                                        <ul class="fa-ul" style="display: inline;">

                                          <li style="display: inline;"><i class="fa fa-check" aria-hidden="true"></i>Secure Payments</li>
                                          <li style="display: inline;">&nbsp;&nbsp;</li>
                                          <li style="display: inline;"><i class="fa fa-check" aria-hidden="true"></i>Guaranteed safe</li>
                                        </ul>
                                      </font>
                                    </div>
                                    </div>

                                      </td>
                                      <td align="left"><img src="../images/payments.png" width="270px" height="90px"/></td>
                                      </tr>

                                </table>


                                <footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">© 2021 Fresh Life Floral</p>
  </footer>



                              </div>

                        </div>
                    </div>

                </div>
                <!--Price Modal End------------------------------------------------------------>

        <?php
                $vari_so = "select p.name,p.id , s.name as subcate
                             from product p
                             inner join grower_product gp on p.id = gp.product_id
                             inner join growers g         on gp.grower_id = g.id
                             inner join subcategory s on p.subcategoryid = s.id
                            where g.active='active'
                              and p.status = 0
                            group by p.id
                            order by p.name ";


                    $rs_variety = mysqli_query($con,$vari_so);

        ?>
        <!-- Portfolio Filter 1-->

        <?php
        if($userSessionID!="N/A"){
         ?>
        <a href="../buyer/buyers-account.php?menu=1">
        <div class="alert alert-success" role="alert" >
          <i class="main-icon fa fa-server" aria-hidden="true"></i> Back to  <strong>Administration Panel</strong>
  </div></a>
      <?php
      }
       ?>

        <div class="text-center">
                        <select class="form-control select2" id="vari_change">

                                    <option value="">Varieties</option>
                            <?php

                            while ($row_variedad = mysqli_fetch_array($rs_variety)) {

                            ?>
                                        <option value="<?php echo $row_variedad['id'] ?>"><?php echo $row_variedad['name']." ".$row_variedad['subcate'] ?></option>

                            <?php } ?>

                        </select>
        </div>

        <br>
        <!-- /Portfolio Filter 2-->
        <!--div class="divider divider-dotted"></div-->

        <div class="row">

            <!-- LEFT 10-->
            <div class="col-md-3 col-sm-3">

                <!-- SIDE NAV -->
                <div class="margin-bottom-60">


                    <h4>FILTERS.</h4>


        <form  method="get" action="">

                        <label class="size-12 margin-top-10"> </label>
                        <div class="panel-default">
                                <!-- <input type="text" placeholder="Keywords" id="product_auto_search" class="form-control" />
                                <input type="hidden" id="hdnmyval" name="hdnmyval" style="border:1px solid;" value="" /> -->
<?php

                $vari_s = "select p.name,p.id , s.name as subcate
                             from product p
                             inner join grower_product gp on p.id = gp.product_id
                             inner join growers g         on gp.grower_id = g.id
                             inner join subcategory s on p.subcategoryid = s.id
                            where g.active='active'
                              and p.status = 0
                            group by p.id
                            order by p.name ";


                    $rs_vari = mysqli_query($con,$vari_s);


?>
                    <!--div class="panel-default">
                        <select class="form-control select2" id="vari_change">

                                    <option value="">All..</option>
                            <?php
                                    $svar=0;
                            while ($row_vari = mysqli_fetch_array($rs_vari)) {
                                    $svar=0;

                            ?>
                                        <option value="<?php echo $row_vari['id'] ?>"><?php echo $row_vari['name']." ".$row_vari['subcate'] ?></option>

                            <?php } ?>

                        </select>
                    </div-->

                            <!-- <div id="divResult"></div> -->

                        </div>
                        <div class="btn-submit" style="text-align: right; margin-top: 10px;display:none;">
                            <a href="javascript:void(0)" id="btn-keyword" style="padding: 5px; color: white ; border-radius: 3px; background: rgb(164, 186, 61) none repeat scroll 0% 0%; position: relative; margin-top: 38px;"  class="keyword-btn">Submit</a>
                        </div>

                        <!-- <label class="size-12 margin-top-10">Category</label>
<?php        /* 1)
                $sel_products_sub = "select s.*
                                       from subcategory s
                                       left join grower_product gp on s.id=gp.subcaegoryid
                                       left join growers g on gp.grower_id=g.id
                                      where g.active='active'
                                      group by s.id
                                      order by s.name";
               */


                $sel_products_sub = "select s.*
                                       from grower_product gp
                                       inner join product  p on gp.product_id = p.id
                                        left join subcategory s on p.subcategoryid = s.id
                                        left join growers  g on gp.grower_id  = g.id
                                       where g.active='active'
                                       group by s.id
                                       order by s.name";

                        $rs_products_sub = mysqli_query($con,$sel_products_sub);
?>
                        <div class="panel-default">
                                <select class="form-control select2" id="sub_cat_change">

                                        <option value="">All</option>
                        <?php
                        while ($sub_cat = mysqli_fetch_array($rs_products_sub)) {

                            ?>
                                  <option value="<?php echo $sub_cat['id'] ?>"><?php echo $sub_cat['name'] ?></option>
                        <?php } ?>

                                </select>
                        </div>
                        -->

                        <label class="size-12 margin-top-10">Growers.</label>


                        <?php
                                $growers_sql = "select id,growers_name
                                                  from growers
                                                 where active='active'
                                                 order by growers_name";

                                $rs_growers = mysqli_query($con,$growers_sql);
                        ?>
                        <div class="field">
                            <select id="sub_cat_change" class="form-control select2 cls_filter" multiple  name="sub_cat_change" style="width: 100%; diplay: none;">
                                       <option value="341"></option>
                        <?php
                                while ($row_grower = mysqli_fetch_array($rs_growers)) {
                        ?>
                                    <option value="<?php echo $row_grower['id'] ?>"><?php echo $row_grower['growers_name'] ?></option>
                        <?php
                                  } ?>

                            </select>

                            <!--h4 class="margin-top-10"><a href="<?php echo SITE_URL."en/single-growers.php?id=".$catinfo['id'];?>">Go Grower</a></h4-->
                        </div>


                    </form>

                    <!--hr-->

                    <div class="side-nav margin-bottom-20">

                        <div class="side-nav-head">
                            <button class="fa fa-bars"> </button>
                            <h4>ALL CATEGORIES.</h4>
                        </div>
<?php

            $sel_cat = "select p.categoryid ,
                               c.name ,
                               c.id
                         from grower_product gp
                        inner join product  p on gp.product_id = p.id
                         left join category c on p.categoryid = c.id
                         left join growers  g on gp.grower_id  = g.id
                        where g.active='active'
                          and  p.status = 0
                        group by p.categoryid
                        order by c.name";

        $rs_cat = mysqli_query($con,$sel_cat);

?>
           <ul class="list-group list-group-bordered list-group-noicon uppercase">

                <?php
                     while ($row_cat = mysqli_fetch_array($rs_cat)) {
                ?>
                                <li class="list-group-item active" id="main_cat_<?php echo $row_cat['categoryid']; ?>">
                                    <a href="#" onclick="dropCatUpDown(<?php echo $row_cat['categoryid']; ?>)" class="dropdown-toggle"><?php echo $row_cat['name'] ;?></a>
                            <?php

                                     $sel_products_sub = "select s.*
                                                            from grower_product gp
                                                           inner join product  p on gp.product_id = p.id
                                                            left join subcategory s on p.subcategoryid = s.id
                                                            left join growers  g on gp.grower_id  = g.id
                                                           where g.active='active'
                                                             and p.status = 0
                                                             and p.categoryid ='" . $row_cat['categoryid'] . "'
                                                           group by s.id
                                                           order by s.name";

                                    $rs_products_sub = mysqli_query($con,$sel_products_sub);
                            ?>
                                    <ul style="display: none;" class="sub_cat_class" id="sub_cat_<?php echo $row_cat['categoryid']; ?>">

                                    <?php
                                    while ($sub_cat = mysqli_fetch_array($rs_products_sub)) {
                                        $sel_products_cnt = "select p.id as cnt_product
                                                               from product p
                                                               left join grower_product gp on p.id = gp.product_id
                                                               left join growers g on gp.grower_id = g.id
                                                              where g.active       ='active'
                                                                and p.subcategoryid='" . $sub_cat['id'] . "'
                                                                and p.status = 0
                                                              GROUP BY p.id";

                                        $rs_pro_cnt = mysqli_query($con,$sel_products_cnt);
                                        $total_pro_sub = mysqli_num_rows($rs_pro_cnt);
                                        ?>
                                            <li id="sub_li_<?php echo $sub_cat['id']; ?>">
                                                <a href="javascript:void(0);" onclick="GetSubCatData(<?php echo $sub_cat['id']; ?>)">
                                                    <span class="size-11 text-muted pull-right">(<?php echo $total_pro_sub; ?>)</span> <?php echo $sub_cat['name']; ?>
                                                </a>
                                                <input type="hidden" id="toggleState_<?php echo $sub_cat['id']; ?>" value="0" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                    <?php } ?>
                        </ul>

                    </div>

<?php
    $sel_colors = "select * from colors GROUP BY id order by name";
    $rs_colors = mysqli_query($con,$sel_colors);
?>
        <div class="margin-bottom-60" id="color-box">

            <h4>Color</h4>
            <?php
                while ($colors = mysqli_fetch_array($rs_colors)) {
                                $pname = preg_replace("![^a-z0-9]+!i", "-", trim($cat["name"]));

        if ($colors["name"] == "Black") { ?>
            <a style="background-color:#000000" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
        <?php } elseif ($colors["name"] == "Bicolor") { ?>
            <a style="background: linear-gradient(to right, red,orange,yellow,green,blue,indigo,violet);" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
        <?php } elseif ($colors["name"] == "Blue") { ?>
            <a style="background-color:#0000CC" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
        <?php } elseif ($colors["name"] == "Burgundy") { ?>
            <a style="background-color:#45001C" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
        <?php } elseif ($colors["name"] == "Earth Tones") { ?>
            <a style="background-color:#D19C4C" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
        <?php } elseif ($colors["name"] == "Green") { ?>
            <a style="background-color:#009900" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>

        <?php } elseif ($colors["name"] == "Hot Pink") { ?>
            <a style="background-color:#7A313D" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>

        <?php } elseif ($colors["name"] == "Lavander") { ?>
            <a style="background-color:#7A313D" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>

        <?php } elseif ($colors["name"] == "Light Pink") { ?>
            <a style="background-color:#CEB5B0" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>

        <?php } elseif ($colors["name"] == "Orange") { ?>
            <a style="background-color:#FF9900" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>

        <?php } elseif ($colors["name"] == "Peach") { ?>
            <a style="background-color:#D8D97B" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>

        <?php } elseif ($colors["name"] == "Pink") { ?>
            <a style="background-color:#FFCCCC" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>

        <?php } elseif ($colors["name"] == "Purple") { ?>
            <a style="background-color:#990099" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>

        <?php } elseif ($colors["name"] == "Red") { ?>
            <a style="background-color:#FF0000" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>

        <?php } elseif ($colors["name"] == "White") { ?>
            <a style="background-color:#FFFFFF" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>

        <?php } elseif ($colors["name"] == "Yellow") { ?>
            <a style="background-color:#FFFF00" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
        <?php } else {
            ?>
            <a style="background-color:<?php echo $colors["name"]; ?>" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
        <?php }
        ?>

            <?php
            }
            ?>
            <hr>
            <div class="clearfix size-12">
                <a href="javascript:void(0);" onclick="DeleteColor();" class="pull-right glyphicon glyphicon-remove"></a>
                SELECTED COLOR : <span id="selected-color-id"> All Color </span>
            </div>
            <hr>


<?php
    $sel_card = "select gp.id   , gp.idsc , gp.subcategory , gp.size , gp.stem_bunch  , gp.description ,
                      gp.type , gp.feature , gp.factor , gp.id_ficha ,gp.price_adm ,
                      s.name as namesize   , f.name as namefeature , b.name as namestems
                 from grower_parameter gp
                inner JOIN sizes s ON gp.size = s.id
                inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
                 left JOIN features f  ON gp.feature = f.id
                where gp.idsc     = '2'
                  and gp.id_ficha = '1'
                order by s.name";

    $rs_card = mysqli_query($con,$sel_card);
?>

                            <!-- SIZE -->
                            <div class="mb-60">
                                    <h4>SIZE</h4>

            <?php
                while ($sized = mysqli_fetch_array($rs_card)) {

            ?>
                                    <a class="tag" href="javascript:void(0);">
                                            <span class="txt"><?php echo $sized["namesize"]; ?></span>
                                    </a>
            <?php
               }  ?>

                                    <!--a class="tag" href="#">
                                            <span class="txt bold">40</span>
                                    </a-->

                                    <hr />

                                    <div class="clearfix fs-12">
                                            <a class="float-right fa fa-remove" href="#"></a>
                                            SELECTED SIZE: <span id="selected-size-id"> All Size </span>
                                    </div>
                            </div>
                            <!-- /SIZE -->
                    </div>


                </div>
                <!-- /SIDE NAV -->






            </div>

            <!-- RIGHT 20 21-->
            <div class="col-md-9 col-sm-9">


                <div id="portfolio" class="portfolio-gutter" style="min-height:1090px;">

                    <div class="row mix-grid" id="result_data" >



<?php
$sel_products = "select p.*
                   from product p
		   left join grower_product gp on p.id = gp.product_id
		   left join growers g on gp.grower_id = g.id
		  where g.active='active'
                  GROUP BY p.id limit 0,15";

$rs_products = mysqli_query($con,$sel_products);

while ($products = mysqli_fetch_array($rs_products)) {

    $sel_catinfo = "select * from category where id='" . $products['categoryid'] . "'";

    $rs_catinfo = mysqli_query($con,$sel_catinfo);

    $catinfo = mysqli_fetch_array($rs_catinfo);

    $cname = preg_replace("![^a-z0-9]+!i", "-", trim($products["name"]));

    $absolute_path = $_SERVER['DOCUMENT_ROOT'] . "/staging/" . $products['image_path'];

        ?>
                          <div class="container-fluid" id="grower-ajax-pagination-container">

                                <div class="col-lg-4 col-lg-4 mix <?php echo $products['color_id']; ?>"> <!-- item xy>


                                    <!--div class="item-box">
                                        <figure>
                                            <!--span class="item-hover">
                                                <span class="overlay dark-5"></span>
                                                <span class="inner">

                                                </span>
                                            </span-->

                                            <!--div class="item-box-overlay-title">
                                                <h3><?php echo $products["name"]; ?></h3>
                                                <ul class="list-inline categories nomargin">
                                                    <li><a href="#"><?php echo $catinfo['name'].".." ?></a></li>
                                                </ul>
                                            </div-->

                                        <!-- carousel get_subcat.php 12-->
                                    <!--div class="owl-carousel buttons-autohide controlls-over nomargin" data-plugin-options='{"singleItem": true, "autoPlay": false, "navigation": false, "pagination": true, "transitionStyle":"fade"}'>
                                        <div>
                                            <!--img src="<?php echo SITE_URL . "/" . $products['image_path'] ?>" width="300" height="250" alt=""-->
                                        <!--/div>

                                    <!--/div>
                                    <!-- /carousel 13 -->


                                        <!--/figure-->
                                    </div>
                                   </div>
                                </div><!-- /item -->
<?php } ?>


                    </div>
                    <input type="hidden" name="hdn_main_cat" id="hdn_main_cat" value="" />
                    <input type="hidden" name="hdn_sub_cat" id="hdn_sub_cat" value="" />
                    <input type="hidden" name="hdn_selected_color" id="hdn_selected_color" value="" />


                    <div class="animation_image" style="display:none;" align="center">
                        <img src="<?php echo SITE_URL; ?>/includes/assets/images/loaders/5.gif"></div>
                </div>
                <div class="divider divider-dotted"><!-- divider --></div>


                <div class="text-right">

                    <!-- Pagination Default -->

                    <!-- /Pagination Default -->

                </div>

            </div>


        </div>

    </div>


</div>
</section>
<!-- / -->

<!-- SCROLL TO TOP -->
<a href="#" id="toTop"></a>
<?php
require_once '../includes/footer.php';
?>

<script type="text/javascript" src="<?php echo SITE_URL; ?>/includes/assets/js/jquery.tokeninput.js"></script>

<!--Modal-->

<!--/Modal-->

                             <?php
                                function clean($string) {
                                    return preg_replace('/[^A-Za-z0-9 \-]/', '', $string); // Removes special chars.
                                }

                                                $auto_products = "select p.name , p.id
                                                                    from product p
                                                                    left join grower_product gp on p.id=gp.product_id
                                                                    left join growers g on gp.grower_id=g.id
                                                                   where g.active='active'
                                                                   GROUP BY p.id";

                                                $rs_auto = mysqli_query($con,$auto_products);
                                                $product_auto_name = "";
                                                $product_array = array();
                                                $i_cnt = 0;

                                                while ($row_auto = mysqli_fetch_array($rs_auto)) {
                                                    $product_array[$i_cnt]['id'] = $row_auto['id'];
                                                    $product_array[$i_cnt]['name'] = clean($row_auto['name']);
                                                    $i_cnt++;
                                                }

                                                $json_convert = json_encode($product_array);
                                        ?>

<script type="text/javascript">

                    //<!-- JS DATATABLES -->
                            loadScript(plugin_path + "datatables/js/jquery.dataTables.min.js", function () {
                                loadScript(plugin_path + "datatables/dataTables.bootstrap.js", function () {

                                    if (jQuery().dataTable) {

                                        var table = jQuery('#datatable_sample');
                                        table.dataTable({
                                            "columns": [{
                                                    "orderable": false
                                                }, {
                                                    "orderable": true
                                                }],
                                            "lengthMenu": [
                                                [5, 15, 20, -1],
                                                [5, 15, 20, "All"] // change per page values here
                                            ],
                                            // set the initial value
                                            "pageLength": 5,
                                            "pagingType": "bootstrap_full_number",
                                            "language": {
                                                "lengthMenu": "  _MENU_ records",
                                                "paginate": {
                                                    "previous": "Prev",
                                                    "next": "Next",
                                                    "last": "Last",
                                                    "first": "First"
                                                }
                                            },
                                            "columnDefs": [{// set default column settings
                                                    'orderable': false,
                                                    'targets': [0]
                                                }, {
                                                    "searchable": false,
                                                    "targets": [0]
                                                }],
                                            "order": [
                                                [1, "asc"]
                                            ] // set first column as a default sort by asc
                                        });

                                        var tableWrapper = jQuery('#datatable_sample_wrapper');

                                        table.find('.group-checkable').change(function () {
                                            var set = jQuery(this).attr("data-set");
                                            var checked = jQuery(this).is(":checked");
                                            jQuery(set).each(function () {
                                                if (checked) {
                                                    jQuery(this).attr("checked", true);
                                                    jQuery(this).parents('tr').addClass("active");
                                                } else {
                                                    jQuery(this).attr("checked", false);
                                                    jQuery(this).parents('tr').removeClass("active");
                                                }
                                            });
                                            jQuery.uniform.update(set);
                                        });

                                        var table = jQuery('#datatable_colors');
                                        table.dataTable({
                                            "columns": [{
                                                    "orderable": false
                                                }, {
                                                    "orderable": true
                                                }],
                                            "lengthMenu": [
                                                [5, 15, 20, -1],
                                                [5, 15, 20, "All"] // change per page values here
                                            ],
                                            // set the initial value
                                            "pageLength": 5,
                                            "pagingType": "bootstrap_full_number",
                                            "language": {
                                                "lengthMenu": "  _MENU_ records",
                                                "paginate": {
                                                    "previous": "Prev",
                                                    "next": "Next",
                                                    "last": "Last",
                                                    "first": "First"
                                                }
                                            },
                                            "columnDefs": [{// set default column settings
                                                    'orderable': false,
                                                    'targets': [0]
                                                }, {
                                                    "searchable": false,
                                                    "targets": [0]
                                                }],
                                            "order": [
                                                [1, "asc"]
                                            ] // set first column as a default sort by asc
                                        });

                                        var tableWrapper = jQuery('#datatable_sample_wrapper_color');

                                        table.find('.group-checkable').change(function () {
                                            var set = jQuery(this).attr("data-set");
                                            var checked = jQuery(this).is(":checked");
                                            jQuery(set).each(function () {
                                                if (checked) {
                                                    jQuery(this).attr("checked", true);
                                                    jQuery(this).parents('tr').addClass("active");
                                                } else {
                                                    jQuery(this).attr("checked", false);
                                                    jQuery(this).parents('tr').removeClass("active");
                                                }
                                            });
                                            jQuery.uniform.update(set);
                                        });

                                        var table = jQuery('#datatable_gro');
                                        table.dataTable({
                                            "columns": [{
                                                    "orderable": false
                                                }, {
                                                    "orderable": true
                                                }],
                                            "lengthMenu": [
                                                [5, 15, 20, -1],
                                                [5, 15, 20, "All"] // change per page values here
                                            ],
                                            // set the initial value
                                            "pageLength": 5,
                                            "pagingType": "bootstrap_full_number",
                                            "language": {
                                                "lengthMenu": "  _MENU_ records",
                                                "paginate": {
                                                    "previous": "Prev",
                                                    "next": "Next",
                                                    "last": "Last",
                                                    "first": "First"
                                                }
                                            },
                                            "columnDefs": [{// set default column settings
                                                    'orderable': false,
                                                    'targets': [0]
                                                }, {
                                                    "searchable": false,
                                                    "targets": [0]
                                                }],
                                            "order": [
                                                [1, "asc"]
                                            ] // set first column as a default sort by asc
                                        });

                                    }

                                });
                            });


                    jQuery(function () {

                        var auto_p = '<?php echo $json_convert; ?>';
                        var obj = JSON.parse(auto_p);
                        jQuery("#product_auto_search").tokenInput(obj);

                        jQuery("#btn-keyword").click(function () {

                            var selected_id = jQuery("#product_auto_search").val();
                            var dataString = 'searchword=' + selected_id;
                            // alert("(1)");

                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html) {
                                    jQuery("#result_data").html(html);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });

                        });

                        // GROWERS FILTERS  (2) //
                        jQuery("#sub_cat_change").change(function () {
                            var inputSearch = ''; //jQuery("#product_auto_search").val();
                            var subcat_id = jQuery("#sub_cat_change").val();
                            var dataString = 'action=varietySubPageGrowersPagination&page=1&searchword=' + inputSearch + '&growers_id=' + subcat_id;

                            //alert("(2)");
                            $.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>/en/get_param_front.php",
                                data: dataString,
                                success: function (response)                                {
                                    //jQuery("#result_data").html(html);
                                    $('#grower-ajax-pagination-container').html(response);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });
                            //return false;+$page+
                            $('#grower-ajax-pagination-container').on('click','.page-numbers',function(){

                                $page    = $(this).attr('href');
                                $pageind = $page.indexOf('page=');
                                $page    = $page.substring(($pageind+5));

                                $.ajax({
                                    url: "<?php echo SITE_URL; ?>/en/get_param_front.php",
                                    type:"POST",
                                    data:"action=varietySubPageGrowersPagination&page="+$page+"&searchword=" + inputSearch + "&growers_id=" + subcat_id,
                                    cache: false,
                                    success: function(response){
                                        $('#grower-ajax-pagination-container').html(response);
                                            _mixitup();
                                            _owl_carousel();
                                    }
                                });

                            return false;

                            });
                         });
                        //---------------------------

                        jQuery("#availability_check").change(function () {
                            var inputSearch = '';// jQuery("#product_auto_search").val();
                            var subcat_id = jQuery("#sub_cat_change").val();
                            var availability_check = jQuery("#availability_check").val();

                            var dataString = 'searchword=' + inputSearch + '&subcat_id=' + subcat_id + "&availability_check=" + availability_check;
                            //alert("(3)");

                            if (availability_check != '')  {
                                jQuery.ajax({
                                    type: "POST",
                                    url: "<?php echo SITE_URL; ?>search_variety.php",
                                    data: dataString,
                                    success: function (html) {
                                        jQuery("#result_data").html(html);
                                        _mixitup();
                                        _owl_carousel();
                                    }
                                });
                            }

                            return false;
                        });
                        jQuery(".growers_name_chk").change(function () {

                            var already_exits_gro = "";
                            already_exits_gro = jQuery("#hdn_gro_selected_text").val();
                            var already_exits_gro_v = "";
                            already_exits_gro_v = jQuery("#hdn_gro_selected").val();

                            jQuery(".growers_name_chk").each(function () {
                                if (jQuery(this).is(":checked"))  {
                                    if (already_exits_gro == "")  {
                                        jQuery("#hdn_gro_selected_text").val(jQuery(this).attr("checkbox_text"));
                                        jQuery("#hdn_gro_selected").val(jQuery(this).val());
                                    }else{
                                        jQuery("#hdn_gro_selected_text").val(already_exits_gro + "," + jQuery(this).attr("checkbox_text"));
                                        jQuery("#hdn_gro_selected").val(already_exits_gro_v + "," + jQuery(this).val());
                                    }
                                }

                            });
                            grower_checked = jQuery("#hdn_gro_selected_text").val();
                            jQuery("#selected_gro").html("[" + grower_checked + "]");
                        });

                        jQuery("#see_more_gro").click(function () {

                            var color_checked = "";
                            var varieties_checked = "";
                            var grower_checked = "";

                            grower_checked = jQuery("#hdn_gro_selected").val();

                            var varieties_checked = "";

                            var inputSearch = '';//jQuery("#product_auto_search").val();
                            var subcat_id = jQuery("#sub_cat_change").val();
                            var availability_check = jQuery("#availability_check").val();

                            var dataString = 'searchword=' + inputSearch + '&subcat_id=' + subcat_id + "&grower_checked=" + grower_checked + "&color_checked=" + color_checked + "&varieties_checked=" + varieties_checked + "&availability_check=" + availability_check;
                            //alert("(4)");

                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html)  {
                                    jQuery(".bs-example-modal-lg-gro").modal('toggle');
                                    jQuery("#result_data").html(html);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });

                        });

                        jQuery(".color_name_chk").change(function () {

                            var already_exits_color = "";
                            already_exits_color_v = jQuery("#hdn_color_selected").val();
                            already_exits_color = jQuery("#hdn_color_selected_text").val();
                            jQuery(".color_name_chk").each(function () {
                                if (jQuery(this).is(":checked"))  {
                                    if (already_exits_color == "") {
                                        jQuery("#hdn_color_selected_text").val(jQuery(this).attr("checkbox_text"));
                                        jQuery("#hdn_color_selected").val(jQuery(this).val());
                                    }else{
                                        jQuery("#hdn_color_selected_text").val(already_exits_color + "," + jQuery(this).attr("checkbox_text"));
                                        jQuery("#hdn_color_selected").val(already_exits_color_v + "," + jQuery(this).val());
                                    }
                                }

                            });
                            color_checked = jQuery("#hdn_color_selected_text").val();
                            jQuery("#selected_colors").html("[" + color_checked + "]");

                        });

                        jQuery("#see_more_colors").click(function () {
                            var grower_checked = "";
                            jQuery(".growers_name_chk").each(function () {
                                if (jQuery(this).is(":checked"))    {
                                    grower_checked += jQuery(this).val() + ",";
                                }
                            });

                            var color_checked = "";

                            color_checked = jQuery("#hdn_color_selected").val();

                            var varieties_checked = "";

                            jQuery(".varieties_chk").each(function () {
                                if (jQuery(this).is(":checked"))  {
                                    varieties_checked += jQuery(this).val() + ",";
                                }
                            });
                            var inputSearch = jQuery("#product_auto_search").val();
                            var subcat_id = jQuery("#sub_cat_change").val();
                            var availability_check = jQuery("#availability_check").val();

                            var dataString = 'searchword=' + inputSearch + '&subcat_id=' + subcat_id + "&grower_checked=" + grower_checked + "&color_checked=" + color_checked + "&varieties_checked=" + varieties_checked + "&availability_check=" + availability_check;
                            //alert("(5)");

                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html) {
                                    jQuery(".bs-example-modal-lg-color").modal('toggle');
                                    jQuery("#result_data").html(html);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });

                        });

                        jQuery(".varieties_chk").change(function () {

                            var already_exits_vari = "";
                            already_exits_vari = jQuery("#hdn_varieties_selected_text").val();
                            already_exits_vari_v = jQuery("#hdn_varieties_selected").val();

                            jQuery(".varieties_chk").each(function () {
                                if (jQuery(this).is(":checked")) {
                                    if (already_exits_vari == "") {
                                        jQuery("#hdn_varieties_selected_text").val(jQuery(this).attr("checkbox_text"));
                                        jQuery("#hdn_varieties_selected").val(jQuery(this).val());
                                    } else {
                                        jQuery("#hdn_varieties_selected_text").val(already_exits_vari + "," + jQuery(this).attr("checkbox_text"));
                                        jQuery("#hdn_varieties_selected").val(already_exits_vari_v + "," + jQuery(this).val());
                                    }
                                }

                            });

                            varieties_checked = jQuery("#hdn_varieties_selected_text").val();
                            jQuery("#selected_varieties").html("[" + varieties_checked + "]");

                        });

                        jQuery("#see_more_varieties").click(function () {
                            var grower_checked = "";
                            jQuery(".growers_name_chk").each(function () {
                                if (jQuery(this).is(":checked"))  {
                                    grower_checked += jQuery(this).val() + ",";
                                }
                            });

                            var color_checked = "";

                            jQuery(".color_name_chk").each(function () {
                                if (jQuery(this).is(":checked"))   {
                                    color_checked += jQuery(this).val() + ",";
                                }

                            });

                            var varieties_checked = "";

                            varieties_checked = jQuery("#hdn_varieties_selected").val();

                            var inputSearch = jQuery("#product_auto_search").val();
                            var subcat_id = jQuery("#sub_cat_change").val();
                            var availability_check = jQuery("#availability_check").val();

                            var dataString = 'searchword=' + inputSearch + '&subcat_id=' + subcat_id + "&grower_checked=" + grower_checked + "&color_checked=" + color_checked + "&varieties_checked=" + varieties_checked + "&availability_check=" + availability_check;
                             // alert("(6)");

                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html)  {
                                    jQuery(".bs-example-modal-lg-varieties").modal('toggle');
                                    jQuery("#result_data").html(html);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });

                        });

                        jQuery("#vari_change").change(function () {

                            var vari_id = jQuery("#vari_change").val();
                            var dataString = 'varieties_checked=' + vari_id;

                            // alert("(7)");

                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html)   {
                                    var split_html = html.split("#$#$#$#$#$");
                                    jQuery("#result_data").html(split_html[0]);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });


                            return false;


                        });






                        var track_load = 1; //total loaded record group(s)
                        var loading = false; //to prevents multipal ajax loads
                        var total_groups = <?php echo $i_cnt; ?>; //total record group(s)

                        jQuery(window).scroll(function () { //detect page scroll

                            if (jQuery(window).scrollTop() + jQuery(window).height() >= (jQuery(document).height() - 100))  //user scrolled to bottom of the page?
                            {
                                if (track_load <= total_groups && loading == false) //there's more data to load
                                {
                                    if (jQuery('.notfound').html() != "No More Data!")
                                    {
                                        loading = true; //prevent further ajax loading
                                        jQuery('.animation_image').show(); //show loading image

                                        var selected_main_cat = jQuery("#hdn_main_cat").val();
                                        var selected_sub_cat = jQuery("#hdn_sub_cat").val();
                                        var selected_color = jQuery("#hdn_selected_color").val();

                                        jQuery.post('autoload_product.php', {'group_no': track_load, 'cate_id': selected_main_cat, 'selected_color': selected_color, 'selected_sub_cat': selected_sub_cat}, function (data) {

                                            jQuery("#result_data").append(data); //append received data into the element
                                            _mixitup();
                                            _owl_carousel();
                                            jQuery('.animation_image').hide(); //hide loading image once data is received

                                            track_load++; //loaded group increment
                                            loading = false;

                                        }).fail(function (xhr, ajaxOptions, thrownError) { //any errors?

                                            alert(thrownError); //alert with HTTP error
                                            jQuery('.animation_image').hide(); //hide loading image
                                            loading = false;

                                        });
                                    }

                                }
                            }
                        });
                    });

                    function get_subcat(main_id) {

                        jQuery(".list-group-item").removeClass("active");
                        jQuery(".sub_cat_class").hide();
                        jQuery("#hdn_main_cat").val(main_id);
                        jQuery("#main_cat_" + main_id).addClass("active");
                        jQuery("#sub_cat_" + main_id).show();
                        var dataString = 'action=subcatdata&cat_main_id=' + main_id;
                        jQuery.ajax({
                            type: "POST",
                            url: "<?php echo SITE_URL; ?>/en/get_subcat.php",
                            data: dataString,
                            success: function (html)  {
                                var split_html = html.split("#$#$#$#$#$");
                                jQuery("#result_data").html(split_html[0]);
                                jQuery("#color-box").html(split_html[1]);
                                _mixitup();
                                _owl_carousel();
                            }
                        });
                    }

                    // COLOR FILTER  (8) //
                    function get_color_data(id) {
                        var selected_main_cat = jQuery("#hdn_main_cat").val();
                        jQuery("#hdn_selected_color").val(id);
                        var selected_color = jQuery("#hdn_selected_color").val();

                        var dataString = 'action=varietySubPageGrowersPagination&page=1&selected_color=' + selected_color + '&selected_main_cat=' + selected_main_cat;

                        //alert("(8)");

                        $.ajax({
                            type: "POST",
                            url: "<?php echo SITE_URL; ?>/en/get_param_front.php",
                            data: dataString,
                            success: function (response)    {
                               $('#grower-ajax-pagination-container').html(response);
                                //var split_html = html.split("#$#$#$#$#$");
                                //jQuery("#result_data").html(split_html[0]);
                                //jQuery("#selected-color-id").html(split_html[1]);
                                _mixitup();
                                _owl_carousel();
                            }
                        });

                            $('#grower-ajax-pagination-container').on('click','.page-numbers',function(){

                                $page    = $(this).attr('href');
                                $pageind = $page.indexOf('page=');
                                $page    = $page.substring(($pageind+5));

                                $.ajax({
                                    url: "<?php echo SITE_URL; ?>/en/get_param_front.php",
                                    type:"POST",
                                    data:"action=varietySubPageGrowersPagination&page="+$page+"&selected_color=" + selected_color + "&selected_main_cat=" + selected_main_cat,
                                    cache: false,
                                    success: function(response){
                                        $('#grower-ajax-pagination-container').html(response);
                                            _mixitup();
                                            _owl_carousel();
                                    }
                                });

                            return false;

                            });
                    }
                    ////////////////////////////////////////////////////////////

                    function GetSubCatData(id) {

                        var toggleState = jQuery("#toggleState_" + id).val();

                        if (toggleState == 0)  { // ALL CATEGORIES  (9) //

                                    jQuery("#sub_li_" + id).addClass("active");
                                    var already_exite_sub = jQuery("#hdn_sub_cat").val();
                                    if (already_exite_sub == "") {
                                        jQuery("#hdn_sub_cat").val(id);
                                    } else {
                                        jQuery("#hdn_sub_cat").val(id);
                                    }
                                    var selected_sub_cat = jQuery("#hdn_sub_cat").val();

                                    var dataString = "action=varietySubPageGrowersPagination&page=1&subcategory_selected=" + selected_sub_cat;
                                    //alert("(9)");

                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo SITE_URL; ?>/en/get_param_front.php",
                                        data: dataString,
                                        success: function (response) {
                                            jQuery("#toggleState_" + id).val("1");
                                              jQuery("#result_data").html('');
                                             $('#grower-ajax-pagination-container').html(response);
                                            //var split_html = html.split("#$#$#$#$#$");
                                            //jQuery("#result_data").html(split_html[0]);
                                            //jQuery("#color_ul").html(split_html[1]);
                                            _mixitup();
                                            _owl_carousel();
                                        }
                                    });

                                        $('#grower-ajax-pagination-container').on('click','.page-numbers',function(){

                                            $page    = $(this).attr('href');
                                            $pageind = $page.indexOf('page=');
                                            $page    = $page.substring(($pageind+5));

                                            $.ajax({
                                                url: "<?php echo SITE_URL; ?>/en/get_param_front.php",
                                                type:"POST",
                                                data:"action=varietySubPageGrowersPagination&page="+$page+"&subcategory_selected=" + selected_sub_cat ,
                                                cache: false,
                                                success: function(response){
                                                    $('#grower-ajax-pagination-container').html(response);
                                                        _mixitup();
                                                        _owl_carousel();
                                                }
                                            });

                                        return false;

                                        });

                        }else {

                                    jQuery("#toggleState_" + id).val("0");
                                    jQuery("#sub_li_" + id).removeClass("active");

                                    var already_exite_sub = jQuery("#hdn_sub_cat").val();
                                    var split_al = already_exite_sub.split(",");
                                    var final_str = "";

                                    for (i = 0; i < split_al.length; i++)    {
                                        if (split_al[i] != id) {
                                            final_str += split_al[i] + ",";
                                        }
                                    }

                                    jQuery("#hdn_sub_cat").val(jQuery.trim(final_str));
                                    var selected_sub_cat = jQuery("#hdn_sub_cat").val();
                                    var dataString = "subcategory_selected=" + selected_sub_cat;
                                    //alert("(10)");

                                    jQuery.ajax({
                                        type: "POST",
                                        url: "<?php echo SITE_URL; ?>search_variety.php",
                                        data: dataString,
                                        success: function (html)   {
                                            var split_html = html.split("#$#$#$#$#$");
                                            jQuery("#result_data").html(split_html[0]);
                                            jQuery("#color_ul").html(split_html[1]);
                                            _mixitup();
                                            _owl_carousel();
                                        }
                                    });
                        }
                    }

                    function DeleteColor() {
                        jQuery("#selected-color-id").html("<strong>All Color</strong>");
                        var dataString = 'selected_color=&selected_main_cat=';
                        //alert("(11)");

                        jQuery.ajax({
                            type: "POST",
                            url: "<?php echo SITE_URL; ?>search_variety.php",
                            data: dataString,
                            success: function (html)    {
                                var split_html = html.split("#$#$#$#$#$");
                                jQuery("#result_data").html(split_html[0]);
                                _mixitup();
                                _owl_carousel();
                            }
                        });
                    }
                        function dropCatUpDown(id){
                        jQuery(".sub_cat_class").each(function(){
                            if (jQuery(this).find("li").hasClass("active"))   {
                                jQuery("#main_cat_" + id).addClass("active");
                            }
                        });
                    }


                    $(window).load(function(){
                        var main_id="<?php echo $_REQUEST['cat'];?>";

                        if(typeof main_id === "undefined")  {

                        }else{

                            var site_url = "<?php echo SITE_URL; ?>";

                            jQuery("#all_id").removeClass('active');
                            jQuery(".list-group-item").removeClass("active");
                            jQuery(".sub_cat_class").hide();
                            jQuery("#hdn_main_cat").val(main_id);
                            jQuery("#main_cat_" + main_id).addClass("active");
                            jQuery("#sub_cat_" + main_id).show();

                            $.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>/en/get_subcat.php",
                                data: "action=varietySubPageGrowersPagination&action2=subcatdata&page=1&cat_main_id=" + main_id,
                               // success: function (html)  {
                                   success: function (response)  {
                                    $('#grower-ajax-pagination-container').html(response);
                                    //var split_html = html.split("#$#$#$#$#$-");
                                    //jQuery("#result_data").html(split_html[0]);
                                    //jQuery("#color-box").html(split_html[1]);

                                    _mixitup();
                                    _owl_carousel();
                                }
                            });

                            $('#grower-ajax-pagination-container').on('click','.page-numbers',function(){

                                $page    = $(this).attr('href');
                                $pageind = $page.indexOf('page=');
                                $page    = $page.substring(($pageind+5));

                                $.ajax({
                                    url: "<?php echo SITE_URL; ?>/en/get_subcat.php",
                                    type:"POST",
                                    data:"action=varietySubPageGrowersPagination&action2=subcatdata&page="+$page+"&cat_main_id=" + main_id,
                                    cache: false,
                                    success: function(response){

                                        $('#grower-ajax-pagination-container').html(response);
                                            _mixitup();
                                            _owl_carousel();

                                    }
                                });

                            return false;

                            });
                        }
                    });

</script>

<script type="text/javascript">



window.onload=function() {
  var key = localStorage.getItem("idSessionCompras");
			countProductPrevious(key);

      datos = "PreviousProductKey="+key;
      $.ajax({
           type: "POST",
           url: "loadDataItemsPreviuos.php",
           data: datos,
           cache: false,
           success: function(r){
          //  alert(r);
            // return false;
             document.getElementById('loadDataItemsPrevious').innerHTML = r;
           }
       });

      /*
      *** Considerar para nifircar combos y boton d compra ****

      dataProduct =  document.getElementById('id_product'+a).value
       datosP = "dataProduct="+dataProduct;
       $.ajax({
            type: "POST",
            url: "loadDataProducts.php",
            data: datosP,
            cache: false,
            success: function(r){
           //  alert(r);
             // return false;
              document.getElementById('loadDataItemsPrevious').innerHTML = r;
            }
        });
        */


       $.ajax({
            type: "POST",
            url: "loadDataPricesSummary.php",
            data: datos,
            cache: false,
            success: function(r){
             // alert(r);
             // return false;
              document.getElementById('loadDataPricesSummary').innerHTML = r;
            }
        });





		}


    function deleteItemPrevious(itemPrevious){
      datos = "PreviousProductKey="+itemPrevious;
      var key = localStorage.getItem("idSessionCompras");



        $.ajax({
             type: "POST",
             url: "del_temp_cart.php",
             data: datos,
             cache: false,
             success: function(r){
               swal({
                      title: "Are you sure?",
                      text: "Once deleted, you will need to select again this product!",
                      icon: "warning",
                      buttons: true,
                      dangerMode: true,
                    })
                    .then((willDelete) => {
                      if (willDelete) {
                        if(r==1){
                          countProductPrevious(key);
                          loadDataCart(key);
                          swal("Ok! Your product has been deleted!", {
                            icon: "success",
                          });
                        }

                      } else {
                        //Nothing to do!
                      }
                    });

              // document.getElementById('countProductosPrev').innerHTML = r;
             }
         });

    }

    function loadDataCart(key){

      datos = "PreviousProductKey="+key;
      $.ajax({
           type: "POST",
           url: "loadDataItemsPreviuos.php",
           data: datos,
           cache: false,
           success: function(r){
          //  alert(r);
            // return false;
             document.getElementById('loadDataItemsPrevious').innerHTML = r;
           }
       });

       $.ajax({
            type: "POST",
            url: "loadDataPricesSummary.php",
            data: datos,
            cache: false,
            success: function(r){
             //alert(r);
             // return false;
              document.getElementById('loadDataPricesSummary').innerHTML = r;
            }
        });


    }


function selectscategory(){

   removeAllOptions(document.frmproduct.sub_cat_change3);


	addOption(document.frmproduct.sub_cat_change3,"","--Select Box--");

	  <?php
                  $growId = $_POST["sub_cat_change1"];

                    $sel_p="select id,boxes from growers where where id='".$growId."' ";
                    $rs_product = mysqli_query($con,$sel_p);
                    $row_product = mysqli_fetch_array($rs_product);

                    $trim_str     = $row_product['boxes'];
                    $product_id_s =  trim($trim_str,",");

                    $sql_boxes = "SELECT id     , name   , width ,
                                         length , height ,  type
                                    FROM boxes
                                   WHERE id IN ($product_id_s)";

		$res_subcategory=mysqli_query($con,$sql_boxes);

	while($rw_subcategory=mysqli_fetch_array($res_subcategory))	{

	  ?>
		//if(document.frmproduct.sub_cat_change1.value=="<?php echo $rw_subcategory["id"]?>")	{

			addOption(document.frmproduct.sub_cat_change3,"<?php echo $rw_subcategory["id"];?>","<?php echo $rw_subcategory["price"];?>");

			 $('#sub_cat_change3').val('<?php echo $_POST["sub_cat_change3"]?>');

		//}

    <?php
          }
    ?>

	}

	function removeAllOptions(selectbox)	{
		var i;
		for(i=selectbox.options.length-1;i>=0;i--){
			selectbox.remove(i);
		}
	}

	function addOption(selectbox,value,text){
		var optn=document.createElement("OPTION");
		optn.text=text;
		optn.value=value;
		selectbox.options.add(optn);
	}

  function removeDisabledP(a,b){
//  localStorage.clear();
    var x = document.getElementById('sub_cat_change1'+a).value;




    if(x=="0"){
      document.getElementById('sub_cat_change2'+a).disabled=true;
      document.getElementById('sub_cat_change2'+a).value = "0";

      document.getElementById('sub_cat_change3'+a).disabled=true;
      document.getElementById('sub_cat_change3'+a).value = "0";

      document.getElementById('sub_cat_change4'+a).disabled=true;
      document.getElementById('sub_cat_change4'+a).value = "0";

      document.getElementById('sub_cat_change5'+a).disabled=true;
    }else{
      datos = "idGrower="+x+"&product_id="+b;

      $.ajax({
           type: "POST",
           url: "selectBoxes.php",
           data: datos,
           cache: false,
           success: function(r){
            // alert(r);
            // return false;
             document.getElementById('sub_cat_change2'+a).innerHTML = r;
           }
       });
        document.getElementById('sub_cat_change2'+a).disabled=false;
        document.getElementById('sub_cat_change3'+a).disabled=true;
        document.getElementById('sub_cat_change3'+a).value = "0";
        document.getElementById('sub_cat_change4'+a).disabled=true;
        document.getElementById('sub_cat_change4'+a).value = "0";

        document.getElementById('sub_cat_change5'+a).disabled=true;
    }

  }

  function removeDisabledC(a){
    var y = document.getElementById('sub_cat_change2'+a).value;
    if(y=="0"){
      document.getElementById('sub_cat_change3'+a).disabled=true;
      document.getElementById('sub_cat_change3'+a).value = "0";

      document.getElementById('sub_cat_change4'+a).disabled=true;
      document.getElementById('sub_cat_change4'+a).value = "0";

      document.getElementById('sub_cat_change5'+a).disabled=true;
    }else{
      document.getElementById('sub_cat_change3'+a).disabled=false;
    }
  }


  function removeDisabledB(a){
    var y = document.getElementById('sub_cat_change3'+a).value;
    if(y=="0"){
      document.getElementById('sub_cat_change4'+a).disabled=true;
      document.getElementById('sub_cat_change4'+a).value = "0";

      document.getElementById('sub_cat_change5'+a).disabled=true;
    }else{
        document.getElementById('sub_cat_change4'+a).disabled=false;
    }
  }



  function removeDisabledD(a){
    var y = document.getElementById('sub_cat_change4'+a).value;
    if(y=="0"){
      document.getElementById('sub_cat_change5'+a).disabled=true;

    }else{
        document.getElementById('sub_cat_change5'+a).disabled=false;
    }
  }




  </script>
<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>


<script>


var stripe = Stripe('pk_test_TYooMQauvdEDq54NiTphI7jx');
var elements = stripe.elements();

var style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '14px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

var card = elements.create('card', {
  iconStyle: 'solid',
  style: {
    base: {
      iconColor: '#8898AA',
      color: 'Black',
      fontWeight: 18,
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSize: '14px',

      '::placeholder': {
        color: '#8898AA',
      },
    },
    invalid: {
      iconColor: '#e85746',
      color: '#e85746',
    }
  },
  classes: {
    focus: 'is-focused',
    empty: 'is-empty',
  },
});

// Add an instance of the card UI component into the `card-element` <div>
card.mount('#card-element');

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}

function createToken() {
  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server
      stripeTokenHandler(result.token);
    }
  });
};

// Create a token when the form is submitted.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(e) {
  e.preventDefault();
  createToken();
});


function revForm(){
var cardName = document.getElementById('cc-card-name').value;
var phoneNumber = document.getElementById('cc-phone-number').value;

if(cardName!=''){
  if(phoneNumber!=''){
    return true;
  }else{
    swal("Your phone number!", "Complete all fields!", "warning");
    document.getElementById('cc-card-name').focus();
    return false;
  }
}else{
  swal("Your name please!", "Complete all fields!", "warning");
  document.getElementById('cc-card-name').focus();
  return false;
}
}

function requestProduct1()
{
/*
    swal({
    title: "Contact us!",
    text: "Please, send us an email to complete your request!     info@freshlifefloral.com",
    icon: "warning",
    buttons: true,
  });
  */
$('#price_modal').modal('hide');

}


  function msnCart(a,prodId){
  newProduct(a,prodId);
}

function countProductPrevious(PreviousProductKey){

  document.getElementById('sessionStorage').value = PreviousProductKey;

datos = "PreviousProductKey="+PreviousProductKey;
  $.ajax({
       type: "POST",
       url: "count_temp_cart.php",
       data: datos,
       cache: false,
       success: function(r){
       //  alert(r);
        // return false;
         document.getElementById('countProductosPrev').innerHTML = r;
       }
   });

}

function newProduct(a,prodId)
{

  if(localStorage.getItem("idSessionCompras") == null)
  {
    var newPreviousProductKey = RandomNum(1000000001, 2147483647);
    localStorage.setItem("idSessionCompras", newPreviousProductKey);
    var idSessionCompras = localStorage.getItem("idSessionCompras");


      var priceSize = document.getElementById('sub_cat_change3'+a).value;
      var priceSizeArray = priceSize.split("/");
      var price = priceSizeArray[0]
      var size  = priceSizeArray[1]+" [cm]";


      var steamsT = document.getElementById('sub_cat_change2'+a).value;
      var steamsArray = steamsT.split("/");
      var steams = steamsArray[1]

      //alert(steams);
      //return false;

      var datosCart = "newPreviousProductKey="+newPreviousProductKey+"&product_id="+prodId+"&grower_id="+document.getElementById('sub_cat_change1'+a).value+"&price_id="+price+"&size="+size+"&box_id="+document.getElementById('sub_cat_change2'+a).value+"&quantity_id="+document.getElementById('sub_cat_change4'+a).value+"&steams="+steams;
    //alert(datosCart);
    //return false;

       $.ajax({
            type: "POST",
            url: "save_temp_cart.php",
            data: datosCart,
            cache: false,
            success: function(r){
            //  alert(r);
            //  return false;
              if(r==1){
                countProductPrevious(idSessionCompras);
                loadDataCart(idSessionCompras);
                document.getElementById('sub_cat_change1'+a).value = "0";
                document.getElementById('sub_cat_change2'+a).value = "0";
                document.getElementById('sub_cat_change3'+a).value = "0";
                document.getElementById('sub_cat_change4'+a).value = "0";

                document.getElementById('sub_cat_change2'+a).disabled=true;
                document.getElementById('sub_cat_change3'+a).disabled=true;
                document.getElementById('sub_cat_change4'+a).disabled=true;

                  document.getElementById('sub_cat_change5'+a).disabled=true;
                swal("Good job!", "Product add to cart!", "success");
              }else{
                swal("Sorry!", "Please, try again!", "error");
              }

            }
        });



  }
    else
  		{
  		addNoFirstTime(localStorage.getItem("idSessionCompras"),a,prodId);
  		}

}

function RandomNum(min, max) {
   return Math.round(Math.random() * (max - min) + min);
}

function addNoFirstTime(newPreviousProductKey,a,prodId)
{

  var priceSize = document.getElementById('sub_cat_change3'+a).value;
  var priceSizeArray = priceSize.split("/");
  var price = priceSizeArray[0]
  var size  = priceSizeArray[1]+" [cm]";

  var steamsT = document.getElementById('sub_cat_change2'+a).value;
  var steamsArray = steamsT.split("/");
  var steams = steamsArray[1]

  //alert(steams);
  //return false;

  var datosCart = "newPreviousProductKey="+newPreviousProductKey+"&product_id="+prodId+"&grower_id="+document.getElementById('sub_cat_change1'+a).value+"&price_id="+price+"&size="+size+"&box_id="+document.getElementById('sub_cat_change2'+a).value+"&quantity_id="+document.getElementById('sub_cat_change4'+a).value+"&steams="+steams;

  //alert(datosCart);
  //return false;



     $.ajax({
          type: "POST",
          url: "save_temp_cart_items.php",
          data: datosCart,
          cache: false,
          success: function(r){

            //  alert(r);
            //  return false;
            if(r==1){

              countProductPrevious(newPreviousProductKey);
              loadDataCart(newPreviousProductKey);
              document.getElementById('sub_cat_change1'+a).value = "0";
              document.getElementById('sub_cat_change2'+a).value = "0";
              document.getElementById('sub_cat_change3'+a).value = "0";
              document.getElementById('sub_cat_change4'+a).value = "0";

              document.getElementById('sub_cat_change2'+a).disabled=true;
              document.getElementById('sub_cat_change3'+a).disabled=true;
              document.getElementById('sub_cat_change4'+a).disabled=true;

              document.getElementById('sub_cat_change5'+a).disabled=true;


              swal("Good job!", "Product add to cart!", "success");
            }else{
              swal("Sorry!", "Please, try again!", "error");
            }
          }
      });
}




</script>
