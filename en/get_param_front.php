<?php

/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 17 Feb 2021
Structure MarketPlace previous to buy
Add Protection SQL INY, XSS
**/

require_once("../config/config_gcp.php");

function calculateKilo($userSessionID,$con){
   $getBuyerShippingMethod = "select shipping_method_id
                              from buyer_shipping_methods
                             where buyer_id ='" . $userSessionID . "'";

  $buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
  $buyerShippingMethod = mysqli_fetch_assoc($buyerShippingMethodRes);
   $shipping_method_id = $buyerShippingMethod['shipping_method_id'];

     $getShippingMethod = "select connect_group
                             from shipping_method
                            where id='" . $shipping_method_id . "'";

      $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
      $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);

       $temp_conn = explode(',', $shippingMethodDetail['connect_group']);

      $id_conn = $temp_conn[1];  // Default

      $getConnect = "select charges_per_kilo
                       from connections
                      where id='" . $id_conn . "'";

      $rs_connect = mysqli_query($con, $getConnect);

      $charges = mysqli_fetch_assoc($rs_connect);

       /////////////////////////////////////////////////////////////

      $cost = $charges['charges_per_kilo'];
      $cost_un = unserialize($cost);

      $cost_sum = 0;

      foreach ($cost_un as $key => $value) {
          $cost_sum = $cost_sum + $value;
      }

      return $charges_per_kilo_trans =  $cost_sum;
}


class pagination{
    var $limit = 10;
    var $adjacentLinks = 2;

  function paginationVarietySubPageGrowers($currentPage, $limit, $qsearch,$idGrower,$id_main,$idColor,$idSubcategory , $idVari) {

        global $con;

        if(empty($limit) )
            $limit = $this->limit;
            $page = $currentPage;

        if ($page == 1) {
            $start = 0;
        } else {
            $start = ($page - 1) * $limit;
        }

///////////////////////CONDITIONS /////////////////////////////////////////////////////////
  $wh = "";

if ($idSubcategory != "") {

    if ($idColor != "") {
        $wh.=" AND p.subcategoryid IN (" . $idSubcategory . ") AND p.color_id IN (" . $idColor . ")";
    } else {
        $wh.=" AND p.subcategoryid IN (" . $idSubcategory . ")";
    }

}

if ($qsearch != "") {
    if ($idGrower != "") {
        $wh.=" OR gp.grower_id='" . $idGrower . "'";
    }
} else {
    if ($idGrower != "") {
        $wh.=" AND gp.grower_id='" . $idGrower . "'";
    }
}

if ($idColor != "") {
    $wh.=" AND p.color_id IN (" . $idColor . ")";
}

if ($idVari != "") {
    $wh.=" AND p.id ='" . $idVari . "' ";
}

///////////////////////////////////////////////////////////////////////////////////////////


 if ($wh == "") {  ?>
        <div class="notfound" style="margin-left: 20px;">xxx</div>
 <?php
 } else {

    $main_cat_id = "";

    if ($id_main != "") {
        $main_cat_id = " AND p.categoryid='" . $id_main . "'";
    }


          $growersSql = "select p.id             , p.categoryid     ,
                                p.subcategoryid  , p.name           ,
                                p.image_path     ,
                                p.color_id       , gp.grower_id     
                          from product p
	                  left join grower_product gp on p.id = gp.product_id
	                  left join growers g         on gp.grower_id = g.id
	                 where g.active='active'
                           and p.status = 0
                               $wh
                               $main_cat_id
                         GROUP BY p.id , p.categoryid , p.subcategoryid , p.name , p.image_path , p.color_id , gp.grower_id ";

        $query = mysqli_query($con, $growersSql);
        $totalResultsCount = $query->num_rows;


        $sel_products = "select p.id             , p.categoryid     ,
                                p.subcategoryid  , p.name           ,
                                p.image_path     ,
                                p.color_id       , gp.grower_id     
                          from product p
	                  left join grower_product gp on p.id = gp.product_id
	                  left join growers g         on gp.grower_id = g.id
	                 where g.active='active'
                           and p.status = 0
                               $wh
                               $main_cat_id
                         GROUP BY p.id , p.categoryid , p.subcategoryid , p.name ,  p.image_path , p.color_id  , gp.grower_id limit $start,$limit  ";



     //////////////////////////////////////////////////

    $rs_products = mysqli_query($con, $sel_products);
    $number_row = mysqli_num_rows($rs_products);

    $selected_color_array = array();

    if ($number_row > 0) {

        $i = 0;
        while ($products = mysqli_fetch_array($rs_products)) {
          $i = $i+1;

            array_push($selected_color_array, $products['color_id']);
            $sel_catinfo = "select * from category where id='" . $products['categoryid'] . "'";

            $rs_catinfo = mysqli_query($con,$sel_catinfo);

            $catinfo = mysqli_fetch_array($rs_catinfo);

            $cname = preg_replace("![^a-z0-9]+!i", "-", trim($products["name"]));

            $sel_imagen = "select im.id         , im.product_id , im.grower_id  ,
                                  im.image_path , im.imagen
                             from grower_product_img im
                            inner join product p on im.product_id = p.id
                            where im.product_id = '" . $products['id'] . "'
                              and im.imagen = 1
                              and p.status = 0  ";

                $rs_imagen = mysqli_query($con, $sel_imagen);
                $number_img = mysqli_num_rows($rs_imagen);

                $rowImg = mysqli_fetch_array($rs_imagen);

                    if ($number_img > 0) {
                        $default_img = $rowImg["image_path"];
                    }else {
                        $default_img = $products['image_path'];
                    }

    ?>

            <div class="col-lg-4 col-lg-4 mix <?php echo $products['categoryid']; ?>"><!-- item -->

                <div class="item-box">
                    <figure>
                        <img src="<?php echo "/" . $products['image_path'] ?>" width="250px" height="250px">
            <span class="item-hover">
						<span class="overlay dark-5"></span>
						<span class="inner">

							<!-- lightbox -->
							<a class="ico-rounded lightbox" href="<?php echo "/" . $products['image_path'] ?>" data-plugin-options='{"type":"image"}'>
								<span class="fa fa-plus size-20"></span>
							</a>

                            <!-- details -->
							<a class="ico-rounded" href="<?php echo '/en/variety-sub-page.php?id=' . $products['id']; ?>">
								<span class="glyphicon glyphicon-option-horizontal size-20"></span>
							</a>

						</span>
                                    </span>

                                    <div class="item-box-overlay-title"  >
                                            <h3><?php echo $products["name"]; ?></h3>
                                            <ul class="list-inline categories nomargin">
                                                <li><a href="#"><?php echo $catinfo['name']?></a></li>
                                            </ul>
                                    </div>

                                        <!-- carousel get_subcat.php-->
                                    <div class="owl-carousel buttons-autohide controlls-over nomargin" data-plugin-options='{"singleItem": true, "autoPlay": false, "navigation": false, "pagination": true, "transitionStyle":"fade"}'>
                                        <div>
                                            <img src="<?php echo "/" . $default_img ?>" width="300" height="250" alt="">
                                        </div>

                                    </div>
                                    <!-- /carousel -->
                    </figure>

                    <?php

                    if($_SESSION["buyer"]){

                      $userSessionID = $_SESSION["buyer"];
                    }
                      else{

                        $userSessionID="N/A";
                      }

                      if($userSessionID != "N/A"){


                     ?>

                        <form name="frmproduct" method="post" onsubmit="" enctype="multipart/form-data">
                                    <?php
                                    $growers_sql1 = "select g.id , g.growers_name,
                                                            (select TIMESTAMPDIFF(DAY, max(date_added), now()) from invoice_requests where grower_id = gp.grower_id ) as dias_transcurridos
                                                        from grower_product gp
                                                        inner join growers  g on gp.grower_id  = g.id
                                                        where gp.product_id = '".$products['id']."'
                                                        and g.active='active'
                                                        order by g.growers_name";

                                    $rs_growers1 = mysqli_query($con,$growers_sql1);
                                    ?>
                                    <div class="field">


                                        <select id="sub_cat_change1<?php echo $i; ?>" class="listmenu" onchange="removeDisabledP(<?php echo $i; ?>);"   name="sub_cat_change1" style="margin-left:10px; margin-top:10px; margin-top:10px; height:35px; padding:3px; width:230px;border-radius: 5px">
                                              <option value="0">Select Grower</option>
                                              <optgroup label="Best Deal">
                                                <option value="100000">Best Deal</option>
                                               </optgroup>
                                        <optgroup label="Regular Options">
                                        <?php
                                            while ($row_grower1 = mysqli_fetch_array($rs_growers1)) {
                                        ?>
                                                <option value="<?php echo $row_grower1['id'] ?>"><?php echo $row_grower1['growers_name']." (".$row_grower1['dias_transcurridos'].")" ?></option>
                                        <?php
                                              } ?>

                                        </select>
                                        </optgroup>
                                    </div>



                                              <?php
                                            // se eliminó  después de and gpb.prodcutid = '".$products['id']."' "and gpb.price > 0"
                                           $tasaKilo = calculateKilo($userSessionID,$con);

                                            $sql_price = "select gp.id,gp.price_adm as price,   s.name as sizename   ,
                                                                 f.name as featurename, gp.feature as feature, s.id size_id,
                                                                 gp.factor , gp.stem_bunch , b.name as stems
                                                            from grower_parameter gp
                                                           inner JOIN sizes s ON gp.size = s.id
                                                           inner JOIN bunch_sizes b ON gp.stem_bunch = b.id
                                                            left JOIN features f  ON gp.feature = f.id
                                                           where gp.idsc = '".$products['subcategoryid']."' " ;


                                                              $rs_price = mysqli_query($con,$sql_price);

                                            ?>


                                            <div class="field">
                                                <select id="sub_cat_change2<?php echo $i; ?>" disabled="true" onchange="removeDisabledB(<?php echo $i; ?>,<?php echo $products['id']; ?>)" class="listmenu"  style="margin-left:10px; margin-top:10px; margin-top:10px; height:35px; padding:3px; width:230px;border-radius: 5px">
                                                <option value="0">Select Price</option>
                                                <?php
                                                while ($row_price = mysqli_fetch_array($rs_price)) {
                                                  $priceBunch = $tasaKilo * $row_price['factor'];
                                                  $priceSteam = $priceBunch / $row_price['stems'];
                                                  $priceCalculado = sprintf('%.2f',round($priceSteam + $row_price['price'],2));
                                                ?>
                                                <option value="<?php echo $priceCalculado."/".$row_price['sizename']."/".$row_price['feature']."/".$row_price['size_id'];  ?>"><?php echo $priceCalculado." ".$row_price['sizename']." cm. ".$row_price['featurename'] ?></option>

                                                <?php
                                                      } ?>

                                                </select>
                                            </div>

                                            <!-- Drop down Boxes -->

                                                <div class="field">
                                                    <select id="sub_cat_change3<?php echo $i; ?>" disabled="true" onchange="removeDisabledC(<?php echo $i; ?>)" class="listmenu" style="margin-left:10px; margin-top:10px; margin-top:10px; height:35px; padding:3px; width:230px;border-radius: 5px">
                                                                <option value='0'>Select Boxes</option>
                                                    </select>
                                                </div>

                                        <!-- Drop down precios -->

                                                    <div class="field">
                                                        <select id="sub_cat_change4<?php echo $i; ?>" disabled="true" onchange="removeDisabledD(<?php echo $i; ?>)" class="listmenu" style="margin-left:10px; margin-top:10px; margin-top:10px; height:35px; padding:3px; width:230px;border-radius: 5px">
                                                                    <option value="0">Select Quantity</option>
                                                                    <?php

                                                                        for($k=1;$k<=50;$k++) {
                                                                    ?>
                                                                    <option value="<?php echo $k; ?>"><?php echo $k." boxes"; ?></option>
                                                                    <?php
                                                                      }
                                                                    ?>

                                                        </select>
                                                    </div>

                                      <!-- Drop down precios -->


                                          <div class="field">
                                              <select id="sub_cat_change6<?php echo $i; ?>" disabled="true" onchange="removeDisabledF(<?php echo $i; ?>)" class="listmenu" style="margin-left:10px; margin-top:10px; margin-top:10px; height:35px; padding:3px; width:230px;border-radius: 5px; display:none">
                                                          <option value="0">Select Steams</option>
                                                          <?php

                                                              for($p=1;$p<=500;$p++) {
                                                          ?>
                                                          <option value="<?php echo $p; ?>"><?php echo $p." steams"; ?></option>
                                                          <?php
                                                            }
                                                          ?>

                                              </select>
                                          </div>
                                                    <div class="field">
                                                    <button  id="sub_cat_change5<?php echo $i; ?>" type="button" onclick="msnCart(<?php echo $i; ?>,<?php echo $products['id']; ?>,'')" disabled style="margin-left:10px; margin-top:10px; height:35px; padding:3px; width:230px;border-radius: 5px; background-color: #dee1e3; color:white"><i class="fa fa-cart-plus"></i> Cart</button>
                                                  </div>
                                                </form>
                                                       <?php }
                                                       ?>
                                                    </div>

                                                </div><!-- /item -->



                </div>

            </div><!-- /item -->

            <?php
        }

    } else { ?>
        <div class="notfound" style="margin-left: 20px;"> X </div>
    <?php

    }

 }


         $this->generatePaginationLinks($limit, $this->adjacentLinks, $totalResultsCount, $page);
 }
    /**
     * GENERATES PAGINATION LINKS
     * @param type $limit
     * @param type $adjacentLinks number of links to be displayed
     * @param type $totalResults TOTAL RESULT COUNT
     * @param int $page CURRENT PAGE
     */
    private function generatePaginationLinks($limit, $adjacentLinks, $totalResultsCount, $page)   {
        #echo "<font color=red>LIMIT:$limit :::\$adjacentLinks:$adjacentLinks :::\$totalResultsCount:$totalResultsCount :::\$page:$page</font><br>";
        $pagination = '';

        if ($page == 0)
            $page = 1;     //if no page var is given, default to 1.

        $prev     = $page - 1;       //previous page is page - 1
        $next     = $page + 1;       //next page is page + 1
        $prev_    = '';
        $first    = '';
        $lastpage = ceil($totalResultsCount / $limit);
        $next_    = '';
        $last     = '';

        if ($lastpage > 1) {

            //previous button
            if ($page > 1)
                $prev_.= "<li><a class='page-numbers' href='?page=$prev' >prev</a></li>";
            else {
                //$pagination.= "<span class=\"disabled\">previous</span>";
            }

            //pages
            if ($lastpage < 5 + ($adjacentLinks * 2)) { //not enough pages to bother breaking it up
                $first = '';
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><span class='page-numbers' >$counter</span></li>";
                    else
                        $pagination.= "<li><a class='page-numbers' href='?page=$counter' >$counter</a></li>";
                }
                $last = '';
            }
            elseif ($lastpage > 3 + ($adjacentLinks * 2)) { //enough pages to hide some
                //close to beginning; only hide later pages
                $first = '';
                if ($page < 1 + ($adjacentLinks * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacentLinks * 2); $counter++) {
                        if ($counter == $page)
                            $pagination.= "<li><span>$counter</span></li>";
                        else
                            $pagination.= "<li><a class='page-numbers' href='?page=$counter' >$counter</a></li>";
                    }
                    $last.= "<li><a class='page-numbers' href='?page=$lastpage' >Last</a></li>";
                }

                //in middle; hide some front and some back
                elseif ($lastpage - ($adjacentLinks * 2) > $page && $page > ($adjacentLinks * 2)) {
                    $first.= "<li><a class='page-numbers' href='?page=1' >First</a></li>";
                    for ($counter = $page - $adjacentLinks; $counter <= $page + $adjacentLinks; $counter++) {
                        if ($counter == $page)
                            $pagination.= "<li><span>$counter</span></li>";
                        else
                            $pagination.= "<li><a class='page-numbers' href='?page=$counter' >$counter</a></li>";
                    }
                    $last.= "<li><a class='page-numbers' href='?page=$lastpage' >Last</a></li>";
                }
                //close to end; only hide early pages
                else {
                    $first.= "<li><a class='page-numbers' href='?page=1' >First</a></li>";
                    for ($counter = $lastpage - (2 + ($adjacentLinks * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $page)
                            $pagination.= "<li><span class='' >$counter</span></li>";
                        else
                            $pagination.= "<li><a class='page-numbers' href='?page=$counter' >$counter</a></li>";
                    }
                    $last = '';
                }
            }
            if ($page < $counter - 1)
                $next_.= "<li><a class='page-numbers' href='?page=$next'>next</a></li>";
            else {
                //$pagination.= "<span class=\"disabled\">next</span>";
            }
            $pagination = "<ul class='pagination pagination-simple pull-right'>" . $first . $prev_ . $pagination . $next_ . $last . "</ul>";
        }

        echo $pagination;
    }

}

if($_POST['action'] == 'varietySubPageGrowersPagination')
{
    $currentPage = $_POST['page'];
    $limit = 15;

    $q = rtrim($_REQUEST['searchword'], ",");
    $grow = $_REQUEST["growers_id"];

    $main_cat = $_REQUEST['selected_main_cat'];
    $color_selected       = trim($_REQUEST['selected_color'], ",");
    $subcategory_selected = trim($_REQUEST['subcategory_selected'], ",");

    $varieties_check    = $_REQUEST['varieties_checked'];

    $pagination = new pagination();
    $pagination->paginationVarietySubPageGrowers($currentPage, $limit, $q , $grow,$main_cat,$color_selected,$subcategory_selected , $varieties_check);
}

?>



<script type="text/javascript" src="https://handydevelopment-9415c.firebaseapp.com/vendor/sweetalert/sweetalert.min.js"></script>
<script>

function loadDataCart(key){

  var order_id_prev = document.getElementById('valueOrderId_MP').value;
  var datos = "ord_id="+order_id_prev+"&PreviousProductKey="+key;

  $.ajax({
       type: "POST",
       url: "loadDataItemsPreviuos.php",
       data: datos,
       cache: false,
       success: function(r){
      //  alert(r);
        // return false;
         document.getElementById('loadDataItemsPrevious').innerHTML = r;
       }
   });

   $.ajax({
        type: "POST",
        url: "loadDataPricesSummary.php",
        data: datos,
        cache: false,
        success: function(r){
         //alert(r);
         // return false;
          document.getElementById('loadDataPricesSummary').innerHTML = r;
        }
    });

    $.ajax({
         type: "POST",
         url: "loadDataItemsPreviuosCheckOut.php",
         data: datos,
         cache: false,
         success: function(r){
          // alert(r);
          // return false;
           document.getElementById('itemPreviousCheckOut').innerHTML = r;
         }
     });


     $.ajax({
          type: "POST",
          url: "loadDataItemsPreviuosSumCheckOut.php",
          data: datos,
          cache: false,
          success: function(r){
           // alert(r);
           // return false;
            document.getElementById('itemPreviousSumCheckOut').innerHTML = r;
          }
      });


}

function removeDisabledP(a){
//  localStorage.clear();
var x = document.getElementById('sub_cat_change1'+a).value;
var sel = document.getElementById('sub_cat_change1'+a);

var name = sel.options[sel.selectedIndex].text;


if(x=="0"){
  document.getElementById('sub_cat_change2'+a).disabled=true;
  document.getElementById('sub_cat_change2'+a).value = "0";

  document.getElementById('sub_cat_change3'+a).disabled=true;
  document.getElementById('sub_cat_change3'+a).value = "0";

  document.getElementById('sub_cat_change4'+a).disabled=true;
  document.getElementById('sub_cat_change4'+a).value = "0";

  document.getElementById('sub_cat_change5'+a).disabled=true;

  document.getElementById('sub_cat_change6'+a).disabled=true;
  document.getElementById('sub_cat_change6'+a).value = "0";

  document.getElementById('sub_cat_change5'+a).style.backgroundColor = "#dee1e3";

  document.getElementById('sub_cat_change1'+a).style.backgroundColor = "#fff";
  document.getElementById('sub_cat_change1'+a).style.color = "#666";

}else{
  if(x=="100000")
  {


    document.getElementById('sub_cat_change2'+a).disabled=false;
    document.getElementById('sub_cat_change2'+a).value = "0";

    document.getElementById('sub_cat_change3'+a).disabled=true;
    document.getElementById('sub_cat_change3'+a).value = "0";
    document.getElementById('sub_cat_change3'+a).style.display="none";

    document.getElementById('sub_cat_change4'+a).disabled=true;
    document.getElementById('sub_cat_change4'+a).value = "0";
    document.getElementById('sub_cat_change4'+a).style.display="none";

    document.getElementById('sub_cat_change6'+a).disabled=false;
    document.getElementById('sub_cat_change6'+a).value = "0";
    document.getElementById('sub_cat_change6'+a).style.display="block";

    document.getElementById('sub_cat_change5'+a).disabled=true;
    document.getElementById('sub_cat_change5'+a).style.backgroundColor = "#dee1e3";

    document.getElementById('sub_cat_change1'+a).style.backgroundColor = "#007BFD";
    document.getElementById('sub_cat_change1'+a).style.color = "#fff";
    document.getElementById('sub_cat_change5'+a).innerText="Add Cart";


  }
  else
    {
    document.getElementById('sub_cat_change2'+a).disabled=false;
    document.getElementById('sub_cat_change5'+a).disabled=false;
    document.getElementById('sub_cat_change2'+a).value = "0";
    document.getElementById('sub_cat_change3'+a).value = "0";
    document.getElementById('sub_cat_change4'+a).value = "0";

    document.getElementById('sub_cat_change3'+a).disabled=true;
    document.getElementById('sub_cat_change3'+a).value = "0";
    document.getElementById('sub_cat_change3'+a).style.display="block";

    document.getElementById('sub_cat_change4'+a).disabled=true;
    document.getElementById('sub_cat_change4'+a).value = "0";
    document.getElementById('sub_cat_change4'+a).style.display="block";

    document.getElementById('sub_cat_change6'+a).disabled=false;
    document.getElementById('sub_cat_change6'+a).value = "0";
    document.getElementById('sub_cat_change6'+a).style.display="none";

    document.getElementById('sub_cat_change5'+a).style.backgroundColor = "#8A2B79";

    document.getElementById('sub_cat_change1'+a).style.backgroundColor = "#fff";
    document.getElementById('sub_cat_change1'+a).style.color = "#666";

document.getElementById('sub_cat_change5'+a).innerText="Star a box with "+name;

    }
}
}

function removeDisabledB(a,b){
  var y = document.getElementById('sub_cat_change1'+a).value;
  var x = document.getElementById('sub_cat_change2'+a).value;

  if(x=="0"){
    document.getElementById('sub_cat_change3'+a).disabled=true;
    document.getElementById('sub_cat_change3'+a).value = "0";

    document.getElementById('sub_cat_change4'+a).disabled=true;
    document.getElementById('sub_cat_change4'+a).value = "0";

    document.getElementById('sub_cat_change6'+a).disabled=true;
    document.getElementById('sub_cat_change6'+a).value = "0";

    document.getElementById('sub_cat_change5'+a).disabled=true;
    document.getElementById('sub_cat_change5'+a).style.backgroundColor = "#dee1e3";
    document.getElementById('sub_cat_change5'+a).innerText="Add Cart";
  }else{

    var priceSize = document.getElementById('sub_cat_change2'+a).value;
    var priceSizeArray = priceSize.split("/");
    var size_id  = priceSizeArray[3];
    var feature_id_add  = priceSizeArray[2];

    datos = "idGrower="+y+"&product_id="+b+"&size_id="+size_id+"&feature_id_add="+feature_id_add;

    $.ajax({
         type: "POST",
         url: "selectBoxes.php",
         data: datos,
         cache: false,
         success: function(r){
           document.getElementById('sub_cat_change3'+a).innerHTML = r;
         }
     });
      document.getElementById('sub_cat_change2'+a).disabled=false;
      document.getElementById('sub_cat_change3'+a).disabled=false;
      document.getElementById('sub_cat_change3'+a).value = "0";
      document.getElementById('sub_cat_change4'+a).disabled=true;
      document.getElementById('sub_cat_change4'+a).value = "0";

      document.getElementById('sub_cat_change6'+a).disabled=false;
      document.getElementById('sub_cat_change6'+a).value = "0";

      document.getElementById('sub_cat_change5'+a).disabled=true;
      document.getElementById('sub_cat_change5'+a).style.backgroundColor = "#dee1e3";
      document.getElementById('sub_cat_change5'+a).innerText="Add Cart";

  }
}


function removeDisabledC(a){
  var y = document.getElementById('sub_cat_change3'+a).value;
  if(y=="0"){
    document.getElementById('sub_cat_change4'+a).disabled=true;
    document.getElementById('sub_cat_change4'+a).value = "0";

    document.getElementById('sub_cat_change5'+a).disabled=true;
  }else{
      document.getElementById('sub_cat_change4'+a).disabled=false;
      document.getElementById('sub_cat_change5'+a).innerText="Add Cart";
  }
}



function removeDisabledD(a){
  var y = document.getElementById('sub_cat_change4'+a).value;
  if(y=="0"){
    document.getElementById('sub_cat_change5'+a).disabled=true;
    document.getElementById('sub_cat_change5'+a).style.backgroundColor = "#dee1e3";

  }else{
      document.getElementById('sub_cat_change5'+a).disabled=false;
      document.getElementById('sub_cat_change5'+a).style.backgroundColor = "#28a745";
  }
}


function removeDisabledF(a){
  var y = document.getElementById('sub_cat_change6'+a).value;
  if(y=="0"){
    document.getElementById('sub_cat_change5'+a).disabled=true;
    document.getElementById('sub_cat_change5'+a).style.backgroundColor = "#dee1e3";
  }else{
      document.getElementById('sub_cat_change5'+a).disabled=false;
      document.getElementById('sub_cat_change5'+a).style.backgroundColor = "#28a745";
  }
}

function requestProduct1(){

  /*
      swal({
      title: "Contact us!",
      text: "Please, send us an email to complete your request!     info@freshlifefloral.com",
      icon: "warning",
      buttons: true,
    });
    */

  $('#price_modal').modal('hide');
}


function msnCart(a,prodId,subcategoryId){

  var v1 = document.getElementById('sub_cat_change1'+a).value;
  var v2 = document.getElementById('sub_cat_change2'+a).value;
  var v3 = document.getElementById('sub_cat_change3'+a).value;
  var v4 = document.getElementById('sub_cat_change4'+a).value;
  var v6 = document.getElementById('sub_cat_change6'+a).value;



  if(v1!=0 && v2!=0 && v3!=0 && v4!=0){
    newProduct(a,prodId,subcategoryId);
  }
  else{
    if(v1!=0 && v2!=0 && v6!=0){
      newProduct(a,prodId,subcategoryId);
    }else{
      if(v1!=0){
        iniciarCajaMix(a,prodId);
      }
    }
  }


//
}


function iniciarCajaMix(a,prodId){
  var gid = document.getElementById('sub_cat_change1'+a).value;
  var gidd = gid;
  var newb = 1;

    var form = document.getElementById('frm-box-mix');
    var hiddenNewBox = document.createElement('input');
    var gid = document.createElement('input');

      hiddenNewBox.setAttribute('type', 'hidden');
      hiddenNewBox.setAttribute('name', 'newbox');
      hiddenNewBox.setAttribute('value', 1);

      gid.setAttribute('type', 'hidden');
      gid.setAttribute('name', 'grower_id');
      gid.setAttribute('value', gidd);

      form.appendChild(hiddenNewBox);
      form.appendChild(gid);

      form.submit();
}


function countProductPrevious(PreviousProductKey){

  document.getElementById('sessionStorage').value = PreviousProductKey;
  var order_id_prev = document.getElementById('valueOrderId_MP').value;
  var datos = "ord_id="+order_id_prev+"&PreviousProductKey="+PreviousProductKey;

  $.ajax({
       type: "POST",
       url: "count_temp_cart.php",
       data: datos,
       cache: false,
       success: function(r){
       //  alert(r);
        // return false;
         document.getElementById('countProductosPrev').innerHTML = r;
         document.getElementById('countProductosPrev1').innerHTML = r;
       }
   });

}

function newProduct(a,prodId,subcategoryId){

  if(localStorage.getItem("idSessionCompras") == null)
  {
    var newPreviousProductKey = RandomNum(1000000001, 2147483647);
    localStorage.setItem("idSessionCompras", newPreviousProductKey);
    var idSessionCompras = localStorage.getItem("idSessionCompras");

    var priceSize = document.getElementById('sub_cat_change2'+a).value;
    var priceSizeArray = priceSize.split("/");
    var price = priceSizeArray[0]
    var size  = priceSizeArray[1]+" [cm]";
    var feature_id  = priceSizeArray[2];
    var size_id  = priceSizeArray[3];


    var steamsT = document.getElementById('sub_cat_change3'+a).value;
    var steamsArray = steamsT.split("/");
    var steams = steamsArray[1];

    var quantity = document.getElementById('sub_cat_change4'+a).value;

    var bestOp = 0;

    if(document.getElementById('sub_cat_change1'+a).value=="100000"){
      var steamsT = document.getElementById('sub_cat_change6'+a).value;
      var steams = steamsT;
      var quantity = 1;
        bestOp = 1;
    }
    var order_id_prev = document.getElementById('valueOrderId_MP').value;
    var datosCart = "ord_id="+order_id_prev+"&bestOp_id="+bestOp+"&size_id="+size_id+"&feature_id="+feature_id+"&newPreviousProductKey="+newPreviousProductKey+"&product_id="+prodId+"&grower_id="+document.getElementById('sub_cat_change1'+a).value+"&price_id="+price+"&size="+size+"&box_id="+document.getElementById('sub_cat_change3'+a).value+"&quantity_id="+quantity+"&steams="+steams+"&categoriaId="+subcategoryId;


     $.ajax({
          type: "POST",
          url: "save_temp_cart.php",
          data: datosCart,
          cache: false,
          success: function(r){

            if(r==1){
              countProductPrevious(idSessionCompras);
              loadDataCart(idSessionCompras);
              document.getElementById('sub_cat_change1'+a).value = "0";
              document.getElementById('sub_cat_change2'+a).value = "0";
              document.getElementById('sub_cat_change3'+a).value = "0";
              document.getElementById('sub_cat_change4'+a).value = "0";
              document.getElementById('sub_cat_change6'+a).value = "0";

              document.getElementById('sub_cat_change2'+a).disabled=true;
              document.getElementById('sub_cat_change3'+a).disabled=true;
              document.getElementById('sub_cat_change4'+a).disabled=true;
              document.getElementById('sub_cat_change6'+a).disabled=true;

                document.getElementById('sub_cat_change5'+a).disabled=true;
                document.getElementById('sub_cat_change5'+a).style.backgroundColor = "#dee1e3";
                document.getElementById('sub_cat_change1'+a).style.backgroundColor = "#fff";
                document.getElementById('sub_cat_change1'+a).style.color = "#666";

              swal("Good job!", "Product add to cart!", "success");
            }else{
              swal("Sorry!", "Please, try again!", "error");
            }

          }
      });


  }
    else
  		{
  		addNoFirstTime(localStorage.getItem("idSessionCompras"),a,prodId,subcategoryId);
  		}

}

function RandomNum(min, max) {
   return Math.round(Math.random() * (max - min) + min);
}

function addNoFirstTime(newPreviousProductKey,a,prodId,subcategoryId){



  var priceSize = document.getElementById('sub_cat_change2'+a).value;
  var priceSizeArray = priceSize.split("/");
  var price = priceSizeArray[0]
  var size  = priceSizeArray[1]+" [cm]";
  var feature_id  = priceSizeArray[2];
  var size_id  = priceSizeArray[3];

  var steamsT = document.getElementById('sub_cat_change3'+a).value;
  var steamsArray = steamsT.split("/");
  var steams = steamsArray[1];

  var quantity = document.getElementById('sub_cat_change4'+a).value;

  var bestOp = 0;

  if(document.getElementById('sub_cat_change1'+a).value=="100000"){
      bestOp = 1;
      var steamsT = document.getElementById('sub_cat_change6'+a).value;
      var steams = steamsT;
      var quantity = 1;
  }

  var order_id_prev = document.getElementById('valueOrderId_MP').value;
  var datosCart = "ord_id="+order_id_prev+"&bestOp_id="+bestOp+"&size_id="+size_id+"&feature_id="+feature_id+"&newPreviousProductKey="+newPreviousProductKey+"&product_id="+prodId+"&grower_id="+document.getElementById('sub_cat_change1'+a).value+"&price_id="+price+"&size="+size+"&box_id="+document.getElementById('sub_cat_change3'+a).value+"&quantity_id="+quantity+"&steams="+steams+"&categoriaId="+subcategoryId;



     $.ajax({
          type: "POST",
          url: "save_temp_cart_items.php",
          data: datosCart,
          cache: false,
          success: function(r){


            if(r==1){

              countProductPrevious(newPreviousProductKey);
              loadDataCart(newPreviousProductKey);
              document.getElementById('sub_cat_change1'+a).value = "0";
              document.getElementById('sub_cat_change2'+a).value = "0";
              document.getElementById('sub_cat_change3'+a).value = "0";
              document.getElementById('sub_cat_change4'+a).value = "0";
              document.getElementById('sub_cat_change6'+a).value = "0";

              document.getElementById('sub_cat_change2'+a).disabled=true;
              document.getElementById('sub_cat_change3'+a).disabled=true;
              document.getElementById('sub_cat_change4'+a).disabled=true;
              document.getElementById('sub_cat_change6'+a).disabled=true;

              document.getElementById('sub_cat_change5'+a).disabled=true;
              document.getElementById('sub_cat_change5'+a).style.backgroundColor = "#dee1e3";

              document.getElementById('sub_cat_change1'+a).style.backgroundColor = "#fff";
              document.getElementById('sub_cat_change1'+a).style.color = "#666";



              swal("Good job!", "Product add to cart!", "success");
            }else{
              swal("Sorry!", "Please, try again!", "error");
            }
          }
      });
}

</script>
