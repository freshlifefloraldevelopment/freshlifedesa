<head>
  <title>Portfolio-loading-ajax-page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<?php
require_once("../config/config_gcp.php");

?>

    <section>

        <div class="container">
<div class="portfolio-ajax-page">


	<header class="page-header">
            <?php
            $productId = $_GET['id'];
            $grower_id = $_GET['id_grow'];

            //$sel_product = "SELECT p.id         , 
            //                       p.image_path 
            //                  FROM product p
            //                  JOIN subcategory s ON p.subcategoryid = s.id 
            //                  JOIN category c ON p.categoryid = c.id 
            //                 WHERE p.id = $productId ";
            
            $sel_product = "SELECT grower_product.product_id, 
                                   product.categoryid, 
                                   product.id,
                                   product.subcategoryid, 
                                   product.name as product_name, 
                                   product.image_path, 
                                   grower_product.image_path as image_path_grow,                                    
                                   category.name as category_name, 
                                   subcategory.name as sub_category_name 
                              FROM grower_product 
                              LEFT JOIN product     ON product.id     = grower_product.product_id 
                              LEFT JOIN category    ON category.id    = product.categoryid 
                              LEFT JOIN subcategory ON subcategory.id = product.subcategoryid 
                             WHERE grower_id = $grower_id 
                               AND product.id < $productId order by product.id DESC LIMIT 1";

            $rs_products = mysqli_query($con, $sel_product);

            $product = mysqli_fetch_assoc($rs_products); 
            
            $product_prev = $product['id'];             
            $product_next = $product['id'];
           
            ?>	            
		<h2><?php echo $product['product_name']; ?></h2>

		<ul class="list-inline">
			<li><a href="<?php echo SITE_URL."../en/single-growers-detail-3.php?id=".$product_prev."&id_grow=".$grower_id ?>" class="portfolio-ajax-page glyphicon glyphicon-menu-left"><!-- prev --></a></li>
			<li><a href="<?php echo SITE_URL."../en/single-growers-detail-2.php?id=".$product_next."&id_grow=".$grower_id ?>" class="portfolio-ajax-page glyphicon glyphicon-menu-right"><!-- next --></a></li>
			<li><a class="portfolio-ajax-close glyphicon glyphicon-remove" href="https://app.freshlifefloral.com/en/growers-page.php"><!-- close --></a></li>
		</ul>
	</header>	
            <?php
            
                     //$absolute_path = SITE_URL.$product['image_path'];
                       $absolute_path = SITE_URL.$product['image_path_grow'];
            ?>	
	<div class="row">

		<!-- left column -->
                <div class="col-lg-4 col-sm-4">
                   <div class="thumbnail relative margin-bottom-3">
			<!-- IMAGE -->
                        <figure id="zoom-primary" class="zoom" data-mode="mouseover">
                                                    
			<a class="lightbox bottom-right" href="<?php echo $absolute_path; ?>" data-plugin-options='{"type":"image"}'><i class="glyphicon glyphicon-search"></i></a>
				<img class="img-responsive" src="<?php echo $absolute_path; ?>" width="1200" height="1500" alt="This is the product title" />
			</figure>
		    </div>
		</div>

		<!-- right column -->
		<div class="col-md-5 col-sm-7">
			

                    <?php
                                        
                $sel_boxes_type ="select id, grower_id, product_id, boxes, chk, box_value, categoryid, colorid, sid
                                    from grower_product_box
                                   where grower_id  = $grower_id
                                     and product_id = $product_prev ";
          
                                        $boxes = mysqli_query($con, $sel_boxes_type);                                                                                                          
                    ?>

                    <!-- Templete -->
                  
							<div class="heading-title heading-border-bottom">
								<h3>Packing</h3>
							</div>

							<div class="toggle toggle-transparent-body toggle-accordion">

                                                        <?php   while ($btype = mysqli_fetch_assoc($boxes)) {?>    
                                                            
							<div class="toggle">

								<label><?php echo $btype["box_value"]?></label>
                                                             <?php   
                                                                    $sel_hb="SELECT s.name AS size_name,
                                                                                    IFNULL(fe.name, ' ') as featurename ,
                                                                                    gs.bunch_value,
                                                                                    gs.is_bunch,
                                                                                    bt.name as caja,
                                                                                    gs.sizes as sizeid,
                                                                                    b.id as boxid,
                                                                                    gs.bunch_sizes                                                                                    
                                                                               FROM grower_product_bunch_sizes gs
                                                                               LEFT JOIN grower_product_box_packing  gr ON( gr.sizeid=gs.sizes AND gr.bunch_size_id=gs.bunch_sizes AND gr.growerid=gs.grower_id AND gr.prodcutid=gs.product_id)
                                                                               LEFT JOIN sizes AS s     ON gs.sizes = s.id
                                                                               LEFT JOIN boxes AS b     ON b.id     = gr.box_id
                                                                               LEFT JOIN boxtype AS bt  ON b.type   = bt.id
                                                                               LEFT JOIN features AS fe ON fe.id    = gr.feature
                                                                              WHERE gs.grower_id    = $grower_id
                                                                                AND gs.product_id   = $product_prev  
                                                                                AND b.id            = '" . $btype['boxes'] . "'
                                                                                AND gs.bunch_value != 0    
                                                                              GROUP by s.name , fe.name , gs.bunch_value, gs.is_bunch, bt.name ";
          
                                                                            $dataMedidahb = mysqli_query($con, $sel_hb);                                                                                                                               
                                                                    while ($dthb = mysqli_fetch_assoc($dataMedidahb)) {
                                                                        
                                                                    $sel_bunch="select qty 
                                                                                  from grower_product_box_packing 
                                                                                 where growerid     = $grower_id 
                                                                                   and prodcutid    = $product_prev 
                                                                                   and sizeid       = '" . $dthb['sizeid'] . "'
                                                                                   and box_id       = '" . $dthb['boxid'] . "'
                                                                                   and bunch_size_id= '" . $dthb['bunch_sizes'] . "'
                                                                                 order by id desc limit 0,1 ";
          
                                                                            $dataBunch = mysqli_query($con, $sel_bunch); 
                                                                            $bunch = mysqli_fetch_assoc($dataBunch)
                                                                                    
                                                                        ?>                                                                                                                                    
								<div class="toggle-content"> 
										<p><?php echo $dthb["size_name"] . " cm ". $dthb["featurename"]." ".$dthb["bunch_value"]." st/bu " . $bunch['qty']." bunches" ?></p>
								</div>
                                                             <?php   }?>                                                               

							</div>

                                                        <?php   }?>                                                                        
							</div>                    
                    
                    <!-- / Templete -->


		</div>

	</div>

</div>
        </div>

    </section>            
<script type="text/javascript" src="/includes/assets/js/view/demo.shop.js"></script>
<script type="text/javascript" src="/assets/js/Chart.js"></script>
<script src="/assets/js/highcharts.js"></script>
<script src="/assets/js/exporting.js"></script>