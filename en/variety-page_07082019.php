<?php
require_once("../config/config_gcp.php");

require_once("../functions/functions.php");

#############QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################
$pageId = 1; //VARIETY PAGE ID 
$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";
$pageQuery = mysqli_query($con, $pageSql);
$pageData = mysqli_fetch_assoc($pageQuery);
#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################
$cssHeadArray = array(SITE_URL . '/includes/assets/css/essentials-flfv3.css', SITE_URL . '/includes/assets/css/layout-flfv3.css'
    , SITE_URL . '/includes/assets/css/header-4.css'
//    , SITE_URL . '/includes/assets/css/color_scheme/blue.css'
    , 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css'
    , SITE_URL . '/includes/assets/css/layout-datatables.css' 
    , SITE_URL . '/includes/assets/css/token-input-facebook.css', SITE_URL . '/includes/assets/css/token-input.css'
    );
$jsHeadArray = array();
#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################

require_once '../includes/header.php';
?>

<style type="text/css">
    .active{
        color: #8a2b83 !important;
        font-weight: bold;
    }
    .tag.shop-color{
        border: 1px solid #666;
        height: 23px !important;
        width: 23px !important;
    }
</style>


<style type="text/css">
.navbar {
  margin-bottom: 0;
  background-color: #f4511e;
  z-index: 9999;
  border: 0;
  font-size: 12px !important;
  line-height: 1.42857143 !important;
  letter-spacing: 4px;
  border-radius: 0;
  font-family: Montserrat, sans-serif;
}
.navbar li a, .navbar .navbar-brand {
  color: #f4511e !important;
}
.navbar-nav li a:hover, .navbar-nav li.active a {
  color: #f4511e !important;
  background-color: #f4511e !important;
}
.navbar-default .navbar-toggle {
  border-color: transparent;
  color: #f4511e !important;
}
.dropdown li a {
  color: #f4511e !important;
}
.navbar-default .navbar-nav>.open>a.dropdown-toggle{
  background-color: #f4511e !important;
}
}
</style>

<section class="page-header page-header-xs">
    <div class="container">

        <!-- breadcrumbs -->
        <ol class="breadcrumb breadcrumb-inverse">
            <li><a href="<?php echo SITE_URL; ?>">Home</a></li>
            <li><a href="#">Pages</a></li>
            <li class="active">Variety</li>
        </ol><!-- /breadcrumbs -->
        
       
    </div>
    
    
</section>
<!-- /PAGE HEADER -->
<section class="page-header page-header-xs">
    <div class="container">

        <!-- breadcrumbs -->
        <span class="logo pull-left">
            <a href="https://app.freshlifefloral.com/"> 
                <img src="https://app.freshlifefloral.com/user/logo.png" alt="admin panel" height="60" width="200"> </a>
            <a>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp</a>  
            <a href="<?php echo SITE_URL; ?>"><FONT SIZE=4>Home</font></a>            
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>                        
            <a href="<?php echo SITE_URL."en/variety-page.php"; ?>"><FONT SIZE=4>Browse</font></a>  
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>  
            <a href="<?php echo SITE_URL."en/growers-page.php"; ?>"><FONT SIZE=4>Growers</font></a>            
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>                        
            <a href="<?php echo "https://www.freshlifefloral.com/blog/how-it-works-buyers"; ?>"><FONT SIZE=4>How it Works</font></a> 
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>              
            <a href="<?php echo "https://www.freshlifefloral.com/blog"; ?>"><FONT SIZE=4>Blog</font></a>            
            <a>&nbsp;&nbsp;&nbsp;&nbsp</a>                        
            <a href="<?php echo "https://www.freshlifefloral.com/blog/contact-us"; ?>"><FONT SIZE=4>Contacts</font></a>                                    
        </span>  

        <!-- /breadcrumbs -->

        
       
    </div>
    
    
</section>
<!-- -->
<section>
    <div class="container">

        <?php
        $sel_cat = "select gp.categoryid,c.name,c.id
		      from grower_product gp 
	              left join growers g on gp.grower_id=g.id
	              left join category c on gp.categoryid=c.id
	             where g.active='active' group by gp.categoryid order by c.name";
        
        $rs_cat = mysqli_query($con,$sel_cat);
        ?>
        <!-- Portfolio Filter -->
        <div class="text-center">
            <ul class="nav nav-pills mix-filter">
                <!-- <li data-filter="all" class="filter active"><a href="#">All</a></li>
                <li data-filter="development" class="filter"><a href="#">Standard Roses</a></li>
                <li data-filter="photography" class="filter"><a href="#">Carnations</a></li>
                <li data-filter="design" class="filter"><a href="#">Flowers & Fillers</a></li> -->
                <li data-filter="all" id="all_id" class=""><a href="javascript:void(0);" onclick="get_subcat()">All</a></li>
<?php
$main_cat_array = array();
while ($cat = mysqli_fetch_array($rs_cat)) {
    $pname = preg_replace("![^a-z0-9]+!i", "-", trim($cat["name"]));
    ?>
                    <li data-filter="<?php echo $cat["id"]; ?>" class="filter <?php if($cat['id'] == $_REQUEST['cat']) echo 'active';?>">
                            <!-- <a href="<?php echo SITE_URL ?><?php echo trim(strtolower($pname)) ?>/<?php echo $cat["id"] ?>"> <?php echo $cat["name"] ?> </a> -->
                        <a href="javascirpt:void(0);" onclick="get_subcat(<?php echo $cat["id"]; ?>)"> <?php echo $cat["name"] ?> </a>
                    </li>
    <?php
    array_push($main_cat_array, $cat["id"]);
}
?>
            </ul>
        </div>
        <!-- /Portfolio Filter -->
        <div class="divider divider-dotted"><!-- divider --></div>

        <div class="row">

            <!-- LEFT -->
            <div class="col-md-3 col-sm-3">

                <!-- SIDE NAV -->
                <div class="margin-bottom-60">


                    <h4>FILTERS</h4>


                    <form  method="get" action="">
                        <label class="size-12 margin-top-10">Varieties</label>
                        <div class="panel-default">
                                <!-- <input type="text" placeholder="Keywords" id="product_auto_search" class="form-control" />
                                <input type="hidden" id="hdnmyval" name="hdnmyval" style="border:1px solid;" value="" /> -->
<?php
$vari_s = "select p.name,p.id from product p 
             left join grower_product gp on p.id=gp.product_id
             left join growers g on gp.grower_id=g.id
            where g.active='active' GROUP BY p.id";

$rs_vari = mysqli_query($con,$vari_s);
?>
                            <div class="panel-default">
                                <select class="form-control select2" id="vari_change">	

                                    <option value="">All</option>
                            <?php
                            while ($row_vari = mysqli_fetch_array($rs_vari)) {
                                //echo "<pre>";print_r($sub_cat);echo "</pre>";exit();
                                ?>
                                        <option value="<?php echo $row_vari['id'] ?>"><?php echo $row_vari['name'] ?></option>
                                    <?php } ?>

                                </select>
                            </div>
                            <!-- <div id="divResult"></div> -->

                        </div>
                        <div class="btn-submit" style="text-align: right; margin-top: 10px;display:none;">
                            <a href="javascript:void(0)" id="btn-keyword" style="padding: 5px; color: white ; border-radius: 3px; background: rgb(164, 186, 61) none repeat scroll 0% 0%; position: relative; margin-top: 38px;"  class="keyword-btn">Submit</a>
                        </div>

                        <!-- <label class="size-12 margin-top-10">Category</label>
<?php
$sel_products_sub = "select s.* from subcategory s 
							   left join grower_product gp on s.id=gp.subcaegoryid
							   left join growers g on gp.grower_id=g.id
							   where g.active='active' group by s.id order by s.name";

$rs_products_sub = mysqli_query($con,$sel_products_sub);
?>
                        <div class="panel-default">
                                <select class="form-control select2" id="sub_cat_change">	
                                        
                                        <option value="">All</option>
                        <?php
                        while ($sub_cat = mysqli_fetch_array($rs_products_sub)) {
                            //echo "<pre>";print_r($sub_cat);echo "</pre>";exit();
                            ?>
                                                    <option value="<?php echo $sub_cat['id'] ?>"><?php echo $sub_cat['name'] ?></option>
<?php } ?>
                                        
                                </select>
                        </div>
                        -->

                        <label class="size-12 margin-top-10">Growers</label>
                        <?php
                        $growers_sql = "select * from growers where active='active' LIMIT 0,5";

                        $rs_growers = mysqli_query($con,$growers_sql);
                        ?>
                        <div class="panel-default">
                            <select class="form-control select2" id="sub_cat_change">	

                                <option value="">All</option>
                        <?php
                        while ($row_grower = mysqli_fetch_array($rs_growers)) {
                            //echo "<pre>";print_r($sub_cat);echo "</pre>";exit();
                            ?>
                                    <option value="<?php echo $row_grower['id'] ?>"><?php echo $row_grower['growers_name'] ?></option>
<?php } ?>

                            </select>
                        </div>

                <!--     (Pedido Eduardo Sprint)   <label class="size-12 margin-top-10">Availability</label>
                        <select class="form-control" id="availability_check">
                            <option value="">All</option>
                            <option value="active">In Stock</option>
                            <option value="deactive">Not in stock</option>
                        </select>   Sprint -->


                    </form>
                    <hr>
                    <div class="side-nav margin-bottom-60">

                        <div class="side-nav-head">
                            <button class="fa fa-bars"></button>
                            <h4>ALL CATEGORIES</h4>
                        </div>
<?php
$sel_cat = "select gp.categoryid,c.name,c.id
		from grower_product gp 
		left join growers g on gp.grower_id=g.id
		left join category c on gp.categoryid=c.id
		where g.active='active' group by gp.categoryid order by c.name";

$rs_cat = mysqli_query($con,$sel_cat);
?>
                        <ul class="list-group list-group-bordered list-group-noicon uppercase">
                        <?php
                        while ($row_cat = mysqli_fetch_array($rs_cat)) {
                            ?>
                                <li class="list-group-item active" id="main_cat_<?php echo $row_cat['categoryid']; ?>">
                                    <a href="#" onclick="dropCatUpDown(<?php echo $row_cat['categoryid']; ?>)" class="dropdown-toggle"><?php echo $row_cat['name'] ;?></a>
                            <?php
                            $sel_products_sub = "select s.* from subcategory s 
						   left join grower_product gp on s.id=gp.subcaegoryid
						   left join growers g on gp.grower_id=g.id
						  where g.active='active' AND gp.categoryid='" . $row_cat['categoryid'] . "' group by s.id order by s.name";
                            
                            $rs_products_sub = mysqli_query($con,$sel_products_sub);
                            ?>
                                    <ul style="display: none;" class="sub_cat_class" id="sub_cat_<?php echo $row_cat['categoryid']; ?>">

                                    <?php
                                    while ($sub_cat = mysqli_fetch_array($rs_products_sub)) {
                                        $sel_products_cnt = "select p.id as cnt_product from product p 
                                                                left join grower_product gp on p.id=gp.product_id
                                                                left join growers g on gp.grower_id=g.id
                                                               where g.active='active' 
                                                                 and p.subcategoryid='" . $sub_cat['id'] . "' 
                                                                GROUP BY p.id";
                                        
                                        $rs_pro_cnt = mysqli_query($con,$sel_products_cnt);
                                        $total_pro_sub = mysqli_num_rows($rs_pro_cnt);
                                        ?> 	
                                            <li id="sub_li_<?php echo $sub_cat['id']; ?>">
                                                <a href="javascript:void(0);" onclick="GetSubCatData(<?php echo $sub_cat['id']; ?>)">
                                                    <span class="size-11 text-muted pull-right">(<?php echo $total_pro_sub; ?>)</span> <?php echo $sub_cat['name']; ?>
                                                </a>
                                                <input type="hidden" id="toggleState_<?php echo $sub_cat['id']; ?>" value="0" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                    <?php } ?>
                        </ul>

                    </div>

<?php
$sel_colors = "select * from colors GROUP BY id order by name";
$rs_colors = mysqli_query($con,$sel_colors);
?>
                    <div class="margin-bottom-60" id="color-box">
                        <h4>Color COLOR</h4>
                            <?php
                            while ($colors = mysqli_fetch_array($rs_colors)) {
                                $pname = preg_replace("![^a-z0-9]+!i", "-", trim($cat["name"]));

	                     if ($colors["name"] == "Bicolor") {
                                    ?>
                                <a style="background: linear-gradient(to right, red,orange,yellow,green,blue,indigo,violet);" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                        <?php } elseif ($colors["name"] == "Black") {
                            ?>
                                <a style="background-color:#000000" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Blue") {
                                ?>
                                <a style="background-color:#6A601F" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Burgundy") {
                                ?>
                                <a style="background-color:#45001C" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Cream") {
                                ?>
                                <a style="background-color:#FAF2D1" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Earth Tones") {
                                ?>
                                <a style="background-color:#C3532E" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Green") {
                                ?>
                                <a style="background-color:#42001D" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Hot Pink") {
                                ?>
                                <a style="background-color:#7A313D" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Lavander") {
                                ?>
                                <a style="background-color:#7A313D" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Light Pink") {
                                ?>
                                <a style="background-color:#CEB5B0" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Orange") {
                                ?>
                                <a style="background-color:#C8A2C8" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Peach") {
                                ?>
                                <a style="background-color:#D8D97B" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Pink") {
                                ?>
                                <a style="background-color:#CA927D" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Purple") {
                                ?>
                                <a style="background-color:#E59570" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Red") {
                                ?>
                                <a style="background-color:#6B3045" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "White") {
                                ?>
                                <a style="background-color:#E9E7DA" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php } elseif ($colors["name"] == "Yellow") {
                                ?>
                                <a style="background-color:#E9E7DA" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>								
                            <?php } else {
                                ?>
                                <a style="background-color:<?php echo $colors["name"]; ?>" href="javascript:void(0);" onclick="get_color_data(<?php echo $colors["id"]; ?>)" class="tag shop-color">&nbsp;</a>
                            <?php }
                            ?>

                            <?php
                        }
                        ?>
                        <hr>
                        <div class="clearfix size-12">
                            <a href="javascript:void(0);" onclick="DeleteColor();" class="pull-right glyphicon glyphicon-remove"></a>
                            SELECTED COLOR : <span id="selected-color-id"> All Color </span>
                        </div>
                    </div>


                </div>
                <!-- /SIDE NAV -->






            </div>

            <!-- RIGHT -->
            <div class="col-md-9 col-sm-9">


                <div id="portfolio" class="portfolio-gutter" style="min-height:1090px;">

                    <div class="row mix-grid" id="result_data" >
<?php
$sel_products = "select p.* from product p 
									   left join grower_product gp on p.id=gp.product_id
									   left join growers g on gp.grower_id=g.id
									   where g.active='active' GROUP BY p.id limit 0,12";

$rs_products = mysqli_query($con,$sel_products);

while ($products = mysqli_fetch_array($rs_products)) {
    //echo "<pre>";print_r($products);echo "</pre>";exit();

    $sel_catinfo = "select * from category where id='" . $products['categoryid'] . "'";

    $rs_catinfo = mysqli_query($con,$sel_catinfo);

    $catinfo = mysqli_fetch_array($rs_catinfo);

    $cname = preg_replace("![^a-z0-9]+!i", "-", trim($products["name"]));

    $absolute_path = $_SERVER['DOCUMENT_ROOT'] . "/staging/" . $products['image_path'];
    //echo '<pre>';print_r($products);die;
    //if (file_exists($absolute_path)) {
        ?>  
                                <div class="col-md-4 col-sm-4 mix <?php echo $products['color_id']; ?>"><!-- item -->

                                    <div class="item-box">
                                        <figure>
                                            <span class="item-hover">
                                                <span class="overlay dark-5"></span>
                                                <span class="inner">

                                                    <!-- lightbox -->
                                                    <a class="ico-rounded lightbox" href="<?php echo SITE_URL.$products['image_path'] ?>" data-plugin-options='{"type":"image"}'>
                                                        <span class="fa fa-plus size-20"></span>
                                                    </a>

                                                    <!-- details -->
                                                    <a class="ico-rounded" href="<?php echo SITE_URL.'variety-sub-page.php?id='.$products['id'];?>"> 
                                                        <span class="glyphicon glyphicon-option-horizontal size-20"></span>
                                                    </a>

                                                </span>
                                            </span>
                                            <div class="item-box-overlay-title">
                                                <h3><?php echo $products["name"]; ?></h3>
                                                <ul class="list-inline categories nomargin">
                                                    <li><a href="#"><?php echo $catinfo['name'] ?></a></li>
                                                </ul>
                                            </div>

                                            <!-- carousel -->
                                            <div class="owl-carousel buttons-autohide controlls-over nomargin" data-plugin-options='{"singleItem": true, "autoPlay": false, "navigation": false, "pagination": true, "transitionStyle":"fade"}'>
                                                <div>
                                                    <img class="img-responsive" src="<?php echo SITE_URL.$products['image_path'] ?>" width="600" height="399" alt="">
                                                </div>

                                            </div>
                                            <!-- /carousel -->

                                        </figure>
                                    </div>

                                </div><!-- /item -->
    <?php //} ?>
<?php } ?>




                    </div>
                    <input type="hidden" name="hdn_main_cat" id="hdn_main_cat" value="" />
                    <input type="hidden" name="hdn_sub_cat" id="hdn_sub_cat" value="" />
                    <input type="hidden" name="hdn_selected_color" id="hdn_selected_color" value="" />


                    <div class="animation_image" style="display:none;" align="center">
                        <img src="<?php echo SITE_URL; ?>/includes/assets/images/loaders/5.gif"></div>
                </div>
                <div class="divider divider-dotted"><!-- divider --></div>


                <div class="text-right">

                    <!-- Pagination Default -->
                    <!-- <ul class="pagination pagination-simple">
                            <li><a href="#">prev</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">next</a></li>
                    </ul> -->
                    <!-- /Pagination Default -->

                </div>

            </div>


        </div>

    </div>


</div>
</section>
<!-- / -->
<!-- PRELOADER -->
<!--<div id="preloader">
    <div class="inner">
        <span class="loader"></span>
    </div>
</div> /PRELOADER -->
<!-- SCROLL TO TOP -->
<a href="#" id="toTop"></a>
<?php
require_once '../includes/footer.php';
?>





<script type="text/javascript" src="<?php echo SITE_URL; ?>/includes/assets/js/jquery.tokeninput.js"></script>

<!--Modal-->
<div class="modal fade bs-example-modal-lg-varieties" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Varieties&nbsp;&nbsp;<span id="selected_varieties" style="font-size: 15px;"></span></h4>
            </div>
            <!-- body modal -->
            <div class="modal-body">
                <div id="datatable_sample_wrapper" class="dataTables_wrapper no-footer"><div class="table-scrollable"><!-- HTML DATATABLES -->
                        <table class="table table-striped table-bordered table-hover" id="datatable_sample">
                            <thead>
                                <tr>
                                    <th class="table-checkbox">
                                        <input type="checkbox" class="group-checkable varieties_chk" data-set="#datatable_sample .checkboxes"/>
                                    </th>
                                    <th>Variety</th>

                                </tr>
                            </thead>
                            <tbody>
<?php
$vari_products_all = "select p.name,p.id from product p 
							   left join grower_product gp on p.id=gp.product_id
							   left join growers g on gp.grower_id=g.id
							   where g.active='active' GROUP BY p.id";
$rs_vari_all = mysqli_query($con,$vari_products_all);
$product_array = array();

while ($row_vari_all = mysqli_fetch_array($rs_vari_all)) {
    ?>
                                    <tr class="odd gradeX">
                                        <td><input type="checkbox" class="checkboxes varieties_chk" checkbox_text="<?php echo $row_vari_all['name']; ?>" value="<?php echo $row_vari_all['id']; ?>"/>
                                        </td>
                                        <td>
    <?php echo $row_vari_all['name']; ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="modal-footer"><!-- modal footer -->
                <!-- <button data-dismiss="modal" class="btn btn-default">Close</button>  -->
                <button class="btn btn-primary" id="see_more_varieties" >Apply Selection</button>
            </div>
        </div>
    </div>
</div>
<!--/Modal-->

<!--Modal-->
<div class="modal fade bs-example-modal-lg-color" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Colors&nbsp;&nbsp;<span id="selected_colors" style="font-size: 15px;"></span></h4>
            </div>
            <!-- body modal -->
            <div class="modal-body">
                <div id="datatable_sample_wrapper_color" class="dataTables_wrapper no-footer"><div class="table-scrollable"><!-- HTML DATATABLES -->
                        <table class="table table-striped table-bordered table-hover" id="datatable_colors">
                            <thead>
                                <tr>
                                    <th class="table-checkbox">
                                        <input type="checkbox" class="group-checkable"  data-set="#datatable_sample .checkboxes"/>
                                    </th>
                                    <th>Colors</th>
                                </tr>
                            </thead>
                            <tbody>
<?php
$sel_colors_all = "select * from colors order by name";
$rs_colors_all = mysqli_query($con,$sel_colors_all);

while ($colors_all = mysqli_fetch_array($rs_colors_all)) {
    ?>
                                    <tr class="odd gradeX">
                                        <td><input type="checkbox" class="checkboxes color_name_chk" checkbox_text="<?php echo $colors_all['name']; ?>"  value="<?php echo $colors_all['id']; ?>"/>
                                        </td>
                                        <td>
    <?php echo $colors_all['name']; ?>
                                        </td>

                                    </tr>
<?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer"><!-- modal footer -->
                <!-- <button data-dismiss="modal" class="btn btn-default">Close</button>  -->
                <button class="btn btn-primary" id="see_more_colors" >Apply Selection</button>
            </div>
        </div>
    </div>
</div>
<!--/Modal-->

<!--Modal-->
<div class="modal fade bs-example-modal-lg-gro" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myLargeModalLabel">Grower&nbsp;&nbsp;<span id="selected_gro" style="font-size: 15px;"></span></h4>
            </div>
            <!-- body modal -->
            <div class="modal-body">
                <div id="datatable_sample_wrapper_gro" class="dataTables_wrapper no-footer"><div class="table-scrollable"><!-- HTML DATATABLES -->
                        <table class="table table-striped table-bordered table-hover" id="datatable_gro">
                            <thead>
                                <tr>
                                    <th class="table-checkbox">
                                        <input type="checkbox" class="group-checkable " data-set="#datatable_sample .checkboxes"/>
                                    </th>
                                    <th>Grower Name</th>

                                </tr>
                            </thead>
                            <tbody>
<?php
$growers_sql_all = "select * from growers where active='active'";

$rs_growers_all = mysqli_query($con,$growers_sql_all);


while ($row_grower_all = mysqli_fetch_array($rs_growers_all)) {
    ?>
                                    <tr class="odd gradeX">
                                        <td><input type="checkbox" class="checkboxes growers_name_chk" checkbox_text="<?php echo $row_grower_all['growers_name']; ?>"  value="<?php echo $row_grower_all['id']; ?>"/>
                                        </td>
                                        <td>
    <?php echo $row_grower_all['growers_name']; ?>
                                        </td>

                                    </tr>
<?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer"><!-- modal footer -->
                <!-- <button data-dismiss="modal" class="btn btn-default">Close</button>  -->
                <button class="btn btn-primary" id="see_more_gro" >Apply Selection</button>
            </div>
        </div>
    </div>
</div>
<!--/Modal-->	
                                        <?php

                                        function clean($string) {
                                            //$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

                                            return preg_replace('/[^A-Za-z0-9 \-]/', '', $string); // Removes special chars.
                                        }

                                        $auto_products = "select p.name,p.id from product p 
	   left join grower_product gp on p.id=gp.product_id
	   left join growers g on gp.grower_id=g.id
	   where g.active='active' GROUP BY p.id";

                                        $rs_auto = mysqli_query($con,$auto_products);
                                        $product_auto_name = "";
                                        /* while($row_auto=mysqli_fetch_array($rs_auto))
                                          {
                                          $product_auto_name.=$row_auto['name'].",";
                                          }
                                          $product_auto_name=trim($product_auto_name,","); */
                                        //$product_auto_name.="[";
                                        $product_array = array();
                                        $i_cnt = 0;
                                        while ($row_auto = mysqli_fetch_array($rs_auto)) {
                                            //$product_auto_name.="{id: ".$row_auto['id'].", name: '".$row_auto['name']."'},";
                                            $product_array[$i_cnt]['id'] = $row_auto['id'];
                                            $product_array[$i_cnt]['name'] = clean($row_auto['name']);
                                            $i_cnt++;
                                        }
                                        //echo "<pre>";print_r($product_array);echo "</pre>";exit();
                                        $json_convert = json_encode($product_array);
                                        //echo $json_convert;
                                        //$product_auto_name.="]";
                                        //echo $product_auto_name;
                                        ?>
    
<script type="text/javascript">
                    //<!-- JS DATATABLES -->
                            loadScript(plugin_path + "datatables/js/jquery.dataTables.min.js", function () {
                                loadScript(plugin_path + "datatables/dataTables.bootstrap.js", function () {

                                    if (jQuery().dataTable) {

                                        var table = jQuery('#datatable_sample');
                                        table.dataTable({
                                            "columns": [{
                                                    "orderable": false
                                                }, {
                                                    "orderable": true
                                                }],
                                            "lengthMenu": [
                                                [5, 15, 20, -1],
                                                [5, 15, 20, "All"] // change per page values here
                                            ],
                                            // set the initial value
                                            "pageLength": 5,
                                            "pagingType": "bootstrap_full_number",
                                            "language": {
                                                "lengthMenu": "  _MENU_ records",
                                                "paginate": {
                                                    "previous": "Prev",
                                                    "next": "Next",
                                                    "last": "Last",
                                                    "first": "First"
                                                }
                                            },
                                            "columnDefs": [{// set default column settings
                                                    'orderable': false,
                                                    'targets': [0]
                                                }, {
                                                    "searchable": false,
                                                    "targets": [0]
                                                }],
                                            "order": [
                                                [1, "asc"]
                                            ] // set first column as a default sort by asc
                                        });

                                        var tableWrapper = jQuery('#datatable_sample_wrapper');

                                        table.find('.group-checkable').change(function () {
                                            var set = jQuery(this).attr("data-set");
                                            var checked = jQuery(this).is(":checked");
                                            jQuery(set).each(function () {
                                                if (checked) {
                                                    jQuery(this).attr("checked", true);
                                                    jQuery(this).parents('tr').addClass("active");
                                                } else {
                                                    jQuery(this).attr("checked", false);
                                                    jQuery(this).parents('tr').removeClass("active");
                                                }
                                            });
                                            jQuery.uniform.update(set);
                                        });

                                        var table = jQuery('#datatable_colors');
                                        table.dataTable({
                                            "columns": [{
                                                    "orderable": false
                                                }, {
                                                    "orderable": true
                                                }],
                                            "lengthMenu": [
                                                [5, 15, 20, -1],
                                                [5, 15, 20, "All"] // change per page values here
                                            ],
                                            // set the initial value
                                            "pageLength": 5,
                                            "pagingType": "bootstrap_full_number",
                                            "language": {
                                                "lengthMenu": "  _MENU_ records",
                                                "paginate": {
                                                    "previous": "Prev",
                                                    "next": "Next",
                                                    "last": "Last",
                                                    "first": "First"
                                                }
                                            },
                                            "columnDefs": [{// set default column settings
                                                    'orderable': false,
                                                    'targets': [0]
                                                }, {
                                                    "searchable": false,
                                                    "targets": [0]
                                                }],
                                            "order": [
                                                [1, "asc"]
                                            ] // set first column as a default sort by asc
                                        });

                                        var tableWrapper = jQuery('#datatable_sample_wrapper_color');

                                        table.find('.group-checkable').change(function () {
                                            var set = jQuery(this).attr("data-set");
                                            var checked = jQuery(this).is(":checked");
                                            jQuery(set).each(function () {
                                                if (checked) {
                                                    jQuery(this).attr("checked", true);
                                                    jQuery(this).parents('tr').addClass("active");
                                                } else {
                                                    jQuery(this).attr("checked", false);
                                                    jQuery(this).parents('tr').removeClass("active");
                                                }
                                            });
                                            jQuery.uniform.update(set);
                                        });

                                        var table = jQuery('#datatable_gro');
                                        table.dataTable({
                                            "columns": [{
                                                    "orderable": false
                                                }, {
                                                    "orderable": true
                                                }],
                                            "lengthMenu": [
                                                [5, 15, 20, -1],
                                                [5, 15, 20, "All"] // change per page values here
                                            ],
                                            // set the initial value
                                            "pageLength": 5,
                                            "pagingType": "bootstrap_full_number",
                                            "language": {
                                                "lengthMenu": "  _MENU_ records",
                                                "paginate": {
                                                    "previous": "Prev",
                                                    "next": "Next",
                                                    "last": "Last",
                                                    "first": "First"
                                                }
                                            },
                                            "columnDefs": [{// set default column settings
                                                    'orderable': false,
                                                    'targets': [0]
                                                }, {
                                                    "searchable": false,
                                                    "targets": [0]
                                                }],
                                            "order": [
                                                [1, "asc"]
                                            ] // set first column as a default sort by asc
                                        });

                                    }

                                });
                            });

                    jQuery(function () {
                        
                        var auto_p = '<?php echo $json_convert; ?>';
                        var obj = JSON.parse(auto_p);
                        jQuery("#product_auto_search").tokenInput(obj);

                        jQuery("#btn-keyword").click(function () {

                            var selected_id = jQuery("#product_auto_search").val();
                            var dataString = 'searchword=' + selected_id;
                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html)
                                {
                                    jQuery("#result_data").html(html);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });

                        });
                        jQuery("#sub_cat_change").change(function () {
                            var inputSearch = ''; //jQuery("#product_auto_search").val();
                            var subcat_id = jQuery("#sub_cat_change").val();
                            //alert(subcat_id);

                            var site_url = "<?php echo SITE_URL; ?>";
                            var dataString = 'searchword=' + inputSearch + '&growers_id=' + subcat_id;

                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html)
                                {
                                    jQuery("#result_data").html(html);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });


                            return false;
                        });
                        jQuery("#availability_check").change(function () {
                            var inputSearch = '';// jQuery("#product_auto_search").val();
                            var subcat_id = jQuery("#sub_cat_change").val();
                            var availability_check = jQuery("#availability_check").val();
                            //alert(subcat_id);
                            var site_url = "<?php echo SITE_URL; ?>";

                            var dataString = 'searchword=' + inputSearch + '&subcat_id=' + subcat_id + "&availability_check=" + availability_check;
                            if (availability_check != '')
                            {
                                jQuery.ajax({
                                    type: "POST",
                                    url: "<?php echo SITE_URL; ?>search_variety.php",
                                    data: dataString,
                                    success: function (html)
                                    {
                                        jQuery("#result_data").html(html);
                                        _mixitup();
                                        _owl_carousel();
                                    }
                                });
                            }

                            return false;
                        });
                        jQuery(".growers_name_chk").change(function () {

                            var already_exits_gro = "";
                            already_exits_gro = jQuery("#hdn_gro_selected_text").val();
                            var already_exits_gro_v = "";
                            already_exits_gro_v = jQuery("#hdn_gro_selected").val();

                            jQuery(".growers_name_chk").each(function () {
                                if (jQuery(this).is(":checked"))
                                {
                                    //grower_checked+=jQuery(this).val()+",";
                                    if (already_exits_gro == "")
                                    {
                                        jQuery("#hdn_gro_selected_text").val(jQuery(this).attr("checkbox_text"));
                                        jQuery("#hdn_gro_selected").val(jQuery(this).val());
                                    }
                                    else
                                    {
                                        jQuery("#hdn_gro_selected_text").val(already_exits_gro + "," + jQuery(this).attr("checkbox_text"));
                                        jQuery("#hdn_gro_selected").val(already_exits_gro_v + "," + jQuery(this).val());
                                    }
                                }

                            });
                            grower_checked = jQuery("#hdn_gro_selected_text").val();
                            //jQuery("#hdn_gro_selected_text").val();
                            jQuery("#selected_gro").html("[" + grower_checked + "]");


                        });
                        jQuery("#see_more_gro").click(function () {

                            var color_checked = "";
                            var varieties_checked = "";
                            var grower_checked = "";

                            grower_checked = jQuery("#hdn_gro_selected").val();

                            var varieties_checked = "";


                            //alert(color_checked);
                            var inputSearch = '';//jQuery("#product_auto_search").val();
                            var subcat_id = jQuery("#sub_cat_change").val();
                            var availability_check = jQuery("#availability_check").val();

                            var dataString = 'searchword=' + inputSearch + '&subcat_id=' + subcat_id + "&grower_checked=" + grower_checked + "&color_checked=" + color_checked + "&varieties_checked=" + varieties_checked + "&availability_check=" + availability_check;
                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html)
                                {
                                    jQuery(".bs-example-modal-lg-gro").modal('toggle');
                                    jQuery("#result_data").html(html);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });

                        });

                        jQuery(".color_name_chk").change(function () {

                            var already_exits_color = "";
                            already_exits_color_v = jQuery("#hdn_color_selected").val();
                            already_exits_color = jQuery("#hdn_color_selected_text").val();
                            //alert(already_exits_color);
                            jQuery(".color_name_chk").each(function () {
                                if (jQuery(this).is(":checked"))
                                {
                                    if (already_exits_color == "")
                                    {
                                        jQuery("#hdn_color_selected_text").val(jQuery(this).attr("checkbox_text"));
                                        jQuery("#hdn_color_selected").val(jQuery(this).val());
                                    }
                                    else
                                    {
                                        jQuery("#hdn_color_selected_text").val(already_exits_color + "," + jQuery(this).attr("checkbox_text"));
                                        jQuery("#hdn_color_selected").val(already_exits_color_v + "," + jQuery(this).val());
                                    }
                                    //color_checked+=jQuery(this).val()+",";
                                    //alert(jQuery(this).val());
                                }

                            });
                            color_checked = jQuery("#hdn_color_selected_text").val();
                            jQuery("#selected_colors").html("[" + color_checked + "]");



                        });

                        jQuery("#see_more_colors").click(function () {
                            var grower_checked = "";
                            jQuery(".growers_name_chk").each(function () {
                                if (jQuery(this).is(":checked"))
                                {
                                    grower_checked += jQuery(this).val() + ",";
                                }

                            });

                            var color_checked = "";

                            color_checked = jQuery("#hdn_color_selected").val();

                            var varieties_checked = "";

                            jQuery(".varieties_chk").each(function () {
                                if (jQuery(this).is(":checked"))
                                {
                                    varieties_checked += jQuery(this).val() + ",";
                                    //alert(jQuery(this).val());
                                }

                            });
                            //alert(color_checked);
                            var inputSearch = jQuery("#product_auto_search").val();
                            var subcat_id = jQuery("#sub_cat_change").val();
                            var availability_check = jQuery("#availability_check").val();

                            var dataString = 'searchword=' + inputSearch + '&subcat_id=' + subcat_id + "&grower_checked=" + grower_checked + "&color_checked=" + color_checked + "&varieties_checked=" + varieties_checked + "&availability_check=" + availability_check;
                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html)
                                {
                                    jQuery(".bs-example-modal-lg-color").modal('toggle');
                                    jQuery("#result_data").html(html);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });

                        });

                        jQuery(".varieties_chk").change(function () {



                            var already_exits_vari = "";
                            already_exits_vari = jQuery("#hdn_varieties_selected_text").val();
                            already_exits_vari_v = jQuery("#hdn_varieties_selected").val();

                            jQuery(".varieties_chk").each(function () {
                                if (jQuery(this).is(":checked"))
                                {
                                    if (already_exits_vari == "")
                                    {
                                        jQuery("#hdn_varieties_selected_text").val(jQuery(this).attr("checkbox_text"));
                                        jQuery("#hdn_varieties_selected").val(jQuery(this).val());
                                    }
                                    else
                                    {
                                        jQuery("#hdn_varieties_selected_text").val(already_exits_vari + "," + jQuery(this).attr("checkbox_text"));
                                        jQuery("#hdn_varieties_selected").val(already_exits_vari_v + "," + jQuery(this).val());
                                    }
                                    //varieties_checked+=jQuery(this).val()+",";
                                    //alert(jQuery(this).val());
                                }

                            });

                            varieties_checked = jQuery("#hdn_varieties_selected_text").val();
                            jQuery("#selected_varieties").html("[" + varieties_checked + "]");



                        });

                        jQuery("#see_more_varieties").click(function () {
                            var grower_checked = "";
                            jQuery(".growers_name_chk").each(function () {
                                if (jQuery(this).is(":checked"))
                                {
                                    grower_checked += jQuery(this).val() + ",";

                                }

                            });

                            var color_checked = "";

                            jQuery(".color_name_chk").each(function () {
                                if (jQuery(this).is(":checked"))
                                {
                                    color_checked += jQuery(this).val() + ",";
                                    //alert(jQuery(this).val());
                                }

                            });

                            var varieties_checked = "";

                            varieties_checked = jQuery("#hdn_varieties_selected").val();


                            //alert(color_checked);
                            var inputSearch = jQuery("#product_auto_search").val();
                            var subcat_id = jQuery("#sub_cat_change").val();
                            var availability_check = jQuery("#availability_check").val();

                            var dataString = 'searchword=' + inputSearch + '&subcat_id=' + subcat_id + "&grower_checked=" + grower_checked + "&color_checked=" + color_checked + "&varieties_checked=" + varieties_checked + "&availability_check=" + availability_check;
                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html)
                                {
                                    jQuery(".bs-example-modal-lg-varieties").modal('toggle');
                                    jQuery("#result_data").html(html);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });

                        });

                        jQuery("#vari_change").change(function () {

                            var vari_id = jQuery("#vari_change").val();
                            //alert(subcat_id);

                            var site_url = "<?php echo SITE_URL; ?>";
                            var dataString = 'varieties_checked=' + vari_id;

                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html)
                                {
                                    //jQuery("#result_data").html(html);
                                    //jQuery("#result_data").html(html);
                                    var split_html = html.split("#$#$#$#$#$");
                                    //alert(split_html[0]);
                                    //alert(split_html[1]);
                                    jQuery("#result_data").html(split_html[0]);
                                    //jQuery("#color-box").html(split_html[1]);

                                    _mixitup();
                                    _owl_carousel();
                                }
                            });


                            return false;
                        });

                        var track_load = 1; //total loaded record group(s)
                        var loading = false; //to prevents multipal ajax loads
                        var total_groups = <?php echo $i_cnt; ?>; //total record group(s)
                        //var total_groups =3;
                        jQuery(window).scroll(function () { //detect page scroll

                            //alert((jQuery(window).scrollTop() + jQuery(window).height()) + " == "+(jQuery(document).height() - 1000))
                            if (jQuery(window).scrollTop() + jQuery(window).height() >= (jQuery(document).height() - 100))  //user scrolled to bottom of the page?
                            {

                                if (track_load <= total_groups && loading == false) //there's more data to load
                                {
                                    if (jQuery('.notfound').html() != "No More Data!")
                                    {
                                        loading = true; //prevent further ajax loading
                                        jQuery('.animation_image').show(); //show loading image

                                        var selected_main_cat = jQuery("#hdn_main_cat").val();
                                        var selected_sub_cat = jQuery("#hdn_sub_cat").val();
                                        var selected_color = jQuery("#hdn_selected_color").val();


                                        //load data from the server using a HTTP POST request
                                        jQuery.post('autoload_product.php', {'group_no': track_load, 'cate_id': selected_main_cat, 'selected_color': selected_color, 'selected_sub_cat': selected_sub_cat}, function (data) {

                                            jQuery("#result_data").append(data); //append received data into the element
                                            _mixitup();
                                            _owl_carousel();
                                            //hide loading image
                                            jQuery('.animation_image').hide(); //hide loading image once data is received

                                            track_load++; //loaded group increment
                                            loading = false;

                                        }).fail(function (xhr, ajaxOptions, thrownError) { //any errors?

                                            alert(thrownError); //alert with HTTP error
                                            jQuery('.animation_image').hide(); //hide loading image
                                            loading = false;

                                        });
                                    }

                                }
                            }
                        });
                    });
                    function get_subcat(main_id) {

                        var site_url = "<?php echo SITE_URL; ?>";
                        
                        jQuery(".list-group-item").removeClass("active");
                        jQuery(".sub_cat_class").hide();
                        jQuery("#hdn_main_cat").val(main_id);
                        jQuery("#main_cat_" + main_id).addClass("active");
                        jQuery("#sub_cat_" + main_id).show();
                        var dataString = 'action=subcatdata&cat_main_id=' + main_id;
                        jQuery.ajax({
                            type: "POST",
                            url: "<?php echo SITE_URL; ?>/en/get_subcat.php",
                            data: dataString,
                            success: function (html)
                            {
                                //jQuery("#result_data").html(html);
                                var split_html = html.split("#$#$#$#$#$");
                                //alert(split_html[0]);
                                //alert(split_html[1]);
                                jQuery("#result_data").html(split_html[0]);
                                jQuery("#color-box").html(split_html[1]);

                                _mixitup();
                                _owl_carousel();
                            }
                        });
                    }
                    function get_color_data(id) {
                        var site_url = "<?php echo SITE_URL; ?>";
                        var selected_main_cat = jQuery("#hdn_main_cat").val();

                        var already_exite_color = jQuery("#hdn_selected_color").val();
                        //if(already_exite_color == "")
                        // {
                        jQuery("#hdn_selected_color").val(id);
                        // }
                        /* else
                         {
                         jQuery("#hdn_selected_color").val(already_exite_color+","+id);	
                         }*/
                        var selected_color = jQuery("#hdn_selected_color").val();

                        var dataString = 'selected_color=' + selected_color + '&selected_main_cat=' + selected_main_cat;
                        jQuery.ajax({
                            type: "POST",
                            url: "<?php echo SITE_URL; ?>search_variety.php",
                            data: dataString,
                            success: function (html)
                            {
                                var split_html = html.split("#$#$#$#$#$");
                                //alert(split_html[0]);
                                //alert(split_html[1]);
                                jQuery("#result_data").html(split_html[0]);
                                jQuery("#selected-color-id").html(split_html[1]);
                                _mixitup();
                                _owl_carousel();
                            }
                        });

                    }


                    function GetSubCatData(id) {
                        var toggleState = jQuery("#toggleState_" + id).val();

                        if (toggleState == 0)
                        {
                            //alert("add active");
                            jQuery("#sub_li_" + id).addClass("active");
                            var already_exite_sub = jQuery("#hdn_sub_cat").val();
                            if (already_exite_sub == "")
                            {
                                jQuery("#hdn_sub_cat").val(id);
                            }
                            else
                            {
                                //jQuery("#hdn_sub_cat").val(already_exite_sub + "," + id);
                                jQuery("#hdn_sub_cat").val(id);                                
                            }
                            var selected_sub_cat = jQuery("#hdn_sub_cat").val();

                            var selected_color = jQuery("#hdn_selected_color").val()

                            //alert("<?php echo SITE_URL; ?>");
                            var site_url = "<?php echo SITE_URL; ?>";

console.log("IF ");


                            var dataString = "subcategory_selected=" + selected_sub_cat;
                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html)
                                {
                                    jQuery("#toggleState_" + id).val("1");
                                    var split_html = html.split("#$#$#$#$#$");
                                    //alert(split_html[0]);
                                    //alert(split_html[1])
                                    jQuery("#result_data").html(split_html[0]);
                                    jQuery("#color_ul").html(split_html[1]);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });
                            
                        }
                        else
                        {
                            //alert("remove active");
                            jQuery("#toggleState_" + id).val("0");
                            jQuery("#sub_li_" + id).removeClass("active");

                            var already_exite_sub = jQuery("#hdn_sub_cat").val();
                            var split_al = already_exite_sub.split(",");
                            //alert(split_al.length);
                            var final_str = "";
                            for (i = 0; i < split_al.length; i++)
                            {
                                if (split_al[i] != id)
                                {
                                    final_str += split_al[i] + ",";
                                }
                            }
                            //alert(final_str);
                            jQuery("#hdn_sub_cat").val(jQuery.trim(final_str));
                            var selected_sub_cat = jQuery("#hdn_sub_cat").val();

                            var selected_color = jQuery("#hdn_selected_color").val()

                            //alert("<?php echo SITE_URL; ?>");
                            var site_url = "<?php echo SITE_URL; ?>";


console.log("ELSE ");

                            var dataString = "subcategory_selected=" + selected_sub_cat;
                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>search_variety.php",
                                data: dataString,
                                success: function (html)
                                {
                                    //jQuery("#toggleState_"+id).val("1");
                                    var split_html = html.split("#$#$#$#$#$");
                                    //alert(split_html[0]);
                                    //alert(split_html[1])
                                    jQuery("#result_data").html(split_html[0]);
                                    jQuery("#color_ul").html(split_html[1]);
                                    _mixitup();
                                    _owl_carousel();
                                }
                            });
                        }



                    }
                    function DeleteColor() {
                        jQuery("#selected-color-id").html("<strong>All Color</strong>");
                        var site_url = "<?php echo SITE_URL; ?>";
                        var selected_main_cat = "";

                        var already_exite_color = jQuery("#hdn_selected_color").val();

                        var selected_color = jQuery("#hdn_selected_color").val();

                        var dataString = 'selected_color=&selected_main_cat=';
                        jQuery.ajax({
                            type: "POST",
                            url: "<?php echo SITE_URL; ?>search_variety.php",
                            data: dataString,
                            success: function (html)
                            {
                                var split_html = html.split("#$#$#$#$#$");
                                //alert(split_html[0]);
                                //alert(split_html[1]);
                                jQuery("#result_data").html(split_html[0]);
                                //jQuery("#selected-color-id").html(split_html[1]);
                                _mixitup();
                                _owl_carousel();
                            }
                        });
                    }
                    function dropCatUpDown(id){
                        jQuery(".sub_cat_class").each(function(){
                            if (jQuery(this).find("li").hasClass("active"))
                            {
                                jQuery("#main_cat_" + id).addClass("active");
                            }
                        });
                    }
                    $(window).load(function(){
                        var main_id="<?php echo $_REQUEST['cat'];?>";
                        //alert(main_id);
                        if(typeof main_id === "undefined")
                        {
                        }
                        else
                        {
                            var site_url = "<?php echo SITE_URL; ?>";
                            jQuery("#all_id").removeClass('active');
                            jQuery(".list-group-item").removeClass("active");
                            jQuery(".sub_cat_class").hide();
                            jQuery("#hdn_main_cat").val(main_id);
                            jQuery("#main_cat_" + main_id).addClass("active");
                            jQuery("#sub_cat_" + main_id).show();
                            var dataString = 'action=subcatdata&cat_main_id=' + main_id;
                            jQuery.ajax({
                                type: "POST",
                                url: "<?php echo SITE_URL; ?>/en/get_subcat.php",
                                data: dataString,
                                success: function (html)
                                {
                                    //jQuery("#result_data").html(html);
                                    var split_html = html.split("#$#$#$#$#$");
                                    //alert(split_html[0]);
                                    //alert(split_html[1]);
                                    jQuery("#result_data").html(split_html[0]);
                                    jQuery("#color-box").html(split_html[1]);

                                    _mixitup();
                                    _owl_carousel();
                                }
                            });
                        }
                    });
</script>
