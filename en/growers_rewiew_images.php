<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php

  // PO  2018/07/13

require_once("../config/config_gcp.php");

$userSessionID = $_SESSION["buyer"];

################QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################

$pageId = 16;//VARIETY PAGE ID 

$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";

$pageQuery = mysqli_query($con, $pageSql);

$pageData = mysqli_fetch_assoc($pageQuery);

#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################



#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################

$cssHeadArray = array(SITE_URL.'../includes/assets/css/essentials-flfv3.css', SITE_URL.'/includes/assets/css/layout-flfv3.css', 

                            SITE_URL.'../includes/assets/css/header-1.css', SITE_URL.'/includes/assets/css/layout-shop.css', SITE_URL.'/includes/assets/css/color_scheme/blue.css' );

$jsHeadArray = array();

#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################

    

require_once '../includes/header.php';

?>

<style type="text/css">
  div.mega-price-table h5 {
    background-color: rgba(0, 0, 0, 0.03);
    color: #666;
    display: block;
    font-size: 15px;
    font-weight: 700;
    margin: 0;
    padding: 7px;
    text-align: center;
  }
  div.mega-price-table .pricing{
    margin-top: 1px!important;
  }
  #v5 .pricing-title{
    height: 133px;
  }
  ul#product-size-dd li{
    width: 100%;
  }
  div.mega-price-table .pricing-head small {
    background-color: inherit;
}



</style>



    <section class="page-header page-header-xs">

        <div class="container">

            <?php

            $productId = $_GET['id'];#100
            $gidprin = $_GET['id_grow'];

            $sel_product = "SELECT p.id , p.tab_desc1 , p.tab_desc2 , substr(p.tab_desc2,1,78) short_desc2,substr(p.tab_desc2,79,80) short_desc3, p.name , p.image_path , s.description ,c.name name_cat
                              FROM product p
                              JOIN subcategory s ON p.subcategoryid = s.id 
                              JOIN category c ON p.categoryid = c.id 
                             WHERE p.id = $productId";

            $rs_products = mysqli_query($con, $sel_product);

            $product = mysqli_fetch_assoc($rs_products);

            $name = preg_replace("![^a-z0-9]+!i", "-", trim($product["name"]));
            $name_cat = preg_replace("![^a-z0-9]+!i", "-", trim($product["name_cat"]));            

            $tab2 = $product["tab_desc2"];
            
            $sel_img = "SELECT gp.id , gp.product_id , gp.grower_id , gp.categoryid , gp.subcaegoryid , gp.colorid , gp.image_path
                          FROM grower_product gp
                         WHERE image_path IS NOT NULL 
                           AND gp.product_id = '".$productId."' 
                         order by image_path ";      

            $rs_img = mysqli_query($con, $sel_img);      
                       
            ?>

            <h1><?php echo $name; ?></h1>

            <!-- breadcrumbs -->

            <ol class="breadcrumb">

                <li><a href="#">Home</a></li>

                <li><a href="#">Shop</a></li>

                <li class="active">Single</li>

            </ol><!-- /breadcrumbs -->

        </div>

    </section>

    <!-- /PAGE HEADER -->



    <!--MAIN BODY SECTION-->

    <section>
        <div class="container">
            <div class="row">                
		<div class="col-md-4 col-sm-4 mix">
                    
		    <!--div class="thumbnail relative mb-3">                
                        <?php
                            $absolute_path = SITE_URL.$product['image_path'];                                         
                        ?>
                        <figure id="zoom-primary" class="zoom" data-mode="mouseover">
                            <a class="lightbox bottom-right" href="<?php echo $absolute_path; ?>" data-plugin-options='{"type":"image"}'><i class="glyphicon glyphicon-search"></i></a>
                            <img class="img-responsive" src="<?php echo $absolute_path; ?>" width="1200" height="1500" alt="This is the product title" />
                        </figure>
                    </div-->                                                                        

                    <!-- Thumbnails (required height:100px) -->
                    <div data-for="zoom-primary" class="zoom-more owl-carousel owl-padding-3 featured" data-plugin-options='{"singleItem": false, "autoPlay": false, "navigation": true, "pagination": false}'>
                            <?php 
                                       $sr=1;
                                    while($imagen=mysqli_fetch_array($rs_img)){
                                            $image_path = SITE_URL.$imagen['image_path'];
                            ?>     
                                        <a class="thumbnail active" href="<?php echo $image_path; ?>">
                                                <img src="<?php echo $image_path; ?>" height="500" alt="" />
                                        </a>
                            <?php  } ?>

                    </div>
                    <!-- /Thumbnails -->   

                </div>
                <!-- /IMAGE -->

<?php


 $sel_ava_box="select  sum(gpb.stock) as stock
                 from grower_product_box_packing gpb
            left join product p on gpb.prodcutid = p.id
            left join subcategory s on p.subcategoryid=s.id  
            left join colors c on p.color_id=c.id 
            left join features ff on gpb.feature=ff.id
            left join sizes sh on gpb.sizeid=sh.id 
            left join boxes b on gpb.box_id=b.id
            left join boxtype bt on b.type=bt.id
            left join growers g on gpb.growerid=g.id
            left join bunch_sizes bs on gpb.bunch_size_id=bs.id
                where g.active     != 'deactive' 
                  and gpb.prodcutid = '".$_REQUEST['id']."' 
                  and gpb.type     != 2  
                  and p.name is not null  
                  and gpb.stock > 0
                  and Date_format(date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y')                                                         
                group by sh.name";
          
  $dataMaximo = mysqli_query($con, $sel_ava_box);
  $box=0;
        while ($dt = mysqli_fetch_assoc($dataMaximo)) {
           $box=$box+ $dt['stock'];       
    }
?>

                <!-- ITEM DESC -->

                <div class="col-lg-5 col-sm-8">

                    <!-- /buttons -->

                    <!-- price -->

                    <div class="shop-item-price">
                        <?php echo $name = "IMAGES" ?>
                        <span id="average_price" class="average_price"></span>
                        <!--span class="pull-right text-success"><i class="fa fa-check"></i> <?php echo $box;?> boxes available</span-->                        
                    </div>

                    <!-- /price -->
                    <hr/>

                    <?php

                    $sel_ava_p="select gpb.prodcutid,gpb.id as gid,GROUP_CONCAT(gpb.price) as price,GROUP_CONCAT(gpb.qty) as qty,gpb.sizeid,gpb.feature,gpb.type as bv,gpb.boxname as bvname,
                                gpb.growerid,p.id,p.name as productname,s.name as sub_cat_name,
                                g.growers_name,sh.name as sizename,ff.name as features_name,b.name as boxname,GROUP_CONCAT(gpb.stock) as stock,
                                GROUP_CONCAT(bs.name) as bunch_stemp_value,GROUP_CONCAT(bt.name) as boxtype from grower_product_box_packing gpb
                              left join product p on gpb.prodcutid = p.id
                              left join subcategory s on p.subcategoryid=s.id  
                              left join colors c on p.color_id=c.id 
                              left join features ff on gpb.feature=ff.id
                              left join sizes sh on gpb.sizeid=sh.id 
                              left join boxes b on gpb.box_id=b.id
                              left join boxtype bt on b.type=bt.id
                              left join growers g on gpb.growerid=g.id
                              left join bunch_sizes bs on gpb.bunch_size_id=bs.id
                              where g.active     != 'deactive' 
                                and gpb.prodcutid = '".$_REQUEST['id']."' 
                                and gpb.type     != 2  
                                and p.name is not null  
                                and gpb.stock > 0
                                and Date_format(date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y')                                                         
                              group by sh.name";
           
                    $rs_ava_p=mysqli_query($con,$sel_ava_p);

                    $number_ava=mysqli_num_rows($rs_ava_p);

                    $sel_ava_box="select  sum(gpb.stock) as stock
                                    from grower_product_box_packing gpb
                               left join product p on gpb.prodcutid = p.id
                               left join subcategory s on p.subcategoryid=s.id  
                               left join colors c on p.color_id=c.id 
                               left join features ff on gpb.feature=ff.id
                               left join sizes sh on gpb.sizeid=sh.id 
                               left join boxes b on gpb.box_id=b.id
                               left join boxtype bt on b.type=bt.id
                               left join growers g on gpb.growerid=g.id
                               left join bunch_sizes bs on gpb.bunch_size_id=bs.id
                              where g.active     != 'deactive' 
                                and gpb.prodcutid = '".$_REQUEST['id']."' 
                                and gpb.type     != 2  
                                and p.name is not null  
                                and gpb.stock > 0
                                and Date_format(date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y')                                                         
                              group by sh.name";
          
  $dataMaximo = mysqli_query($con, $sel_ava_box);
  $box=0;
        while ($dt = mysqli_fetch_assoc($dataMaximo)) {
           $box=$box+ $dt['stock'];     
    }
                            
                    ?>


                    <!-- FORM -->

            <form class="clearfix form-inline nomargin" method="get" action="shop-cart.html" onsubmit="return login_status();">

                        <input type="hidden" name="product_id" value="1" />
                        <input type="hidden" id="color" name="color" value="yellow" />
                        <input type="hidden" id="qty" name="qty" value="1" />
                        <input type="hidden" id="size" name="size" value="5" />                        

                        <!--button class="btn btn-primary pull-left product-add-cart noradius" type="button" onclick="sendRequestPage()" id="btn_request_v"><i class="fa fa-file-text"></i>REQUEST</button-->                        
                        
                            <input type="file" name="image" id="image" />
                            <br>
                            <input name="Submit" type="Submit" class="buttongrey" value="Add" />                        
                        
            </form>

                    <!-- /FORM -->


                </div>

                <!-- /ITEM DESC -->


            </div>



            <ul id="myTab" class="nav nav-tabs nav-top-border margin-top-80" role="tablist">
                <li role="presentation" class="active"><a href="#v7" role="tab" data-toggle="tab" id="subs">Images</a></li>
                <li role="presentation">               <a href="#v2" role="tab" data-toggle="tab" id="grow">      </a></li>

            </ul>


            <div class="tab-content padding-top-20">

                <!-- SPECIFICATIONS 11-->

                <div role="tabpanel" class="tab-pane fade" id="v2">
                    <div class="container" id="grower-ajax-pagination-container" >                                         
                    <div class="row">               <!-- item -->
                        <div class="col-md-2">      <!-- company logo1 -->
                            <img src="/assets/images/demo/brands/1.jpg" class="img-responsive" alt="company logo">
                        </div>
                        <div class="col-md-10">     <!-- company detail -->
                            <h4 class="margin-bottom-10">Company Name, Inc</h4>
                            <p>Lorem ipsum eos et accusamus et iusto odio dignissimos </p>
                        </div>
                    </div>                          <!-- /item -->                                       

            </div>                   

        </div>


                <!-- REVIEWS -->
               
                
                
<div role="tabpanel" class="tab-pane fade" id="v7">

    <div class="container" id="grower-ajax-pagination-container" >                                         
<div class="owl-carousel featured nomargin owl-padding-10" data-plugin-options='{"singleItem": false, "items": "5", "stopOnHover":false, "autoPlay":4500, "autoHeight": false, "navigation": true, "pagination": false}'>
              <!-- item -->
              <?php 
              $sel_p="select categoryid,subcategoryid,substitutes from product where id='277' ";
              $rs_product = mysqli_query($con,$sel_p);
              $row_product = mysqli_fetch_array($rs_product);
              $trim_str = $row_product['substitutes'];
              $product_id_s =  trim($trim_str,",");
              
              $sql_sub = "SELECT * FROM product WHERE id IN ($product_id_s)";
              $rs_sub = mysqli_query($con,$sql_sub);
              while ($row_substitutes = mysqli_fetch_array($rs_sub)) {
                ?>
                <div class="shop-item nomargin">
                  <div class="thumbnail">
                    <!-- product image(s) -->
                    <a class="shop-item-image" href="<?php echo SITE_URL."variety-sub-page.php?id=".$row_substitutes['id'];?>">
                      <img class="img-responsive" src="<?php echo SITE_URL.$row_substitutes['image_path'];?>" alt="<?php echo $row_substitutes['name'];?>" />
                    </a>
                    <!-- /product image(s) -->
                  </div>
                  <div class="shop-item-summary text-center">
                    <h2><?php echo $row_substitutes['name'];?></h2>
                  </div>
                  <!-- buttons -->
                  <!--div class="shop-item-buttons text-center">
                    <a class="btn btn-default" href="<?php echo SITE_URL."/en/variety-sub-page.php?id=".$row_substitutes['id'];?>"><i class="fa fa-file-text"></i> Request</a>
                  </div-->
                  <!-- /buttons -->
                </div>
              <?php }
              ?>
              
              <!-- /item -->
            </div>                                          

                    </div>                                     
                </div>                
                
                
            </div>
            
            

        </div>

    </section>
    

    <?php 

    require_once '../includes/footer.php'; 

    ?>

<!-- PAGE LEVEL SCRIPTS -->

<script type="text/javascript" src="/includes/assets/js/view/demo.shop.js"></script>
<script type="text/javascript" src="/assets/js/Chart.js"></script>
<script src="/includes/assets/js/highcharts.js"></script>
<script src="/includes/assets/js/exporting.js"></script>
<?php

$pageSql_h="SELECT grower_id,verities_id,market_type,price,country,date,entry_date FROM historic_price WHERE verities_id='".$_REQUEST['id']."' AND date < Now() and date > DATE_ADD(Now(), INTERVAL- 6 MONTH)  order by date";
$pageQuery_h = mysqli_query($con, $pageSql_h);

$month_Array="";
$str_final="";
$i=0;
$rawdata = array();

while ($row_Data_h = mysqli_fetch_array($pageQuery_h)) {

    $month_Array=date("m",strtotime($row_Data_h['date']));
    $day_Array=date("d",strtotime($row_Data_h['date']));
    $year_Array=date("Y",strtotime($row_Data_h['date']));
    $ts = mktime(0, 0, 0, $month_Array,$day_Array, $year_Array);
    $str_final.=$ts.",".$row_Data_h['price']."=====";
    $price_array.=$row_Data_h['price'].",";

     $rawdata[$i] = $row_Data_h;
    $i++;

}
for($i=0;$i<count($rawdata);$i++){
    $time = $rawdata[$i]["date"];
    $date = new DateTime($time);
    $rawdata[$i]["time"]=$date->getTimestamp()*1000;
}

$f_month_Array=trim($month_Array,","); 
$f_month_Array=trim($str_final,"====="); 
echo $f_month_Array;
?>

<script type="text/javascript">

var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
var fmonth_Array='<?php echo $f_month_Array;?>';
var split_array=fmonth_Array.split("=====");
var ftotal_price='<?php echo $price_array;?>';
var split_price=ftotal_price.split(",");
var month=[];

for(i=0;i<split_array.length;i++){
    var str=split_array[i].split(",");
    var final_string=str[0]+","+parseInt(str[1]);
    month.push([final_string]);
}

var price_val=[];

for(j=0;j<split_price.length;j++){
    price_val.push(split_price[j]);    
}

var lineChartData = {

    labels : month,
    datasets : [
        {

            label: "Stading Market",
            fillColor : "rgba(220,220,220,0.2)",
            strokeColor : "rgba(220,220,220,1)",
            pointColor : "rgba(220,220,220,1)",
            pointStrokeColor : "#fff",
            pointHighlightFill : "#fff",
            pointHighlightStroke : "rgba(220,220,220,1)",
            data : price_val
        }
    ]

};

$(function(){
    Highcharts.setOptions({
         global: { 
        useUTC: false
    }
    });
    jQuery('#container').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Price'
        },
        
        xAxis: {
                type: 'datetime',
                tickPixelInterval: 150,
                dateTimeLabelFormats: {
                    month: '%Y',
                    year: '%Y'
                }

        },
        yAxis: {
            title: {
                text: 'Price Value'
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%e. %b}: {point.y:.2f}'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

        series: [{
            name: 'Price',
                data: (function() {
                   var data = [];
                    <?php
                        for($i = 0 ;$i<count($rawdata);$i++){
                    ?>
                    data.push([<?php echo $rawdata[$i]["time"];?>,<?php echo $rawdata[$i]["price"];?>]);
                    <?php } ?>
                return data;
                })()
            
        }]
    });

    jQuery(".live_price").css('visibility','hidden');

    jQuery("#open_pop").click(function(){
        jQuery(".live_price").css('visibility','visible').show("slow");

    });
    jQuery(".close").click(function(){
        jQuery(".live_price").css('visibility','hidden').show("slow");

    });


    var pid = "<?php echo $_GET['id']; ?>";
    $.ajax({
            url:"/ajax/pagination.php",
            type:"POST",
            data:"action=varietySubPageGrowersPagination&page=1&pid="+pid,
            cache: false,
            success: function(response){

                $('#grower-ajax-pagination-container').html(response);
            }
    });  

    $('#grower-ajax-pagination-container').on('click','.page-numbers',function(){

        $page = $(this).attr('href');
        $pageind = $page.indexOf('page=');
        $page = $page.substring(($pageind+5));

        $.ajax({

            url:"/ajax/pagination.php",
            type:"POST",
            data:"action=varietySubPageGrowersPagination&page="+$page+"&pid="+pid,
            cache: false,

            success: function(response){           

                $('#grower-ajax-pagination-container').html(response);       

            }       

        });

    return false;

    });
    
});  

$("#graph_year").change(function(){

    var selected_year=$(this).val();
    var verities_id="<?php echo $_REQUEST['id']?>";

     $.ajax({

        url:"ajax/graph_data.php",
        type:"POST",
        data:"year="+selected_year+"&verities_id="+verities_id,
        cache: false,

        success: function(response){ 

            var res_val=response.split("#####");                
            var fmonth_Array=res_val[0];
            var split_array=fmonth_Array.split(",");
            var ftotal_price=res_val[1];
            var split_price=ftotal_price.split(",");
            var month=[];

            for(i=0;i<split_array.length;i++)     {

                month.push(split_array[i]);
            }

            var price_val=[];

            for(j=0;j<split_price.length;j++)   {
                price_val.push(split_price[j]);    
            }

            var lineChartDataAjax = {

                labels : month,
                datasets : [

                    {
                        label: "Stading Market",
                        fillColor : "rgba(220,220,220,0.2)",
                        strokeColor : "rgba(220,220,220,1)",
                        pointColor : "rgba(220,220,220,1)",
                        pointStrokeColor : "#fff",
                        pointHighlightFill : "#fff",
                        pointHighlightStroke : "rgba(220,220,220,1)",
                        data : price_val
                    }
                ]
            }

            var ctx_Ajax = document.getElementById("lineChartCanvas").getContext("2d");
            window.myLine = new Chart(ctx_Ajax).Line(lineChartDataAjax, {
                responsive: true

            });

        }       

    });

});
var myLineChart=null;
function getPriceDetails(size_id,product_id_k)
{
  $("#product-size-dd").toggle();
  $("#average_price").html("");
  var ctx_Ajax1 = document.getElementById("lineChartCanvas").getContext("2d");
  if(myLineChart!=null){
        myLineChart.destroy();
    }
   $.ajax({
        url:"getPriceDetailsVariety.php",
        async: false,
        type:"POST",
        data:"action_type=getproductdetails&size_id="+size_id+"&product_id_k="+product_id_k,
        success: function(response){ 
            var res_val=response.split("#####");             
            var fmonth_Array=res_val[0];
            var split_array=fmonth_Array.split(",");
            var ftotal_price=res_val[1];
            var split_price=ftotal_price.split(",");

            if(res_val[2] != "")  {
              var res_price_ave = parseFloat(res_val[2]).toFixed(2);
              $("#average_price").html("($"+res_price_ave+")");
            } 
            var month=[];
            for(i=0;i<split_array.length;i++)      {
                month.push(split_array[i]);
            }

            var price_val=[];
            for(j=0;j<split_price.length;j++)
            {

                price_val.push(split_price[j]);    

            }
            var lineChartDataAjax1 = {
                labels : month,
                datasets : [
                    {
                        label: "Stading Market",
                        fillColor : "rgba(220,220,220,0.2)",
                        strokeColor : "rgba(220,220,220,1)",
                        pointColor : "rgba(220,220,220,1)",
                        pointStrokeColor : "#fff",
                        pointHighlightFill : "#fff",
                        pointHighlightStroke : "rgba(220,220,220,1)",
                        data : price_val
                    }
                ]
            }

            myLineChart = new Chart(ctx_Ajax1).Line(lineChartDataAjax1, {
          responsive: true,
          bezierCurveTension : 0.3
      });

      window.myLine = myLineChart;    
            $("#product-size-dd").hide();
        }
    });    
}
$("#product-type-dd li a").click(function(){
  $("#product-box-type").html($(this).attr("data-val"));
  $("#hdn_box_type").val($(this).attr("pass_box_type"));
  $("#product-type-dd").toggle();
});
$("#product-size-dd li a").click(function(){
  $("#hdn_product_size").val($(this).attr("data-val"));
});

$("#product-qty-dd li a").click(function(){
  $("#hdn_product_qty").val($(this).attr("data-val"));
  $("#product-qty-dd").toggle();
});

$("#product-size-dd-box").click(function(){
  $("#product-size-dd").toggle();
});
$("#product-type-dd-box").click(function(){
    $("#product-type-dd").toggle();
});
$("#product-qty-dd-box").click(function(){
  $("#product-qty-dd").toggle();
});



function sendRequestPage(){
  var pass_box_type = $("#hdn_box_type").val();
  var product_size = $("#hdn_product_size").val();
  var product_qty = $("#hdn_product_qty").val();

  if(pass_box_type != "" && product_size !="" && product_qty != ""){
    var product_id = "<?php echo $_REQUEST['id'];?>";
    window.location.href = "<?php echo SITE_URL?>"+"buyer/name-your-price.php?pass_box_type="+pass_box_type+"&product_size="+product_size+"&product_qty="+product_qty+"&product_id="+product_id;
  }else{
    alert("Please select all option.");
  }
} 

</script>
<script>
  function login_status(){
    var is_login = true;
    <?php 
      if($_SESSION['login'] == ''){
    ?>
        is_login = false;
    <?php
      }
    ?>

  }
</script>
<script type="text/javascript">
  $(document).ready(function(){
    //$("#grow").trigger('click');
    $("#grow").get(0).click();
    $("#subs").get(0).click();
  })
</script>
