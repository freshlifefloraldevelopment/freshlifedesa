<?php
/**
#Count temporal register to market place cart
Developer educristo@gmail.com
Start 17 Feb 2021
Structure MarketPlace previous to buy
Add Protection SQL INY, XSS
**/
require("/home/portega/vendor/autoload.php");
require_once("../config/config_gcp.php");

if($_SESSION["buyer"]){
  $userSessionID = $_SESSION["buyer"];
}else{
  header("location: variety-page.php");
}

$token = $_POST['token'];
$key = $_POST['key'];
$delNow = date('l jS \of F Y h:i:s A');
$description = "Payment made for Fresh Life Floral by buyerId ".$userSessionID;
$deliveryDate = $_POST['delivery'];

      $loadDataItemsPrevious= "select sct.id_buyer_temp_item as keyItem, sct.id_product , p.name as prodname, p.image_path as prodimage, (round((sct.price*100),0)) as prodprice, sct.quantity as prodquantity, sct.size_steam as sizesteam, sct.steams as steams
      from buyer_requests_shoping_cart_temp_items sct
      inner join product  p   ON sct.id_product=p.id
      where sct.id_buyer_temp = '$key'
      order by sct.id_buyer_temp_item";

      $result_loadDataItemsPrevious = mysqli_query($con,$loadDataItemsPrevious);
      while ($row_result = mysqli_fetch_array($result_loadDataItemsPrevious))
      {
      $subTotalItemsT += $row_result['prodprice'] * $row_result['steams'] * $row_result['prodquantity'];
      }

      $Transport = "0";
      $valueTransport = number_format((float)$Transport,2);
      $amount = $subTotalItemsT + $valueTransport;
      $amountShow = number_format($amount/100,2);

      // Datos de la Base de Datos

\Stripe\Stripe::setApiKey("sk_live_dASBR7Mv2y6pT9GVn4SlOzKy");

      $charge =  \Stripe\PaymentIntent::create([
          'amount' => $amount,
          'currency' => 'usd',
          'payment_method' => $token,
          'capture_method' => 'manual',
          'confirm' => true,
          ]);

     $chargeID = $charge->status;
     $chargeURL = $charge->receipt_url;
     $payment_method = $charge->payment_method;

$htmlPaySuccess = "<header class='d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-body border-bottom shadow-sm'>
  <p class='h5 my-0 me-md-auto fw-normal'>Fresh Life Floral</p>
  <nav class='my-2 my-md-0 me-md-3'>
  </nav>
<img class='mb-2' src='https://app.freshlifefloral.com/user/logo.png' alt='Fresh Life Floral' width='94' height='30'>
</header>

<main class='container'>
  <div class='pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center'>
    <h1 class='display-4' style='color:green;'>Successfull</h1>
    <p class='lead'><strong>Thank you! </strong><br>Order completed Successfully!</div>

  <div class='row row-cols-1 row-cols-md-1 mb-1 text-center'>


    <div class='col'>
      <div class='card mb-4 shadow-sm'>
      <div class='w-100 btn btn-lg btn-primary'>
        <h4 class='my-0 fw-normal' style='color:#fff;'>Order completed Successfully</h4>
      </div>
        <p class='lead'><br>Your order was successfully processed using the next details. <br>Please, check your email for your receipt.
      <div class='card-body'>
        <h1 class='card-title pricing-card-title'>$amountShow<small class='text-muted'> USD</small></h1>
        <ul class='list-unstyled mt-3 mb-4'>
          <li><strong>Payment Method: </strong>$payment_method</li>
          <li><strong>Description:</strong>$description</li>
          <li><strong>Date:</strong>$delNow</li>
          <li><strong>Amount:</strong>$amountShow</li>
          <li>________________________________</li>
          <li style='color:#aaa;'>Contact us, if you have any questions.</li>

        </ul>
        <a href='variety-page.php' type='button' class='w-100 btn btn-lg btn-success' style='background:#8a2b83!important;'>Return to Fresh Life Floral</a>
      </div>
    </div>
    </div>
  </div>

  <footer class='pt-4 my-md-12 pt-md-12 border-top'>
    <div class='row'>
      <div class='col-12'>
        <img class='mb-2' src='https://app.freshlifefloral.com/user/logo.png' alt='Fresh Life Floral' width='64' height='19'>
        <small class='d-block mb-3 text-muted'>© 2021</small>
      </div>
    </div>
  </footer>
</main>";

$htmlPayFail = "<header class='d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-body border-bottom shadow-sm'>
  <p class='h5 my-0 me-md-auto fw-normal'>Fresh Life Floral</p>
  <nav class='my-2 my-md-0 me-md-3'>
  </nav>
<img class='mb-2' src='https://app.freshlifefloral.com/user/logo.png' alt='Fresh Life Floral' width='94' height='30'>
</header>

<main class='container'>
  <div class='pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center'>
    <h1 class='display-4' style='color:red;'>Failed</h1>
    <p class='lead'><strong>Really Sorry! </strong><br>Order was not completed Successfully!</div>

  <div class='row row-cols-1 row-cols-md-1 mb-1 text-center'>


    <div class='col'>
      <div class='card mb-4 shadow-sm'>
      <div class='w-100 btn btn-lg btn-danger'>
        <h4 class='my-0 fw-normal' style='color:#fff;'>Please, try again!</h4>
      </div>
        <p class='lead'><br>Your order was not successfully processed.<br>Please, try again.
      <div class='card-body'>
        <h1 class='card-title pricing-card-title'>0.00<small class='text-muted'> USD</small></h1>
        <ul class='list-unstyled mt-3 mb-4'>

          <li style='color:#aaa;'> $chargeID Contact us, if you have any questions.</li>

        </ul>
        <a href='variety-page.php' type='button' class='w-100 btn btn-lg btn-success' style='background:#8a2b83!important;'>Return to Fresh Life Floral</a>
      </div>
    </div>
    </div>
  </div>

  <footer class='pt-4 my-md-12 pt-md-12 border-top'>
    <div class='row'>
      <div class='col-12'>
        <img class='mb-2' src='https://app.freshlifefloral.com/user/logo.png' alt='Fresh Life Floral' width='64' height='19'>
        <small class='d-block mb-3 text-muted'>© 2021</small>
      </div>
    </div>
  </footer>
</main>";

if($chargeID=="requires_capture")
{
   //createRequest($userSessionID,$deliveryDate,$con,$key);
 if(createRequest($userSessionID,$deliveryDate,$con,$key)){
      echo "1".$htmlPaySuccess;
  }
}else{
  echo "2".$htmlPayFail;
}

function createRequest($userSessionID,$deliveryDate,$con,$key){

$idbuy = $userSessionID;
$order_serial = 1;
$delNow = date('Y-m-d');
$contador = 0;
$verificador = 0;

           // Desde el calendario implementar la misma rutina
            // llamar al mismo PHP de buyer en create order

                        $delDate= $deliveryDate;
                        $delAdd = $deliveryDate;


// ORDENES FIJAS  BUYERS REQUESTS//////////////////////////////////////////////
    $cabCount = 0;

                $qryMaxOrd ="select (max(id)+1) as ido from buyer_orders";
                $dataMaxOrd = mysqli_query($con, $qryMaxOrd);

                while ($dto = mysqli_fetch_assoc($dataMaxOrd)) {
                $IdMaxOrd= $dto['ido'];
		}

//---------------- Proceso Shipping ----------------------------

$getBuyerShippingMethod = "select shipping_code , shipping_method_id
                             from buyer_shipping_methods
                            where buyer_id = '".$idbuy."'  ";

$buyerShippingMethodRes = mysqli_query($con, $getBuyerShippingMethod);
$buyerShippingMethod = mysqli_fetch_assoc($buyerShippingMethodRes);

    //////////////////////////////////////////////////////////////
    $getShippingMethod = "select connect_group from shipping_method where id='".$buyerShippingMethod['shipping_method_id']."' ";
    $getShippingMethodRes = mysqli_query($con, $getShippingMethod);
    $shippingMethodDetail = mysqli_fetch_assoc($getShippingMethodRes);

    $connections = explode(",",$shippingMethodDetail['connect_group']);
    $id_conn = $connections[1];  // Pantalla para Escojer conexion por el momento va la de default
    $var= date("W");
    ///////////////////////////////////////

    	 $sel_info="select  id          , id_conn     , connections , name        ,
                           description , type        , days        , trasit_time
                     from more_days_connection
                    where id_conn='" . $id_conn . "' LIMIT 0,1";

	$rs_info=mysqli_query($con,$sel_info);
  $info=mysqli_fetch_array($rs_info);
   $trasit_time = $info['trasit_time'];

	//$info=mysqli_fetch_array($rs_info);

     ///////////////////////////////////////////////////////////////////////////

                // LFD
                 $sqlids = "SELECT date_add('".$delDate."', INTERVAL -".$trasit_time." DAY) fecha";
                $row_sqlids = mysqli_query($con, $sqlids);

                while ($fila =mysqli_fetch_assoc($row_sqlids)) {
                         $date_lfd= $fila["fecha"];
                }

                 $fecha_tmp = $date_lfd;
                $dayofweek = date('D', strtotime($date_lfd));

                if ($dayofweek == "Sun")  {
                        $date_lfd = strtotime ('-2 day',strtotime ($fecha_tmp) );
                        $date_lfd = date ( 'Y-m-j' , $date_lfd );
                }else{
                         $date_lfd=$fecha_tmp;
                }
    ////////////////////////////////////////////////////////////////////////////
                $dayOfMonth = date('d', strtotime($date_lfd));
                $monthOfYear = date('m', strtotime($date_lfd));
                $year = date('y', strtotime($date_lfd));

                $order_number = $buyerShippingMethod['shipping_code'] . $dayOfMonth . $monthOfYear . $year;

                $qrys = "select id from buyer_orders where order_number='" . $order_number . "' ORDER BY order_serial DESC";
                $datas = mysqli_query($con, $qrys);
                $numrows = mysqli_num_rows($datas);

                if ($numrows > 0) {
                    while ($dts = mysqli_fetch_assoc($data)) {
                        $order_serial = $dts['order_serial'] + $order_serial;
                    }
                }

                 $set_order = "insert into buyer_orders set
                            id              = '".$IdMaxOrd."',
                            buyer_id        = '".$idbuy."',
                            order_number    = '".$order_number."',
                            order_date      = '".$delDate."',
                            shipping_method = '".$buyerShippingMethod['shipping_method_id']."',
                            del_date        = '".$delDate."',
                            date_range      = '".$delAdd."',
                            is_pending      = '0',
                            order_serial    = '".$order_serial."',
                            seen            = '1',
                            delivery_dates  = '".$delDate."',
                            lfd_grower      = '99',
                            qucik_desc      = 'MARKET PLACE'  ";

               mysqli_query($con,$set_order);


        ////////////////////////////////////////////////////////////////////////////////////


       $query_request = "SELECT * FROM buyer_requests_shoping_cart_temp_items where id_buyer_temp = '".$key."'  ";
       $buy_request = mysqli_query($con, $query_request);

        while($request_cab = mysqli_fetch_array($buy_request))  {

                 $id_product_d = $request_cab['id_product'];
                 $sizeid_d = $request_cab['size_id'];
                 $feature_d = $request_cab['feature_id'];
                 $steams_d = $request_cab['steams'];
                 $quantity_d = $request_cab['quantity'];

                 $best_id = $request_cab['id_best_op'];
                 $grower_id = $request_cab['id_grower'];

                $qryMaxReq     = "select (max(id)+1) as idr from buyer_requests";
                $dataMaximoReq = mysqli_query($con, $qryMaxReq);

                        while ($dtr = mysqli_fetch_assoc($dataMaximoReq)) {
                        $IdMaxReq= $dtr['idr'];
		                    }

            $contador = $contador + 1;

            $set_req = "insert into buyer_requests set
                        id              = '".$IdMaxReq."',
                        id_order        = '".$IdMaxOrd."',
                        order_serial    = '".$contador."',
                        cod_order       = '".$order_number."-1-".$contador."',
                        product         = '".$id_product_d."',
                        sizeid          = '".$sizeid_d."',
                        feature         = '".$feature_d."',
                        noofstems       = '".$steams_d."',
                        qty             = '".$steams_d."',
                        box_qty         = '".$quantity_d."',
                        buyer           = '".$idbuy."',
                        boxtype         = '39',
                        date_added      = '".$delNow."',
                        type            = '1',
                        bunches         = '0',
                        box_name        = null,
                        lfd             = '".$date_lfd."',
                        lfd2            = '".$date_lfd."',
                        comment         = 'MARKET PLACE',
                        box_id          = '0',
                        shpping_method  = '".$buyerShippingMethod['shipping_method_id']."',
                        isy             = '0',
                        mreject         = '0',
                        bunch_size      = '0',
                        unseen          = '1',
                        req_qty         = '0',
                        bunches2        = null,
                        discount        = '0',
                        inventary       = '0',
                        type_price      = '0',
                        id_client       = '0'  ";

                  mysqli_query($con,$set_req);

     //----------------------------------------------------------------------------------------------------
     // DETALLE DE FINCAS
     //------------------

     if($best_id==1){

       $query_grow_req = "select gs.grower_id as growerid,
                      bs.id as bunchsizeid    ,
                      bs.name as bname        ,
                      gs.bunch_value
                 from growcard_prod_bunch_sizes gs
                 left join grower_product_box gpb on gs.product_id=gpb.product_id and gs.grower_id=gpb.grower_id
                 left join bunch_sizes bs         on gs.bunch_sizes=bs.id
                 left join (select id,profile_image,file_path5,active FROM growers) gr on  gs.grower_id=gr.id
                where gs.product_id ='" . $id_product_d . "'
                  and gs.sizes      ='" . $sizeid_d . "'
                  and gr.active     ='active'
                group by gs.grower_id";

       $req_grow_det = mysqli_query($con, $query_grow_req);
       $verificador = 1;
     }
     else{

       $query_grow_req = "select gs.grower_id as growerid,
                      bs.id as bunchsizeid    ,
                      bs.name as bname        ,
                      gs.bunch_value
                 from growcard_prod_bunch_sizes gs
                 left join grower_product_box gpb on gs.product_id=gpb.product_id and gs.grower_id=gpb.grower_id
                 left join bunch_sizes bs on gs.bunch_sizes=bs.id
                 left join (select id,profile_image,file_path5,active FROM growers) gr on  gs.grower_id=gr.id
                where gs.product_id ='" . $id_product_d . "'
                  and gs.sizes      ='" . $sizeid_d . "'
                  and gs.grower_id  ='" . $grower_id . "'
                  and gr.active     ='active'
                group by gs.grower_id";

       $req_grow_det = mysqli_query($con, $query_grow_req);
       $verificador = 1;
     }


      while($request_grower = mysqli_fetch_array($req_grow_det))  {
                  $sel_prod = "select id from product where id = '" . $id_product_d . "' ";
                  $rs_prod = mysqli_query($con,$sel_prod);
                  $idprod = mysqli_fetch_array($rs_prod);

            $set_det = "insert into request_growers set
                    tax        = '0',
                    gprice     = '0',
                    shipping   = '0',
                    handling   = '0',
                    gid        = '".$request_grower['growerid']."',
                    bid        = '".$idbuy."',
                    rid        = '".$IdMaxReq."',
                    mailsend   = '9',
                    order_view = '9' " ;

                    mysqli_query($con,$set_det);
                      $verificador = 1;
        }
    }
return $verificador;
  }
?>
