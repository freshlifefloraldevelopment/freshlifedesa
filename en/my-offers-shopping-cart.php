<?php
require_once("../config/config_gcp.php");
// PO 2018-07-02

$menuoff = 1;
$page_id = 5;
$message = 0;

if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
}
$userSessionID = $_SESSION["buyer"];
/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,country,state_text,city,zip,coordination,address,is_public,biographical_info,profile_image FROM buyers WHERE id =?")) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $country, $state_text, $city, $zip, $coordination, $address, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    header("location:" . SITE_URL);
    die;
}

//$img_url = '../imagenes/profile_images/noavatar.jpg';

if ($profile_image) {
    $img_url = '../imagenes/profile_images/' . $profile_image;
}

$sel_info = "select * from buyers where id='" . $userSessionID . "'";
$rs_info = mysqli_query($con, $sel_info);
$info = mysqli_fetch_array($rs_info);

if ($info["active"] != "demo" && $info["active"] != "suspended") {
    
    if (isset($_GET["offerid"]) && isset($_GET["replyid"])) {
        
        $update = "update grower_offer_reply set status=1,accept_date='" . date("Y-m-d") . "' where id='" . $_GET["replyid"] . "' and offer_id='" . $_GET["offerid"] . "'";
        mysqli_query($con, $update);

        $sel_grower = "select grower_id from grower_offer_reply where id='" . $_GET["replyid"] . "' and offer_id='" . $_GET["offerid"] . "'";
        $rs_grower = mysqli_query($con, $sel_grower);
        $grower = mysqli_fetch_array($rs_grower);

        $sel_mail_email = "select contact1email,growers_name from growers where id='" . $grower["grower_id"] . "'";
        $rs_mail_email = mysqli_query($con, $sel_mail_email);
        $mail_email = mysqli_fetch_array($rs_mail_email);
        $email_address = $mail_email["contact1email"];

        $er = "select gpb.id as cartid,gpb.type,gpb.lfd,gpb.noofstems,gpb.bunches,gpb.qty,gpb.boxtype,p.id,p.name,s.name as subs,sh.name as sizename,
                      ff.name as featurename 
                 from buyer_requests gpb
                 left join product p     on gpb.product     = p.id
		 left join subcategory s on p.subcategoryid = s.id  
		 left join features ff   on gpb.feature     = ff.id
		 left join sizes sh      on gpb.sizeid      = sh.id 
                where gpb.id='" . $_GET["offerid"] . "' ";
        
        $ers = mysqli_query($con, $er);

        while ($er5 = mysqli_fetch_array($ers)) {

            $text .= "You proposal for following offer has been accepted. <br/> <br/>";
            if ($er5["type"] != "3") {

                if ($er5["type"] == 2) {
                    $text .= "Name Your Price - &nbsp;&nbsp;";
                }

                if ($er5["type"] == 0) {
                    $text .= "Request for Product Quote - &nbsp;&nbsp;";
                }


                $text .= "For ";
                $temp_lfd = explode("-", $er5["lfd"]);
                $text .= $temp_lfd[1] . "-" . $temp_lfd[2] . "-" . $temp_lfd[0];
                $text .= " - " . $er5["qty"] . " ";
                $temp = explode("-", $er5["boxtype"]);
                $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                $rs_box_type = mysqli_query($con, $sel_box_type);
                $box_type = mysqli_fetch_array($rs_box_type);
                $text .= $box_type["name"] . " Box(es)";
                $text .= "<br/>";

                $text .= $er5["subs"] . " - " . $er5["name"] . " - ";
                if ($er5["featurename"] != "") {
                    $text .= $er5["featurename"] . " - ";
                }

                $text .= $er5["sizename"] . " cm ";

                $text .= "For ";
                $temp_lfd = explode("-", $er5["lfd"]);
                $text .= $temp_lfd[1] . "-" . $temp_lfd[2] . "-" . $temp_lfd[0];

                if ($er5["noofstems"] > 0) {
                    $text .= " - " . $er5["noofstems"] . " Stems";
                } else {
                    $text .= " - " . $er5["qty"] . " ";

                    $temp = explode("-", $er5["boxtype"]);
                    $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                    $rs_box_type = mysqli_query($con, $sel_box_type);
                    $box_type = mysqli_fetch_array($rs_box_type);
                    $text .= $box_type["name"] . " Box(es)";
                }
            } else {
                $text .= "By the Bunch - &nbsp;&nbsp;";

                $text .= "For ";
                $temp_lfd = explode("-", $er5["lfd"]);
                $text .= $temp_lfd[1] . "-" . $temp_lfd[2] . "-" . $temp_lfd[0];

                $text .= " - " . $er5["qty"] . " ";

                $temp = explode("-", $er5["boxtype"]);
                $sel_box_type = "select * from boxtype where id='" . $temp[0] . "'";
                $rs_box_type = mysqli_query($con, $sel_box_type);
                $box_type = mysqli_fetch_array($rs_box_type);
                $text .= $box_type["name"] . " Box(es)";

                $text .= "<br/>";
                $text .= "Selected Bunch(es)<br/><br/>";


                $templ = explode(",", $er5["bunches"]);
                $count = sizeof($templ);
                
                for ($io = 0; $io <= $count - 2; $io++) {
                    $r = explode(":", $templ[$io]);
                    $query2 = "select gpb.growerid,gpb.prodcutid,gpb.sizeid,gpb.feature ,p.name as productname,s.name as subs,sh.name as sizename,ff.name as featurename,bs.name as bname 
                                 from grower_product_box_packing gpb
				 left join product p on gpb.prodcutid = p.id
				 left join subcategory s on p.subcategoryid=s.id  
				 left join features ff on gpb.feature=ff.id
				 left join sizes sh on gpb.sizeid=sh.id 
				 left join bunch_sizes bs on gpb.bunch_size_id=bs.id
				where gpb.id='" . $r[0] . "'";
                    
                    $rs2 = mysqli_query($con, $query2);
                    while ($product_box = mysqli_fetch_array($rs2)) {

                        $text .= '&nbsp;' . $product_box["subs"] . '&nbsp;&nbsp;  
				  ' . $product_box["productname"] . ' ' . $product_box["featurename"] . ' &nbsp;&nbsp; - ' . $product_box["sizename"] . ' cm &nbsp;&nbsp; - ' . $product_box["bname"] . ' Stems &nbsp;&nbsp;-' . $r[1] . ' Bunch(es) &nbsp;
						  <br/>';
                    }
                }

                $text .= '<br/>';

                $text .= "<br/><br/>";
            }

            $text .= "<br/>";
        }


        header("location:" . SITE_URL . "my-offers.php");
    }
}

$today = date("m-d-Y H:i:s", strtotime("+2 hours"));
$today1 = explode(" ", $today);
$today_date = $today1["0"];
$time = $today1["1"];
$hours_array = explode(":", $time);

if ($hours_array[0] <= 12) {
    if ($info["live_days"] == 0) {
        $shpping_on = date("Y-m-d");
        $shpping_onr = $shpping_on;
        $tempk = explode("-", $shpping_on);
        $shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
    } else {
        $shpping_on = date('Y-m-j', strtotime($info["live_days"] . ' weekdays'));
        $shpping_onr = $shpping_on;
        $tempk = explode("-", $shpping_on);
        $shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
    }
} else {
    if ($info["live_days"] == 0) {
        $shpping_on = date('Y-m-j', strtotime('1 weekdays'));
        $shpping_onr = $shpping_on;
        $tempk = explode("-", $shpping_on);
        $shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
    } else {
        $addone = $info["live_days"] + 1;
        $shpping_on = date('Y-m-j', strtotime($addon . ' weekdays'));
        $shpping_onr = $shpping_on;
        $tempk = explode("-", $shpping_on);
        $shpping_on = $tempk[1] . "-" . $tempk[2] . "-" . $tempk[0];
    }
}



$page_request = "my_offers";


// Paginacion
$sel_pagina = "select gpb.id as cartid , gpb.date_added , p.id , p.name , p.color_id , p.image_path , p.box_type , 
                     s.name as subs , gpb.qty , gpb.boxtype,
                     sh.name as sizename,
                     ff.name as featurename,
                     gpb.type , if(gpb.type=1,'OPEN','STAND') as type_request,
                     gpb.id_client , pa.price_card 
		from buyer_requests_shoping_cart gpb
		left join product p     on gpb.product     = p.id
		left join subcategory s on p.subcategoryid = s.id  
		left join features ff   on gpb.feature     = ff.id
		left join sizes sh      on gpb.sizeid      = sh.id 
                left join sub_client c  on gpb.id_client   = c.id 
	       where gpb.buyer='" . $userSessionID . "' 
                 and gpb.isy = '0'  ";
                   


$rs_pagina = mysqli_query($con, $sel_pagina);
$total_pagina = mysqli_num_rows($rs_pagina);
$num_record = $total_pagina;
$display = 50;

$XX = '<div class="notfound">No Item Found !</div>';

function box_type($box)
{
    $box_type_s = "";
    if ($box == "0") {
        $box_type_s = "Stems";
    } else if ($box == "1") {
        $box_type_s = "Bunch";
    }
    return $box_type_s;

}

function box_type_name($id_box_type){
    global $con;
   // $sel_box_type = "select * from boxtype where id='" . $id_box_type . "'";
    
    $sel_box_type = "select code as name from units where id='" . $id_box_type . "'";
    $rs_box_type = mysqli_query($con, $sel_box_type);
    $box_type = mysqli_fetch_array($rs_box_type);
    return $box_type["name"];
}

function box_descrip($id_box_type){
    global $con;    
    $sel_unit_type = "select descrip from units where id='" . $id_box_type . "'";
    $rs_unit_type = mysqli_query($con, $sel_unit_type);
    $box_descrip_type = mysqli_fetch_array($rs_unit_type);
    return $box_descrip_type["descrip"];
}

function box_name($box_type)
{
    //$box_type["name"]
    $res = "";
    if ($box_type == "HB") {
        $res = "half boxes";
    } else if ($box_type == "EB") {
        $res = "eight boxes";
    } else if ($box_type == "QB") {
        $res = "quarter boxes";
    } else if ($box_type == "JB") {
        $res = "jumbo boxes";
    }
    return $res;

}

function customer()
{
    global $con;
    $customer_sql = "SELECT id,name from sub_client order by  name";
    $result_customer = mysqli_query($con, $customer_sql);
    return $result_customer;
}

function category()
{
    global $con;
    
    $category_sql = "SELECT id,name from category order by  name";
    $result_category = mysqli_query($con, $category_sql);
    return $result_category;
}

//function query_main($userSessionID)

function query_main($user, $init, $display){ 
   
   // verificar el join con los features
    
    $query = "select gpb.id as cartid , gpb.date_added , p.id , p.name , p.color_id , p.image_path , p.box_type , 
                     s.name as subs , gpb.qty , gpb.boxtype,
                     sh.name as sizename,
                     ff.name as featurename,
                     gpb.type , if(gpb.type=1,'OPEN','STAND') as type_request,
                     gpb.id_client , pa.price_card 
                from buyer_requests_shoping_cart gpb
               inner join product p     on gpb.product     = p.id
               inner join subcategory s on p.subcategoryid = s.id
                left join grower_parameter pa on p.subcategoryid=pa.idsc and gpb.sizeid=pa.size
                left join features ff   on gpb.feature     = ff.id
                 left join sizes sh      on gpb.sizeid      = sh.id
	       where gpb.buyer='" . $user . "' 
                 and gpb.isy = '0'
               order by gpb.id desc LIMIT " . $init . ",$display";
    
    return $query;
}


if (isset($_POST["startrow"]) && $_POST["startrow"] != "") {
    $sr = $_POST["startrow"] + 1;
    $query2 = query_main($userSessionID, $_POST["startrow"], $display);
    $result2 = mysqli_query($con, $query2);
} else {
    if (empty($startrow)) {
        $startrow = 0;
        $sr = 1;
    }
    $query2 = query_main($userSessionID, 0, $display);
    $result2 = mysqli_query($con, $query2);
}

?>
<?php include("../includes/profile-header.php"); ?>
<link href="<?php echo SITE_URL; ?>includes/assets/css/essentials_new.css" rel="stylesheet" type="text/css"/>
<?php include("../includes/left_sidebar_buyer.php"); ?>

<style type="text/css">
    .not_set_border td {
        border: none !important;
    }
</style>

<section id="middle">
    <!-- page title -->
    <header id="page-header">
        <h1>Shopping Cart</h1>
        <ol class="breadcrumb">
            <li><a href="#">Products</a></li>
            <li class="active">List</li>
        </ol>
    </header>
    <!-- /page title -->
    <div id="content" class="padding-20">
        
        <div id="panel-2" class="panel panel-default">
            
            <div class="panel-heading">
                
                <span class="title elipsis">
                    <strong>----</strong> <!-- panel title -->
                </span>

                <!-- right options -->
               <?php $dates = date("jS F Y");?>
                
                                    <ul class="options pull-right relative list-unstyled">
                                        <div class="tags_selected" id="tab_for_filter" style="float: left; margin-top: 4px; margin-right: 4px; display: none;"></div>
                                        <li>
                                          <!--  <a class="date_filter btn btn-danger btn-xs white" id="dates"><?= $_POST['order'] ?></a>-->
                                            <a class="date_filter btn btn-danger btn-xs white" id="dates"><?php echo $dates; ?></a>
                                            <!--a href="#" class="btn btn-primary btn-xs white" data-toggle="modal" data-target=".search_modal_open"><i class="fa fa-filter"></i> Filter</a-->
                                        </li>
                                    </ul>
                                        <div class="modal fade search_modal_open" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                            <form name="form1" id="form1" method="post" action="buyer/my-offers.php" class="modal fade search_modal_open">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">                                    
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Choose Filter</h4>
                                                        </div>
                                                        <!-- Modal Body -->
                                                        <div class="modal-body">
                                                            <div class="panel-body">                                                        

                                                                <label>Category</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_category" id="filter_category" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Category</option>
                                                                        <?php
                                                                        $result_category = category();
                                                                        while ($row_category = mysqli_fetch_assoc($result_category)) { ?>
                                                                            <option value="<?= $row_category['id']; ?>"><?= $row_category['name']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div> 
                                                                
                                                                <label>Customer</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_customer" id="filter_customer" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Customer</option>
                                                                        <?php
                                                                        $result_customer = customer();
                                                                        while ($row_customer = mysqli_fetch_assoc($result_customer)) { ?>
                                                                            <option value="<?= $row_customer['id']; ?>"><?= $row_customer['name']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div>                                                                 
                                                                
                                                                
                                                                <label>Order</label>
                                                                <div class="form-group">
                                                                    <select style="width: 100%; display: none;" name="filter_variety" id="filter_variety" class="form-control select2 cls_filter" tabindex="-1">
                                                                        <option value="">Select Order</option>
                                                                        <?php
                                                                        $subcategory_sql = "select  id, qucik_desc  from  buyer_orders where  buyer_id='" . $userSessionID . "' order by id desc";
                                                                        $result_subcategory = mysqli_query($con, $subcategory_sql);
                                                                        while ($row_subcategory = mysqli_fetch_assoc($result_subcategory)) { ?>
                                                                            <option value="<?php echo $row_subcategory['id']; ?>"><?php echo $row_subcategory['qucik_desc']; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                </div>                                                                

                                                            </div>
                                                        </div>                                                                
                                                        <!-- Modal Footer -->
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            <button id="btn_filter" class="btn btn-primary apply" type="button">Apply</button>
                                                            <img class="ajax_loader_s" style="display: none;" src="<?php echo SITE_URL; ?>images/ajax-loader.gif"/>
                                                        </div>                                                        
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                <!-- /right options -->
            </div>            
            
            <!-- panel content inicio po -->
            
                <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">            
            
            <div class="panel-body">
                                    <div id='loading'>
                                        <div class=" cssload-spin-box">
                                         <!--   <img src="<?php echo SITE_URL; ?>../includes/assets/images/loaders/5.gif"> -->
                                        </div>
                                    </div>                
                
                
                <div  class="dataRequest">

                                        
                    <table class="table table-hover table-vertical-middle nomargin">
                        <thead>
                        <tr>
                            <th class="width-30">PRODUCT</th>
                            <th>ORDER</th>
                            <th>REQUEST</th>
                            <th>PRICE</th>                            
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                                 $i = 1;
                                 $totalVenta = 0;                                                          
                                 
                            while ($producs = mysqli_fetch_array($result2)) {
                                
                                $sel_check = "select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,
                                                     cr.flag , (gr.bunchqty*gr.steams) as totstems, 
                                                     if(type_market=1,'OPEN','STAND') as tmarquet
                                                from grower_offer_reply gr 
                                                left join growers g  on gr.grower_id = g.id 
                                                left join country cr on g.country_id = cr.id
                                               where gr.request_id='" . $producs["cartid"] . "' 
                                                 and gr.buyer_id='" . $userSessionID . "' 
                                                 and reject in (0) 
                                                 and g.id is not NULL 
                                               order by gr.grower_id , gr.offer_id_index";

                                $rs_growers  = mysqli_query($con, $sel_check);
                                $rs_growers1 = mysqli_query($con, $sel_check);
                                $totalio = mysqli_num_rows($rs_growers);


                                
                                $box_type_name = "";
                                
                                if ($producs["boxtype"] != "") {
                                    $temp = explode("-", $producs["boxtype"]);
                                    $box_type_name = box_type_name($temp[0]);
                                    $box_descrip = box_descrip($temp[0]);                                    
                                }
                                /***************Process count boxes required************************/
                                $bq = 0;
                                $bunst = 0;
                                
                                while ($growers_1 = mysqli_fetch_assoc($rs_growers1)) {
                                    if ($growers_1['status'] == 1) {
                                        $bq    += $growers_1['boxqty'] ;
                                        $bunst += $growers_1['totstems'] ;
                                        $type_unit = $growers_1['unit'] ;
                                    }
                                }
                                
                                
                                if ($type_unit == 'ST') {                                                                    
                                    $cant_required = ($bunst)." " .$box_type_name;                                                               
                                    $cumpli        =(($bunst)/$producs["qty"])*100;                                
                                } else {
                                    $cant_required = ($bq)." " .$box_type_name;                                                               
                                    $cumpli        =(($bq)/$producs["qty"])*100;
                                }                                
                                
                                
                                if ($cant_required < 0) {
                                    $cant_required = 0;
                                }
                                /******************************************************************/
                                /**************************Process LDF***************************************/
                                $date_lftd = "";
                                if ($producs["lfd2"] == '0000-00-00') {
                                    $date_lftd = date("F j, Y", strtotime($producs["del_date"]));
                                } else {
                                    $date_lftd = date("F j, Y", strtotime($producs["del_date"])) . " ";
                                }
                                /****************************Close LFD***************************************/
                                ?>
                                <tr>
                                    <td class="text-center"><a data-toggle="modal" data-target=".flower_smal_<?php echo $i; ?>"><img src="<?php echo SITE_URL . $producs['image_path']; ?>" alt="" width="60"></a>
                                        <div class="modal fade flower_smal_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <!-- header modal -->
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="mySmallModalLabel"><?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] . " cm " ?></h4>
                                                    </div>
                                                    <!-- body modal -->
                                                    <div class="modal-body">
                                                        <img src="<?php echo SITE_URL . $producs['image_path']; ?>" alt="" width="100%">
                                                    </div>
                                                    <h5>Fresh Life Floral</h5>

                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                                        <!-- Columnas Stems -->
                                                        <td width="300">
                                                            <a href="#" data-toggle="modal" data-target="#text_modal_1<?= $i; ?>"><?= $producs["subs"] ?> <?= $producs["name"] ?> <?= $producs["featurename"] ?> <?= $producs["sizename"] . " cm " ?></a>
                                                            <div class="modal fade" id="text_modal_1<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
                                                                 test="dialog" aria-hidden="true">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            <h4 class="modal-title" id="mySmallModalLabel">Description Units</h4>
                                                                        </div>
                                                                        <div class="modal-body"><?= $box_descrip; ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>                                                                        
                                    
                                    <td><?= $producs["qty"] ?> <?= $box_type_name ?></td>                                                                        
                                    
                                    <td><?= $producs["price_card"] ?></td>                                      
                                    
                                    
                                    
                                    
                                    
                                    
                                </tr>
                                <?php
                                
                                    $totalVenta = $totalVenta + $producs["price_card"];
                                    
                                    $i++;
                            }
                                                        
                        ?>
                        </tbody>
                    </table>
                </div>
                
							<div class="row">

								<div class="col-sm-6 text-left">
									<h4><strong>-</strong> </h4>

								</div>
                                                            
								<div class="col-sm-6 text-left">
									<ul class="list-unstyled">
                                                                            <!--li> <strong><h3>Subtotal :  <?php echo $total_bunch;  ?></h3></strong> </li-->                                                                                                                                                                                                              
                                                                            <li> <strong><font color="red">Subtotal ..........:  <?php echo "$".number_format($totalVenta, 2, '.', ',');  ?> </font></strong></li>                                                                                                                                  
									</ul>  
								</div>
                                                           
							</div>                
            </div>
                             </form>  
            <!-- /panel content -->
        </div>
        <!-- /PANEL -->
        
            <nav>
                <ul class="pagination">
                    <?php
                    if ($_POST["startrow"] != 0) {

                        $prevrow = $_POST["startrow"] - $display;

                        print("<li><a href=\"javascript:onclick=funPage($prevrow)\" class='link-sample'>Previous </a></li>");
                    }
                    $pages = intval($num_record / $display);

                    if ($num_record % $display) {

                        $pages++;
                    }
                    $numofpages = $pages;
                    $cur_page = $_POST["startrow"] / $display;
                    $range = 5;
                    $range_min = ($range % 2 == 0) ? ($range / 2) - 1 : ($range - 1) / 2;
                    $range_max = ($range % 2 == 0) ? $range_min + 1 : $range_min;
                    $page_min = $cur_page - $range_min;
                    $page_max = $cur_page + $range_max;
                    $page_min = ($page_min < 1) ? 1 : $page_min;
                    $page_max = ($page_max < ($page_min + $range - 1)) ? $page_min + $range - 1 : $page_max;
                    if ($page_max > $numofpages) {
                        $page_min = ($page_min > 1) ? $numofpages - $range + 1 : 1;
                        $page_max = $numofpages;
                    }
                    if ($pages > 1) {
                        print("&nbsp;");
                        for ($i = $page_min; $i <= $page_max; $i++) {
                            if ($cur_page + 1 == $i) {
                                $nextrow = $display * ($i - 1);
                                print("<li class='active'><a href='javascript:void();'>$i</a></li>");
                            } else {

                                $nextrow = $display * ($i - 1);
                                print("<li><a href=\"javascript:onclick=funPage($nextrow)\"  class='link-sample'> $i </a></li>");
                            }
                        }
                        print("&nbsp;");
                    }
                    if ($pages > 1) {

                        if (!(($_POST["startrow"] / $display) == $pages - 1) && $pages != 1) {

                            $nextrow = $_POST["startrow"] + $display;

                            print("<li><a href=\"javascript:onclick=funPage($nextrow)\" class='link-sample'> Next</a></li> ");
                        }
                    }

                    if ($num_record < 1) {
                        print("<span class='text'>" . $XX . "</span>");
                    }
                    ?></ul>
            </nav>        
        
        
        
    </div>
    
        <form method="post" name="frmfprd" action="">
            <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
        </form>        
</section>

<?php require_once '../includes/footer_new.php'; ?>

<script tpe="text/javascript">
    $(document).ready(function () {
        var iduser = $('#id_user').val();
        var productsearch = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '<?php echo SITE_URL; ?>/includes/autosuggest.php?limit=10&search=%QUERY',
                wildcard: '%QUERY'
            }
        });
        $('.autosuggest #typeSearch').typeahead(null, {
            name: 'productsearch',
            source: productsearch
        });
        $('.typeahead').bind('typeahead:select', function (ev, suggestion) {
            $('.imgDiv').show();
            $.ajax({
                type: 'post',
                url: '<?php echo SITE_URL; ?>buyer/search_request_page_by_name_offers.php',
                data: 'name=' + suggestion + '&id_buyer=' + iduser,
                success: function (data) {
                    $("#pagination_nav").hide();
                    $('.imgDiv').hide();
                    $('.dataRequest tbody').html(data);
                    jQuery('select.select2').select2();

                }
            });
        });

        $body = $("body");
        function call_Ajax_Fliter() {
            jQuery("#btn_filter").click(function () {

                var check_value = 0;
                var data_ajax = "";
                if (jQuery("#filter_category").val() == "" && jQuery("#filter_variety").val() == "" && jQuery("#filter_customer").val() == "" && jQuery("#filter_grower").val() == "" && jQuery("#filter_size").val() == "" && jQuery("#filter_pack").val() == "") {
                    check_value = 1;
                }
                if (check_value == 1) {
                    alert("Please select any one option.");
                }
                else {
                    var filter_category = jQuery("#filter_category").val();
                    var filter_variety = jQuery("#filter_variety").val();
                    var filter_customer = jQuery("#filter_customer").val();
                    var filter_grower = jQuery("#filter_grower").val();
                    var filter_size = jQuery("#filter_size").val();

                    var filter_category_text = jQuery("#filter_category option:selected").text();
                    var filter_variety_text = jQuery("#filter_variety option:selected").text();
                    var filter_customer_text = jQuery("#filter_customer option:selected").text();
                    var filter_grower_text = jQuery("#filter_grower option:selected").text();
                    var filter_size_text = jQuery("#filter_size option:selected").text();
                    
                    jQuery(".ajax_loader_s").css("display", "inline-block");
                    $.ajax({
                        type: 'post',
                        url: '<?php echo SITE_URL; ?>buyer/getFilterResults.php',
                        data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_customer=' + filter_customer + '&filter_grower=' + filter_grower + '&filter_size=' + filter_size,
                        success: function (data) {

                            jQuery("#tab_for_filter").html("");
                            $("#pagination_fresh").hide();
                            jQuery('.search_modal_open').modal('hide');
                            jQuery("#listing_product_s").html(data);
                            var pass_delete_f = "";
                            if (filter_category == "") {
                                var filter_category_temp = "0";
                            }
                            else {
                                var filter_category_temp = filter_category;
                            }
                            if (filter_variety == "") {
                                var filter_variety_temp = "0";
                            }
                            else {
                                var filter_variety_temp = filter_variety;
                            }
                            if (filter_customer == "") {
                                var filter_customer_temp = "0";
                            }
                            else {
                                var filter_customer_temp = filter_customer;
                            }
                            if (filter_grower == "") {
                                var filter_grower_temp = "0";
                            }
                            else {
                                var filter_grower_temp = filter_grower;
                            }
                            if (filter_size == "") {
                                var filter_size_temp = "0";
                            }
                            else {
                                var filter_size_temp = filter_size;
                            }
                            pass_delete_f += "'" + filter_category_text + "'" + ',' + filter_category_temp + ',';
                            pass_delete_f += "'" + filter_variety_text + "'" + ',' + filter_variety_temp + ',';
                            pass_delete_f += "'" + filter_customer_text + "'" + ',' + filter_customer_temp + ',';
                            pass_delete_f += "'" + filter_grower_text + "'" + ',' + filter_grower_temp + ',';
                            pass_delete_f += "'" + filter_size_text + "'" + ',' + filter_size_temp;
                            

                            if (filter_category != "") {
                                var pass_click_cate = "'" + filter_category_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_category_filter" cate_label="' + filter_category_text + '" name="hdn_selected_category_filter" value="' + filter_category + '" />' + filter_category_text);
                            }
                            if (filter_variety != "") {
                                var pass_click_cate = "'" + filter_variety_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_variety_filter" cate_label="' + filter_category_text + '" name="hdn_selected_variety_filter" value="' + filter_variety + '" />' + filter_variety_text);
                            }
                            if (filter_customer != "") {
                                var pass_click_cate = "'" + filter_customer_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_color_filter" cate_label="' + filter_category_text + '" name="hdn_selected_color_filter" value="' + filter_customer + '" />' + filter_customer_text);
                            }
                            if (filter_grower != "") {
                                var pass_click_cate = "'" + filter_grower_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_grower_filter" cate_label="' + filter_category_text + '" name="hdn_selected_grower_filter" value="' + filter_grower + '" />' + filter_grower_text);
                            }
                            if (filter_size != "") {
                                var pass_click_cate = "'" + filter_size_text + "'";
                                jQuery("#tab_for_filter").append('<a class="btn btn-danger btn-xs white" onclick="deleteFilter(' + pass_click_cate + ',' + pass_delete_f + ')" href="javascript:void(0);" style="text-transform: capitalize!important;margin-left:5px;"><i class="fa fa-times"></i><input type="hidden" id="hdn_selected_size_filter" cate_label="' + filter_category_text + '" name="hdn_selected_size_filter" value="' + filter_size + '" />' + filter_size_text);
                            }
                            jQuery("#tab_for_filter").show();

                            jQuery.ajax({
                                type: 'post',
                                url: '<?php echo SITE_URL; ?>buyer/getFilterPagination.php',
                                data: 'filter_category=' + filter_category + '&filter_variety=' + filter_variety + '&filter_customer=' + filter_customer + '&filter_grower=' + filter_grower + '&filter_size=' + filter_size,
                                success: function (data) {
                                    jQuery("#pagination_nav").html(data);
                                    jQuery(".ajax_loader_s").hide();
                                }
                            });
                        }
                    });
                }
            });
        }

        call_Ajax_Fliter();        
    });
</script>

<script>    
    
    function funPage(pageno) {
            document.frmfprd.startrow.value = pageno;
            document.frmfprd.submit();
    }    
    
</script>
