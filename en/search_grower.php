<?php

require_once("../config/config_gcp.php");

$varieties_checked = trim($_REQUEST['varieties_checked'], ",");
$wh = "";


if ($varieties_checked != "") {
    $wh.=" AND g.id IN (" . $varieties_checked . ")";
}

if ($wh != "") {                   
        
        $sel_products = "select g.id , g.growers_name , g.page_desc , g.certificates , g.file_path5 , g.short_desc ,
                                g.active as growers_status , g.country_id 
                           from growers as g 
                          where g.active IN ('active','advertising') 
                               $wh        ";
       
        $rs_catinfo = mysqli_query($con,$sel_products);                      
                                            
 ?>  
<div class="row">
    

        <div class="col-md-9 col-sm-9 col-md-push-3 col-sm-push-3">
                            
        <?php 
                 while($catinfo=mysqli_fetch_array($rs_catinfo)) { 

                    $growers_status=$catinfo['growers_status'];                                
                    $sel_country="SELECT * from country where id='".$catinfo['country_id']."'";

                    $rs_country=mysqli_query($con,$sel_country);
                    $row_Country=mysqli_fetch_array($rs_country);                                
        ?>
                    <div class="row"><!-- item -->
                        
                        <div class="col-md-2"><!-- company logo -->
                            <?php
                                    if($catinfo['file_path5'] != "")  { 
                            ?>
                                            <img src="<?php echo SITE_URL."user/".$catinfo['file_path5'];?>" class="img-responsive growers_logo" alt="company logo88888">
                            <?php                                         
                                    }else{
                            ?>
                                            <img src="../assets/images/demo/people/300x300/no_user.png" class="img-responsive" alt="company logo">
                              <?php } ?>  
                        </div>

                        <div class="col-md-10">
                                <h4 class="margin-bottom-10"><a href="<?php echo SITE_URL."en/single-growers.php?id=".$catinfo['id'];?>"><?php echo $catinfo['growers_name'];?></a></h4>
                                <ul class="list-inline">
                                    
                                <?php 
                                   if($row_Country['name'] != "")  {
                                ?>
                                       <li>
                                           <img src="<?php echo SITE_URL;?>../includes/assets/images/flags/<?php echo $row_Country['flag']?>" />
                                           <?php 
                                       echo $row_Country['name']." ".$start;
                                       ?>
                                       </li>
                                       <?php 
                                   }                                                
                                   ?>
                                            
                                <?php 
                                
                                if($growers_status != "")  {?>
                                    <li>
                                   <?php 
                                    if($growers_status == "active")   { 
                                        echo '<i class="fa fa-info-circle color-green"></i>';
                                        echo "Premium Member";
                                    }

                                    $review_p="select * from review_rating where grower_id='".$catinfo['id']."'";

                                    $rs_review_p=mysqli_query($con,$review_p);
                                    $total_rating=0;
                                    $cnt_row=mysqli_num_rows($rs_review_p);

                                    if($cnt_row > 0) {
                                        while($review_detail=mysqli_fetch_array($rs_review_p))     {
                                            $total_rating+=$review_detail['final_rating'];
                                        }   
                                    }
                                    $f_rating=0;
                                    if($total_rating != 0)   {
                                        $f_rating=($total_rating*5/($cnt_row*5));
                                    }   

                                    ?>  

                                        <span class="size-14 text-muted">
                                            <?php
                                            for($i=1;$i<=5;$i++)    {
                                                if($i <= round($f_rating))   { ?>
                                                    <i class="fa fa-star"></i>
                                                <?php }else{
                                                ?>
                                                    <i class="fa fa-star-o"></i>
                                                <?php }   
                                            }
                                            ?>
                                         </span>    
                                    </li>
                                    <?php 
                                }
                                ?>                                           
                                        </ul>
                                        <p><?php echo $catinfo['short_desc'];?></p>
                        </div>
                        
                        
                     </div><!-- /item -->

            <?php
               } 
            ?>

    </div>

</div>
        
<?php    
}
?>           