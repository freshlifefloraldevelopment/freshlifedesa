<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php

  // PO  2018/07/13

require_once("../config/config_gcp.php");

$userSessionID = $_SESSION["buyer"];

################QUERY TO FETCH PAGE DETAILS###################STARTS###########################################################################

$pageId = 16;//VARIETY PAGE ID 

$pageSql = "SELECT * FROM page_mgmt WHERE page_id=$pageId";

$pageQuery = mysqli_query($con, $pageSql);

$pageData = mysqli_fetch_assoc($pageQuery);

#############QUERY TO FETCH PAGE DETAILS###################ENDS###########################################################################



#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################STARTS################################################################

$cssHeadArray = array(SITE_URL.'../includes/assets/css/essentials-flfv3.css', SITE_URL.'/includes/assets/css/layout-flfv3.css', 

                            SITE_URL.'../includes/assets/css/header-1.css', SITE_URL.'/includes/assets/css/layout-shop.css', SITE_URL.'/includes/assets/css/color_scheme/blue.css' );

$jsHeadArray = array();

#############ADD THE REQUIRED CSS/JS IN PHP ARRAY TO INCLUDE IN HEADER AND KEEP THE HEADER.PHP COMMON################################ENDS################################################################

    

require_once '../includes/header.php';

?>

<style type="text/css">
  div.mega-price-table h5 {
    background-color: rgba(0, 0, 0, 0.03);
    color: #666;
    display: block;
    font-size: 15px;
    font-weight: 700;
    margin: 0;
    padding: 7px;
    text-align: center;
  }
  div.mega-price-table .pricing{
    margin-top: 1px!important;
  }
  #v5 .pricing-title{
    height: 133px;
  }
  ul#product-size-dd li{
    width: 100%;
  }
  div.mega-price-table .pricing-head small {
    background-color: inherit;
}



</style>



    <section class="page-header page-header-xs">

        <div class="container">

            <?php

            $productId = $_GET['id'];#100
            $gidprin = $_GET['id_grow'];

            $sel_product = "SELECT p.id , p.tab_desc1 , p.tab_desc2 , substr(p.tab_desc2,1,78) short_desc2,substr(p.tab_desc2,79,80) short_desc3, p.name , p.image_path , s.description ,c.name name_cat
                              FROM product p
                              JOIN subcategory s ON p.subcategoryid = s.id 
                              JOIN category c ON p.categoryid = c.id 
                             WHERE p.id = $productId";

            $rs_products = mysqli_query($con, $sel_product);

            $product = mysqli_fetch_assoc($rs_products);

            $name = preg_replace("![^a-z0-9]+!i", "-", trim($product["name"]));
            $name_cat = preg_replace("![^a-z0-9]+!i", "-", trim($product["name_cat"]));            

            $tab2 = $product["tab_desc2"];
            
            $sel_img = "SELECT gp.id , gp.product_id , gp.grower_id , gp.categoryid , gp.subcaegoryid , gp.colorid , gp.image_path
                          FROM grower_product gp
                         WHERE image_path IS NOT NULL 
                           AND gp.product_id = '".$productId."' 
                         order by image_path ";      

            $rs_img = mysqli_query($con, $sel_img);      
            
            
 

            ?>

            <h1><?php echo $name; ?></h1>



            <!-- breadcrumbs -->

            <ol class="breadcrumb">

                <li><a href="#">Home</a></li>

                <li><a href="#">Shop</a></li>

                <li class="active">Single</li>

            </ol><!-- /breadcrumbs -->



        </div>

    </section>

    <!-- /PAGE HEADER -->



    <!--MAIN BODY SECTION-->

    <section>

        <div class="container">

            <div class="row">

                <!-- IMAGE 

                <div class="col-lg-4 col-sm-4">

                    <div class="thumbnail relative margin-bottom-3"> -->
                
		<div class="col-md-4 col-sm-4 mix">
									
			<div class="thumbnail relative mb-3">                

                        <?php
                            #$absolute_path = $_SERVER['DOCUMENT_ROOT'] . "/staging/" . $product['image_path'];
                            $absolute_path = SITE_URL.$product['image_path'];
              
            /*            
           $sel_img_prin = "SELECT gp.id , gp.product_id , gp.grower_id , gp.categoryid , gp.subcaegoryid , gp.colorid , gp.image_path
                              FROM grower_product gp
                             WHERE gp.product_id = '".$productId."' 
                               AND gp.grower_id  = '".$gidprin."'  ";

            $rs_img_prin = mysqli_query($con, $sel_img_prin);  
            while($imagen_prin=mysqli_fetch_array($rs_img_prin))	  {
                   $absolute_path = SITE_URL.$imagen_prin['image_path'];
            }                        
              */          
                            
                        ?>

                        <figure id="zoom-primary" class="zoom" data-mode="mouseover">

                            <a class="lightbox bottom-right" href="<?php echo $absolute_path; ?>" data-plugin-options='{"type":"image"}'><i class="glyphicon glyphicon-search"></i></a>

                            <img class="img-responsive" src="<?php echo $absolute_path; ?>" width="1200" height="1500" alt="This is the product title" />

                        </figure>

                    </div>                                                                        


									<!-- Thumbnails (required height:100px) -->
									<div data-for="zoom-primary" class="zoom-more owl-carousel owl-padding-3 featured" data-plugin-options='{"singleItem": false, "autoPlay": false, "navigation": true, "pagination": false}'>
                                                                       		<?php 
                                                                                           $sr=1;

                                                                                            while($imagen=mysqli_fetch_array($rs_img))	  {
                                                                                                
                                                                                            
                                                                                                $image_path = SITE_URL.$imagen['image_path'];
                                                                                            
                                                                                        ?>     
                                                                                                                                                        
										<a class="thumbnail active" href="<?php echo $image_path; ?>">
											<img src="<?php echo $image_path; ?>" height="500" alt="" />
										</a>
                                                                            
                                                                            <?php  } ?>

									</div>
									<!-- /Thumbnails -->   

                </div>
                <!-- /IMAGE -->

<?php


 $sel_ava_box="select  sum(gpb.stock) as stock
                 from grower_product_box_packing gpb
            left join product p on gpb.prodcutid = p.id
            left join subcategory s on p.subcategoryid=s.id  
            left join colors c on p.color_id=c.id 
            left join features ff on gpb.feature=ff.id
            left join sizes sh on gpb.sizeid=sh.id 
            left join boxes b on gpb.box_id=b.id
            left join boxtype bt on b.type=bt.id
            left join growers g on gpb.growerid=g.id
            left join bunch_sizes bs on gpb.bunch_size_id=bs.id
                where g.active     != 'deactive' 
                  and gpb.prodcutid = '".$_REQUEST['id']."' 
                  and gpb.type     != 2  
                  and p.name is not null  
                  and gpb.stock > 0
                  and Date_format(date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y')                                                         
                group by sh.name";
          
  $dataMaximo = mysqli_query($con, $sel_ava_box);
  $box=0;
        while ($dt = mysqli_fetch_assoc($dataMaximo)) {
           $box=$box+ $dt['stock'];       
    }
?>

                <!-- ITEM DESC -->

                <div class="col-lg-5 col-sm-8">

                    <!-- /buttons -->

                    <!-- price -->

                    <div class="shop-item-price">

                        <!--Suggested Market Price&nbsp;-->
                        <?php echo $name = $name_cat . " " .$name; ?>
                        <span id="average_price" class="average_price"></span>
                        <span class="pull-right text-success"><i class="fa fa-check"></i> <?php echo $box;?> boxes available</span>                        
                    </div>

                    <!-- /price -->
                    <hr/>

                    <?php

                    $sel_ava_p="select gpb.prodcutid,gpb.id as gid,GROUP_CONCAT(gpb.price) as price,GROUP_CONCAT(gpb.qty) as qty,gpb.sizeid,gpb.feature,gpb.type as bv,gpb.boxname as bvname,
                                gpb.growerid,p.id,p.name as productname,s.name as sub_cat_name,
                                g.growers_name,sh.name as sizename,ff.name as features_name,b.name as boxname,GROUP_CONCAT(gpb.stock) as stock,
                                GROUP_CONCAT(bs.name) as bunch_stemp_value,GROUP_CONCAT(bt.name) as boxtype from grower_product_box_packing gpb
                              left join product p on gpb.prodcutid = p.id
                              left join subcategory s on p.subcategoryid=s.id  
                              left join colors c on p.color_id=c.id 
                              left join features ff on gpb.feature=ff.id
                              left join sizes sh on gpb.sizeid=sh.id 
                              left join boxes b on gpb.box_id=b.id
                              left join boxtype bt on b.type=bt.id
                              left join growers g on gpb.growerid=g.id
                              left join bunch_sizes bs on gpb.bunch_size_id=bs.id
                              where g.active     != 'deactive' 
                                and gpb.prodcutid = '".$_REQUEST['id']."' 
                                and gpb.type     != 2  
                                and p.name is not null  
                                and gpb.stock > 0
                                and Date_format(date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y')                                                         
                              group by sh.name";
           
                    $rs_ava_p=mysqli_query($con,$sel_ava_p);

                    $number_ava=mysqli_num_rows($rs_ava_p);

                    $sel_ava_box="select  sum(gpb.stock) as stock
                                    from grower_product_box_packing gpb
                               left join product p on gpb.prodcutid = p.id
                               left join subcategory s on p.subcategoryid=s.id  
                               left join colors c on p.color_id=c.id 
                               left join features ff on gpb.feature=ff.id
                               left join sizes sh on gpb.sizeid=sh.id 
                               left join boxes b on gpb.box_id=b.id
                               left join boxtype bt on b.type=bt.id
                               left join growers g on gpb.growerid=g.id
                               left join bunch_sizes bs on gpb.bunch_size_id=bs.id
                              where g.active     != 'deactive' 
                                and gpb.prodcutid = '".$_REQUEST['id']."' 
                                and gpb.type     != 2  
                                and p.name is not null  
                                and gpb.stock > 0
                                and Date_format(date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y')                                                         
                              group by sh.name";
          
  $dataMaximo = mysqli_query($con, $sel_ava_box);
  $box=0;
        while ($dt = mysqli_fetch_assoc($dataMaximo)) {
           $box=$box+ $dt['stock'];     
    }
                            
                    ?>

                    <!-- short description -->
                    <?php /*   <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> */ ?>

                    <p><?php echo $product["short_desc2"]; ?></p>
                    <p><?php echo $product["short_desc3"]; ?></p>                    
                   <!-- 
                       /* nice an beautifull select description from subcategory
                    -->

                    <!-- /short description -->

                    <hr />

                    <!-- FORM -->

                    <form class="clearfix form-inline nomargin" method="get" action="shop-cart.html" onsubmit="return login_status();">

                        <input type="hidden" name="product_id" value="1" />

                        <!-- see assets/js/view/demo.shop.js -->

                        <input type="hidden" id="color" name="color" value="yellow" />

                        <input type="hidden" id="qty" name="qty" value="1" />

                        <input type="hidden" id="size" name="size" value="5" />

                        <!-- see assets/js/view/demo.shop.js -->


                        <div class="btn-group pull-left product-opt-qty">

                            <button type="button" class="btn btn-default dropdown-toggle product-type-dd noradius" id="product-type-dd-box" data-toggle="dropdown">                              
                                <small id="product-box-type"> Box Type</small>
        <span class="caret"></span> 
                                <input type="hidden" value="" id="hdn_box_type" name="hdn_box_type" />
                            </button>

                            <ul   id="product-type-dd" class="dropdown-menu">

                                <li style="width:100%!important;"><a data-val="Jumbo Box" pass_box_type="JB" href="javascript:void(0);">Jumbo Box</a></li>

                                <li style="width:100%!important;"><a data-val="Half Box" pass_box_type="HB" href="javascript:void(0);">Half Box</a></li>

                                <li style="width:100%!important;"><a data-val="Quarter Box" pass_box_type="QB" href="javascript:void(0);">Quarter Box</a></li>

                                <li style="width:100%!important;"><a data-val="Eighth Box" pass_box_type="EB" href="javascript:void(0);">Eighth Box</a></li>

                                <!-- <li style="width:100%!important;"><a data-val="Bunch" href="javascript:void(0);">Bunch</a></li> -->

                            </ul>

                        </div>
                        
                        <!-- /btn-group -->

                        <div class="btn-group pull-left product-opt-size">

                            <button type="button" class="btn btn-default dropdown-toggle product-size-dd noradius" id="product-size-dd-box" data-toggle="dropdown">

                                <span class="caret"></span>

                                Size <small id="product-selected-size">(<span>-</span>)</small>
                                <input type="hidden" value="" id="hdn_product_size" name="hdn_product_size" />
                            </button>

                            <!-- data-val = size value or size id -->

                            <ul id="product-size-dd" class="dropdown-menu" role="menu">
                                <!-- <li class="active"><a data-val="5" href="#">5</a></li> -->
                                <?php
                               $final_size_sql="select gp.prodcutname,gp.prodcutid,gp.price,gp.sizename,gp.sizeid,gp.growerid,gp.growername,feat.name as features_name
                                                from grower_product_price as gp 
                                                LEFT JOIN features as feat ON feat.id=gp.feature 
                                                INNER JOIN product as p ON p.id=gp.prodcutid
                                                where gp.prodcutid='".$_REQUEST['id']."' GROUP BY sizeid,features_name";
                                
                                $result_sk = mysqli_query($con, $final_size_sql);
                                $product_id_k = $_REQUEST['id'];
                                while($row_sk = mysqli_fetch_array($result_sk)){ 
                                  ?>
                                    <li class=""><a data-val="<?php echo $row_sk['sizename'];?>" href="javascript:void(0);" onclick="getPriceDetails(<?php echo $row_sk['sizeid'];?>,<?php echo$product_id_k; ?>);"><?php echo $row_sk['sizename'];?>CM <?php echo $row_sk['features_name']?></a></li>
                                <?php }
                                
                                ?>
                                
                            </ul>

                        </div><!-- /btn-group -->


                        <div class="btn-group pull-left product-opt-qty">

                            <button type="button" class="btn btn-default dropdown-toggle product-qty-dd noradius" id="product-qty-dd-box" data-toggle="dropdown">

                                <span class="caret"></span>

                                Qty <small id="product-selected-qty">(<span>-</span>)</small>
                                <input type="hidden" value="" id="hdn_product_qty" name="hdn_product_qty" />
                            </button>


                            <ul id="product-qty-dd" class="dropdown-menu clearfix" role="menu">
                                <li><a data-val="1" href="#">1</a></li>
                                <li><a data-val="2" href="#">2</a></li>
                                <li><a data-val="3" href="#">3</a></li>
                                <li><a data-val="4" href="#">4</a></li>
                                <li><a data-val="5" href="#">5</a></li>
                                <li><a data-val="6" href="#">6</a></li>
                                <li><a data-val="7" href="#">7</a></li>
                                <li><a data-val="8" href="#">8</a></li>
                                <li><a data-val="9" href="#">9</a></li>
                                <li><a data-val="10" href="#">10</a></li>
                            </ul>

                        </div><!-- /btn-group -->


                       <?php 
                       if($userSessionID != "") { ?>
                            <button class="btn btn-primary pull-left product-add-cart noradius" type="button" onclick="sendRequestPage()" id="btn_request_v"><i class="fa fa-file-text"></i>REQUEST</button>
                            <?php                     
                       }else{
                            ?>
                            <button type="button" class="btn btn-primary pull-left product-add-cart noradius" onclick="window.location.href='<?php echo SITE_URL?>login.php'" ><i class="fa fa-file-text"></i>REQUEST</button>
                       <?php } 
                       ?>
                        

                    </form>

                    <!-- /FORM -->

                    <hr />

                    <!-- Share -->

                    <div class="pull-right">

                        <a href="http://www.facebook.com/sharer.php?u=<?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" class="social-icon social-icon-sm social-icon-transparent social-facebook pull-left" data-toggle="tooltip" data-placement="top" title="Facebook">

                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>

                        </a>

                        <a href="http://twitter.com/home?status=<?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" class="social-icon social-icon-sm social-icon-transparent social-twitter pull-left" data-toggle="tooltip" data-placement="top" title="Twitter">

                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>

                        </a>

                        <a href="http://plus.google.com/share?url=<?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" class="social-icon social-icon-sm social-icon-transparent social-gplus pull-left" data-toggle="tooltip" data-placement="top" title="Google plus">

                            <i class="icon-gplus"></i>
                            <i class="icon-gplus"></i>

                        </a>

                        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>&title=freshlifefloral&summary=&source=" class="social-icon social-icon-sm social-icon-transparent social-linkedin pull-left" data-toggle="tooltip" data-placement="top" title="Linkedin">

                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>

                        </a>



                    </div>

                    <!-- /Share -->

                </div>

                <!-- /ITEM DESC -->

                <!-- INFO -->

                <div class="col-sm-4 col-md-3">

                
                    <?php
                        /*[Start] This is for fetcing sidebar content from CMS*/
      //echo 
                        $html = file_get_contents('http://development.freshlifefloral.com/blog/?page_id=392');
                        /*[END] This is for fetcing sidebar content from CMS*/
                    ?>
       
                </div>

                <!-- /INFO -->



            </div>



            <ul id="myTab" class="nav nav-tabs nav-top-border margin-top-80" role="tablist">
                <li role="presentation" class="active"><a href="#v7" role="tab" data-toggle="tab" id="subs">Substitutes</a></li>
                <li role="presentation"><a href="#v2" role="tab" data-toggle="tab" id="grow">Growers</a></li>

                <li role="presentation"><a href="#v4" role="tab" data-toggle="tab">Availability</a></li>
                <li role="presentation"><a href="#v5" role="tab" data-toggle="tab">Packing Information</a></li>

            </ul>


            <div class="tab-content padding-top-20">

                <!-- SPECIFICATIONS 11-->

                <div role="tabpanel" class="tab-pane fade" id="v2">

                    <div class="container" id="grower-ajax-pagination-container" >                                         

                    <div class="row">               <!-- item -->

                        <div class="col-md-2">      <!-- company logo1 -->

                            <img src="/assets/images/demo/brands/1.jpg" class="img-responsive" alt="company logo">

                        </div>

                        <div class="col-md-10">     <!-- company detail -->

                            <h4 class="margin-bottom-10">Company Name, Inc</h4>

                            <p>Lorem ipsum eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati</p>

                        </div>

                    </div>                          <!-- /item -->


                    <hr>


                    <div class="row">               <!-- item -->

                        <div class="col-md-2">      <!-- company logo2 -->

                            <img src="/assets/images/demo/brands/2.jpg" class="img-responsive" alt="company logo">

                        </div>

                        <div class="col-md-10">     <!-- company detail -->

                            <h4 class="margin-bottom-10">Company Name, Inc</h4>

                            <p>Lorem ipsum eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati</p>

                        </div>

                    </div>                              <!-- /item -->


                    <hr>


                    <div class="row">                   <!-- item -->

                        <div class="col-md-2">          <!-- company logo3 -->

                            <img src="/assets/images/demo/brands/3.jpg" class="img-responsive" alt="company logo">

                        </div>

                        <div class="col-md-10">         <!-- company detail -->

                            <h4 class="margin-bottom-10">Company Name, Inc</h4>

                            <p>Lorem ipsum eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati</p>

                        </div>

                    </div>                              <!-- /item -->



                    <hr>



                    <div class="row">                   <!-- item -->

                        <div class="col-md-2">          <!-- company logo4 -->

                            <img src="/assets/images/demo/brands/4.jpg" class="img-responsive" alt="company logo">

                        </div>

                        <div class="col-md-10">         <!-- company detail -->

                            <h4 class="margin-bottom-10">Company Name, Inc</h4>

                            <p>Lorem ipsum eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati</p>

                        </div>

                    </div>                              <!-- /item -->


                    <hr>


                    <div class="row">                   <!-- item -->

                        <div class="col-md-2">          <!-- company logo5 -->

                            <img src="/assets/images/demo/brands/5.jpg" class="img-responsive" alt="company logo">

                        </div>

                        <div class="col-md-10">         <!-- company detail -->

                            <h4 class="margin-bottom-10">Company Name, Inc</h4>

                            <p>Lorem ipsum eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati</p>

                        </div>

                    </div>                               <!-- /item -->                                           

                    </div>                   

                </div>


                <!-- REVIEWS -->
                <!-- V3 -->


                <div role="tabpanel" class="tab-pane fade" id="v4">

                    <div class="table-responsive">

                         <?php 

                                $sel_ava_p="select gpb.prodcutid,gpb.id as gid,GROUP_CONCAT(gpb.price) as price,GROUP_CONCAT(gpb.qty) as qty,gpb.sizeid,gpb.feature,gpb.type as bv,gpb.boxname as bvname,
                                            gpb.growerid,p.id,p.name as productname,s.name as sub_cat_name,
                                            g.growers_name,sh.name as sizename,ff.name as features_name,b.name as boxname,GROUP_CONCAT(gpb.stock) as stock,
                                            GROUP_CONCAT(bs.name) as bunch_stemp_value,GROUP_CONCAT(bt.name) as boxtype from grower_product_box_packing gpb
                                           left join product p on gpb.prodcutid = p.id
                                           left join subcategory s on p.subcategoryid=s.id  
                                           left join colors c on p.color_id=c.id 
                                           left join features ff on gpb.feature=ff.id
                                           left join sizes sh on gpb.sizeid=sh.id 
                                           left join boxes b on gpb.box_id=b.id
                                           left join boxtype bt on b.type=bt.id
                                           left join growers g on gpb.growerid=g.id
                                           left join bunch_sizes bs on gpb.bunch_size_id=bs.id
                                          where g.active!='deactive' 
                                            and Date_format(date_update,'%d/%m/%Y')=Date_format(now(),'%d/%m/%Y')  
                                            and gpb.prodcutid='".$_REQUEST['id']."' and gpb.type!=2  and p.name is not null  and gpb.stock > 0
                                          GROUP BY sh.name";


                             $rs_ava_p=mysqli_query($con,$sel_ava_p);
                             $number_ava=mysqli_num_rows($rs_ava_p);

                             if($number_ava > 0)   {
                                 ?>

                                <table class="table table-hover table-vertical-middle">

                                <thead>

                                <tr>

                                    <th>Product description</th>
                                    <th>BU/ST</th>
                                    <th>Pack</th>
                                    <th>Unit Price</th>
                                    <th>Boxes for Sell</th>
                                    <th>Buy Now</th>
                                    <th>Counter Bid</th>

                                </tr>

                                </thead>

                                <tbody>

                               <?php 

                                while($ava_p=mysqli_fetch_array($rs_ava_p))   { 

                                    $features_name="";

                                    if($ava_p['features_name'] != "")     {
                                        $features_name=$ava_p['features_name'];
                                    }

                                    $sel_bunch_size="Select gpbs.bunch_sizes,gpbs.is_bunch,gpbs.is_bunch_value from grower_product_bunch_sizes as gpbs 
                                                     where gpbs.grower_id='".$_REQUEST['id']."' AND gpbs.product_id='".$ava_p['prodcutid']."' AND gpbs.sizes='".$ava_p['sizeid']."'";


                                    $rs_bunch_p=mysqli_query($con,$sel_bunch_size);                 
                                    $row_bunch=mysqli_fetch_array($rs_bunch_p);
                                    $exp_boxtype=explode(",",$ava_p['boxtype']);
                                    
                                    for($i=0;$i<count($exp_boxtype);$i++)      { 

                                        ?>

                                        <tr>

                                            <td>

                                                <?php 
                                                
                                                    if($row_bunch['is_bunch'] == 0)   {

                                                        $exp_bunch=explode(",", $ava_p['bunch_stemp_value']);

                                                        //echo $ava_p['sub_cat_name']." ".$ava_p['productname']." ".$ava_p['growers_name']." ".$ava_p['sizename']."cm"." ".$features_name." ".$exp_bunch[0]."st/bu";

                                                        echo $ava_p['growers_name']." ".$ava_p['sizename']."cm"." ".$features_name." ".$exp_bunch[0]."st/bu";

                                                    }else{

                                                        //echo $ava_p['sub_cat_name']." ".$ava_p['productname']." ".$ava_p['growers_name']." ".$ava_p['sizename']."cm"." ".$features_name." ".$row_bunch['is_bunch_value']."st/bu";   

                                                        echo $ava_p['growers_name']." ".$ava_p['sizename']."cm"." ".$features_name." ".$row_bunch['is_bunch_value']."st/bu";   
                                                    }

                                                    

                                                    ?>

                                            </td>

                                            <td>

                                                <?php                                                
                                                    if($row_bunch['is_bunch'] == 0) {
                                                        echo "Stem";  
                                                    }else{
                                                        echo "Bunch";
                                                    }                                                                                                    
                                                ?>     

                                            </td>        

                                             <td>

                                                <?php

                                                $exp_bunch_stemp_value=explode(",", $ava_p['bunch_stemp_value']);
                                                $exp_qty=explode(",", $ava_p["qty"]);    

                                                if($row_bunch['is_bunch'] == 0)  {
                                                    echo $exp_bunch_stemp_value[$i] * $exp_qty[$i];  
                                                }else{
                                                    echo $exp_qty[$i];
                                                }
                                                ?>
                                             </td>


                                            <?php 

                                            $exp_price=explode(",", $ava_p['price']);

                                            if($exp_price[$i] == "")      {
                                                $f_price="0.00";
                                            }else{
                                               $f_price= $exp_price[$i];
                                            }

                                            ?>

                                            <td>$<?php echo $f_price;?></td>                                            

                                            <td><?php

                                                $exp_stock=explode(",", $ava_p['stock']);  

                                                if($exp_boxtype[$i] == "QB")   {
                                                    echo $exp_stock[$i]." Quarter box";
                                                }elseif ($exp_boxtype[$i] == "HB") {
                                                    echo $exp_stock[$i]." Half Box";
                                                }elseif ($exp_boxtype[$i] == "EB") {
                                                    echo $exp_stock[$i]." Eight Box";
                                                }elseif ($exp_boxtype[$i] == "JB") {
                                                    echo $exp_stock[$i]." Jumbo Box";
                                                }
                                            ?>
                                            </td>

                                            <td><span class="label label-success">Buy Now</span></td>
                                            <td><button class="btn btn-success btn-xs"><i class="fa fa-gavel"></i> 100</button></td>

                                        </tr>

                                <?php }

                                }                                                             
                                ?>

                            </tbody>

                            </table>

                        <?php

                        }else{
                                echo "Data Not Found.";
                        } 

                        ?>  

                    </div>

                </div>
<div role="tabpanel" class="tab-pane fade" id="v7">

    <div class="container" id="grower-ajax-pagination-container" >                                         
<div class="owl-carousel featured nomargin owl-padding-10" data-plugin-options='{"singleItem": false, "items": "5", "stopOnHover":false, "autoPlay":4500, "autoHeight": false, "navigation": true, "pagination": false}'>
              <!-- item -->
              <?php 
              $sel_p="select categoryid,subcategoryid,substitutes from product where id='".$_REQUEST['id']."'";
              $rs_product = mysqli_query($con,$sel_p);
              $row_product = mysqli_fetch_array($rs_product);
              $trim_str = $row_product['substitutes'];
              $product_id_s =  trim($trim_str,",");
              
              $sql_sub = "SELECT * FROM product WHERE id IN ($product_id_s)";
              $rs_sub = mysqli_query($con,$sql_sub);
              while ($row_substitutes = mysqli_fetch_array($rs_sub)) {
                ?>
                <div class="shop-item nomargin">
                  <div class="thumbnail">
                    <!-- product image(s) -->
                    <a class="shop-item-image" href="<?php echo SITE_URL."variety-sub-page.php?id=".$row_substitutes['id'];?>">
                      <img class="img-responsive" src="<?php echo SITE_URL.$row_substitutes['image_path'];?>" alt="<?php echo $row_substitutes['name'];?>" />
                    </a>
                    <!-- /product image(s) -->
                  </div>
                  <div class="shop-item-summary text-center">
                    <h2><?php echo $row_substitutes['name'];?></h2>
                  </div>
                  <!-- buttons -->
                  <div class="shop-item-buttons text-center">
                    <a class="btn btn-default" href="<?php echo SITE_URL."/en/variety-sub-page.php?id=".$row_substitutes['id'];?>"><i class="fa fa-file-text"></i> Request</a>
                  </div>
                  <!-- /buttons -->
                </div>
              <?php }
              ?>
              
              <!-- /item -->
            </div>                                          

                    </div>                                     
                </div>

                <div role="tabpanel" class="tab-pane fade" id="v5">

                    <div class="row mega-price-table">

                        <div class="col-md-4 col-sm-6 hidden-sm hidden-xs pricing-desc">

                            <div class="pricing-title">

                                <h3>Grower</h3>

                            </div>

                        </div>

                        <div class="col-md-2 col-sm-6 block">

                            <div class="pricing">

                                <div class="pricing-head">

                                    <h3>Eight Box</h3>

                                    <small><?php 
                                    
                                    $qsel="select gs.grower_id,gs.product_id,gs.sizes,gs.is_bunch,gs.is_bunch_value,gs.bunch_sizes,sh.name as sizename,
                                                  p.id as pid,p.name as productname,bs.name as bunchname,gpb.boxes,gpb.box_value,b.name,
                                                  b.width,b.length,b.height,b.type,
                                                  sub_cat.name as sub_cat_name,fet.name as features_name
                                             from grower_product_bunch_sizes gs
                                            inner JOIN grower_product_box as gpb on gpb.product_id = gs.product_id
                                             left join boxes b                   on gpb.boxes      = b.id
                                            inner JOIN boxtype bt                on bt.id          = b.type
                                             left join product p                 on gs.product_id  = p.id 
                                            inner JOIN subcategory sub_cat       on sub_cat.id     = p.subcategoryid 
                                             left JOIN features as fet           on fet.id         = gs.feature
                                             left join sizes sh                  on gs.sizes       = sh.id
                                             left join bunch_sizes bs            on gs.bunch_sizes = bs.id
                                            where gs.product_id='".$_REQUEST['id']."' and b.type = 11 and p.name is not NULL ORDER by p.name";  
                                    
                                    $rs=mysqli_query($con,$qsel);
                                    $num = mysqli_num_rows($rs);
                                    
                                    if($num>0){
                                            $qsel1="SELECT AVG(wa.weight) as weight FROM `grower_product_box_weight` as wa left join boxes b on wa.box_id=b.id WHERE wa.prodcutid='".$_REQUEST['id']."' AND b.type=11";
                                            $rs1=mysqli_query($con,$qsel1);
                                            $row_p1=mysqli_fetch_array($rs1);                 
                                            $weightq = round($row_p1['weight'], 2);

					    $rs1=mysqli_query($con,$qsel1);
					    $row_p1=mysqli_fetch_array($rs1);									
					    $weightq = round($row_p1['weight'], 2);

                                            echo 'Dimensional Factor: 6000';                                            
                                    }else{
                                            echo 'Dimensional Factor: 6000'; 
                                    }
          ?></small>

                                </div>


                                <h5>    <!-- price -->

                                <?php 
                                if($weightq){
                                    echo '0-4.76 Kg';
                                
                                }else{ 
                                    echo '0-4.76 Kg';} 
                                ?>

                                </h5>   <!-- /price -->

                                <!-- button -->                              

                            </div>

                        </div>


                        <div class="col-md-2 col-sm-6 block">

                            <div class="pricing">

                                <div class="pricing-head">

                                    <h3>Quarter Box</h3>

                                    <small>
                                        <?php 
                                            $qsel="select gs.grower_id,gs.product_id,gs.sizes,gs.is_bunch,gs.is_bunch_value,gs.bunch_sizes,sh.name as sizename,
                                                    p.id as pid,p.name as productname,bs.name as bunchname,gpb.boxes,gpb.box_value,b.name,
                                                    b.width,b.length,b.height,b.type,
                                                    sub_cat.name as sub_cat_name,fet.name as features_name
                                                from  grower_product_bunch_sizes gs
                                                INNER JOIN grower_product_box as gpb on gpb.product_id=gs.product_id
                                                left join boxes b on gpb.boxes=b.id
                                                INNER JOIN boxtype bt ON bt.id=b.type
                                                left join product p on gs.product_id=p.id 
                                                INNER JOIN  subcategory sub_cat ON sub_cat.id=p.subcategoryid 
                                                LEFT JOIN features as fet ON fet.id=gs.feature
                                                left join sizes sh on gs.sizes=sh.id
                                                left join bunch_sizes bs on gs.bunch_sizes=bs.id
                                                where gs.product_id='".$_REQUEST['id']."' and b.type =5 and p.name is not NULL ORDER by p.name";  
                                            
                                            $rs=mysqli_query($con,$qsel);
                                            $num = mysqli_num_rows($rs);

                                            if($num>0){
            $qsel1="SELECT AVG(wa.weight) as weight FROM `grower_product_box_weight` as wa left join boxes b on wa.box_id=b.id WHERE wa.prodcutid='".$_REQUEST['id']."' AND b.type=5";
            $rs1=mysqli_query($con,$qsel1);
            $row_p1=mysqli_fetch_array($rs1);
                  
            $weightq2 = round($row_p1['weight'], 2);
            //echo 'Dimensional Factor: '.$weightq2.'Kg';                                                            
						$qsel1="SELECT AVG(wa.weight) as weight FROM `grower_product_box_weight` as wa left join boxes b on wa.box_id=b.id WHERE wa.prodcutid='".$_REQUEST['id']."' AND b.type=5";
						$rs1=mysqli_query($con,$qsel1);
						$row_p1=mysqli_fetch_array($rs1);
									
						$weightq2 = round($row_p1['weight'], 2);
						//echo 'Dimensional Factor: '.$weightq2.'Kg';                                                                        
            echo 'Dimensional Factor: 6000'; 
                                            }else{
                                                echo 'Dimensional Factor: 0.00Kg';
                                            }
              ?>
                                    </small>

                                </div>



                                <h5>    <!-- price -->

                                    <?php 
                                        if($weightq2){
                                             echo '4.76-9.11 Kg';                                   
                                        }else{ 
                                            echo 'Kg 0.00';                                        
                                        } 
                                    ?>

                                </h5>   <!-- /price -->

                            </div>

                        </div>



                        <div class="col-md-2 col-sm-6 block">

                            <div class="pricing">

                                <div class="pricing-head">

                                    <h3>Half Box</h3>

                                    <small>
                                        <?php 
                                            $qsel="select gs.grower_id,gs.product_id,gs.sizes,gs.is_bunch,gs.is_bunch_value,gs.bunch_sizes,sh.name as sizename,
                                                    p.id as pid,p.name as productname,bs.name as bunchname,gpb.boxes,gpb.box_value,b.name,
                                                    b.width,b.length,b.height,b.type,
                                                    sub_cat.name as sub_cat_name,fet.name as features_name
                                                from  grower_product_bunch_sizes gs
                                                INNER JOIN grower_product_box as gpb on gpb.product_id=gs.product_id
                                                left join boxes b on gpb.boxes=b.id
                                                INNER JOIN boxtype bt ON bt.id=b.type
                                                left join product p on gs.product_id=p.id 
                                                INNER JOIN  subcategory sub_cat ON sub_cat.id=p.subcategoryid 
                                                LEFT JOIN features as fet ON fet.id=gs.feature
                                                left join sizes sh on gs.sizes=sh.id
                                                left join bunch_sizes bs on gs.bunch_sizes=bs.id
                                                where gs.product_id='".$_REQUEST['id']."' and b.type =3 and p.name is not NULL ORDER by p.name";  
                                            
                                        $rs=mysqli_query($con,$qsel);
                                        $num = mysqli_num_rows($rs);
                                        
                                        if($num>0){
              $qsel1="SELECT AVG(wa.weight) as weight FROM `grower_product_box_weight` as wa left join boxes b on wa.box_id=b.id WHERE wa.prodcutid='".$_REQUEST['id']."' AND b.type=3";
              $rs1=mysqli_query($con,$qsel1);
              $row_p1=mysqli_fetch_array($rs1);
                  
              $weightq3 = round($row_p1['weight'], 2);
              //echo 'Dimensional Factor: '.$weightq3.'Kg';
							$qsel1="SELECT AVG(wa.weight) as weight FROM `grower_product_box_weight` as wa left join boxes b on wa.box_id=b.id WHERE wa.prodcutid='".$_REQUEST['id']."' AND b.type=3";
							$rs1=mysqli_query($con,$qsel1);
							$row_p1=mysqli_fetch_array($rs1);
									
							$weightq3 = round($row_p1['weight'], 2);
							//echo 'Dimensional Factor: '.$weightq3.'Kg';
              echo 'Dimensional Factor: 6000'; 
                                                                        
                                        }else{
                                                        echo 'Dimensional Factor: 0.00Kg';
                                        }
              ?>
                                    </small>

                                </div>


                                <h5>    <!-- price -->
                                <?php 
                                    if($weightq3){
                                         echo '9.11-17.92 Kg';                                 
                                    }else{ 
                                        echo 'Kg 0.00';                                    
                                    } 
                                ?>
                                    
                                </h5><!-- /price -->

                             </div>

                        </div>


                        <div class="col-md-2 col-sm-6 block">

                            <div class="pricing">

                                <div class="pricing-head">

                                    <h3>Jumbo Box</h3>

                                    <small>
                                        <?php 
                                            $qsel="select gs.grower_id,gs.product_id,gs.sizes,gs.is_bunch,gs.is_bunch_value,gs.bunch_sizes,sh.name as sizename,
                                                    p.id as pid,p.name as productname,bs.name as bunchname,gpb.boxes,gpb.box_value,b.name,
                                                    b.width,b.length,b.height,b.type,
                                                    sub_cat.name as sub_cat_name,fet.name as features_name
                                                 from  grower_product_bunch_sizes gs
                                                INNER JOIN grower_product_box as gpb on gpb.product_id=gs.product_id
                                                left join boxes b on gpb.boxes=b.id
                                                INNER JOIN boxtype bt ON bt.id=b.type
                                                left join product p on gs.product_id=p.id 
                                                INNER JOIN  subcategory sub_cat ON sub_cat.id=p.subcategoryid 
                                                LEFT JOIN features as fet ON fet.id=gs.feature
                                                left join sizes sh on gs.sizes=sh.id
                                                left join bunch_sizes bs on gs.bunch_sizes=bs.id
                                                where gs.product_id='".$_REQUEST['id']."' and b.type =11 and p.name is not NULL ORDER by p.name";  
                                            
                                        $rs=mysqli_query($con,$qsel);
                                        $num = mysqli_num_rows($rs);

                                        if($num>0){
                $qsel1="SELECT AVG(wa.weight) as weight FROM `grower_product_box_weight` as wa left join boxes b on wa.box_id=b.id WHERE wa.prodcutid='".$_REQUEST['id']."' AND b.type=10";
                                                    $rs1=mysqli_query($con,$qsel1);
                $row_p1=mysqli_fetch_array($rs1);                 
                $weightq4 = round($row_p1['weight'], 2);
                //echo 'Dimensional Factor: '.$weightq4.'Kg';
						    $row_p1=mysqli_fetch_array($rs1);									
						    $weightq4 = round($row_p1['weight'], 2);
						    //echo 'Dimensional Factor: '.$weightq4.'Kg';
                echo 'Dimensional Factor: 6000'; 
                                                                        
                                        }else{
                                                    echo 'Dimensional Factor: 6000'; 
                                        }
              ?>
                                    </small>

                                </div>

                                <h5>    <!-- price -->
                                   <?php 
                                        if($weightq4){
                                            echo '18 Kg and Up';                                        
                                        }else{ 
                                            echo '18 Kg and Up';                                       
                                        } 
                                   ?>
                                </h5>   <!-- /price -->



                            </div>

                        </div>

                        <?php 
                            

                            $qsel="select gs.grower_id,gs.product_id,gs.sizes,gs.is_bunch,gs.is_bunch_value,gs.bunch_sizes,sh.name as sizename,
                                          p.id as pid,p.name as productname,bs.name as bunchname,gpb.boxes,gpb.box_value,b.name,
                                          b.width,b.length,b.height,b.type,GROUP_CONCAT(DISTINCT bt.name) as box_type_name,GROUP_CONCAT(DISTINCT bt.id) as box_type_id,
                                          sub_cat.name as sub_cat_name,fet.name as features_name
                                     from grower_product_bunch_sizes gs
                                    inner JOIN grower_product_box as gpb on gpb.product_id = gs.product_id
                                     left join boxes b                   on gpb.boxes      = b.id
                                    inner JOIN boxtype bt                on bt.id          = b.type
                                     left join product p                 on gs.product_id  = p.id 
                                    inner JOIN subcategory sub_cat       on sub_cat.id     = p.subcategoryid 
                                     left JOIN features as fet           on fet.id         = gs.feature
                                     left join sizes sh                  on gs.sizes       = sh.id
                                     left join growers g                 on gs.grower_id   = g.id                                     
                                     left join bunch_sizes bs            on gs.bunch_sizes = bs.id
                                    where gs.product_id='".$_REQUEST['id']."' and active != 'deactive' and p.name is not NULL GROUP BY p.id,sh.name ORDER by p.name";       

                            $rs=mysqli_query($con,$qsel);

                         ?>

                            

                         <?php

                                $pro_array=array();

                                while($row_p=mysqli_fetch_array($rs))  {                                   

                                      $qsel_grower="SELECT growers_name from growers where id='".$row_p['grower_id']."' and active != 'deactive' ";
                                      $rs_grower=mysqli_query($con,$qsel_grower);
                                      $row_gro=mysqli_fetch_array($rs_grower);


                                      $qsel_growerqa="SELECT weight from grower_product_box_weight where prodcutid='".$_REQUEST['id']."' AND growerid ='".$row_p['grower_id']."'";
                                      $rs_growerq=mysqli_query($con,$qsel_growerqa);
                                      $row_grow=mysqli_fetch_array($rs_growerq);
                                      
                                    ?>

                                      <div class="col-md-4 col-sm-6 hidden-sm hidden-xs pricing-desc"> 


                                         <div class="pricing"> 
                                             
                                            <div class="pricing"> 

                                             <ul class="list-unstyled"> 
                                       
                                            <!-- <li class="alternate">Florana Farms 70 cm S.A. 3-5 Blooms 25st/bu</li> (1) -->

                                            <?php 

                                                $features_name="";

                                                if($ava_p['features_name'] != "")   {
                                                    $features_name=$ava_p['features_name'];
                                                } 

                                            ?>       

                                                <li>  

                                                    <?php 

                                                        if($row_p['is_bunch'] == 0)    {
                                                              echo $row_gro['growers_name']." ".$row_p['sizename']."cm";
                                                            //echo $row_gro['growers_name']."(".$row_p['width'].'cm X '.$row_p['length'].'cm X '.$row_p['height']."cm) V Weight ".$row_grow['weight']."kg";
                                                        } else{

                                                               echo $row_gro['growers_name']." ".$row_p['sizename']."cm"; 
                                                             //echo $row_gro['growers_name']."(".$row_p['width'].'cm X '.$row_p['length'].'cm X '.$row_p['height']."cm) V Weight ".$row_grow['weight']."kg"; 
                                                        }    

                                                    ?>

                                                </li>  

                                            </ul>

                                          </div> 
                                             
                                         </div>

                                    </div>

                                    <?php
                                            $exp_boxtype_eb=explode(",", $row_p['box_type_id']);
                                    ?>

                                    <div class="col-md-2 col-sm-6 block ">

                                         <div class="pricing"> 
                                             
                                            <div class="pricing">                                              

                                            <ul class="pricing-table list-unstyled"> 

                                            <!-- <li class="alternate">Florana Farms 70 cm S.A. 3-5 Blooms 25st/bu</li> (2) -->

                                                <?php 

                                                if(in_array("11", $exp_boxtype_eb))  {

                                                    $box_type_value="SELECT gpb.boxes,b.type FROM  grower_product_box  as gpb 
                                                                     INNER JOIN boxes as b ON b.id=gpb.boxes
                                                                     WHERE  gpb.grower_id ='".$row_p['grower_id']."' AND  gpb.product_id ='".$_REQUEST['id']."' AND b.type=11";

                                                    $rs_box_val=mysqli_query($con,$box_type_value);
                                                    $row_box_val=mysqli_fetch_array($rs_box_val);
                                                                      
                                                     $sel_last_price="select qty from grower_product_box_packing where growerid='".$row_p['grower_id']."' and prodcutid='".$_REQUEST['id']."' and sizeid='".$row_p['sizes']."' and box_id='".$row_box_val['boxes']."' order by id desc limit 0,1";
                                                     $rs_last_price=mysqli_query($con,$sel_last_price);
                                                     $last_price=mysqli_fetch_array($rs_last_price);

                                                     $qty_bunches=0;

                                                     if($last_price['qty'] != "")    {
                                                        $qty_bunches=$last_price['qty'];
                                                     }else{
                                                        $qty_bunches=0;
                                                     }
                                                ?>

                                                    <li>
                                                        <?php 

                                                         if($row_p['is_bunch'] == 0) {

                                                            $total_stm=$row_p['bunchname']*$qty_bunches;

                                                            if($total_stm == "0") { 
                                                        ?>

                                                                <i class="fa fa-times"></i>

                                                                <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>

                                                        <?php 
                                                        
                                                            }else{ 
                                                                 echo $total_stm." Stems";
                                                            }

                                                         }else{

                                                            if($qty_bunches == "0")  { 
                                                                ?>

                                                                <i class="fa fa-times"></i>
                                                                <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>

                                                                <?php                                                                 
                                                            }else{
                                                                echo $qty_bunches." Bunches";    
                                                            }                                                            

                                                         }   

                                                    ?>
                                                    </li>

                                                    <?php 
                                                
                                                            }else{ 
                                                                ?>   
                                                                <li>
                                                                <i class="fa fa-times"></i>
                                                               <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                                                </li>

                                                                <?php 
                                               
                                                            }       

                                                     ?>                                                   

                                            </ul>

                                           </div>
                                             
                                         </div>

                                    </div>

                                    <?php

                                    $exp_boxtype_qb=explode(",", $row_p['box_type_id']);

                                    ?>

                                    <div class="col-md-2 col-sm-6 block">

                                         <div class="pricing"> 
                                             
                                            <div class="pricing">                                              

                                            <ul class="pricing-table list-unstyled"> 

                                            <!-- <li class="alternate">Florana Farms 70 cm S.A. 3-5 Blooms 25st/bu</li> (3) -->

                                                <?php                                                

                                                  if(in_array("5", $exp_boxtype_qb))  {

                                                    $box_type_value="SELECT gpb.boxes,b.type FROM  grower_product_box  as gpb 
                                                                     INNER JOIN boxes as b ON b.id=gpb.boxes
                                                                     WHERE  gpb.grower_id ='".$row_p['grower_id']."' AND  gpb.product_id ='".$_REQUEST["id"]."' AND b.type=5";

                                                    $rs_box_val=mysqli_query($con,$box_type_value);
                                                    $row_box_val=mysqli_fetch_array($rs_box_val);                                                                      

                                                     $sel_last_price="select qty from grower_product_box_packing where growerid='".$row_p['grower_id']."' and prodcutid='".$_REQUEST["id"]."' and sizeid='".$row_p["sizes"]."' and box_id='".$row_box_val['boxes']."' order by id desc limit 0,1";

                                                     $rs_last_price=mysqli_query($con,$sel_last_price);
                                                     $last_price=mysqli_fetch_array($rs_last_price);
                                                     
                                                     $qty_bunches=0;

                                                     if($last_price['qty'] != "")    {
                                                        $qty_bunches=$last_price['qty'];
                                                     }else{
                                                        $qty_bunches=0;
                                                     }

                                                ?>

                                                    <li>
                                                        <?php 

                                                    if($row_p['is_bunch'] == 0)   {

                                                       $total_stm=$row_p['bunchname']*$qty_bunches;

                                                        if($total_stm == "0")  { 
                                                            ?>
                                                            <i class="fa fa-times"></i>
                                                            <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>

                                                        <?php                                                         
                                                        }else{ 
                                                             echo $total_stm." Stems";
                                                        }
                                                    }else{

                                                        if($qty_bunches == "0"){ 
                                                            ?>
                                                                <i class="fa fa-times"></i>
                                                                <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>

                                                            <?php 
                                                            
                                                        }else{
                                                                echo $qty_bunches." Bunches";    
                                                        }
                                                    }    

                                                    ?>
                                                    </li>

                                                    <?php 
                                                
                                                        }else{
                                                            ?>   
                                                            <li>
                                                            <i class="fa fa-times"></i>
                                                            <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                                            </li>
                                                            <?php 
                                                 
                                                        }       

                                                 ?>

                                                    

                                            </ul>

                                           </div>
                                             
                                         </div>

                                    </div>

                                    <?php

                                    $exp_boxtype_hb=explode(",", $row_p['box_type_id']);

                                    
                                    ?>

                                    <div class="col-md-2 col-sm-6 block ">

                                         <div class="pricing"> 
                                             
                                            <div class="pricing">                                              

                                            <ul class="pricing-table list-unstyled">

                                            <!-- <li class="alternate">Florana Farms 70 cm S.A. 3-5 Blooms 25st/bu</li> (4)-->

                                                <?php 

                                                 if(in_array("3", $exp_boxtype_hb))   {

                                                    $box_type_value="SELECT gpb.boxes,b.type FROM  grower_product_box  as gpb 
                                                                     INNER JOIN boxes as b ON b.id=gpb.boxes
                                                                     WHERE  gpb.grower_id ='".$row_p['grower_id']."' AND  gpb.product_id ='".$_REQUEST["id"]."' AND b.type=3";

                                                    $rs_box_val=mysqli_query($con,$box_type_value);
                                                    $row_box_val=mysqli_fetch_array($rs_box_val);
                                                                      

                                                     $sel_last_price="select qty from grower_product_box_packing where growerid='".$row_p['grower_id']."' and prodcutid='".$_REQUEST["id"]."' and sizeid='".$row_p["sizes"]."' and box_id='".$row_box_val['boxes']."' order by id desc limit 0,1";
                                                     $rs_last_price=mysqli_query($con,$sel_last_price);
                                                     $last_price=mysqli_fetch_array($rs_last_price);

                                                     $qty_bunches=0;

                                                     if($last_price['qty'] != "")     {
                                                        $qty_bunches=$last_price['qty'];
                                                     }else{
                                                        $qty_bunches=0;
                                                     }

                                                    ?>

                                                    <li>
                                                        <?php 

                                                        if($row_p['is_bunch'] == 0)  {
                                                                $total_stm=$row_p['bunchname']*$qty_bunches;

                                                                if($total_stm == "0") { 
                                                                    ?>
                                                                    <i class="fa fa-times"></i>
                                                                    <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                                                    <?php                                                                     
                                                                }else{ 
                                                                        echo $total_stm." Stems";
                                                                }
                                                        }else{

                                                                if($qty_bunches == "0") { 
                                                                    ?>
                                                                    <i class="fa fa-times"></i>
                                                                    <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                                                    <?php                                                                     
                                                                }else{
                                                                     echo $qty_bunches." Bunches";    
                                                                }
                                                        }    

                                                        ?>
                                                    </li>

                                                <?php 
                                                
                                                }else{ 
                                                    ?>   

                                                    <li>
                                                        <i class="fa fa-times"></i>
                                                        <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                                    </li>
                                                    <?php }       

                                                    ?>                                                    

                                            </ul>

                                           </div>
                                         </div>

                                    </div>

                                    <?php

                                    $exp_boxtype_jumbo=explode(",", $row_p['box_type_id']);

                                    ?>

                                    <div class="col-md-2 col-sm-6 block ">

                                         <div class="pricing"> 
                                             
                                            <div class="pricing">                                              

                                            <ul class="pricing-table list-unstyled"> 

                                            <!-- <li class="alternate">Florana Farms 70 cm S.A. 3-5 Blooms 25st/bu</li> (5) -->

                                                <?php 

                                                if(in_array("10", $exp_boxtype_jumbo))  {

                                                    $box_type_value="SELECT gpb.boxes,b.type FROM  grower_product_box  as gpb 
                                                                     INNER JOIN boxes as b ON b.id=gpb.boxes
                                                                     WHERE  gpb.grower_id ='".$row_p['grower_id']."' AND  gpb.product_id ='".$_REQUEST["id"]."' AND b.type=10";

                                                    $rs_box_val=mysqli_query($con,$box_type_value);
                                                    $row_box_val=mysqli_fetch_array($rs_box_val);                                                                     

                                                     $sel_last_price="select qty from grower_product_box_packing where growerid='".$row_p['grower_id']."' and prodcutid='".$_REQUEST["id"]."' and sizeid='".$row_p["sizes"]."' and box_id='".$row_box_val['boxes']."' order by id desc limit 0,1";
                                                     $rs_last_price=mysqli_query($con,$sel_last_price);
                                                     $last_price=mysqli_fetch_array($rs_last_price);

                                                     $qty_bunches=0;

                                                     if($last_price['qty'] != "") {
                                                        $qty_bunches=$last_price['qty'];
                                                     }else{
                                                        $qty_bunches=0;
                                                     }

                                                ?>

                                                    <li><?php 

                                                        if($row_p['is_bunch'] == 0)  {

                                                            $total_stm=$row_p['bunchname']*$qty_bunches;

                                                            if($total_stm == "0")  { 
                                                                ?>

                                                                <i class="fa fa-times"></i>
                                                                <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>

                                                                <?php 
                                                                
                                                            }else{ 
                                                                 echo $total_stm." Stems";
                                                            }
                                                        }else{

                                                           if($qty_bunches == "0") { 
                                                               ?>
                                                                <i class="fa fa-times"></i>
                                                                <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                                                <?php                                                                
                                                           }else{
                                                                echo $qty_bunches." Bunches";    
                                                            }
                                                        }    

                                                    ?>
                                                    </li>

                                                <?php                                                 
                                                           }else{
                                                               ?>   
                                                            <li>
                                                                <i class="fa fa-times"></i>
                                                                <span class="hidden-md hidden-lg"><i class="fa fa-times"></i></span>
                                                            </li>
                                                            <?php                                                  
                                                           }       

                                                 ?>                                                    

                                            </ul>

                                           </div>
                                             
                                         </div>

                                    </div>                                    

                                    <?php 

                                        array_push($pro_array,$row_p['id']);

                                } 

                                ?>

                    </div>
                </div>
            </div>
            
            

        </div>

    </section>
    

    <?php 

    require_once '../includes/footer.php'; 

    ?>

<!-- PAGE LEVEL SCRIPTS -->

<script type="text/javascript" src="/includes/assets/js/view/demo.shop.js"></script>
<script type="text/javascript" src="/assets/js/Chart.js"></script>
<script src="/includes/assets/js/highcharts.js"></script>
<script src="/includes/assets/js/exporting.js"></script>
<?php

$pageSql_h="SELECT grower_id,verities_id,market_type,price,country,date,entry_date FROM historic_price WHERE verities_id='".$_REQUEST['id']."' AND date < Now() and date > DATE_ADD(Now(), INTERVAL- 6 MONTH)  order by date";
$pageQuery_h = mysqli_query($con, $pageSql_h);

$month_Array="";
$str_final="";
$i=0;
$rawdata = array();

while ($row_Data_h = mysqli_fetch_array($pageQuery_h)) {

    $month_Array=date("m",strtotime($row_Data_h['date']));
    $day_Array=date("d",strtotime($row_Data_h['date']));
    $year_Array=date("Y",strtotime($row_Data_h['date']));
    $ts = mktime(0, 0, 0, $month_Array,$day_Array, $year_Array);
    $str_final.=$ts.",".$row_Data_h['price']."=====";
    $price_array.=$row_Data_h['price'].",";

     $rawdata[$i] = $row_Data_h;
    $i++;

}
for($i=0;$i<count($rawdata);$i++){
    $time = $rawdata[$i]["date"];
    $date = new DateTime($time);
    $rawdata[$i]["time"]=$date->getTimestamp()*1000;
}

$f_month_Array=trim($month_Array,","); 
$f_month_Array=trim($str_final,"====="); 
echo $f_month_Array;
?>

<script type="text/javascript">

var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
var fmonth_Array='<?php echo $f_month_Array;?>';
var split_array=fmonth_Array.split("=====");
var ftotal_price='<?php echo $price_array;?>';
var split_price=ftotal_price.split(",");
var month=[];

for(i=0;i<split_array.length;i++){
    var str=split_array[i].split(",");
    var final_string=str[0]+","+parseInt(str[1]);
    month.push([final_string]);
}

var price_val=[];

for(j=0;j<split_price.length;j++){
    price_val.push(split_price[j]);    
}

var lineChartData = {

    labels : month,
    datasets : [
        {

            label: "Stading Market",
            fillColor : "rgba(220,220,220,0.2)",
            strokeColor : "rgba(220,220,220,1)",
            pointColor : "rgba(220,220,220,1)",
            pointStrokeColor : "#fff",
            pointHighlightFill : "#fff",
            pointHighlightStroke : "rgba(220,220,220,1)",
            data : price_val
        }
    ]

};

$(function(){
    Highcharts.setOptions({
         global: { 
        useUTC: false
    }
    });
    jQuery('#container').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Price'
        },
        
        xAxis: {
                type: 'datetime',
                tickPixelInterval: 150,
                dateTimeLabelFormats: {
                    month: '%Y',
                    year: '%Y'
                }

        },
        yAxis: {
            title: {
                text: 'Price Value'
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%e. %b}: {point.y:.2f}'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },

        series: [{
            name: 'Price',
                data: (function() {
                   var data = [];
                    <?php
                        for($i = 0 ;$i<count($rawdata);$i++){
                    ?>
                    data.push([<?php echo $rawdata[$i]["time"];?>,<?php echo $rawdata[$i]["price"];?>]);
                    <?php } ?>
                return data;
                })()
            
        }]
    });

    jQuery(".live_price").css('visibility','hidden');

    jQuery("#open_pop").click(function(){

        jQuery(".live_price").css('visibility','visible').show("slow");

    });
    jQuery(".close").click(function(){

        jQuery(".live_price").css('visibility','hidden').show("slow");

    });


    var pid = "<?php echo $_GET['id']; ?>";
    $.ajax({

            url:"/ajax/pagination.php",

            type:"POST",

            data:"action=varietySubPageGrowersPagination&page=1&pid="+pid,

            cache: false,

            success: function(response){

                $('#grower-ajax-pagination-container').html(response);
            }
    });  

    $('#grower-ajax-pagination-container').on('click','.page-numbers',function(){

        $page = $(this).attr('href');
        $pageind = $page.indexOf('page=');
        $page = $page.substring(($pageind+5));

        $.ajax({

            url:"/ajax/pagination.php",
            type:"POST",
            data:"action=varietySubPageGrowersPagination&page="+$page+"&pid="+pid,
            cache: false,

            success: function(response){           

                $('#grower-ajax-pagination-container').html(response);       

            }       

        });

    return false;

    });
    
});  

$("#graph_year").change(function(){

    var selected_year=$(this).val();
    var verities_id="<?php echo $_REQUEST['id']?>";

     $.ajax({

        url:"ajax/graph_data.php",
        type:"POST",
        data:"year="+selected_year+"&verities_id="+verities_id,
        cache: false,

        success: function(response){ 

            var res_val=response.split("#####");                
            var fmonth_Array=res_val[0];
            var split_array=fmonth_Array.split(",");
            var ftotal_price=res_val[1];
            var split_price=ftotal_price.split(",");
            var month=[];

            for(i=0;i<split_array.length;i++)     {

                month.push(split_array[i]);
            }

            var price_val=[];

            for(j=0;j<split_price.length;j++)   {
                price_val.push(split_price[j]);    
            }

            var lineChartDataAjax = {

                labels : month,
                datasets : [

                    {
                        label: "Stading Market",
                        fillColor : "rgba(220,220,220,0.2)",
                        strokeColor : "rgba(220,220,220,1)",
                        pointColor : "rgba(220,220,220,1)",
                        pointStrokeColor : "#fff",
                        pointHighlightFill : "#fff",
                        pointHighlightStroke : "rgba(220,220,220,1)",
                        data : price_val
                    }
                ]
            }

            var ctx_Ajax = document.getElementById("lineChartCanvas").getContext("2d");

            window.myLine = new Chart(ctx_Ajax).Line(lineChartDataAjax, {

                responsive: true

            });

        }       

    });

});
var myLineChart=null;
function getPriceDetails(size_id,product_id_k)
{
  $("#product-size-dd").toggle();
  $("#average_price").html("");
  var ctx_Ajax1 = document.getElementById("lineChartCanvas").getContext("2d");
  if(myLineChart!=null){
        myLineChart.destroy();
    }
   $.ajax({
        url:"getPriceDetailsVariety.php",
        async: false,
        type:"POST",
        data:"action_type=getproductdetails&size_id="+size_id+"&product_id_k="+product_id_k,
        success: function(response){ 
            var res_val=response.split("#####");             
            var fmonth_Array=res_val[0];
            var split_array=fmonth_Array.split(",");
            var ftotal_price=res_val[1];
            var split_price=ftotal_price.split(",");

            if(res_val[2] != "")  {
              var res_price_ave = parseFloat(res_val[2]).toFixed(2);
              $("#average_price").html("($"+res_price_ave+")");
            } 
            var month=[];
            for(i=0;i<split_array.length;i++)      {
                month.push(split_array[i]);
            }

            var price_val=[];
            for(j=0;j<split_price.length;j++)
            {

                price_val.push(split_price[j]);    

            }
            var lineChartDataAjax1 = {
                labels : month,
                datasets : [
                    {
                        label: "Stading Market",
                        fillColor : "rgba(220,220,220,0.2)",
                        strokeColor : "rgba(220,220,220,1)",
                        pointColor : "rgba(220,220,220,1)",
                        pointStrokeColor : "#fff",
                        pointHighlightFill : "#fff",
                        pointHighlightStroke : "rgba(220,220,220,1)",
                        data : price_val
                    }
                ]
            }

            myLineChart = new Chart(ctx_Ajax1).Line(lineChartDataAjax1, {
          responsive: true,
          bezierCurveTension : 0.3
      });

      window.myLine = myLineChart;
      //... chart is drawn
    
            $("#product-size-dd").hide();
        }
    });    
}
$("#product-type-dd li a").click(function(){
  $("#product-box-type").html($(this).attr("data-val"));
  $("#hdn_box_type").val($(this).attr("pass_box_type"));
  $("#product-type-dd").toggle();
});
$("#product-size-dd li a").click(function(){
  $("#hdn_product_size").val($(this).attr("data-val"));
});

$("#product-qty-dd li a").click(function(){
  $("#hdn_product_qty").val($(this).attr("data-val"));
  $("#product-qty-dd").toggle();
});

$("#product-size-dd-box").click(function(){
  $("#product-size-dd").toggle();
});
$("#product-type-dd-box").click(function(){
    $("#product-type-dd").toggle();
});
$("#product-qty-dd-box").click(function(){
  $("#product-qty-dd").toggle();
});



function sendRequestPage(){
  var pass_box_type = $("#hdn_box_type").val();
  var product_size = $("#hdn_product_size").val();
  var product_qty = $("#hdn_product_qty").val();

  if(pass_box_type != "" && product_size !="" && product_qty != ""){
    var product_id = "<?php echo $_REQUEST['id'];?>";
    window.location.href = "<?php echo SITE_URL?>"+"buyer/name-your-price.php?pass_box_type="+pass_box_type+"&product_size="+product_size+"&product_qty="+product_qty+"&product_id="+product_id;
  }else{
    alert("Please select all option.");
  }
} 

</script>
<script>
  function login_status(){
    var is_login = true;
    <?php 
      if($_SESSION['login'] == ''){
    ?>
        is_login = false;
    <?php
      }
    ?>

  }
</script>
<script type="text/javascript">
  $(document).ready(function(){
    //$("#grow").trigger('click');
    $("#grow").get(0).click();
    $("#subs").get(0).click();
  })
</script>
