<?php
require('../config/config_new.php');

$sqlCat = "SELECT gp.categoryid, c.name,c.id
                FROM grower_product gp 
                LEFT JOIN growers g ON gp.grower_id=g.id
                LEFT JOIN category c ON gp.categoryid=c.id
                WHERE g.active='active' AND gp.categoryid!=23 
                GROUP BY gp.categoryid ORDER BY c.name";

$categoryData = mysqli_query($con, $sqlCat);
?>

<div class="navbar-collapse pull-right nav-main-collapse collapse">
    <nav class="nav-main">

        <!--
                NOTE

                For a regular link, remove "dropdown" class from LI tag and "dropdown-toggle" class from the href.
                Direct Link Example:

                <li>
                        <a href="#">HOME</a>
                </li>
        -->
        <ul id="topMain" class="nav nav-pills nav-main">
            <?php $page = explode('/', $_SERVER['REQUEST_URI']);
            if ($page[1] == 'blog' || $page[1] == 'cms') {
                $page1 = $page[2];
            } else {
                $page1 = $page[1];
            }
            $page_query = explode('?', $page1);
            $page2 = $page_query[0]; ?>
            <?php

            $sql = "SELECT * FROM main_menu ORDER BY m_menu_order ASC";

            $res = mysqli_query($con, $sql) or die(mysqli_error());

            while ($row = mysqli_fetch_array($res)) {

                ?>

                <li class="dropdown <?php if (($page2 == 'home-page' && strtoupper($row['m_menu_name']) == 'HOME') || ($page2 == 'about-us' && strtoupper($row['m_menu_name']) == 'ABOUT US') || (($page2 == 'front_end/growers-page.php' || $page2 == 'single-growers.php') && strtoupper($row['m_menu_name']) == 'GROWERS') || ($page2 == 'how-it-works-buyers' && strtoupper($row['m_menu_name']) == 'HOW IT WORKS') || ($page2 == 'contact-us' && strtoupper($row['m_menu_name']) == 'CONTACT US') || (($page2 == 'faqs') && strtoupper($row['m_menu_name']) == 'FAQS') || (($page2 == '/front_end/variety-page.php' || $page2 == '') && strtoupper($row['m_menu_name']) == 'BROWSE')) {
                    echo ' active ';
                } else {
                    echo '';
                }
                echo (strtoupper($row['m_menu_name']) == 'BROWSE') ? 'mega-menu' : ''; ?>" id="<?php echo strtoupper($row['m_menu_name']); ?>"><!-- HOME -->
                    <a class="" href="<?php echo $row['m_menu_link']; ?>">
                        <span class="theme-color"><?php echo strtoupper($row['m_menu_name']); ?></span>
                    </a>

                    <?php

                    $is_submenu = mysqli_query($con, "SELECT * FROM sub_menu WHERE m_menu_id=" . $row['m_menu_id'] . " ORDER BY s_menu_order ASC");
                    $count_sub_menu = mysqli_num_rows($is_submenu);
                    if (strtoupper($row['m_menu_name']) == 'BROWSE' || $count_sub_menu > 0) {
                        ?>
                        <ul class="dropdown-menu">
                            <?php
                            if (strtoupper($row['m_menu_name']) != 'BROWSE') {
                                $sql2 = "SELECT * FROM sub_menu WHERE m_menu_id=" . $row['m_menu_id'] . " ORDER BY s_menu_order ASC";
                                $res_pro = mysqli_query($con, $sql2);
                                while ($pro_row = mysqli_fetch_array($res_pro)) {
                                    ?>
                                    <!--<li><a href="<?php //echo $pro_row['s_menu_link'];
                                    ?>"><?php //echo $pro_row['s_menu_name'];
                                    ?></a></li>-->

                                    <li id="<?php echo $row['m_menu_id'] . '_' . $pro_row['s_menu_id']; ?>"><a href="<?php echo $pro_row['s_menu_link']; ?>"><?php echo $pro_row['s_menu_name']; ?></a></li>
                                    <?php
                                }
                            } else {
                                ?>
                                <li>
                                    <div class="row">

                                        <?php
                                        while ($cat = mysqli_fetch_array($categoryData)) {
                                            $pname = preg_replace("![^a-z0-9]+!i", "-", trim($cat["name"]));
                                            ?>
                                            <div class="col-md-5th">
                                                <ul class="list-unstyled">
                                                    <li><span><?= $cat["name"] ?></span></li>
                                                    <?php
                                                    $sqlSubCat = "SELECT gp.subcaegoryid, s.name,s.id
                                                                        FROM grower_product gp 
                                                                        LEFT JOIN growers g ON gp.grower_id=g.id
                                                                        left join subcategory s ON gp.subcaegoryid=s.id
                                                                        WHERE g.active='active' AND gp.categoryid='" . $cat["id"] . "'
                                                                        AND gp.categoryid!=23 GROUP BY gp.subcaegoryid order by s.name";

                                                    $subCategoryQuery = mysqli_query($con, $sqlSubCat);
                                                    $total = mysqli_num_rows($subCategoryQuery);
                                                    if ($total >= 1) {

                                                        while ($subCategoryData = mysqli_fetch_array($subCategoryQuery)) {
                                                            $cname = preg_replace("![^a-z0-9]+!i", "-", trim($subCategoryData["name"]));
                                                            if ($subCategoryData["name"] != '') {
                                                                ?>
                                                                <li><a href="<?php echo SITE_URL; ?>variety-page.php?cat=<?= $cat["id"]; ?>&subcat=<?= $subCategoryData["id"] ?>"><?= $subCategoryData["name"] ?></a></li>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?>

                                                </ul>

                                            </div>
                                        <?php } ?>
                                    </div>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                        <?php
                    }
                    ?>
                </li>

                <?php
            }
            ?>

        </ul>

    </nav>
</div>

</div>
