/** ********************************************** **
 Your Custom Javascript File
 Put here all your custom functions
 *************************************************** **/



/** Remove Panel
 Function called by app.js on panel Close (remove)
 ************************************************** **/
function _closePanel(panel_id) {
    /** 
     EXAMPLE - LOCAL STORAGE PANEL REMOVE|UNREMOVE
     
     // SET PANEL HIDDEN
     localStorage.setItem(panel_id, 'closed');
     
     // SET PANEL VISIBLE
     localStorage.removeItem(panel_id);
     **/
}
$(document).ready(function () {
    $("#show_hide").hide();
    $("#show_hide1").hide();
    $("#show_d").hide();
    $("#on_click").click(function () {
        $("#show_hide").toggle("slow");
    });
    $("#on_click1").click(function () {
        $("#show_hide1").toggle("slow");
    });
    $("#click_date").click(function () {
        $("#show_d").toggle("slow");
        setTimeout(function () {
            $("#show_d").trigger('click');
        }, 10);
    });
    $('#add_new_box').on('click', function () {
        var r = $('<table class="table table-hover"><thead><tr><th>Box Quantity</th><th>Product</th><th>Bunch Quantity</th></tr></thead><tbody><tr><td><select name="" class="form-control"><option value="">select</option><option value="">1</option><option value="">2</option></select></td><td><select name="" class="form-control"><option value="">select Product</option><option value="">1</option><option value="">Product Name</option><option value="">Product Name</option><option value="">Product Name</option></select></td><td><select name="" class="form-control"><option value="">select</option><option value="">1</option><option value="">2</option></select></td></tr></tbody></table>');
        $(".here").append(r);
    });


    /*Steps of contribution*/
    $(".step_2").hide();
    $(".step_3").hide();
    $(".step_4").hide();
     $("#not_same_show").hide();
    $("#local_dif_show").hide();
    $("#local_same_show").hide();
    $("#same_address_show").hide();
    $("#not_same_address").click(function () {
        $("#not_same_show").show();
        $("#same_address_show").hide();
    });
    $("#same_address").click(function () {
        $("#not_same_show").hide();
        $("#same_address_show").show();
    });

    $("#local_same_address").click(function () {
        $("#local_dif_show").hide();
        $("#local_same_show").show();
    });
    $("#local_now_same_address").click(function () {
        $("#local_dif_show").show();
        $("#local_same_show").hide();
    });
    
    $("#local_same_address_non_usa").click(function(){
$("#local_dif_show_non_usa").hide();
$("#local_same_show_non_usa").show();
}); 
$("#local_now_same_address_non_usa").click(function(){
$("#local_dif_show_non_usa").show();
$("#local_same_show_non_usa").hide();
});
    
    
    $(".next_1").click(function () {
        var grower_name = $("#grower_name :selected").val();
        var custom_growername = $("#custom_growername").val();
       
        if (grower_name != "")
        {
            $(".step_1").hide();
            $(".step_2").show();
        }
        else {
            if (custom_growername == "")
            {
                $("#grower_name").css("border", "1px solid red");
                $("#custom_growername").css("border", "1px solid red");
            }
            else
            {
                $(".step_1").hide();
                $(".step_2").show();
            }
            //$("#grower_name").css("border","1px solid red");
        }
    });
    $(".next_2").click(function () {
        $(".step_2").hide();
        $(".step_3").show();
    });
    $(".next_3").click(function () {
        var check_market_type = $("#recent_purchase :selected").val();
        var check_file = $("#file_recent_purchase").val();
        if (check_market_type != "" && check_file != "")
        {
            $(".step_3").hide();
            $(".step_4").show();
        }
        else {
            if (check_market_type == "")
            {
                $("#recent_purchase").css("border", "1px solid red");
            }
            if (check_file == "")
            {
                $("#file_recent_purchase").css("border", "1px solid red");
                $("#recent_file_temp").css("border", "2px solid red");
            }
        }
    });

    $(".back_1").click(function () {
        $(".step_1").show();
        $(".step_2").hide();
    });
    $(".back_2").click(function () {
        $(".step_2").show();
        $(".step_3").hide();
    });
    $(".back_3").click(function () {
        $(".step_3").show();
        $(".step_4").hide();
    });
    /*Steps of contribution*/
    $( "#create_new_ship" ).click(function() {
  $("#select_country").show();
});
    $("#for_fedx").hide();
    $("#for_local").hide();
    $("#for_miami").hide();
    $("#fedx").click(function () {
        $("#for_miami").hide();
        $("#for_fedx").show();
        $("#for_local").hide();
        $("#last_step").show();
    });
    $("#local").click(function () {
        $("#for_miami").hide();
        $("#for_fedx").hide();
        $("#for_local").show();
        $("#last_step").show();
    });
    $("#miami").click(function () {
        $("#for_miami").show();
        $("#for_fedx").hide();
        $("#for_local").hide();
        $("#last_step").hide();
    });

    $("#for_usa").hide();
    $("#for_non_usa").hide();
    $("#shipping_mod").hide();
//    $("#country_name").on("input", function () {
//        if ((this.value == 'United State') || (this.value == 'united state') || (this.value == 'USA') || (this.value == 'usa') || ((this.value).length > 3))
//        {
//            $("#shipping_mod").show().delay(5000);
//        }
//        else
//        {
//            $("#shipping_mod").hide();
//            $("#for_usa").hide();
//            $("#miami_1st").hide();
//            $("#miami_2nd").hide();
//            $("#for_miami").hide();
//            $("#for_fedx").hide();
//            $("#for_local").hide();
//        }
//    });
    $("#last_step").hide();
//    $("#no_ship").click(function () {
//        var check_usa = $("#country_name").val();
//        if (check_usa == 'USA' || check_usa == 'usa' || check_usa == 'United State' || check_usa == 'united state')
//        {
//            $("#for_usa").show();
//            $("#yes_handle_cargo").hide();
//            $("#for_non_usa").hide();
//            $("#last_step").hide();
//        }
//        else
//        {
//            $("#for_non_usa").show();
//            $("#last_step").show();
//            $("#last_step").show();
//            $("#yes_handle_cargo").hide();
//
//        }
//    });
    $("#yes_handle_cargo").hide();
//    $("#yes_ship").click(function () {
//        var check_usa = $("#country_name").val();
//        if (check_usa == 'USA' || check_usa == 'usa' || check_usa == 'United State' || check_usa == 'united state')
//        {
//            $("#yes_handle_cargo").show();
//            $("#for_usa").hide();
//            $("#miami_1st").hide();
//            $("#miami_2nd").hide();
//            $("#for_miami").hide();
//            $("#for_fedx").hide();
//            $("#for_local").hide();
//            $("#last_step").show();
//        }
//        $("#yes_handle_cargo").show();
//        $("#last_step").show();
//    });
    $("#miami_1st").hide();
    $("#miami_2nd").hide();
    $("#cargo_agency").click(function () {
        $("#miami_1st").show();
        $("#miami_2nd").hide();
        $("#last_step").hide();
        $("#last_step").show();
    });
    $("#shiping_company").click(function () {
        $("#miami_1st").hide();
        $("#miami_2nd").show();
        $("#last_step").show();
    });
    
    
    $("#ccPayment").hide();
    $("#Cheque").hide();
    $("#wire_trans").hide();
    $("#paypal").hide();
    $("#deposite").hide();

    $("#card_payment").click(function(){
        $("#ccPayment").show("slow");
        $("#wire_trans").hide();
        $("#paypal").hide();
        $("#deposite").hide();
        $("#Cheque").hide();
    });
    $("#wire_t").click(function(){
        $("#ccPayment").hide();
        $("#wire_trans").show("slow");
        $("#Cheque").hide();
        $("#paypal").hide();
        $("#deposite").hide();
    });
    $("#check_payment").click(function(){
        $("#ccPayment").hide();
        $("#wire_trans").hide();
        $("#Cheque").show("slow");
        $("#paypal").hide();
        $("#deposite").hide();
    });
    $("#paypal_payment").click(function(){
        $("#ccPayment").hide();
        $("#wire_trans").hide();
        $("#paypal").show("slow");
        $("#deposite").hide();
        $("#Cheque").hide();
    });
    $("#wire_payment").click(function(){
        $("#ccPayment").hide();
        $("#wire_trans").show("slow");
        $("#paypal").hide();
        $("#deposite").hide();
        $("#Cheque").hide();
    });
    $("#direct_payment").click(function(){
        $("#ccPayment").hide();
        $("#wire_trans").hide();
        $("#paypal").hide();
        $("#deposite").show("slow");
        $("#Cheque").hide();
    });
    
    
    
});

jQuery(window).ready(function () {

    loadScript(plugin_path + 'jquery/jquery-ui.min.js', function () { /** jQuery UI **/
        loadScript(plugin_path + 'jquery/jquery.ui.touch-punch.min.js', function () { /** Mobile Touch Slider **/
            loadScript(plugin_path + 'form.slidebar/jquery-ui-slider-pips.min.js', function () { /** Slider Script **/

                /** Slider 0
                 ******************** **/
                var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                var $slidern = jQuery("#slider").slider({
                    range: "min",
                    animate: true,
                    min: 0,
                    max: 11,
                    value: 5
                });

                $slidern.slider("pips", {rest: "label", labels: months});
                $slidern.slider("float", {labels: months});
                $slidern.on("slidechange", function (e, ui) {
                    jQuery("#monthOutput").text(months[ui.value]);
                });


                /** Slider 1
                 ******************** **/
                var $slider1 = jQuery("#slider1").slider({
                    range: true,
                    animate: true,
                    min: 0,
                    max: 10,
                    values: [2, 8]
                });

                jQuery("#slider1").slider("pips", {first: "pip", last: "pip"});



                /** Slider 2
                 ******************** **/
                jQuery("#slider2").slider({
                    range: "min",
                    animate: true,
                    value: 299,
                    min: 99,
                    max: 499,
                    step: 100,
                    slide: function (event, ui) {
                        jQuery("#amount").val("$" + ui.value);
                    }
                });

                jQuery("#amount").val("$" + jQuery("#slider2").slider("value"));
                jQuery("#slider2").slider("pips", {rest: "label", prefix: "$", suffix: ""});
                jQuery("#slider2").slider("float", {prefix: "$", suffix: "", pips: true});



                /** Slider 3
                 ******************** **/
                jQuery("#slider3").slider({
                    range: "max",
                    animate: true,
                    min: 1,
                    max: 10,
                    value: 2,
                    slide: function (event, ui) {
                        jQuery("#bedrooms").val(ui.value);
                    }
                });

                jQuery("#bedrooms").val($("#slider3").slider("value"));
                jQuery("#slider3").slider("pips", {rest: "label"});



                /** Slider 4
                 ******************** **/
                var select = jQuery("#guests");
                var guestnumber = jQuery("#slider4").slider({
                    min: 1,
                    max: 10,
                    animate: true,
                    range: "min",
                    value: select[0].selectedIndex + 1,
                    slide: function (event, ui) {
                        select[0].selectedIndex = ui.value - 1;
                    }
                });

                jQuery("#guests").change(function () {
                    guestnumber.slider("value", this.selectedIndex + 1);
                });

                jQuery("#slider4").slider("pips", {
                    rest: false
                });





                /** Slider 5
                 ******************** **/
                jQuery("#slider5").slider({
                    value: 100,
                    animate: true,
                    min: 0,
                    max: 500,
                    step: 50,
                    range: "min",
                    slide: function (event, ui) {
                        jQuery("#donation").val(ui.value);
                    }
                });

                jQuery("#donation").val(jQuery("#slider5").slider("value"));
                jQuery("#donation").blur(function () {
                    jQuery("#slider5").slider("value", jQuery(this).val());
                });


                /** Vertical 1
                 ******************** **/
                var $sliderv = jQuery("#eq > .sliderv-wrapper").each(function () {
                    var value = parseInt(jQuery(this).text(), 10);
                    jQuery(this).empty().slider({
                        value: value,
                        range: "min",
                        step: 10,
                        animate: true,
                        orientation: "vertical"
                    });
                });

                $sliderv.slider("pips", {
                    first: "pip",
                    last: "pip"
                });


                /** Vertical 2
                 ******************** **/
                var $sliderv1 = $("#eq2 > .sliderv-wrapper").each(function () {
                    var value = parseInt(jQuery(this).text(), 10);
                    jQuery(this).empty().slider({
                        value: value,
                        range: "min",
                        step: 5,
                        animate: true,
                        orientation: "vertical"
                    });
                });

                $sliderv1.slider("pips", {
                    first: "pip",
                    last: "pip"
                });


            });
        });
    });

});
