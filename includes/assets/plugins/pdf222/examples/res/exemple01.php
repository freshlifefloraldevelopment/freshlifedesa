<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */

    // get the HTML
    ob_start();
    include(dirname(__FILE__).'/res/exemple01.php');
    $content = '<div id=":5bk" class="a3s" style="overflow: hidden;">Hello <b>vdfg</b> your invoice no.1 details are as below. <br><br><table style="border-collapse:collapse;font-size:12px;font-family:verdana" cellpadding="5" border="1">
							
							<tbody><tr><td colspan="6" style="padding-top:20px;padding-bottom:20px;font-size:18px;text-align:center"> <img src="https://ci3.googleusercontent.com/proxy/aeIbQz0P7KYfBwCJ8JSI9EVhLEIKreMy9pV8QMckmbOuwrXyCQQUbASS6fIk9RMZA70W5Gp39RmF--Wa-BZHWQ=s0-d-e1-ft#http://freshlifefloral.com/images/logo.png">
							<br>
							Camino Real y Callejon D. OE5-371
							<br>
							<br>
							<b>Barrio El Chilpesito / Quito - Ecuador</b>
							</td><td colspan="7" style="padding:2%">
							<table width="100%">
							
							<tbody><tr><td style="width:25%" align="left"><b>Invoice No:</b>	</td> <td style="width:75%">1</td></tr>
							<tr><td style="width:25%" align="left"><b>DATE:</b>	</td> <td style="width:75%"><span class="aBn" data-term="goog_502561632" tabindex="0"><span class="aQJ">08-22-2014</span></span></td></tr>
							<tr><td style="width:25%" align="left"><b>CLIENT:</b></td> <td style="width:75%">vdfg</td> </tr>
							<tr><td style="width:25%" align="left"><b>ADDRESS:</b></td> <td style="width:75%">this is address</td></tr>
							<tr><td style="width:25%" align="left"><b>PHONE:</b></td> <td style="width:75%">8980069189</td> </tr>
							<tr><td style="width:25%" align="left"><b>Email:</b></td> <td style="width:50%"><a href="mailto:adhiyahiren108@gmail.com" target="_blank">adhiyahiren108@gmail.com</a></td> </tr>
							</tbody></table>
							</td></tr>
							 <tr>
							 
							 <td style="padding:5px;width:50px;text-align:center"> No of Boxes </td>
							 
							<td style="padding:5px;width:50px;text-align:center"> Boxes </td>
							<td style="padding:5px;width:50px;text-align:center"> Box Type </td>
							<td style="padding:5px;width:150px;text-align:center"> Varieties </td>
							<td style="padding:5px;width:50px;text-align:center"> CM </td>
							<td style="padding:5px;width:50px;text-align:center"> Bunch per box </td>
							<td> Total Bunches </td>
							<td style="padding:5px;width:50px;text-align:center"> S/B </td>
							<td style="padding:5px;width:50px;text-align:center"> Total Stems </td>
							<td style="padding:5px;width:50px;text-align:center">  Unit Price  </td>
							<td style="padding:5px;width:50px;text-align:center">  Total  </td>
							<td style="padding:5px;width:60px;text-align:center"> Box Label </td>
							<td style="padding:5px;width:143px;text-align:center"> Growers </td> </tr><tr></tr><tr><td style="padding:5px" rowspan="1">1 - 0</td><td style="padding:5px" rowspan="1"></td><td style="padding:5px" rowspan="1">HB</td><td style="padding:5px">3D  </td><td style="padding:5px">60 </td><td style="padding:5px">5  </td><td style="padding:5px">0 </td><td style="padding:5px">25 </td><td style="padding:5px">0</td><td style="padding:5px">1.26</td><td style="padding:5px">0</td><td style="padding:5px" rowspan="1">box 1 : Hiren , Box 2 : Hiren 3 </td><td style="padding:5px" rowspan="1">FLORANA FARMS S.A.</td></tr><tr><td style="padding:5px">1 - 3</td><td style="padding:5px">3</td><td style="padding:5px">HB</td><td style="padding:5px">Absurda </td><td style="padding:5px">40 </td><td style="padding:5px">8 </td><td style="padding:5px">24 </td><td style="padding:5px">25 </td><td style="padding:5px">600 </td><td style="padding:5px">1.2</td><td style="padding:5px">720</td><td style="padding:5px">vdfg</td><td style="padding:5px">FLORANA FARMS S.A.</td></tr><tr><td style="padding:5px">4 - 5</td><td style="padding:5px">2</td><td style="padding:5px">HB</td><td style="padding:5px">Absurda </td><td style="padding:5px">40 </td><td style="padding:5px">12 </td><td style="padding:5px">24 </td><td style="padding:5px">20 </td><td style="padding:5px">480 </td><td style="padding:5px">1.1</td><td style="padding:5px">528</td><td style="padding:5px">vdfg</td><td style="padding:5px">Test Grower</td></tr><tr><td style="padding:5px" colspan="6" align="right"> <b>Total</b></td><td style="padding:5px"><b>48  <b></b></b></td><td style="padding:5px"></td><td style="padding:5px"><b>1080  <b></b></b></td><td style="padding:5px"><b>1.156</b></td><td style="padding:5px"><b>1248</b></td><td style="padding:5px"></td><td style="padding:5px"></td></tr></tbody></table><br><table width="300" border="1" style="border-collapse:collapse"><tbody><tr>
						 <td>Half Box</td> <td>5</td></tr><tr>
						 </tr><tr> <td>Quater Box</td> <td>0</td></tr><tr>
						  </tr><tr> <td>Total Pieces </td> <td>5</td></tr>
						    <tr> <td>Full Box </td> <td>2.5</td>
						 </tr></tbody></table><br><br><p>All quality problems must be reported in writing and sending pictures within 48 hours after receipt of											
	 the product. No claims for freight will be accepted. We will not be responsible for shipping problems,											
	ACCEPTANCE OF THIS SHIPMENT CONSTITUES AGREEMENT TO ALL OF THE ABOVE TERMS AND 											
	CONDITIONS. If this invoice is not cancelled by its due date, our company will charge you a											
	legal fee as well as the cost.		</p><br><br>Please click on following link to login. <br><a href="http://freshlifefloral.com/" target="_blank">http://freshlifefloral.com/</a><br><br>Thanks!<br><b>Freshlife Floral Team<b>
</b></b></div>';

    // convert in PDF
    require_once(dirname(__FILE__).'/../html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('exemple01.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
