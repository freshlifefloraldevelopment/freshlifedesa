<style type="text/css">
    .inventory_icon {
        width: 165px;
        float: left;
    }

    .req_icon {
        width: 165px;
        float: left;
    }
</style>
<style>
    .quick-cart-box {
        display: none;
    }

    .quick-cart > #cart-icon {
        display: inline !important;
    }

    .quick-cart > #cart-icon > i {
        opacity: 0.6 !important;
    }

    .quick-cart:hover > #cart-icon {
        background: none !important;
        color: #999 !important;
    }

    .quick-cart:hover > #cart-icon > i {
        opacity: 1 !important;
    }

    .quick-cart-box .btn-primary {
        background-color: #449D44 !important;
    }
</style>
<aside id="aside">
    <nav id="sideNav"><!-- MAIN MENU -->
        <ul class="nav nav-list">
            <li <?php if ($page_request == 'dashboard') { ?> class="active" <?php } ?>><!-- dashboard -->
                <a class="dashboard" href="<?php echo SITE_URL; ?>buyer/buyers-account.php"><!-- warning - url used by default by ajax (if eneabled) -->
                    <i class="main-icon fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <?php $page_array = array("request_for_product", "inventory", "counter_bid", "fresh_watch", "client1", "client2"); ?>

            <li <?php if (in_array($page_request, $page_array)) { ?> class="menu-open" <?php } ?>>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-money"></i> <span>Buy Flowers</span>
                </a>
                <ul <?php if (in_array($page_request, $page_array)) { ?> style="display: block;" <?php } ?>><!-- submenus -->

                    <li>
                        <a href="<?php echo SITE_URL; ?>buyer/name-your-price.php" class="req_icon">
                            Request For Quote
                        </a>
                        <a href="#" data-toggle="modal" data-target=".req_icon_info"><i class="fa fa-info-circle"></i></a>

                    </li>
                    <!--inventory.php-->
                    <li <?php if ($page_request == 'inventory') { ?> class="active" <?php } ?>>
                        <!--<a href="<?php echo SITE_URL; ?>buyer/invent.php" class="inventory_icon">Price and Availability</a>-->
                        <a href="<?php echo SITE_URL; ?>buyer/inventory.php" class="inventory_icon">Price and Availability</a>                        
                        <a href="#" data-toggle="modal" data-target=".inventory_icon_info"><i class="fa fa-info-circle"></i></a>
                    </li>
                    
                    <li>
                        <a href="<?php echo SITE_URL; ?>buyer/name-your-price-boxmix.php" class="req_icon">Request Mixed Box</a>
                        <a href="#" data-toggle="modal" data-target=".req_icon_info"><i class="fa fa-info-circle"></i></a>

                    </li>  
                    
                    <li>
                        <a href="<?php echo SITE_URL; ?>buyer/marquet_place.php" class="req_icon">Marquet Place</a>
                        <a href="#" data-toggle="modal" data-target=".req_icon_info"><i class="fa fa-info-circle"></i></a>

                    </li>                                        
                                                                                                                                                                                                                                                
                </ul>
            </li>

            <li <?php if ($page_request == 'growers_offers') { ?> class="active" <?php } ?>>
                <a href="<?php echo SITE_URL; ?>/buyer/my-offers.php">
                    <?php
                    $userSessionID = $_SESSION["buyer"];
                    $query2_side = "select gpb.*, gpb.id as cartid,gpb.date_added ,p.id,p.name,p.color_id,p.image_path,s.name as subs,sh.name as sizename,
                                           ff.name as featurename 
                                      from buyer_requests gpb
                                      left join product p     on gpb.product     = p.id
                                      left join subcategory s on p.subcategoryid = s.id  
                                      left join features ff   on gpb.feature     = ff.id
                                      left join sizes sh      on gpb.sizeid      = sh.id 
                                     where gpb.buyer='" . $userSessionID . "' 
                                       and gpb.lfd>='" . date("Y-m-d") . "' order by gpb.id desc";

                    $result2_side = mysqli_query($con, $query2_side);
                    $tp_side = mysqli_num_rows($result2_side);
                    
                    if ($tp_side >= 1) {
                        $final_grower_offer = 0;
                        while ($producs_side = mysqli_fetch_array($result2_side)) {

                            $sel_check_side = "select gr.*,gr.id as grid,g.growers_name,g.file_path5,cr.name as countryname,cr.flag 
                                                 from grower_offer_reply gr 
                                                 left join growers g  on gr.grower_id=g.id 
                                                 left join country cr on g.country_id=cr.id
                                                where gr.offer_id='" . $producs_side["cartid"] . "' 
                                                  and gr.buyer_id='" . $userSessionID . "' 
                                                  and g.id is not NULL 
                                                 GROUP BY gr.grower_id 
                                                 order by gr.date_added desc";
                            
                            $rs_growers_side = mysqli_query($con, $sel_check_side);
                            $totalio_side = mysqli_num_rows($rs_growers_side);
                            $final_grower_offer = $final_grower_offer + $totalio_side;
                        }
                    }
                    ?>
                    <i class="main-icon fa fa-usd"></i> <span>Grower's offers
                        
                        <?php
                        if ($final_grower_offer != "") { ?>
                            <span class="label label-success radius-10"><b id='grower_cnt'><?php echo $final_grower_offer; ?></b></span>
                        <?php } ?>
                    </span>
                </a>
            </li>
                        
           <li <?php if (in_array($page_request, $page_array)) { ?> class="menu-open" <?php } ?>>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-file-text-o"></i> <span>Report</span>
                </a>
                <ul <?php if (in_array($page_request, $page_array)) { ?> style="display: block;" <?php } ?>><!-- submenus -->
                    
                    <li>
                        <a href="<?php echo SITE_URL; ?>buyer/buyer_invoices.php" class="req_icon"> Invoice </a>
                        <a href="#" data-toggle="modal" data-target=".req_icon_info"><i class="fa fa-info-circle"></i></a>
                    </li>                    

                </ul>
            </li>
			
	<!--    <li >
                <a href="<?php echo SITE_URL; ?>buyer/buyer_report.php" >
                   
                    <i class="main-icon fa fa-file-text-o"></i> <span>Sell Flowers</span>
                </a>
            </li>    --> 

	<!--    <li >
                <a href="<?php echo SITE_URL; ?>buyer/customer_list.php" >
                   
                    <i class="main-icon fa fa-file-text-o"></i> <span>Customer List</span>
                </a>
            </li>                                    
            
	    <li >
                <a href="<?php echo SITE_URL; ?>buyer/offers-subcli.php" >
                   
                    <i class="main-icon fa fa-file-text-o"></i> <span>Client's Offers</span>
                </a>
            </li>  -->
        
        
            <li <?php if (in_array($page_request, $page_array)) { ?> class="menu-open" <?php } ?>>
                <a href="#">
                    <i class="fa fa-menu-arrow pull-right"></i>
                    <i class="main-icon fa fa-group"></i> <span>Customer</span>
                </a>
                <ul <?php if (in_array($page_request, $page_array)) { ?> style="display: block;" <?php } ?>><!-- submenus -->

                    <li>
                        <a href="<?php echo SITE_URL; ?>buyer/customer_list.php" class="req_icon">Customer List </a>
                        <a href="#" data-toggle="modal" data-target=".req_icon_info"><i class="fa fa-info-circle"></i></a>

                    </li>                                        

                    
                    <li <?php if ($page_request == 'inventory') { ?> class="active" <?php } ?>>
                        <a href="<?php echo SITE_URL; ?>buyer/packing-subcli.php" class="inventory_icon">Customer Box Pack</a>
                        <a href="#" data-toggle="modal" data-target=".inventory_icon_info"><i class="fa fa-info-circle"></i></a>
                    </li>       
                    
                                                                                                                                                                                                                                                
                </ul>
            </li>        
        
            
        </ul>
        <!-- SECOND MAIN LIST -->
    </nav>
    <span id="asidebg"><!-- aside fixed background --></span>
</aside>

<!-- HEADER -->
<!--Inventory modal-->
<div class="modal req_icon_info" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <!-- header modal -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mySmallModalLabel">Request For Quotation</h4>
            </div>

            <!-- body modal -->
            <div class="modal-body">
                <p>A request for quotation (RFQ) is a standard buying process whose purpose is to invite growers into a bidding process to bid on specific flower varieties.</p>
            </div>

        </div>
    </div>
</div>
<!--Inventory modal-->
<!--Inventory modal-->
<div class="modal inventory_icon_info" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <!-- header modal -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mySmallModalLabel">Information</h4>
            </div>
            <!-- body modal -->
            <div class="modal-body">
                <p>At some point after the post-harvest is done, our growers have inventory to sell - so we help them move it. When you sign in, you will immediately be able to view our guaranteed inventory - it is the equivalent of looking right into all the growers inventory systems that their own sales staff are using.</p>
            </div>

        </div>
    </div>
</div>
<!--Inventory modal-->
<header id="header">
    <!-- Mobile Button -->
    <button id="mobileMenuBtn"></button>
    <!-- Logo -->
    <span class="logo pull-left"><a href="<?= SITE_URL ?>"> <img src="<?php echo SITE_URL; ?>/includes/assets/images/logo.png" alt="admin panel" height="35"/> </a></span>
    <form method="get" action="page-search.html" class="search pull-left hidden-xs">
        <input type="text" class="form-control" name="k" placeholder="Search for something..."/>
    </form>
    <nav>
        <!-- OPTIONS LIST -->
        <a href="../file/sign-out-adm.php"></a>
        <ul class="nav pull-right">
            <!-- USER OPTIONS -->

            <li class="dropdown pull-left"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <img class="user-avatar" alt="" src="<?php echo $img_url; ?>" height="34"/> <span class="user-name"> <span class="hidden-xs">
                            <?php echo $first_name . " " . $last_name; ?><i class="fa fa-angle-down"></i> </span> </span> </a>
                <ul class="dropdown-menu hold-on-click">
                
                    <li>
                   
            <?php
            if ($page_request == 'dashboard') {
                ?>
		        <a href="<?= SITE_URL ?>sign-out-admby.php"><i class="fa fa-power-off"></i> Go Back to Admin</a></li>
            <?php }else{ ?>				
			<a href="<?= SITE_URL ?>sign-out.php"><i class="fa fa-power-off"></i> Log Out</a></li>
	    <?php } ?>
                
                </ul>
            </li>
            <!-- /USER OPTIONS -->
        </ul>
        <ul class="pull-right nav nav-pills nav-second-main">
            <?php
            $page_path = $_SERVER['PHP_SELF'];
            $page_name = explode('/', $page_path);
            if ($page_name[3] != 'cart.php') {
                ?>
                <li class="quick-cart">
                    <a href="javascript:;" id="cart-icon">
                        <?php
                        $total_items = 0;
                        $sql_cart = "select gr.*,gr.id as grid,p.image_path
                                       from grower_offer_reply gr 
                                       left join product p on  p.name=gr.product
                                      where gr.status='1' and 
                                            gr.buyer_id='" . $_SESSION["buyer"] . "' order  by  gr.grower_id,gr.offer_id_index";
                        
                        $rst_cart = mysqli_query($con, $sql_cart);
                        $result = mysqli_query($con, $sql_cart);
                        $rowcount = mysqli_num_rows($result);
                        ?>
                    </a>
                    <div class="quick-cart-box">
                        <h4>Shopping Cart</h4>
                        <div class="quick-cart-wrapper">
                            <?php
                            if ($rowcount > 0){
                                    $tempArr = array();
                                    $total_price = 0;
                            ?>
                            
                            <?php

                            while ($growers = mysqli_fetch_assoc($rst_cart)) {
                                $total_price += $growers['total_price_st_bu'];
                                ?>
                                <a href="#"><!-- cart item -->
                                    <img src="<?php echo SITE_URL . $growers['image_path']; ?>" width="45" height="45" alt="Product">
                                    <h6><span><?php echo $growers['bunchqty'] . " Bu x ";
                                            echo $growers['steams'];
                                            echo ($v['is_bunch'] == 1) ? ' bunches' : ' stems'; ?><?= " x " . $growers['price'] ?></span></h6>
                                    <small>$<?= $growers['total_price_st_bu']; ?></small>
                                </a><!-- /cart item -->

                            <?php }

                            ?>
                        </div>
                        <!-- quick cart footer -->
                        <div class="quick-cart-footer clearfix">
                            <div>
                                <a href="<?php echo SITE_URL; ?>cart/cart.php" class="btn btn-primary btn-xs pull-right">VIEW CART</a>
                                <span class="pull-left"><strong>TOTAL:</strong> $<?php echo $total_price; ?></span>
                            </div>

                        </div>                        
                        <!-- /quick cart footer -->
                        <?php
                        }
                        else {
                            echo '<p style="margin-top:10px; color: #CD0000; padding:2px 7px;">Please add some items in your cart.</p>';
                        }
                        ?>

                    </div>
                </li>
            <?php } ?>
        </ul>
        <!-- /OPTIONS LIST -->
    </nav>
</header>
<!-- /HEADER -->
<script>
    $(document).ready(function () {
        $('#cart-icon').click(function () {
            $('.quick-cart-box').toggle();
        });
    });

</script>
