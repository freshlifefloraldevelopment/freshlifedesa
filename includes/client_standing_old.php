<!doctype html>

<?php

// PO 2018-08-24

require_once("../config/config_gcp.php");

if (isset($_GET["lang"]) && $_GET["lang"] != "") {
    $_SESSION["lang"] = $_GET["lang"];
}
if (!isset($_SESSION["lang"])) {
    $_SESSION["lang"] = "en";
}
if ($_SESSION["login"] != 1) {
    header("location:" . SITE_URL);
    die;
}
if ($_SESSION["grower"] >= 1) {
    header("location:" . SITE_URL . "vendor-account.php");
    die;
}
$userSessionID = $_SESSION["buyer"];
$idfac = $_GET['idi'];
$cliente_inv = $_GET['id_cli'];

$year = $_GET['idyr'];
$week = $_GET['idwk'];
$scli = $_GET['id_cli'];


if (isset($_GET['sd']) && !empty($_GET['sd'])) {
    $id = $_GET['sd'];
    $qry = "DELETE FROM `buyer_shipping_methods` WHERE `id` = '$id'";


    echo '<script language="javascript">alert("' . $id . '");</script>';
    if (mysqli_query($con, $qry)) {
        header("location:" . SITE_URL . "buyer/buyers-account.php");
        die;
    }
}



/* * *******get the data of session user*************** */
if ($stmt = $con->prepare("SELECT id,first_name,last_name,email,phone,web_site,company,company_role,country,state_text,city,zip,shipping_country,shipping_state_text,shipping_city,
shipping_zip,coordination,address,address2,shipping_address,shipping_address2,name_on_card,credit_card_type,credit_card,cvv,expiration_month,
expiration_year,is_public,biographical_info,profile_image FROM buyers WHERE id =$userSessionID")
) {
    $stmt->bind_param('i', $userSessionID);
    $stmt->execute();
    $stmt->bind_result($userID, $first_name, $last_name, $email, $phone, $web_site, $company, $company_role, $country, $state_text, $city, $zip, $shipping_country, $shipping_state_text, $shipping_city, $shipping_zip, $coordination, $address, $address2, $shipping_address, $shipping_address2, $name_on_card, $credit_card_type, $credit_card, $cvv, $expiration_month, $expiration_year, $is_public, $biographical_info, $profile_image);
    $stmt->fetch();
    $stmt->close();
    if (empty($userID)) {
        /*         * *******If not exist send to home page*************** */
        header("location:" . SITE_URL);
        die;
    }
} else {
    /*     * *******If not statement send to home page*************** */
    echo "else";
    exit();
    header("location:" . SITE_URL);
    die;
}


//  Actualizacion en Grupo
if (isset($_REQUEST["total"])) {
    
    for ($i = 1; $i <= $_POST["totalrow"] - 1; $i++) {
              
            $update = "update standing_order_subclient 
                          set qty_pack  = '" . $_POST["qty_pack-". $_POST["pro-" . $i]] . "'  ,
                              id_state  = '" . $_POST["id_state-". $_POST["pro-" . $i]] . "'  
                        where id='" . $_POST["pro-" . $i] . "'  ";                               
    
            mysqli_query($con, $update);            
                                                
    }
    
  }  
  
  if(isset($_POST["submiso"]) && $_POST["submiso"]=="Add SO")	{	
       $insert="insert into standing_order_subclient set
                            id_year   = '".$year."'  ,
                            id_week   = '".$week."'  ,    
                            id_client = '".$scli."'  ,                
                            product   = '".$_POST["var_add"]."' ,
                            prod_name = '1'   ,
                            qty_pack  = '".$_POST["qty_pack"]."',
                            size      = '".$_POST["size_add"]."' , 
                            steams    = '25'  ,    
                            id_state  = '0'   ,
                            comment = 'CALENDAR'    ";
       
	   mysqli_query($con,$insert);      
  }
      
  
    
$img_url = '/images/profile_images/noavatar.jpg';
if ($profile_image) {
    $img_url = '/images/profile_images/' . $profile_image;
}
$page_request = "dashboard";
?>
<?php require_once '../includes/profile-header.php'; ?>
<?php require_once "../includes/left_sidebar_buyer.php"; ?>

    <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>

<style type="text/css">
    .comment-author .g_title {
        color: #337ab7;
        font-weight: normal;
    }
</style>
<style type="text/css">
    .comment-body.cls_comment_body {
        padding-left: 65px;
    }

    li.grower-comments .comment-body {
        padding-left: 0px;
    }

    .status_modal.in li.comment .comment-body {
        margin-left: 60px;
    }
</style>

<?php
   

    $sqlDetalis="select irs.id  as gid      , irs.id_year , irs.id_week   , irs.id_client , irs.product, 
                        irs.prod_name , irs.buyer   , irs.grower_id , irs.qty_pack  , irs.size   , 
                        irs.steams    , irs.comment,
                        s.name as psubcatego , p.name as name_product , sz.name as size_name,
                        cli.name as subclient,
                        id_state
                  from standing_order_subclient irs
                 INNER JOIN product p on irs.product = p.id 
                 INNER JOIN sizes sz  on irs.size = sz.id 
                 INNER join subcategory s on p.subcategoryid = s.id 
                 INNER JOIN sub_client cli on irs.id_client = cli.id 
                 where irs.id_year   = '" . $year . "'
                   and irs.id_week   = '" . $week . "' 
                   and irs.id_client = '" . $scli . "'    
                 order by s.name,p.name ";

    $result = mysqli_query($con, $sqlDetalis);     


 ?>

            <!-- panel content inicio po -->            
            <form name="frmrequest" id="frmrequest" method="post" action="">
                    <input type="hidden" name="startrow" value="<?php echo $_POST["startrow"]; ?>">
                    <input type="hidden" name="total" id="total" value="<?php echo $total ?>">
			<section id="middle">

				<!-- page title -->
				<header id="page-header">
					<h1>Standing Order</h1>
					<ol class="breadcrumb">
						<li><a href="#">Client</a></li>
						<li class="active">Page</li>
					</ol>
				</header>
				<!-- /page title -->


				<div id="content" class="padding-20">

					<div class="panel panel-default">
            <div class="panel-heading">
                 <!--a href="<?php echo SITE_URL; ?>buyer/client_standing_add.php?idweek=<?php echo $week."&subcli=".$scli."&idyear=".$year ?>" class="btn btn-success btn-xs relative">Add SO</a--> 
                 <a href="<?php echo SITE_URL; ?>buyer/client_calendar.php?id_cli=<?php echo $scli ?>" class="btn btn-success btn-xs relative">Back to calendar</a>                


                 
                 
                 
                 
                <!-- right options -->
                <ul class="options pull-right list-inline">
                    <li><a href="#" class="opt panel_colapse" data-toggle="tooltip" title="Colapse" data-placement="bottom"></a></li>
                    <li><a href="#" class="opt panel_fullscreen hidden-xs" data-toggle="tooltip" title="Fullscreen" data-placement="bottom"><i class="fa fa-expand"></i></a></li>
                    <li><a href="#" class="opt panel_close" data-Confirmed-title="Confirmed" data-Confirmed-message="Are you sure you want to remove this panel?" data-toggle="tooltip" title="Close" data-placement="bottom"><i class="fa fa-times"></i></a></li>
                </ul>
                <!-- /right options -->
                
            </div>                                            

						<div class="panel-body">
                                                    <input type="submit" id="submitu" class="btn btn-success btn-sm" name="submitu" value="Update SO">     
                                                    <input type="submit" id="submiso" class="btn btn-success btn-sm" name="submiso" value="Add SO">     
							<div class="row">                                                                             
								<div class="col-md-6 col-sm-6 text-left">

								<h4><strong>Details SO</strong> </h4>
								<ul class="list-unstyled">
                                                                    
									<li><strong>Variety.:</strong>
                       
                                                                        <?php 	$sel_prodadd="select p.id as prid, p.name as productname , p.categoryid , p.subcategoryid , s.name as subcate, sz.name size_name,
                                                                                                sz.id as szid, qty
                                                                                                from product p
                                                                                               inner join buyer_requests_standing_order brs on p.id = brs.product 
                                                                                               inner join category pc   on p.categoryid    = pc.id 
                                                                                               inner join subcategory s on p.subcategoryid = s.id  
                                                                                               inner join sizes sz on brs.sizeid = sz.id  
                                                                                               where brs.week = '" . $week . "' 
                                                                                               order by p.name" ;
                                
                                                                                        $rs_prodadd=mysqli_query($con,$sel_prodadd);    
                                                                                        

                                                                                        
                                                                                        
                                                                                        
                                                                        ?>	                            
                                                                            
                                                                        <select name="var_add" id="var_add" style="margin-top:10px; height:35px; padding:3px; width:140px;" class="listmenu" onchange="changef();">
                                                                                    <option value="">--Select--</option>
                                                                            <?php				       
                                                                                while($prodcal=mysqli_fetch_array($rs_prodadd)){  
                                                                                    
                                                                                $sel_stems="select gp.product_id , gp.sizes , gp.sid , gp.bunch_sizes , bs.name as bunch
                                                                                              from grower_product_bunch_sizes gp
                                                                                             inner join bunch_sizes bs   on gp.bunch_sizes = bs.id 
                                                                                             where gp.product_id  = '" . $prodcal["prid"] . "'
                                                                                               and gp.sizes       = '" . $prodcal["szid"] . "'
                                                                                               and gp.categoryid  = '" . $prodcal["categoryid"] . "'
                                                                                               and gp.sid         = '" . $prodcal["subcategoryid"] . "'
                                                                                             group by gp.product_id , gp.sizes , gp.sid , gp.bunch_sizes  Limit 1" ;
                                
                                                                                        $rs_stems=mysqli_query($con,$sel_stems);    
                                                                                        $prodstems=mysqli_fetch_array($rs_stems);
                                                                                        
                                                                                        $total_bunches = $prodcal["qty"]/$prodstems["bunch"];
                                                                                        
                                                                            ?>                       
                                                                                    <option value="<?php echo $prodcal["prid"]?>">
                                                                                        <?php echo $prodcal["productname"]." ".$total_bunches." Bunch ".$prodcal["subcate"]." ".$prodcal["size_name"]." cm ".$prodstems["bunch"]." st/bu"?>
                                                                                    </option>                        
                                                                            <?php    }	?>		
                                                                        </select>
                                                                        </li>
                                                                        
									<li><strong>Size.......:</strong>
                       
                                                                        <?php 	$sel_sizeadd="select id as idsz,cast(name as int) newsize from  sizes 
                                                                                               order by newsize" ;
                                
                                                                                        $rs_sizeadd=mysqli_query($con,$sel_sizeadd);                                                                                                
                                                                        ?>	                            
                                                                            
                                                                        <select name="size_add" id="size_add" style="margin-top:10px; height:35px; padding:3px; width:140px;" class="listmenu" onchange="changef();">
                                                                                    <option value="">--Select--</option>
                                                                            <?php				       
                                                                                while($sizecal=mysqli_fetch_array($rs_sizeadd)){                                    
                                                                            ?>                       
                                                                                    <option value="<?php echo $sizecal["idsz"]?>">
                                                                                        <?php echo $sizecal["newsize"]?>
                                                                                    </option>                        
                                                                            <?php    }	?>		
                                                                        </select>
                                                                        </li>
									<li><strong>Bunch...:</strong>
                                                                                                   
                                                                        <select name="qty_pack" id="qty_pack" style="margin-top:10px; height:35px; padding:3px; width:140px;" class="listmenu" onchange="changef();">
                                                                                    <option value="">--Select--</option>
                                                                                <?php
                                                                                 for ($ib = 1; $ib <= 20; $ib++) {
                                                                                ?>
                                                                                       <option value="<?= $ib ?>"><?= $ib ?></option>
                                                                                <?php } ?>                                                                                                                                        
                                                                        </select>
                                                                        </li>
								</ul>
								</div>

								<div class="col-md-6 col-sm-6 text-right">
									<h4><strong>-</strong> </h4>
									<ul class="list-unstyled">                                                                              
                                                                                <li>                                                                                    
                                                                                    <div class="col-md-6 col-sm-6 text-right"></div>  
                                                                                </li>
									</ul>
								</div>

							</div>

							<div class="table-responsive">
								<table class="table table-condensed nomargin">
									<thead>
										<tr>
											<th>Variety</th>
                                                                                        <th>Size</th>
                                                                                        <th>Bunches</th>
                                                                                        <th>State</th>
                                                                                        <th>Client</th>
										</tr>
									</thead>
                                                                        
									<tbody>																					

									<?php 
                                                                                 $tmp_idorder = 0;
                                                                                 
                                                                                 $cn=1;
                                                                                 
                                         while($row = mysqli_fetch_assoc($result))  {
                                                                                  
                                                                                         
                                                                                          
                                                                            ?>
										<tr>
										    <td>
                                                                                        <?php

        

         // Verificacion Stems/Bunch
                                                                                            
        $sel_subcatego = "select name from subcategory where id = '" . $bunch_stem['subcategoryid'] . "' ";
        $rs_subcatego = mysqli_query($con,$sel_subcatego);       
        $subc = mysqli_fetch_array($rs_subcatego);

                          ?>
                                                                                                                                           
                                                                                        <div><?php echo $row['name_product']." ".$row['psubcatego']; ?></div>
											<!--small><?php echo $row['psubcatego'] ?></small-->
										</td> 
                                                                                                                            
                                                                                    
										<td>
                                                                                        <?php echo $row['size_name']."  cm." ; ?>
                                                                                </td>                                                                                    
                                                                                    
										<td>                                                                   
                                                                                        <input type="text" class="form-control" name="qty_pack-<?php echo $row['gid'] ?>" id="qty_pack-<?php echo $row['gid'] ?>" value="<?php echo $row['qty_pack'] ?>">
                                                                                </td>

                                                                                    <td>
                                                                                        <select style="width: 100%; diplay: none;" name="id_state-<?php echo $row["gid"] ; ?>" id="id_state<?php echo $row["gid"] ; ?>" class="form-control select2 fancy-form-select" tabindex="-1">
                                                                                            <option value="0" <?php  if($row['id_state'] == 0) echo "selected";?>>REQUEST</option>
                                                                                            <option value="1" <?php  if($row['id_state'] == 1) echo "selected";?>>CANCEL</option>   
                                                                                        </select>                                                                        
                                                                                    </td>                                                                                                                                                                     
                                                                                        <input type="hidden" class="form-control" name="pro-<?php echo $cn ?>" value="<?php echo $row["gid"] ?>"/>
                                                                                                                                                                                                                                                     

                                                                                    
										    <td>
                                                                                        <?php echo $row['subclient'];?>
                                                                                    </td>
                                                                                    
                                                                                 
										</tr>																				
											<?php 
                                                                                                $totalCal = $totalCal + $Subtotal;   
                                                                                                $cn++;
                                                                                                                                                                      
                                                                             } ?>
									</tbody>
								</table>
							</div>

							<hr class="nomargin-top" />

							<div class="row">

								<div class="col-sm-6 text-left">
									<h4><strong>-</strong> </h4>

                                                                        <br>

									<address>
                                                                                --<br>
									</address>

								</div>
                                                            
								<div class="col-sm-6 text-right">
									<!--ul class="list-unstyled">
                                                                            <li> <strong>Sub - Total Amount: </strong> <?php echo "$".number_format($totalCal, 2, '.', ',');  ?>                                                                            </li>                                                                                                                                  
									</ul-->  
								</div>
                                                           
							</div>

						</div>
					</div>					

					<div class="panel panel-default text-right">
						<div class="panel-body">
							<!--a class="btn btn-success" href="<?php echo SITE_URL; ?>buyer/print_invoice_client.php?b=<?php echo $idfac."&id_cli=".$cliente_inv ?>" target="_blank"><i class="fa fa-print"></i> INVOICE</a-->                                                    
						</div>
					</div>                                                                                                                    
                                    
                                    
				</div>
			</section>
			<!-- /MIDDLE -->

		</div>



	
		<!-- JAVASCRIPT FILES -->
		<script type="text/javascript">var plugin_path = 'assets/plugins/';</script>
		<script type="text/javascript" src="assets/plugins/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="assets/js/app.js"></script>

	<input type="hidden" name="totalrow" value="<?php echo $cn ?>"/>
        </form>
</html>
<script>
    
    function boxQuantityChange(id, cartid, main_tr, with_a) {
        
        var selected_text = $("#request_qty_box_" + main_tr + " option:selected").text();
        var selected_val  = $("#request_qty_box_" + main_tr + " option:selected").val();
        
        	//console.log(selected_val);
                
		var nbox = selected_val.split('-');
                var tbox = $.trim(selected_text).split(" ");
                
		$('input[name=boxcant]').val(nbox[0]);
                
                $('input[name=boxtypen]').val(tbox[1]);
                
          //                      
        $('input[name=product_su]').val(selected_val);
        $(".cls_hidden_selected_qty").val(selected_val);
        

                         
    }    
</script>    
    
    