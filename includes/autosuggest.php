<?php
include "../config/config_gcp.php";

if(!empty($_REQUEST['page_name']) && $_REQUEST['page_name'] == 'inventory') {
    if (isset($_REQUEST['search']) && !empty($_REQUEST['search'])) {
    $array = array();
    $LIMIT = isset($_REQUEST['limit']) ? (int) $_REQUEST['limit'] : 30;
    $KEYWORD = isset($_REQUEST['search']) ? (string) $_REQUEST['search'] : null;
//    echo "SELECT * FROM product WHERE name LIKE '%$KEYWORD%' LIMIT $LIMIT";
    $res = mysqli_query($con, "SELECT gpb.prodcutid,
                                      gpb.comment  ,gpb.id AS gid , gpb.stock AS stock , gpb.qty , gpb.sizeid , gpb.feature , gpb.type AS bv , 
                                      gpb.boxname AS bvname , gpb.box_type AS bvtype , gpb.price , gpb.growerid , 
                                      p.id,p.name , p.color_id , p.image_path,
                                      s.name AS subs,
                                      g.file_path5 , g.growers_name , sh.name AS sizename , ff.name AS featurename , b.name AS boxname ,
                                      b.width , b.length , b.height , bs.name AS bname , bt.name AS boxtype , b.type ,c.name AS colorname ,gpb.box_id 
                                 FROM grower_product_box_packing gpb 
                                 LEFT JOIN product p ON gpb.prodcutid = p.id 
                                 LEFT JOIN subcategory s ON p.subcategoryid=s.id 
                                 LEFT JOIN colors c ON p.color_id=c.id 
                                 LEFT JOIN features ff ON gpb.feature=ff.id 
                                 LEFT JOIN sizes sh ON gpb.sizeid=sh.id 
                                 LEFT JOIN boxes b ON gpb.box_id=b.id 
                                 LEFT JOIN boxtype bt ON b.type=bt.id 
                                 LEFT JOIN growers g ON gpb.growerid=g.id 
                                 LEFT JOIN bunch_sizes bs ON gpb.bunch_size_id=bs.id 
                                WHERE g.active='active' 
                                  AND gpb.stock > 0 
                                  AND p.name LIKE '%$KEYWORD%' 
                                  AND p.name IS NOT NULL 
                                GROUP BY p.id ORDER BY s.name,p.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER),g.growers_name LIMIT 0,50 ");     
    
    
    while ($fetchres = mysqli_fetch_array($res)) {
        $array[] = $fetchres['name'];
    }
    // Convert to JSON
    $json = json_encode($array);
    die($json);
}
}else if(!empty($_REQUEST['page_name']) && $_REQUEST['page_name'] == 'counterbid') {
    if (isset($_REQUEST['search']) && !empty($_REQUEST['search'])) {
    $array = array();
    $LIMIT = isset($_REQUEST['limit']) ? (int) $_REQUEST['limit'] : 30;
    $KEYWORD = isset($_REQUEST['search']) ? (string) $_REQUEST['search'] : null;
    $query2 = "select gpb.prodcutid,
                      gpb.comment , gpb.id as gid , gpb.stock as stock , gpb.qty , gpb.sizeid , gpb.feature , gpb.type as bv,
                      gpb.boxname as bvname,gpb.box_type as bvtype,gpb.growerid,gpb.price,
                      p.id,p.name,p.color_id,p.image_path,
                      s.name as subs,
                      g.file_path5,g.growers_name,sh.name as sizename,ff.name as featurename,b.name as boxname,
                      b.width,b.length,b.height,bs.name as bname,bt.name as boxtype,bt.id as boxtypeid,b.type,
                      c.name as colorname,gpb.box_id from grower_product_box_packing gpb
			  left join product p on gpb.prodcutid = p.id
			  left join subcategory s on p.subcategoryid=s.id  
	                  left join colors c on p.color_id=c.id 
			  left join features ff on gpb.feature=ff.id
			  left join sizes sh on gpb.sizeid=sh.id 
			  left join boxes b on gpb.box_id=b.id
			  left join boxtype bt on b.type=bt.id
			  left join growers g on gpb.growerid=g.id
			  left join bunch_sizes bs on gpb.bunch_size_id=bs.id
			 where g.active='active' AND p.name LIKE '%$KEYWORD%' 
                           and p.name  is not null  
                           and sh.name is not null 
                           and  bt.name is not null 
                         GROUP BY p.id order by p.name,CONVERT(SUBSTRING(sh.name,1), SIGNED INTEGER) LIMIT 0,50";
    
    
    
    $res = mysqli_query($con, $query2);
    while ($fetchres = mysqli_fetch_array($res)) {
        $array[] = $fetchres['name'];
    }
    // Convert to JSON
    $json = json_encode($array);
    die($json);
}
}else{
    if (isset($_REQUEST['search']) && !empty($_REQUEST['search'])) {
    $array = array();
    $LIMIT = isset($_REQUEST['limit']) ? (int) $_REQUEST['limit'] : 30;
    $KEYWORD = isset($_REQUEST['search']) ? (string) $_REQUEST['search'] : null;
    //echo "SELECT * FROM product WHERE name LIKE '%$KEYWORD%' LIMIT $LIMIT";
    $res = mysqli_query($con, "SELECT name FROM product WHERE name LIKE '%$KEYWORD%' LIMIT 15");
        while ($fetchres = mysqli_fetch_array($res)) {
        $array[] = $fetchres['name'];
    }
    $json = json_encode($array);
    die($json);
}
}
?>
